#!/usr/bin/env php
<?php
/**
 * Yii Application Initialization Tool
 *
 * In order to run in non-interactive mode:
 *
 * init --env=Development --overwrite=n
 *
 * @author Alexander Makarov <sam@rmcreative.ru>
 *
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

if (!extension_loaded('openssl')) {
    die('The OpenSSL PHP extension is required by Yii2.');
}

$params = getParams();
$root = str_replace('\\', '/', __DIR__);
$envs = require("$root/environments/index.php");
$envNames = array_keys($envs);

echo "Yii Application Initialization Tool v1.0\n\n";

$envName = null;

if (empty($params['full'])) {
    echo "\n\e[1;31mShort init\033[0m, just copy environment files.";
    echo "\nTo make complete init use --full option.\n\n";
} else {
    echo "\n\e[1;31mFull init\033[0m, copy environment files, generate cookie validation keys etc.\n\n";
}

if (empty($params['env']) || $params['env'] === '1') {
    echo "Which environment do you want the application to be initialized in?\n\n";
    foreach ($envNames as $i => $name) {
        echo "  [$i] $name\n";
    }
    echo "\n  Your choice [0-" . (count($envs) - 1) . ', or "q" to quit] ';
    $answer = trim(fgets(STDIN));

    if (!ctype_digit($answer) || !in_array($answer, range(0, count($envs) - 1))) {
        echo "\n  Quit initialization.\n";
        exit(0);
    }

    if (isset($envNames[$answer])) {
        $envName = $envNames[$answer];
    }
} else {
    $envName = $params['env'];
}

if (!in_array($envName, $envNames)) {
    $envsList = implode(', ', $envNames);
    echo "\n  $envName is not a valid environment. Try one of the following: $envsList. \n";
    exit(2);
}

$env = $envs[$envName];

if (empty($params['env'])) {
    echo "\n  Initialize the application under '{$envNames[$answer]}' environment? [yes|no] ";
    $answer = trim(fgets(STDIN));
    if (strncasecmp($answer, 'y', 1)) {
        echo "\n  Quit initialization.\n";
        exit(0);
    }
} else {
    echo "\n  Initialize the application under '{$envName}' environment.";
}

echo "\n  Start initialization ...\n\n";

$cookieValidationKeys = [];
if (empty($params['full'])) {
    echo "\n  Store cookie validation keys.\n\n";
    foreach ($env['setCookieValidationKey'] as $file) {
        if (!is_file("$root/$file")) {
            continue;
        }
        $config = file_get_contents("$root/$file");
        if (preg_match('/(("|\')cookieValidationKey\2\s*=>\s*)("|\')([^"\']+)\3/', $config, $m)) {
            $cookieValidationKeys[$file] = $m[4];
        } else {
            $cookieValidationKeys[$file] = '';
        }
    }
}

if (!empty($env['cleanUp'])) {
    cleanUp($root, $env['cleanUp']);
}

$files = getFileList("$root/environments/{$env['path']}");
if (isset($env['skipFiles'])) {
    $skipFiles = $env['skipFiles'];
    array_walk($skipFiles, function(&$value) use($env, $root) {
        $value = "$root/$value";
    });
    $files = array_diff($files, array_intersect_key($env['skipFiles'], array_filter($skipFiles, 'file_exists')));
}
$all = (isset($params['overwrite']) && !strncasecmp($params['overwrite'], 'a', 1)) ? true : false;
echo "\n  copyFiles\n";
foreach ($files as $file) {
    if (!copyFile($root, "environments/{$env['path']}/$file", $file, $all, $params)) {
        break;
    }
}

if (empty($params['full'])) {
    setCookieValidationKey($root, $env['setCookieValidationKey'], $cookieValidationKeys);
} else {
    $callbacks = ['setCookieValidationKey', 'setGroup', 'setWritable', 'setExecutable', 'createSymlink'];
    foreach ($callbacks as $callback) {
        if (!empty($env[$callback])) {
            $callback($root, $env[$callback]);
        }
    }
}

echo "\n  ... initialization completed.\n\n";

function getFileList($root, $basePath = '')
{
    $files = [];
    $handle = opendir($root);
    while (($path = readdir($handle)) !== false) {
        if ($path === '.git' || $path === '.svn' || $path === '.' || $path === '..') {
            continue;
        }
        $fullPath = "$root/$path";
        $relativePath = $basePath === '' ? $path : "$basePath/$path";
        if (is_dir($fullPath)) {
            $files = array_merge($files, getFileList($fullPath, $relativePath));
        } else {
            $files[] = $relativePath;
        }
    }
    closedir($handle);
    return $files;
}

function copyFile($root, $source, $target, &$all, $params)
{
    if (!is_file($root . '/' . $source)) {
        echo "       skip $target ($source not exist)\n";
        return true;
    }
    if (is_file($root . '/' . $target)) {
        if (file_get_contents($root . '/' . $source) === file_get_contents($root . '/' . $target)) {
            echo "    unchanged $target\n";
            return true;
        }
        if ($all) {
            echo "    overwrite $target\n";
        } else {
            echo "      exist $target\n";
            echo "            ...overwrite? [Yes|No|All|Quit] ";


            $answer = !empty($params['overwrite']) ? $params['overwrite'] : trim(fgets(STDIN));
            if (!strncasecmp($answer, 'q', 1)) {
                return false;
            } else {
                if (!strncasecmp($answer, 'y', 1)) {
                    echo "    overwrite $target\n";
                } else {
                    if (!strncasecmp($answer, 'a', 1)) {
                        echo "    overwrite $target\n";
                        $all = true;
                    } else {
                        echo "       skip $target\n";
                        return true;
                    }
                }
            }
        }
        file_put_contents($root . '/' . $target, file_get_contents($root . '/' . $source));
        return true;
    }
    echo "   generate $target\n";
    @mkdir(dirname($root . '/' . $target), 0777, true);
    file_put_contents($root . '/' . $target, file_get_contents($root . '/' . $source));
    return true;
}

function getParams()
{
    $rawParams = [];
    if (isset($_SERVER['argv'])) {
        $rawParams = $_SERVER['argv'];
        array_shift($rawParams);
    }

    $params = [];
    foreach ($rawParams as $param) {
        if (preg_match('/^--(\w+)(=(.*))?$/', $param, $matches)) {
            $name = $matches[1];
            $params[$name] = isset($matches[3]) ? $matches[3] : true;
        } else {
            $params[] = $param;
        }
    }
    return $params;
}

function setGroup($root, $group)
{
    if (empty($group)) {
        return;
    }
    echo "\n  setGroup\n";
    echo "    chgrp -R --silent $group $root\n";
    @system('chgrp -R --silent ' . escapeshellarg($group) . ' ' . escapeshellarg($root));
}

function setWritable($root, $conf)
{
    echo "\n  setWritable\n";
    foreach ($conf['dirs'] as $writable) {
        if (is_dir("$root/$writable")) {
            printf("    chmod 0%o %s\n", $conf['mode'], $writable);
            @chmod("$root/$writable", $conf['mode']);
        } else {
            echo "\n  Error. Directory $writable does not exist. \n";
        }
    }
}

function setExecutable($root, $conf)
{
    echo "\n  setExecutable\n";
    foreach ($conf['files'] as $executable) {
        printf("    chmod 0%o %s\n", $conf['mode'], $executable);
        @chmod("$root/$executable", $conf['mode']);
    }
}

function setCookieValidationKey($root, $paths, $keys = null)
{
    echo "\n  setCookieValidationKey\n";
    foreach ($paths as $file) {
        echo "    generate cookie validation key in $file\n";
        $fullPath = $root . '/' . $file;
        $length = 32;
        $bytes = openssl_random_pseudo_bytes($length);
        if (isset($keys[$file])) {
            $key = $keys[$file];
        } else {
            $key = strtr(substr(base64_encode($bytes), 0, $length), '+/=', '_-.');
        }
        $content = preg_replace('/(("|\')cookieValidationKey("|\')\s*=>\s*)(""|\'\')/', "\\1'$key'", file_get_contents($fullPath));
        file_put_contents($fullPath, $content);
    }
}

function createSymlink($root, $links)
{
    echo "\n  createSymlink\n";
    foreach ($links as $link => $target) {
        /* if target is not absolute, add root path */
        if (strncasecmp($target, '/', 1)) {
            $target = "{$root}/{$target}";
        }
        /* add root path to link */
        $link = "{$root}/{$link}";

        echo "    symlink {$link} -> {$target}\n";

        /* check if target exists */
        if (!file_exists($target)) {
            echo "      \e[1;31mError:\033[0m target \e[1;31m{$target}\e[0m does not exist.\n";
            return false;
        }

        /* check if link exists */
        if (file_exists($link) && !is_link($link)) {
            echo "      \e[1;31mError:\033[0m dir/file \e[1;31m{$link}\e[0m already exists.\n";
            return false;
        }

        /* create links */
        system('ln -sf ' . escapeshellarg($target) . ' ' . escapeshellarg($link));
    }
    return true;
}

function cleanUp($root, $paths)
{
    echo "\n  cleanUp\n";
    foreach ($paths as $target) {
        if (file_exists("$root/$target")) {
            echo "    remove file/dir $root/$target\n";
            system('rm -rf ' . escapeshellarg("$root/$target"));
        }
    }
}
