<?php

class m161005_091725_6816_partner_sites extends CDbMigration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable(
            'partner_site', [
            'id'         => 'pk',
            'host'       => 'string',
            'ident_id'   => 'integer',
            'comment'    => 'text',
            'created_at' => 'datetime',
            'updated_at' => 'datetime'
        ], $tableOptions
        );
        $this->createTable(
            'partner_site_url', [
            'id'         => 'pk',
            'site_id'    => 'integer',
            'url'        => 'string',
            'comment'    => 'string',
            'created_at' => 'datetime',
            'updated_at' => 'datetime'
        ], $tableOptions
        );
        $this->addForeignKey(
            'partner_site_url[site_id]partner_site[id]', 'partner_site_url', 'site_id', 'partner_site', 'id', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('partner_site_url[site_id]partner_site[id]', 'partner_site_url');
        $this->dropTable('partner_site');
        $this->dropTable('partner_site_url');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}