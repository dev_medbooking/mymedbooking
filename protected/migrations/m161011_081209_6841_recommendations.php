<?php

class m161011_081209_6841_recommendations extends CDbMigration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable(
            'recommendation', [
            'id'         => 'pk',
            'message'    => 'text',
            'title'      => 'string',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
            'ident_id'   => 'integer'
        ], $tableOptions
        );
        $this->createTable(
            'recommendation_read', [
            'ident_id'          => 'integer',
            'recommendation_id' => 'integer'
        ], $tableOptions
        );
        $this->addPrimaryKey('recommendation_read_pk', 'recommendation_read', 'ident_id,recommendation_id');
        $this->addForeignKey(
            'recommendation_read[recommendation_id]recommendation[id]', 'recommendation_read', 'recommendation_id',
            'recommendation', 'id', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('recommendation_read[recommendation_id]recommendation[id]', 'recommendation_read');
        $this->dropTable('recommendation');
        $this->dropTable('recommendation_read');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}