<?php
/* @var $this MissedCallsCallbackController */
/* @var $model MissedCallsCallback */
/*
$this->breadcrumbs=array(
	'Missed Calls Callbacks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MissedCallsCallback', 'url'=>array('index')),
	array('label'=>'Create MissedCallsCallback', 'url'=>array('create')),
	array('label'=>'Update MissedCallsCallback', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MissedCallsCallback', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MissedCallsCallback', 'url'=>array('admin')),
);
*/
?>

<h1>View MissedCallsCallback #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'datetime',
		'ext',
		'callerid',
		'project',
	),
)); ?>
