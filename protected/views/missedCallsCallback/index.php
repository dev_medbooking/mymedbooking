<?php
/* @var $this MissedCallsCallbackController */
/* @var $dataProvider CActiveDataProvider */
/*
$this->breadcrumbs=array(
	'Missed Calls Callbacks',
);
$this->menu=array(
	array('label'=>'Create MissedCallsCallback', 'url'=>array('create')),
	array('label'=>'Manage MissedCallsCallback', 'url'=>array('admin')),
);
*/
?>

<h1>Missed Calls Callbacks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
