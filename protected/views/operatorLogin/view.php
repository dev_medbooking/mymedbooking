<?php
/* @var $this OperatorLoginController */
/* @var $model OperatorLogin */
/*
$this->breadcrumbs=array(
	'Operator Logins'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OperatorLogin', 'url'=>array('index')),
	array('label'=>'Create OperatorLogin', 'url'=>array('create')),
	array('label'=>'Update OperatorLogin', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OperatorLogin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OperatorLogin', 'url'=>array('admin')),
);
*/
?>

<h1>View OperatorLogin #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		//'uid',
		'login',
		//'date',
		'datetime',
	),
)); ?>
