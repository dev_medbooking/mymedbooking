<?php
/* @var $this OperatorLoginController */
/* @var $model OperatorLogin */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--<div class="row">
		<?//php echo $form->label($model,'id'); ?>
		<?//php echo $form->textField($model,'id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?//php echo $form->label($model,'uid'); ?>
		<?//php echo $form->textField($model,'uid'); ?>
	</div>
	-->
	<div class="row">
		<?php echo $form->label($model,'nameperson'); ?>
		<?php echo $form->textField($model,'nameperson',array('size'=>30,'maxlength'=>124)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'login'); ?>
		<?php echo $form->textField($model,'login',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?//php echo $form->textField($model,'date'); ?>
		<?
		$form->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'OperatorLogin[date]',
			'value'=>!empty($_REQUEST['date']) ? $_REQUEST['date'] : date('Y-m-d'),
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'dateFormat' => 'yy-mm-dd'
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;',
				'id' => 'OperatorLogin_date'

			),
		));
		?>
	</div>
<!--
	<div class="row">
		<?//php echo $form->label($model,'datetime'); ?>
		<?//php echo $form->textField($model,'datetime'); ?>
	</div>
-->
	<div class="row buttons">
		<?php echo CHtml::submitButton('Поиск'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->