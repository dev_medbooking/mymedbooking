<?php
/* @var $this OperatorLoginController */
/* @var $model OperatorLogin */
/*
$this->breadcrumbs=array(
	'Operator Logins'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List OperatorLogin', 'url'=>array('index')),
	array('label'=>'Create OperatorLogin', 'url'=>array('create')),
);
*/
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#operator-login-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Залогинивание Операторов</h1>

<p>Вы можете дополнительно ввести оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>).
</p>

<?//php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'operator-login-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		//'uid',
		'nameperson',
		'login',
		'datetime',
		'date',
		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
)); ?>
