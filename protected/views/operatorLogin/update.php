<?php
/* @var $this OperatorLoginController */
/* @var $model OperatorLogin */

$this->breadcrumbs=array(
	'Operator Logins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OperatorLogin', 'url'=>array('index')),
	array('label'=>'Create OperatorLogin', 'url'=>array('create')),
	array('label'=>'View OperatorLogin', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OperatorLogin', 'url'=>array('admin')),
);
?>

<h1>Update OperatorLogin <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>