<?php
/* @var $this OperatorLoginController */
/* @var $dataProvider CActiveDataProvider */
/*
$this->breadcrumbs=array(
	'Operator Logins',
);

$this->menu=array(
	array('label'=>'Create OperatorLogin', 'url'=>array('create')),
	array('label'=>'Manage OperatorLogin', 'url'=>array('admin')),
);
*/
?>

<h1>Operator Logins</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
