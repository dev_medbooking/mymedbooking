<?php
/* @var $this OperatorLoginController */
/* @var $model OperatorLogin */

$this->breadcrumbs=array(
	'Operator Logins'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OperatorLogin', 'url'=>array('index')),
	array('label'=>'Manage OperatorLogin', 'url'=>array('admin')),
);
?>

<h1>Create OperatorLogin</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>