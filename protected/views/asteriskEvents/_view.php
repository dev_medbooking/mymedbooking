<?php
/* @var $this AsteriskEventsController */
/* @var $data AsteriskEvents */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datetime')); ?>:</b>
	<?php echo CHtml::encode($data->datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('event')); ?>:</b>
	<?php echo CHtml::encode($data->event); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('channel')); ?>:</b>
	<?php echo CHtml::encode($data->channel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('channel_state')); ?>:</b>
	<?php echo CHtml::encode($data->channel_state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('channel_state_desc')); ?>:</b>
	<?php echo CHtml::encode($data->channel_state_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('caller_id_num')); ?>:</b>
	<?php echo CHtml::encode($data->caller_id_num); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('caller_id_name')); ?>:</b>
	<?php echo CHtml::encode($data->caller_id_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('account_code')); ?>:</b>
	<?php echo CHtml::encode($data->account_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exten')); ?>:</b>
	<?php echo CHtml::encode($data->exten); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('context')); ?>:</b>
	<?php echo CHtml::encode($data->context); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uniqueid')); ?>:</b>
	<?php echo CHtml::encode($data->uniqueid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('queue')); ?>:</b>
	<?php echo CHtml::encode($data->queue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('position')); ?>:</b>
	<?php echo CHtml::encode($data->position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('original_position')); ?>:</b>
	<?php echo CHtml::encode($data->original_position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hold_time')); ?>:</b>
	<?php echo CHtml::encode($data->hold_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('count')); ?>:</b>
	<?php echo CHtml::encode($data->count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('connected_line_num')); ?>:</b>
	<?php echo CHtml::encode($data->connected_line_num); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('connected_line_name')); ?>:</b>
	<?php echo CHtml::encode($data->connected_line_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cause')); ?>:</b>
	<?php echo CHtml::encode($data->cause); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cause_txt')); ?>:</b>
	<?php echo CHtml::encode($data->cause_txt); ?>
	<br />

	*/ ?>

</div>