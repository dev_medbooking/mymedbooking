<?php
/* @var $this AsteriskEventsController */
/* @var $model AsteriskEvents */

$this->breadcrumbs=array(
	'Asterisk Events'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AsteriskEvents', 'url'=>array('index')),
	array('label'=>'Create AsteriskEvents', 'url'=>array('create')),
	array('label'=>'View AsteriskEvents', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AsteriskEvents', 'url'=>array('admin')),
);
?>

<h1>Update AsteriskEvents <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>