<?php
/* @var $this AsteriskEventsController */
/* @var $model AsteriskEvents */

$this->breadcrumbs=array(
	'Asterisk Events'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AsteriskEvents', 'url'=>array('index')),
	array('label'=>'Create AsteriskEvents', 'url'=>array('create')),
	array('label'=>'Update AsteriskEvents', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AsteriskEvents', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AsteriskEvents', 'url'=>array('admin')),
);
?>

<h1>View AsteriskEvents #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'datetime',
		'event',
		'channel',
		'channel_state',
		'channel_state_desc',
		'caller_id_num',
		'caller_id_name',
		'account_code',
		'exten',
		'context',
		'uniqueid',
		'queue',
		'position',
		'original_position',
		'hold_time',
		'status',
		'count',
		'connected_line_num',
		'connected_line_name',
		'cause',
		'cause_txt',
	),
)); ?>
