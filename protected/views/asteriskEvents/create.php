<?php
/* @var $this AsteriskEventsController */
/* @var $model AsteriskEvents */

$this->breadcrumbs=array(
	'Asterisk Events'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AsteriskEvents', 'url'=>array('index')),
	array('label'=>'Manage AsteriskEvents', 'url'=>array('admin')),
);
?>

<h1>Create AsteriskEvents</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>