<?php
/* @var $this AsteriskEventsController */
/* @var $model AsteriskEvents */

$this->breadcrumbs=array(
	'Asterisk Events'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AsteriskEvents', 'url'=>array('index')),
	array('label'=>'Create AsteriskEvents', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#asterisk-events-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Asterisk Events</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'asterisk-events-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'datetime',
		'event',
		'channel',
		'channel_state',
		'channel_state_desc',
		/*
		'caller_id_num',
		'caller_id_name',
		'account_code',
		'exten',
		'context',
		'uniqueid',
		'queue',
		'position',
		'original_position',
		'hold_time',
		'status',
		'count',
		'connected_line_num',
		'connected_line_name',
		'cause',
		'cause_txt',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
