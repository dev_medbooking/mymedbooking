<?php
/* @var $this AsteriskEventsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Asterisk Events',
);

$this->menu=array(
	array('label'=>'Create AsteriskEvents', 'url'=>array('create')),
	array('label'=>'Manage AsteriskEvents', 'url'=>array('admin')),
);
?>

<h1>Asterisk Events</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
