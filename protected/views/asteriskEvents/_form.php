<?php
/* @var $this AsteriskEventsController */
/* @var $model AsteriskEvents */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'asterisk-events-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'datetime'); ?>
		<?php echo $form->textField($model,'datetime',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'event'); ?>
		<?php echo $form->textField($model,'event',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'event'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channel'); ?>
		<?php echo $form->textField($model,'channel',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'channel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channel_state'); ?>
		<?php echo $form->textField($model,'channel_state'); ?>
		<?php echo $form->error($model,'channel_state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channel_state_desc'); ?>
		<?php echo $form->textField($model,'channel_state_desc',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'channel_state_desc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'caller_id_num'); ?>
		<?php echo $form->textField($model,'caller_id_num',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'caller_id_num'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'caller_id_name'); ?>
		<?php echo $form->textField($model,'caller_id_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'caller_id_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'account_code'); ?>
		<?php echo $form->textField($model,'account_code',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'account_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exten'); ?>
		<?php echo $form->textField($model,'exten',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'exten'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'context'); ?>
		<?php echo $form->textField($model,'context',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'context'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uniqueid'); ?>
		<?php echo $form->textField($model,'uniqueid',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'uniqueid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'queue'); ?>
		<?php echo $form->textField($model,'queue',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'queue'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php echo $form->textField($model,'position'); ?>
		<?php echo $form->error($model,'position'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'original_position'); ?>
		<?php echo $form->textField($model,'original_position'); ?>
		<?php echo $form->error($model,'original_position'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hold_time'); ?>
		<?php echo $form->textField($model,'hold_time',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'hold_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'count'); ?>
		<?php echo $form->textField($model,'count'); ?>
		<?php echo $form->error($model,'count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'connected_line_num'); ?>
		<?php echo $form->textField($model,'connected_line_num',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'connected_line_num'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'connected_line_name'); ?>
		<?php echo $form->textField($model,'connected_line_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'connected_line_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cause'); ?>
		<?php echo $form->textField($model,'cause'); ?>
		<?php echo $form->error($model,'cause'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cause_txt'); ?>
		<?php echo $form->textField($model,'cause_txt',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'cause_txt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->