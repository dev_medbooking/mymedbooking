<?php
/* @var $this SessionsTrackingStatisticController */
/* @var $data SessionsTrackingStatistic */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('host')); ?>:</b>
	<?php echo CHtml::encode($data->host); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('current_url')); ?>:</b>
	<?php echo CHtml::encode($data->current_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('referer')); ?>:</b>
	<?php echo CHtml::encode($data->referer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ip')); ?>:</b>
	<?php echo CHtml::encode($data->ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('server_addr')); ?>:</b>
	<?php echo CHtml::encode($data->server_addr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remote_addr')); ?>:</b>
	<?php echo CHtml::encode($data->remote_addr); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('remote_host')); ?>:</b>
	<?php echo CHtml::encode($data->remote_host); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sessionid')); ?>:</b>
	<?php echo CHtml::encode($data->sessionid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('get')); ?>:</b>
	<?php echo CHtml::encode($data->get); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datetime')); ?>:</b>
	<?php echo CHtml::encode($data->datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tracking_phone')); ?>:</b>
	<?php echo CHtml::encode($data->tracking_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('utm_medium')); ?>:</b>
	<?php echo CHtml::encode($data->utm_medium); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('utm_source')); ?>:</b>
	<?php echo CHtml::encode($data->utm_source); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('utm_campaign')); ?>:</b>
	<?php echo CHtml::encode($data->utm_campaign); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('utm_term')); ?>:</b>
	<?php echo CHtml::encode($data->utm_term); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('utm_content')); ?>:</b>
	<?php echo CHtml::encode($data->utm_content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('k50id')); ?>:</b>
	<?php echo CHtml::encode($data->k50id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yclid')); ?>:</b>
	<?php echo CHtml::encode($data->yclid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('api_record_id')); ?>:</b>
	<?php echo CHtml::encode($data->api_record_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sessions_tracking_id')); ?>:</b>
	<?php echo CHtml::encode($data->sessions_tracking_id); ?>
	<br />

	*/ ?>

</div>