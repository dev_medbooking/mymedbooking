<?php
/* @var $this SessionsTrackingStatisticController */
/* @var $model SessionsTrackingStatistic */
/*
$this->breadcrumbs=array(
	'Sessions Tracking Statistics'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SessionsTrackingStatistic', 'url'=>array('index')),
	array('label'=>'Create SessionsTrackingStatistic', 'url'=>array('create')),
	array('label'=>'View SessionsTrackingStatistic', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SessionsTrackingStatistic', 'url'=>array('admin')),
);
*/
?>

<h1>Update SessionsTrackingStatistic <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>