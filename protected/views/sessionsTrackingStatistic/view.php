<?php
/* @var $this SessionsTrackingStatisticController */
/* @var $model SessionsTrackingStatistic */
/*
$this->breadcrumbs=array(
	'Sessions Tracking Statistics'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SessionsTrackingStatistic', 'url'=>array('index')),
	array('label'=>'Create SessionsTrackingStatistic', 'url'=>array('create')),
	array('label'=>'Update SessionsTrackingStatistic', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SessionsTrackingStatistic', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SessionsTrackingStatistic', 'url'=>array('admin')),
);
*/
?>

<h1>View SessionsTrackingStatistic #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'host',
		'current_url',
		'referer',
		'ip',
		'server_addr',
		'remote_addr',
		'remote_host',
		'sessionid',
		'get',
		'datetime',
		'tracking_phone',
		'utm_medium',
		'utm_source',
		'utm_campaign',
		'utm_term',
		'utm_content',
		'k50id',
		'yclid',
		'api_record_id',
		'sessions_tracking_id',
	),
)); ?>
