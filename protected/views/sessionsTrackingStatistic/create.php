<?php
/* @var $this SessionsTrackingStatisticController */
/* @var $model SessionsTrackingStatistic */
/*
$this->breadcrumbs=array(
	'Sessions Tracking Statistics'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SessionsTrackingStatistic', 'url'=>array('index')),
	array('label'=>'Manage SessionsTrackingStatistic', 'url'=>array('admin')),
);
*/
?>

<h1>Create SessionsTrackingStatistic</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>