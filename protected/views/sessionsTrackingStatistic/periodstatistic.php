<?
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 'On');
?>
<?if (isset($data) || isset($model)): ?>
    <?php if(Yii::app()->user->checkAccess('Administrator')): ?>
        <?php

        //echo $form = CHtml::beginForm(CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId())),'get');
        echo $form = CHtml::beginForm(CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/periodStatisticExcel')),'get');
        echo "Проект: ";
        //echo $form->dropDownList($model,'project',array(''=>'','medbooking'=>'medbooking','testpuls' => 'testpuls'));
        echo CHtml::dropDownList('project', '',
            array(''=>'','medbooking'=>'medbooking','testpuls' => 'testpuls'));
        echo "Период: ";
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'name'=>'dateStart',
            'value'=>!empty($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : date('Y-m-d'),
            // additional javascript options for the date picker plugin
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat' => 'yy-mm-dd'
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;',

            ),
        ));
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'name'=>'dateEnd',
            'value'=>!empty($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : date('Y-m-d'),
            // additional javascript options for the date picker plugin
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat' => 'yy-mm-dd'
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;',
                'value'=>date('Y-m-d'),
            ),
        ));
        echo CHtml::submitButton();
        /*echo CHtml::link('Скачать', [CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/periodStatisticExcel')),
            'dateFrom' => !empty($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : date('Y-m-d'),
            'dateTo'   => !empty($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : date('Y-m-d'),
        ]);*/
        echo CHtml::endForm();
        ?>
    <?php endif;?>
<?endif?>
