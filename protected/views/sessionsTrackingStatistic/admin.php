<?php
/* @var $this SessionsTrackingStatisticController */
/* @var $model SessionsTrackingStatistic */
/*
$this->breadcrumbs=array(
	'Sessions Tracking Statistics'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SessionsTrackingStatistic', 'url'=>array('index')),
	array('label'=>'Create SessionsTrackingStatistic', 'url'=>array('create')),
);
*/
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sessions-tracking-statistic-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sessions Tracking Statistics</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sessions-tracking-statistic-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		//'host',
		'current_url',
		'referer',
		//'ip',
		//'server_addr',
		//'remote_addr',
		//'remote_host',
		'sessionid',
		//'get',
		//'datetime',
		'tracking_phone',
		'utm_medium',
		'utm_source',
		'utm_campaign',
		'utm_term',
		'utm_content',
		'k50id',
		'yclid',
		'api_record_id',
		'sessions_tracking_id',

		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
)); ?>
