<?php
/* @var $this SessionsTrackingStatisticController */
/* @var $dataProvider CActiveDataProvider */
/*
$this->breadcrumbs=array(
	'Sessions Tracking Statistics',
);

$this->menu=array(
	array('label'=>'Create SessionsTrackingStatistic', 'url'=>array('create')),
	array('label'=>'Manage SessionsTrackingStatistic', 'url'=>array('admin')),
);
*/
?>

<h1>Sessions Tracking Statistics</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
