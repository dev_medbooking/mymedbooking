<?php
/* @var $this SessionsTrackingController */
/* @var $model SessionsTracking */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sessions-tracking-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'host'); ?>
		<?php echo $form->textField($model,'host',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'host'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'current_url'); ?>
		<?php echo $form->textField($model,'current_url',array('size'=>60,'maxlength'=>512)); ?>
		<?php echo $form->error($model,'current_url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'referer'); ?>
		<?php echo $form->textField($model,'referer',array('size'=>60,'maxlength'=>512)); ?>
		<?php echo $form->error($model,'referer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ip'); ?>
		<?php echo $form->textField($model,'ip',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'ip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'server_addr'); ?>
		<?php echo $form->textField($model,'server_addr',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'server_addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remote_addr'); ?>
		<?php echo $form->textField($model,'remote_addr',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'remote_addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remote_host'); ?>
		<?php echo $form->textField($model,'remote_host',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'remote_host'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sessionid'); ?>
		<?php echo $form->textField($model,'sessionid',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'sessionid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'get'); ?>
		<?php echo $form->textArea($model,'get',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'get'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datetime'); ?>
		<?php echo $form->textField($model,'datetime',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tracking_phone'); ?>
		<?php echo $form->textField($model,'tracking_phone',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'tracking_phone'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->