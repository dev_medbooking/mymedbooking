<?php
/* @var $this SessionsTrackingController */
/* @var $model SessionsTracking */

$this->breadcrumbs=array(
	'Sessions Trackings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SessionsTracking', 'url'=>array('index')),
	array('label'=>'Create SessionsTracking', 'url'=>array('create')),
	array('label'=>'View SessionsTracking', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SessionsTracking', 'url'=>array('admin')),
);
?>

<h1>Update SessionsTracking <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>