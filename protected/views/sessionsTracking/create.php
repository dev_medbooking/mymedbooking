<?php
/* @var $this SessionsTrackingController */
/* @var $model SessionsTracking */

$this->breadcrumbs=array(
	'Sessions Trackings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SessionsTracking', 'url'=>array('index')),
	array('label'=>'Manage SessionsTracking', 'url'=>array('admin')),
);
?>

<h1>Create SessionsTracking</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>