<?php
/* @var $this SessionsTrackingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sessions Trackings',
);

$this->menu=array(
	array('label'=>'Create SessionsTracking', 'url'=>array('create')),
	array('label'=>'Manage SessionsTracking', 'url'=>array('admin')),
);
?>

<h1>Sessions Trackings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
