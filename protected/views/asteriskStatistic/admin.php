<?php
/* @var $this AsteriskStatisticController */
/* @var $model AsteriskStatistic */

$this->breadcrumbs=array(
	'Asterisk Statistics'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AsteriskStatistic', 'url'=>array('index')),
	array('label'=>'Create AsteriskStatistic', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#asterisk-statistic-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Asterisk Statistics</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'asterisk-statistic-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'eventcontent',
		'clientstatus',
		'datetime',
		'event',
		'timestamp',
		/*
		'channel',
		'channelstate',
		'channelstatedesc',
		'calleridnum',
		'calleridname',
		'accountcode',
		'exten',
		'context',
		'uniqueid',
		'queue',
		'position',
		'originalposition',
		'holdtime',
		'status',
		'count',
		'connectedlinenum',
		'connectedlinename',
		'cause',
		'causetxt',
		'bridgestate',
		'bridgetype',
		'channel1',
		'channel2',
		'uniqueid1',
		'uniqueid2',
		'callerid1',
		'callerid2',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
