<?php
/* @var $this AsteriskStatisticController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Asterisk Statistics',
);

$this->menu=array(
	array('label'=>'Create AsteriskStatistic', 'url'=>array('create')),
	array('label'=>'Manage AsteriskStatistic', 'url'=>array('admin')),
);
?>

<h1>Asterisk Statistics</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
