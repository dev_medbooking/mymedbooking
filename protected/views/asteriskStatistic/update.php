<?php
/* @var $this AsteriskStatisticController */
/* @var $model AsteriskStatistic */

$this->breadcrumbs=array(
	'Asterisk Statistics'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AsteriskStatistic', 'url'=>array('index')),
	array('label'=>'Create AsteriskStatistic', 'url'=>array('create')),
	array('label'=>'View AsteriskStatistic', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AsteriskStatistic', 'url'=>array('admin')),
);
?>

<h1>Update AsteriskStatistic <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>