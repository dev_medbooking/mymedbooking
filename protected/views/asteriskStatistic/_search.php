<?php
/* @var $this AsteriskStatisticController */
/* @var $model AsteriskStatistic */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'eventcontent'); ?>
		<?php echo $form->textArea($model,'eventcontent',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'clientstatus'); ?>
		<?php echo $form->textField($model,'clientstatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datetime'); ?>
		<?php echo $form->textField($model,'datetime',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'event'); ?>
		<?php echo $form->textField($model,'event',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'timestamp'); ?>
		<?php echo $form->textField($model,'timestamp',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'channel'); ?>
		<?php echo $form->textField($model,'channel',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'channelstate'); ?>
		<?php echo $form->textField($model,'channelstate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'channelstatedesc'); ?>
		<?php echo $form->textField($model,'channelstatedesc',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'calleridnum'); ?>
		<?php echo $form->textField($model,'calleridnum',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'calleridname'); ?>
		<?php echo $form->textField($model,'calleridname',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accountcode'); ?>
		<?php echo $form->textField($model,'accountcode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'exten'); ?>
		<?php echo $form->textField($model,'exten',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'context'); ?>
		<?php echo $form->textField($model,'context',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uniqueid'); ?>
		<?php echo $form->textField($model,'uniqueid',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'queue'); ?>
		<?php echo $form->textField($model,'queue',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'position'); ?>
		<?php echo $form->textField($model,'position'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'originalposition'); ?>
		<?php echo $form->textField($model,'originalposition'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'holdtime'); ?>
		<?php echo $form->textField($model,'holdtime',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'count'); ?>
		<?php echo $form->textField($model,'count'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'connectedlinenum'); ?>
		<?php echo $form->textField($model,'connectedlinenum',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'connectedlinename'); ?>
		<?php echo $form->textField($model,'connectedlinename',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cause'); ?>
		<?php echo $form->textField($model,'cause'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'causetxt'); ?>
		<?php echo $form->textField($model,'causetxt',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bridgestate'); ?>
		<?php echo $form->textField($model,'bridgestate',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bridgetype'); ?>
		<?php echo $form->textField($model,'bridgetype',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'channel1'); ?>
		<?php echo $form->textField($model,'channel1',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'channel2'); ?>
		<?php echo $form->textField($model,'channel2',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uniqueid1'); ?>
		<?php echo $form->textField($model,'uniqueid1',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uniqueid2'); ?>
		<?php echo $form->textField($model,'uniqueid2',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'callerid1'); ?>
		<?php echo $form->textField($model,'callerid1',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'callerid2'); ?>
		<?php echo $form->textField($model,'callerid2',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->