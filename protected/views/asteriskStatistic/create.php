<?php
/* @var $this AsteriskStatisticController */
/* @var $model AsteriskStatistic */

$this->breadcrumbs=array(
	'Asterisk Statistics'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AsteriskStatistic', 'url'=>array('index')),
	array('label'=>'Manage AsteriskStatistic', 'url'=>array('admin')),
);
?>

<h1>Create AsteriskStatistic</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>