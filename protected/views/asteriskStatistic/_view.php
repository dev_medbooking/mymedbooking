<?php
/* @var $this AsteriskStatisticController */
/* @var $data AsteriskStatistic */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eventcontent')); ?>:</b>
	<?php echo CHtml::encode($data->eventcontent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('clientstatus')); ?>:</b>
	<?php echo CHtml::encode($data->clientstatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datetime')); ?>:</b>
	<?php echo CHtml::encode($data->datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('event')); ?>:</b>
	<?php echo CHtml::encode($data->event); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timestamp')); ?>:</b>
	<?php echo CHtml::encode($data->timestamp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('channel')); ?>:</b>
	<?php echo CHtml::encode($data->channel); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('channelstate')); ?>:</b>
	<?php echo CHtml::encode($data->channelstate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('channelstatedesc')); ?>:</b>
	<?php echo CHtml::encode($data->channelstatedesc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('calleridnum')); ?>:</b>
	<?php echo CHtml::encode($data->calleridnum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('calleridname')); ?>:</b>
	<?php echo CHtml::encode($data->calleridname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accountcode')); ?>:</b>
	<?php echo CHtml::encode($data->accountcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exten')); ?>:</b>
	<?php echo CHtml::encode($data->exten); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('context')); ?>:</b>
	<?php echo CHtml::encode($data->context); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uniqueid')); ?>:</b>
	<?php echo CHtml::encode($data->uniqueid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('queue')); ?>:</b>
	<?php echo CHtml::encode($data->queue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('position')); ?>:</b>
	<?php echo CHtml::encode($data->position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('originalposition')); ?>:</b>
	<?php echo CHtml::encode($data->originalposition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('holdtime')); ?>:</b>
	<?php echo CHtml::encode($data->holdtime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('count')); ?>:</b>
	<?php echo CHtml::encode($data->count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('connectedlinenum')); ?>:</b>
	<?php echo CHtml::encode($data->connectedlinenum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('connectedlinename')); ?>:</b>
	<?php echo CHtml::encode($data->connectedlinename); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cause')); ?>:</b>
	<?php echo CHtml::encode($data->cause); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('causetxt')); ?>:</b>
	<?php echo CHtml::encode($data->causetxt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bridgestate')); ?>:</b>
	<?php echo CHtml::encode($data->bridgestate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bridgetype')); ?>:</b>
	<?php echo CHtml::encode($data->bridgetype); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('channel1')); ?>:</b>
	<?php echo CHtml::encode($data->channel1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('channel2')); ?>:</b>
	<?php echo CHtml::encode($data->channel2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uniqueid1')); ?>:</b>
	<?php echo CHtml::encode($data->uniqueid1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uniqueid2')); ?>:</b>
	<?php echo CHtml::encode($data->uniqueid2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('callerid1')); ?>:</b>
	<?php echo CHtml::encode($data->callerid1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('callerid2')); ?>:</b>
	<?php echo CHtml::encode($data->callerid2); ?>
	<br />

	*/ ?>

</div>