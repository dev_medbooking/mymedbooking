<?php
/* @var $this AsteriskStatisticController */
/* @var $model AsteriskStatistic */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'asterisk-statistic-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'eventcontent'); ?>
		<?php echo $form->textArea($model,'eventcontent',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'eventcontent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'clientstatus'); ?>
		<?php echo $form->textField($model,'clientstatus'); ?>
		<?php echo $form->error($model,'clientstatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datetime'); ?>
		<?php echo $form->textField($model,'datetime',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'event'); ?>
		<?php echo $form->textField($model,'event',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'event'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'timestamp'); ?>
		<?php echo $form->textField($model,'timestamp',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'timestamp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channel'); ?>
		<?php echo $form->textField($model,'channel',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'channel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channelstate'); ?>
		<?php echo $form->textField($model,'channelstate'); ?>
		<?php echo $form->error($model,'channelstate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channelstatedesc'); ?>
		<?php echo $form->textField($model,'channelstatedesc',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'channelstatedesc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'calleridnum'); ?>
		<?php echo $form->textField($model,'calleridnum',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'calleridnum'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'calleridname'); ?>
		<?php echo $form->textField($model,'calleridname',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'calleridname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accountcode'); ?>
		<?php echo $form->textField($model,'accountcode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'accountcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exten'); ?>
		<?php echo $form->textField($model,'exten',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'exten'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'context'); ?>
		<?php echo $form->textField($model,'context',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'context'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uniqueid'); ?>
		<?php echo $form->textField($model,'uniqueid',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'uniqueid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'queue'); ?>
		<?php echo $form->textField($model,'queue',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'queue'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php echo $form->textField($model,'position'); ?>
		<?php echo $form->error($model,'position'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'originalposition'); ?>
		<?php echo $form->textField($model,'originalposition'); ?>
		<?php echo $form->error($model,'originalposition'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'holdtime'); ?>
		<?php echo $form->textField($model,'holdtime',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'holdtime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'count'); ?>
		<?php echo $form->textField($model,'count'); ?>
		<?php echo $form->error($model,'count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'connectedlinenum'); ?>
		<?php echo $form->textField($model,'connectedlinenum',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'connectedlinenum'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'connectedlinename'); ?>
		<?php echo $form->textField($model,'connectedlinename',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'connectedlinename'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cause'); ?>
		<?php echo $form->textField($model,'cause'); ?>
		<?php echo $form->error($model,'cause'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'causetxt'); ?>
		<?php echo $form->textField($model,'causetxt',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'causetxt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bridgestate'); ?>
		<?php echo $form->textField($model,'bridgestate',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'bridgestate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bridgetype'); ?>
		<?php echo $form->textField($model,'bridgetype',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'bridgetype'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channel1'); ?>
		<?php echo $form->textField($model,'channel1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'channel1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channel2'); ?>
		<?php echo $form->textField($model,'channel2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'channel2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uniqueid1'); ?>
		<?php echo $form->textField($model,'uniqueid1',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'uniqueid1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uniqueid2'); ?>
		<?php echo $form->textField($model,'uniqueid2',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'uniqueid2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'callerid1'); ?>
		<?php echo $form->textField($model,'callerid1',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'callerid1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'callerid2'); ?>
		<?php echo $form->textField($model,'callerid2',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'callerid2'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->