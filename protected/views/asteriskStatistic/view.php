<?php
/* @var $this AsteriskStatisticController */
/* @var $model AsteriskStatistic */

$this->breadcrumbs=array(
	'Asterisk Statistics'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AsteriskStatistic', 'url'=>array('index')),
	array('label'=>'Create AsteriskStatistic', 'url'=>array('create')),
	array('label'=>'Update AsteriskStatistic', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AsteriskStatistic', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AsteriskStatistic', 'url'=>array('admin')),
);
?>

<h1>View AsteriskStatistic #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'eventcontent',
		'clientstatus',
		'datetime',
		'event',
		'timestamp',
		'channel',
		'channelstate',
		'channelstatedesc',
		'calleridnum',
		'calleridname',
		'accountcode',
		'exten',
		'context',
		'uniqueid',
		'queue',
		'position',
		'originalposition',
		'holdtime',
		'status',
		'count',
		'connectedlinenum',
		'connectedlinename',
		'cause',
		'causetxt',
		'bridgestate',
		'bridgetype',
		'channel1',
		'channel2',
		'uniqueid1',
		'uniqueid2',
		'callerid1',
		'callerid2',
	),
)); ?>
