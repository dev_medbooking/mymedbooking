<?php
/* @var $this QueuePauseController */
/* @var $model QueuePause */

$this->breadcrumbs=array(
	'Queue Pauses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List QueuePause', 'url'=>array('index')),
	array('label'=>'Create QueuePause', 'url'=>array('create')),
	array('label'=>'Update QueuePause', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete QueuePause', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage QueuePause', 'url'=>array('admin')),
);
?>

<h1>View QueuePause #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'uid',
		'action',
		'create_time',
		'reason',
		'ext',
	),
)); ?>
