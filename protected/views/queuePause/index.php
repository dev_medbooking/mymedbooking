<?php
/* @var $this QueuePauseController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Queue Pauses',
);

$this->menu=array(
	array('label'=>'Create QueuePause', 'url'=>array('create')),
	array('label'=>'Manage QueuePause', 'url'=>array('admin')),
);
?>

<h1>Queue Pauses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
