<?php
/* @var $this QueuePauseController */
/* @var $model QueuePause */

$this->breadcrumbs=array(
	'Queue Pauses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List QueuePause', 'url'=>array('index')),
	array('label'=>'Create QueuePause', 'url'=>array('create')),
	array('label'=>'View QueuePause', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage QueuePause', 'url'=>array('admin')),
);
?>

<h1>Update QueuePause <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>