<?php
/* @var $this QueuePauseController */
/* @var $model QueuePause */

$this->breadcrumbs=array(
	'Queue Pauses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List QueuePause', 'url'=>array('index')),
	array('label'=>'Manage QueuePause', 'url'=>array('admin')),
);
?>

<h1>Create QueuePause</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>