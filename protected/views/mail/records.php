<table class="records" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
    <thead>
        <tr>
            <th style="border:1px solid #dfdfdf;">Сайт</th>
            <th style="border:1px solid #dfdfdf;">Заявок</th>
            <th style="border:1px solid #dfdfdf;">Записей</th>
            <th style="border:1px solid #dfdfdf;">Запросов обратной связи</th>
        </tr>
    </thead>
    <tbody>
        <?php if(empty($mailBody)||empty($mailBody['domains'])):?>
        <tr>
            <td colspan="4" style="border:1px solid #dfdfdf;">Нет записей</td>
        </tr>
        <?php else:?>
        <?php foreach($mailBody['domains'] as $domain):?>
        <tr>
            <td style="border:1px solid #dfdfdf;"><?=$domain['title']?></td>
            <td style="border:1px solid #dfdfdf;"><?=$domain['records']['new']?></td>
            <td style="border:1px solid #dfdfdf;"><?=$domain['records']['confirmed']?></td>
            <td style="border:1px solid #dfdfdf;"><?=$domain['records']['call']?></td>
        </tr>
        <?php endforeach;?>
        <tr>
            <td style="border:1px solid #dfdfdf; font-weight: bold;">Итого:</td>
            <td style="border:1px solid #dfdfdf; font-weight: bold;"><?=$mailBody['totals']['new']?></td>
            <td style="border:1px solid #dfdfdf; font-weight: bold;"><?=$mailBody['totals']['confirmed']?></td>
            <td style="border:1px solid #dfdfdf; font-weight: bold;"><?=$mailBody['totals']['call']?></td>
        </tr>
        <?php endif;?>
    </tbody>
</table>