<table class="records" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
	<thead>
	<tr>
		<th style="border:1px solid #dfdfdf;">Сайт</th>
		<th style="border:1px solid #dfdfdf;">Заявок</th>
		<th style="border:1px solid #dfdfdf;">Записей</th>
		<th style="border:1px solid #dfdfdf;">Запросов обратной связи</th>
		<th style="border:1px solid #dfdfdf;">Дошедшие</th>
	</tr>
	</thead>
	<tbody>
	<?php if(empty($mailBody)||empty($mailBody['domains'])):?>
		<tr>
			<td colspan="5" style="border:1px solid #dfdfdf;">Нет записей</td>
		</tr>
	<?php else:?>
		<?php foreach($mailBody['domains'] as $domain):?>
			<tr>
				<td style="border:1px solid #dfdfdf;"><?=$domain['title']?></td>
				<td style="border:1px solid #dfdfdf;"><?=$domain['records']['new']?></td>
				<td style="border:1px solid #dfdfdf;"><?=$domain['records']['confirmed']?></td>
				<td style="border:1px solid #dfdfdf;"><?=$domain['records']['call']?></td>
				<td style="border:1px solid #dfdfdf;"><?=$domain['records']['in']?></td>
			</tr>
		<?php endforeach;?>
		<tr>
			<td style="border:1px solid #dfdfdf; font-weight: bold;">Итого:</td>
			<td style="border:1px solid #dfdfdf; font-weight: bold;"><?=$mailBody['totals']['new']?></td>
			<td style="border:1px solid #dfdfdf; font-weight: bold;"><?=$mailBody['totals']['confirmed']?></td>
			<td style="border:1px solid #dfdfdf; font-weight: bold;"><?=$mailBody['totals']['call']?></td>
			<td style="border:1px solid #dfdfdf; font-weight: bold;"><?=$mailBody['totals']['in']?></td>
		</tr>
		<?php /* ?>
        <tr>
            <td style="border:1px solid #dfdfdf; font-weight: bold;">Звонков:</td>
            <td colspan="4" style="border:1px solid #dfdfdf; font-weight: bold;"><?=$oktell?></td>
        </tr>
	<?php */ ?>
	<?php endif;?>
	</tbody>
</table>
<br><br>
<table class="records" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
	<?php if( ! empty($mailBody) AND ! empty($mailBody['money'])) :?>
		<?php foreach($mailBody['money'] as $key => $value) :?>
			<tr>
				<td><?=$key?>: <?=$value;?></td>
			</tr>
		<?php endforeach;?>
	<?php endif;?>
</table>
<br><br>
<?php if( ! empty($mailBody) AND ! empty($mailBody['partner'])) :?>
	<strong><?=$mailBody['partner'];?></strong>
<?php endif;?>

<section id="content" class="content_prices">
	<div class="moneyInfo center">
		<?php if( ! empty($data['detailPartner'])) :?>
			<table style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
				<tr>
					<td>Партнер</td>
					<td>Всего заявок</td>
					<td>Записей</td>
					<td>Сумма</td>
				</tr>
				<?php foreach($data['detailPartner'] as $k => $v) :?>
					<tr>
						<td><?=( ! empty($v['name']) ? $v['name'] : '');?><?=( ! empty($v['site']) ? "(<a href='".$v['site']."' target='_blank'>".$v['site']."</a>)" : '');?></td>
						<td><?=( ! empty($v['all_records']) ? $v['all_records'] : '0');?></td>
						<td><?=( ! empty($v['success_record']) ? $v['success_record'] : '0');?></td>
						<td><?=( ! empty($v['price']) ? $v['price'] : '0');?></td>
					</tr>
				<?php endforeach;?>
			</table>
		<?php endif;?>
	</div>
</section>
