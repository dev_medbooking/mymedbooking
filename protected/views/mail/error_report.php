<?php /** @var string $newPassword */?>
<h3>
    Проблемы при регистрации заявки
</h3>
<br>
<table cellpadding="5" cellspacing="0" border="0">
    <tr >
        <td nowrap>ID</td>
        <td><?php echo $data['id']?></td>
    </tr>
    <tr>
        <td nowrap>Дата/время заявки</td>
        <td><?php echo $data['date']?></td>
    </tr>
    <tr>
        <td nowrap>Телефон</td>
        <td><?php echo $data['phone']?></td>
    </tr>
    <tr>
        <td nowrap>Клиника</td>
        <td><?php echo $data['clinic']?></td>
    </tr>
    <tr>
        <td nowrap>Доктор</td>
        <td><?php echo $data['doctor']?></td>
    </tr>
    <tr>
        <td nowrap>Проблема</td>
        <td><?php echo $data['error']?></td>
    </tr>
    <tr>
        <td nowrap>Комментарий</td>
        <td><?php echo $data['comment']?></td>
    </tr>
</table>
