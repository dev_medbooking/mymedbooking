<?php
/* @var $this OperatorDutyController */
/* @var $dataProvider CActiveDataProvider */
/*
$this->breadcrumbs=array(
	'Operator Duties',
);

$this->menu=array(
	array('label'=>'Create OperatorDuty', 'url'=>array('create')),
	array('label'=>'Manage OperatorDuty', 'url'=>array('admin')),
);
*/
?>

<h1>Operator Duties</h1>
<br><a href="<?php echo Yii::app()->createUrl('/operatorDuty/admin/'); ?>">Список</a>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
