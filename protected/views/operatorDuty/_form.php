<?php
/* @var $this OperatorDutyController */
/* @var $model OperatorDuty */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'operator-duty-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля со <span class="required">*</span> являются обязательными к заполнению.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'extention'); ?>
		<?php echo $form->textField($model,'extention'); ?>
		<?php echo $form->error($model,'extention'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_start'); ?>
		<?//php echo $form->textField($model,'date_start'); ?>
		<?
		$form->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'OperatorDuty[date_start]',
			'language' => 'en',
			'value'=>!empty($model->date_start) ? $model->date_start : date('Y-m-d'),
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'dateFormat' => 'yy-mm-dd'
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;',
				'id' => 'OperatorDuty_date_start'

			),
		));
		?>
		<?php echo $form->error($model,'date_start'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_end'); ?>
		<?//php echo $form->textField($model,'date_end'); ?>
		<?
		$form->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'OperatorDuty[_date_end]',
			'language' => 'en',
			'value'=>!empty($model->date_end) ? $model->date_end : date('Y-m-d'),
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'dateFormat' => 'yy-mm-dd'
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;',
				'id' => 'OperatorDuty_date_end'

			),
		));
		?>
		<?php echo $form->error($model,'date_end'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'queue_name'); ?>
		<?php echo $form->textField($model,'queue_name',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'queue_name'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->