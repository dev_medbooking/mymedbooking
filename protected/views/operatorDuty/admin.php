<?php
/* @var $this OperatorDutyController */
/* @var $model OperatorDuty */
/*
$this->breadcrumbs=array(
	'Operator Duties'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List OperatorDuty', 'url'=>array('index')),
	array('label'=>'Create OperatorDuty', 'url'=>array('create')),
);
*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#operator-duty-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Дежурство операторов</h1>
<br><a href="<?php echo Yii::app()->createUrl('/operatorDuty/create/'); ?>">Добавить оператора в дежурство</a>
<!--
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?//php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?//php $this->renderPartial('_search',array(
//	'model'=>$model,
//)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'operator-duty-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'extention',
		'date_start',
		'date_end',
		'queue_name',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
