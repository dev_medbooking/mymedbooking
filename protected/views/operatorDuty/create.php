<?php

/* @var $this OperatorDutyController */
/* @var $model OperatorDuty */
/*
/*
$this->breadcrumbs=array(
	'Operator Duties'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OperatorDuty', 'url'=>array('index')),
	array('label'=>'Manage OperatorDuty', 'url'=>array('admin')),
);
*/
?>


<h1>Добавить оператора в дежурство</h1>
<br><a href="<?php echo Yii::app()->createUrl('/operatorDuty/admin/'); ?>">Список</a>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>