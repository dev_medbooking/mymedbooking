<?php
/* @var $this OperatorDutyController */
/* @var $model OperatorDuty */
/*
$this->breadcrumbs=array(
	'Operator Duties'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OperatorDuty', 'url'=>array('index')),
	array('label'=>'Create OperatorDuty', 'url'=>array('create')),
	array('label'=>'Update OperatorDuty', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OperatorDuty', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OperatorDuty', 'url'=>array('admin')),
);
*/
?>

<h1>View OperatorDuty #<?php echo $model->id; ?></h1>
<br><a href="<?php echo Yii::app()->createUrl('/operatorDuty/admin/'); ?>">Список</a>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'extention',
		'date_start',
		'date_end',
		'queue_name',
	),
)); ?>
