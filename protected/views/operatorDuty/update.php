<?php
/* @var $this OperatorDutyController */
/* @var $model OperatorDuty */
/*
$this->breadcrumbs=array(
	'Operator Duties'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OperatorDuty', 'url'=>array('index')),
	array('label'=>'Create OperatorDuty', 'url'=>array('create')),
	array('label'=>'View OperatorDuty', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OperatorDuty', 'url'=>array('admin')),
);
*/
?>

<h1>Обновить <?php echo $model->id; ?></h1>
	<br><a href="<?php echo Yii::app()->createUrl('/operatorDuty/admin/'); ?>">Список</a>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>