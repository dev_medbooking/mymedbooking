<?php

/**
 * GxActiveForm class file.
 *
 * @author    Rodrigo Coelho <rodrigo@giix.org>
 * @link      http://giix.org/
 * @copyright Copyright &copy; 2010-2011 Rodrigo Coelho
 * @license   http://giix.org/license/ New BSD License
 */

/**
 * GxActiveForm provides forms with additional features.
 *
 * @author Rodrigo Coelho <rodrigo@giix.org>
 */
class GxActiveForm extends CActiveForm
{
	public $uploadUrl;
	public $deleteUrl;

	public function init()
	{

		foreach ($this->htmlOptions as $key => $value) {
			if (is_array($value) || $key == "validation") {
				$this->htmlOptions[$key] = json_encode($value);
			}
		}
		if (!isset($this->htmlOptions['id'])) {
			$this->htmlOptions['id'] = $this->id;
		} else {
			$this->id = $this->htmlOptions['id'];
		}

		if ($this->stateful) {
			echo CHtml::statefulForm($this->action, $this->method, $this->htmlOptions);
		} else {
			echo CHtml::beginForm($this->action, $this->method, $this->htmlOptions);
		}
	}

	public function textField($model, $attribute, $htmlOptions = array())
	{
		if (!self::_getAttributesForAccess($model, $attribute, $htmlOptions)) {
			return;
		}
		return CHtml::activeTextField($model, $attribute, $htmlOptions);
	}

	public function dropDownList($model, $attribute, $data, $htmlOptions = array())
	{
		if (!self::_getAttributesForAccess($model, $attribute, $htmlOptions)) {
			return;
		}
		return CHtml::activeDropDownList($model, $attribute, $data, $htmlOptions);
	}

	/**
	 * Renders a checkbox list for a model attribute.
	 * This method is a wrapper of {@link GxHtml::activeCheckBoxList}.
	 * #MethodTracker
	 * This method is based on {@link CActiveForm::checkBoxList}, from version 1.1.7 (r3135). Changes:
	 * <ul>
	 * <li>Uses GxHtml.</li>
	 * </ul>
	 *
	 * @see CActiveForm::checkBoxList
	 *
	 * @param CModel $model       The data model.
	 * @param string $attribute   The attribute.
	 * @param array  $data        Value-label pairs used to generate the check box list.
	 * @param array  $htmlOptions Addtional HTML options.
	 *
	 * @return string The generated check box list.
	 */
	public function checkBoxList($model, $attribute, $data, $htmlOptions = array(), $attributes = [])
	{
		if (isset($attributes["valueField"])) {
			$valueField = $attributes["valueField"];
		} else {
			$valueField = $attribute;
		}
		if (isset($attributes["group"])) {
			$group = $attributes["group"];
		} else {
			$group = '';
		}
		$formedData = self::_formData($model, $data, null, null, $group);
		if (is_array($model)) {
			$cModel = current($model);
		} else {
			$cModel = $model;
		}
		GxHtml::resolveNameID($model, $attribute, $htmlOptions);
		if (isset($attributes["selected"])) {
			$selection = self::_formData($cModel, $attributes["selected"], null, $valueField);
		} else {
			$selection = self::_formData($cModel, $model->$attribute, null, $valueField);
		}
		$name = $htmlOptions['name'];
		unset($htmlOptions['name']);
		if (array_key_exists('uncheckValue', $htmlOptions)) {
			$uncheck = $htmlOptions['uncheckValue'];
			unset($htmlOptions['uncheckValue']);
		} else {
			$uncheck = '';
		}
		$hiddenOptions = isset($htmlOptions['id']) ? array('id' => GxHtml::ID_PREFIX . $htmlOptions['id'])
			: array('id' => false);
		$hidden = $uncheck !== null ? GxHtml::hiddenField($name, $uncheck, $hiddenOptions) : '';
		$list = $hidden . $this->_formCheckBoxListRecursive($name, $selection, $formedData, $htmlOptions);
		return CHtml::tag('div', ["id" => "checkBoxList_" . GxHtml::activeName($model, $attribute)], $list);
	}

	private function _formCheckBoxListRecursive($name, $selection, $formedData, $htmlOptions)
	{
		if (!isset($htmlOptions["deep"])) {
			$htmlOptions["deep"] = 1;
		}
		$html = '';
		$simpleData = [];
		foreach ($formedData as $key => $data) {
			if (is_array($data)) {
				$caption = GxHtml::tag('div', [], $key);
				$deep = $htmlOptions["deep"]++;
				$baseID = isset($htmlOptions['baseID']) ? $htmlOptions['baseID'] : GxHtml::getIdByName($name);
				$baseID = $baseID . "_" . $deep;
				$list = $this->_formCheckBoxListRecursive(
					$name, $selection, $data, $htmlOptions + ["baseID" => $baseID]
				);
				$html .= GxHtml::tag('div', [], $caption . $list);
			} else {
				$simpleData[$key] = $data;
			}
		}
		if ($simpleData) {
			unset($htmlOptions["deep"]);
			$html .= GxHtml::checkBoxList($name, $selection, $simpleData, $htmlOptions);
		}
		return $html;
	}

	private static function _getAttributesForAccess($model, $name, &$params)
	{
		$values = [];
		if (method_exists($model, '_getColumnAccessData')) {
			if (!call_user_func_array(array($model, 'canRead'), [$name]) && !isset($params["noHide"])) {
				return false;
			}
			if (!call_user_func_array(array($model, 'canEdit'), [$name])) {
				$values["disabled"] = "disabled";
				$values["readonly"] = "readonly";
			}
			$params = array_merge($params, $values);
			return $values ? $values : true;
		}

		return true;
	}

	/**
	 * @param CModel $model
	 * @param        $name
	 * @param        $method
	 * @param array  $values
	 * @param array  $params
	 */
	public function showInputRow($model, $name, $method, $values = array(), $params = array())
	{
		$isText = $this->_isTextField($method);

		if ($isText) {
			$params = $values;
		}
		if ($model->isAttributeRequired($name)) {
			$params['required'] = 'required';
		}

		if (!self::_getAttributesForAccess($model, $name, $params)) {
			return;
		}

		if ($method != "hiddenField") {
			echo '<div class="control-group';
			if ($model->getError($name)) {
				echo ' error';
			}
			echo '" id="inputrow__' . $name . '" >';
			echo $this->labelEx($model, $name, array_merge($params, array('class' => 'control-label')));
			echo '<div class="controls">';
		}
		if ($isText) {
			$result = call_user_func_array(array($this, $method), array($model, $name, $params));
		} else {
			$result = call_user_func_array(array($this, $method), array($model, $name, $values, $params));
		}
		echo $result;

		if ($method != "hiddenField") {
			if (array_key_exists('help', $params)) {
				echo '<span class="help-block">' . $params['help'] . '</span>';
			} elseif ($model->getError($name)) {
				echo '<span class="help-block">' . $this->error($model, $name) . '</span>';
			}


			echo '</div>';
			echo '</div>';
		}
	}


	/**
	 * @param CModel $model
	 * @param        $name
	 * @param        $method
	 * @param array  $values
	 * @param array  $params
	 * @param string $controlsClass
	 */
	public function showCheckBox($model, $name, $method, $values = array(), $params = array(), $controlsClass = "")
	{
		if ($model->isAttributeRequired($name)) {
			$params['required'] = 'required';
		}
		if (!self::_getAttributesForAccess($model, $name, $values)) {
			return;
		}

		echo '<div class="control-group ';
		if ($model->getError($name)) {
			echo ' error';
		}
		echo '" id="inputrow__' . $name . '" >';
		echo '<div class="controls ' . $controlsClass . '">';
		echo '<label class="checkbox">';
		$result = call_user_func_array(array($this, $method), array($model, $name, $values, $params));
		echo $result;
		echo $this->labelEx($model, $name, array_merge($params, array()));
		echo '</label>';
		echo '</div>';
		echo '</div>';
	}

	public function modelLabel($inpModel, $name, $type = 'text', $attributes = [])
	{
		$origName = $name;
		$origAttributes = $attributes;
		if (is_array($inpModel)) {
			$className = get_class(current($inpModel));
			$model = current($inpModel);
		} else {
			$className = get_class($inpModel);
			$model = $inpModel;
		}
		if (!$x = self::_getAttributesForAccess($model, $name, $attributes)) {
			return;
		}
		$rname = $name;
		$html = '';
		$html .= '<div class="control-group">';

		if (isset($attributes["label"])) {
			if (DereferenceModel::getValue($inpModel, $attributes["label"]) !== null) {
				$attributes["label"] = $inpModel->getAttributeLabel($attributes["label"]);
			}
		}
		if (strpos($name, ".") !== false) {
			$path = DereferenceModel::deleteReference($name, -1);
			$newName = DereferenceModel::getReference($name, -1);
			$model = DereferenceModel::getValue($model, $path);
			if (!isset($attributes["label"])) {
				$firstAttributeName = DereferenceModel::getReference($name, 0);
				$attributes["label"] = $inpModel->getAttributeLabel($firstAttributeName);
			}
			$name = $newName;
		}
		if (isset($attributes["value"])) {
			$value = $attributes["value"];
			unset($attributes["value"]);
		} else {
			$value = DereferenceModel::getValue($model, $name);
		}
		if ($value === null) {
			if (isset($attributes["prompt"])) {
				$value = $attributes["prompt"];
			} else {
				$value = '<span class="null">Не задан<span>';
			}
		}
		if ($model) {
			$html .= $this->labelEx($model, $name, array('class' => 'control-label') + $attributes);
		} else {
			$html .= $this->label($inpModel, $name, array('class' => 'control-label') + $attributes);
		}
		$html .= '<div class="controls"><b>';
		switch ($type) {
			case "text":
				$html .= $value . "</b>";
				break;
			case "boolean":
				$html .= $value == 1 ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
				break;
			case "date":
				$html .= Yii::app()->dateFormatter->format("dd.MM.y", $value);
				break;
			case "phone":
//                $x = new SBFormatter();
				$html .= Yii::app()->format->formatPhone($value);
				break;
			default:
				return $this->modelLabel($inpModel, $origName, 'text', $origAttributes);
		}
		$html .= '</b></div>';
		$html .= '</div>';
		return $html;
	}

	/**
	 * @param string|GxActiveRecord $url
	 * @param string                $type
	 * @param string                $caption
	 * @param array                 $htmlOptions
	 *
	 * @return mixed
	 */
	public static function button($url, $type = '', $caption = '', $htmlOptions = [])
	{
		switch (strtolower($type)) {
			case "add":
				$caption = $caption ? $caption : Yii::t('captions', 'Add');
				$i = CHtml::tag('i', ["class" => "fa fa-plus mr5"], " " . $caption);
				return CHtml::link($i, $url, $htmlOptions + ["class" => "btn btn-success mr10"]);
				break;
			case "edit":
				$caption = $caption ? $caption : Yii::t('captions', 'Edit');
				$i = CHtml::tag('i', ["class" => "fa fa-pencil-square-o mr5"], " " . $caption);
				return CHtml::link($i, $url, $htmlOptions + ["class" => "btn btn-success mr10"]);
				break;
			case "delete":
				break;
			case "default":
				$caption = $caption ? $caption : Yii::t('captions', 'Button');
				return CHtml::link($caption, $url, $htmlOptions + ["class" => "btn btn-success mr10"]);
				break;
			case "ajax":
				$id = RandomHelper::getMd5();
				foreach ($htmlOptions as &$option) {
					if (is_array($option)) {
						$option = json_encode($option);
					}
				}
				$data = CArray::get($htmlOptions, 'ajax-data', '{}');
				$success = CArray::get($htmlOptions, 'ajax-success', '');
				$error = CArray::get($htmlOptions, 'ajax-error', '');
				$script
					= "$.get('$url', $data).success(function (data) { $success}).error(function (data) { $error});";
				$htmlOptions["id"] = $id;
				Yii::app()->clientScript->registerScript(
					'ajax-button#' . $id, "$('#$id').click(function(){ " . $script . "})"
				);

				$caption = $caption ? $caption : Yii::t('captions', 'Ajax');
				return CHtml::tag('span', $htmlOptions + ["class" => "btn btn-success mr10"], $caption);
				break;
			case "view":
				$caption = $caption ? $caption : Yii::t('captions', 'View');
				$i = CHtml::tag('i', ["class" => "fa fa-search mr5"], " " . $caption);
				return CHtml::link($i, $url, $htmlOptions + ["class" => "btn btn-success mr10"]);
				break;
			case "return":
				$caption = $caption ? $caption : Yii::t('captions', 'Return');
				$i = CHtml::tag('i', ["class" => "fa fa-arrow-left mr5"], " " . $caption);
				return CHtml::link($i, $url, $htmlOptions + ["class" => "btn btn-info mr10"]);
				break;
			case "print":
				$caption = $caption ? $caption : Yii::t('captions', 'Print');
				$i = CHtml::tag('i', ["class" => "fa fa-print mr5"], " " . $caption);
				return CHtml::link($i, $url, $htmlOptions + ["class" => "btn mr10"]);
				break;
			case "download":
				$caption = $caption ? $caption : Yii::t('captions', 'Download');
				$i = CHtml::tag('i', ["class" => "fa fa-download mr5"], " " . $caption);
				return CHtml::link($i, $url, $htmlOptions + ["class" => "btn mr10"]);
				break;
			case "set":
				return GxHtml::submitButton(Yii::t('captions', 'Set'), ['class' => 'btn btn-success']);
				break;
			case "save":
				$caption = $caption
					? $caption
					: ($url->getIsNewRecord()
						? Yii::t('captions', 'Add')
						: Yii::t(
							'captions', 'Save'
						));

				return GxHtml::submitButton(
					$caption, HtmlOptionsHelper::merge($htmlOptions, ['class' => 'btn btn-success'])
				);
				break;
			default:
				return self::button($url, 'default', $caption, $htmlOptions);
		}
	}

	public static function progressBar($percent, $htmlOptions = [], $attributes = [])
	{
		$htmlOptions = array_merge(["class" => "progress progress-striped active"], $htmlOptions);
		$bar = CHtml::tag('div', ["class" => "bar", "style" => "width: " . $percent . "%"]);
		$tag = CHtml::tag('div', $htmlOptions, $bar);
		return $tag;
	}

	/**
	 * @param CModel         $model
	 * @param                $attribute
	 * @param string         $type
	 * @param array          $attributes
	 * @param array          $htmlOptions
	 *
	 * @return string
	 */
	public function modelField($model, $attribute, $type = 'textField', $attributes = [], $htmlOptions = [],
		$htmlContOptions = [])
	{

		$defaultHtmlOptions = ["class" => "form-control"];

		$labelAttributes = [];
		$showLabel = true;
		$className = get_class($model);

		foreach ($htmlOptions as $key => $option) {
			if (is_array($option)) {
				$htmlOptions[$key] = json_encode($option);
			}
		}

		if (!self::_getAttributesForAccess($model, $attribute, $htmlOptions)) {
			return;
		}

		$htmlOptions = array_merge($defaultHtmlOptions, $htmlOptions);
		if ($model->isAttributeRequired($attribute)) {
			$htmlOptions["class"] .= " required";
		}


		$html = '';
		$html .= '<div class="control-group';

		if (isset($htmlContOptions['class'])) {
			$html .= ' ' . $htmlContOptions['class'];
		}

		if ($model->getError($attribute)) {
			$html .= ' error';
		}

		if ($type == 'radio' || $type == 'checkBox') {
			$html .= ' control-group__rc ';
			$htmlOptions['template'] = '<div>{input} {label}</div>';
			$htmlOptions['container'] = 'div';
			$htmlOptions['separator'] = '';
		}

		$html .= '" id="' . $className . '_' . $attribute . '_row">';

		$labelAttributes["class"] = "control-label";

		if (isset($attributes["label"])) {
			$labelAttributes["label"] = $attributes["label"];
			unset($attributes["label"]);
			$showLabel = (bool)$labelAttributes["label"];
		}

		if ($showLabel) {
			$html .= $this->labelEx($model, $attribute, $labelAttributes);
		}

		$html .= '<div class="controls">';
		switch ($type) {
			case "textField":
				$html .= $this->textField($model, $attribute, array_merge(['maxlength' => 255], $htmlOptions));
				break;
			case "phone":
				$model->$attribute = substr($model->$attribute, 1);
				$attributes["help"] = (isset($attributes['help']) && ($attributes['help'] !== false))
					? $attributes['help'] : '';
				$html .= '<div class="pull-left controls-phone_num" >+7</div> <div class="controls-phone"><div>';
				$html .= $this->widget(
					'CMaskedTextField', array(
					'model'       => $model,
					'attribute'   => $attribute,
					'mask'        => '(999)-999-9999',
					'placeholder' => '(123)-456-7890',
					'completed'   => 'function(){}',
					'htmlOptions' => $htmlOptions
				), true
				);
				$html .= '</div></div>';
				break;
			case "boolean":
				$data = [0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes')];
				$html .= $this->dropDownList($model, $attribute, $data, $htmlOptions);
				break;
			case "sex":
				$data = [0 => Yii::t('app', 'Female'), 1 => Yii::t('app', 'Male')];
				$html .= $this->dropDownList($model, $attribute, $data, $htmlOptions);
				break;
			case "dropDownList":
				if (isset($attributes["data"])) {
					$res = self::formDropDownListData($model, $attribute, $attributes);
					$data = $res["data"];
					if (isset($attributes["prompt"])) {
						$newData[null] = $attributes["prompt"];
						foreach ($data as $key => $value) {
							$newData[$key] = $value;
						}
						unset($attributes["prompt"]);
						$data = $newData;
					}
					if (isset($attributes["exclude"])) {
						$exclude = is_array($attributes["exclude"]) ? $attributes["exclude"] : [$attributes["exclude"]];
						$data = array_diff_key($data, array_flip(array_filter($exclude)));
					}
					$opt = array_merge($res["htmlOptions"], $htmlOptions);
					$html .= $this->dropDownList($model, $attribute, $data, $opt);
					if (isset($attributes['resultBlock'])) {
						$html .= CHtml::tag('div', array('id' => 'resultBlock', 'style' => 'display:none;'));
						$html .= CHtml::closeTag('div');
					}
				} else {
					if ($className = DereferenceModel::getRelatedClass($model, $attribute)) {
						$attributes = array_merge($attributes, ["data" => $className::model()->findAll()]);
						return $this->modelField($model, $attribute, $type, $attributes, $htmlOptions);
					}
				}
				break;
			case "password":
				$htmlOptions = array_merge(['maxlength' => 255, 'value' => ''], $htmlOptions);
				if (isset($attributes["generator"]) && $attributes["generator"]) {
					JSLoaderHelper::loadJsPGenerator();
					$pid = CHtml::getIdByName(CHtml::resolveName($model, $attribute));
					$id = $pid . "_pg";
					$cs = Yii::app()->clientScript;
					$cs->registerScript(
						'passGen#' . $id, "jQuery('#$id').pGenerator({
                            'bind': 'click',
                            'passwordElement': '#$pid',
                            'displayElement': '#$pid',
                            'passwordLength': 8,
                            'uppercase': true,
                            'lowercase': true,
                            'numbers':   true,
                            'specialChars': false,
                            'onPasswordGenerated': function(generatedPassword) {
                                $('#$pid').prop('type', 'text');
                            }
                        });"
					);
					$html .= CHtml::tag('div', ["class" => "input-append"], false, false);
				}
				$html .= $this->passwordField($model, $attribute, $htmlOptions);
				if (isset($attributes["generator"]) && $attributes["generator"]) {
					$i = CHtml::tag('i', ["class" => "fa fa-random"], '');
					$html .= CHtml::tag('span', ["class" => "btn", "id" => $id], $i);
					$html .= CHtml::closeTag('div');
				}
				break;
			case "fio":
				$htmlOptions += ['placeholder' => 'Иванов Иван Иванович'];
				$html .= $this->textField($model, $attribute, array_merge(['maxlength' => 255], $htmlOptions));
				break;
			case "float":
				JSLoaderHelper::loadMask();
				$d = CArray::get($htmlOptions, 'decimals', 10);
				$s = CArray::get($htmlOptions, 'sub-decimals', 10);
				$sep = CArray::get($htmlOptions, 'separator', '.');
				$html .= $this->widget(
					'CMaskedTextField', array(
					'model'       => $model,
					'attribute'   => $attribute,
					'mask'        => str_repeat('9', $d) . $sep . str_repeat('9', $s),
					'placeholder' => '00' . $sep . '00',
					'htmlOptions' => $htmlOptions
				), true
				);
				break;
			case "number":
				JSLoaderHelper::loadMask();
				$mask = str_repeat('9', $length = CArray::get($htmlOptions, 'maxlength', 10));
				$placeholder = '1234567890';
				if ($length < strlen($placeholder)) {
					$placeholder = substr($placeholder, 0, $length);
				}
				$html .= $this->widget(
					'CMaskedTextField', array(
					'model'       => $model,
					'attribute'   => $attribute,
					'mask'        => $mask,
					'placeholder' => $placeholder,
					'htmlOptions' => $htmlOptions
				), true
				);
				break;
			case "date":
				$html .= $this->dateField($model, $attribute, $htmlOptions);
				break;
			case "time":
				JSLoaderHelper::loadDateTimePicker();
				if (isset($attributes['start']) && isset($attributes['end'])) {
					$html .= "<span>c: </span>";
					$html .= $this->textField(
						$model, $attributes['start'], array(
							'maxlength' => 5,
							'class'     => 'span4 js-timepicker',
						) + $htmlOptions
					);
					$html .= "<span> по: </span>";
					$html .= $this->textField(
						$model, $attributes['end'], array(
							'maxlength' => 5,
							'class'     => 'span4 js-timepicker',
						) + $htmlOptions
					);
				} else {
					$html .= $this->textField(
						$model, $attribute, array(
							'maxlength' => 5,
							'class'     => 'span4 js-timepicker',
						) + $htmlOptions
					);
				}
				break;
			case "passport":
				$html .= $this->textField(
					$model, $attribute, array(
						'maxlength'   => 255,
						'data-mask'   => '9999 999999',
						'placeholder' => 'xxxx xxxxxx'
					) + $htmlOptions
				);
				break;
			case "money":
				JSLoaderHelper::loadMask();
				$html .= $this->widget(
					'CMaskedTextField', array(
					'model'       => $model,
					'attribute'   => $attribute,
					'mask'        => '000 000 000 000 000.00',
					'placeholder' => '0',
					'reverse'     => true,
					'htmlOptions' => $htmlOptions
				), true
				);
				break;
			case "datePeriod":
				$html .= "с " . $this->textField($model, $attribute, ["class" => "date"]);
				$html .= "по " . $this->textField($model, $attribute, ["class" => "date"]);
				break;
			case "email";
				$html .= $this->textField(
					$model, $attribute, array(
						'maxlength'   => 255,
						'placeholder' => 'mail@mail.ru'
					)
				);
				break;
			case "textArea":
				$html .= $this->textArea($model, $attribute, $htmlOptions);
				break;
			case "checkBox":

				$html .= $this->checkBox($model, $attribute, $htmlOptions);
				break;
			case "radio":

				if (isset($attributes["group"])) {
					$data = (isset($attributes["data"])) ? $attributes["data"] : [];
					$html .= $this->radioGroup($model, $attribute, $data, $htmlOptions);
				} else {
					$html .= $this->radioButton($model, $attribute, $htmlOptions);
				}
				break;
			default:
				return $this->modelField($model, $attribute, 'textField', $attributes, $htmlOptions);
		}

		if ($model->getError($attribute) || isset($attributes["help"])) {
			$html .= '<div class="help-block">';
			if ($model->getError($attribute)) {
				$html .= $this->error($model, $attribute);
			} else {
				if (isset($attributes["help"])) {
					$html .= $attributes["help"];
				}
			}
			$html .= '</div>';
		}
		$html .= '</div>';
		$html .= '<div id="' . $className . '_' . $attribute . '_em_"></div>';
		$html .= "</div>";
		return $html;
	}

	public function dateField($model, $attribute, $htmlOptions = array())
	{
		JSLoaderHelper::loadMask();
		if ($model->{$attribute}) {
			$htmlOptions["value"] = DateHelper::reformat($model->{$attribute}, 'd.m.Y');
		}
		$htmlOptions = HtmlOptionsHelper::merge($htmlOptions, ["class" => "date"]);
		$html = $this->widget(
			'CMaskedTextField', array(
			'model'       => $model,
			'attribute'   => $attribute,
			'mask'        => '00.00.0000',
			'placeholder' => 'ДД.ММ.ГГГГ',
			'completed'   => 'function(){console.log(343);}',
			'htmlOptions' => $htmlOptions
		), true
		);
		return $html;
	}

	public static function formDropDownListData($model, $attribute, $attributes)
	{
		$group = '';
		if (isset($attributes["group"])) {
			$group = $attributes["group"];
		}
		$opt = [];
		$valueText = @$attributes["valueText"];
		$valueField = @$attributes["valueField"];
		if (isset($attributes["insertModel"]) && $attributes["insertModel"]) {
			$insert = [];
			if ($model->$attribute && $attributes["insertModel"] === true) {
				$insert[] = $model->$attribute;
			} elseif (is_string($attributes["insertModel"])) {
				if ($model->$attribute) {
					$insert[] = DereferenceModel::getValue($model->$attribute, $attributes["insertModel"]);
				} else {
					$insert[] = $attributes["insertModel"];
				}
			} elseif (is_array($attributes["insertModel"])) {
				foreach ($attributes["insertModel"] as $elem) {
					$insert[] = $elem;
				}
			} elseif (is_object($attributes["insertModel"])) {
				$insert[] = $attributes["insertModel"];
			}
			if ($insert) {
				$attributes["data"] = array_merge($insert, $attributes["data"]);
			}
			unset($attributes["insertModel"]);
		}
		$data = self::_formData($model, $attributes["data"], $valueField, $valueText, $group);
		$selectAttribute = $attribute;
		if (!isset($attributes["selected"])) {
			$attributes["selected"] = $model->$attribute;
			if (!isset($attribute["selectAttribute"])) {
				$dataClassName = '';
				if (is_array($attributes["data"]) && current($attributes["data"]) instanceof GxActiveRecord) {
					$dataClassName = get_class(current($attributes["data"]));
				}
				if (is_array($attributes["selected"])) {
					if ($attributes["selected"]) {
						$relationClassName = get_class(current($attributes["selected"]));
					} else {
						$relationClassName = DereferenceModel::getRelatedClass($model, $attribute);
					}
					foreach ($relationClassName::model()->relations() as $relation) {
						if ($relation[1] == $dataClassName) {
							$selectAttribute = $relation[2];
							break;
						}
					}
				} elseif ($attributes["selected"] instanceof GxActiveRecord) {
					$relationClassName = get_class($attributes["selected"]);
					foreach ($relationClassName::model()->relations() as $relation) {
						if ($relation[1] == $dataClassName) {
							$selectAttribute = $relation[2];
							break;
						}
					}
				}
			}
		}
		if (isset($attributes["selectAttribute"])) {
			$selectAttribute = $attributes["selectAttribute"];
		}
		$selected = self::_formData($model, $attributes["selected"], null, $selectAttribute);
		foreach ($selected as $value) {
			$opt["options"][$value] = ['selected' => true];
		}
		return ["data" => $data, "htmlOptions" => $opt];
	}

	private static function _formData($model, $data, $valueField = null, $valueText = null, $group = '')
	{
		if ($data instanceof CDbCriteria) {
			return self::_formData($model, $model::model()->findAll($data), $valueField, $valueText);
		}
		if (is_array($data)) {
			$rawData = [];
			foreach ($data as $key => $element) {
				if (!($element instanceof CActiveRecord)) {
					$rawData[$key] = $element;
					unset($data[$key]);
				}
			}
			return $rawData + GxHtml::listDataEx($data, $valueField, $valueText, $group);
		}
		return self::_formData($model, [$data], $valueField, $valueText);
	}

	/**
	 * @param CModel $model
	 * @param        $name
	 */
	public function isError($model, $name)
	{
		if ($model->getError($name)) {
			echo 'error';
		}
	}

	/**
	 * @param       $model
	 * @param null  $attribute
	 * @param null  $types
	 * @param array $htmlOptions
	 * @param array $params
	 */
	public function showFileUpload($model, $attribute = null, $types = null, $htmlOptions = [], $params = [])
	{
		$buttonHtmlOptions = [];
		if (!self::_getAttributesForAccess($model, $attribute, $buttonHtmlOptions)) {
			return;
		}

		$ext = [];
		$maxCount = -1;
		if ($attribute) {
			foreach ($model->getValidators($attribute) as $validator) {
				if ($validator instanceof sessionFile) {
					if ($extensions = $validator->types) {
						$arr = explode(',', $extensions);
						foreach ($arr as $element) {
							$ext[] = Yii::t('extensions', trim($element));
						}
					}
					$maxCount = $validator->count;
					break;
				}
			}
		}

		$this->widget(
			'application.widgets.FileUploader', array(
				'model'             => $model,
				'attribute'         => $attribute,
				'types'             => $types,
				'uploadUrl'         => CArray::get($params, 'uploadUrl'),
				'deleteUrl'         => CArray::get($params, 'deleteUrl'),
				'slug'              => isset($params["slug"]) ? $params["slug"] : null,
				'htmlOptions'       => $htmlOptions,
				'buttonHtmlOptions' => $buttonHtmlOptions,
				'help'              => null,
				'accessExtensions'  => $ext,
				'files'             => null,
				'maxCount'          => $maxCount,
			)
		);
	}

	/**
	 * @param       $model
	 * @param null  $attribute
	 * @param null  $types
	 * @param array $htmlOptions
	 * @param array $params
	 */
	public function newShowFileUpload($model, $attribute = null, $types = null, $apiType = null, $htmlOptions = [],
		$params = [])
	{
		$buttonHtmlOptions = [];
		if (!self::_getAttributesForAccess($model, $attribute, $buttonHtmlOptions)) {
			return;
		}

		$ext = [];
		$maxCount = -1;
		if ($attribute) {
			foreach ($model->getValidators($attribute) as $validator) {
				if ($validator instanceof sessionFile) {
					if ($extensions = $validator->types) {
						$arr = explode(',', $extensions);
						foreach ($arr as $element) {
							$ext[] = Yii::t('extensions', trim($element));
						}
					}
					$maxCount = $validator->count;
					break;
				}
			}
		}

		$this->widget(
			'application.widgets.FileApi', array(
				'model'             => $model,
				'attribute'         => $attribute,
				'method'            => isset($params["method"]) ? $params["method"] : null,
				'types'             => $types,
				'apiType'           => $apiType,
				'uploadUrl'         => null,
				'deleteUrl'         => null,
				'slug'              => isset($params["slug"]) ? $params["slug"] : null,
				'htmlOptions'       => $htmlOptions,
				'buttonHtmlOptions' => $buttonHtmlOptions,
				'buttonText'        => isset($params["btn"]) ? $params["btn"] : null,
				'help'              => null,
				'accessExtensions'  => $ext,
				'files'             => null,
				'maxCount'          => $maxCount
			)
		);
	}


	/**
	 * @param       $model
	 * @param null  $attribute
	 * @param null  $types
	 * @param array $htmlOptions
	 * @param array $params
	 */
	public function LinkedLists(CDataProvider $dataProvider, CActiveRecord $model, $attribute, $method,
		$htmlOptions = [], $params = [])
	{

		$this->widget(
			'application.widgets.AppLinkedLists', [
				'id'                 => (isset($htmlOptions['id'])) ? $htmlOptions['id'] : null,
				'dataProvider'       => $dataProvider,
				'model'              => $model,
				'attribute'          => $attribute,
				'methodName'         => $method,
				'htmlOptions'        => $htmlOptions,
				'params'             => $params,
				'ajaxUpdate'         => true,
				'emptyText'          => '<h3>Не привязан никто</h3>',
				'itemsCssClass'      => 'urm-company',
				'rowCssClass'        => 'js-urm-box',
				'toggleLinkCssClass' => 'js-urm-open',
				'innerRowCssClass'   => 'urm-office js-urm-office',
			]
		);
	}

	public static function actionButton($model, $attribute, $caption, $value, $htmlOptions = [])
	{
		$b = new GxActiveForm();
		$id = "activeButton_" . $b->getId();
		$formHtmlOptions = [
			'id'    => $id,
			'class' => 'btn'
		];
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');
		$htmlOptions = array_merge($formHtmlOptions, $htmlOptions);
		if (isset($htmlOptions["onclick"])) {
			$onclick = $htmlOptions["onclick"];
			unset($htmlOptions["onclick"]);
			$cs->registerScript('cl#' . $id, "function onclick_$id(){ $onclick }");
			$script = "function(){ if (onclick_$id()){ this.submit(); }else{ return false; }}";
		} else {
			$script = "function(){ this.submit(); }";
		}

		$b->beginWidget('GxActiveForm', ['htmlOptions' => $htmlOptions]);
		echo CHtml::activeHiddenField($model, $attribute, ["value" => $value]);
		echo $caption;
		$cs->registerScript('actionButton#' . $id, "jQuery('#$id').click($script);");
		$b->endWidget();
	}

	private function _isTextField($method)
	{
		$texts = array('textField', 'dateField');

		if (in_array($method, $texts)) {
			return true;
		}
		return false;
	}

	public function showExportTable($model, $rows)
	{
		JSLoaderHelper::loadExportForm();
		echo '<table class="inner_tbl_padding" cellspacing="0" cellpadding="0" border="0">';
		echo '<tbody>';
		echo '<tr>';
		echo '<td class="inner_td_padding">';
		$x = 0;
		foreach ($rows as $row) {
			$rowHtml = '';
			$rowHtml .= '<table cellspacing="0" cellpadding="0" border="0">';
			$rowHtml .= '<tbody>';
			$name = false;
			$leftHtml = '';
			$rightHtml = '';
			$i = 0;
			foreach ($row as $data) {
				$data["data"] = CArray::get($data, 'data', []);
				if (!CArray::get($data, "visible", true)) {
					continue;
				}
				$name = $name ? $name : CArray::get($data, "name", "exportRow_" . $i);
				$type = count($row) > 1 ? "radio" : "checkBox";
				$leftHtml .= $this->_exportTableRenderRowLeft($model, $data, $i, $name, $type);
				$rightHtml .= $this->_exportTableRenderRowRight($model, $data["data"], $name, $i);
				$i++;
			}
			$rowHtml .= '<tr>';
			$rowHtml .= '<td id="td_report_tabs' . $x . '" style="width: 235px;">';
			$rowHtml .= $leftHtml;
			$rowHtml .= '</td>';
			$rowHtml .= '<td class="item_type_select" style="width: 300px;">';
			$rowHtml .= $rightHtml;
			$rowHtml .= '</td>';
			$rowHtml .= '</tr>';
			$rowHtml .= '</tbody>';
			$rowHtml .= '</table>';
			echo $i ? $rowHtml : "";
			$x++;
		}

		echo '</td>';
		echo '</tr>';
		echo '</tbody>';
		echo '</table>';
	}

	private function _exportTableRenderRowLeft($model, $data, $row, $name, $type = 'checkBox')
	{

		$data["data"] = CArray::get($data, 'data', []);
		$data["data"]["name"] = CArray::get($data["data"], 'name', '');
		$html = '';
		$html .= '<table cellspacing="0" cellpadding="0" border="0">';
		$html .= '<tbody>';
		$html .= "<tr>";
		$html .= '<td style="height: 25px;">';

		$html .= '<div data="' . $name . 'Tab" row="' . $row . '" class="report_tab_active report_tab">';
		if ($type == "checkBox" && !isset($data["data"]["required"])) {
			$html .= '<input type="checkbox" checked>';
		} elseif ($type == "radio") {
			$checked = ($row == 0 ? "checked" : "");
			$html .= '<input type="radio" ' . $checked . ' name="' . $name . '" value="' . $data["data"]["name"] . '">';
		}

		$html .= '<span class="item_type_name" style="padding-left: 5px;">' . $data['caption'] . '</span>';
		$html .= '</div>';
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '</tr>';
		$html .= '</tbody>';
		$html .= '</table>';
		return $html;
	}

	private function _exportTableRenderRowRight($model, $data, $name, $row)
	{
		$data["caption"] = CArray::get($data, 'caption', '');
		$data["inputs"] = CArray::get($data, 'inputs', []);
		$html = '';
		$html .= '<div id="TabValue_' . $name . 'Tab_' . $row . '" class="current_tab_value" style="display: block;">';
		$html .= '<div class="item_type_text" style="min-height: 75px; padding: 5px 10px 10px 10px;">';
		$html .= '<div class="report_inner" style="position: relative; padding: 10px 0px 0px 0px;">';
		$html .= '<table>';
		$html .= '<tbody>';
		$html .= '<tr>';
		$html .= '<td colspan="4">' . $data["caption"] . '</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>';
		if (isset($data["html"])) {
			$html .= $data["html"];
		} else {
			foreach ($data["inputs"] as $input) {
				if (!CArray::get($input, "visible", true)) {
					continue;
				}
				if (isset($input["htmlBefore"])) {
					$html .= $input["htmlBefore"];
				}
				if (isset($input["html"])) {
					$html .= $input["html"];
				} else {
					ob_start();
					if (isset($input["method"])) {
						echo call_user_func_array(array($this, $input["method"]), $input["values"]);
					}
					$html .= ob_get_contents();
					ob_end_clean();
				}
				if (isset($input["htmlAfter"])) {
					$html .= $input["htmlAfter"];
				}
			}
		}
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</tbody>';
		$html .= '</table>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		return $html;
	}

	public static function showFlash($key = null)
	{
		if (!$key) {
			foreach (CurrentUser::webUser()->getFlashes(false) as $key => $value) {
				self::showFlash($key);
			}
		} else {
			switch ($key) {
				case "success":
					$htmlOptions = ["class" => "alert alert-success"];
					break;
				case "error":
					$htmlOptions = ["class" => "alert alert-danger"];
					break;
				default:
					$htmlOptions = ["class" => "alert alert-info"];
			}
			if (CurrentUser::webUser()->hasFlash($key)) //
			{
				echo CHtml::tag(CHtml::$errorContainerTag, $htmlOptions, CurrentUser::webUser()->getFlash($key));
			}
		};
	}

	public function radioGroup($model, $attribute, $data, $htmlOptions)
	{
		$html = '';
		$html .= CHtml::activeRadioButtonList(
			$model, $attribute, $data, array_merge($htmlOptions, ['uncheckValue' => null])
		);
		return $html;
	}
}