<?php

class RecordsStatCommand extends CConsoleCommand
{

	public function run($args)
	{
		date_default_timezone_set("Europe/Moscow");
		setlocale(LC_ALL, "ru_RU.UTF-8");
		$dayParam = date('Y-m-d', time());
		$leftDateParam = $dayParam . ' 00:00:00';
		$rightDateParam = $dayParam . ' 23:59:59';
		$currentDay = date('d-m-Y', time());
		$subscribers = Yii::app()->params['newRecordsSubs'];
		$subscribers2 = Yii::app()->params['newRecordsSubs2'];
		if (empty($subscribers)) {
			return;
		}
		if (is_array($subscribers) == false) {
			$subscribers = array($subscribers);
		}
		$domains = MbDomain::model()->findAll();
		if (empty($domains) || is_array($domains) == false || count($domains) == 0) {
			return;
		}
		$mailBody = array(
			'domains' => array(),
			'totals' => array(
				'new' => 0,
				'confirmed' => 0,
				'call' => 0,
				'in' => 0,
			),
			'money' => array()
		);
		foreach ($domains as $domain) {
			$recordsNew = Yii::app()->db->createCommand("SELECT * FROM api_record WHERE (deleted IS NULL OR deleted=0) AND (status!=8 AND status!=9 AND status!=10) AND domain='" . $domain->title . "' AND create_time between '" . $leftDateParam . "' AND '" . $rightDateParam . "' ORDER BY create_time, name")->queryAll();
			$recordsConfirmed = Yii::app()->db->createCommand("SELECT * FROM api_record WHERE (deleted IS NULL OR deleted=0) AND domain='" . $domain->title . "' AND create_time between '" . $leftDateParam . "' AND '" . $rightDateParam . "' AND (status=5 or status=3 or status=2 or status=52 or status=11) ORDER BY create_time, name")->queryAll();
			$recordsCallReq = Yii::app()->db->createCommand("SELECT * FROM api_call WHERE domain='" . $domain->title . "' AND create_time between '" . $leftDateParam . "' AND '" . $rightDateParam . "' ORDER BY create_time, name")->queryAll();
			$recordsInReq = Yii::app()->db->createCommand("SELECT * FROM api_record WHERE (deleted IS NULL OR deleted=0) AND domain='" . $domain->title . "' AND lead_time between '" . $leftDateParam . "' AND '" . $rightDateParam . "' AND status=3 ORDER BY lead_time, name")->queryAll();
			if (empty($recordsNew) && empty($recordsConfirmed) && empty($recordsCallReq) && empty($recordsInReq)) {
				continue;
			}
			array_push($mailBody['domains'], array(
				'title' => $domain->title,
				'records' => array(
					'new' => count($recordsNew),
					'confirmed' => count($recordsConfirmed),
					'call' => count($recordsCallReq),
					'in' => count($recordsInReq),
				)
			));
			$mailBody['totals']['new'] += count($recordsNew);
			$mailBody['totals']['confirmed'] += count($recordsConfirmed);
			$mailBody['totals']['call'] += count($recordsCallReq);
			$mailBody['totals']['in'] += count($recordsInReq);
		}

		$time = new DateTime();
		$day = $time->format('Y-m-d');
		$todayPayments = Yii::app()->db->createCommand("SELECT IFNULL(SUM(summ), 0) AS summ FROM integrator_law_reports_payments WHERE DATE_FORMAT(payment_date, '%Y-%m-%d') = '".$day."'")->queryScalar();
		$todayPaymentsDebt = Yii::app()->db->createCommand("SELECT IFNULL(SUM(summ), 0) AS summ FROM integrator_law_reports_payments WHERE DATE_FORMAT(payment_date, '%Y-%m-%d') = '".$day."' AND payment_type = '".IntegratorLawReportsPayments::PAYMENT_DEBT."'")->queryScalar();
		$todayReportSumm = Yii::app()->db->createCommand("
				SELECT IFNULL(SUM(law_reports.summ), 0) AS summ FROM
				integrator_clinic_all AS t
				LEFT JOIN integrator_law_reports AS law_reports ON (law_reports.law_id=t.id)
				LEFT JOIN integrator_law_reports_status AS law_report_status ON (law_report_status.report_id=law_reports.id)
				LEFT OUTER JOIN auth_user AS iu ON (t.uid=iu.uid)
				WHERE (((law_reports.status = '".IntegratorLawReports::STATUS_BILL."')
				AND (law_report_status.status = '".IntegratorLawReports::STATUS_BILL."'))
				AND (DATE_FORMAT(law_report_status.status_date, '%Y-%m-%d') = '".$day."'))")->queryScalar();

		$todayDebtSumm = Yii::app()->db->createCommand("
				SELECT IFNULL(SUM(law_reports.summ), 0) AS summ FROM
				integrator_clinic_all AS t
				LEFT JOIN integrator_law_reports AS law_reports ON (law_reports.law_id=t.id)
				LEFT JOIN integrator_law_reports_status AS law_report_status ON (law_report_status.report_id=law_reports.id)
				LEFT OUTER JOIN auth_user AS iu ON (t.uid=iu.uid)
				WHERE (((law_reports.status = '".IntegratorLawReports::STATUS_DEBT."')
				AND (law_report_status.status = '".IntegratorLawReports::STATUS_DEBT."'))
				AND (DATE_FORMAT(law_report_status.status_date, '%Y-%m-%d') < '".$day."'))")->queryScalar();

		$mailBody['money']['Сумма собранных денег за день'] = $todayPayments." руб.";
		$mailBody['money']['Сумма выставленных счетов за день'] = $todayReportSumm." руб.";

		$partnerStat = new Partner();
		$partnerStat->admin = TRUE;
		$partnerStat->startDate = $dayParam;
		$partnerStat->endDate = $dayParam;
		$partnerStat = $partnerStat->getRecordInfo();

		$mailBody['partner'] = 'Всего записей от партнеров: '.( ! empty($partnerStat['all_record']) ? $partnerStat['all_record'] : 0).
			" из них к оплате ".( ! empty($partnerStat['success_record']) ? $partnerStat['success_record'] : 0).
			" на сумму ".( ! empty($partnerStat['price']) ? $partnerStat['price'] : 0)." руб.";

		$oktell = Yii::app()->db->createCommand("SELECT oktell FROM api_settings WHERE 1 ORDER BY id DESC")->queryScalar();
		$subject = 'Сервис онлайн записи, ' . $currentDay . ', отчет по записям';
		$header = 'Отчет по записям на ' . $currentDay;

		// Рендер для newRecordsSubs
		$mail = new YiiMailer('records2', array('mailBody' => $mailBody, 'title' => 'Сервис онлайн записи', 'header' => $header, 'oktell' => $oktell));
		$mail->render();
		$mail->From = Yii::app()->params['fromEmailAddress'];
		$mail->FromName = Yii::app()->params['fromName'];
		$mail->Subject = $subject;
		foreach ($subscribers as $subscriber) {
			if ( ! in_array($subscriber, $subscribers2)) $mail->AddAddress($subscriber);
		}
		if ($mail->Send()) {
			$mail->ClearAddresses();
		}

		// Рендер для newRecordsSubs2
		$mailBody['money']['Сумма оплат по дебиторской за день'] = $todayPaymentsDebt." руб.";
		$mailBody['money']['Сумма дебиторской задолженности по всем счетам'] = $todayDebtSumm." руб.";
		$mail = new YiiMailer('recordsSubs2', array('mailBody' => $mailBody, 'title' => 'Сервис онлайн записи', 'header' => $header, 'oktell' => $oktell));
		$mail->render();
		$mail->From = Yii::app()->params['fromEmailAddress'];
		$mail->FromName = Yii::app()->params['fromName'];
		$mail->Subject = $subject;
		foreach ($subscribers as $subscriber) {
			if (in_array($subscriber, $subscribers2)) $mail->AddAddress($subscriber);
		}
		if ($mail->Send()) {
			$mail->ClearAddresses();
		}
		$day = $time->format('d');
		if ($day == '01') {
			$timePast = new DateTime();
			$timePast->sub(new DateInterval('P2D'));
			$timePast = $timePast->format('Y-m');
			$time = $time->format('Y-m');
			$this->cronByRecord($timePast);
			$this->cronByRecordPrice($time, $timePast);
			$this->cronByRecordPriceOld($time, $timePast);
			$this->cronUpdateDebtOld($time);
			$this->cronUpdateDebt($time);
		}
	}

	public function cronByRecord($time)
	{
		set_time_limit(0);
		$data = Yii::app()->db->createCommand('SELECT * FROM api_record'
			. ' WHERE status IN(3,2,4,5,52,11) AND'
			. ' reception_time LIKE "%' . $time . '%" AND'
			. ' (deleted IS NULL OR deleted=0) AND'
			. ' (book_id="" OR book_id IS NULL)  ORDER BY status DESC')->queryAll();

		foreach ($data as $value) {
			if (!empty($value['clinic_id']) && !empty($value['domain'])) {
				if ($value['domain'] == 'medbooking.com') {
					$model = IntegratorClinic::model()->findByAttributes(array('medbooking_id' => $value['clinic_id']));
					if (!empty($model->id)) {
						Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
					} else {
						$model = IntegratorClinic::model()->findByAttributes(array('title' => $value['clinic_name']));
						if (!empty($model->id)) {
							Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
						}
					}
				}
				if ($value['domain'] == 'diagnostika.medbooking.com') {
					$model = IntegratorClinic::model()->findByAttributes(array('diagnostica_id' => $value['clinic_id']));
					if (!empty($model->id)) {
						Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
					} else {
						$model = IntegratorClinic::model()->findByAttributes(array('title' => $value['clinic_name']));
						if (!empty($model->id)) {
							Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
						}
					}
				}
				if ($value['domain'] == 'testpuls.ru') {
					$model = IntegratorClinic::model()->findByAttributes(array('testpuls_id' => $value['clinic_id']));
					if (!empty($model->id)) {
						Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
					} else {
						$model = IntegratorClinic::model()->findByAttributes(array('title' => $value['clinic_name']));
						if (!empty($model->id)) {
							Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
						}
					}
				}
				if ($value['domain'] == 'timetovisit.ru') {
					$model = IntegratorClinic::model()->findByAttributes(array('timetovisit_id' => $value['clinic_id']));
					if (!empty($model->id)) {
						Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
					} else {
						$model = IntegratorClinic::model()->findByAttributes(array('title' => $value['clinic_name']));
						if (!empty($model->id)) {
							Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
						}
					}
				}
				if ($value['domain'] == 'fromed.ru') {
					$model = IntegratorClinic::model()->findByAttributes(array('fromed_id' => $value['clinic_id']));
					if (!empty($model->id)) {
						Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
					} else {
						$model = IntegratorClinic::model()->findByAttributes(array('title' => $value['clinic_name']));
						if (!empty($model->id)) {
							Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
						}
					}
				}
			}
		}
	}

	public function cronByRecordPriceOld($time, $timePast)
	{
		set_time_limit(0);
		$data=Yii::app()->db->createCommand('SELECT id FROM integrator_clinic')->queryAll();
		if(!empty($data)){
			foreach ($data as $key=>$value){
				$record = Yii::app()->db->createCommand('SELECT COUNT(*) FROM api_record  WHERE book_id='.$value['id'].' AND status IN(2,3,4,5,52,11) AND reception_time LIKE "%'.$timePast.'%"')->queryScalar();
				$come = Yii::app()->db->createCommand('SELECT COUNT(*) FROM api_record  WHERE book_id='.$value['id'].' AND status IN(3,5,52,11) AND reception_time LIKE "%'.$timePast.'%"')->queryScalar();
				$repeat = Yii::app()->db->createCommand('SELECT COUNT(*) FROM api_record  WHERE book_id='.$value['id'].' AND status IN(3,5,52,11) AND reception_time LIKE "%'.$timePast.'%" AND koll_repeat=1')->queryScalar();
				$diagnostika = Yii::app()->db->createCommand('SELECT COUNT(*) FROM api_record  WHERE book_id='.$value['id'].' AND status IN(3,5,52,11) AND reception_time LIKE "%'.$timePast.'%" AND status_record=2')->queryScalar();
				if(!empty($record)){
					$price = new IntegratorClinicPrice;
					$price->koll_record = $record;
					$price->koll_come = $come;
					$price->koll_repeat = $repeat;
					$price->koll_diagnostik = $diagnostika;
					$price->status = 1;
					$price->create_time = date($timePast.'-01');
					$price->date_status_1 = date($time.'-01');
					$price->integrator_id=$value['id'];
					$price->save();
				}
			}

		}
	}

	public function cronByRecordPrice($time, $timePast)
	{
		set_time_limit(0);
		//$data = Yii::app()->db->createCommand('SELECT ic.all_id, ic.id FROM integrator_clinic AS ic INNER JOIN integrator_clinic_all AS ica ON ica.id = ic.all_id WHERE ic.all_id IN (832) ORDER BY ic.all_id DESC LIMIT 40')->queryAll();
		$data = Yii::app()->db->createCommand('SELECT ic.all_id, ic.id, ica.duplicate FROM integrator_clinic AS ic INNER JOIN integrator_clinic_all AS ica ON ica.id = ic.all_id')->queryAll();
		if( ! empty($data)) {
			$law = array();
			foreach ($data as $key => $value){
				$law[$value['all_id']][$value['id']]['patient_record'] = Yii::app()->db->createCommand("
					SELECT COUNT(*) FROM (
						SELECT
							COUNT(ar.id)
						FROM api_record AS ar
						WHERE
							ar.book_id = '" . $value['id'] . "' AND
							ar.status IN(2,3,4,5,52,11) AND
							ar.reception_time LIKE '%" . $timePast . "%'
						GROUP BY IF (" . $value['duplicate'] . " = '1', CONCAT(ar.name, DATE_FORMAT(ar.create_time, '%Y-%m-%d')), ar.id)
					) AS count")->queryScalar();
				$law[$value['all_id']][$value['id']]['patient_count'] = Yii::app()->db->createCommand("
					SELECT COUNT(*) FROM (
						SELECT
							COUNT(ar.id)
						FROM api_record AS ar
						WHERE
							ar.book_id = '" . $value['id'] . "' AND
							ar.status IN(3,5,52,11) AND
							ar.reception_time LIKE '%" . $timePast . "%'
						GROUP BY IF (" . $value['duplicate'] . " = '1', CONCAT(ar.name, DATE_FORMAT(ar.create_time, '%Y-%m-%d')), ar.id)
					) AS count")->queryScalar();
				$law[$value['all_id']][$value['id']]['patient_repeat'] = Yii::app()->db->createCommand("
					SELECT COUNT(*) FROM (
						SELECT
							COUNT(ar.id)
						FROM api_record AS ar
						WHERE
							ar.book_id = '" . $value['id'] . "' AND
							ar.status IN(3,5,52,11) AND
							ar.reception_time LIKE '%" . $timePast . "%' AND
							koll_repeat = '1'
						GROUP BY IF (" . $value['duplicate'] . " = '1', CONCAT(ar.name, DATE_FORMAT(ar.create_time, '%Y-%m-%d')), ar.id)
					) AS count")->queryScalar();
				$law[$value['all_id']][$value['id']]['patient_diag'] = Yii::app()->db->createCommand("
					SELECT COUNT(*) FROM (
						SELECT
							COUNT(ar.id)
						FROM api_record AS ar
						WHERE
							ar.book_id = '" . $value['id'] . "' AND
							ar.status IN(3,5,52,11) AND
							ar.reception_time LIKE '%" . $timePast . "%' AND
							record_type = '2'
						GROUP BY IF (" . $value['duplicate'] . " = '1', CONCAT(ar.name, DATE_FORMAT(ar.create_time, '%Y-%m-%d')), ar.id)
					) AS count")->queryScalar();
			}
			foreach ($law as $key => $value) {
				$law[$key]['patient_record'] = 0;
				$law[$key]['patient_count'] = 0;
				$law[$key]['patient_repeat'] = 0;
				$law[$key]['patient_diag'] = 0;
				foreach ($value as $key2 => $value2) {
					$law[$key]['patient_record'] += $value2['patient_record'];
					$law[$key]['patient_count'] += $value2['patient_count'];
					$law[$key]['patient_repeat'] += $value2['patient_repeat'];
					$law[$key]['patient_diag'] += $value2['patient_diag'];
				}
				$modelLawReports = new IntegratorLawReports();
				$modelLawReports->law_id = $key;
				$modelLawReports->status = $modelLawReports::STATUS_REPORT;
				$modelLawReports->create_date = $time.'-01';
				$modelLawReports->report_date = $timePast."-01";
				$modelLawReports->patient_record = $law[$key]['patient_record'];
				$modelLawReports->patient_count = $law[$key]['patient_count'];
				$modelLawReports->patient_repeat = $law[$key]['patient_repeat'];
				$modelLawReports->patient_diag = $law[$key]['patient_diag'];
				if ($modelLawReports->save()) {
					$modelLawReportsStatus = new IntegratorLawReportsStatus();
					$modelLawReportsStatus->report_id = $modelLawReports->getPrimaryKey();
					$modelLawReportsStatus->status = $modelLawReports::STATUS_REPORT;
					$modelLawReportsStatus->status_date = $time."-01";
					$modelLawReportsStatus->status_summ = 0;
					if ($modelLawReportsStatus->save()) {
						foreach ($value as $key2 => $value2) {
							$modelClinicsReports = new IntegratorClinicsReports();
							$modelClinicsReports->clinic_id = $key2;
							$modelClinicsReports->law_report_id = $modelLawReports->getPrimaryKey();
							$modelClinicsReports->patient_record = $value2['patient_record'];
							$modelClinicsReports->patient_count = $value2['patient_count'];
							$modelClinicsReports->patient_repeat = $value2['patient_repeat'];
							$modelClinicsReports->patient_diag = $value2['patient_diag'];
							$modelClinicsReports->save();
						}
					}
				}
			}
		}
	}

	public function cronUpdateDebtOld($time)
	{
		set_time_limit(0);
		$time = $time.'-01';
		$data = Yii::app()->db->createCommand("SELECT * FROM integrator_clinic_price WHERE DATE_FORMAT(date_status_3, '%Y-%m-%d') < '".$time."' AND status = 3")->queryAll();
		if ( ! empty($data)) {
			foreach ($data as $key => $value) {
				$model = IntegratorClinicPrice::model()->findByPk($value['id']);
				$model->status = 6;
				$model->date_status_6 = $time;
				$model->save();
			}
		}
	}

	public function cronUpdateDebt($time)
	{
		set_time_limit(0);
		$time = $time.'-01';
		$data = Yii::app()->db->createCommand("
				SELECT ilr.id AS id FROM integrator_law_reports AS ilr
				LEFT JOIN integrator_law_reports_status AS ilrs ON (ilrs.report_id = ilr.id AND ilrs.status = ilr.status)
				WHERE ilr.status NOT IN (".implode(',', array(IntegratorLawReports::STATUS_PAY, IntegratorLawReports::STATUS_DECLINE, IntegratorLawReports::STATUS_DEBT)).")
				AND ilr.status IS NOT NULL
				AND DATE_FORMAT(ilrs.status_date, '%Y-%m-%d') < :time
				GROUP BY ilr.id")
			->queryAll(TRUE, array(
				':time' => $time
			));
		if ( ! empty($data)) {
			foreach ($data as $key => $value) {
				$model = IntegratorLawReports::model()->findByPk($value['id']);
				$model->status = $model::STATUS_DEBT;
				if ($model->save()) {
					$modelReportSatus = new IntegratorLawReportsStatus();
					$modelReportSatus->report_id = $value['id'];
					$modelReportSatus->status = $model::STATUS_DEBT;
					$modelReportSatus->status_date = $time;
					$modelReportSatus->save();
				}
			}
		}
	}
}