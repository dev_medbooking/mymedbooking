<?php

class UploadPodmoskovieReasonCommand extends CConsoleCommand
{
    private $status=1;
    private $reason="Подмосковье - нечего предложить";
    private $report_mail = "mt@medbooking.com;vr@medbooking.com";

    public function run($args)
    {
        date_default_timezone_set("Europe/Moscow");
        setlocale(LC_ALL, "ru_RU.UTF-8");

        $headers = "From: REPORT <REPORT>\r\n".
            "MIME-Version: 1.0" . "\r\n" .
            "Content-type: text/html; charset=UTF-8" . "\r\n";

        $timeto = new DateTime();
        $timeto->sub(new DateInterval('P1D'));
        $timeto = $timeto->format('Y-m-d 23:59:59');
        $timefrom = new DateTime();
        $timefrom->sub(new DateInterval('P7D'));
        $timefrom = $timefrom->format('Y-m-d 23:59:59');

        $body = "Отчет за период с ".$timefrom." по ".$timeto."<br>";
        $body.= "<html><body><table border='1'><tr><td>#</td><td>ID записи</td><td>Дата создания</td><td>Комментарий</td><td>Клиника</td><td>Специальность</td></tr>";

        $sql="select id,create_time,note,clinic_name,category_name from api_record where status=".$this->status." and reason='".$this->reason."' and create_time >='".$timefrom."' and create_time <='".$timeto."' order by create_time";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $i=1;
        foreach ($data as $value) {
            $body.= "<tr><td>".$i."</td><td>".$value['id']."</td><td>".$value['create_time']."</td><td>".$value['note']."</td><td>".$value['clinic_name']."</td><td>".$value['category_name']."</td></tr>";
            $i++;
        }
        $body.= "</table></body></html>";
        mail($this->report_mail,'Отчет по отказам Подмосковье - нечего предложить', $body,$headers);
    }
}