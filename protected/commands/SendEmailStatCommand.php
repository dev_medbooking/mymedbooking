<?php
class SendEmailStatCommand extends CConsoleCommand
{
    public function run($args)
    {
        date_default_timezone_set("Europe/Moscow");
        setlocale(LC_ALL, "ru_RU.UTF-8");

        $dayParam = new DateTime();
        $dayParam->sub(new DateInterval('P1D'));
        $dayParam = $dayParam->format('Y-m-d');
        $leftDateParam = $dayParam . ' 00:00:00';
        $rightDateParam = $dayParam . ' 23:59:59';

        $currentDay = new DateTime();
        $currentDay->sub(new DateInterval('P1D'));
        $currentDay = $currentDay->format('d-m-Y');

        $subscribers = Yii::app()->params['newRecordsSubs'];
        $subscribers2 = Yii::app()->params['newRecordsSubs2'];
        if (empty($subscribers)) {
            return;
        }
        if (is_array($subscribers) == false) {
            $subscribers = array($subscribers);
        }
        $domains = MbDomain::model()->findAll();
        if (empty($domains) || is_array($domains) == false || count($domains) == 0) {
            return;
        }
        $mailBody = array(
            'domains' => array(),
            'totals' => array(
                'new' => 0,
                'confirmed' => 0,
                'call' => 0,
                'in' => 0,
            ),
            'money' => array()
        );
        foreach ($domains as $domain) {
            $recordsNew = Yii::app()->db->createCommand("SELECT * FROM api_record WHERE (deleted IS NULL OR deleted=0) AND (status!=8 AND status!=9 AND status!=10) AND domain='" . $domain->title . "' AND create_time between '" . $leftDateParam . "' AND '" . $rightDateParam . "' ORDER BY create_time, name")->queryAll();
            $recordsConfirmed = Yii::app()->db->createCommand("SELECT * FROM api_record WHERE (deleted IS NULL OR deleted=0) AND domain='" . $domain->title . "' AND create_time between '" . $leftDateParam . "' AND '" . $rightDateParam . "' AND (status=5 or status=3 or status=2 or status=52 or status=11) ORDER BY create_time, name")->queryAll();
            $recordsCallReq = Yii::app()->db->createCommand("SELECT * FROM api_call WHERE domain='" . $domain->title . "' AND create_time between '" . $leftDateParam . "' AND '" . $rightDateParam . "' ORDER BY create_time, name")->queryAll();
            $recordsInReq = Yii::app()->db->createCommand("SELECT * FROM api_record WHERE (deleted IS NULL OR deleted=0) AND domain='" . $domain->title . "' AND lead_time between '" . $leftDateParam . "' AND '" . $rightDateParam . "' AND status=3 ORDER BY lead_time, name")->queryAll();
            if (empty($recordsNew) && empty($recordsConfirmed) && empty($recordsCallReq) && empty($recordsInReq)) {
                continue;
            }
            array_push($mailBody['domains'], array(
                'title' => $domain->title,
                'records' => array(
                    'new' => count($recordsNew),
                    'confirmed' => count($recordsConfirmed),
                    'call' => count($recordsCallReq),
                    'in' => count($recordsInReq),
                )
            ));
            $mailBody['totals']['new'] += count($recordsNew);
            $mailBody['totals']['confirmed'] += count($recordsConfirmed);
            $mailBody['totals']['call'] += count($recordsCallReq);
            $mailBody['totals']['in'] += count($recordsInReq);
        }

        $time = new DateTime();
        $time->sub(new DateInterval('P1D'));
        $day = $time->format('Y-m-d');
        $todayPayments = Yii::app()->db->createCommand("SELECT IFNULL(SUM(summ), 0) AS summ FROM integrator_law_reports_payments WHERE DATE_FORMAT(payment_date, '%Y-%m-%d') = '".$day."'")->queryScalar();
        $todayPaymentsDebt = Yii::app()->db->createCommand("SELECT IFNULL(SUM(summ), 0) AS summ FROM integrator_law_reports_payments WHERE DATE_FORMAT(payment_date, '%Y-%m-%d') = '".$day."' AND payment_type = '".IntegratorLawReportsPayments::PAYMENT_DEBT."'")->queryScalar();
        $todayReportSumm = Yii::app()->db->createCommand("
				SELECT IFNULL(SUM(law_reports.summ), 0) AS summ FROM
				integrator_clinic_all AS t
				LEFT JOIN integrator_law_reports AS law_reports ON (law_reports.law_id=t.id)
				LEFT JOIN integrator_law_reports_status AS law_report_status ON (law_report_status.report_id=law_reports.id)
				LEFT OUTER JOIN auth_user AS iu ON (t.uid=iu.uid)
				WHERE (((law_reports.status = '".IntegratorLawReports::STATUS_BILL."')
				AND (law_report_status.status = '".IntegratorLawReports::STATUS_BILL."'))
				AND (DATE_FORMAT(law_report_status.status_date, '%Y-%m-%d') = '".$day."'))")->queryScalar();

        $todayDebtSumm = Yii::app()->db->createCommand("
				SELECT IFNULL(SUM(law_reports.summ), 0) AS summ FROM
				integrator_clinic_all AS t
				LEFT JOIN integrator_law_reports AS law_reports ON (law_reports.law_id=t.id)
				LEFT JOIN integrator_law_reports_status AS law_report_status ON (law_report_status.report_id=law_reports.id)
				LEFT OUTER JOIN auth_user AS iu ON (t.uid=iu.uid)
				WHERE (((law_reports.status = '".IntegratorLawReports::STATUS_DEBT."')
				AND (law_report_status.status = '".IntegratorLawReports::STATUS_DEBT."'))
				AND (DATE_FORMAT(law_report_status.status_date, '%Y-%m-%d') < '".$day."'))")->queryScalar();

        $mailBody['money']['Сумма собранных денег за день'] = $todayPayments." руб.";
        $mailBody['money']['Сумма выставленных счетов за день'] = $todayReportSumm." руб.";

        $partnerStat = new Partner();
        $partnerStat->admin = TRUE;
        $partnerStat->startDate = $dayParam;
        $partnerStat->endDate = $dayParam;
        $data['detailPartner'] = $partnerStat->getDetailRecordInfo();
        $partnerStat = $partnerStat->getRecordInfo();

        $mailBody['partner'] = 'Всего записей от партнеров: '.( ! empty($partnerStat['all_record']) ? $partnerStat['all_record'] : 0).
            " из них к оплате ".( ! empty($partnerStat['success_record']) ? $partnerStat['success_record'] : 0).
            " на сумму ".( ! empty($partnerStat['price']) ? $partnerStat['price'] : 0)." руб.";

        $oktell = Yii::app()->db->createCommand("SELECT oktell FROM api_settings WHERE 1 ORDER BY id DESC")->queryScalar();
        $subject = 'Сервис онлайн записи, ' . $currentDay . ', отчет по записям';
        $header = 'Отчет по записям на ' . $currentDay;

        // Рендер для newRecordsSubs
        $mail = new YiiMailer('records2', array('mailBody' => $mailBody, 'title' => 'Сервис онлайн записи', 'header' => $header, 'oktell' => $oktell));
        $mail->render();
        $mail->From = Yii::app()->params['fromEmailAddress'];
        $mail->FromName = Yii::app()->params['fromName'];
        $mail->Subject = $subject;
        foreach ($subscribers as $subscriber) {
            if ( ! in_array($subscriber, $subscribers2)) $mail->AddAddress($subscriber);
        }
        if ($mail->Send()) {
            $mail->ClearAddresses();
        }

        // Рендер для newRecordsSubs2
        $mailBody['money']['Сумма оплат по дебиторской за день'] = $todayPaymentsDebt." руб.";
        $mailBody['money']['Сумма дебиторской задолженности по всем счетам'] = $todayDebtSumm." руб.";
        $mail = new YiiMailer('recordsSubs2', array('mailBody' => $mailBody, 'title' => 'Сервис онлайн записи', 'header' => $header, 'oktell' => $oktell, 'data' => $data));
        $mail->render();
        $mail->From = Yii::app()->params['fromEmailAddress'];
        $mail->FromName = Yii::app()->params['fromName'];
        $mail->Subject = $subject;
        foreach ($subscribers as $subscriber) {
            if (in_array($subscriber, $subscribers2)) $mail->AddAddress($subscriber);
        }
        if ($mail->Send()) {
            $mail->ClearAddresses();
        }
    }
}