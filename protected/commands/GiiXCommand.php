<?php
Yii::import('application.components.redefined.CCodeFile', true);
Yii::import('application.components.redefined.ModelCode', true);
Yii::import('ext.giix-core.giixModelNew.*');
Yii::import('ext.giix-components.*');
Yii::import('ext.giix-components.components.*');
Yii::import('system.gii.*');
Yii::import('ext.giix-components.components.TextDiff', true);


class GiiXCommand extends CConsoleCommand
{
    public $template = "ext.giix-core.giixModelNew.templates.default";

    public function actionIndex($table = null)
    {
        /**
         * @var CCodeFile     $file
         * @var GiixModelCode $code
         */
        $paths = [//            "application.models" => ["partner"],
        ];
        if (!$table) {
            $schema = Yii::app()->db->getSchema();
            $tables = $schema->tableNames;
        } else {
            $tables = explode(',', $table);
        }
        $exception = ["tbl_migration", "email_sender_log", "email_sender_files"];
        foreach (array_diff($tables, $exception) as $table) {
            echo "$table\n";
            $code = new GiixModelCode();
            $code->tableName = $table;
            $code->baseClass = 'GxActiveRecord';
            $code->template = 'default';
            $code->modelPath = null;
            foreach ($paths as $path => $partials) {
                foreach ($partials as $partial) {
                    if (strpos($table, $partial) !== false) {
                        $code->modelPath = $path;
                        break;
                    }
                }
                if ($code->modelPath) {
                    break;
                }
            }
            if (!$code->modelPath) {
                $code->modelPath = 'application.models';
            }
            $code->_templates = ["default" => Yii::getPathOfAlias($this->template)];
            $code->prepare();
            foreach ($code->files as $file) {
                try {
                    if ($file->operation != CCodeFile::OP_SKIP) {
                        if ($file->operation == CCodeFile::OP_OVERWRITE) {
                            $diff = TextDiff::compare(file_get_contents($file->path), $file->content);
                        } else {
                            $diff = true;
                        }
                        if ($diff) {
                            echo "\t{$file->relativePath} - generated\n";
                            $file->save();
                        }
                    }
                } catch (Exception $e) {
                }
            }
        }
    }
}