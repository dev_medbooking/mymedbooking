<?php

class EmptyCallsWeekCommand extends CConsoleCommand
{
    private $report_mail = "vr@medbooking.com";

    public function run($args)
    {
        date_default_timezone_set("Europe/Moscow");
        setlocale(LC_ALL, "ru_RU.UTF-8");

        $headers = "From: REPORT <REPORT>\r\n".
            "MIME-Version: 1.0" . "\r\n" .
            "Content-type: text/html; charset=UTF-8" . "\r\n";

        $skip_symbols=array("+","(",")","-");
        $phone_array_vh=array();
        $phone_array_ish=array();
        $sum_duration_vh=0;
        $sum_billsec_vh=0;
        $sum_duration_ish=0;
        $sum_billsec_ish=0;

        /*$time = new DateTime();
        $time_print = $time->format('H:i:s d.m.Y');
        $time = $time->format('Y-m-d');

        $timePast = new DateTime();
        $timePast->sub(new DateInterval('P1D'));
        $timePast = $timePast->format('Y-m-d');*/

        $dateStart='2016-03-01';
        $dateEnd='2016-04-01';

        $body = "Отчет за период с ".$dateStart." по ".$dateEnd."<br>";
        $body.= "<html><body><table border='1'><tr><td>Входящие, время звонка (ч)</td><td>Входящие, время разговора (ч)</td><td>Исходящие, время звонка (ч)</td><td>Исходящие, время разговора (ч)</td></tr>";

        $sql="select telephone,phone_record from api_record where status=9 and update_time>='".$dateStart."' AND update_time<'".$dateEnd."'";
        echo $sql."\n\n";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($data as $value) {
            $phone_record="";
            $telephone=$value['telephone'];
            if (strlen($telephone)==10)
                $telephone="7".$telephone;
            if (isset($value['phone_record'])) {
                $phone_record = str_replace($skip_symbols,'',$value['phone_record']);
                if (strlen($phone_record)==10)
                    $phone_record="7".$phone_record;
            }

            //входящие
            if (!in_array($telephone,$phone_array_vh)) {
                $data_call=$this->get_calls($telephone,$dateStart,$dateEnd);
                $sum_duration_vh+=$data_call['duration'];
                $sum_billsec_vh+=$data_call['billsec'];
                $phone_array_vh[]=$telephone;
            }

            if ($phone_record && !in_array($phone_record,$phone_array_vh)) {
                $data_call=$this->get_calls($phone_record,$dateStart,$dateEnd);
                $sum_duration_vh+=$data_call['duration'];
                $sum_billsec_vh+=$data_call['billsec'];
                $phone_array_vh[]=$phone_record;
            }

            //исходящие
            if (!in_array($telephone,$phone_array_ish)) {
                $data_call=$this->get_calls($telephone,$dateStart,$dateEnd,'dst');
                $sum_duration_ish+=$data_call['duration'];
                $sum_billsec_ish+=$data_call['billsec'];
                $phone_array_ish[]=$telephone;
            }

            if ($phone_record && !in_array($phone_record,$phone_array_ish)) {
                $data_call=$this->get_calls($phone_record,$dateStart,$dateEnd,'dst');
                $sum_duration_ish+=$data_call['duration'];
                $sum_billsec_ish+=$data_call['billsec'];
                $phone_array_ish[]=$phone_record;
            }
        }
        $body.= "<tr><td>".(sprintf("%6.2f",$sum_duration_vh/3600))."</td><td>".(sprintf("%6.2f",$sum_billsec_vh/3600))."</td><td>".(sprintf("%6.2f",$sum_duration_ish/3600))."</td><td>".(sprintf("%6.2f",$sum_billsec_ish/3600))."</td></tr>";
        $body.= "</table></body></html>";
        mail($this->report_mail,'Отчет по пустым звонкам', $body,$headers);
    }

    private function get_calls($phone,$dateStart,$dateEnd,$source="src") {
        $sql_ast="select sum(duration) as duration,sum(billsec) as billsec from cdr2 where ".$source."='".$phone."' AND start>='".$dateStart."' AND start<'".$dateEnd."'";
        $data_call = Yii::app()->db_ast->createCommand($sql_ast)->queryRow();
        echo $sql_ast."\n";
        return $data_call;
    }
}