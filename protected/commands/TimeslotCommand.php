<?php

class TimeslotCommand extends CConsoleCommand
{

    private function hourSender()
    {
        $data=Yii::app()->db->createCommand("SELECT * FROM api_record WHERE hour_send_status=0 AND STATUS=5 AND UNIX_TIMESTAMP(reception_time) BETWEEN ( UNIX_TIMESTAMP( NOW( ) ) -3600 ) AND ( UNIX_TIMESTAMP( NOW( ) ) +3600 )")->queryAll();
        if(!empty($data)){
            foreach($data as $value){
                $template=MbSmsTemplate::model()->findByAttributes(array('title'=>'VISIT_NOTIFY_2H.ftl'));
                if(!empty($template->id)){
                    $model=MbRecordSms::model()->findByAttributes(array('t_id'=>$template->id,'record_id'=>$value['id']));
                    if(empty($model->id)){
                        $smsModel=new MbRecordSms;
                        $smsModel->sms_send_admin=true;
                        //$smsModel->test_mode=true;
                        $smsModel->cron_sender=1;
                        $smsModel->message=$template->message;
                        $smsModel->record_id=$value['id'];
                        $smsModel->t_id=$template->id;
                        if($smsModel->save()){
                            Yii::app()->db->createCommand("UPDATE api_record SET hour_send_status=1 WHERE id=".$value['id'])->execute();
                        } else{
                            $errors=CActiveForm::validate($smsModel);
                        }
                    }
                }
            }
        }
    }

    private function notifySender()
    {
        $data=Yii::app()->db->createCommand("SELECT * FROM api_record WHERE day_send_status=0 AND (status=5 OR status=3) AND UNIX_TIMESTAMP(reception_time) BETWEEN (UNIX_TIMESTAMP(NOW())-7200) AND (UNIX_TIMESTAMP(NOW())-3600)")->queryAll();
        if(!empty($data)){
            foreach($data as $value){
                $template=MbSmsTemplate::model()->findByAttributes(array('title'=>'REVIEW_NOTIFY.ftl'));
                if(!empty($template->id)){
                    $model=MbRecordSms::model()->findByAttributes(array('t_id'=>$template->id,'record_id'=>$value['id']));
                    if(empty($model->id)){
                        $smsModel=new MbRecordSms;
                        $smsModel->sms_send_admin=true;
                        //$smsModel->test_mode=true;
                        $smsModel->cron_sender=1;
                        $smsModel->message=$template->message;
                        $smsModel->record_id=$value['id'];
                        $smsModel->t_id=$template->id;
                        if($smsModel->save()){
                            Yii::app()->db->createCommand("UPDATE api_record SET day_send_status=1 WHERE id=".$value['id'])->execute();
                        } else{
                            $errors=CActiveForm::validate($smsModel);
                        }
                    }
                }
            }
        }
    }

    public function run($args)
    {
        $this->hourSender();
        $this->notifySender();
    }

}