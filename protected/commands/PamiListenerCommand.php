<?php
//error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
require(implode(DIRECTORY_SEPARATOR, array(
    __DIR__,
    '..',
    'vendor',
    'autoload.php'
)));
use PAMI\Client\Impl\ClientImpl;
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\EventMessage;
//use PAMI\Message\Action\VGMSMSTxAction;

class PamiListenerCommand extends CConsoleCommand
{

    public function run($args)
    {
        try
        {
            $listener = new ClientImpl(Yii::app()->params['asterisk_listener']);
            // Registering a closure
            //$client->registerEventListener(function ($event) {
            //});
            // Register a specific method of an object for event listening
            //$client->registerEventListener(array($listener, 'handle'));
            // Register an IEventListener:
            $listener->registerEventListener(new PamiEventListener());
            $listener->open();
            $time=time();

            /*$sms = new VGMSMSTxAction();
            $sms->setContentType('text/plain; charset=ASCII');
            $sms->setContent("test listener");
            $sms->setCellPhone("79265565305");
            $listener->send($sms);*/
            while(true)//(time() - $time) < 60) // Wait for events.
            {
                usleep(1000); // 1ms delay
                // Since we declare(ticks=1) at the top, the following line is not necessary
                $listener->process();
            }
            $listener->close(); // send logoff and close the connection.
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }
}