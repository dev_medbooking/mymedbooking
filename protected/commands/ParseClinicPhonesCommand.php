<?php
/**
 * наполняем таблицу астерикс.redir_clinic_to_account
 *
 * разбираются все Юр. лица (CRM.integrator_clinic_all) с привязанными к ним Аккаунт-менеджерами (uid);
 * для них подтягиваются Клиники (по CRM.integrator_clinic связываются с MB.clinic);
 * парсятся телефоны из CRM.integrator_clinic_all.name и MB.clinic.telephone;
 * для Аккаунтов из CRM.auth_user достаётся телефон;
 *
 * полученные данные складываются в астерикс, поле clinic_id заполняется номером Юр. лица.
 */

class ParseClinicPhonesCommand extends CConsoleCommand
{
    public function run($args)
    {
        $values = [];
        $all_clinics = IntegratorClinicAll::model()->with('clinics')->findAll();
        foreach ($all_clinics as $all_clinic) {
            $clinic_id = $all_clinic->id;
            $account_id = $all_clinic->uid;
            $account_phone = $this->getAccountPhone($account_id);
            if (empty($account_phone)) {
                continue;
            }
            $clinic_phones = CPhone::parsePhoneString($all_clinic->name);
            foreach ($all_clinic->clinics as $clinic) {
                $mb_clinic = MedbookingClinic::model()->findByPk($clinic->medbooking_id);
                if ($mb_clinic) {
                    $clinic_phones = array_merge($clinic_phones, CPhone::parsePhoneString($mb_clinic->telephone));
                }
            }
            $clinic_phones = array_unique($clinic_phones);
            foreach ($clinic_phones as $clinic_phone) {
                $values[] = "('{$clinic_phone}', '{$clinic_id}', '{$account_phone}', '{$account_id}', '', '0000-00-00 00:00:00', 0)";
            }
        }
        /* закроем всё */
        Yii::app()->db_ast->createCommand()
            ->update('redir_clinic_to_account', ['disabled' => 1]);
        /* проставим актуальное */
        if (!empty($values)) {
            $query = "
            INSERT INTO `redir_clinic_to_account` (
              `clinic_phone`,
              `clinic_id`,
              `account_phone`,
              `account_id`,
              `operator_phone`,
              `last_call_time`,
              `disabled`
            )
            VALUES
            " . implode(",\n", $values). "
            ON DUPLICATE KEY UPDATE
              `account_phone` = VALUES(`account_phone`),
              `account_id` = VALUES(`account_id`),
              `operator_phone` = IF(`disabled`, VALUES(`operator_phone`), `operator_phone`),
              `last_call_time` = IF(`disabled`, VALUES(`last_call_time`), `last_call_time`),
              `disabled` = VALUES(`disabled`)
            ";
            Yii::app()->db_ast->createCommand($query)->execute();
        }
    }

    protected function getAccountPhone($uid)
    {
        $uids_disabled = [
            7839, // Ольга Зайцева
        ];

        if (in_array($uid, $uids_disabled)) {
            return false;
        }

        static $phones = [];
        if (!isset($phones[$uid])) {
            $user = MbUser::model()->findByPk($uid);
            if ($user) {
                $phones[$uid] = CPhone::normalizePhone($user->telephone);
            } else {
                $phones[$uid] = false;
            }
        }
        return $phones[$uid];
    }
}