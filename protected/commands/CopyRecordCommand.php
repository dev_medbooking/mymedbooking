<?php

class CopyRecordCommand extends CConsoleCommand
{
    public function ActionCopyJan()
    {
        $records = Yii::app()->db->createCommand('select * from api_record where create_time between "2017-01-01 00:00:00" and "2017-01-31 23:59:59"')->queryAll();
        foreach ($records as $record) {
            $new_record = new ApiRecord();
            $rnd = rand(4, 6);
            foreach ($record as $key => $item) {
                if ($key != 'id') {
                    if ($item != null) {
                        $new_record->{$key} = $item;
                    }
                }
                if ($key == 'uid') {
                    $new_record->{$key} = '8091';
                }

                if ($key == 'create_time') {
                    $new_record->{$key} = date('Y-m-d H:i:s', strtotime($item . ' +' . $rnd . ' month'));
                }
                if ($key == 'update_time') {
                    $new_record->{$key} = date('Y-m-d H:i:s', strtotime($item . ' +' . $rnd . ' month'));
                }
                if ($key == 'first_time_status') {
                    $new_record->{$key} = date('Y-m-d H:i:s', strtotime($item . ' +' . $rnd . ' month'));
                }

            }
            if (!$new_record->save(false)) {
                print_r($new_record->errors);
            }

        }
    }

    public function ActionCronAprilCopy()
    {
        if (date('Y-m-d H:i:s') < '2017-09-01 00:00:00') {
            $cur_april_date = date('Y-m-d', strtotime(date('Y-m-d') . ' -4 month'));
            $max_id_call = Yii::app()->db->createCommand('select max(id_call) as max from api_record')->queryScalar();
            $max_id_call = !empty($max_id_call) ? $max_id_call : 0;

            $records = Yii::app()->db->createCommand('select * from api_record where (create_time BETWEEN "' . $cur_april_date . ' 00:00:00" and "' . $cur_april_date . ' ' . date('H:i:s') . '") and (id > ' . $max_id_call . ')')->queryAll();
            foreach ($records as $record) {
                $new_record = new ApiRecord();
                foreach ($record as $key => $item) {
                    if ($key != 'id') {
                        if ($item != null) {
                            $new_record->{$key} = $item;
                        }
                    }
                    if ($key == 'uid') {
                        $new_record->{$key} = '8091';
                    }
                    if ($key == 'create_time') {
                        $new_record->{$key} = date('Y-m-d') . ' ' . date('H:i:s', strtotime($item));
                    }
                    if ($key == 'update_time') {
                        $new_record->{$key} = date('Y-m-d') . ' ' . date('H:i:s', strtotime($item));
                    }
                    if ($key == 'first_time_status') {
                        $new_record->{$key} = date('Y-m-d') . ' ' . date('H:i:s', strtotime($item));
                    }
                    if ($key == 'id_call') {
                        $new_record->{$key} = $record['id'];
                    }


                }
                if (!$new_record->save(false)) {
                    print_r($new_record->errors);
                }
            }
        }
    }

    public function ActionGenerate()
    {
        $data = [
//            '349' => [
//                [
//                    'year' => 2017,
//                    'month' => 12,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 6,
//                    'countUnactive' => 6,
//                    'countVisits' => 674
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 1,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 13,
//                    'countUnactive' => 8,
//                    'countVisits' => 1173
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 2,
//                    'fromDay' => 1,
//                    'toDay' => 28,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 29,
//                    'countUnactive' => 25,
//                    'countVisits' => 2561
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 3,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 47,
//                    'countUnactive' => 31,
//                    'countVisits' => 3256
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 4,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 70,
//                    'countUnactive' => 56,
//                    'countVisits' => 4589
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 5,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 58,
//                    'countUnactive' => 43,
//                    'countVisits' => 3892
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 6,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 89,
//                    'countUnactive' => 71,
//                    'countVisits' => 5678
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 7,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 134,
//                    'countUnactive' => 76,
//                    'countVisits' => 6891
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 8,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 312,
//                    'countUnactive' => 188,
//                    'countVisits' => 15432
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 9,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 499,
//                    'countUnactive' => 251,
//                    'countVisits' => 23148
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 10,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 699,
//                    'countUnactive' => 276,
//                    'countVisits' => 34722
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 11,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 978,
//                    'countUnactive' => 484,
//                    'countVisits' => 52083
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 12,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 1565,
//                    'countUnactive' => 628,
//                    'countVisits' => 78125
//                ],
//                [
//                    'year' => 2019,
//                    'month' => 1,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 1581,
//                    'countUnactive' => 1271,
//                    'countVisits' => 117187
//                ],
//                [
//                    'year' => 2019,
//                    'month' => 2,
//                    'fromDay' => 1,
//                    'toDay' => 28,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 2214,
//                    'countUnactive' => 1548,
//                    'countVisits' => 175780
//                ],
//                [
//                    'year' => 2019,
//                    'month' => 3,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 2216,
//                    'countUnactive' => 1548,
//                    'countVisits' => 263670
//                ],
//                [
//                    'year' => 2019,
//                    'month' => 4,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 2243,
//                    'countUnactive' => 1973,
//                    'countVisits' => 395505
//                ]
//            ],
//            '350' => [
//                [
//                    'year' => 2018,
//                    'month' => 2,
//                    'fromDay' => 1,
//                    'toDay' => 28,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 5,
//                    'countUnactive' => 4,
//                    'countVisits' => 672
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 3,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 16,
//                    'countUnactive' => 7,
//                    'countVisits' => 1348
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 4,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 28,
//                    'countUnactive' => 16,
//                    'countVisits' => 2137
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 5,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 21,
//                    'countUnactive' => 13,
//                    'countVisits' => 1890
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 6,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 32,
//                    'countUnactive' => 22,
//                    'countVisits' => 2890
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 7,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 62,
//                    'countUnactive' => 36,
//                    'countVisits' => 3890
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 8,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 113,
//                    'countUnactive' => 66,
//                    'countVisits' => 4901
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 9,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 181,
//                    'countUnactive' => 111,
//                    'countVisits' => 9442
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 10,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 289,
//                    'countUnactive' => 198,
//                    'countVisits' => 15107
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 11,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 463,
//                    'countUnactive' => 312,
//                    'countVisits' => 24170
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 12,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 741,
//                    'countUnactive' => 530,
//                    'countVisits' => 38673
//                ],
//                [
//                    'year' => 2019,
//                    'month' => 1,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 1185,
//                    'countUnactive' => 962,
//                    'countVisits' => 46407
//                ],
//                [
//                    'year' => 2019,
//                    'month' => 2,
//                    'fromDay' => 1,
//                    'toDay' => 28,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 1896,
//                    'countUnactive' => 1540,
//                    'countVisits' => 51048
//                ],
//                [
//                    'year' => 2019,
//                    'month' => 3,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 1915,
//                    'countUnactive' => 1864,
//                    'countVisits' => 52222
//                ],
//                [
//                    'year' => 2019,
//                    'month' => 4,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 1917,
//                    'countUnactive' => 1240,
//                    'countVisits' => 52744
//                ]
//            ],
            '351' => [
//                [
//                    'year' => 2018,
//                    'month' => 2,
//                    'fromDay' => 1,
//                    'toDay' => 28,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 12,
//                    'countUnactive' => 2,
//                    'countVisits' => 982
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 3,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 21,
//                    'countUnactive' => 15,
//                    'countVisits' => 1564
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 4,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 44,
//                    'countUnactive' => 19,
//                    'countVisits' => 2671
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 5,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 33,
//                    'countUnactive' => 22,
//                    'countVisits' => 2212
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 6,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 56,
//                    'countUnactive' => 33,
//                    'countVisits' => 3219
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 7,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 69,
//                    'countUnactive' => 55,
//                    'countVisits' => 4123
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 8,
//                    'fromDay' => 1,
//                    'toDay' => 31,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 112,
//                    'countUnactive' => 68,
//                    'countVisits' => 5689
//                ],
//                [
//                    'year' => 2018,
//                    'month' => 9,
//                    'fromDay' => 1,
//                    'toDay' => 30,
//                    'fromHour' => 07,
//                    'toHour' => 20,
//                    'countActive' => 179,
//                    'countUnactive' => 109,
//                    'countVisits' => 9102
//                ],
                [
                    'year' => 2018,
                    'month' => 10,
                    'fromDay' => 1,
                    'toDay' => 31,
                    'fromHour' => 07,
                    'toHour' => 20,
                    'countActive' => 287,
                    'countUnactive' => 186,
                    'countVisits' => 14564
                ],
                [
                    'year' => 2018,
                    'month' => 11,
                    'fromDay' => 1,
                    'toDay' => 30,
                    'fromHour' => 07,
                    'toHour' => 20,
                    'countActive' => 459,
                    'countUnactive' => 250,
                    'countVisits' => 23302
                ],
                [
                    'year' => 2018,
                    'month' => 12,
                    'fromDay' => 1,
                    'toDay' => 31,
                    'fromHour' => 07,
                    'toHour' => 20,
                    'countActive' => 734,
                    'countUnactive' => 470,
                    'countVisits' => 37283
                ],
                [
                    'year' => 2019,
                    'month' => 1,
                    'fromDay' => 1,
                    'toDay' => 31,
                    'fromHour' => 07,
                    'toHour' => 20,
                    'countActive' => 1174,
                    'countUnactive' => 632,
                    'countVisits' => 41012
                ],
                [
                    'year' => 2019,
                    'month' => 2,
                    'fromDay' => 1,
                    'toDay' => 28,
                    'fromHour' => 07,
                    'toHour' => 20,
                    'countActive' => 1879,
                    'countUnactive' => 1012,
                    'countVisits' => 45113
                ],
                [
                    'year' => 2019,
                    'month' => 3,
                    'fromDay' => 1,
                    'toDay' => 31,
                    'fromHour' => 07,
                    'toHour' => 20,
                    'countActive' => 1898,
                    'countUnactive' => 1022,
                    'countVisits' => 49624
                ],
                [
                    'year' => 2019,
                    'month' => 4,
                    'fromDay' => 1,
                    'toDay' => 30,
                    'fromHour' => 07,
                    'toHour' => 20,
                    'countActive' => 1955,
                    'countUnactive' => 994,
                    'countVisits' => 54587
                ]
            ]
        ];
        foreach ($data as $key => $val) {
            foreach ($val as $item){
                $this->generateFake($key, $item['year'], $item['month'], $item['fromDay'], $item['toDay'], $item['fromHour'], $item['toHour'], $item['countActive'], $item['countUnactive'], $item['countVisits']);
            }
        }
    }


    private function generateFake($forUser, $year, $month, $fromDay, $toDay, $fromHour, $toHour, $countActive, $countUnactive, $countVisits)
    {
        $countOther = 0;
        $stop = false;
        $i = 0;
        $modelsToSave = [];
        while (!$stop) {
            $i++;
            srand($i);
            $rndDay = rand($fromDay, $toDay);
            $rndHour = rand($fromHour, $toHour);
            $rndMin = rand(0, 59);
            $rndSec = rand(0, 59);
            $allCount = ($countOther + $countUnactive + $countActive);
            if ($allCount == 0) {
                $stop = true;
                continue;
            }
            $rndStatus = rand(1, $allCount);
            $new_record = new ApiRecord();
            $new_record->gender = 0;
            $new_record->phone_record = '+7(905)584-8775';
            $new_record->uid = 8290;
            $new_record->create_time = $year . '-' . $month . '-' . $rndDay . ' ' . $rndHour . ':' . $rndMin . ':' . $rndSec;
            $new_record->update_time = $year . '-' . $month . '-' . $rndDay . ' ' . $rndHour . ':' . $rndMin . ':' . $rndSec;
            $new_record->status = 9;
            $new_record->status_time = $year . '-' . $month . '-' . $rndDay . ' ' . $rndHour . ':' . $rndMin . ':' . $rndSec;
            $new_record->author_id = 7842;
            $new_record->domain = 'medbooking.com';
            $new_record->domain_phone = '+7(499)705-3999';
            $new_record->source = 'звонок';
            $new_record->note = $year . '-' . $month . '-' . $rndDay . ' ' . $rndHour . ':' . $rndMin . ':' . $rndSec;
            $new_record->ident_id = $forUser;
            $new_record->city_id = 1;
            if ($rndStatus <= $countActive) {
                if ($countActive == 0) {
                    continue;
                }
                $countActive--;
                $new_record->partner_status = 2;
                $modelsToSave[] = $new_record;
                continue;
            }
            if ($rndStatus > $countActive && $rndStatus <= ($countActive + $countUnactive)) {
                if ($countUnactive == 0) {
                    continue;
                }
                $countUnactive--;
                $new_record->partner_status = 3;
                $modelsToSave[] = $new_record;
                continue;
            }
            if ($rndStatus > ($countActive + $countUnactive)) {
                if ($countOther == 0) {
                    continue;
                }
                $countOther--;
                $new_record->partner_status = 4;
                $modelsToSave[] = $new_record;
                continue;
            }
        }
        uasort($modelsToSave, array($this, 'objSort'));
        for ($i=0; $i < $countVisits; $i++){
            srand($i);
            $rndDay = rand($fromDay, $toDay);
            $rndHour = rand($fromHour, $toHour);
            $rndMin = rand(0, 59);
            $rndSec = rand(0, 59);
            $visit = new PartnerVisits();
            $visit->ident_id =$forUser;
            $url = 'mymedbooking.com';
            switch ($forUser) {
                case 349:
                    $url = 'http://doctogo.ru';
                    break;
                case 350:
                    $url = 'http://doctortop.ru';
                    break;
                case 351:
                    $url = 'http://promedicals.ru';
                    break;
            }
            $visit->url = $url;
            $visit->ref = 'mymedbooking.com';
            $visit->date = $year . '-' . $month . '-' . $rndDay . ' ' . $rndHour . ':' . $rndMin . ':' . $rndSec;
            if (!$visit->save(false)) {
                print_r($visit->errors);
            }

        }
        foreach ($modelsToSave as $item) {
            if (!$item->save(false)) {
                print_r($item->errors);
            }
        }
    }

    public function ActionGeneratecron(){//23:59
        $data = [
            '349' => [
                [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'fromDay' => date('d'),
                    'toDay' => date('d'),
                    'fromHour' => 07,
                    'toHour' => 20,
                    'countActive' => rand(8,12),
                    'countUnactive' => rand(4,8),
                ]
            ],
            '350' => [
                [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'fromDay' => date('d'),
                    'toDay' => date('d'),
                    'fromHour' => 07,
                    'toHour' => 20,
                    'countActive' => rand(3,5),
                    'countUnactive' => rand(1,3),
                ]
            ],
            '351' => [
                [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'fromDay' => date('d'),
                    'toDay' => date('d'),
                    'fromHour' => 07,
                    'toHour' => 20,
                    'countActive' => rand(3,5),
                    'countUnactive' => rand(1,3),
                ]
            ]
        ];
        foreach ($data as $key => $val) {
            foreach ($val as $item){
                $this->generateFake($key, $item['year'], $item['month'], $item['fromDay'], $item['toDay'], $item['fromHour'], $item['toHour'], $item['countActive'], $item['countUnactive']);
            }
        }

    }

    private function objSort($f1, $f2)
    {
        if ($f1->create_time < $f2->create_time) return -1;
        elseif ($f1->create_time > $f2->create_time) return 1;
        else return 0;
    }
}