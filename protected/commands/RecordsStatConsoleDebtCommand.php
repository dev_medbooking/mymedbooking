<?php
class RecordsStatConsoleDebtCommand extends CConsoleCommand
{

    public function run($args)
    {
        date_default_timezone_set("Europe/Moscow");
        setlocale(LC_ALL, "ru_RU.UTF-8");

        $time = new DateTime();
        $time = $time->format('Y-m');
        $this->cronUpdateDebt($time);
    }

    public function cronUpdateDebt($time)
    {
        set_time_limit(0);
        $time = $time.'-01';

        $data = Yii::app()->db->createCommand("
				SELECT ilr.id AS id FROM integrator_law_reports AS ilr
				LEFT JOIN integrator_law_reports_status AS ilrs ON (ilrs.report_id = ilr.id AND ilrs.status = ilr.status)
				WHERE ilr.status IN (".implode(',', array(IntegratorLawReports::STATUS_PART, IntegratorLawReports::STATUS_BILL, IntegratorLawReports::STATUS_DEBT)).")
				AND summ>0 AND DATE_FORMAT(ilrs.status_date, '%Y-%m-%d') <= :time
				GROUP BY ilr.id")
            ->queryAll(TRUE, array(
                ':time' => $time
            ));
        if ( ! empty($data)) {
            foreach ($data as $key => $value) {
                $model = IntegratorLawReports::model()->findByPk($value['id']);
                $model->status = $model::STATUS_DEBT;
                if ($model->save()) {
                    $modelReportSatus = new IntegratorLawReportsStatus();
                    $modelReportSatus->report_id = $value['id'];
                    $modelReportSatus->status = $model::STATUS_DEBT;
                    $modelReportSatus->status_date = $time;
                    $modelReportSatus->save();
                }
            }
        }
    }
}