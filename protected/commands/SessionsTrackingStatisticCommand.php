<?php

class SessionsTrackingStatisticCommand extends CConsoleCommand
{

    public function run($args)
    {
        echo "Дата запуска:".date("H:i:s d.m.Y")."\n";
        $time=time();
        $action="statistic";
        $get_params=array('utm_medium','utm_source','utm_campaign','utm_term','utm_content','k50id','yclid');

        $sql="select datetime from sessions_tracking_statistic_last_request where action='".$action."'";
        $prev_time = Yii::app()->db->createCommand($sql)->queryScalar();
        echo $sql."\n";
        echo "time=".$time." prev_time=".$prev_time."\n";

        $criteria = new CDbCriteria;
        $criteria->select = array('*');
        $criteria->addCondition("datetime>=".$prev_time);
        $criteria->addCondition("datetime<".$time);
        //$criteria->group = "sessionid";
        $criteria->order = "id asc";
        $data = SessionsTracking::model()->findAll($criteria);
        foreach ($data as $value) {
            $stat=new SessionsTrackingStatistic();
            $stat->attributes = $value->attributes;
            $stat->sessions_tracking_id = $value->id;

            $sql="select id from api_record where tracking_session_id='".$value->sessionid."'";
            $api_record_id = Yii::app()->db->createCommand($sql)->queryScalar();
            $stat->api_record_id=$api_record_id;

            if (isset($stat->get)) {
                parse_str($stat->get, $_get);
                foreach ($get_params as $field) {
                    $stat->$field = isset($_get[$field])?trim($_get[$field]):"";
                }
            }

            $stat->keyword = $stat->getKeyword ($stat->utm_term,$stat->referer);
            $stat->source = $stat->getSource ($stat->utm_source,$stat->referer);

            try {
                if (!$stat->save()) {
                    var_dump($stat->getErrors());
                }
            } catch (Exception $e) {
                echo 'ERROR: ',  $e->getMessage(), "\n";
            }
        }

        $sql="update sessions_tracking_statistic_last_request set datetime=".$time." where action='".$action."'";
        echo $sql."\n";
        Yii::app()->db->createCommand($sql)->execute();
    }
}