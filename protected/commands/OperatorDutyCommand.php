<?php

class OperatorDutyCommand extends CConsoleCommand
{

    public function run($args)
    {
        $time = new DateTime();
        $time_print = $time->format('H:i:s d.m.Y');
        $time = $time->format('Y-m-d');

        $timePast = new DateTime();
        $timePast->sub(new DateInterval('P1D'));
        $timePast = $timePast->format('Y-m-d');

        $model = new PamiModel;
        $model->uid = 0;

        $penalty_low=0;
        $penalty_high=1;

        echo "Дата запуска: ".$time_print."\n";

        $sql="select * from operator_duty where date_start>='".$timePast." 00:00:00' and date_end<='".$timePast." 23:59:59'";

        $data = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($data as $value) {
            echo "\nУдаление оператора ".$value['extention']." из очереди ".$value['queue_name']."\n";
            var_dump($model->QueueRemove($value['extention'],$value['queue_name']));
            echo "\nУдаление оператора ".$value['extention']." из очереди ".$model->queue_name."\n";
            var_dump($model->QueueRemove($value['extention'],$model->queue_name));
            echo "\nДобавление оператора ".$value['extention']." в очередь ".$model->queue_name." с penalty=".$penalty_low."\n";
            var_dump($model->QueueAdd($value['extention'],$value['extention'],1,$model->queue_name,$penalty_low));
        }
        echo "======================================== окончание перераспределения предыдущего дня ============================================================================\n";

        // заменить цикл на запрос Queue
        $sql="select * from operator_duty where date_start>='".$time." 00:00:00' and date_end<='".$time." 23:59:59'";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($data as $value) {
            foreach ($model->queue_name_array as $key=>$queue_name) {
                if (!in_array($queue_name,$model->queue_name_secondary)) {
                    echo "\nУдаление оператора ".$value['extention']." из очереди ".$queue_name."\n";
                    var_dump($model->QueueRemove($value['extention'],$queue_name));
                }
            }
            echo "\nДобавление оператора ".$value['extention']." в очередь ".$value['queue_name']." с penalty=".$penalty_low."\n";
            var_dump($model->QueueAdd($value['extention'],$value['extention'],1,$value['queue_name'],$penalty_low));
            echo "\nДобавление оператора ".$value['extention']." в очередь ".$model->queue_name." с penalty=".$penalty_high."\n";
            var_dump($model->QueueAdd($value['extention'],$value['extention'],1,$model->queue_name,$penalty_high));
        }
        echo "======================================== окончание перераспределения текущего дня ===============================================================================\n";
        $model->close();
    }
}