<?php
class RecordsStatConsoleCommand extends CConsoleCommand
{

    public function run($args)
    {
        date_default_timezone_set("Europe/Moscow");
        setlocale(LC_ALL, "ru_RU.UTF-8");

        $time = new DateTime();
        $day = $time->format('d');
        if ($day == '01') {
            $timePast = new DateTime();
            $timePast->sub(new DateInterval('P2D'));
            $timePast = $timePast->format('Y-m');
            $time = $time->format('Y-m');
            $this->cronByRecord($timePast);
            $this->cronByRecordPrice($time, $timePast);
            //если просчет дебиторки запускается кроном, то просто раскомментировать строку.
            //$this->cronUpdateDebt($time);
        }
    }

    public function cronByRecord($time)
    {
        set_time_limit(0);
        $data = Yii::app()->db->createCommand('SELECT * FROM api_record'
            . ' WHERE status IN(2,3,4,5,52,53,101,103,104,11) AND'
            . ' reception_time LIKE "%' . $time . '%" AND'
            . ' (deleted IS NULL OR deleted=0) AND'
            . ' (book_id="" OR book_id IS NULL) AND'
            . ' new = 0'
            . ' ORDER BY status DESC')->queryAll();

        foreach ($data as $value) {
            if (!empty($value['clinic_id']) && !empty($value['domain'])) {
                if ($value['domain'] == 'medbooking.com') {
                    $model = IntegratorClinic::model()->findByAttributes(array('medbooking_id' => $value['clinic_id']));
                    if (!empty($model->id)) {
                        Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                    } else {
                        $model = IntegratorClinic::model()->findByAttributes(array('title' => $value['clinic_name']));
                        if (!empty($model->id)) {
                            Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                        }
                    }
                }
                if ($value['domain'] == 'diagnostika.medbooking.com') {
                    $model = IntegratorClinic::model()->findByAttributes(array('diagnostica_id' => $value['clinic_id']));
                    if (!empty($model->id)) {
                        Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                    } else {
                        $model = IntegratorClinic::model()->findByAttributes(array('title' => $value['clinic_name']));
                        if (!empty($model->id)) {
                            Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                        }
                    }
                }
                if ($value['domain'] == 'testpuls.ru') {
                    $model = IntegratorClinic::model()->findByAttributes(array('testpuls_id' => $value['clinic_id']));
                    if (!empty($model->id)) {
                        Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                    } else {
                        $model = IntegratorClinic::model()->findByAttributes(array('title' => $value['clinic_name']));
                        if (!empty($model->id)) {
                            Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                        }
                    }
                }
                if ($value['domain'] == 'timetovisit.ru') {
                    $model = IntegratorClinic::model()->findByAttributes(array('timetovisit_id' => $value['clinic_id']));
                    if (!empty($model->id)) {
                        Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                    } else {
                        $model = IntegratorClinic::model()->findByAttributes(array('title' => $value['clinic_name']));
                        if (!empty($model->id)) {
                            Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                        }
                    }
                }
                if ($value['domain'] == 'fromed.ru') {
                    $model = IntegratorClinic::model()->findByAttributes(array('fromed_id' => $value['clinic_id']));
                    if (!empty($model->id)) {
                        Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                    } else {
                        $model = IntegratorClinic::model()->findByAttributes(array('title' => $value['clinic_name']));
                        if (!empty($model->id)) {
                            Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                        }
                    }
                }
            }
        }


        $data = Yii::app()->db->createCommand('SELECT * FROM api_record'
            . ' WHERE status IN(2,3,4,5,52,53,101,103,104,11) AND'
            . ' reception_time LIKE "%' . $time . '%" AND'
            . ' (deleted IS NULL OR deleted=0) AND'
            . ' (book_id="" OR book_id IS NULL) AND'
            . ' new = 1'
            . ' ORDER BY status DESC')->queryAll();

        foreach ($data as $value) {
            if (!empty($value['clinic_id'])) {
                $model = IntegratorClinic::model()->findByAttributes(array('medbooking_id' => $value['clinic_id']));
                if (!empty($model->id)) {
                    Yii::app()->db->createCommand("UPDATE api_record SET book_id='" . $model->id . "' WHERE id='" . $value['id'] . "'")->execute();
                }
            }
        }
    }

    public function cronByRecordPrice($time, $timePast)
    {
        set_time_limit(0);
        //$data = Yii::app()->db->createCommand('SELECT ic.all_id, ic.id, ica.duplicate FROM integrator_clinic AS ic INNER JOIN integrator_clinic_all AS ica ON ica.id = ic.all_id WHERE ic.all_id IN (6) ORDER BY ic.all_id')->queryAll();
        $data = Yii::app()->db->createCommand('SELECT ic.all_id, ic.id, ica.duplicate FROM integrator_clinic AS ic INNER JOIN integrator_clinic_all AS ica ON ica.id = ic.all_id')->queryAll();
        if (!empty($data)) {
            $law = array();
            foreach ($data as $key => $value) {
                $law[$value['all_id']][$value['id']]['patient_record'] = Yii::app()->db->createCommand("
					SELECT COUNT(*) FROM (
						SELECT
							COUNT(ar.id)
						FROM api_record AS ar
						WHERE
							ar.book_id ='" . $value['id'] . "' AND
							ar.status IN(2,3,4,5,52,53,101,103,104,11) AND
							ar.reception_time LIKE'%" . $timePast . "%'
						GROUP BY IF (" . $value['duplicate'] . " ='1', CONCAT(ar.name, DATE_FORMAT(ar.create_time,'%Y-%m-%d')), ar.id)
					) AS count")->queryScalar();
                $law[$value['all_id']][$value['id']]['patient_count'] = Yii::app()->db->createCommand("
					SELECT COUNT(*) FROM (
						SELECT
							COUNT(ar.id)
						FROM api_record AS ar
						WHERE
							ar.book_id ='" . $value['id'] . "' AND
							ar.status IN(3,5,52,53,101,104,11) AND
							ar.reception_time LIKE'%" . $timePast . "%'
						GROUP BY IF (" . $value['duplicate'] . " ='1', CONCAT(ar.name, DATE_FORMAT(ar.create_time,'%Y-%m-%d')), ar.id)
					) AS count")->queryScalar();
                $law[$value['all_id']][$value['id']]['patient_repeat'] = Yii::app()->db->createCommand("
					SELECT COUNT(*) FROM (
						SELECT
							COUNT(ar.id)
						FROM api_record AS ar
						WHERE
							ar.book_id ='" . $value['id'] . "' AND
							ar.status IN(3,5,52,53,101,104,11) AND
							ar.reception_time LIKE'%" . $timePast . "%' AND
							koll_repeat ='1'
						GROUP BY IF (" . $value['duplicate'] . " ='1', CONCAT(ar.name, DATE_FORMAT(ar.create_time,'%Y-%m-%d')), ar.id)
					) AS count")->queryScalar();
                $law[$value['all_id']][$value['id']]['patient_diag'] = Yii::app()->db->createCommand("
					SELECT COUNT(*) FROM (
						SELECT
							COUNT(ar.id)
						FROM api_record AS ar
						WHERE
							ar.book_id ='" . $value['id'] . "' AND
							ar.status IN(3,5,52,53,101,104,11) AND
							ar.reception_time LIKE'%" . $timePast . "%' AND
							record_type ='2'
						GROUP BY IF (" . $value['duplicate'] . " ='1', CONCAT(ar.name, DATE_FORMAT(ar.create_time,'%Y-%m-%d')), ar.id)
					) AS count")->queryScalar();
                $law[$value['all_id']][$value['id']]['patient_analyz'] = Yii::app()->db->createCommand("
					SELECT COUNT(*) FROM (
						SELECT
							COUNT(ar.id)
						FROM api_record AS ar
						WHERE
							ar.book_id ='" . $value['id'] . "' AND
							ar.status IN(3,5,52,53,101,104,11) AND
							ar.reception_time LIKE'%" . $timePast . "%' AND
							record_type ='3'
						GROUP BY IF (" . $value['duplicate'] . " ='1', CONCAT(ar.name, DATE_FORMAT(ar.create_time,'%Y-%m-%d')), ar.id)
					) AS count")->queryScalar();
            }
            foreach ($law as $key => $value) {
                $law[$key]['patient_record'] = 0;
                $law[$key]['patient_count'] = 0;
                $law[$key]['patient_repeat'] = 0;
                $law[$key]['patient_diag'] = 0;
                $law[$key]['patient_analyz'] = 0;
                foreach ($value as $key2 => $value2) {
                    $law[$key]['patient_record'] += $value2['patient_record'];
                    $law[$key]['patient_count'] += $value2['patient_count'];
                    $law[$key]['patient_repeat'] += $value2['patient_repeat'];
                    $law[$key]['patient_diag'] += $value2['patient_diag'];
                    $law[$key]['patient_analyz'] += $value2['patient_analyz'];
                }
                $modelLawReports = new IntegratorLawReports();
                $modelLawReports->law_id = $key;
                $modelLawReports->create_date = $time . "-01";
                $modelLawReports->report_date = $timePast . "-01";
                $modelLawReports->patient_record = $law[$key]['patient_record'];
                $modelLawReports->patient_count = $law[$key]['patient_count'];
                $modelLawReports->patient_repeat = $law[$key]['patient_repeat'];
                $modelLawReports->patient_diag = $law[$key]['patient_diag'];
                $modelLawReports->patient_analyz = $law[$key]['patient_analyz'];

                if (!$modelLawReports->patient_record) {
                    $modelLawReports->status = $modelLawReports::STATUS_NONE;
                } else {
                    $modelLawReports->status = $modelLawReports::STATUS_REPORT;
                }
                if ($modelLawReports->save()) {
                    $modelLawReportsStatus = new IntegratorLawReportsStatus();
                    $modelLawReportsStatus->report_id = $modelLawReports->getPrimaryKey();
                    if (!$modelLawReports->patient_record) {
                        $modelLawReportsStatus->status = $modelLawReports::STATUS_NONE;
                    } else {
                        $modelLawReportsStatus->status = $modelLawReports::STATUS_REPORT;
                    }

                    $modelLawReportsStatus->status_date = $time . "-01";
                    $modelLawReportsStatus->status_summ = 0;
                    if ($modelLawReportsStatus->save()) {
                        foreach ($value as $key2 => $value2) {
                            $modelClinicsReports = new IntegratorClinicsReports();
                            $modelClinicsReports->clinic_id = $key2;
                            $modelClinicsReports->law_report_id = $modelLawReports->getPrimaryKey();
                            $modelClinicsReports->patient_record = $value2['patient_record'];
                            $modelClinicsReports->patient_count = $value2['patient_count'];
                            $modelClinicsReports->patient_repeat = $value2['patient_repeat'];
                            $modelClinicsReports->patient_diag = $value2['patient_diag'];
                            $modelClinicsReports->patient_analyz = $value2['patient_analyz'];
                            $modelClinicsReports->save();
                        }
                    }
                }
            }
        }
    }

    public function cronByRecordPriceOld($time, $timePast)
    {
        set_time_limit(0);
        $data=Yii::app()->db->createCommand('SELECT id FROM integrator_clinic')->queryAll();
        if(!empty($data)){
            foreach ($data as $key=>$value){
                $record = Yii::app()->db->createCommand('SELECT COUNT(*) FROM api_record  WHERE book_id='.$value['id'].' AND status IN(2,3,4,5,52,53,11) AND reception_time LIKE "%'.$timePast.'%"')->queryScalar();
                $come = Yii::app()->db->createCommand('SELECT COUNT(*) FROM api_record  WHERE book_id='.$value['id'].' AND status IN(3,5,52,53,11) AND reception_time LIKE "%'.$timePast.'%"')->queryScalar();
                $repeat = Yii::app()->db->createCommand('SELECT COUNT(*) FROM api_record  WHERE book_id='.$value['id'].' AND status IN(3,5,52,53,11) AND reception_time LIKE "%'.$timePast.'%" AND koll_repeat=1')->queryScalar();
                $diagnostika = Yii::app()->db->createCommand('SELECT COUNT(*) FROM api_record  WHERE book_id='.$value['id'].' AND status IN(3,5,52,53,11) AND reception_time LIKE "%'.$timePast.'%" AND status_record=2')->queryScalar();
                if(!empty($record)){
                    $price = new IntegratorClinicPrice;
                    $price->koll_record = $record;
                    $price->koll_come = $come;
                    $price->koll_repeat = $repeat;
                    $price->koll_diagnostik = $diagnostika;
                    $price->status = 1;
                    $price->create_time = date($timePast.'-01');
                    $price->date_status_1 = date($time.'-01');
                    $price->integrator_id=$value['id'];
                    $price->save();
                }
            }

        }
    }

    public function cronUpdateDebtOld($time)
    {
        set_time_limit(0);
        $time = $time.'-01';
        $data = Yii::app()->db->createCommand("SELECT * FROM integrator_clinic_price WHERE DATE_FORMAT(date_status_3, '%Y-%m-%d') < '".$time."' AND status IN (3,5,6,7)")->queryAll();
        if ( ! empty($data)) {
            foreach ($data as $key => $value) {
                $model = IntegratorClinicPrice::model()->findByPk($value['id']);
                $model->status = 6;
                $model->date_status_6 = $time;
                $model->save();
            }
        }
    }

    public function cronUpdateDebt($time)
    {
        set_time_limit(0);
        $time = $time.'-01';

        $data = Yii::app()->db->createCommand("
				SELECT ilr.id AS id FROM integrator_law_reports AS ilr
				LEFT JOIN integrator_law_reports_status AS ilrs ON (ilrs.report_id = ilr.id AND ilrs.status = ilr.status)
				WHERE ilr.status IN (".implode(',', array(IntegratorLawReports::STATUS_PART, IntegratorLawReports::STATUS_BILL, IntegratorLawReports::STATUS_DEBT)).")
				AND summ>0 AND DATE_FORMAT(ilrs.status_date, '%Y-%m-%d') <= :time
				GROUP BY ilr.id")
            ->queryAll(TRUE, array(
                ':time' => $time
            ));
        if ( ! empty($data)) {
            foreach ($data as $key => $value) {
                $model = IntegratorLawReports::model()->findByPk($value['id']);
                $model->status = $model::STATUS_DEBT;
                if ($model->save()) {
                    $modelReportSatus = new IntegratorLawReportsStatus();
                    $modelReportSatus->report_id = $value['id'];
                    $modelReportSatus->status = $model::STATUS_DEBT;
                    $modelReportSatus->status_date = $time;
                    $modelReportSatus->save();
                }
            }
        }
    }
}