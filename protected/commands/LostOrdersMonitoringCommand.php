<?php
class LostOrdersMonitoringCommand extends CConsoleCommand
{
    private $report_mail = "oz@medbooking.com;du@medbooking.com;as@medbooking.com;un@medbooking.com";

    public function run($args)
    {
        date_default_timezone_set("Europe/Moscow");
        setlocale(LC_ALL, "ru_RU.UTF-8");

        $time = new DateTime();
        $time = $time->format('Y-m');

        $body_tr="";
        $headers = "From: REPORT <REPORT>\r\n".
            "MIME-Version: 1.0" . "\r\n" .
            "Content-type: text/html; charset=UTF-8" . "\r\n";
        $body= "<html><body><table border='1'><tr><td>ID заявки</td><td>ID клиники</td><td>Наименование клиники</td></tr>";

        $data = Yii::app()->db->createCommand('SELECT * FROM api_record'
            . ' WHERE status IN(2,3,4,5,52,101,103,104,11) AND'
            . ' reception_time LIKE "' . $time . '%" AND'
            . ' (deleted IS NULL OR deleted=0) AND'
            . ' (book_id="" OR book_id IS NULL) AND'
            . ' new = 1'
            . ' ORDER BY status DESC')->queryAll();

        foreach ($data as $value) {
            if (!empty($value['clinic_id'])) {
                $model = IntegratorClinic::model()->findByAttributes(array('medbooking_id' => $value['clinic_id']));
                if (empty($model->id)) {
                    $body_tr.="<tr><td>".$value['id']."</td><td>".$value['clinic_id']."</td><td>".$value['clinic_name']."</td></tr>";
                }
            }
            else
                $body_tr.="<tr><td>".$value['id']."</td><td>&nbsp;</td><td>".($value['clinic_name']?$value['clinic_name']:"&nbsp;")."</td>";
        }
        if (!$body_tr)
            $body_tr="<tr><td colspan='3'>Данные отсутствуют</td></tr>";
        $body.= $body_tr."</table></body></html>";
        mail($this->report_mail,'Клиники, не подключенные в отчетность или сбойные заявки', $body,$headers);
    }
}