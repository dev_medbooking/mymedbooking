<?php

$config = array(
    'name' => 'API.Medbooking',
    'language' => 'ru',
    'sourceLanguage' => 'ru_RU',
    'charset' => 'utf-8',
    'preload'=>array(
        'log',
    ),
    'theme' => 'apinew',
    'import' => array(
        'application.models.*',
        'application.models.medbooking.*',
        'application.models.diagnostic.*',
        'application.models.testpulse.*',
        'application.models.yandex.*',
        'application.components.*',
        'ext.YiiMailer.YiiMailer',
        'ext.curl.Curl',
        'ext.giix-components.GxActiveRecord',
        'application.controllers.api.*',
        'application.vendor.*',
        'application.helpers.*',
        'application.exceptions.*',
        'webroot.vendor.z_bodya.yii-tinymce.*',
    ),
    'defaultController' => 'site',
    'components' => array(
        'clientScript' => array(
            'packages' => array(
                'jquery' => array(
                    'baseUrl' => '//ajax.googleapis.com/ajax/libs/jquery/1.8.0/',
                    'js' => array(
                        'jquery.min.js',
                    ),
                ),
                'jquery.ui' => array(
                    'baseUrl' => '//code.jquery.com/ui/1.11.1/',
                    'js' => array(
                        'jquery-ui.js',
                    ),
                ),
            ),
        ),
        'curl' => array(
            'class' => 'application.extensions.curl.Curl',
            'options'=>array(
                'timeout'=>30,
                'setOptions'=>array(
                    CURLOPT_UPLOAD => false,
                    CURLOPT_USERAGENT => Yii::app()->params['agent'],
                    CURLOPT_HEADER => false,
                    CURLOPT_RETURNTRANSFER => true
                ),
            )
        ),
        'image' => array(
            'class' => 'application.extensions.image.CImageComponent',
            'driver' => 'GD',
        ),
        'user' => array(
            'class' => 'WebUser',
            'allowAutoLogin' => true,
            'autoRenewCookie' => false,
            'loginUrl' => array('/site/index'),
        ),
        'session' => array(
            'autoStart' => true,
            'class' => 'CDbHttpSession',
            'connectionID' => 'db',
            'sessionTableName' => 'sessions'
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
            'discardOutput' => false,
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
            'itemTable' => 'auth_item',
            'itemChildTable' => 'auth_item_child',
            'assignmentTable' => 'auth_assignment',
            'defaultRoles' => array('Guest')
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'error' => 'site/error',
                '' => 'site/index',
                'gii' => 'gii/default/login',
                //tracking
                //'tracking'=>'tracking/default/index',
                'track'=>'tracking/default/createTrack',
                'tracking/<controller:\w+>/<action:\w+>'=>'tracking/<controller>/<action>',
                'call' => 'site/call',
                'step1' => 'site/step1',
                'step2' => 'site/step2',
                'sms' => 'site/sms',
                'send' => 'site/send',
                'timeslot' => 'site/timeslot',
                'recovery' => 'site/recovery',
                'logout' => 'site/logout',
                'register' => 'site/register',
                'support' => 'site/support',
                'api/createOrder' => 'api/apiOrder/create',
                'api/getStatistics' => 'api/apiStatistics/index',
                'api/getStatus' => 'api/apiPartner/status',
                'api/order/<action:\w+>' => 'api/apiOrder/<action>',
                'api/doctor' => 'api/apiDoctor/single',
                'api/doctors' => 'api/apiDoctor/index',
                'api/clinic' => 'api/apiClinic/single',
                'api/clinics' => 'api/apiClinic',
                'api/service' => 'api/apiService/single',
                'api/dservice' => 'api/apiService/singleDiagnostic',
                'api/services' => 'api/apiService/index',
                'api/statistics' => 'api/apiStatistics/index',
                'api/partner/status' => 'api/apiPartner/status',
                'api/categories' => 'api/apiCategory/index',
                'api/category' => 'api/apiCategory/single',
                'api/dclinics' => 'api/apiDiagnosticClinic/index',
                'api/dclinic' => 'api/apiDiagnosticClinic/single',
                'api/speciality' => 'api/apiCategory/speciality',
                'api/subway/clinic' => 'api/apiSubway/getSelSubways',
                'api/subway/dclinic' => 'api/apiSubway/getSelSubwaysD',
                'api/subway' => 'api/apiSubway/index',
                'api/clinic-services' => 'api/ApiClinicByServices/index',
                'mbTable/module/<id:[2]+>/<url:[\w_\/-]+>' => 'mbTable/module/',
                'mbTable/module/<id:[3]+>/<url:[\w_\/-]+>' => 'mbTable/module/',
                '<controller:\w+>/<action:\w+>/<id:[\d\w]+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

            ),
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
//                    'levels'=>'error, warning',
                    'levels'=>'info, error, warning',
                    'categories'=>'system.*, debug.*, application.*',
                ),
//                array(
//                    'class'=>'CFileLogRoute',
//                    'levels'=>'trace',
//                    'enabled'=>YII_DEBUG,
//                ),
//				array(
//					'class'=>'ext.db_profiler.DbProfileLogRoute',
//					'countLimit' => 1, // How many times the same query should be executed to be considered inefficient
//					'slowQueryMin' => 0.01, // Minimum time for the query to be slow
//				),
            ),
        ),
        'callTouch' => [
            'class' => 'CallTouchApiComponent',
            'clientApiId' => '1240391522ct72fd4f5d42aa0a3d7d35f3b95744118f',
            'siteId' => 2456
        ]
    ),
    'modules'=>array(
        /*'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'73jdndui',
            'ipFilters'=>array('*'),
            // 'newFileMode'=>0666,
            // 'newDirMode'=>0777,
        ),*/
        'tracking',
    ),
    'params' => array(
        'appAlias' => 'crm-msk', // для разделения различных серверов (москва, питер и т.д.); должен быть уникальным между серверами
        'adminEmail' => 'grood@medbooking.com',
        'testEmail' => 'grood@medbooking.com',
        'newRecordsSubs' => array( 'yugay@yuramsite.ru', 'max.medvedev@gmail.com', 'maxrybalko@gmail.com', 'pt@addventure.to'),
        'newRecordsSubs2' => array('yugay@yuramsite.ru'),
        'adminOperatorQueue' => array('un@medbooking.com', 'elena.d@medbooking.com', 'ts@medbooking.com', 'tr@medbooking.com'),
        'allowedRoles' => array(
            'crm' => array('Accounter', 'Administrator', 'Callcenter', 'Dasha', 'Moderator', 'NDasha', 'Operator', 'Seo', 'Toperator'),
            'partner' => array('Administrator', 'Partner'),
        ),
        'serviceEmail' => array('service@medbooking.com'),
        'partnerEmail' => array('mg@medbooking.com'),
        'fromEmailAddress' => 'noreply@medbooking.com',
        'fromName' => 'Сервис онлайн записи',
        'moduleDomain' => 'medbooking.com',
        'apiDomain' => 'a.medbooking.com',
        'sms_gate_url' => "http://crm2.medbooking.com/sms-gate/",
        'allowedAdmins' => [
            'stashek.i@medbooking.com',
            'admin@medbooking.com',
            // 'sk@medbooking.com',
            // 'denis.m@medbooking.com',
            'yugay@medbooking.com',
            // 'yashina.s@medbooking.com',
            'mg@medbooking.com',
            'ab@medbooking.com',
            // 'bd@medbooking.com',
            'konkov.a@medbooking.com',
        ],
        'smsOnAuthUsers' => [
            7865, // mg@medbooking.com graff13
//            8149, // konkov.a@medbooking.com Алексей Коньков для тестовых целей
        ],
    ),

);

// file extra_params
if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'extra_params.php')) {
    require_once('extra_params.php');
    if (!empty($extra_params))
        $config['params'] = array_merge($config['params'], $extra_params);
}

if (is_file(__DIR__ . '/db.php')) {
    require __DIR__ . '/db.php';
}

return $config;

