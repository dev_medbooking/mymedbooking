<?php

return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Cron',
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'ext.YiiMailer.YiiMailer',
    ),
    'components'=>array(
        'db'=>array(
            'connectionString'=>'mysql:host=localhost;dbname=api',
            'emulatePrepare'=>true,
            'username'=>'api',
            'password'=>'thosh1aiSuri',
            'charset'=>'utf8',
            'tablePrefix'=>'',
        ),
        'errorHandler'=>array(
            'errorAction'=>'site/error',
            'discardOutput'=>false,
        ),
    ),
    'params'=>array(
        'newRecordsSubs'=>array('yugay@yuramsite.ru', 'max.medvedev@gmail.com', 'maxrybalko@gmail.com', 'pt@addventure.to'),
        'newRecordsTest'=>array(''),
        'newRecordsSubs2'=>array('yugay@yuramsite.ru'),
        'fromEmailAddress'=>'noreply@medbooking.com',
        'fromName'=>'Сервис онлайн записи',
    ),
);
