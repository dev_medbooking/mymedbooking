<?php

$config = array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Only Console Application',
    'sourceLanguage' => 'ru_RU',
    'charset' => 'utf-8',
    'preload'=>array(
        'log',
    ),
    'import' => array(
        'application.modules.*',
        'application.models.*',
        'application.models.medbooking.*',
        'application.models.diagnostic.*',
        'application.models.yandex.*',
        'application.components.*',
        'ext.YiiMailer.YiiMailer',
        'ext.giix-components.GxActiveRecord',
        'application.controllers.api.*',
        'application.vendor.*',
        'application.helpers.*',
    ),
    'components' => array(
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
//                    'levels'=>'error, warning',
                    'levels'=>'info, error, warning',
                    'categories'=>'system.*, debug.*, application.*',
                ),
//				array(
//					'class'=>'ext.db_profiler.DbProfileLogRoute',
//					'countLimit' => 1, // How many times the same query should be executed to be considered inefficient
//					'slowQueryMin' => 0.01, // Minimum time for the query to be slow
//				),
            ),
        ),
    ),
    'params' => array(
        'adminEmail' => 'grood@medbooking.com',
        'testEmail' => 'grood@medbooking.com',
        'newRecordsSubs' => array( 'yugay@yuramsite.ru', 'max.medvedev@gmail.com', 'maxrybalko@gmail.com', 'pt@addventure.to', 'vr@medbooking.com'),
        'newRecordsSubs2' => array('yugay@yuramsite.ru', 'vr@medbooking.com'),
        'serviceEmail' => array('service@medbooking.com'),
        'partnerEmail' => array('mg@medbooking.com'),
        'fromEmailAddress' => 'noreply@medbooking.com',
        'fromName' => 'Сервис онлайн записи',
        'moduleDomain' => 'medbooking.com',
        'asterisk_options' => array(
            'host' => '88.198.19.201',
            'scheme' => 'tcp://',
            'port' => 5838,
            'username' => 'service_ami',
            'secret' => '7#k%A*U2Q*',
            'connect_timeout' => 90000000,
            'read_timeout' => 90000000
        ),
        'asterisk_listener' => array(
            'host' => '88.198.19.201',
            'scheme' => 'tcp://',
            'port' => 5838,
            'username' => 'listener_ami',
            'secret' => '7#k%A*U2Q*',
            'connect_timeout' => 90000000,
            'read_timeout' => 90000000
        ),
    ),
);

// file extra_params
if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'extra_params.php')) {
    require_once('extra_params.php');
    if (!empty($extra_params))
        $config['params'] = array_merge($config['params'], $extra_params);
}

if (is_file(__DIR__ . '/db.php')) {
    require __DIR__ . '/db.php';
}

return $config;