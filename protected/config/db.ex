<?php
$config['components']['db'] = [
    'connectionString' => 'mysql:host=localhost;dbname=api',
    'username'         => 'root',
    'password'         => '',
    'charset'          => 'utf8',
    'class'            => 'CDbConnection',
];

$config['components']['db2'] = [
    'class'            => 'system.db.CDbConnection',
    'connectionString' => 'mysql:host=localhost;dbname=mb_prod',
    'emulatePrepare'   => true,
    'username'         => 'root',
    'password'         => '',
    'charset'          => 'utf8',
    'tablePrefix'      => '',
];