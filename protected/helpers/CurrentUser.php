<?php

class CurrentUser
{
    /**
     * @return WebUser
     */
    public static function webUser()
    {
        return self::get(false);
    }

    /**
     * @param bool $fromDb
     * @param bool $asRobot
     *
     * @return User|WebUser
     */

    public static function get($fromDb = false, $asRobot = false)
    {
        if (array_key_exists("user", Yii::app()->getComponents())) {
            if ($fromDb) {
                $result = Partner::model()->findByAttributes(['ident_id' => Yii::app()->user->id]);
            } else {
                return Yii::app()->user;
            }
        } else {
            $result = null;
        }
        if (!$result && $asRobot) {
            return User::model()->findByPk(self::getId(true));
        } else {
            return $result;
        }
    }


    public static function userComponentLoaded()
    {
        return array_key_exists("user", Yii::app()->getComponents());
    }


    /**
     * @param bool $returnAsRobotIf
     *
     * @return bool|int|mixed
     */
    public static function getId($returnAsRobotIf = false)
    {
        if ($user = self::get()) {
            return $user->id;
        } else {
            if ($returnAsRobotIf) {
                /**
                 * Если юзер не найден, значит выполняем комманду из консоли, и возвращаем ID юзера-робота
                 */
                return Yii::app()->params['system_user_id'];
            } else {
                return false;
            }
        }
    }

    /**
     * @param User $user
     *
     * @return bool
     * @throws Exception
     */
    public static function isCurrent($user)
    {
        if (!$user) {
            return false;
        }
        if (!($user instanceof User) && !($user instanceof WebUser)) {
            throw new Exception("Need 'User' model or 'WebUser' class");
        }
        if ($user instanceof User) {
            return $user->id == self::getId() && $user->id;
        } else {
            return Yii::app()->user->getFromDb()->id == self::getId();
        }
    }
}