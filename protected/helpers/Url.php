<?php


class Url
{
    public static function to($url, $params = [], $scheme = false)
    {
        return Yii::app()->createUrl($url, $params, $scheme);
    }

    public static function referrer()
    {
        if ($urlReferrer = Yii::app()->request->urlReferrer) {
            return $urlReferrer;
        } else {
            return self::to('/mbTable');
        }
    }

    public static function is($url, $return = true)
    {
        /**
         * Временный вариант, определяем урл исключительно как controller/action
         * TODO переделать на полноценное определение текущего урла через urlManager
         */
        $arr = explode('/', $url);
        return Yii::app()->controller->action->id == $arr[1] && Yii::app()->controller->id == $arr[0] ? $return : false;
    }
}