<?php


class Html extends CHtml
{
    public static function a($text, $url, $options = [])
    {
        $options['href'] = $url;
        return self::tag('a', $options, $text);
    }
}