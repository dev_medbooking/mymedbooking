<?php


/**
 * Class RoleManager
 */
class RoleManager
{
	private static $instance;
	const CH_ROLE_ACCESSES = 'cache_role_accesses';

	public function init()
	{
		return true;
	}

	public static function checkAccessInArray(array $array, $user = null)
	{
		$access = [];
		foreach ($array as $key => $operation) {
			if (is_array($operation)) {
				$operationName = CArray::get($operation, "operation");
			} else {
				$operationName = $operation;
			}
			if (self::checkAccess($operationName, self::user($user))) {
				$access[] = $operation;
			}
		}
		return $access;
	}

	/**
	 * @param        $role
	 * @param string $description
	 * @param null   $bizRule
	 * @param null   $data
	 *
	 * @return CAuthItem
	 */
	public static function createRole($role, $description = '', $bizRule = null, $data = null)
	{
		$auth = Yii::app()->authManager;
		if ($roleItem = $auth->getAuthItem($role)) {
			return $roleItem;
		}
		$auth->clearCache();
		return $auth->createRole($role, $description, $bizRule, $data);
	}


	public static function deleteRole($role)
	{
		$auth = Yii::app()->authManager;
		if (!$roleItem = $auth->getAuthItem($role)) {
			return true;
		}
		foreach ($roleItem->getChildren() as $child) {
			$roleItem->removeChild($child->name);
		}
		AuthAssignment::model()->deleteAll('itemname=:role', [":role" => $role]);
		AuthItem::model()->deleteAll('name=:role', [":role" => $role]);
	}

	public static function user($user = null)
	{
		if (!$user) {
			return CurrentUser::get(true, 0);
		} else {
			return $user;
		}
	}

	public static function instance()
	{
		if (!self::$instance) {
			return self::$instance = new RoleManager();
		} else {
			return self::$instance;
		}
	}

	public static function checkAccess($operation, $user = null)
	{
		if ($user) {
			return \Yii::app()->getAuthManager()->checkAccess($operation, self::user($user)->id, []);
		} else {
			return CurrentUser::userComponentLoaded() && \Yii::app()->user->checkAccess($operation);
		}
	}

	public static function roles()
	{
		return \Yii::app()->authManager->getRoles();
	}

	/**
	 * @param User   $user
	 * @param string $action
	 *
	 * @return bool
	 */
	public static function actionIsExist($action, $user = null)
	{
		return in_array($action, RoleManager::getAccesses(self::user($user)));
	}

	/**
	 * @param User $user
	 *
	 * @return array
	 */
	public static function getRoles($user = null)
	{
		/** @var \CAuthManager $auth */
		$auth = \Yii::app()->authManager;
		if ($user) {
			if (self::user($user)->id) {
				$roles = $auth->getRoles(self::user($user)->id, true);
			} else {
				return [];
			}
		} else {
			$roles = $auth->getRoles(CurrentUser::getId(0));
		}
		return $roles;
	}

	/**
	 * @param User|null $user
	 *
	 * @return array
	 */
	public static function getAccesses($user = null)
	{
		/**
		 * @var \CDbAuthManager $auth
		 * @var CAuthItem       $authItem
		 */
		if ($user) {
			if ($cache = Yii::app()->cache->get(self::CH_ROLE_ACCESSES . "_" . $user->id)) {
				return $cache;
			};
		}

		$auth = \Yii::app()->authManager;
		if ($user) {
			if (self::user($user)->id) {
				$accesses = $auth->getAuthItems(null, self::user($user)->id);
			} else {
				return [];
			}
		} else {
			$accesses = $auth->getAuthItems(null, CurrentUser::getId(0));
		}
		$result = [];
		foreach ($accesses as $roleName => $authItem) {
			$result = array_merge(AuthItem::getAllActions($roleName), $result);
		}
		$result = array_unique($result);
		sort($result);
		if ($user) {
			Yii::app()->cache->set(self::CH_ROLE_ACCESSES . "_" . $user->id, $result, 60);
		}
		return $result;
	}

	/**
	 * @param string $roleName
	 * @param User   $user
	 *
	 * @return bool
	 */
	public static function haveRole($roleName, $user = null)
	{
		$roles = self::getRoles(self::user($user));
		if ($roles) {
			return in_array($roleName, array_keys($roles));
		}
		return false;
	}

	/**
	 * @param array     $roles
	 * @param User|null $user
	 * @param bool      $all
	 *
	 * @return bool
	 */
	public static function haveRoles(array $roles, $user = null, $all = true)
	{
		$allAssigned = false;
		$oneAssigned = false;
		foreach ($roles as $role) {
			$have = self::haveRole($role, $user);
			$allAssigned = $allAssigned && $have;
			$oneAssigned = $oneAssigned || $have;
		}
		return $all ? $allAssigned : $oneAssigned;
	}

	/**
	 * @param       $roleName
	 * @param array $attributes
	 * @param User  $user
	 *
	 * @return bool
	 * @throws Exception
	 */
	public static function assign($roleName, array $attributes = array(), User $user = null)
	{
        /** @var CAuthManager $auth */
		$auth = \Yii::app()->authManager;
		if ($auth->getAuthItem($roleName)) {
			self::authAssign($roleName, self::user($user));
			if (method_exists(self::instance(), "assign_" . $roleName)) {
				call_user_func_array(
					array(self::instance(), "assign_" . $roleName), [$attributes, self::user($user)]
				);
			}
			return self::haveRole($roleName, $user);
		} else {
			throw new Exception(Yii::t('errors', 'Role {role} not found', ["{role}" => $roleName]));
		}
	}

	public static function revoke($roleName, $attributes = array(), $user = null)
	{
        /** @var CAuthManager $auth */
		$auth = \Yii::app()->authManager;
		if ($auth->getAuthItem($roleName)) {
			if (method_exists(self::instance(), "revoke_" . $roleName)) {
				call_user_func_array(array(self::instance(), "revoke_" . $roleName), [$attributes, self::user($user)]);
			}
			self::authRevoke($roleName, self::user($user));
		} else {
			throw new Exception(Yii::t('errors', 'Role {role} not found', ["{role}" => $roleName]));
		}
	}

	private static function authRevoke($roleName, $user = null)
	{
		/** @var CAuthManager $auth */
		$auth = \Yii::app()->authManager;
		if (self::haveRole($roleName, self::user($user))) {
			if (!$auth->revoke($roleName, self::user($user)->id)) {
				throw new Exception(Yii::t('errors', "Can't revoke {role} role", ["{role}" => $roleName]));
			}
		}
		return true;
	}

	private static function authAssign($roleName, $user = null)
	{
		/** @var CAuthManager $auth */
		$auth = \Yii::app()->authManager;
		if (!RoleManager::haveRole($roleName, self::user($user)) && !$auth->assign($roleName, self::user($user)->id)) {
			throw new Exception(Yii::t('errors', "Can't assign {role} role", ["{role}" => $roleName]));
		}
		return true;
	}

	/**
	 * @param null  $user
	 * @param array $exclude
	 *
	 * @throws Exception
	 */
	public static function revokeAllRoles($user = null, $exclude = [])
	{
		if (is_string($exclude)) {
			$exclude = [$exclude];
		}
		foreach (self::getRoles(self::user($user)) as $roleName => $value) {
			if (!in_array($roleName, $exclude)) {
				self::revoke($roleName, [], $user);
			}
		}
	}
}