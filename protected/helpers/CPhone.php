<?php
/**
 * вспомогательные функции для работы с телефонами
 */

class CPhone
{
    /**
     * хитрая метода получения телефонов из списка
     * поле $string -- довольно произвольный текст,
     * содержащий телефоны, возможно, ещё что-то
     *
     * поэтому:
     * - если просто одиночный телефон, только его и возвращаем
     * - если есть запятые, бьём по запятым
     * - если есть точки с запятой, бьём по ним
     * - если нет, бьём по пробелам
     * - в каждом куске оставляем только цифры
     * - нормализуем номер
     * - если получилось меньше 10 цифр -- пропускаем
     * - если получилось больше 11 цифр -- пропускаем
     * - если получилось 10 цифр -- добавляем семёрку
     * - получившееся возвращаем массивом
     *
     * @param string $telephone -- строка, содержащая телефоны
     * @return array -- что получилось напарсить
     */
    public static function parsePhoneString($string)
    {
        $phone_list = [];
        $phone = self::normalizePhone($string);
        if (empty($phone)) {
            if (false !== strpos($string, ',')) {
                $phone_parts = explode(',', $string);
            } elseif (false !== strpos($string, ';')) {
                $phone_parts = explode(';', $string);
            } elseif (false !== strpos($string, '  ')) {
                $phone_parts = explode('  ', $string);
            } else {
                $phone_parts = explode(' ', $string);
            }
            foreach ($phone_parts as $phone) {
                $phone = self::normalizePhone($phone);
                if (empty($phone)) {
                    continue;
                }
                $phone_list[] = $phone;
            }
        } else {
            $phone_list = array($phone);
        }
        return $phone_list;
    }

    /**
     * нормализация телефонного номера (России)
     * - если получилось меньше 10 цифр -- пропускаем (если $return_local = false)
     * - если получилось больше 11 цифр -- пропускаем (проверяем "78" в начале для 12 цифр
     * - если получилось 10 цифр -- добавляем семёрку
     * - если 11 цифр, начиная с "8", заменяем на "7"
     * - проверяем, что номер российский
     *
     * @param string $phone -- телефон для нормализации
     * @param bool $return_local -- возвращать "локальные" номера (короче 10 цифр)
     * @return bool|string
     */
    public static function normalizePhone($phone, $return_local = false)
    {
        $phone = preg_replace('#[^0-9]#', '', $phone);
        $len = strlen($phone);
        /* меньше 10 цифр -- либо локальный телефон, либо ошибка */
        if ($len < 10) {
            if ($return_local) {
                return $phone;
            } else {
                return false;
            }
        }
        /* больше 11 цифр -- либо добавили лишнюю 8ку, либо ошибка */
        if ($len > 11) {
            $start = substr($phone, 0, 2);
            if ($len == 12 && in_array($start, array('78', '87'))) {
                $phone = '7' . substr($phone, 2);
            } else {
                return false;
            }
        }
        /* 10 цифр -- добавим 7ку в начало */
        if ($len == 10) {
            $phone = '7' . $phone;
        }

        /* здесь уже случай длины номера 11 */
        $start = substr($phone, 0, 1);
        if ('8' == $start) {
            $phone = '7' . substr($phone, 1);
        }

        /* проверим, что номер российский */
        $start = substr($phone, 0, 2);
        if (!in_array($start, array('73', '74', '75', '78', '79'))) {
            return false;
        }

        /* конечный результат */
        return $phone;
    }

    public static function getMaskedPhone($phone, $show_start = 2, $show_end = 2)
    {
        $len = strlen($phone);
        if ($show_start + $show_end < $len) {
            $phone = ''
                . substr($phone, 0, $show_start)
                . str_repeat('x', $len - $show_start - $show_end)
                . substr($phone, -1 * $show_end)
            ;
        }
        return $phone;
    }

}
