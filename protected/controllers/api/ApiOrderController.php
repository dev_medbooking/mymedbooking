<?php

class ApiOrderController extends ApiBaseController
{
    /** @api {get} /api/order/create Order
     * @apiVersion 0.1.0
     * @apiGroup Order
     * @apiDescription Создание завки (для тестирования и отладки скриптов интеграции использовать тестовый режим - метод /api/order/testCreate )
     * @apiParam {Number}   ident_id  ID партнера
     * @apiParam {Number}   service_id ID услуги
     * @apiParam {String}   telephone Телефон
     * @apiParam {String}   name Фио пациента
     * @apiParam {Number}   [clinic_id] ID клиники
     * @apiParam {String}   [clinic_name] Название клиники
     * @apiParam {Number}   [doctor_id] ID врача
     * @apiParam {String}   [doctor_name] Фио доктора
     * @apiSampleRequest /api/order/testCreate
     *
     * */
    public function actionCreate($test = false)
    {
        $partner = $this->checkPartner();
        $model = new MbRecord;
        $model->setScenario('guest');
        $model->gender = 1;
        $model->sogl = 1;
        $model->referer = Yii::app()->request->urlReferrer;
        if (true
            AND !empty($_REQUEST['service_id'])
            AND !empty($_REQUEST['telephone'])
            AND !empty($_REQUEST['name'])
            AND !in_array(preg_replace('#[^0-9]#', '', $_REQUEST['telephone']), MbRecord::model()->angry_users)
        ) {
            if (!empty($_REQUEST['clinic_id'])) {
                $model->clinic_id = $_REQUEST['clinic_id'];
            }
            if (!empty($_REQUEST['clinic_name'])) {
//                $model->clinic_name = mb_convert_encoding($_REQUEST['clinic_name'], 'utf8', 'cp1251');
                $model->clinic_name = $_REQUEST['clinic_name'];
            }
            if (!empty($_REQUEST['doctor_id'])) {
                $model->doctor_id = $_REQUEST['doctor_id'];
            }
            if (!empty($_REQUEST['doctor_name'])) {
//                $model->doctor_name = mb_convert_encoding($_REQUEST['doctor_name'], 'utf8', 'cp1251');
                $model->doctor_name = $_REQUEST['doctor_name'];
            }
            if (!empty($_REQUEST['ident_id'])) {
                $model->ident_id = $_REQUEST['ident_id'];
                $partner = Partner::model()->findByPk($model->ident_id);
                if (!empty($partner)) {
                    $phone = MbDomainPhone::model()->findByAttributes(array('partner_id' => $partner->ident_id));
                    if (!empty($phone)) {
                        $model->domain_phone = $phone->phone;
                    }
                }
            }

            /** @var MbRecordServices[] $sModels */
            $sModels = [];

            if(($services = Yii::app()->request->getParam('service_id')) !== null) {
                $services = explode(',', $services);

                foreach($services as $service) {
                    if(MedbookingNodeService::model()->exists('id = :id', [':id' => $service]) === false) {
                        echo CJSON::encode(array('status' => 'error', 'message' => "Услуга с идентификатором {$service} не найдена."));
                        Yii::app()->end();
                    }
                    $sModels[] = call_user_func(function($id) {
                        $model = new MbRecordServices();
                        $model->service_id = $id;
                        return $model;
                    }, $service);
                }
            }

            $model->telephone = $_REQUEST['telephone'];
//            $model->name = mb_convert_encoding($_REQUEST['name'], 'utf8','cp1251');
            $model->name = $_REQUEST['name'];
            $model->domain = 'medbooking.com';
            $model->fromsite = 1;
            $model->phone_record = '';
            $model->koll_repeat = 0;
            $model->status_record = 0;
            $model->comment_for_none_date = '';
            $model->ga = '';
            $method = $test ? 'validate' : 'save';
            if ($model->$method()) {

                if (!$test) {
                    $this->writeCallCallback($model->telephone, $model->domain);
                    $model = MbRecord::model()->findByPk($model->id);
                }

                foreach($sModels as $service) {
                    $service->record_id = $model->id;
                    $service->$method();
                }

                /*$comments = '';
                $comments .= ( ! empty($model->telephone) ? "Контактный телефон: ".$model->telephone."<br>"."\r\n" : "");
                $comments .= ( ! empty($model->name) ? "ФИО пациента: ".$model->name."<br>"."\r\n" : "");
                $comments .= ( ! empty($model->create_time) ? "Время подачи заявки: ".$model->create_time."<br>"."\r\n" : "");
                $comments .= ( ! empty($model->doctor_name) ? "Доктор: ".$model->doctor_name."<br>"."\r\n" : "");
                $comments .= ( ! empty($model->clinic_name) ? "Клиника: ".$model->clinic_name."<br>"."\r\n" : "");
                $comments .= ( ! empty($model->domain) ? "Запись отправлена c сайта ".$model->domain : "");
                if( ! empty(Yii::app()->params['adminEmail']) AND ! empty($model->domain)){
                    $this->mailto('Запись с партнерского сайта '.$model->domain, $comments, Yii::app()->params['adminEmail']);
                }*/
                $template = MbSmsTemplate::model()->findByAttributes(array('title' => 'SENDER'), "CURTIME()>time_start AND CURTIME()<time_end");
                if (!empty($template->primaryKey)) {
                    $_sms_message = $template->message;
                } else {
                    $_sms_message = 'Спасибо за запись на сайте, мы свяжемся с вами в течение 15 минут!';
                }
                if ($test) {
                    $_sms_message = '(тестовый режим: заявка не создавалась)' . $_sms_message;
                }
                $smsSend = new MbRecordSms;
                $smsSend->sms_msisdn = $model->telephone;
                $smsSend->sms_send_admin = 1;
                $smsSend->sms_shortcode = 'medbooking';
                $smsSend->sms_text = $_sms_message;
                $smsSend->message = $_sms_message;
                $smsSend->status = 0;
                $smsSend->record_id = $model->primaryKey;
                if (!empty($model->author_id)) {
                    $smsSend->author_id = $model->author_id;
                }
                if ($smsSend->save()) {
                    $smsSend->sendSms();
                }
                if ($test) {
                    echo CJSON::encode(array('status' => 'success', 'message' => 'Заявка принята в тестовом режиме ( в реальности не создавалась)', 'id' => '101' . $model->id . rand(10000,20000) . rand(0, 9)));
                } else {
                    echo CJSON::encode(array('status' => 'success', 'message' => 'Заявка принята', 'id' => '101' . $model->id . rand(0, 9)));
                }
            } else {
                echo CJSON::encode(array('status' => 'error', 'message' => CActiveForm::validate($model)));
            }
        } else {
            echo CJSON::encode(array('status' => 'error', 'message' => 'Переданы не все обязательные поля'));
        }
        Yii::app()->end();
    }


    private function writeCallCallback($phone,$domain='medbooking.com') {
        //записываем телефон в астериск для автоматического перезвона
        if (!in_array(preg_replace('#[^0-9]#', '',$phone),MbRecord::model()->bad_numbers)) {
            $domainPhone = new MbDomainPhone;
            $domain_phone = $domainPhone->findBySql("select phone from " . $domainPhone->tableName() . " where domain='" . $domain . "' AND status=1 AND isnull(domain_name) limit 1");
            if (!isset($domain_phone['phone'])) {
                $domain = 'medbooking.com';
                $domain_phone = $domainPhone->findBySql("select phone from " . $domainPhone->tableName() . " where domain='" . $domain . "' AND status=1 AND isnull(domain_name) limit 1");
            }
            $dst = preg_replace('#[^0-9]#', '', $domain_phone['phone']);
            if (strlen($dst) < 11)
                $dst = "7" . $dst;
            $src = preg_replace('#[^0-9]#', '', $phone);
            if (strlen($src) < 11)
                $src = "7" . $src;
            Yii::app()->db_ast->createCommand("INSERT INTO autodial(src,dst,clid,type,disposition,call_date)  VALUES('" . $src . "','" . $dst . "','CallBack-Order_From_Site','callback','WAIT',NOW())")->execute();
        }
    }



    public function actionTestCreate()
    {
        return $this->actionCreate($test = true);
    }
}
