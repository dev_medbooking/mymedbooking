<?php

class ApiDiagnosticClinicControllerBAckup extends ApiBaseController
{
    public function actionIndex(){
        $array = $this->ajaxPerform();
        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос  выполнен', 'data' => $array]);
        Yii::app()->end();
    }
    public function actionSingle()
    {
        if(empty($_REQUEST['id'])) {
            echo CJSON::encode(['status' => 'error', 'message' => 'Необходимо передать id диагностический клиники']);
        } else {
            $array = $this->ajaxPerform();
            if(empty($array['data'])){
                echo CJSON::encode(['status' => 'error', 'message' => 'клиники с таким id не существует']);
                Yii::app()->end();
            }
            echo CJSON::encode(['status' => 'success', 'message' =>'Запрос выполнен', 'data' => $array['data']], true);
        }
        Yii::app()->end();

    }
    private function ajaxPerform()
    {
        $partner = $this->checkPartner();
        $criteria=new CDbCriteria;
        $criteria->with=array(
//            'clinic_user',
            'clinic_subway_s'=>array(
                "select"=>false,
                'with'=>array(
                    'sub'
                )
            ),
        );
        $criteria->together=true;
        $criteria->group="t.id";
        $sort=new CSort('MedbookingDiagnosticClinic');
        $sort->defaultOrder="t.id DESC";
        $sort->attributes=array(
            'id'=>array(
                'asc'=>'t.id ASC',
                'desc'=>'t.id DESC',
            ),
            'title'=>array(
                'asc'=>'t.title ASC',
                'desc'=>'t.title DESC',
            ),
            'translit'=>array(
                'asc'=>'t.translit ASC',
                'desc'=>'t.translit DESC',
            ),
            'address'=>array(
                'asc'=>'t.address ASC',
                'desc'=>'t.address DESC',
            ),
            'telephone'=>array(
                'asc'=>'t.telephone ASC',
                'desc'=>'t.telephone DESC',
            ),
            'status'=>array(
                'asc'=>'t.status ASC',
                'desc'=>'t.status DESC',
            ),
            'subway_name'=>array(
                'asc'=>'sub.title ASC',
                'desc'=>'sub.title DESC',
                'label'=>'Метро',
            ),
        );
        $model=new MedbookingDiagnosticClinic();
        $criteria->addColumnCondition(array('t.status' => 1));
        $sort->applyOrder($criteria);
        if(!empty($_REQUEST['id'])){
            $criteria->addCondition("t.id=:id");
            $criteria->params[':id']=$_REQUEST['id'];
            $model->id=$_REQUEST['id'];
        }
        if(!empty($_REQUEST['title'])){
            $criteria->addSearchCondition("t.title",$_REQUEST['title']);
            $model->title=$_REQUEST['title'];
        }
        if(!empty($_REQUEST['translit'])){
            $criteria->addSearchCondition("t.translit",$_REQUEST['translit']);
            $model->translit=$_REQUEST['translit'];
        }
        if(!empty($_REQUEST['address'])){
            $criteria->addSearchCondition("t.address",$_REQUEST['address']);
            $model->address=$_REQUEST['address'];
        }
        if(!empty($_REQUEST['telephone'])){
            $criteria->addSearchCondition("t.telephone",$_REQUEST['telephone']);
            $model->telephone=$_REQUEST['telephone'];
        }
//        if(isset($_REQUEST['status'])&&$_REQUEST['status']!=''){
//            if($_REQUEST['status']==0){
//                $criteria->addCondition("t.status=0");
//                $model->status=0;
//            } else{
//                $criteria->addCondition("t.status=".$_REQUEST['status']);
//                $model->status=$_REQUEST['status'];
//            }
//        } else{
//            unset($model->status);
//        }
        unset($model->status);
        if(!empty($_REQUEST['subway_name'])){
            $criteria->addSearchCondition("sub.title",$_REQUEST['subway_name']);
//            $model->subway_name=$_REQUEST['subway_name'];
        }
        if(!empty($_REQUEST['search-free'])){
            $criteria->addSearchCondition("t.title",$_REQUEST['search-free'],true,'OR');
            $criteria->addSearchCondition("t.address",$_REQUEST['search-free'],true,'OR');
            $criteria->addSearchCondition("t.meta_title",$_REQUEST['search-free'],true,'OR');
            $criteria->addSearchCondition("t.meta_keywords",$_REQUEST['search-free'],true,'OR');
            $criteria->addSearchCondition("t.meta_description",$_REQUEST['search-free'],true,'OR');
        }
        $count=MedbookingDiagnosticClinic::model()->count($criteria);
        $pages=new CPagination($count);
        if(!empty($_REQUEST['pageSize'])) {
            $pages->pageSize = intval($_REQUEST['pageSize']);
        } else {
            $pages->pageSize = 10;
        }
        $pages->applyLimit($criteria);
        /** @var MedbookingDiagnosticClinic[] $data */
        $data=MedbookingDiagnosticClinic::model()->findAll($criteria);
        $mbDomainPhone = MbDomainPhone::model()->findByAttributes(array('partner_id' => $partner->ident_id));
        foreach($data as $key => $v) {
            $_data = array();
            $_data['id'] = $v->id;
            $_data['title'] = $v->title;
            $_data['translit'] = $v->translit;
            $_data['lat'] = $v->lat;
            $_data['lng'] = $v->lng;
            $_data['services'] = $v->listPrice();
            switch($v->status) {
                case 0: $_data['status'] = 'Не модерирован';break;
                case 1: $_data['status'] = 'Модерирован';break;
                case 2: $_data['status'] = 'Удалено';break;
                default: $_data['status'] = 0;
            }
            $_data['status'] = $v->status;
            $_data['address'] = $v->address;

            //Todo: possible should import ImageBehavior extension from medbooking.com to a.medbooking.com
            if(!empty($v->image))
                $_data['image'] = 'http://medbooking.com/files/clinic/photo/' . $v->image;

//            $_data['telephone'] = $v->telephone;
            if(!empty($mbDomainPhone)) {
                $_data['telephone'] = $mbDomainPhone->phone;
            } else {
//                $_data['telephone'] = '(не указан)';
                echo CJSON::encode(['status' => 'error', 'message' => 'Ошибка при поиске контактного телефона.Свяжитесь с тех поддержкой.']);
                Yii::app()->end();
            }
            $_data['subway'] = $v->getSubway_name();
            $data[$key] = $_data;
        }
        return array('data'=>$data,'page'=>$pages->getCurrentPage(),'sort'=>$sort->getOrderBy(),'count'=>$count);
    }

}
