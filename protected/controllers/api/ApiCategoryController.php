<?php

class ApiCategoryController extends ApiBaseController
{
    /** @api {get} /api/categories Categories
     * @apiVersion 0.1.0
     * @apiGroup Category
     * @apiDescription Список категорий
     * @apiParam {Number}   ident_id  ID партнера
     * @apiParam {Number}   [id]  ID категории
     * @apiParam {String}   [name] Название
     * @apiParam {String}   [translit] Название (транслит)
     * @apiParam {Number}   [level] Уровень вложенности категории
     * @apiParam {Number}   [page] Страница
     * @apiParam {Number}   [pageSize] Размер страницы
     * @apiParam {String}   [sort] Сортировка
     * @apiSampleRequest /api/categories
     *
     * */
    public function actionIndex($limit = 10)
    {
        $array = $this->ajaxPerform();
        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос  выполнен','data' =>  $array]);
        Yii::app()->end();
    }

    public function actionSingle(){
        if(empty($_REQUEST['id'])) {
            echo CJSON::encode(['status' => 'error', 'message' => 'Необходимо передать id категории']);
            Yii::app()->end();
        }
        $array = $this->ajaxPerform();
        if(count($array{'data'}) == 0)
        {
            echo CJSON::encode(['status' => 'error', 'message' => 'Записи с таким id  не обнаружено.']);
            Yii::app()->end();
        }
        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос  выполнен','data' =>  $array['data']]);
        Yii::app()->end();
    }
    public function actionSpeciality(){
        $level = 2;
        $array = $this->ajaxPerform($level);
        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос  выполнен','data' =>  $array['data']]);
        Yii::app()->end();
    }

    private function ajaxPerform($level = null)
    {
        $partner = $this->checkPartner();
        $criteria=new CDbCriteria;
        $sort=new CSort('MedbookingCategory');
        $sort->defaultOrder="t.id DESC";
        $sort->attributes=array(
            'id'=>array(
                'asc'=>'t.id ASC',
                'desc'=>'t.id DESC',
                'label'=>'ID',
            ),
            'name'=>array(
                'asc'=>'t.name ASC',
                'desc'=>'t.name DESC',
                'label'=>'Услуга',
            ),
            'level'=>array(
                'asc'=>'t.level ASC',
                'desc'=>'t.level DESC',
                'label'=>'Уровень',
            ),
            'root'=>array(
                'asc'=>'t.root ASC',
                'desc'=>'t.root DESC',
                'label'=>'Тип',
            ),
            'translit'=>array(
                'asc'=>'t.translit ASC',
                'desc'=>'t.translit DESC',
                'label'=>'URL',
            ),
        );
        $sort->applyOrder($criteria);
        $model=new MedbookingCategory;
        if(!empty($_REQUEST['id'])){
            $criteria->addCondition("t.id=:id");
            $criteria->params[':id']=$_REQUEST['id'];
//            $model->id=$_REQUEST['id'];
        }
        if(!empty($_REQUEST['level'])){
            $criteria->addCondition("t.level=:level");
            $criteria->params[':level']=$_REQUEST['level'];
//            $model->level=$_REQUEST['level'];
        }
        if($level) {
            $criteria->addCondition("t.level=:level");
            $criteria->params[':level']=$level;
        }
        if(isset($_REQUEST['link'])){
            $criteria->addCondition("t.link=:link");
            $criteria->params[':link']=$_REQUEST['link'];
        }
        if(isset($_REQUEST['link2'])){
            $criteria->addCondition("t.link2=:link2");
            $criteria->params[':link2']=$_REQUEST['link2'];
        }
        if(!empty($_REQUEST['root'])){
            $criteria->addCondition("t.root=:root");
            $criteria->params[':root']=$_REQUEST['root'];
//            $model->root=$_REQUEST['root'];
        }
        if(!empty($_REQUEST['name'])){
            $criteria->addSearchCondition("t.name",mb_convert_encoding($_REQUEST['name'],'utf8'));
//            $model->name=$_REQUEST['name'];
        }
        if(!empty($_REQUEST['translit'])){
            $criteria->addSearchCondition("t.translit",$_REQUEST['translit']);
//            $model->translit=$_REQUEST['translit'];
        }
        if(!empty($_REQUEST['level'])){
            $criteria->addSearchCondition("t.level",$_REQUEST['level']);
//            $model->translit=$_REQUEST['translit'];
        }
//        $criteria->addCondition("t.level=1");
        if(!empty($_REQUEST['category'])){
            $criteria->addCondition("t.level=2");
            $criteria->addSearchCondition("t.name",mb_convert_encoding($_REQUEST['category'], 'utf8'));
//            $model->category=$_REQUEST['category'];
        }
        if(!empty($_REQUEST['search-free'])){
            $criteria->addSearchCondition("t.name",$_REQUEST['search-free'],true,'OR');
        }
        $count=MedbookingCategory::model()->count($criteria);
        $pages=new CPagination($count);
        if(!empty($_REQUEST['pageSize'])) {
            $pages->pageSize = intval($_REQUEST['pageSize']);
        } else {
            $pages->pageSize = 10;
        }
        $pages->applyLimit($criteria);
//        if(!empty($_REQUEST['id'])) {
//            $criteria->addCondition("t.id=".$_REQUEST['id']);
//        }
        $data=MedbookingCategory::model()->findAll($criteria);
        foreach($data as $k => $v) {
            $_data = array();
            $_data['id'] = $v->id;
            $_data['name'] = $v->name;
            $_data['translit'] = $v->translit;
            $_data['level'] = $v->level;
            $data[$k] = $_data;
        }
        return array('data'=>$data,'page' =>$pages->getCurrentPage(),'sort'=>$sort->getOrderBy(),'count' => $count);
    }


    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}