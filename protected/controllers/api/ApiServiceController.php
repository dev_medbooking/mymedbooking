<?php

class ApiServiceController extends ApiBaseController
{
    /** @api {get} /api/services Services
     * @apiVersion 0.1.0
     * @apiGroup Services
     * @apiDescription Список услуг
     * @apiParam  {Number} ident_id  ID Партнера
     * @apiParam  {Number} [id]  ID категории услуг
     * @apiParam {string}   [name] Название категории услуг
     * @apiParam {Number}   [page] Страница
     * @apiParam {Number}   [pageSize] размер странцы
     * @apiSampleRequest /api/services
     * */
    public function actionIndex($limit = 10)
    {
        $array = $this->ajaxPerform();
        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос  выполнен', 'data' => $array]);
        Yii::app()->end();
    }

    /** @api {get} /api/service Service
     * @apiVersion 0.1.0
     * @apiGroup Services
     * @apiDescription Услуга и список клиник с ценами
     * @apiParam  {Number} ident_id  ID Партнераz`
     * @apiParam  {Number} [id]  ID услуги
     * @apiSampleRequest /api/service
     * */
    public function actionSingle()
    {
        $this->checkPartner();
        if (empty($_REQUEST['id'])) {
            echo CJSON::encode(['status' => 'error', 'message' => 'Необходимо передать id услуги']);
        } else {
            $id = $_REQUEST['id'];
            $nodeService = MedbookingNodeService::model()->findByPk($id);
            if (!empty($nodeService)) {
                $data = array();
                $data['id'] = $nodeService->id;
                $data['title'] = $nodeService->title;
                $data['translit'] = $nodeService->translit;
                $data['description'] = $nodeService->description;
                $data['type'] = $nodeService->type;
                $data['category_id'] = $nodeService->category_id;
                $clinics = array();
                /**
                 * @var  $key
                 * @var MedbookingNodeServicePrice $price
                 */
                foreach($nodeService->nodeServicePrices as $key => $price) {
                    if(!empty($price->clinic)){// and $price->clinic->status != 0 ) {
                        /** @var MedbookingClinic $clinic */
                        $clinic = $price ->clinic;
                        $clinicData = array();
                        $clinicData['id'] = $clinic->id;
                        $clinicData['title'] = $clinic->title;
                        $clinicData['translit'] = $clinic->translit;
                        $clinicData['description'] = $clinic->description;
                        $clinicData['address'] = $clinic->address;
//                        $clinicData['email'] = $clinic->email;
                        $clinicData['url'] = $clinic->url;
//                        $clinicData['body'] = $clinic->body;
                        $clinicData['district'] = $clinic->district;
                        $clinicData['regime'] = $clinic->getRegimeClinic();
                        $clinicData['subway'] = $clinic->getSubway_name();
                        $clinicData['current_service_price'] = $price->price . ' руб';
                        $clinicData['discount_service_price'] = $price->discount ? $price->discount . ' руб' : false;
                        $clinics[] = $clinicData;
                    }
                }
                $data['clinics'] = $clinics;
                echo CJSON::encode(['status' => 'success', 'message' => 'Запрос  выполнен', 'data' => $data]);
            } else {
                echo CJSON::encode(['status' => 'error', 'message' => 'Услуги с таким id не существует']);
            }
        }
        Yii::app()->end();
    }

    public function actionSingleDiagnostic()
    {
        $this->checkPartner();
        if (empty($_REQUEST['id'])) {
            echo CJSON::encode(['status' => 'error', 'message' => 'Необходимо передать id услуги']);
        } else {
            $id=intval($_REQUEST['id']);
            $_category = DiagnosticCategoryPrice::model()->findByPk($id);
            if (!empty($_category)) {
                $criteria = new CDbCriteria();
                $saveCriteria = array();
                $saveParams = array();
                $saveCriteria['status'] = "t.status=1";
                $saveCriteria['category'] = "catc_category.lft>=:lft AND catc_category.rgt<=:rgt AND catc_category.root=:root";
                $saveParams['category'] = array(":lft" => $_category->lft, ":rgt" => $_category->rgt, ":root" => $_category->root);

                $criteria->with = array('clinic_category_s');
                $criteria->together = true;
                $criteria->group = "t.id";
                if (!empty($saveCriteria)) {
                    foreach ($saveCriteria as $value) {
                        $criteria->addCondition($value);
                    }
                }
                if (!empty($saveParams)) {
                    $params = array();
                    foreach ($saveParams as $value) {
                        $params = array_merge($params, $value);
                    }
                    $criteria->params = $params;
                }
                $clinics = DiagnosticMedbookingClinic::model()->findAll($criteria);
                $data = [];
                $data['id'] = $_category->id;
                $data['title'] = $_category->name;
                $data['clinics'] = [];
                if (!empty($clinics)) {
                    foreach($clinics as $clinic) {
                        $clinicData = array();
                        $clinicData['id'] = $clinic->id;
                        $clinicData['title'] = $clinic->title;
                        $clinicData['translit'] = $clinic->translit;
                        $clinicData['description'] = $clinic->description;
                        $clinicData['address'] = $clinic->address;
                        $clinicData['price'] = $clinic->clinic_category_s[0]->price;
                        $clinicData['action_price'] = $clinic->clinic_category_s[0]->action_price;
                        $data['clinics'][] = $clinicData;
                    }
                }
                echo CJSON::encode(['status' => 'success', 'message' => 'Запрос  выполнен', 'data' => $data]);
            } else {
                echo CJSON::encode(['status' => 'error', 'message' => 'Услуги с таким id не существует']);
            }
        }
        Yii::app()->end();
    }

    private function ajaxPerform()
    {
        $this->checkPartner();
        $output = array();
        $output['data'] = array();

        /** @var CDbCriteria $criteria */
        $criteria = new CDbCriteria();
//        $criteria->compare('t.status', 1);
        if(!empty($_REQUEST['id'])) {
            $criteria->addCondition("t.id = " . intval($_REQUEST['id']));
        }
        if(!empty($_REQUEST['name'])) {
            $criteria->addCondition("t.name = '" . ($_REQUEST['name']) . "'");
        }
        if (!empty($_REQUEST['category_id'])) {
            if (is_array($_REQUEST['category_id'])) {
                $criteria->addInCondition("t.id", $_REQUEST['category_id']);
            } else {
                $criteria->addCondition("t.id=" . $_REQUEST['category_id']);
            }
        }
        $count = MedbookingCategory::model()->count($criteria);
        $pages = new CPagination($count);
        if(!empty($_REQUEST['pageSize'])) {
            $pages->pageSize = intval($_REQUEST['pageSize']);
        } else {
            $pages->pageSize = 10;
        }
        $pages->applyLimit($criteria);
        /** @var MedbookingCategory[] $modelsCategory */
        $modelsCategory = MedbookingCategory::model()->findAll($criteria);
        foreach ($modelsCategory as $k => $v) {
            $_data = array();
            $_data['id'] = $v->id;
            $_data['name'] = $v->name;
            $modelsServices = MedbookingNodeService::model()->findAllByAttributes(array('category_id' => $v->id, 'status'=>1), array('order' => 'title ASC'));
            $services = array();
            foreach ($modelsServices as $k => $s) {
                $_serviceData = array();
                $_serviceData['id'] = $s->id;
                $_serviceData['title'] = $s->title;
                $_serviceData['translit'] = $s->translit;
                $_serviceData['description'] = $s->description;
                $_serviceData['category_id'] = $s->category_id;
                $_serviceData['type'] = $s->type;
                $services[] = $_serviceData;
            }
            $modelsDiagnosticServices = MedbookingCategoryDiagnosticService::model()->findAllByAttributes(array('category_id' => $v->id), array('order' => 'title ASC'));
            $diagnosticServices = array();
            foreach ($modelsDiagnosticServices as $k => $s) {
                $_dServiceData = array();
                $_dServiceData['id'] = $s->id;
                $_dServiceData['title'] = $s->title;
                $_dServiceData['utl'] = $s->url;
                $_dServiceData['category_id'] = $s->category_id;
                $diagnosticServices[] = $_dServiceData;
            }
//            if (empty($services) && empty($diagnosticServices))
//                continue;
            $_data['services'] = $services;
            $_data['diagnostic_services'] = $diagnosticServices;

            $output['data'][] = $_data;
            $output['page'] = $pages->getCurrentPage();
            $output['count'] = $count;
        }
        return $output;
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}