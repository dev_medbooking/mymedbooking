<?php

class ApiClinicByServicesController extends ApiBaseController
{
    /** @api {get} /api/clinic-services Service
     * @apiVersion 0.1.0
     * @apiGroup Services
     * @apiDescription Услуги и список клиник с ценами эти услуги
     * @apiParam  {Number} ident_id  ID Партнера
     * @apiParam  {Number} [services]  ID услуги
     * @apiSampleRequest /api/clinic-services
     * */
    public function actionIndex()
    {
        // Через AR, критерии и джоины все очень тормозило, так что более 50 записей за раз не выберешь
        // Через GROUP_CONCAT упрется в max_packet_size на продакшене
        // Прошу простить за sql запросы из контроллера и прочие прелести, так как надо было уже вчера
        $this->checkPartner();
        $data = ['count' => 0, 'clinicsCount' => 0, 'data' => []];
        $db = Yii::app()->db2;

        $pageSize = 10;
        if (!empty($_REQUEST['pageSize'])) {
            $pageSize = intval($_REQUEST['pageSize']);
        }

        $sql = "
            SELECT
              node_service.id,
              node_service.title,
              node_service.translit,
              IFNULL(node_service.image, '') as image,
              node_service.category_id,
              node_service.type
            FROM node_service
        ";

        $countSql = "select count(*) from node_service ";

        $categorySql = $serviceSql ='';
        if (!empty($_REQUEST['services'])) {
            if (is_array($_REQUEST['services'])) {
                $serviceSql = 'node_service.id IN (' . implode(',', array_map('intval', $_REQUEST['services'])) . ')';
            } else {
                $serviceSql = 'node_service.id = ' . intval($_REQUEST['services']);
            }
        }
        if (!empty($_REQUEST['categories']) && is_array($_REQUEST['categories'])) {
            $categorySql = 'node_service.category_id IN (' . implode(',', array_map('intval', $_REQUEST['categories'])) . ')';
        }

        if ($serviceSql || $categorySql) {
            $where = ($serviceSql && $categorySql ) ? $serviceSql . ' AND ' . $categorySql : ($serviceSql ? $serviceSql: $categorySql);
            $sql .= ' WHERE 1!=1 OR (' . $where . ')';
            $countSql .= ' WHERE 1!=1 OR (' . $where . ')';
        }

        $data['count'] = $count = $db->createCommand($countSql)->queryScalar();
        $pages = new CPagination($count);
        $pages->pageSize = $pageSize;
        $pages->route = false;

        $sql .= " GROUP BY node_service.id ORDER BY node_service.views DESC LIMIT {$pages->getOffset()}, {$pages->getLimit()} ";
        $services = $db->createCommand($sql)->queryAll();

        if (!$services) {
            echo CJSON::encode($data);
            Yii::app()->end();
        }

        $servicesIds = array_column($services, 'id');
        $services = array_combine($servicesIds, $services);

        $prices = $db->createCommand(
            'select distinct
                node_service_price.clinic_id,
                node_service_price.service_id,
                clinic.title,
                COUNT(reviews.id) as comm,
                IF(clinic.rate10 > 10, 10, clinic.rate10) as rate10,
                IFNULL(clinic.image, \'\') as image,
                IFNULL(clinic.address, \'\') address,
                node_service_price.price
                from node_service_price '
            . ' join clinic on clinic.id = node_service_price.clinic_id'
            . ' join reviews on clinic.id = reviews.clinic_id and reviews.status = 1'
            . ' where node_service_price.service_id in (' . implode(',', $servicesIds) . ') '
            . ' GROUP BY node_service_price.clinic_id'
            . ' ORDER BY rate10 desc, comm desc'
        )->queryAll();
//        $clinicsCount = $db->createCommand(
//            'select count(distinct node_service_price.clinic_id) from node_service_price
//             join node_service on node_service.id = node_service_price.service_id ' . $where
//        )->queryScalar();

        $clinicsCount = count(array_unique(array_column($prices, 'clinic_id')));

        $subways = $db->createCommand(
            'select clinic_id, translit, subway_id, title, lat, lng, lin1, lin2, subway_line_id from subway_clinic
             join subway on subway.id = subway_clinic.subway_id
             where clinic_id in (' . implode(',', array_unique(array_column($prices, 'clinic_id'))) . ');'
        )->queryAll();
        foreach ($prices as &$clinic) {
            $clinic['subways'] = array_values(
                array_filter(
                    $subways,
                    function ($row) use ($clinic) {
                        return $row['clinic_id'] == $clinic['clinic_id'];
                    }
                )
            );

            $clinic['subways_another_count'] = 0;
            if (count($clinic['subways']) > 1) {
                $clinic['subways_another_count'] = count($clinic['subways']) - 1;
            }
        }

        foreach ($services as &$service) {
            $service['clinics'] = array_values(
                array_filter(
                    $prices,
                    function ($row) use ($service) {
                        return $row['service_id'] == $service['id'];
                    }
                )
            );

            $hasMinPrice = false;
            $service['min_price'] = 0;
            $minPrice = array_reduce(
                $service['clinics'],
                function ($min, $clinic) use (&$hasMinPrice) {
                    if ($min > $clinic['price'] && $clinic['price'] > 0) {
                        $hasMinPrice = true;
                        $min = $clinic['price'];
                    }

                    return $min;
                },
                PHP_INT_MAX
            );

            if ($hasMinPrice) {
                $service['min_price'] = $minPrice;
            }
        }

        $data['data'] = array_values($services);
        $data['clinicsCount'] = $clinicsCount;
        $data['pages'] = '';
        if ($pages->pageCount > 1) {
            $data['pages'] = $this->widget(
                'CLinkPager',
                array(
                    'pages'          => $pages,
                    'header'         => '',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',
                    'nextPageLabel'  => '>',
                    'prevPageLabel'  => '<',
                    'maxButtonCount' => 5,
                ),
                true
            );
        }
        echo CJSON::encode($data);
        Yii::app()->end();
    }
}