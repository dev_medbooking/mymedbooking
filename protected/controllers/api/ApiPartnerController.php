<?php

class ApiPartnerController extends ApiBaseController
{
    /** @api {get} /api/partner/status Partner's order status
     * @apiVersion 0.1.0
     * @apiGroup Partner
     * @apiDescription Статус партнерской заявки
     * @apiParam {Number} ident_id ID партнера
     * @apiParam {Number} id ID заявки
     * @apiSampleRequest /api/partner/status
     *
     * */
    public function actionStatus()
    {
        $partner = $this->checkPartner();
        if (empty($_REQUEST['id'])) {
            echo CJSON::encode(array('status' => 'error', 'message' => 'Необходимо передать id'));
            Yii::app()->end();
        }
        $id = $_REQUEST['id'];
        $id = substr($id, 3);
        $id = substr($id, 0, -1);
        $model = MbRecord::model()->findByAttributes(array('id' => $id, 'ident_id' => $partner->id));
        if (empty($model)) {
                $id = $_REQUEST['id'];
            echo CJSON::encode(array('status' => 'error', 'message' => 'Не найдено заявки с id=' . $id));
            Yii::app()->end();
        }

        $partner_status_number = $model->partner_status;
        switch ($model->partner_status) {
            case MbRecord::PARTNER_STATUS_WAIT :
                $partner_status = 'В ожидании';
                break;
            case MbRecord::PARTNER_STATUS_APPROVE:
                $partner_status = 'Утвержден';
                break;
            case MbRecord::PARTNER_STATUS_DECLINE:
                $partner_status = 'Отклонен';
                break;
            default:
                Yii::log('error in partner_status, api_record with id =' . $model->id . ' has partner_status = ' . $model->partner_status);
                $partner_status = 'В ожидании';
        }

        echo CJSON::encode(array('status' => 'success', 'message' => 'Запрос выполнен', 'partner_status' => $partner_status, 'partner_status_number' => $partner_status_number));
        Yii::app()->end();
    }

}
