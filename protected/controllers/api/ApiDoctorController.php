<?php
class ApiDoctorController extends ApiBaseController
{
    const REVIEWS_PAGE_SIZE = 10;

	public function actionSingle()
	{
        if(empty($_REQUEST['id'])) {
            echo CJSON::encode(['status' => 'error', 'message' => 'Необходимо передать id врача']);
        } else {
            $array = $this->ajaxPerform();
            echo CJSON::encode(['status' => 'success', 'message' =>'Запрос выполнен', 'data' => $array['data']], true);
        }
        Yii::app()->end();
	}

    /** @api {get} /api/doctors Doctors
     * @apiVersion 0.1.0
     * @apiGroup Doctor
     * @apiDescription Список врачей
     * @apiParam  {Number} ident_id  ID партнера
     * @apiParam  {Number} [id] ID Врача
     * @apiParam {String}   [fio] Фио
     * @apiParam {String}   [translit] Фио (транслит)
     * @apiParam {String}   [clinic_name] Название клиники
     * @apiParam {Number}   [child] Детский врач
     * @apiParam {String}   [speciality] Специальность
     * @apiParam {String}   [position] Должность
     * @apiParam {String}   [startyear] Год начала работ
     * @apiParam {String}   [degree] Степень
     * @apiParam {String}   [clinics] Клиники
     * @apiParam {number}   [clinic_id] ID клиники
     * @apiParam {Number}   [doctor_city_id]  ID города
     * @apiParam {Number}   [position] Должность
     * @apiParam {Number}   [page] Страница
     * @apiParam {Number}   [pageSize] Колл-во на странице
     * @apiParam {String}   [sort] Сортировка
     * @apiSampleRequest /api/doctors
     *
     * */
	public function actionIndex()
	{
        $array = $this->ajaxPerform();
        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос выполнен',$array]);
        Yii::app()->end();
	}
    private function ajaxPerform()
    {
        $partner = $this->checkPartner();
        $criteria=new CDbCriteria([
            'select' => [
                't.id',
                't.lname', 't.fname', 't.sname',
                't.translit',
                't.child',
                't.house',
                't.position',
                't.startyear',
                't.degree',
                't.description',
                't.speciality',
                't.doctor_city_id',
                't.education',
                't.awards',
                't.image',
                't.rate10',
                't.comm'
            ],

            'with' => [
                'clinicDoctors' => [
                    'select' => '*',
                    'with' => [
                        'clinic'=>[
                            'with'=>[
                                'clinic_subway_s'=>[
                                    'with'=>[
                                        'subway'=>[
                                            'select'=>['title','lat', 'lng'],

                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'categoryDoctors' => [
                    'select' => '*'
                ],
                'doctor_price_s',
                'reviews'
            ]
        ]);

        if(!empty($_REQUEST['limit'])) {
            $limit = $_REQUEST['limit'];
        } else {
            $limit =-1;
        }

        $criteria->limit =  $limit;
        $criteria->together=true;
        $criteria->group="t.id";

        $criteria->compare('t.status', 1);
        $criteria->addCondition("t.rate10 is not null");
        $criteria->addCondition("t.startyear is not null");
        $criteria->addCondition("t.image is not null");

        $sort=new CSort('MedbookingDoctor');
        $req_sort = 'rate';
        $req_sway = 'desc';
        if(!empty($_REQUEST['sort'])) list($req_sort,$req_sway) = explode('.',$_REQUEST['sort']);
        if(!empty($_REQUEST['sort']) && $req_sort=='price') {
            $sort->defaultOrder = "doctor_price_s.price $req_sway, t.comm DESC";
        } elseif(!empty($_REQUEST['sort']) && $req_sort=='exp') {
            $sort->defaultOrder = "t.startyear ".(($req_sway=='desc')?'asc':'desc').", t.comm DESC";
        } else {
            $sort->defaultOrder = "t.rate10 $req_sway, t.comm DESC, sort DESC";
        }
        $sort->attributes=array(
            'id'=>array(
                'asc'=>'t.id ASC',
                'desc'=>'t.id DESC',
                'label'=>'ID',
            ),
            'fio'=>array(
                'asc'=>'t.lname ASC, t.fname ASC, t.sname ASC',
                'desc'=>'t.lname DESC, t.fname DESC, t.sname DESC',
                'label'=>'ФИО',
            ),
            'translit'=>array(
                'asc'=>'t.translit ASC',
                'desc'=>'t.translit DESC',
                'label'=>'URL',
            ),
            'speciality'=>array(
                'asc'=>'t.speciality ASC',
                'desc'=>'t.speciality DESC',
                'label'=>'Специальность',
            ),
            'child'=>array(
                'asc'=>'t.child ASC',
                'desc'=>'t.child DESC',
                'label'=>'Детские',
            ),
            'position'=>array(
                'asc'=>'t.position ASC',
                'desc'=>'t.position DESC',
                'label'=>'Должность',
            ),
            'doctor_city_id'=>array(
                'asc'=>'t.doctor_city_id ASC',
                'desc'=>'t.doctor_city_id DESC',
                'label'=>'Город',
            ),
            'startyear'=>array(
                'asc'=>'t.startyear ASC',
                'desc'=>'t.startyear DESC',
                'label'=>'Карьера',
            ),
            'degree'=>array(
                'asc'=>'t.degree ASC',
                'desc'=>'t.degree DESC',
                'label'=>'Степень',
            ),
            'image'=>array(
                'asc'=>'t.image ASC',
                'desc'=>'t.image DESC',
                'label'=>'Фото',
            ),
            'countReviews'=>array(
                'asc'=>'countReviews ASC',
                'desc'=>'countReviews DESC',
                'label'=>'Отзывы',
            ),
            'countClinics'=>array(
                'asc'=>'countClinics ASC',
                'desc'=>'countClinics DESC',
                'label'=>'Клиники',
            ),
            'clinic_name'=>array(
                'asc'=>'clinic.title ASC',
                'desc'=>'clinic.title DESC',
                'label'=>'Клиники',
            ),
            'countRecords'=>array(
                'asc'=>'countRecords ASC',
                'desc'=>'countRecords DESC',
                'label'=>'Записи',
            ),
            'status'=>array(
                'asc'=>'t.status ASC',
                'desc'=>'t.status DESC',
                'label'=>'Модерация',
            ),
            'doctor_city_id'=>array(
                'asc'=>'t.doctor_city_id ASC',
                'desc'=>'t.doctor_city_id DESC',
                'label'=>'Город',
            ),

        );
        $model=new MedbookingDoctor;

        $sort->applyOrder($criteria);
        if(!empty($_REQUEST['id'])){
            $criteria->addCondition("t.id=:id");
            $criteria->params[':id']=$_REQUEST['id'];
            $model->id=$_REQUEST['id'];
        }
        if(!empty($_REQUEST['fio'])){
            $criteria->addSearchCondition("CONCAT_WS(' ',t.lname,t.fname,t.sname)",$_REQUEST['fio']);
//            $model->fio=$_REQUEST['fio'];
        }
        if(!empty($_REQUEST['translit'])){
            $criteria->addSearchCondition("t.translit",$_REQUEST['translit']);
//            $model->translit=$_REQUEST['translit'];
        }
        if(!empty($_REQUEST['clinic_name'])){
            $criteria->addSearchCondition("clinic.title",$_REQUEST['clinic_name']);
//            $model->clinic_name=$_REQUEST['clinic_name'];
        }
        if(!empty($_REQUEST['child'])){
            if($_REQUEST['child']==1 or trim(htmlspecialchars($_REQUEST['child']==='Детский'))){
                $criteria->addCondition("t.child=1");
            }
//            $model->child=$_REQUEST['child'];
        }
        if(!empty($_REQUEST['capital'])){
            $criteria->addCondition("t.doctor_city_id=1 OR t.doctor_city_id=2");
        }
        if(!empty($_REQUEST['speciality'])){
            $criteria->addInCondition("categoryDoctors.category_id",explode(',',$_REQUEST['speciality']));
//            $model->speciality=$_REQUEST['speciality'];
        }
        if (isset($_REQUEST['nation'])) {
            $criteria->addCondition("t.nation = :nation");
            $criteria->params[':nation'] = $_REQUEST['nation'];
//            $model->id=$_REQUEST['id'];
        }
        if(!empty($_REQUEST['position'])){
            $criteria->addSearchCondition("t.position",$_REQUEST['position']);
//            $model->position=$_REQUEST['position'];
        }
        if(!empty($_REQUEST['startyear'])){
            $criteria->addSearchCondition("t.startyear",$_REQUEST['startyear']);
//            $model->startyear=$_REQUEST['startyear'];
        }
        if(!empty($_REQUEST['telephone'])){
            $criteria->addSearchCondition("t.telephone",$_REQUEST['telephone']);
//            $model->telephone=$_REQUEST['telephone'];
        }
        if(!empty($_REQUEST['degree'])){
            $criteria->addSearchCondition("t.degree",$_REQUEST['degree']);
//            $model->degree=$_REQUEST['degree'];
        }
        if(!empty($_REQUEST['clinics'])){
            $criteria->addSearchCondition("clinic.title",$_REQUEST['clinics']);
//            $model->clinics=$_REQUEST['clinics'];
        }
        if(!empty($_REQUEST['clinic_id'])){
            $criteria->addCondition("clinic.id=".intval($_REQUEST['clinic_id']));
//            $model->clinics=$_REQUEST['clinics'];
        }
        if(!empty($_REQUEST['doctor_city_id'])){
            $criteria->addSearchCondition("t.doctor_city_id",$_REQUEST['doctor_city_id']);
//            $model->doctor_city_id=$_REQUEST['doctor_city_id'];
        }

        if(($value = Yii::app()->request->getParam('education')) !== null) {
            $criteria->addSearchCondition('t.education', $value, true);
        }

        if(($value = Yii::app()->request->getParam('awards')) !== null) {
            $criteria->addSearchCondition('t.awards', $value, true);
        }

        if(($value = Yii::app()->request->getParam('call_home')) !== null) {
            $criteria->compare('t.house', ($value ? 1 : 0));
        }

        if(($value = Yii::app()->request->getParam('house')) == 1) {
            $criteria->compare('t.house', ($value ? 1 : 0));
        }

        if(!empty($_REQUEST['search-free'])){
            $criteria->addSearchCondition("CONCAT_WS(' ',t.lname,t.fname,t.sname)",$_REQUEST['search-free'],true,'OR');
            $criteria->addSearchCondition("t.meta_title",$_REQUEST['search-free'],true,'OR');
            $criteria->addSearchCondition("t.meta_keywords",$_REQUEST['search-free'],true,'OR');
            $criteria->addSearchCondition("t.meta_description",$_REQUEST['search-free'],true,'OR');
            $criteria->addSearchCondition("t.speciality",$_REQUEST['search-free'],true,'OR');
        }

        $search=[];
        if(!empty($_REQUEST['subways'])){
            $search=clone($criteria);
            $criteria->addInCondition("subway.translit",explode('_',$_REQUEST['subways']));
        }

        $count=MedbookingDoctor::model()->count($criteria);

        if($count==0 AND ! empty($_REQUEST['subways'])){

            $count=MedbookingSubway::model()->countByAttributes(['translit'=>explode('_',$_REQUEST['subways'])]);
            if($count>0) {
                $tmp = MedbookingSubway::model()->findAllByAttributes(['translit' => explode('_', $_REQUEST['subways'])]);
                $order[] = 'CASE WHEN subway.lat IS NULL THEN 1 ELSE 0 END';

                foreach ($tmp as $sub) {
                    if(!empty($sub->lat) AND !empty($sub->lng)) {
                        $lat = str_replace(",", ".", CHtml::encode($sub->lat));
                        $lng = str_replace(",", ".", CHtml::encode($sub->lng));
                        $order[] = 'acos(cos(radians(' . $lat . '))*cos(radians(' . $lat . '))*cos(radians(' . $lng . ')-radians(subway.lng))+sin(radians(subway.lat))*sin(radians(' . $lat . '))) ASC';
                    }
                }
                $sort->defaultOrder =  implode(',', $order).','.$sort->defaultOrder;
                $criteria = $search;
                $sort->applyOrder($criteria);
            }
            $count=MedbookingDoctor::model()->count($criteria);
        }
        $pages=new CPagination($count);
        if(!empty($_REQUEST['pageSize'])) {
            $pages->pageSize = intval($_REQUEST['pageSize']);
        } else {
            $pages->pageSize = 10;
        }
        $pages->applyLimit($criteria);
        $pager = $this->pager($pages);

        /** @var MedbookingDoctor[] $data */
        $data=MedbookingDoctor::model()->findAll($criteria);

        foreach($data as $key => $v) {

            $clinics = $v->getClinicsName();
            $subways = $v->getClinicsSubways();
            $_data = array();
            $_data['id'] = $v->id;
            $_data['fio'] = $v->getFio();
            $_data['lname'] = $v->lname;
            $_data['fname'] = $v->fname;
            $_data['sname'] = $v->sname;
            $_data['image'] = '//medbooking.com/files/doctor/photo/'.($v->image?:$v->translit.'.jpg');
            $_data['translit'] = $v->translit;
            $_data['speciality'] = $v->speciality;
            $_data['child'] = !empty($v->child)?'Детский':'';
            $_data['house'] = !empty($v->house)?1:0;
            $_data['position'] = !empty($v->position)?$v->position:'';
            $_data['startyear'] = !empty($v->startyear)?$v->startyear:'';
            $_data['degree'] = !empty($v->degree)?$v->degree:'';
            $_data['description'] = !empty($v->description)?$v->description:'';
            $_data['clinic_id'] = !empty($v->clinicDoctors)?$v->clinicDoctors[0]['clinic']['id']:'';
            $_data['clinic_address'] = !empty($v->clinicDoctors)?$v->clinicDoctors[0]['clinic']['address']:'';
            $_data['lat'] = !empty($v->clinicDoctors)?$v->clinicDoctors[0]['clinic']['lat']:'';
            $_data['lng'] = !empty($v->clinicDoctors)?$v->clinicDoctors[0]['clinic']['lng']:'';
            $_data['clinic_name'] = !empty($v->clinicDoctors)?$v->clinicDoctors[0]['clinic']['title']:'';
            $_data['clinics_name'] = implode(',',$clinics);
            $_data['clinics_subways'] = $subways[0];
            $_data['subways_info'] = $subways[1];
//            $_data['status'] = !empty($v->status)?$v->status:'0';
            $_data['doctor_city_id'] = !empty($v->doctor_city_id)?$v->doctor_city_id:'0';
            $_data['education'] = !empty($v->education) ? $v->education : '';
            $_data['awards'] = !empty($v->awards) ? $v->awards : '';
            $_data['reviews'] = $v->getReviews();
            $_data['comm'] = count($_data['reviews']);
            $reviewsPages = new CPagination(count($_data['reviews']));
            $reviewsPages->pageSize = self::REVIEWS_PAGE_SIZE;
            $reviewsPages->currentPage = 0;
            $_data['pager'] = $this->pager($reviewsPages);
            $_data['services'] = $this->_doctorServices($v->id);
            $_data['consultation_price'] = $this->_doctorConsultationPrice($v->id);
            $_data['experience'] = !empty($v->startyear) ? ((int) date('Y') - (int) $v->startyear) : '';
            $_data['call_home'] = $v->house;
            $rate=str_replace(",",".",sprintf("%.1f",!empty($v->rate10)?round($v->rate10,2):0));
            $_data['rate'] = $rate>=10?'10':$rate;
            $data[$key] = $_data;
        }
        return array('data'=>$data,'page' => $pages->getCurrentPage(),'sort'=>$sort->getOrderBy(), 'count' => $count, 'pager'=>$pager, 'pageCount'=>$pages->pageCount, 'pageNum'=>$pages->currentPage+1);
    }

    private function _doctorServices($id)
    {
        static $result = [];

        if(!isset($result[$id])) {
            $result[$id] = Yii::app()->db2->createCommand()
                ->select([
                    'node_service.id',
                    'node_service.title name'
                ])
                ->from('node_service')
                ->join('node_service_doctor', 'node_service_doctor.service_id = node_service.id')
                ->where('node_service_doctor.doctor_id = :id', [
                    ':id' => $id
                ])
                ->queryAll()
            ;
        }

        return $result[$id];
    }

    private function _doctorConsultationPrice($id)
    {
        return ($price = Yii::app()->db2->createCommand()
            ->select('price')
            ->from('price')
            ->where('doctor_id = :id', [
                ':id' => $id
            ])
            ->order('price')
            ->queryScalar()) ? $price : ''
        ;
    }

    private function pager(CPagination $pages)
    {
        if ($pages->pageCount < 1) {
            return '';
        }

        return $this->widget(
                'CLinkPager',
                array(
                    'pages'             =>  $pages,
                    'header'            =>  '',
                    'firstPageLabel'    =>  '<<',
                    'lastPageLabel'     =>  '>>',
                    'nextPageLabel'     =>  '>',
                    'prevPageLabel'     =>  '<',
                    'maxButtonCount'    =>  5,
                ),
                true
            );
    }
}