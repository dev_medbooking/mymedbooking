<?php

class ApiStatisticsController extends ApiBaseController
{

    /** @api {get} /api/statistics Statistics
     * @apiVersion 0.1.0
     * @apiGroup Statistics
     * @apiDescription Статистика
     * @apiParam  {Number} ident_id  ID партнера
     * @apiParam  {String} [name]  Поиск (по полям: имя, телефон, услуга, id)
     * @apiParam {Number}   [status] Статус
     * @apiParam {String}   [date_start] Дата "от" формат "Y-m-d H:i:s"
     * @apiParam {String}   [date_end] Дата "до"  формат "Y-m-d H:i:s"
     * @apiParam {Number}   [page] Страница
     * @apiParam {Number}   [pageSize] размер странцы
     * @apiSampleRequest /api/statistics
     * */
    public function actionIndex()
    {
        $request = Yii::app()->request;

        $date_start = $request->getQuery('date_start');
        $date_end = $request->getQuery('date_end');
        $status = $request->getQuery('status');
        $limit = $request->getQuery('limit');
        $id = $request->getQuery('ident_id');
        if (empty($id)) {
            echo CJSON::encode(array('status' => 'error', 'message' => 'Необходимо передать параметр ident_id'));
            Yii::app()->end();
        }
        $array = $this->ajaxPerform($id, $date_start, $date_end, $status, $limit);
        echo CJSON::encode(array('status' => 'success', 'message' => 'Запрос выполнен', 'array' => $array));

    }

    private function ajaxPerform($id, $date_start, $date_end, $status, $limit)
    {
        $partner = $this->checkPartner();
        $criteria = new CDbCriteria;
        $criteria->select = array('id', 'name', 'create_time', 'telephone');
        $array['date_start'] = !empty($date_start) ? $date_start : '';
        $array['date_end'] = !empty($date_end) ? $date_end : '';
        $array['limit'] = !empty($limit) ? $limit : 10;
        $array['status'] = !empty($status) ? $status : 0;
        $array['ident_id'] = $id;
        if (!empty($_REQUEST['name'])) {
            $criteria->addSearchCondition("t.name", $_REQUEST['name'], TRUE, "OR");
            $criteria->addSearchCondition("t.telephone", $_REQUEST['name'], TRUE, "OR");
            $criteria->addSearchCondition("t.service", $_REQUEST['name'], TRUE, "OR");
            $criteria->addSearchCondition("t.id", $_REQUEST['name'], TRUE, "OR");
            $array['name'] = $_REQUEST['name'];
//            $_GET['MbRecord']['name'] = $_REQUEST['name'];
        }
        if (!empty($array['status'])) {
            $criteria->addSearchCondition("t.partner_status", $array['status']);
        }

        if (!Yii::app()->user->checkAccess("Administrator")) {
            $criteria->addCondition("t.ident_id=" . $id);
        } else {
            $criteria->addCondition("t.ident_id IS NOT NULL AND t.ident_id!='0'");
        }
        if (!empty($date_start) AND !empty($date_end)) {
            $criteria->addCondition("DATE(t.create_time)>='" . addslashes($array['date_start']) . "'");
            $criteria->addCondition("DATE(t.create_time)<='" . addslashes($array['date_end']) . "'");
        }
        $criteria->addCondition("t.deleted IS NULL OR t.deleted = 0");
        $array['count'] = $count =  MbRecord::model()->count($criteria);
        $pages = new CPagination($array['count']);
//        $criteria->limit = $array['limit'];
        if(!empty($_REQUEST['pageSize'])) {
            $pages->pageSize = intval($_REQUEST['pageSize']);
        } else {
            $pages->pageSize = 10;
        }
        $pages->applyLimit($criteria);
        /** @var MbRecord[] $data */
        $data = MbRecord::model()->findAll($criteria);
        foreach ($data as $key => $value) {
            $data_ = array();
            $data_['id'] = $value->id;
            $data_['create_time'] = $value->create_time;
            $data_['name'] = $value->name;
            $data_['telephone'] = $value->telephone;
            $data_['typeText'] = $value->getTypeText();
            $data_['service'] = $value->getPartnerService();
            $data_['partner_price'] = $value->getPartnerPrice();
            $data_ ['time_processing_text'] = $value->getTimeProcessingText();
            $data_['partner_status'] = $value->getPartnerStatus();
            $data[$key] = $data_;
        }
//        $array['data'] = $data;
        $output  = array('data' => $data,'page' => $pages->getCurrentPage(), 'count' => $count );
        return $output;
    }
}
