<?php

class ApiSubwayController extends ApiBaseController
{

    public function actionGetSelSubways()
    {
        $this->checkPartner();
        if (empty($_REQUEST['clinic_id'])) {
            echo CJSON::encode(['status' => 'error', 'message' => 'Необходимо передать id клиники']);
            Yii::app()->end();
        }
        $clinic_id = intval($_REQUEST['clinic_id']);
        $data = Yii::app()->db2->createCommand("SELECT subway.title AS title FROM subway_clinic LEFT JOIN subway ON subway_clinic.subway_id=subway.id WHERE subway_clinic.clinic_id=" . $clinic_id)->queryAll();

        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос  выполнен', 'data' => $data]);
        Yii::app()->end();
    }

    public function actionGetSelSubwaysD()
    {
        $this->checkPartner();
        if (empty($_REQUEST['dclinic_id'])) {
            echo CJSON::encode(['status' => 'error', 'message' => 'Необходимо передать id диагностической клиники']);
            Yii::app()->end();
        }
        $diagnostic_clinic_id = intval($_REQUEST['dclinic_id']);
        $data = Yii::app()->db2->createCommand("SELECT subway.title AS title FROM subway_clinic_diagnostic LEFT JOIN subway ON subway_clinic_diagnostic.subway_id=subway.id WHERE subway_clinic_diagnostic.clinic_id=" . $diagnostic_clinic_id)->queryAll();
        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос  выполнен', 'data' => $data]);
        Yii::app()->end();
    }

    /** @api {get} /api/subway Subway
     * @apiVersion 0.1.0
     * @apiGroup Subway
     * @apiDescription Список станций метро
     * @apiParam {Number}   ident_id  ID Партнера
     * @apiParam {Number}   [id]  ID
     * @apiParam {String}   [title] Нвзвание
     * @apiParam {String}   [translit] Название (транслит)
     * @apiParam {Number}   [city] ID Города
     * @apiParam {Number}   [subway_line_id] id ветки метро
     * @apiParam {Number}   [page] Страница
     * @apiParam {Number}   [pageSize] размер странцы
     * @apiParam {String}   [sort] Сортировка
     * @apiSampleRequest /api/subway
     * */
    public function actionIndex()
    {
        $array = $this->ajaxPerform();
        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос  выполнен', 'data' => $array]);
        Yii::app()->end();
    }

    private function ajaxPerform()
    {
        $this->checkPartner();
        $criteria = new CDbCriteria;
        $sort = new CSort('MedbookingSubway');
        $sort->defaultOrder = "t.id DESC";
        $sort->attributes = array(
            'id' => array(
                'asc' => 't.id ASC',
                'desc' => 't.id DESC',
                'label' => 'ID',
            ),
            'title' => array(
                'asc' => 't.title ASC',
                'desc' => 't.title DESC',
                'label' => 'Название',
            ),
            'translit' => array(
                'asc' => 't.translit ASC',
                'desc' => 't.translit DESC',
                'label' => 'URL: станции',
            ),
            'subway_line_id' => array(
                'asc' => 'subway_line_id ASC',
                'desc' => 'subway_line_id DESC',
                'label' => 'Линии',
            ),
            'line_title' => array(
                'asc' => 'line.title ASC',
                'desc' => 'line.title DESC',
                'label' => 'Линии',
            ),
            'city' => array(
                'asc' => 't.city ASC',
                'desc' => 't.city DESC',
                'label' => 'Город',
            ),
        );
        $sort->applyOrder($criteria);
        $model = new MedbookingSubway;
        if (!empty($_REQUEST['id'])) {
            $criteria->addCondition("t.id=:id");
            $criteria->params[':id'] = $_REQUEST['id'];
//            $model->id=$_REQUEST['id'];
        }
        if (!empty($_REQUEST['title'])) {
            $criteria->addSearchCondition("t.title", $_REQUEST['title']);
//            $model->title=$_REQUEST['title'];
        }
        if (!empty($_REQUEST['translit'])) {
            $criteria->addSearchCondition("t.translit", $_REQUEST['translit']);
//            $model->translit=$_REQUEST['translit'];
        }
        if (isset($_REQUEST['city'])) {
            $criteria->addSearchCondition("t.city", $_REQUEST['city']);
//            $model->city=$_REQUEST['city'];
        }
        if (!empty($_REQUEST['subway_line_id'])) {
            $criteria->addCondition("t.subway_line_id=:subway_line_id");
            $criteria->params[':subway_line_id'] = $_REQUEST['subway_line_id'];
//            $model->subway_line_id=$_REQUEST['subway_line_id'];
        }
        if (!empty($_REQUEST['search-free'])) {
            $criteria->addSearchCondition("t.title", $_REQUEST['search-free'], true, 'OR');
        }
        $count = MedbookingSubway::model()->count($criteria);
        $pages=new CPagination($count);
        if(!empty($_REQUEST['pageSize'])) {
            $pages->pageSize = intval($_REQUEST['pageSize']);
        } else {
            $pages->pageSize = 10;
        }
        $pages->applyLimit($criteria);
        $data = MedbookingSubway::model()->with('subwayLine')->with('district')->with('cityObject')->findAll($criteria);
        foreach ($data as $key => $d) {
            $_data = array();
            $_data['id'] = $d->id;
            $_data['title'] = $d->title;
            $_data['translit'] = $d->translit;
            if (!empty($d->subwayLine))
                $_data['subwayLine'] = $d->subwayLine->title;
            else
                $_data['subwayLine'] = '';
            $_data['subway_line_id'] = $d->subway_line_id;
            if (!empty($d->district))
                $_data['district'] = $d->district->title;
            else
                $_data['district'] = '';
            $_data['city'] = $d->city;
            if (!empty($d->cityObject))
                $_data['city_name'] = $d->cityObject->title;
            else
                $_data['city_name'] = '';

            $data[$key] = $_data;
        }
        return array('data' => $data,'page'=> $pages->getCurrentPage(),'sort' => $sort->getOrderBy(), 'count' => $count);
    }

}
