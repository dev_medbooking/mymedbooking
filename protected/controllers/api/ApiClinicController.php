<?php

class ApiClinicController extends ApiBaseController
{
    const REVIEWS_PAGE_SIZE = 10;

    /** @api {get} /api/clinics Clinics
     * @apiVersion 0.1.0
     * @apiGroup Clinics
     * @apiDescription Список клиник
     * @apiParam  {Number}  ident_id  ID Партнера
     * @apiParam  {Number}  [id]  ID Клиники
     * @apiParam {String}   [title] Название
     * @apiParam {String}   [specialist] Специальность врача
     * @apiParam {String}   [translit] Название (транслит)
     * @apiParam {String}   [address] Адрес
     * @apiParam {String}   [telephone] Телефон
     * @apiParam {String}   [url] Ссылка
     * @apiParam {String}   [subway_name] Станция метро
     * @apiParam {Number}   [page] Страница
     * @apiParam {Number}   [pageSize] Размер странцы
     * @apiParam {String}   [sort] Сортировка
     * @apiSampleRequest /api/clinics
     *
     * */
    public function actionIndex()
    {
        $array = $this->ajaxPerform();
        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос выполнен', 'data' => $array]);
        Yii::app()->end();
    }

    public function actionSingle()
    {
        if (empty($_REQUEST['id'])) {
            echo CJSON::encode(['status' => 'error', 'message' => 'Необходимо указать id']);
            Yii::app()->end();
        }
        $array = $this->ajaxPerform();
        echo CJSON::encode(['status' => 'success', 'message' => 'Запрос выполнен', 'data' => $array['data']]);
        Yii::app()->end();
    }

// Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
    private function ajaxPerform()
    {
        $partner = $this->checkPartner();
        $criteria = new CDbCriteria;
        $criteria->with = array(
            'user',
            'clinic_doctor_sa' => array(
                'with' => array(
                    'doctor' => array(
                        'select' => false,
                        'with'   => array(
                            'categoryDoctors' => array(
                                'select' => false,
                                'with'   => array(
                                    'category' => array(
                                        'select' => 'name',
                                    )
                                ),
                            )
                        )
                    )
                )
            ),
            'subwayClinics'    => array(
                "select" => false,
                'with'   => array(
                    'subway'
                )
            ),
            'categoryClinics'  => array(
                "select" => false
            ),
        );
        $criteria->together = true;
        $criteria->group = "t.id";

        $criteria->compare('t.status', 1);
        $criteria->addCondition("t.rate10 is not null");
        $criteria->addCondition("t.image is not null");

        $sort = new CSort('MedbookingClinic');
        $req_sort = 'rate';
        $req_sway = 'desc';
        if (!empty($_REQUEST['sort'])) {
            list($req_sort, $req_sway) = explode('.', $_REQUEST['sort']);
        }
        if (!empty($_REQUEST['sort']) && $req_sort == 'price') {
            $sort->defaultOrder = "categoryClinics.price $req_sway, t.comm DESC";
        } else {
            $sort->defaultOrder = "t.rate10 $req_sway";
        }
        $sort->attributes = array(
            'id'                  => array(
                'asc'   => 't.id ASC',
                'desc'  => 't.id DESC',
                'label' => 'ID',
            ),
            'title'               => array(
                'asc'   => 't.title ASC',
                'desc'  => 't.title DESC',
                'label' => 'Название',
            ),
            'translit'            => array(
                'asc'   => 't.translit ASC',
                'desc'  => 't.translit DESC',
                'label' => 'URL',
            ),
            'address'             => array(
                'asc'   => 't.address ASC',
                'desc'  => 't.address DESC',
                'label' => 'Адрес',
            ),
            'telephone'           => array(
                'asc'   => 't.telephone ASC',
                'desc'  => 't.telephone DESC',
                'label' => 'Телефон',
            ),
            'url'                 => array(
                'asc'   => 't.url ASC',
                'desc'  => 't.url DESC',
                'label' => 'Сайт',
            ),
            'countDoctors'        => array(
                'asc'   => 'countDoctors ASC',
                'desc'  => 'countDoctors DESC',
                'label' => 'Доктора',
            ),
            'countRecords'        => array(
                'asc'   => 'countRecords ASC',
                'desc'  => 'countRecords DESC',
                'label' => 'Записи',
            ),
            'status'              => array(
                'asc'   => 't.status ASC',
                'desc'  => 't.status DESC',
                'label' => 'Модерация',
            ),
            'subway_name'         => array(
                'asc'   => 'subway.title ASC',
                'desc'  => 'subway.title DESC',
                'label' => 'Метро',
            ),
            'specialist'          => array(
                'asc'   => 'category.name ASC',
                'desc'  => 'category.name DESC',
                'label' => 'Специалисты',
            ),
            'clinic_city_line_id' => array(
                'asc'   => 't.clinic_city_line_id ASC',
                'desc'  => 't.clinic_city_line_id DESC',
                'label' => 'Город',
            ),

        );
        $model = new MedbookingClinic;

        $criteria->addCondition("t.clinic_city_line_id IN(1,2)");
        $sort->applyOrder($criteria);
        if (!empty($_REQUEST['id'])) {
            $criteria->addCondition("t.id=:id");
            $criteria->params[':id'] = $_REQUEST['id'];
        }
        if (!empty($_REQUEST['doctor_id'])) {
            $criteria->addCondition("clinic_doctor_sa.doctor_id=:doctor_id");
            $criteria->params[':doctor_id'] = $_REQUEST['doctor_id'];
        }
        if (!empty($_REQUEST['net'])) {
            $criteria->addCondition("t.network_id is not null");
        }
        if (isset($_REQUEST['nation'])) {
            $criteria->addCondition("t.nation = :nation");
            $criteria->params[':nation'] = $_REQUEST['nation'];
        }
        if (!empty($_REQUEST['specialist'])) {
            $criteria->addSearchCondition("category.name", mb_convert_encoding($_REQUEST['specialist'], 'utf8'));
        }
        if (!empty($_REQUEST['speciality'])) {
            $criteria->addInCondition("category.id", explode(',', $_REQUEST['speciality']));
        }
        if (!empty($_REQUEST['subways'])) {
            $criteria->addInCondition("subway.translit", explode('_', $_REQUEST['subways']));
        }
        if (!empty($_REQUEST['clinic_city_line_id'])) {
            if ($_REQUEST['clinic_city_line_id'] == 1) {
                $criteria->addCondition("t.clinic_city_line_id IN(1,2)");
            } else {
                $criteria->addSearchCondition("t.clinic_city_line_id", $_REQUEST['clinic_city_line_id']);
            }
        } else {
            $criteria->addCondition("t.clinic_city_line_id IN(1,2)");
        }
        if (!empty($_REQUEST['title'])) {
            $criteria->addSearchCondition("t.title", mb_convert_encoding($_REQUEST['title'], 'utf8'));
        }
        if (!empty($_REQUEST['translit'])) {
            $criteria->addSearchCondition("t.translit", $_REQUEST['translit']);
        }
        if (!empty($_REQUEST['address'])) {
            $criteria->addSearchCondition("t.address", mb_convert_encoding($_REQUEST['address'], 'utf8'));
        }
        if (!empty($_REQUEST['telephone'])) {
            $criteria->addSearchCondition("t.telephone", $_REQUEST['telephone']);
        }
        if (!empty($_REQUEST['url'])) {
            $criteria->addSearchCondition("t.url", $_REQUEST['url']);
        }
        if (!empty($_REQUEST['subway_name'])) {
            $criteria->addSearchCondition("subway.title", mb_convert_encoding($_REQUEST['subway_name'], 'utf8'));
        }
        if (!empty($_REQUEST['specialistAll'])) {
            $criteria->addSearchCondition("category.name", mb_convert_encoding($_REQUEST['specialistAll'], 'utf8'));
        }
        if (!empty($_REQUEST['search-free'])) {
            $criteria->addSearchCondition("t.title", $_REQUEST['search-free'], true, 'OR');
            $criteria->addSearchCondition("t.address", $_REQUEST['search-free'], true, 'OR');
            $criteria->addSearchCondition("t.meta_title", $_REQUEST['search-free'], true, 'OR');
            $criteria->addSearchCondition("t.meta_keywords", $_REQUEST['search-free'], true, 'OR');
            $criteria->addSearchCondition("t.meta_description", $_REQUEST['search-free'], true, 'OR');
        }

        if (!empty($_REQUEST['id'])) {
            $count = MedbookingClinic::model()->count($criteria);
            $pages = new CPagination($count);
        } elseif (!empty($_REQUEST['doctor_id'])) {
            $count = Yii::app()->db2->createCommand()
                ->select('count(*)')
                ->from('reviews')
                ->where('doctor_id = :id and status = 1', [':id' => $_REQUEST['doctor_id']])
                ->queryScalar();
            $pages = new CPagination($count);
        } else {
            $count = MedbookingClinic::model()->count($criteria);
            $pages = new CPagination($count);
        }

        if (!empty($_REQUEST['pageSize'])) {
            $pages->pageSize = intval($_REQUEST['pageSize']);
        } else {
            $pages->pageSize = 10;
        }
        $pages->applyLimit($criteria);
        $pager = '';
        if ($pages->pageCount > 1) {
            ob_start();
            $this->widget(
                'CLinkPager',
                array(
                    'pages'          => $pages,
                    'header'         => '',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',
                    'nextPageLabel'  => '>',
                    'prevPageLabel'  => '<',
                    'maxButtonCount' => 5,
                )
            );
            $pager = ob_get_clean();

        }
        /** @var MedbookingClinic[] $data */
        $collection = MedbookingClinic::model()->findAll($criteria);
        // Кол-во комментов и докторов
        $ids = [];
        foreach ($collection as $model) {
            $ids[] = $model->id;
        }
        if ($ids) {
            $tmp = [];
            $idsString = implode(',', $ids);
            $countReviews = Yii::app()->db2->createCommand(
                "select clinic_id, count(*) as qty from reviews
                    where clinic_id in ($idsString) group by clinic_id"
            )->queryAll();
            $countDoctors = Yii::app()->db2->createCommand(
                "select clinic_id, count(*) as qty from clinic_doctor
                    where clinic_id in ($idsString) group by clinic_id"
            )->queryAll();

            foreach ($countReviews as $row) {
                $tmp[$row['clinic_id']] = $row['qty'];
            }
            $countReviews = $tmp;

            foreach ($countDoctors as $row) {
                $tmp[$row['clinic_id']] = $row['qty'];
            }
            $countDoctors = $tmp;
            unset($tmp, $ids, $idsString);
        }
        $data = [];
        $key = 0;
        foreach ($collection as $key => $v) {
            $_data = [];
            $_data['id'] = $v->id;
            $_data['title'] = !empty($v->title) ? $v->title : '';
            $_data['translit'] = !empty($v->translit) ? $v->translit : '';
            $_data['url'] = !empty($v->url) ? $v->url : '';
            $_data['address'] = !empty($v->address) ? $v->address : '';
            $_data['description'] = !empty($v->description) ? $v->description : '';
            $_data['comm'] = isset($countReviews[$v->id]) ? $countReviews[$v->id] : $v->comm;
            $_data['net'] = !empty($v->network_id) ? $v->network_id : '';
            $_data['body'] = !empty($v->body) ? $v->body : '';
            $_data['lat'] = !empty($v->lat) ? $v->lat : '';
            $_data['lng'] = !empty($v->lng) ? $v->lng : '';
            if (!empty($_REQUEST['id'])) {
                $reviewPages = new CPagination($_data['comm']);
                $reviewPages->pageSize = self::REVIEWS_PAGE_SIZE;
                if (empty($_REQUEST['page'])) {
                    $reviewPages->currentPage = 0;
                }
                $_data['reviews'] = $v->getReviews(
                    $reviewPages->currentPage * self::REVIEWS_PAGE_SIZE,
                    self::REVIEWS_PAGE_SIZE
                );
                $_data['pager'] = '';
                if ($reviewPages->pageCount > 1) {
                    $_data['pager'] = $this->widget(
                        'CLinkPager',
                        array(
                            'pages'          => $reviewPages,
                            'header'         => '',
                            'firstPageLabel' => '<<',
                            'lastPageLabel'  => '>>',
                            'nextPageLabel'  => '>',
                            'prevPageLabel'  => '<',
                            'maxButtonCount' => 5,
                        ),
                        true
                    );

                }
            }
            //Todo: possible should import ImageBehavior extension from medbooking.com to a.medbooking.com
            if (!empty($v->image)) {
                $_data['image'] = '//medbooking.com/files/clinic/photo/' . $v->image;
            }

            $regimeClinic = '';
            if (count($v->getRegimeClinic())):
                foreach ($v->getRegimeClinic() as $k => $value):
                    $regimeClinic .= $k . ": ";
                    $regimeClinic .= $value . "\n";
                endforeach;
            endif;
            $_data['regime'] = $v->getRegimeClinic();
            $_data['regime_clinic'] = $regimeClinic;
            $_data['countDoctors'] = isset($countDoctors[$v->id]) ? $countDoctors[$v->id] : $v->getCountDoctors();
            $subway = $v->getSubway_name();
            $_data['subway_name'] = $subway[0];
            $_data['subway_info'] = $subway[1];
            $_data['status'] = !empty($v->status) ? $v->status : '0';
            $_data['note'] = !empty($v->note) ? $v->note : '';
            $rate = str_replace(",", ".", sprintf("%.1f", !empty($v->rate10) ? round($v->rate10, 2) : 0));
            $_data['rate'] = $rate >= 10 ? '10' : $rate;

            /** Medbooking clinic services */
            if (!empty($_REQUEST['id'])) {
                $criteria = new CDbCriteria;
                $criteria->select = [
                    'id',
                    'price',
                    'discount',
                    'service_id',
                ];
                $criteria->addCondition("clinic_id={$v->id}");
                $criteria->with = array(
                    'service' => array(
                        'select' => 'id, title, translit, description, category_id'
                    )
                );
                $serv = MedbookingNodeServicePrice::model()->findAll($criteria);
                foreach ($serv as $skey => $s) {
                    $service = $s->service;
                    $_data['services'][] = [
                        'id'                     => $service->id,
                        'title'                  => $service->title,
                        'translit'               => $service->translit,
                        'description'            => $service->description,
                        'category_id'            => $service->category_id,
                        'current_service_price'  => $s->price,
                        'discount_service_price' => $s->discount ? $s->discount : false
                    ];
                }
            }

            $data[] = $_data;
        }

        return array('data' => $data, 'pager' => $pager, 'sort' => $sort->getOrderBy(), 'count' => $count);
    }
}