<?php

class CheckController extends CController
{

	public function actionServicesJSON($term)
	{
		if( ! empty($term)){
			$criteria = new CDbCriteria;
			$criteria->select = array("id","name","type");
			$criteria->limit = 30;
			$criteria->group = "name";
			$criteria->addCondition('name IS NOT NULL AND name<>""');
			$criteria->addSearchCondition('name', trim($term));
			$data = ClinicPrice::model()->findAll($criteria);
			$list = array();
			if( ! empty($data)){
				foreach($data as $value) {
					$list[$value['id']] = array('title'=>$value['name'], 'id'=>$value['id'], 'type'=>$value['type']);
				}
			}
			echo $_REQUEST['callback'].'('.CJSON::encode($list).')';
		}
	}
}
