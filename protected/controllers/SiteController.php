<?php

class SiteController extends MbController
{

    public $layout='//layouts/column_index';

	public function filterCheckTheme($filterChain)
	{
		if (isset($_REQUEST['theme'])) {
			Yii::app()->session->add("theme", $_REQUEST['theme']);
		}
		$theme = Yii::app()->session->get("theme");
		if ($theme) Yii::app()->theme = $theme;
		$filterChain->run();
	}

	public function filters()
	{
		return array(
			'checkTheme'
		);
	}

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error){
            if(Yii::app()->request->isAjaxRequest){
                echo $error['message'];
            } else{
                $this->render('error',$error);
            }
        }
    }

    public function actionRate()
    {
        $domain=addslashes($_POST['sn']);
        $type=addslashes($_POST['mode'])."_id";
        $data=Yii::app()->db->createCommand("SELECT COUNT(*) AS qty, {$type} AS {$type} FROM api_record WHERE status=3 AND domain='{$domain}' AND {$type} IS NOT NULL GROUP BY {$type}")->queryAll();
        echo CJSON::encode($data);
    }

    public function actionRateId($id)
    {
        $domain=addslashes($_POST['sn']);
        $type=addslashes($_POST['mode'])."_id";
        $data=Yii::app()->db->createCommand("SELECT COUNT(*) AS qty, {$type} AS {$type} FROM api_record WHERE {$type}='".addslashes($id)."' AND status=3 AND domain='{$domain}' AND {$type} IS NOT NULL GROUP BY {$type}")->queryAll();
        echo CJSON::encode($data);
    }

    public function actionRateFull()
    {
        $domain=addslashes($_POST['sn']);
        $type=addslashes($_POST['mode']);
        $data=Yii::app()->db->createCommand("SELECT COUNT(*) AS qty, {$type} AS {$type} FROM api_record WHERE status=3 AND domain='{$domain}' AND {$type} IS NOT NULL GROUP BY {$type}")->queryAll();
        echo CJSON::encode($data);
    }

    public function actionRateWithoutDate()
    {
        $domain=addslashes($_POST['sn']);
        $dataRec=date("Y-m-d",time()-3600*24);
        $type=addslashes($_POST['mode'])."_id";
        $data=Yii::app()->db->createCommand("SELECT COUNT(*) AS qty, {$type} AS {$type} FROM api_record WHERE DATE(create_time)='".addslashes($dataRec)."' AND domain='{$domain}' AND {$type} IS NOT NULL GROUP BY {$type}")->queryAll();
        echo CJSON::encode($data);
    }

    public function actionRateWithoutDateId($id)
    {
        $domain=addslashes($_POST['sn']);
        $dataRec=date("Y-m-d",time()-3600*24);
        $type=addslashes($_POST['mode'])."_id";
        $data=Yii::app()->db->createCommand("SELECT COUNT(*) AS qty, {$type} AS {$type} FROM api_record WHERE {$type}='".addslashes($id)."' AND DATE(create_time)='".addslashes($dataRec)."' AND domain='{$domain}' AND {$type} IS NOT NULL GROUP BY {$type}")->queryAll();
        echo CJSON::encode($data);
    }

    public function actionRateWithout()
    {
        $domain=addslashes($_POST['sn']);
        $type=addslashes($_POST['mode'])."_id";
        ;
        $data=Yii::app()->db->createCommand("SELECT COUNT(*) AS qty, {$type} AS {$type} FROM api_record WHERE domain='{$domain}' AND {$type} IS NOT NULL GROUP BY {$type}")->queryAll();
        echo CJSON::encode($data);
    }

    public function actionRateWithoutId($id)
    {
        $domain=addslashes($_POST['sn']);
        $type=addslashes($_POST['mode']);
        $data=Yii::app()->db->createCommand("SELECT COUNT(*) AS qty, {$type} AS {$type} FROM api_record WHERE {$type}='".addslashes($id)."' AND domain='{$domain}' AND {$type} IS NOT NULL GROUP BY {$type}")->queryAll();
        echo CJSON::encode($data);
    }

    public function actionIndex()
    {
		Yii::app()->theme = 'cpanew';
        $model=new LoginForm;

        /* двух-факторная авторизация */
        do {
            if(!empty(Yii::app()->user->id)) {
                break;
            }
            if(!isset($_POST['LoginForm'])){
                break;
            }
            $session = Yii::app()->session;

            /* сессию для ОТП не устанавливали -- надо проверить логин-пароль */
            if (empty($session['LoginForm.user']) || isset($_POST['LoginForm']['scenario']) && 'start' == $_POST['LoginForm']['scenario']) {
                $log = new Call();
                $log->name = empty($_POST['LoginForm']['username'])?'':$_POST['LoginForm']['username'];
                $log->async = empty($_POST['loc'])?'':$_POST['loc'];
                $log->create_time = date('Y-m-d H:i:s');
                $log->mp3 = json_encode($_SERVER);
                $log->context = json_encode($_REQUEST);
                $log->save();
                $model->scenario = 'start';
                $model->attributes = $_POST['LoginForm'];
                if (!$model->validate()) {
                    break;
                }
                if (!$model->login()) {
                    break;
                }

                /* 2х-факторная только для "избранных" пользователей */
                if (!in_array(Yii::app()->user->id, Yii::app()->params['smsOnAuthUsers'])) {
                    break;
                }

                /* сохраняем пользователя в сессию */
                $user = MbUser::model()->findByPk(Yii::app()->user->id);
                $session['LoginForm.user'] = array(
                    'username' => $model->username,
                    'password' => $model->password,
                    'phone' => $user->telephone,
                );
                $model->username = null;
                $model->password = null;
                Yii::app()->user->logout(false);
            }

            $model->otp_phone = $session['LoginForm.user']['phone'];
            /* если ОТП в сессии ещё нет, либо запрос на переотправку */
            if(empty($session['LoginForm.otp']) || !empty($_POST['LoginForm']['otp_send'])) {
                $model->scenario = 'otp_send';
                $model->attributes = $_POST['LoginForm'];
                /* валидация для данного сценария занимается отправкой смс */
                if (!$model->validate()) {
                    $model->scenario = 'otp_check';
                    break;
                }

                /* переходим к вводу ОТП */
                $model->scenario = 'otp_check';
                break;
            }

            /* OTP выслан -- надо проверить */
            $model->scenario = 'otp_check';
            $model->attributes = $_POST['LoginForm'];
            if (!$model->validate()) {
                break;
            }

            /* ОТП проверили -- наконец заходим */
            $model->username = $session['LoginForm.user']['username'];
            $model->password = $session['LoginForm.user']['password'];
            /* прибьём вспомогательные сессии */
            unset($session['LoginForm.user']);
            unset($session['LoginForm.otp']);

            $model->scenario = 'start';
            if (!$model->validate() || !$model->login()) {
                break;
            }

        }while(false);

        /* проверим доступы, направим куда надо */
        if(!empty(Yii::app()->user->id)) {
            if (Yii::app()->user->checkAccess("Partner")) {
                $this->redirect(array('/mbTable'));
            } else {
                $model->scenario = 'start';
                Yii::app()->user->logout(false);
            }
        }

        $this->render('login',array('model'=>$model));
    }

	public function actionRegister()
	{
		if(Yii::app()->user->id){
			$this->redirect(array('/mbRecord'));
		}
		Yii::app()->theme = 'cpanew';
		$partner = new Partner;
		$user = new MbUser;
		if(isset($_POST['Partner']) AND isset($_POST['MbUser'])){
			$user->setScenario('registration');
			$partner->attributes = $_POST['Partner'];
			$user->attributes = $_POST['MbUser'];
			$user->role = 'Partner';
			if ($user->save()) {
				$partner->name = $user->fio;
				$partner->title = $user->fio;
				$partner->ident_id = $user->primaryKey;
				$partner->domain = 'medbooking.com';
				$partner->contact_phone = $user->telephone;
				if ($partner->save()) {
					$model = new LoginForm;
					$model->username = $user->email;
					$model->password = $user->password;
					if($model->validate() AND $model->login()){
						if (Yii::app()->user->checkAccess("Partner")) {
							$this->redirect(array('/mbTable'));
						} else {
							Yii::app()->user->logout(false);
						}
					}
				}
			}
		}
		$this->render('register', array('partner' => $partner, 'user' => $user));
	}

	public function actionSupport()
	{
        header('Access-Control-Allow-Origin: *');
        Yii::app()->theme = 'cpanew';
        $model = new SupportForm();
        if ( ! empty($_POST['SupportForm'])) {
            $model->attributes = $_POST['SupportForm'];
			if ($model->validate()) {
                    $model->sendMessage();
				if ( ! empty($_POST['ajax']) OR Yii::app()->request->isAjaxRequest) {
					echo CJSON::encode(array('html' => 'done'));
					Yii::app()->end();
				} else {
					$this->redirect(array('index'));
				}
			}
		}
		if ( ! empty($_POST['ajax']) OR Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array('html' => $this->renderPartial('support', array('model' => $model), TRUE, TRUE)));
			Yii::app()->end();
		} else {
			$this->render('support', array('model' => $model));
		}
	}

    public function actionLogout()
    {
	    Yii::app()->session->remove("partnerID");
        Yii::app()->user->logout(false);
        $this->redirect(array('index'));
    }

    /**
     * Headers
     */
    private function head()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
        header('Access-Control-Allow-Credentials: true');
        header("Content-Type: application/json;");
        header('Access-Control-Max-Age: 1800');
    }

    /**
     * Запись в клинику
     * @throws CHttpException
     */
    public function actionSend()
    {
        $this->head();
        $model=new MbRecord;
        if(Yii::app()->user->id){
            if(Yii::app()->user->checkAccess("Callcenter")){
                $model->setScenario('admin');
            } else{
                $model->setScenario('user');
            }
        } else{
            $model->setScenario('guest');
        }
        if(isset($_POST['AApiRecord'])){
            $model->attributes=$_POST['AApiRecord'];
            if(!empty($_SERVER['HTTP_REFERER'])){
	            $model->referer = $_SERVER['HTTP_REFERER'];
                if(!empty($_POST['host'])){
                    $url_source=parse_url($_POST['host']);
                    $model->source=$url_source['host'];
                }
                $url=parse_url($_SERVER['HTTP_REFERER']);
                $model->domain=$url['host'];
                if(!empty($model->domain)&&$model->domain=='medbooking.com'&&!empty($_POST['sn'])&&$_POST['sn']=='child.medbooking.com'){
                    $model->domain='child';
                }
            }
            $model->fromsite=1;
            $route=$_POST['AApiRecord']['route'];
	        $model->first_time = FALSE;
	        if( ! empty($_POST['firstVisitTime']) AND $_POST['firstVisitTime'] != 'undefined'){
		        if ((Yii::app()->params['timeFromFirstVisit'] + $_POST['firstVisitTime']) > time())
			        $model->first_time = TRUE;
	        }
	        if( ! empty($_POST['admitad']) AND $_POST['admitad'] != 'undefined')
		        $model->admitad = 11;
            if($model->save()){
                $model=MbRecord::model()->findByPk($model->id);
                $comments='';
                $comments.=(!empty($model->telephone)?"Контактный телефон: ".$model->telephone."<br>"."\r\n":'');
                $comments.=(!empty($model->name)?"ФИО пациента: ".$model->name."<br>"."\r\n":'');
                $comments.=(!empty($model->email)?"E-mail пациента: ".$model->email."<br>"."\r\n":'');
                $comments.=(!empty($model->description)?"Суть вопроса: ".$model->description."<br>"."\r\n":'');
                $comments.=(!empty($model->create_time)?"Время подачи заявки: ".$model->create_time."<br>"."\r\n":'');
                $comments.=(!empty($model->uid)?"Пользователь в системе: ".$model->u->nameperson."<br>"."\r\n":'');
                $comments.=(!empty($model->doctor_name)?"Доктор: ".$model->doctor_name."<br>"."\r\n":'');
                $comments.=(!empty($model->clinic_name)?"Клиника: ".$model->clinic_name."<br>"."\r\n":'');
                $comments.=(!empty($model->timeslot_id)?"Метка времени: ".$model->timeslot_id."<br>"."\r\n":'');
                $comments.=(!empty($model->reception_time)?"Время записи: ".$model->reception_time."<br>"."\r\n":'');
                $comments.=(!empty($model->author_id)?"Пользователь в системе: ".$model->а->nameperson."<br>"."\r\n":'');
                $comments.=(!empty($model->domain)?"Запись отправлена c сайта ".$model->domain:'');
                if(!empty(Yii::app()->params['adminEmail'])&&!empty($model->domain)){
                    $this->mailto('Запись с сайта '.$model->domain,$comments,Yii::app()->params['adminEmail']);
                    $this->mailto('Запись с сайта '.$model->domain,$comments,Yii::app()->params['testEmail']);
                }
                $template=MbSmsTemplate::model()->findByAttributes(array('title'=>'SENDER'),"CURTIME()>time_start AND CURTIME()<time_end");
                if(!empty($template->primaryKey)){
                    $_sms_message=$template->message;
                } else{
                    $_sms_message='Спасибо за запись на сайте, мы свяжемся с вами в течение 15 минут!';
                }
                echo CJSON::encode(array('url'=>$route,'message'=>$_sms_message));
                $smsSend=new MbRecordSms;
                $smsSend->sms_msisdn=$model->telephone;
                $smsSend->sms_send_admin=1;
                if($model->domain=='timetovisit.ru'){
                    $smsSend->sms_shortcode='timetovisit';
                } elseif($model->domain=='medbooking.com'){
                    $smsSend->sms_shortcode='medbooking';
                } elseif($model->domain=='m.medbooking.com'){
                    $smsSend->sms_shortcode='medbooking';
                } elseif($model->domain=='diagnostika.medbooking.com'){
                    $smsSend->sms_shortcode='diagnostika';
                } elseif($model->domain=='child'){
                    $smsSend->sms_shortcode='child';
                } elseif($model->domain=='testpuls.ru'){
                    $smsSend->sms_shortcode='testpuls';
                } elseif($model->domain=='fromed.ru'){
                    $smsSend->sms_shortcode='fromed';
                }
                $smsSend->sms_text=$_sms_message;
                $smsSend->message=$_sms_message;
                $smsSend->status=0;
                $smsSend->record_id=$model->primaryKey;
                if(!empty($model->author_id)){
                    $smsSend->author_id=$model->author_id;
                }
                if($smsSend->save()){
                    $smsSend->sendSms();
                }
            } else{
                echo CActiveForm::validate($model);
            }
        } else{
            throw new CHttpException(404,'Страница не найдена #21.');
        }
    }

    /**
     * Шаг 1
     * @throws CHttpException
     */
    public function actionStep1()
    {
        $this->head();
        $model=new MbRecordStep1;
        $model->setScenario('step1');
        if(isset($_POST['MbRecord'])){
            $model->attributes=$_POST['MbRecord'];
            if($model->validate()){
                echo CJSON::encode(1);
            } else{
                echo CActiveForm::validate($model);
            }
        } else{
            throw new CHttpException(404,'Страница не найдена.');
        }
    }

    /**
     * Шаг 2
     * @throws CHttpException
     */
    public function actionStep2()
    {
        $this->head();
        $model=new MbRecordStep1;
        $model->setScenario('step2');
        if(isset($_POST['AApiRecord'])){
            $model->attributes=$_POST['AApiRecord'];
            if($model->validate()){
                echo CJSON::encode(1);
            } else{
                echo CActiveForm::validate($model);
            }
        } else{
            throw new CHttpException(404,'Страница не найдена.');
        }
    }

    /**
     * Запись в клинику
     * @throws CHttpException
     */
    public function actionCall()
    {
        $this->head();
        $model=new MbCall;
        $model->setScenario('api');
        if(!empty($_POST['AApiCall']['telephone'])){
            $model->attributes=$_POST['AApiCall'];
            if(!empty($_SERVER['HTTP_REFERER'])){
                $url=parse_url($_SERVER['HTTP_REFERER']);
                $model->domain=$url['host'];
            }
            if(!empty($_POST['AApiCall']['description'])){
                $model->message=$_POST['AApiCall']['description'];
            }
	        if( ! empty($_POST['firstVisitTime']) AND $_POST['firstVisitTime'] != 'undefined') {
		        if ((Yii::app()->params['timeFromFirstVisit'] + $_POST['firstVisitTime']) > time())
			        $model->first_time = TRUE;
	        }
	        if( ! empty($_POST['admitad']) AND $_POST['admitad'] != 'undefined')
		        $model->admitad = 11;
            if($model->save()){
                $model=MbCall::model()->findByPk($model->id);
                $comments='';
                $comments.=(!empty($model->telephone)?"Контактный телефон: ".$model->telephone."<br>"."\r\n":'');
                $comments.=(!empty($model->name)?"ФИО пациента: ".$model->name."<br>"."\r\n":'');
                $comments.=(!empty($model->message)?"Сообщения: ".$model->message."<br>"."\r\n":'');
                $comments.=(!empty($model->create_time)?"Время подачи заявки: ".$model->create_time."<br>"."\r\n":'');
                $comments.="Запись отправлена c сайта ".$model->domain;
                if(!empty(Yii::app()->params['adminEmail'])&&!empty($model->domain)){
                    $this->mailto('Звонок с сайта '.$model->domain,$comments,Yii::app()->params['adminEmail']);
                    $this->mailto('Звонок с сайта '.$model->domain,$comments,Yii::app()->params['testEmail']);
                }
                echo CJSON::encode(array('url'=>'1','message'=>'Спасибо за запись на сайте, мы свяжемся с вами в течение часа!'));
            } else{
                echo CActiveForm::validate($model);
            }
        } else{
            echo CJSON::encode(array('message'=>'Нет данных'));
        }
    }

    /**
     * Отправка СМС. Код авторизации отправляется по СМС
     */
    public function actionSms()
    {
        $this->head();
        if(isset($_POST['AApiRecord']['telephone'])){
            $_telephone=str_replace(array("+7","(",")"," ","-"),array("","","","",""),$_POST['AApiRecord']['telephone']);
        }
        if(!empty($_telephone)){
            $_sn='medbooking';
            if(isset($_POST['sn'])){
                $_sn=$_POST['sn'];
            }
            $value='';
            $_pattern='123456789';
            $counter=strlen($_pattern)-1;
            for($i=0; $i<5; $i++){
                $value.=$_pattern{rand(0,$counter)};
            }
            $sms=new MbSmsSession;
            $sms->sid=$value;
            $sms->save();
            $sms_response=$sms->id;
            $smsSend=new MbRecordSms;
            $smsSend->sms_msisdn=$_telephone;
            $smsSend->sms_shortcode=$_sn;
            $smsSend->sms_text="Код авторизации: ".$value;
            if($smsSend->sendSms()){
                echo CJSON::encode(array('message'=>1,'sms'=>$sms_response));
            } else{
                echo CJSON::encode(array('message'=>'СМС не может быть отправлено'));
            }
        } else{
            echo CJSON::encode(array('message'=>'Укажите контактный телефон'));
        }
    }

}
