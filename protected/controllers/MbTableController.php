<?php

class MbTableController extends MbController
{

    public $layout = '//layouts/column_table';
    public $defaultAction = 'admin';

    public function filters()
    {
        return array(
            'accessControl',
            'checkTheme'
        );
    }

    public function beforeAction($action)
    {
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . "/css/modal.bootstrap.min.css");
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/modal.bootstrap.min.js',CClientScript::POS_END);
        return parent::beforeAction($action);
    }

    public function filterCheckTheme($filterChain)
    {
        if ((Yii::app()->user->checkAccess("Administrator") OR Yii::app()->session->get(
                    "theme"
                )) AND isset($_REQUEST['theme'])
        ) {
            Yii::app()->session->add("theme", $_REQUEST['theme']);
        }
        $theme = Yii::app()->session->get("theme");
        if ($theme) {
            Yii::app()->theme = $theme;
        }
        Yii::app()->theme = 'cpanew';
        $filterChain->run();
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'expression' => 'Yii::app()->user->checkAccess("Partner")',
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionAdmin()
    {
        $array = $this->partnerPerform();
        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(
                array(
                    'tbody' => $this->renderPartial(
                        'data',
                        array(
                            'model' => $array['data']
                        ),
                        true,
                        false
                    ),
                    'pages' => $this->renderPartial(
                        '_pages',
                        array(
                            'sort'  => $array['sort'],
                            'pages' => $array['pages'],
                            'count' => $array['count']
                        ),
                        true,
                        false
                    )
                )
            );
            Yii::app()->end();
        } else {
            $this->render('admin', $array);
        }
    }

    public function partnerPerform()
    {
        $request = Yii::app()->request;

        $date_start = $request->getQuery('date_start');
        $date_end = $request->getQuery('date_end');
        $status = $request->getQuery('status');
        $limit = $request->getQuery('limit');

        $date = new DateTime();
        $date->modify('first day of this month');
        if (!empty($date_start)) {
            try {
                $_date_start = new DateTime($date_start);
            } catch(Exception $e) {
                $_date_start = $date;
            }
            $_date_start = $_date_start->format('Y-m-d');
        } else {
            $_date_start = $date->format('Y-m-d');
        }
        $date->modify('last day of this month');
        if (!empty($date_end)) {
            try {
                $_date_end = new DateTime($date_end);
            } catch(Exception $e) {
                $_date_end = $date;
            }
            $_date_end = $_date_end->format('Y-m-d');
        } else {
            $_date_end = $date->format('Y-m-d');
        }
        $_limit = !empty($limit) ? $limit : 10;
        $_status = !empty($status) ? $status : 0;
        if (!Yii::app()->user->checkAccess("Administrator")) {
            $partner = Partner::model()->findByAttributes(array('ident_id' => Yii::app()->user->id));
            $partner->startDate = $_date_start;
            $partner->endDate = $_date_end;
            $ident_id = $partner->primaryKey;
        } else {
            $partner = Partner::model()->findByAttributes(array('ident_id' => Yii::app()->user->id));
            if (!is_object($partner)) {
                $partner = new Partner();
                $partner->usr = MbUser::model()->findByPk(Yii::app()->user->id);
            }
            $partner->startDate = $_date_start;
            $partner->endDate = $_date_end;
            $partner->admin = true;
            $ident_id = $partner->primaryKey;
        }
        $array = $this->ajaxPerform($ident_id, $_date_start, $_date_end, $_status, $_limit);
        $array['partner'] = $partner;
        $array['partners'] = ((Yii::app()->user->checkAccess("Administrator") OR Yii::app()->session->get(
                "partnerID"
            )) ? Partner::model()->findAll() : array());
        $phone = MbDomainPhone::model()->findByAttributes(array('partner_id' => Yii::app()->user->id));
        $array['phone'] = (empty($phone) ? 'нет' : $phone->phone);

        return $array;
    }

    private function ajaxPerform($id, $date_start, $date_end, $status, $limit)
    {
        $isAdmin = Yii::app()->user->checkAccess('Administrator');
        $request = Yii::app()->request;
        $array['model'] = new MbRecord;
        $criteria = new CDbCriteria;
        $criteria->with = array(
            'partner' => array('together' => true)
        );
        $array['sort'] = new CSort('MbRecord');
        $array['date_start'] = !empty($date_start) ? $date_start : '';
        $array['date_end'] = !empty($date_end) ? $date_end : '';
        $array['limit'] = !empty($limit) ? $limit : 10;
        $array['status'] = !empty($status) ? $status : 0;
        $array['ident_id'] = $id;
        $array['sort']->defaultOrder = "t.create_time DESC";
        $array['sort']->attributes = array(
            'id'             => array(
                'asc'   => 't.id',
                'desc'  => 't.id DESC',
                'label' => 'Номер',
            ),
            'create_time'    => array(
                'asc'   => 't.create_time',
                'desc'  => 't.create_time DESC',
                'label' => 'Дата создания',
                'class' => '1'
            ),
            'status5_dt'     => array(
                'asc'   => 't.status5_dt',
                'desc'  => 't.status5_dt DESC',
                'label' => 'Дата записи',
                'class' => '1'
            ),
            'reception_time' => array(
                'asc'   => 't.reception_time',
                'desc'  => 't.reception_time DESC',
                'label' => 'Дата приема',
            ),
            'name'           => array(
                'asc'   => 't.name',
                'desc'  => 't.name DESC',
                'label' => 'Пациент',
            ),
            'telephone'      => array(
                'asc'   => 't.telephone',
                'desc'  => 't.telephone DESC',
                'label' => 'Телефон',
            ),
            'category_name'  => array(
                'asc'   => 't.category_name',
                'desc'  => 't.category_name DESC',
                'label' => 'Услуга',
            ),
            'status'         => array(
                'asc'   => 't.status',
                'desc'  => 't.status DESC',
                'label' => 'Статус',
            ),


        );
        $array['sort']->applyOrder($criteria);
        if (!empty($_REQUEST['MbRecord']['name'])) {
            $criteria->addSearchCondition("t.name", $_REQUEST['MbRecord']['name'], true, "OR");
            $criteria->addSearchCondition("t.telephone", $_REQUEST['MbRecord']['name'], true, "OR");
            $criteria->addSearchCondition("t.service", $_REQUEST['MbRecord']['name'], true, "OR");
            $criteria->addSearchCondition("t.id", $_REQUEST['MbRecord']['name'], true, "OR");
            $array['name'] = $_REQUEST['MbRecord']['name'];
            $_GET['MbRecord']['name'] = $_REQUEST['MbRecord']['name'];
        }
        if (!empty($array['status'])) {
            $criteria->addSearchCondition("t.partner_status", $array['status']);
            $array['model']->status = $status;
        }

        if (!Yii::app()->user->checkAccess("Administrator")) {
            $criteria->addCondition("t.ident_id=" . $id);
        } else {
            $criteria->addCondition("t.ident_id IS NOT NULL AND t.ident_id!='0' AND t.ident_id!='9'");
        }

        $borderDatesFilled = !empty($date_start) && !empty($date_end);
        if ($borderDatesFilled) {
            $criteria->addCondition("DATE(t.create_time)>='" . addslashes($array['date_start']) . "'");
            $criteria->addCondition("DATE(t.create_time)<='" . addslashes($array['date_end']) . "'");
        }
        $criteria->addCondition("t.create_time<='" . date("Y-m-d H:i:s") . "'");
        $criteria->addCondition("t.deleted IS NULL OR t.deleted = 0");
        $array['count'] = MbRecord::model()->count($criteria);
        $array['pages'] = new CPagination($array['count']);
        $array['pages']->pageSize = $array['limit'];
        $array['pages']->applyLimit($criteria);
        $criteria->select = 'id';

        $tmp = MbRecord::model()->findAll($criteria);
        $ids = [];
        foreach ($tmp as $model) {
            $ids[] = $model->id;
        }
        $array['data'] = MbRecord::model()->findAllByPk($ids);
        $array['today'] = date('Y-m-d', strtotime('today'));
        $array['yesterday'] = date('Y-m-d', strtotime('yesterday'));
        $array['week'] = date('Y-m-d', strtotime('-7 days'));
        $array['month'] = date('Y-m-d', strtotime('-1 month'));
        $array['month3'] = date('Y-m-d', strtotime('-3 month'));
        $array['prev_month_first'] = date('Y-m-d', strtotime('first day of previous month'));
        $array['prev_month_last'] = date('Y-m-d', strtotime('last day of previous month'));
        return $array;
    }

    public function actionVisitstatisticlocatinosofuser(){
        $stat = Call::model()->findAll();
        return $this->render(
            'statsVisit', [
                'data'     => $stat,
            ]
        );
    }
    public function actionSites($id = null)
    {
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . "/css/bootstrap.min.css");
        /**
         * @var CHttpRequest $request
         */
        $partnerSite = null;
        if ($id) {
            $partnerSite = PartnerSite::model()->findMy($id, true);
        }
        $request = Yii::app()->request;
        $array = $this->partnerPerform();
        $criteria = PartnerSite::getDefaultCriteria();
        $criteria->addCondition('ident_id=:id');
        $criteria->params = [':id' => Yii::app()->user->id];
        $dataProviderSite = new CActiveDataProvider(
            PartnerSite::model(), [
                'criteria'   => $criteria,
                'pagination' => ["pageSize" => 30],
            ]
        );
        $criteria = PartnerSiteUrl::getDefaultCriteria();
        $criteria->addCondition('site_id=:id');
        $criteria->params = [':id' => (int)$id];
        $dataProviderSiteUrls = new CActiveDataProvider(
            PartnerSiteUrl::model(), [
                'criteria'   => $criteria,
                'pagination' => ["pageSize" => 30],
            ]
        );

        if ($partnerSiteData = $request->getPost('PartnerSite')) {
            $partnerSiteModel = new PartnerSite();
            $partnerSiteModel->ident_id = CurrentUser::getId();
            $partnerSiteModel->setAttributes($partnerSiteData);
            if ($partnerSiteModel->save()) {
                $this->refresh();
            } else {
                Yii::app()->user->setFlash('error', Html::errorSummary($partnerSiteModel));
            }
        }

        if (($partnerSiteUrlData = $request->getPost('PartnerSiteUrl')) && $partnerSite) {
            if ($partnerSite->addUrl($partnerSiteUrlData['url'], $partnerSiteUrlData['comment'])) {
                $this->refresh();
            } else {
                Yii::app()->user->setFlash('error', Html::errorSummary($partnerSite));
            }
        }
        $this->render(
            'sites', [
                'dataProviderSite'     => $dataProviderSite,
                'dataProviderSiteUrls' => $dataProviderSiteUrls,
                'partnerSite'          => $partnerSite
            ] + $array
        );
    }

    public function actionDelSiteUrl($id)
    {
        $criteria = PartnerSiteUrl::getDefaultCriteria();
        $criteria->with['site'] = ["alias" => "s"];
        $criteria->addCondition('s.ident_id=:uid');
        $criteria->addCondition('t.id=:id');
        $criteria->params[":uid"] = CurrentUser::getId();
        $criteria->params[":id"] = (int)$id;
        if (!$partnerSiteUrl = PartnerSiteUrl::model()->find($criteria)) {
            throw new CHttpException(404, PartnerSiteUrl::model()->messageNotFound);
        }
        $partnerSiteUrl->delete();
        $this->redirect(Url::referrer());
    }

    public function actionDelSite($id)
    {
        $partnerSite = PartnerSite::model()->findMy($id, true);
        $partnerSite->delete();
        $this->redirect(Url::to('/mbTable/sites'));
    }

    public function actionUpdatePartnerName()
    {
        if (isset($_POST)) {
            $ident_id = Yii::app()->user->id; /*временно*/
            $query = Yii::app()->db->createCommand(
                "UPDATE partner SET name='" . addslashes($_POST['PartnerName']) . "' WHERE ident_id='" . $ident_id . "'"
            )->execute();
            echo CJSON::encode(array('success' => 1));
        }
    }

    public function actionReportByPartners($start = null, $end = null, $by_recorded_date = true)
    {
        $where = ' 1=1 ';
        $params = [];
        if ($start && $end) {
            $field = 'api_record.create_time';
            if ($by_recorded_date) {
                $field = 'status5_dt';
            }
            $where .= ' AND ' . $field . ' BETWEEN :start and :end ';
            $params[':start'] = $start;
            $params[':end'] = $end;
        }

        $sql = "SELECT
                partner.id,
                partner.ident_id,
                partner.name,
                api_record.summ_approved,
                partner.price_all,
                partner.price_analyz,
                partner.price_diag,
                partner.price_stomat,
                api_record.record_type
            FROM api_record
                INNER JOIN partner ON partner.id = api_record.ident_id
            WHERE summ_approved IS NOT NULL
                AND api_record.summ_approved != 0
                AND api_record.partner_status in (2,5,3)
                AND $where
            ORDER BY status5_dt DESC, api_record.create_time DESC;";
        $query = Yii::app()->db->createCommand($sql);
        $query->params = $params;
        $dataReader = $query->query();
        $partner = new Partner;
        $record  = new MbRecord;
        $record->partner_status = MbRecord::PARTNER_STATUS_APPROVE;
        $record->addRelatedRecord('partner', $partner, false);
        $partners = [];
        foreach ($dataReader as $row) {
            $record->record_type   = $row['record_type'];
            $partner->id           = $row['id'];
            $partner->price_all    = $row['price_all'];
            $partner->price_diag   = $row['price_diag'];
            $partner->price_stomat = $row['price_stomat'];
            $partner->price_analyz = $row['price_analyz'];
            if (empty($partners[$row['id']])) {
                $partners[$row['id']] = [
                    'name'           => $row['name'],
                    'credit'         => 0,
                    'debit'          => 0,
                    'record_count'   => 0,
                    'count_by_type'  => [],
                    'debit_by_type'  => [],
                    'credit_by_type' => [],
                ];
            }
            ++$partners[$row['id']]['record_count'];
            $partners[$row['id']]['debit']  += $row['summ_approved'];
            $partners[$row['id']]['credit'] += $record->partnerPrice;
            if (empty($partners[$row['id']]['count_by_type'][$row['record_type']])) {
                $partners[$row['id']]['count_by_type'][$row['record_type']]  = 0;
                $partners[$row['id']]['debit_by_type'][$row['record_type']]  = 0;
                $partners[$row['id']]['credit_by_type'][$row['record_type']] = 0;
            }
            ++$partners[$row['id']]['count_by_type'][$row['record_type']];
            $partners[$row['id']]['debit_by_type'][$row['record_type']]  += $row['summ_approved'];
            $partners[$row['id']]['credit_by_type'][$row['record_type']] += $record->getPartnerPrice();
        }

        $xls = new PHPExcel();
        PHPExcel_Calculation::getInstance()->setCalculationCacheEnabled(false);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $sheet->setTitle('Отчет по партнерам');
        $tableHeader = [
            'Партнер',
            'Кол-во записей',
            'Выплаты партнеру, руб.',
            'Приход от клиник по записям партнера, руб.',
            'ROI, % (Приход - Выплаты)/Выплаты * 100%',
            'А (кол-во записей по анализам)',
            'ROI, % по анализам',
            'Д (кол-во записей по диагностике)',
            'ROI, % по диагностике',
            'О (кол-во записей по общей мелицине)',
            'ROI, % по общей медицине',
            'C (кол-во записей по стамотологии)',
            'ROI, % по стамотологии'
        ];

        foreach ($tableHeader as $index => $cell) {
            $sheet->getColumnDimension(chr(65 + $index))->setAutoSize(true);
            $sheet->setCellValueByColumnAndRow($index, 1, $cell);
        }

        $roi = function ($debit, $credit) {
            return round((($debit - $credit) / $debit) * 100, 2);
        };

        $rowIndex = 2;

        foreach ($partners as $id => $row) {
            $column = 0;
            $roiGroup = $roi($row['debit'], $row['credit']);
            $countAnalyz = isset($row['count_by_type'][3]) ? $row['count_by_type'][3] : 0;
            $roiAnalyz = $countAnalyz ? $roi($row['debit_by_type'][3], $row['credit_by_type'][3]) : '';
            $countDiag = isset($row['count_by_type'][2]) ? $row['count_by_type'][2] : 0;
            $roiDiag = $countDiag ? $roi($row['debit_by_type'][2], $row['credit_by_type'][2]) : '';
            $countAll = isset($row['count_by_type'][1]) ? $row['count_by_type'][1] : 0;
            $roiAll = $countAll ? $roi($row['debit_by_type'][1], $row['credit_by_type'][1]) : '';
            $countStomat = isset($row['count_by_type'][4]) ? $row['count_by_type'][4] : 0;
            $roiStomat = $countStomat ? $roi($row['debit_by_type'][4], $row['credit_by_type'][4]) : '';
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $row['name']);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $row['record_count']);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $row['credit']);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $row['debit']);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $roiGroup);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $countAnalyz);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $roiAnalyz);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $countAll);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $roiAll);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $countDiag);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $roiDiag);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $countStomat);
            $sheet->setCellValueByColumnAndRow($column++, $rowIndex, $roiStomat);
            $rowIndex++;
        }

        $this->writeExcelFile($xls);
    }

    public function actionCreateExel($start, $end, $limit, $status = 0, $page = 0)
    {
        $params = [];
        $isAdmin = Yii::app()->user->checkAccess('Administrator');
        $condition = array();
        $dateField = 'create_time';

        if (!empty($start)) {
            $condition[] = 'DATE(ar.' . $dateField . ')>=:start';
            $params[':start'] = $start;
        }
        if (!empty($end)) {
            $condition[] = 'DATE(ar.' . $dateField . ')<=:end';
            $params[':end'] = $end;
        }
        if (!empty($_REQUEST['MbRecord']['name'])) {
            if (is_numeric($_REQUEST['MbRecord']['name'])) {
                $condition[] = 'ar.id = ' . $_REQUEST['MbRecord']['name'];
            } else {
                $condition[] = 'ar.name LIKE"%' . $_REQUEST['MbRecord']['name'] . '%"';
            }
        }
        if (!empty($status)) {
            $params[':status'] = $status;
            $condition[] = 'ar.partner_status=:status';
        }
        $condition[] = '(ar.deleted IS NULL OR ar.deleted = 0)';

        $xls = new PHPExcel();
        PHPExcel_Calculation::getInstance()->setCalculationCacheEnabled(false);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $sheet->setTitle('Таблица');
        $head_file = array(
            'Номер',
            'Дата создания',
            'На время',
            'Тип',
            'Пациент',
            'Телефон',
            'Цена за заявку',
            'Статус',
            'Тип услуги',
            'URL'
        );
        if ($isAdmin) {
            array_push($head_file, 'Услуга', 'Партнер');
        }
        array_push($head_file, 'Клиника');
        if ($isAdmin) {
            array_push($head_file,  'Статус записи', 'Средний чек клиники');
        }
        $sql = "SELECT
                    ar.referer,
					ar.id,
			        ar.create_time,
			        ar.reception_time,
			        ar.fromsite,
			        ar.fromcall,
			        ar.name,
			        ar.telephone,
			        ar.clinic_name,
			        ar.status,
			        ar.record_type,
			        ar.category_name AS category_title,
			        rs.service_id,
			        CASE
			            WHEN (ar.category_name != '') THEN ar.category_name
			            WHEN (ar.doctor_name != '') THEN ar.doctor_name
			            ELSE ''
			        END AS category_name,
					CASE
			            WHEN (ar.record_type = '1' AND ar.partner_status = '" . MbRecord::PARTNER_STATUS_APPROVE . "') THEN p.price_all
						WHEN (ar.record_type = '2' AND ar.partner_status = '" . MbRecord::PARTNER_STATUS_APPROVE . "') THEN p.price_diag
						WHEN (ar.record_type = '3' AND ar.partner_status = '" . MbRecord::PARTNER_STATUS_APPROVE . "') THEN p.price_analyz
						WHEN (ar.record_type = '4' AND ar.partner_status = '" . MbRecord::PARTNER_STATUS_APPROVE . "') THEN p.price_stomat
			            ELSE 0
					END AS price,
					ar.partner_status,
					p.name AS partner_name,
                    average_check.average_check
		        FROM api_record AS ar
		        LEFT JOIN api_record_services AS rs ON (rs.record_id = ar.id)
		        LEFT JOIN partner AS p ON (p.id = ar.ident_id)
                LEFT JOIN (
                    SELECT
                        integrator_clinic.medbooking_id clinic_id,
                        SUM(integrator_clinic_reports.summ / integrator_clinic_reports.patient_success) average_check
                    FROM
                        integrator_clinic_reports
                    JOIN
                        integrator_clinic ON integrator_clinic.id = integrator_clinic_reports.clinic_id
                    JOIN
                        integrator_law_reports ON integrator_law_reports.id = integrator_clinic_reports.law_report_id
                    WHERE
                        integrator_law_reports.report_date >= '2015-09-01' AND integrator_law_reports.report_date <= '2015-09-30'
                    GROUP BY
                        integrator_clinic.medbooking_id
                ) average_check ON average_check.clinic_id = ar.clinic_id
		        WHERE " . implode(' AND ', $condition);
        if (!$isAdmin) {
            $partner = Partner::model()->findByAttributes(array('ident_id' => Yii::app()->user->id));
            $sql .= (!empty($condition) ? ' AND ' : '') . 'ar.ident_id=' . $partner->primaryKey;
        } else {
            $sql .= (!empty($condition) ? ' AND ' : '') . 'ar.ident_id != 0 AND ar.ident_id != 9';
        }
        $sql .= ' ORDER BY ar.id DESC';//LIMIT '.$offset.','.$limit;
        $table = Yii::app()->db->createCommand($sql);
        $table->params = $params;
        $table = $table->queryAll();
        $table_new = $this->tableNew($table);
        for ($i = 0; $i < count($head_file); $i++) {
            $sheet->getColumnDimension(chr(65 + $i))->setAutoSize(true);
            $sheet->setCellValueByColumnAndRow($i, 1, $head_file[$i]);
        }
        for ($i = 0; $i < count($table_new); $i++) {
            $column = 0;
            $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['id']);
            $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['create_time']);
            $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['reception_time']);
            $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['type']);
            if ($isAdmin) {
                $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['name']);
                $sheet->setCellValueByColumnAndRow($column++, $i + 2,  $table_new[$i]['telephone']);
            } else {
                $sheet->setCellValueByColumnAndRow($column++, $i + 2, '');
                $sheet->setCellValueByColumnAndRow($column++, $i + 2,  CPhone::getMaskedPhone($table_new[$i]['telephone']));
            }
            $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['price']);
            $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['partner_status']);
            $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['category_name']);
            $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['referer']);
            if ($isAdmin) {
                $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['category_title']);
                $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['partner_name']);
                $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['clinic_name']);
                $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['status']);
                $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['average_check']);
            } else {
                $sheet->setCellValueByColumnAndRow($column++, $i + 2, $table_new[$i]['clinic_name']);
            }
        }

        $this->writeExcelFile($xls);
    }

    private function writeExcelFile(PHPExcel $xls)
    {
        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=report.xls");

        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');
    }

    private function tableNew($table_raw)
    {
        $table = array();
        foreach ($table_raw as $key => $value) {
            if (isset($table[$value['id']]) && $table[$value['id']]['record_type'] == 3) {
                continue;
            }
            $table[$value['id']] = $value;
        }
        reset($table_raw);

        if (Yii::app()->user->checkAccess("Administrator")) {
            $service_ids = array();
            foreach ($table_raw as $key => $value) {
                if ($value['service_id']) {
                    $service_ids[] = $value['service_id'];
                }
            }
            $services_res = MedbookingNodeService::model()->findAllByAttributes(array('id' => $service_ids));
            $services = array();
            foreach ($services_res as $key => $value) {
                $services[$value->id] = $value->title;
            }
        }

        $new_table = array();
        if (!empty($table)) {
            $mbRecord = new MbRecord();
            $key = 0;
            foreach ($table as $value) {
                $new_table[$key]['id'] = $value['id'];
                $new_table[$key]['create_time'] = $value['create_time'];
                $new_table[$key]['reception_time'] = $value['reception_time'];
                $new_table[$key]['type'] = $this->typeText($value['fromsite'], $value['fromcall']);
                $new_table[$key]['name'] = $value['name'];
                $new_table[$key]['telephone'] = !empty($value['telephone']) ? $value['telephone'] : 'Телефон не указан';

                if (Yii::app()->user->checkAccess("Administrator")) {
                    $new_table[$key]['category_title'] = isset($services[$value['service_id']]) ? $services[$value['service_id']] : 'не указана';
                }

                $mbRecord->record_type = $value['record_type'];
                $new_table[$key]['category_name'] = $mbRecord->getPartnerService();

                $new_table[$key]['price'] = !empty($value['price']) ? $value['price'] : '0';
                $new_table[$key]['partner_status'] = $this->statusText($value['partner_status']);
                $new_table[$key]['partner_name'] = $value['partner_name'];
                $new_table[$key]['clinic_name'] = $value['clinic_name'];
                $new_table[$key]['status'] = $value['status'];
                $new_table[$key]['average_check'] = $value['average_check'];
                $new_table[$key]['referer'] = $value['referer'];

                $key++;
            }
        }

        return $new_table;
    }


    private function typeText($site, $call)
    {
        if (!empty($call)) {
            $output = 'звонок';
        } else {
            if (!empty($site)) {
                $output = 'сайт';
            } else {
                $output = 'оператор';
            }
        }

        return $output;
    }

    public function actionRecommendationAdd()
    {
        if (!Yii::app()->user->checkAccess("Administrator")) {
            throw new CHttpForbidden();
        }
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . "/css/bootstrap.min.css");
        $array = $this->partnerPerform();
        $recommendation = new Recommendation();
        if ($_POST) {
            $recommendation->setAttributes($_POST['Recommendation']);
            $recommendation->ident_id = CurrentUser::getId();
            if ($recommendation->save()) {
                return $this->redirect('/mbTable/recommendations');
            }
        }
        $this->render('recommendation-add', array_merge($array, ['recommendation' => $recommendation]));
    }

    public function actionRecommendations()
    {
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . "/css/bootstrap.min.css");
        $array = $this->partnerPerform();
        $dataProvider = Recommendation::model()->search();
        $this->render('recommendations-list', array_merge($array, ['dataProvider' => $dataProvider]));
    }

    /**
     * @param $id
     *
     * @throws CHttpForbidden
     */
    public function actionRecommendationEdit($id)
    {
        if (!Yii::app()->user->checkAccess("Administrator")) {
            throw new CHttpForbidden();
        }
        $recommendation = Recommendation::model()->findByPk($id, '', [], true);
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . "/css/bootstrap.min.css");
        $array = $this->partnerPerform();
        if ($_POST) {
            $recommendation->setAttributes($_POST['Recommendation']);
            $recommendation->ident_id = CurrentUser::getId();
            if ($recommendation->save()) {
                return $this->redirect('/mbTable/recommendations');
            }
        }
        $this->render('recommendation-add', array_merge($array, ['recommendation' => $recommendation]));
    }

    public function actionRecommendationDelete($id)
    {
        if (!Yii::app()->user->checkAccess("Administrator")) {
            throw new CHttpForbidden();
        }
        $recommendation = Recommendation::model()->findByPk($id, '', [], true);
        $recommendation->delete();
        $this->redirect(Url::referrer());
    }

    private function statusText($status)
    {
        switch ($status) {
            case MbRecord::PARTNER_STATUS_WAIT:
                $status = "Требующая ответа";
                break;
            case MbRecord::PARTNER_STATUS_APPROVE:
                $status = "Записан";
                break;
            case MbRecord::PARTNER_STATUS_DECLINE:
                $status = "Отклонен";
                break;
            default:
                $status = "Не указан";
                break;
        }

        return $status;
    }

    public function actionViewAsPartner()
    {
        if ((Yii::app()->user->checkAccess("Administrator") OR Yii::app()->session->get(
                    "partnerID"
                )) AND isset($_POST['ident_id'])
        ) {
            Yii::app()->session->add("partnerID", $_POST['ident_id']);
        }
        $this->redirect(Yii::app()->createUrl('mbTable'));
    }

    public function actionLogoutAsPartner()
    {
        Yii::app()->session->remove("partnerID");
        $this->redirect(Yii::app()->createUrl('mbTable'));
    }

    public function actionDeleteFromCabinet($id)
    {
        $model = MbRecord::model()->findByPk($id);
        if (!empty($model)) {
            $model->ident_id = 0;
            $model->save();
        }
        $this->redirect(Yii::app()->createUrl('mbTable'));
    }

    public function actionModule($id = false)
    {
        $array = $this->partnerPerform();
        if (($id > 25 OR $id < 1) AND $id != false) {
            $id = 1;
        }
        $array['id'] = $id;
        $this->render('_module', $array);
    }

    public function actionSettings()
    {
        $array = $this->partnerPerform();
        $this->render('_settings', $array);
    }

    public function actionAgreement()
    {
        $array = $this->partnerPerform();
        $this->render('_agreement', $array);
    }

    public function actionDisallow()
    {
        $array = $this->partnerPerform();
        $this->render('_disallow', $array);
    }

    public function actionUpdatePartnerFields()
    {
        if (isset($_POST)) {
            switch ($_POST['field']) {
                case 'name':
                    $partner = Partner::model()->findByAttributes(array('ident_id' => Yii::app()->user->id));
                    if (!empty($partner)) {
                        $partner->name = $_POST['value'];
                        $partner->save();
                        echo CJSON::encode(array('success' => 1));
                    }
                    break;
                case 'phone':
                    $partner = Partner::model()->findByAttributes(array('ident_id' => Yii::app()->user->id));
                    if (!empty($partner)) {
                        $partner->contact_phone = $_POST['value'];
                        $partner->save();
                        echo CJSON::encode(array('success' => 1));
                    }
                    break;
                case 'money':
                    $partner = Partner::model()->findByAttributes(array('ident_id' => Yii::app()->user->id));
                    if (!empty($partner)) {
                        $partner->payment_number = $_POST['value'];
                        $partner->save();
                        echo CJSON::encode(array('success' => 1));
                    }
                    break;
                case 'password':
                    $user = MbUser::model()->findByAttributes(array('uid' => Yii::app()->user->id));
                    if (!empty($user)) {
                        $user->password = $_POST['value'];
                        $user->save();
                        echo CJSON::encode(array('success' => 1));
                    }
                    break;
                default:
                    echo CJSON::encode(array('success' => 0));
                    break;
            }
        } else {
            echo CJSON::encode(array('success' => 0));
        }
    }
    public function actionPrice(){
        $array = $this->partnerPerform();
        $array['status']=4;
        $this->render('price', $array);
    }
}
