<?php

class RegisterController extends MbController
{

    public $layout='//layouts/column_law';
    public $defaultAction='admin';
    public $month_number;
    public $year_number;

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'expression'=>'Yii::app()->user->checkAccess("Accounter")',
            ),
            array('allow',
                'actions'=>array('accounter','updateAccount','createAccount','viewAccount','deleteAccount'),
                'expression'=>'Yii::app()->user->checkAccess("NUser")',
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }
    
    private function loadModel($id)
    {
        $model=  Register::model()->findByPk($id);
        if($model===null){
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }
    
    private function loadModelCommon($id)
    {
        $model= RegisterCommon::model()->findByPk($id);
        if($model===null){
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }

   public function actionCreate()
   {
        $model=new Register;
        if(isset($_POST['Register'])){
            $model->attributes=$_POST['Register'];
            if($model->save()){
                $this->redirect(array('admin'));
            }
        }
        $this->render('create',array('model'=>$model));
   }
   
   public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        if(isset($_POST['Register'])){
            $model->attributes=$_POST['Register'];
            $model->update_time= date('Y-m-d');
            if($model->save()){
                $this->redirect(array('admin'));
            }
        }
        $this->render('create',array('model'=>$model));
    }
    
    public function actionDelete($id)
    {
        $model=$this->loadModel($id);
        if($model->delete()){
            $this->redirect(array('admin'));
        }
    }
    
    public function actionCreateCommon()
   {
        $model=new RegisterCommon;
        if(isset($_POST['RegisterCommon'])){
            $model->attributes=$_POST['RegisterCommon'];
            if($model->save()){
                $this->redirect(array('common'));
            }
        }
        $this->render('createCommon',array('model'=>$model));
   }
   
   public function actionUpdateCommon($id)
    {
        $model=$this->loadModelCommon($id);
        if(isset($_POST['Register'])){
            $model->attributes=$_POST['Register'];
            $model->update_time= date('Y-m-d');
            if($model->save()){
                $this->redirect(array('common'));
            }
        }
        $this->render('createCommon',array('model'=>$model));
    }
    
    public function actionDeleteCommon($id)
    {
        $model=$this->loadModelCommon($id);
        if($model->delete()){
            $this->redirect(array('common'));
        }
    }
    
    public function actionCommon()
    {
        $array=$this->ajaxPerformСommon();
        if(Yii::app()->request->isAjaxRequest){
            echo CJSON::encode(array('filter'=>$this->renderPartial('_filter',array('model'=>$array['model'],'sort'=>$array['sort'],'count'=>$array['count']),true,false),'tbody'=>$this->renderPartial('_admin',array('data'=>$array['data']),true,false),'count'=>$this->renderPartial('//layouts/_admin_count',array('count'=>$array['count']),true,false),'pages'=>$this->renderPartial('_pages',array('sort'=>$array['sort'],'pages'=>$array['pages'],'count'=>$array['count']),true,false)));
            Yii::app()->end();
        } else{
            $this->render('common',$array);
        }
    }
    
    public function actionAdmin()
    {
        $array=$this->ajaxPerform();
        if(Yii::app()->request->isAjaxRequest){
            echo CJSON::encode(array('filter'=>$this->renderPartial('_filter',array('model'=>$array['model'],'sort'=>$array['sort'],'count'=>$array['count']),true,false),'tbody'=>$this->renderPartial('_admin',array('data'=>$array['data']),true,false),'count'=>$this->renderPartial('//layouts/_admin_count',array('count'=>$array['count']),true,false),'pages'=>$this->renderPartial('_pages',array('sort'=>$array['sort'],'pages'=>$array['pages'],'count'=>$array['count']),true,false)));
            Yii::app()->end();
        } else{
            $this->render('admin',$array);
        }
    }
    
    
    private function ajaxPerform()
    {
        $model=new Register;
        $criteria=new CDbCriteria;
        $criteria->group='t.id';
        $sort=new CSort('Register');
        $criteria->with=array('ria','ri');
        $criteria->together=true;
        $sort->defaultOrder="t.id DESC";
        $sort->attributes=array(
            'id'=>array(
                'asc'=>'t.id',
                'desc'=>'t.id DESC',
                'label'=>'№',
            ),
            'title'=>array(
                'asc'=>'t.title',
                'desc'=>'t.title DESC',
                'label'=>'Название',
            ),
            'telephone'=>array(
                'asc'=>'t.telephone',
                'desc'=>'t.telephone DESC',
                'label'=>'Телефон',
            ),
            'integrator_id'=>array(
                'asc'=>'ri.title',
                'desc'=>'ri.title DESC',
                'label'=>'Клиника',
            ),
            'integrator_all_id'=>array(
                'asc'=>'ria.title',
                'desc'=>'ria.title DESC',
                'label'=>'Юр.лицо',
            ),
            'contact'=>array(
                'asc'=>'t.contact',
                'desc'=>'t.contact DESC',
                'label'=>'Контакт',
            ),
        );
        $sort->applyOrder($criteria);
        if(!empty($_REQUEST['Register']['title'])){
            $criteria->addSearchCondition("t.title",$_REQUEST['Register']['title']);
            $model->title=$_REQUEST['Register']['title'];
        }
        if(!empty($_REQUEST['Register']['telephone'])){
            $criteria->addSearchCondition("t.telephone",$_REQUEST['Register']['telephone']);
            $model->telephone=$_REQUEST['Register']['telephone'];
        }
        if(!empty($_REQUEST['Register']['contact'])){
            $criteria->addSearchCondition("t.contact",$_REQUEST['Register']['contact']);
            $model->contact=$_REQUEST['Register']['contact'];
        }
        if(!empty($_REQUEST['Register']['integrator_id'])){
            $criteria->addSearchCondition("ri.title",$_REQUEST['Register']['integrator_id']);
            $model->integrator_id=$_REQUEST['Register']['integrator_id'];
        }
        if(!empty($_REQUEST['Register']['integrator_all_id'])){
            $criteria->addSearchCondition("ria.title",$_REQUEST['Register']['integrator_all_id']);
            $model->integrator_all_id=$_REQUEST['Register']['integrator_all_id'];
        }
        $count=Register::model()->count($criteria);
        $pages=new CPagination($count);
        $pages->pageSize=50;
        $pages->applyLimit($criteria);
        $data=Register::model()->findAll($criteria);
        return array('data'=>$data,'model'=>$model,'sort'=>$sort,'pages'=>$pages,'count'=>$count);
    }
    
    private function ajaxPerformСommon()
    {
        $model=new RegisterCommon;
        $criteria=new CDbCriteria;
        $criteria->group='t.id';
        $sort=new CSort('RegisterCommon');
        $criteria->with=array('ria','ri','rg');
        $criteria->together=true;
        $sort->defaultOrder="t.id DESC";
        $sort->attributes=array(
            'id'=>array(
                'asc'=>'t.id',
                'desc'=>'t.id DESC',
                'label'=>'№',
            ),
            'title'=>array(
                'asc'=>'t.title',
                'desc'=>'t.title DESC',
                'label'=>'Название',
            ),
            'telephone'=>array(
                'asc'=>'t.telephone',
                'desc'=>'t.telephone DESC',
                'label'=>'Телефон',
            ),
            'integrator_id'=>array(
                'asc'=>'ri.title',
                'desc'=>'ri.title DESC',
                'label'=>'Клиника',
            ),
            'integrator_all_id'=>array(
                'asc'=>'ria.title',
                'desc'=>'ria.title DESC',
                'label'=>'Юр.лицо',
            ),
            'register_id'=>array(
                'asc'=>'rg.title',
                'desc'=>'rg.title DESC',
                'label'=>'Реестр',
            ),
            'contact'=>array(
                'asc'=>'t.contact',
                'desc'=>'t.contact DESC',
                'label'=>'Контакт',
            ),
        );
        $sort->applyOrder($criteria);
        if(!empty($_REQUEST['RegisterCommon']['title'])){
            $criteria->addSearchCondition("t.title",$_REQUEST['RegisterCommon']['title']);
            $model->title=$_REQUEST['RegisterCommon']['title'];
        }
        if(!empty($_REQUEST['RegisterCommon']['telephone'])){
            $criteria->addSearchCondition("t.telephone",$_REQUEST['RegisterCommon']['telephone']);
            $model->telephone=$_REQUEST['RegisterCommon']['telephone'];
        }
        if(!empty($_REQUEST['RegisterCommon']['contact'])){
            $criteria->addSearchCondition("t.contact",$_REQUEST['RegisterCommon']['contact']);
            $model->contact=$_REQUEST['RegisterCommon']['contact'];
        }
        $count=  RegisterCommon::model()->count($criteria);
        $pages=new CPagination($count);
        $pages->pageSize=50;
        $pages->applyLimit($criteria);
        $data=RegisterCommon::model()->findAll($criteria);
        return array('data'=>$data,'model'=>$model,'sort'=>$sort,'pages'=>$pages,'count'=>$count);
    }
    
}
