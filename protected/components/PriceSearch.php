<?php

class PriceSearch extends CWidget {

	public $name;

	public function run() {

		$services = Yii::app()->db2->createCommand("
			SELECT
				category.*
			FROM category
			WHERE
				level=2
			GROUP BY category.name
			ORDER BY name
		")->queryAll();
		ob_start();
		$_data = '';
		$name = !empty($this->name) ? $this->name : '';
		foreach ($services as $key => $value) {
			$_data .= '<option value="'.$value['name'].'" '.($name == $value['name'] ? "selected" : "").'>'.$value['name'].'</option>';
		}
		echo $_data;
		ob_end_flush();
	}

}
