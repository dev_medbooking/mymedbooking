<?php

class  CategoryIndex extends CWidget {

	public $id;

    public function run() {

	    $services = Yii::app()->db2->createCommand("
			SELECT
				ns.id AS id,
				ns.title AS service
			FROM node_service AS ns
			GROUP BY ns.id
			ORDER BY ns.title
		")->queryAll();
        ob_start();
	    $_data = '';
	    $id = !empty($this->id) ? $this->id : 0;
        foreach ($services as $key => $value) {
            $_data .= '<option value="'.$value['id'].'" '.($id == $value['id'] ? "selected" : "").'>'.$value['service'].'</option>';
        }
        echo $_data;
        ob_end_flush();
    }

}
