<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    private $_id;

    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $user = MbUser::model()->find(
            'LOWER(email)=:user OR LOWER(nameperson)=:user',
            array(':user' => strtolower($this->username))
        );
        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif ($user->password != $this->password) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $user->uid;
            $lastvisit = date("Y-m-d H:i:s");
            MbUser::model()->updateByPk($this->_id, array('lastvisit' => $lastvisit));
            $this->errorCode = self::ERROR_NONE;
        }

        /**
         * CRUTCH: проверяем различные роли в зависимости от домена
         * если домен не начинается с "crm", считаем, что это Партнёрка
         */
        if ($this->errorCode == self::ERROR_NONE) {
            $role = $user->getRole();
            if (preg_match('#^https?://crm#', Yii::app()->request->hostInfo)) {
                $roles_allowed = Yii::app()->params['allowedRoles']['crm'];
            } else {
                $roles_allowed = Yii::app()->params['allowedRoles']['partner'];
            }
            if (!in_array($role, $roles_allowed)) {
                $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
            }

            $whiteList = Yii::app()->params['allowedAdmins'];
            if ($role == 'Administrator' && !in_array($user->email, $whiteList)) {
                $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
            }
        }

        return $this->errorCode == self::ERROR_NONE;
    }

    public function getId()
    {
        return $this->_id;
    }

}
