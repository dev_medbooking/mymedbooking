<?php

class MbController extends CController
{

    /**
     * Удалено
     * @return type
     */
    public function statusDeleted()
    {
        $count=MbRecord::model()->count("(deleted IS NOT NULL AND deleted!=0)");
        return $count;
    }

    /**
     * Количество в меню
     * @param type $arg
     * @return type
     */
    public function statusCount($arg)
    {
        $count=0;
        $dataTime=date("Y-m-d 00:00:00");
        $dataRec=date("Y-m-d");
        if(empty($arg)){
            $arg=0;
            $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND status=0");
        } elseif($arg=='statistic'){
            $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND status NOT IN (8,9,10,101,102) AND DATE(create_time)>='".addslashes($dataTime)."'");
        } elseif($arg==3){
            $arg=(int)$arg;
            $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND status NOT IN (0,1,8,9) AND DATE(reception_time)='".addslashes($dataRec)."'");
        } elseif($arg==30){
            $arg=(int)$arg;
            $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND status=3 AND DATE(reception_time)='".addslashes($dataRec)."'");
        } elseif($arg==31){
            $arg=(int)$arg;
            $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND status IN (3,104) AND DATE(lead_time)='".addslashes($dataRec)."'");
        } elseif($arg==51){
            $arg=(int)$arg;
            //$count=MbRecord::model()->with(array('fl'))->count("(t.deleted IS NULL OR t.deleted=0) AND fl.title='Не дозвон' AND DATE(t.create_time)='".addslashes($dataRec)."'");
	        $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND (status=51) AND DATE(t.create_time)='".addslashes($dataRec)."'");
        } elseif($arg=='call'){
            $count=MbCall::model()->count("DATE(create_time)>='".addslashes($dataTime)."' AND t.status = 0");
        } elseif($arg==5){
            $arg=(int)$arg;
            $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND DATE(create_time)>='".addslashes($dataTime)."' AND status IN (2,3,5,52)");
        } elseif($arg==7){
            $arg=(int)$arg;
            $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND (status=7)");
        } elseif($arg==8){
            $arg=(int)$arg;
            $count=MbRecord::model()->count(array("condition"=>"(status=8)",'group'=>"telephone"));
        } elseif($arg==81){
            $arg=(int)$arg;
            $count=MbRecord::model()->count(array("condition"=>"(status=8) AND DATE(create_time)>='".addslashes($dataTime)."'",'group'=>"telephone"));
        } else{
            $arg=(int)$arg;
            $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND DATE(create_time)>='".addslashes($dataTime)."' AND status='".addslashes($arg)."'");
        }
        return $count;
    }

    /**
     * Генерирование случайной строки
     * @param type $n
     * @param type $pattern
     * @return string
     */
    public static function generate($n=5,$pattern=1)
    {
        $key='';
        $_pattern='123456789';
        if($pattern==2) $_pattern='0123456789abcdefghijgklmnopqrstuvwxyz';
        elseif($pattern==3) $_pattern='0123456789abcdef';
        $counter=strlen($_pattern)-1;
        for($i=0; $i<$n; $i++) $key.=$_pattern{rand(0,$counter)};
        return $key;
    }

    /**
     * Количество по хосту
     * @param type $arg
     * @param type $host
     * @return type
     */
    public function statusCountHost($arg,$host)
    {
        $count=0;
        $modelHost=MbDomain::model()->findByPk($host);
        $dataTime=date("Y-m-d 00:00:00");
        $dataRec=date("Y-m-d");
        if(!empty($modelHost->title)){
            if(empty($arg)){
                $arg=0;
                $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND status=0 AND domain='".addslashes($modelHost->title)."'");
            } elseif($arg=='statistic'){
                $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND status NOT IN (8,9,10) AND DATE(create_time)>='".addslashes($dataTime)."' AND domain='".addslashes($modelHost->title)."'");
            } elseif($arg==3){
                $arg=(int)$arg;
                $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND status NOT IN (0,1,8,9) AND DATE(reception_time)='".addslashes($dataRec)."' AND domain='".addslashes($modelHost->title)."'");
            } elseif($arg==30){
                $arg=(int)$arg;
                $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND status=3 AND DATE(reception_time)='".addslashes($dataRec)."' AND domain='".addslashes($modelHost->title)."'");
            } elseif($arg==31){
                $arg=(int)$arg;
                $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND status IN (3, 104) AND DATE(lead_time)='".addslashes($dataRec)."' AND domain='".addslashes($modelHost->title)."'");
            } elseif($arg==51){
                $arg=(int)$arg;
                //$count=MbRecord::model()->with(array('fl'))->count("(t.deleted IS NULL OR t.deleted=0) AND fl.title='Не дозвон' AND DATE(create_time)='".addslashes($dataRec)."' AND domain='".addslashes($modelHost->title)."'");
	            $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND (status=51) AND domain='".addslashes($modelHost->title)."'");
            } elseif($arg==5){
                $arg=(int)$arg;
                $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND DATE(create_time)>='".addslashes($dataTime)."' AND status IN (2,3,5,52) AND domain='".addslashes($modelHost->title)."'");
            } elseif($arg==7){
                $arg=(int)$arg;
                $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND (status=7) AND domain='".addslashes($modelHost->title)."'");
            } elseif($arg==8){
                $arg=(int)$arg;
                $count=MbRecord::model()->count(array("condition"=>"(status=8) AND domain='".addslashes($modelHost->title)."'",'group'=>"telephone"));
            } elseif($arg==81){
                $arg=(int)$arg;
                $count=MbRecord::model()->count(array("condition"=>"(status=8) AND DATE(create_time)>='".addslashes($dataTime)."' AND domain='".addslashes($modelHost->title)."'",'group'=>"telephone"));
            } else{
                $arg=(int)$arg;
                $count=MbRecord::model()->count("(deleted IS NULL OR deleted=0) AND DATE(create_time)>='".addslashes($dataTime)."' AND status='".addslashes($arg)."' AND domain='".addslashes($modelHost->title)."'");
            }
        }
        return $count;
    }

    /**
     * Количество звонков
     * @param type $host
     * @return type
     */
    public function statusCountCall($host)
    {
        $count=0;
        $modelHost=MbDomain::model()->findByPk($host);
        $dataTime=date("Y-m-d 00:00:00");
        if(!empty($modelHost->title)){
            $count=MbCall::model()->count("DATE(create_time)>='".addslashes($dataTime)."' AND domain='".addslashes($modelHost->title)."'");
        }
        return $count;
    }

    /**
     * Отправка сообщения
     * @param type $_subject
     * @param type $_message
     * @param type $mail
     * @return type
     */
    public static function mailto($_subject,$_message,$mail)
    {
        $subject='=?utf-8?B?'.base64_encode($_subject).'?=';
        $message=iconv("utf-8","koi8-r//TRANSLIT",$_message);
        $header='';
        $header.='MIME-Version: 1.0'."\r\n";
        $header.="Content-type: text/html; charset=koi8-r"."\r\n";
        $header.="Date: ".date("r")."\r\n";
        $header.="From: noreply@api.medbooking.com"."\r\n";
        $header.="To: ".$mail."\r\n";
        $header.="Subject: =?utf-8?B?".base64_encode($_subject)."?="."\r\n";
        $header.="Content-type: text/html; charset=koi8-r"."\r\n";
        return mail($mail,$subject,$message,$header);
    }

    /**
     * Нормальная обрезка текста
     * @param type $string
     * @param type $maxlen
     * @return type
     */
    public function cutString($string,$maxlen)
    {
        $len=(mb_strlen($string)>$maxlen)?mb_strripos(mb_substr($string,0,$maxlen),' '):$maxlen;
        $cutStr=mb_substr($string,0,$len);
        return (mb_strlen($string)>$maxlen)?''.$cutStr.' ...':''.$cutStr.'';
    }
}
