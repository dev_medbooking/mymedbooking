<?php

class RelationNames
{
    public static $relations = [];

    public static function getName($currentTable, $relatedTable, $field = null)
    {
        if (isset(self::$relations[$currentTable])) {
            if ($field) {
                foreach (self::$relations[$currentTable] as $key => $values) {
                    if (is_array($values) && isset($values[$relatedTable])) {
                        if ($values[$relatedTable] == $field) {
                            return $key;
                        }
                    }
                }
            } else {
                return array_search($relatedTable, self::$relations[$currentTable]);
            }
        }
        return null;
    }
}