<?php

class CallTouchApiComponent extends CApplicationComponent
{
    const API_HOST = 'api.calltouch.ru';
    const API_PATH = 'calls-service/RestAPI';

    /**
     * @var string
     */
    public $clientApiId;

    /**
     * @var int
     */
    public $siteId;

    /**
     * @throws CException
     */
    public function init()
    {
        foreach(['clientApiId', 'siteId'] as $property) {
            if(empty($this->{$property})) {
                throw new CException(Yii::t('yii', 'Property "{class}.{property}" is not defined.', [
                    '{class}' => get_class($this),
                    '{property}' => $property
                ]));
            }
        }
    }

    /**
     * @param string $url
     * @param array $params
     * @return array
     */
    private function _request($path, array $params)
    {
        $curl = curl_init(call_user_func(function($scheme, $host, $path, array $params) {
            return $scheme . '://' . $host . '/' . implode('/', (array) $path) . '?' . Yii::app()->urlManager->createPathInfo($params, '=', '&');
        }, 'http', static::API_HOST, [
            static::API_PATH,
            $this->siteId,
            $path
        ], array_merge($params, [
            'clientApiId' => $this->clientApiId
        ])));

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true
        ]);

        $content = curl_exec($curl);

        switch(curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
            case 400:
                throw new CException('Синтаксическая ошибка в запросе');
            break;

            case 404:
                throw new CException('Запрошенный ресурс недоступен');
            break;

            case 500:
                throw new CException('Неверные авторизационные данные');
            break;
        }

        return CJSON::decode($content);
    }

    /**
     * получение информации о звонках по источникам
     * @param array $params
     * @return array
     */
    public function getCalls(array $params)
    {
        if(!isset($params['dateFrom'])) {
            $params['dateFrom'] = date('Y-m-01');
        }

        if(!isset($params['dateTo'])) {
            $params['dateTo'] = date('Y-m-t');
        }

        return $this->_request('calls-diary/calls', array_merge($params, [
            'dateFrom' => date('d/m/Y', strtotime($params['dateFrom'])),
            'dateTo'   => date('d/m/Y', strtotime($params['dateTo']))
        ]));
    }

    /**
     * получение информации о уникальных звонках по источникам
     * @param array $params
     * @return array
     */
    public function getUniqueCalls(array $params)
    {
        return $this->getCalls(array_merge($params, ['uniqueOnly' => true]));
    }

    /**
     * отличается от посылки звонков отсутствием ID сайта
     * @param string $url
     * @param array $params
     * @return array
     */
    private function _request_request($path, array $params)
    {
        $curl = curl_init(call_user_func(function($scheme, $host, $path, array $params) {
            return $scheme . '://' . $host . '/' . implode('/', (array) $path) . '?' . Yii::app()->urlManager->createPathInfo($params, '=', '&');
        }, 'http', static::API_HOST, [
            static::API_PATH,
            $path
        ], array_merge($params, [
            'clientApiId' => $this->clientApiId
        ])));

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true
        ]);

        $content = curl_exec($curl);

        switch(curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
            case 400:
                throw new CException('Синтаксическая ошибка в запросе');
                break;

            case 404:
                throw new CException('Запрошенный ресурс недоступен');
                break;

            case 500:
                throw new CException('Неверные авторизационные данные');
                break;
        }

        return CJSON::decode($content);
    }


    /**
     * получение информации о заявках по источникам
     * @param array $params
     * @return array
     */
    public function getRequests(array $params)
    {
        if(!isset($params['dateFrom'])) {
            $params['dateFrom'] = date('Y-m-01');
        }

        if(!isset($params['dateTo'])) {
            $params['dateTo'] = date('Y-m-t');
        }

        return $this->_request_request('requests', array_merge($params, [
            'dateFrom' => date('d/m/Y', strtotime($params['dateFrom'])),
            'dateTo'   => date('d/m/Y', strtotime($params['dateTo']))
        ]));
    }

    /**
     * получение информации о заявках по источникам
     * @param array $params
     * @return array
     */
    public function getOrders(array $params)
    {
        if(!isset($params['dateFrom'])) {
            $params['dateFrom'] = date('Y-m-01');
        }

        if(!isset($params['dateTo'])) {
            $params['dateTo'] = date('Y-m-t');
        }

        if(!isset($params['orderSource'])) {
            $params['orderSource'] = 'REQUEST';
        }

        return $this->_request('orders-diary/orders', array_merge($params, [
            'dateFrom' => date('d/m/Y', strtotime($params['dateFrom'])),
            'dateTo'   => date('d/m/Y', strtotime($params['dateTo'])),
            'orderSource' => $params['orderSource']
        ]));
    }

}