<?php
require(implode(DIRECTORY_SEPARATOR, array(
    __DIR__,
    '..',
    'vendor',
    'autoload.php'
)));
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\EventMessage;

class PamiEventListener implements IEventListener
{
    public function handle(EventMessage $event)
    {
        $log_events = new AsteriskEvents;
        try {
            $log_events->log_events($event);
            //var_dump($event);
            //echo "\n------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }
}