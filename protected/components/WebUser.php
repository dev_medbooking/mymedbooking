<?php

class WebUser extends CWebUser
{

    private $_model;

    public function getUserData()
    {
        $user=$this->loadUser(Yii::app()->user->id);
        $array=array(
            'telephone'=>isset($user->telehone)?$user->telehone:'',
            'email'=>isset($user->email)?$user->email:'',
            'gender'=>isset($user->gender)?$user->gender:'',
        );
        return $array;
    }

    private function loadUser($id=null)
    {
        if($this->_model===null){
            if($id!==null){
                $this->_model=MbUser::model()->findByPk($id);
            }
        }
        return $this->_model;
    }

	public function getId()
	{
		$partnerID = Yii::app()->session->get("partnerID");
		if (true
            && $partnerID
            && Yii::app()->getAuthManager()->checkAccess('Administrator', $this->getState('__id'))
        ) {
            return $partnerID;
        }
		return $this->getState('__id');
	}

}