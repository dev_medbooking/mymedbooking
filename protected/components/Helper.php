<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.08.2015
 * Time: 12:57
 */

class Helper {
    public static function getYiiParam($name, $default = null)
    {
        if ( isset(Yii::app()->params[$name]) )
            return Yii::app()->params[$name];
        else
            return $default;
    }
}