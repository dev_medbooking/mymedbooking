<?php
/**
 * отправка смс
 */

class Sms
{
    protected static $sms_gate = 'http://smsc.ru/sys/send.php';
    protected static $sms_login = 'bronirovanie';
    protected static $sms_password = 'tt289dsn';
    protected static $last_response = null;
    protected static $last_sms_id = null;
    protected static $last_error = '';
    protected static $sender = 'medbooking';

    /**
     * @param $phones -- телефон или массив телефонов
     * @param $message -- сообщение
     * @param bool $sender
     *
     * @return bool
     */
    public static function send($phone, $message, $sender = false)
    {
        Yii::log(__CLASS__ . "::" . __FUNCTION__ . "({$phone}, {$message}, {$sender})");
        $result = false;
        self::$last_response = null;
        self::$last_sms_id = null;
        do {
            $phone = CPhone::normalizePhone($phone);
            if (empty($phone)) {
                self::$last_error = 'Invalid phone';
                break;
            }
            $message = iconv("utf-8", "windows-1251//TRANSLIT", $message);
            if (empty($message)) {
                self::$last_error = 'Message convertion error';
                break;
            }
            if (empty($sender)) {
                $sender = self::$sender;
            }

            $params = array(
                'login' => self::$sms_login,
                'psw' => self::$sms_password,
                'phones' => $phone,
                'mes' => $message,
                'sender' => $sender,
                'cost' => '0',
                'fmt' => '3', // json
            );

            $ch = curl_init();
            $query = http_build_query($params);
            curl_setopt($ch, CURLOPT_URL, self::$sms_gate . '?' . $query);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            Yii::log(__CLASS__ . "::" . __FUNCTION__ . " curl response: '" . $response . "'");
            if (curl_error($ch)) {
                self::$last_error = CVarDumper::dumpAsString(curl_error($ch));
                Yii::log(__CLASS__ . "::" . __FUNCTION__ . " curl last error: " . self::$last_error);
                break;
            }
            curl_close($ch);

            self::$last_response = json_decode($response, true);

            if (empty(self::$last_response)) {
                Yii::log(__CLASS__ . "::" . __FUNCTION__ . " response decode error, but return true");
                $result = true;
                break;
            }

            if (!empty(self::$last_response['error_code'])) {
                self::$last_error = "[" . self::$last_response['error_code'] . "] " . self::$last_response['error'];
                Yii::log(__CLASS__ . "::" . __FUNCTION__ . " response error: " . self::$last_error);
                break;
            }

            self::$last_sms_id = empty(self::$last_response['id']) ? 0 : self::$last_response['id'];
            $result = true;
            break;

        } while (false);

        return $result;
    }

    /**
     * @return null|string|array
     */
    public static function getLastResponse()
    {
        return self::$last_response;
    }

    /**
     * @return null|string|array
     */
    public static function getLastSmsId()
    {
        return (int)self::$last_sms_id;
    }

    /**
     * @return null|string
     */
    public static function getLastError()
    {
        return self::$last_error;
    }

}