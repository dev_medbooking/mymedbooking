<?php

/**
 * This is the model class for table "sessions_tracking".
 *
 * The followings are the available columns in table 'sessions_tracking':
 * @property string $id
 * @property string $host
 * @property string $ip
 * @property string $sessionid
 * @property string $get
 * @property string $utm_medium
 * @property string $utm_source
 * @property string $utm_campaign
 * @property string $utm_term
 * @property string $utm_content
 * @property string $k50id
 * @property string $yclid
 * @property string $datetime
 * @property string $tracking_phone
 * @property string $server
 */
class Tracking extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tracking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sessions_tracking';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('host, ip, sessionid', 'required'),
			array('host,remote_host', 'length', 'max'=>50),
			array('ip,server_addr,remote_addr,tracking_phone', 'length', 'max'=>15),
			array('sessionid', 'length', 'max'=>64),
			array('referer', 'length', 'max'=>512),
			array('datetime', 'length', 'max'=>11),
			array('get, server, project', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, host, referer, ip,  server_addr, remote_addr, remote_host, sessionid, get, utm_medium, utm_source, utm_campaign, utm_term, utm_content, k50id, yclid, datetime,tracking_phone, server, project', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'host' => 'Host',
			'referer' => 'referer',
			'ip' => 'Ip',
			'server_addr' => 'server_addr',
			'remote_addr' => 'remote_addr',
			'remote_host' => 'remote_host',
			'sessionid' => 'Sessionid',
			'get' => 'Get',
			'datetime' => 'Дата записи',
			'tracking_phone' => 'Номер телефона подмены',
			'server' => 'переменные сервера',
			'project' => 'Проект'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('host',$this->host,true);
		$criteria->compare('referer',$this->referer,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('server_addr',$this->server_addr,true);
		$criteria->compare('remote_addr',$this->remote_addr,true);
		$criteria->compare('remote_host',$this->remote_host,true);
		$criteria->compare('sessionid',$this->sessionid,true);
		$criteria->compare('get',$this->get,true);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('tracking_phone',$this->tracking_phone,true);
		$criteria->compare('server',$this->server,true);
		$criteria->compare('project',$this->project,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function write_log()
	{
		//реальный реферер передается в get строке к данному скрипту.
		$this->host = $_SERVER['HTTP_HOST'];
		//$this->server_name = $_SERVER['SERVER_NAME'];
		$this->current_url = (isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"");
		$current_url_array=parse_url($this->current_url);
		//if (isset($current_url_array['query']))
		//	parse_str($current_url_array['query'],$_get);
		$this->ip = (isset($_SERVER['HTTP_X_REAL_IP'])?$_SERVER['HTTP_X_REAL_IP']:$_SERVER['HTTP_X_FORWARDED_FOR']);
		$this->server_addr = (isset($_SERVER['SERVER_ADDR'])?$_SERVER['SERVER_ADDR']:"");
		$this->remote_addr = (isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:"");
		$this->remote_host = (isset($_SERVER['REMOTE_HOST'])?$_SERVER['REMOTE_HOST']:(isset($current_url_array['host'])?$current_url_array['host']:""));
		$this->sessionid = session_id();
		//$this->script_name = urldecode($_SERVER['SCRIPT_NAME']);
		//$this->redirect_url = urldecode($_SERVER['REDIRECT_URL']);
		//$this->request_uri = urldecode($_SERVER['REQUEST_URI']);
		//$this->php_self = urldecode($_SERVER['PHP_SELF']);

		$this->referer = isset($_SERVER['QUERY_STRING'])?base64_decode($_SERVER['QUERY_STRING']):"";
		$this->get = (isset($current_url_array['query'])?$current_url_array['query']:"");

		//$this->utm_medium = (isset($_GET['utm_medium'])?$_GET['utm_medium']:(isset($_get['utm_medium'])?$_get['utm_medium']:""));
		//$this->utm_source = (isset($_GET['utm_source'])?$_GET['utm_source']:(isset($_get['utm_source'])?$_get['utm_source']:""));
		//$this->utm_campaign = (isset($_GET['utm_campaign'])?(string)$_GET['utm_campaign']:(isset($_get['utm_campaign'])?$_get['utm_campaign']:""));
		//$this->utm_term = (isset($_GET['utm_term'])?$_GET['utm_term']:(isset($_get['utm_term'])?$_get['utm_term']:""));
		//$this->utm_content = (isset($_GET['utm_content'])?(string)$_GET['utm_content']:(isset($_get['utm_content'])?$_get['utm_content']:""));
		//$this->k50id = (isset($_GET['k50id'])?$_GET['k50id']:(isset($_get['k50id'])?$_get['k50id']:""));
		//$this->yclid = (isset($_GET['yclid'])?$_GET['yclid']:(isset($_get['yclid'])?$_get['yclid']:""));
		$this->datetime = time();
		$this->tracking_phone = (isset(Yii::app()->request->cookies['phoneData'])?Yii::app()->request->cookies['phoneData']:'');
		/*if (isset($this->tracking_phone)) {
			$cookie_tracking_phone = new CHttpCookie('phoneData', $this->tracking_phone);
			$cookie_tracking_phone->expire = time() + 60*30;
			Yii::app()->request->cookies['phoneData'] = $cookie_tracking_phone;
		}*/

		foreach ($_SERVER as $k=>$v) {
			$this->server.=$k."=".$v."\n";
        }
		$this->save();
		/*if (!$this->save()) {
			echo "ERROR:";
			print_r($this->getErrors());
		}*/
	}

	public function createRow()
	{
		$this->host = $_SERVER['HTTP_HOST'];
		$this->current_url = (isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"");
		$current_url_array=parse_url($this->current_url);
		$this->ip = (isset($_SERVER['HTTP_X_REAL_IP'])?$_SERVER['HTTP_X_REAL_IP']:$_SERVER['HTTP_X_FORWARDED_FOR']);
		$this->server_addr = (isset($_SERVER['SERVER_ADDR'])?$_SERVER['SERVER_ADDR']:"");
		$this->remote_addr = (isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:"");
		$this->remote_host = (isset($_SERVER['REMOTE_HOST'])?$_SERVER['REMOTE_HOST']:(isset($current_url_array['host'])?$current_url_array['host']:""));
		if (!empty($_POST['sid']))
			$this->sessionid = $_POST['sid'];
		else
			$this->sessionid = Yii::app()->db->createCommand("select uuid()")->queryScalar();
		$this->referer = isset($_POST['referer'])?$_POST['referer']:"";
		$this->project = isset($_POST['project'])?$_POST['project']:"";
		$this->get = (isset($current_url_array['query'])?$current_url_array['query']:"");
		$this->datetime = time();
		$this->tracking_phone = (isset(Yii::app()->request->cookies['phoneData'])?Yii::app()->request->cookies['phoneData']:'');
		foreach ($_SERVER as $k=>$v) {
			$this->server.=$k."=".$v."\n";
		}
		if ($this->save()) {
			header('Access-Control-Allow-Origin: '.parse_url((isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:$_SERVER['HTTP_HOST']),PHP_URL_SCHEME).'://'.parse_url((isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:$_SERVER['HTTP_HOST']),PHP_URL_HOST));
			echo $this->sessionid;
		}
		/*else {
			echo "ERROR:";
			print_r($this->getErrors());
		}*/
	}
}