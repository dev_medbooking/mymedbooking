<?php
class DefaultController extends CController
{
	public $session_expires = 60*60*24*365*5; // 5 лет

	public function actionIndex()
	{
		//session_name ("tracking_session_id");
		session_start([
			'cookie_lifetime' => $this->session_expires,
			'read_and_close'  => true,
		]);
		$domain = false;
		//локальная кука
		/*$cookie = new CHttpCookie('tracking_session_id', session_id());
		$cookie->expire = time()+$this->session_expires;
		Yii::app()->request->cookies['tracking_session_id'] = $cookie;
		*/
		if (isset($_SERVER['REMOTE_HOST'])) {
			$domain = $_SERVER['REMOTE_HOST'];
		}
		else if (isset($_SERVER['HTTP_REFERER'])) {
			$domain = parse_url($_SERVER['HTTP_REFERER'],PHP_URL_HOST);
		}
		if ($domain) {
			//кука refferer-сервера
			$cookie_domain = new CHttpCookie('domain_tracking_sid', session_id());
			$cookie_domain->expire = time() + $this->session_expires;
			list($x1, $x2) = array_reverse(explode('.', $domain));
			$cookie_domain->domain = '.' . $x2 . '.' . $x1;
			$cookie_domain->path = "/";
			$cookie_domain->httpOnly = false; //иначе кука не будет доступна javascript
			Yii::app()->request->cookies['domain_tracking_sid'] = $cookie_domain;
		}

/*
		// Create an image, 1x1 pixel in size
		$im=imagecreate(1,1);

		// Set the background colour
		$white=imagecolorallocate($im,255,255,255);

		// Allocate the background colour
		imagesetpixel($im,1,1,$white);

		// Set the image type
		header("content-type:image/jpg");

		// Create a JPEG file from the image
		imagejpeg($im);

		// Free memory associated with the image
		imagedestroy($im);
*/

		header('Content-type: image/png');
		echo gzinflate(base64_decode('6wzwc+flkuJiYGDg9fRwCQLSjCDMwQQkJ5QH3wNSbCVBfsEMYJC3jH0ikOLxdHEMqZiTnJCQAOSxMDB+E7cIBcl7uvq5rHNKaAIA'));
		$track = new Tracking();
		$track->write_log();
		//$this->render('index');
	}

	public function actionCreateTrack()
	{
		$track = new Tracking();
		$track->createRow();
	}
}