<?php

/**
 * Created by PhpStorm.
 * User: serg
 * Date: 31.01.14
 * Time: 15:27
 */
class AContentClinic extends AContentModel
{

    protected $entityType='clinics';
    public $id;
    public $title;
    public $status;
    public $author;
    public $rating;
    public $is_desc;
    public $is_det_desc;
    public $is_img;
    public $is_address;
    public $is_mtitle;
    public $is_mdesc;
    public $is_mkeys;
    public $reviews;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('id,title,status,author,rating,is_desc,is_det_desc,is_img,is_address,is_mtitle,is_mdesc,is_mkeys,reviews','safe'),
            array('id,title,status,author,rating,is_desc,is_det_desc,is_img,is_address,is_mtitle,is_mdesc,is_mkeys,reviews','safe','on'=>'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'title'=>'Наименование',
            'status'=>'Статус',
        );
    }

    public function search($domain)
    {
        return $this->getDomainContent($domain);
    }

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array('id','title','status','author','rating','is_desc','is_det_desc','is_img','is_address','is_mtitle','is_mdesc','is_mkeys','reviews');
    }

}