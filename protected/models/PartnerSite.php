<?php

Yii::import('application.models._base.BasePartnerSite');

class PartnerSite extends BasePartnerSite
{
    /**
     * @param string $className
     *
     * @return PartnerSite
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(), [
                ['host', 'required']
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function relations()
    {
        return array_merge(parent::relations(), []);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), []);
    }

    public function beforeSave()
    {
        if (strpos($this->host, '://') === false) {
            $this->host = 'http://' . $this->host;
        }
        if (!$array = parse_url($this->host)) {
            $this->addError('host', 'Необходимо указать сайт');
            return false;
        };
        if (!isset($array['scheme'])) {
            $this->addError('host', 'Неверно указан адрес сайта');
            return false;
        }
        $this->host = $array['scheme'] . '://' . $array['host'];
        return parent::beforeSave();
    }

    public function findMy($id, $raise = false)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.ident_id=:uid');
        $criteria->addCondition('t.id=:id');
        $criteria->params[':uid'] = CurrentUser::getId();
        $criteria->params[':id'] = (int)$id;
        if (!($model = self::find($criteria)) && $raise) {
            throw new CHttpException(404, $this->messageNotFound);
        } else {
            return $model;
        }
    }

    public function addUrl($url, $comment)
    {
        $partnerSiteUrl = new PartnerSiteUrl();
        $partnerSiteUrl->site_id = $this->id;
        $partnerSiteUrl->url = $url;
        $partnerSiteUrl->comment = $comment;
        if (!$partnerSiteUrl->save()) {
            $this->addErrors($partnerSiteUrl->getErrors());
            return false;
        } else {
            return true;
        }
    }

    public function getUrl($action)
    {
        $url = '';
        if ($action == "delete") {
            $url = Url::to('/mbTable/delSite', ["id" => $this->id]);
        }
        return Url::to($url);
    }
}