<?php

class MbSmsTemplate extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_sms_template';
    }

    public function rules()
    {
        return array(
            array('message, title','required'),
            array('title','length','max'=>255),
            array('message','safe'),
            array('time_start, time_end','required'),
            array('time_start, time_end','type','type'=>'time','timeFormat'=>'hh:mm:ss'),
        );
    }

    public function relations()
    {
        return array(
            'sms_template_s'=>array(self::HAS_MANY,'MbRecordSms','t_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'message'=>'Сообщение',
            'title'=>'Заголовок',
            'time_start'=>'Время от - ',
            'time_end'=>'Время до -',
        );
    }

}