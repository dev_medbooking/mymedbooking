<?php

class MbRecord extends CActiveRecord
{
    //2,4, 5,11, 101,103
    const STATUS_CHECK = 0; // заявка
    const STATUS_REVERT = 1; // отказ
    const STATUS_LOOSE = 2; // не явился
    const STATUS_LEAD = 3; // были на приеме
    const STATUS_DECLINE = 4; // отмена
    const STATUS_RECEPT = 5; // запись
    const STATUS_PREVENT = 6; // преварительная отмена
    const STATUS_LIST = 7; // лист ожидания
    const STATUS_OKTELL = 8; // oktell ожидания
    const STATUS_RERECORD = 11; // перезаписался
    const STATUS_NOT = 51; // недозвон
    const STATUS_EMPTY = 9;    // пустой звонок
    const STATUS_MISSED = 60;    // пропущенный звонок
    const STATUS_NINETY_DAYS = 53; // 90 дней
    const STATUS_ZOON_ORDER = 101; //zoon запись
    const STATUS_ZOON_DECLINE = 102; //zoon отказ
    const STATUS_ZOON_LOOSE = 103; //zoon не явился
    const STATUS_ZOON_LEAD = 104; //zoon был на приеме


    const PARTNER_STATUS_WAIT = 1; // требующие ответа
    const PARTNER_STATUS_APPROVE = 2; // записаные
    const PARTNER_STATUS_DECLINE = 3; // отклоненные

    const SMS_GATE = "http://crm.vr.dev/smsgate/";

    public $addClass = 'MbRecordAdd';
    public $addTemplate = 'MbSmsTemplate';
    public $addSms = 'MbRecordSms';
    public $addSession = 'MbSmsSession';
    public $addUser = 'MbUser';
    public $addDomain = 'MbDomain';
    public $callerid2;
    public $callerid1;
    public $dt;
    public $qty;
    public $tool_checked = 0;
    public $allqty;
    public $infospec;
    public $is_paid;
    public $callproove; // возможность добавить запись в коллцентр
    public $userproove; // возможность получить пользователя
    public $sogl; // отмечено ли пользовательское соглашение для некоторых сценариев
    public $sms = 'sms'; // ID-key session для проверки SMS кода ответа    private $_timecode;
    public $leadCheck = 0;
    public $addressCheck = true;
    public $smsCheck = 1;
    public $_during;
    public $_countTel;
    public $_countTelAll;
    public $_telephoneNext;
    public $_telephoneOut;
    public $qtyTel;
    public $onlyDiff;
    public $_history; // переменная для модели с историей
    public $fail_sms;
    public $error_type;
    public $error_comment;


    public $bad_numbers = array('anonymous', '1111111111', '2222222222', '3333333333', '4444444444', '5555555555', '6666666666', '7777777777', '8888888888', '9999999999', '0000000000', '1234567890', '0987654321');
    public $angry_users = ['9110000001'];

    /**
     * Длительность звонка
     * @return type
     */
    public function getDuring()
    {
        if (!empty($this->o->during)) {
            $this->_during = $this->o->during;
        }
        return $this->_during;
    }

    public function getErrorType($id = null)
    {
        $list = [
            1 => 'Врач больше не работает в клинике',
            2 => 'Неверная цена',
            3 => 'Нет выезда на дом',
            4 => 'Не принимает детей',
            5 => 'Не оказывает такую услугу -врач',
            6 => 'Не оказывает такую услугу -клиника',
            7 => 'Изменение адреса приема врача в сети клиник',
            8 => 'Временно не работает аппарат',
            9 => 'Врач временно не работает',
            10 => 'Прочее',
        ];

        if (!is_null($id) AND isset($list[$id]))
            return $list[$id];

        return $list;

    }

    public function traceFOT()
    {

        if (empty($this->domain)) return;
        if ($this->domain != 'testpuls.ru') return;

        $q = 'select id from record_open_time where record_id=' . $this->id;
        if (Yii::app()->db->createCommand($q)->queryScalar()) return;
        $q = "INSERT INTO record_open_time(`record_id`,`open_time`) VALUES('" . $this->id . "','" . time() . "')";
        Yii::app()->db->createCommand($q)->execute();


    }

    /**
     * Длительность звонка
     * @param type $value
     */
    public function setDuring($value)
    {
        $this->_during = $value;
    }

    /**
     * Телефон вхождения
     * @return type
     */
    public function getTelephoneNext()
    {
        $this->_telephoneNext = 'Не обработан';
        if (!empty($this->oktell_id)) {
            $dateFrom = date("Y-m-d", strtotime($this->create_time) - 3600 * 24);
            $dateTo = date("Y-m-d", strtotime($this->create_time) + 3600 * 24);
            $_telephone = str_replace(array("+", "(", ")", " ", "-", "."), array("", "", "", "", "", ""), $this->telephone);
            $telephone = substr($_telephone, 1);
            $flag = Yii::app()->db->createCommand("SELECT api_record.id FROM api_record LEFT JOIN invoice ON invoice.id=api_record.oktell_id WHERE (invoice.callerid1 LIKE '%{$telephone}%' AND invoice.callerid2!='IVR' AND DATE(invoice.dt)<='{$dateTo}' AND DATE(invoice.dt)>='{$dateFrom}') OR (invoice.callerid1 LIKE '%{$telephone}%' AND invoice.direction='cdOutcoming' AND DATE(invoice.dt)<='{$dateTo}' AND DATE(invoice.dt)>='{$dateFrom}') OR (api_record.telephone LIKE '%{$telephone}%' AND api_record.status!=8 AND DATE(api_record.create_time)<='{$dateTo}' AND DATE(api_record.create_time)>='{$dateFrom}')")->queryRow();
            if ($flag) {
                $this->_telephoneNext = 'Обработан';
            }
        }
        return $this->_telephoneNext;
    }

    /**
     * Телефон вхождения
     * @param type $value
     */
    public function setTelephoneNext($value)
    {
        $this->_telephoneNext = $value;
    }

    /**
     * Количество номеров телефона
     * @return type
     */
    public function getCountTel()
    {
        $this->_countTel = '1';
        if (!empty($this->telephone)) {
            $dateFrom = date("Y-m-d", strtotime($this->create_time) - 3600 * 24);
            $dateTo = date("Y-m-d", strtotime($this->create_time) + 3600 * 24);
            $_telephone = str_replace(array("+", "(", ")", " ", "-", "."), array("", "", "", "", "", ""), $this->telephone);
            $telephone = substr($_telephone, 1);
            $flag = Yii::app()->db->createCommand("SELECT COUNT(*) AS qty FROM invoice WHERE invoice.callerid1 LIKE '%{$telephone}%' AND DATE(invoice.dt)<='{$dateTo}' AND DATE(invoice.dt)>='{$dateFrom}'")->queryRow();
            if ($flag['qty']) {
                $this->_countTel = $flag['qty'];
            }
        }
        return $this->_countTel;
    }

    /**
     * Количество номеров телефона
     * @param type $value
     */
    public function setCountTel($value)
    {
        $this->_countTel = $value;
    }

    /**
     * Количество телефонов
     * @return type
     */
    public function getCountTelAll()
    {
        $this->_countTelAll = '1';
        if (!empty($this->telephone)) {
            $_telephone = str_replace(array("+", "(", ")", " ", "-", "."), array("", "", "", "", "", ""), $this->telephone);
            $telephone = substr($_telephone, 1);
            $flag = Yii::app()->db->createCommand("SELECT COUNT(*) AS qty FROM invoice WHERE invoice.callerid1 LIKE '%{$telephone}%'")->queryRow();
            if ($flag['qty']) {
                $this->_countTelAll = $flag['qty'];
            }
        }
        return $this->_countTelAll;
    }

    /**
     * Количе ство телефонов
     * @param type $value
     */
    public function setCountTelAll($value)
    {
        $this->_countTelAll = $value;
    }

    /**
     * Телефон
     * @return type
     */
    public function getTelephoneOut()
    {
        $this->_telephoneOut = '&mdash;';
        if (!empty($this->oktell_id)) {
            $_telephone = str_replace(array("+", "(", ")", " ", "-", "."), array("", "", "", "", "", ""), $this->telephone);
            $telephone = substr($_telephone, 1);
            $flag = Yii::app()->db->createCommand("SELECT * FROM invoice WHERE callerid1 LIKE '%{$telephone}%' AND direction='cdOutcoming' AND id>'{$this->oktell_id}'")->queryRow();
            if ($flag) {
                $this->_telephoneOut = 'Исходящий';
            }
        }
        return $this->_telephoneOut;
    }

    /**
     * Телефон
     * @param type $value
     */
    public function setTelephoneOut($value)
    {
        $this->_telephoneOut = $value;
    }

    /**
     * Модель
     * @param type $className
     * @return type
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Таблица в базе данных
     * @return string
     */
    public function tableName()
    {
        return 'api_record';
    }

    /**
     * Установить $statusName
     * @param type $value
     */
    public function setStatusName($value)
    {
        $this->_statusName = $value;
    }

    public function getStatusList()
    {
        return [
            self::STATUS_CHECK => 'заявка',
            self::STATUS_REVERT => 'отказ',
            self::STATUS_LOOSE => 'не явился',
            self::STATUS_LEAD => 'был',
            self::STATUS_DECLINE => 'отмена',
            self::STATUS_RECEPT => 'запись',
            self::STATUS_PREVENT => 'преварительная отмена',
            self::STATUS_LIST => 'лист ожидания',
            self::STATUS_OKTELL => 'oktell ожидания',
            self::STATUS_RERECORD => 'перезаписался',
            self::STATUS_NOT => 'недозвон',
            self::STATUS_EMPTY => 'пустой звонок',
            self::STATUS_MISSED => 'пропущенный',
            self::STATUS_NINETY_DAYS => '90 дней',
            self::STATUS_ZOON_ORDER => 'zoon запись',
            self::STATUS_ZOON_DECLINE => 'zoon отказ',
            self::STATUS_ZOON_LOOSE => 'zoon не явился',
            self::STATUS_ZOON_LEAD => 'zoon был на приеме',
        ];
    }

    /**
     * @return string
     * const STATUS_CHECK=0; // заявка
     * const STATUS_REVERT=1; // отказ
     * const STATUS_LOOSE=2; // не явился
     * const STATUS_LEAD=3; // были на приеме
     * const STATUS_DECLINE=4; // отмена
     * const STATUS_RECEPT=5; // записи
     */
    public function getStatusName()
    {
        if (empty($this->_statusName)) {
            if (isset($this->status)) {
                switch ($this->status) {
                    case self::STATUS_CHECK:
                        $this->_statusName = 'Заявка';
                        break;
                    case self::STATUS_REVERT:
                        $this->_statusName = 'Отказ';
                        break;
                    case self::STATUS_LOOSE:
                        $this->_statusName = 'Не явился';
                        break;
                    case self::STATUS_LEAD:
                        $this->_statusName = 'Были на приеме';
                        break;

                    case self::STATUS_EMPTY:
                        $this->_statusName = 'Пустой';
                        break;

                    case self::STATUS_MISSED:
                        $this->_statusName = 'Пропущенный';
                        break;

                    case self::STATUS_DECLINE:
                        $this->_statusName = 'Отменена';
                        break;
                    case self::STATUS_RECEPT:
                        $this->_statusName = 'Запись';
                        break;
                    case self::STATUS_PREVENT:
                        $this->_statusName = 'Окончательный отказ';
                        break;
                    case self::STATUS_LIST:
                        $this->_statusName = 'Лист ожидания';
                        break;
                    case self::STATUS_OKTELL:
                        $this->_statusName = 'Oktell';
                        break;
                    case self::STATUS_NOT:
                        $this->_statusName = 'Недозвон';
                        break;
                    case self::STATUS_ZOON_DECLINE:
                        $this->_statusName = 'Zoon - отказ';
                        break;
                    case self::STATUS_ZOON_ORDER:
                        $this->_statusName = 'Zoon - запись';
                        break;
                    case self::STATUS_ZOON_LOOSE:
                        $this->_statusName = 'Zoon - не явился';
                        break;
                    case self::STATUS_ZOON_LEAD:
                        $this->_statusName = 'Zoon - был на приеме';
                        break;
                    case self::STATUS_NINETY_DAYS:
                        $this->_statusName = '90 дней';
                        break;
                    default:
                        $this->_statusName = 'n/a';
                }
            }
        }
        return $this->_statusName;
    }

    /*
     *  Статус партнера
     */

    public function getPartnerStatus()
    {
        switch ($this->partner_status) {
            case self::PARTNER_STATUS_WAIT:
                $status = "Требующая ответа";
                break;
            case self::PARTNER_STATUS_APPROVE:
                $status = "Записан";
                break;
            case self::PARTNER_STATUS_DECLINE:
                $status = "Отклонен";
                break;
            default:
                $status = "Не указан";
                break;
        }
        return $status;
    }

    /**
     * Цвет
     * @return string
     */
    public function getColor()
    {
        $output = 'row-empty';
        if (!empty($this->domain)) {
            switch ($this->domain) {
                case 'medbooking.com':
                    $output = 'row-medbooking';
                    break;
                case 'testpuls.ru':
                    $output = 'row-testpuls';
                    break;
                case 'timetovisit.ru':
                    $output = 'row-timetovisit';
                    break;
                case 'fromed.ru':
                    $output = 'row-fromed';
                    break;
            }
        }
        return $output;
    }

    /**
     * Цвет хоста
     * @return string
     */
    public function getHostColor()
    {
        $color = '#fff';
        if ($this->host == 1) {
            $color = '#0f70b3';
        } elseif ($this->host == 2) {
            $color = '#b0d083';
        } elseif ($this->host == 3) {
            $color = '#ffa500';
        } elseif ($this->host == 4) {
            $color = '#c66f6a';
        }
        return $color;
    }

    /**
     * Тип заявки
     * @return string
     */
    public function getType()
    {
        if (!empty($this->fromcall)) {
            $output = 'ico-call-status-application';
        } elseif (!empty($this->fromsite)) {
            $output = 'ico-application';
        } else {
            $output = 'ico-phone-status-application';
        }
        return $output;
    }

    /**
     * Тип заявки Тестовое
     * @return string
     */
    public function getTypeText()
    {
        if (!empty($this->fromcall)) {
            $output = 'звонок';
        } elseif (!empty($this->fromsite)) {
            $output = 'сайт';
        } else {
            $output = 'оператор';
        }
        return $output;
    }

    /**
     * Время обработки заявки текстовый формат
     * @return string
     */
    public function getTimeProcessingText()
    {
        $output = 'Заявка не обработана';
        if (!is_null($this->first_time_status)) {

            $d1 = new DateTime($this->create_time);
            $hour = $d1->format('H');
            if ($hour < 9) {
                $d1 = $d1->setTime(9, 0, 0);
            } else if ($hour >= 21) {
                $d1->add(new DateInterval('P1D'));
                $d1 = $d1->setTime(9, 0, 0);
            }
            $d1 = $d1->format('U');

            $d2 = new DateTime($this->first_time_status);
            $hour = $d2->format('H');
            if ($hour < 9 OR $hour >= 21) {
                $d2 = $d1 + 60;
            } else {
                $d2 = $d2->format('U');
            }
            $diff = $d2 - $d1;

            $minutes = round($diff / (60));
            $hours = (int)floor($minutes / 60);
            if ($hours >= 1) {
                $new_minutes = (int)floor($minutes - ($hours * 60));
                return Yii::t('app', "{n} час|{n} часа|{n} часов", array($hours)) . ' ' . Yii::t('app', "{n} минута|{n} минуты|{n} минут", array($new_minutes));
            } else {
                return Yii::t('app', "{n} минута|{n} минуты|{n} минут", array($minutes));
            }
        }
        return $output;
    }

    /**
     * Время обработки заявки
     * @return string
     */
    public function getTimeProcessing()
    {
        $output = '0';
        if (!is_null($this->first_time_status)) {

            $d1 = new DateTime($this->create_time);
            $hour = $d1->format('H');
            if ($hour < 9) {
                $d1 = $d1->setTime(9, 0, 0);
            } else if ($hour >= 21) {
                $d1->add(new DateInterval('P1D'));
                $d1 = $d1->setTime(9, 0, 0);
            }
            $d1 = $d1->format('U');

            $d2 = new DateTime($this->first_time_status);
            $hour = $d2->format('H');
            if ($hour < 9 OR $hour >= 21) {
                $d2 = $d1 + 60;
            } else {
                $d2 = $d2->format('U');
            }
            $diff = $d2 - $d1;

            $minutes = round($diff / (60));
            return $minutes;
        }
        return $output;
    }

    /**
     * Стоимость заявки партнера
     * @return int
     */
    public function getPartnerPrice()
    {
        if ($this->partner AND $this->partner_status == self::PARTNER_STATUS_APPROVE) {
//		    $record = MbRecord::find()->where(['ident_id'=>$this->partner->ident_id])->all();\
            $cur_record = MbRecord::findByPk($this->id);

            if (empty($cur_record))
                return 0;

            if ($cur_record->create_time > '2017-09-20 00:00:00') {//костыль что бы не изменились старые цены
                $min_month_time = date('Y.m.', strtotime($cur_record->create_time)) . '01 00:00:00';
                $max_month_time = date('Y.m.', strtotime($cur_record->create_time)) . date('t', strtotime($cur_record->create_time)) . ' 23:59:59';
//                $records = MbRecord::findAllByAttributes([], 'ident_id =' . $this->partner->id . ' and create_time>"' . $min_month_time . '" and create_time<"' . $max_month_time . '"');
                $records = Yii::app()->db->createCommand('select id FROM api_record WHERE ident_id =' . $this->partner->id . ' and create_time>"' . $min_month_time . '" and create_time<"' . $max_month_time . '"AND partner_status = 2 group by telephone')->queryAll();
                if (count($records) <= 20) {
                    return 100;
                } elseif (count($records) <= 40) {
                    return 150;
                } elseif (count($records) <= 70) {
                    return 200;
                } elseif (count($records) <= 100) {
                    return 250;
                } else {
                    return 300;
                }
            } else {
                // Убрать на хуй, ебаный стыд, не забыть!!!!!
                switch ($this->primaryKey) {
                    case 71873:
                        return 500;
                    case 71887:
                        return 400;
                    case 72776:
                        return 400;
                    case 76197:
                        return 400;
                    case 137051:
                        return 1000;
                    case 151290:
                        return 1000;
                }
                switch ($this->record_type) {
                    case 1:
                        return $this->partner->price_all;
                    case 2:
                        return $this->partner->price_diag;
                    case 3:
                        return $this->partner->price_analyz;
                    case 4:
                        return $this->partner->price_stomat;
                    default:
                        return 0;
                }
            }
        }
        return 0;
    }

    /**
     * $formattedLeadTime
     * @return string
     */
    public function getFormattedLeadTime()
    {
        if (!empty($this->lead_time) && $this->lead_time != '0000-00-00 00:00:00') {
            return date('d-m-Y H:i', strtotime($this->lead_time));
        } else {
            return '0000-00-00 00:00';
        }
    }

    /**
     * Фрматировнное время lead
     * @param type $formattedLeadTime
     */
    public function setFormattedLeadTime($formattedLeadTime)
    {
        $this->lead_time = date("Y-m-d H:i:s", strtotime($formattedLeadTime . ":00"));
    }

    /**
     * $formattedRecTime
     * @return string
     */
    public function getFormattedRecTime()
    {
        if (!empty($this->reception_time) && $this->reception_time != '0000-00-00 00:00:00') {
            return date('d.m.Y H:i', strtotime($this->reception_time));
        } else {
            return '00.00.0000 00:00';
        }
    }

    /**
     * get NinetyDaysDiff
     * @return string
     */
    public function getNinetyDaysDiff()
    {
        if (!empty($this->reception_time) && $this->reception_time != '0000-00-00 00:00:00') {
            /** @var DateTime $recTime */
            $recTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->reception_time);
            $recTime->add(new DateInterval('P90D'));
            $newTime = $recTime;
            $nowTime = new DateTime();
            if (!empty($recTime)) {
                $interval = $nowTime->diff($newTime);
                return $interval->format('%R%a');
            }
        }
        return '';
    }


    /**
     *
     * @return string
     */
    public function getFormattedCallTime()
    {
        if ($this->call_time) {
            return date('d-m-Y H:i', strtotime($this->call_time));
        }
        return '00-00-0000 00:00';
    }

    /**
     * $formattedRecTime
     * @param mixed $formattedRecTime
     */
    public function setFormattedRecTime($formattedRecTime)
    {
        $this->reception_time = date("Y-m-d H:i:s", strtotime($formattedRecTime . ":00"));
    }

    /**
     * $formattedCreateTime
     * @return string
     */
    public function getFormattedCreateTime()
    {
        if (!empty($this->create_time) && $this->create_time != '0000-00-00 00:00:00') {
            return date('d-m-Y H:i', strtotime($this->create_time));
        } else {
            return '0000-00-00 00:00';
        }
    }

    /**
     * $formattedCreateTime
     * @param mixed $formattedCreateTime
     */
    public function setFormattedCreateTime($formattedCreateTime)
    {
        $this->create_time = date("Y-m-d H:i:s", strtotime($formattedCreateTime . ":00"));
    }

    /**
     * $formattedUpdateTime
     * @return type
     */
    public function getFormattedUpdateTime()
    {
        if (!empty($this->update_time) && $this->update_time != '0000-00-00 00:00:00') {
            return date('d-m-Y H:i', strtotime($this->update_time));
        } else {
            return '0000-00-00 00:00';
        }
    }

    /**
     * $formattedUpdateTime
     * @param type $formattedUpdateTime
     */
    public function setFormattedUpdateTime($formattedUpdateTime)
    {
        $this->update_time = date("Y-m-d H:i:s", strtotime($formattedUpdateTime . ":00"));
    }

    /**
     * Код из SMS
     * @return type
     */
    public function getTimecode()
    {
        return $this->_timecode;
    }

    /**
     * Код из SMS
     * @param type $value
     */
    public function setTimecode($value)
    {
        $this->_timecode = $value;
    }

    /**
     * Сессия
     * @return type
     */
    public function getTimesession()
    {
        if (!empty($this->timecode) && !empty($_POST['sms'])) {
            $_addSession = $this->addSession;
            $sms = $_addSession::model()->findByAttributes(array('sid' => $this->timecode));
            if (!empty($sms->id) && $sms->id == $_POST['sms']) {
                $this->_timesession = $this->timecode;
            }
        }
        if (empty($this->_timesession)) {
            $this->_timesession = false;
        }
        return $this->_timesession;
    }

    /**
     * Сессия
     * @param type $value
     */
    public function setTimesession($value)
    {
        $this->_timesession = $value;
    }

    /**
     * Телефон пользователя
     * @return type
     */
    public function getUserphone()
    {
        if (Yii::app()->user->id) { // переписываем при наличии пользовательского ID
            $_addUser = $this->addUser;
            $_userModel = $_addUser::model()->findByPk(Yii::app()->user->id);
            if (!empty($_userModel->telephone)) {
                $this->_userphone = $_userModel->telephone;
            }
        }
        if (empty($this->_userphone)) {
            $this->_userphone = false;
        }
        return $this->_userphone;
    }

    /**
     * Телефон пользователя
     * @param type $value
     */
    public function setUserphone($value)
    {
        $this->_userphone = $value;
    }

    /**
     * Правила валидации
     * @return type
     */
    public function rules()
    {
        return array(
            array('gender', 'required', 'on' => 'guest', 'message' => 'Укажите пол'),
            array('record_type', 'required', 'message' => 'Укажите тип заявки'),
            array('sogl', 'required', 'on' => 'guest', 'message' => 'Пользовательское соглашение'),
            array('timecode', 'required', 'on' => 'user', 'message' => 'Телефон, введенный Вами не соответствует телефону в Вашем профиле, проверьте его значение или смените телефон в профиле'),
            array('telephone', 'compare', 'compareAttribute' => 'userphone', 'on' => 'user', 'message' => 'Укажите телефон'),
            array('tool_checked', 'compare', 'compareValue' => 0),
            array('userphone', 'required', 'on' => 'user', 'message' => 'Телефон, введенный Вами не соответствует телефону в Вашем профиле, проверьте его значение или смените телефон в профиле'),
            array('uid', 'required', 'on' => 'user', 'message' => 'Пользователь в системе не обнаружен'),
            array('author_id', 'required', 'on' => 'user', 'message' => 'Автор в системе не обнаружен'),
            array('telephone', 'required', 'on' => 'admin', 'message' => 'Укажите телефон'),
            array('author_id', 'required', 'on' => 'admin', 'message' => 'Укажите автора'),
            array('reception_time', 'required', 'message' => 'Время для записи не указано'),
            array('telephone', 'required', 'message' => 'Укажите свой телефон'),
            array('userproove, callproove', 'boolean'),
            array('timesession, userphone, callproove, userproove, note, reason, comment, lead_time, update_time, create_time, reception_time, service, comment_for_none_date, fail_sms, call_time, tracking_session_id, tracking_url, tracking_phone', 'safe'),
            array('gender, new_pacient, fromcall, rerecord, fromsite, house, sms_checked', 'length', 'max' => 1),
            array('status', 'length', 'max' => 3),
            array('hour_send_status, day_send_status, rerecord, oldstatus, id_call, ident_id', 'numerical', 'integerOnly' => true),
            array('attention_value, doctor_value, price_value, koll_repeat, status_record, insurance, expensive_service', 'numerical', 'integerOnly' => true),
            array('gender, status, new_pacient, category_id, uid, author_id, account_author_id, account_first_id, clinic_id, doctor_id, timeslot_id, deleted, house, flag_id', 'numerical', 'integerOnly' => true),
            array('name, email, domain, domain_phone, first_name', 'length', 'max' => 128),
            array('error_type, error_comment', 'safe'),


            array('telephone', 'length', 'max' => 12, 'min' => 10),
            array('book_id, source, description, clinic_name, doctor_name, category_name', 'length', 'max' => 255),
            array('email', 'email'),
            array('review, phone_record, not_our_patient, promo_code, referer, none_service, name_none_service, home, child, contrast', 'safe'),
            array('uid, author_id, first_uid, account_author_id, account_first_id', 'exist', 'attributeName' => 'uid', 'className' => 'MbUser'),
            array('oktell_id', 'exist', 'attributeName' => 'id', 'className' => 'AdminInvoice'),
            array('flag_id', 'exist', 'attributeName' => 'id', 'className' => 'MbFlag'),
            array('status_time', 'type', 'type' => 'datetime', 'allowEmpty' => true, 'datetimeFormat' => 'yyyy-mm-dd hh:mm:ss'),
            array('description, email, timeslot_id, new_pacient, category_id, doctor_id, author_id, first_uid, uid', 'default', 'setOnEmpty' => true, 'value' => null),
            array('description, telephone, name, first_name, clinic_name, doctor_name, category_name', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
            array('clinic_propose', 'checkClinicPropose'),
            array('clinic_disagreed', 'checkClinicDisagreed'),
            array('telephone', 'checkPhoneNumber'),
        );
    }

    public function checkPhoneNumber($attribute, $params)
    {

        if ($this->$attribute === '9168494056')
            $this->addError($attribute, 'bad number');
    }

    public function checkClinicPropose()
    {
        if ($this->clinic_propose == 1 and $this->hasErrors('clinic_id') !== false) {
            $this->clinic_propose = 0;
        }
    }

    public function checkClinicDisagreed()
    {
        if ($this->clinic_disagreed == 1 and $this->hasErrors('clinic_id') !== false) {
            $this->clinic_disagreed = 0;
        }
    }

    /**
     * Связт с другими моделями
     * @return type
     */
    public function relations()
    {
        return array(
            'u' => array(self::BELONGS_TO, 'MbUser', 'uid'),
            'o' => array(self::BELONGS_TO, 'AdminInvoice', 'oktell_id'),
            'f' => array(self::BELONGS_TO, 'MbUser', 'first_uid'),
            'account_f' => array(self::BELONGS_TO, 'MbUser', 'account_first_id'),
            'fl' => array(self::BELONGS_TO, 'MbFlag', 'flag_id'),
            'author' => array(self::BELONGS_TO, 'MbUser', 'author_id'),
            'account_author' => array(self::BELONGS_TO, 'MbUser', 'account_author_id'),
            'apisms_ar_s' => array(self::HAS_MANY, 'MbRecordSms', 'record_id'),
            'info' => array(self::HAS_ONE, 'MbRecordAdd', 'record_id'),
            'partner' => array(self::BELONGS_TO, 'Partner', 'ident_id'),
            'history' => array(self::HAS_MANY, 'MbRecordHistory', 'record_id'),
            'services' => array(self::HAS_MANY, 'MbRecordServices', 'record_id'),
        );
    }

    /**
     * Метки аттрибутов
     * @return type
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'service' => 'Услуга',
            'sms_checked' => 'Без SMS',
            'id_call' => 'Call ID',
            'flag_id' => 'Флаг',
            'oktell_id' => 'Oktell ID',
            'deleted' => 'Корзина',
            'infospec' => 'Специальности',
            'is_paid' => 'Оплачено',
            'note' => 'Комментарии',
            'hour_send_status' => 'Уведомление за час',
            'day_send_status' => 'Уведомление за день',
            'domain' => 'Домен',
            'domain_phone' => 'Телефон домена',
            'doctor_value' => 'Оценка доктора',
            'price_value' => 'Оценка цены',
            'attention_value' => 'Оценка внимания',
            'gender' => 'Пол',
            'name' => 'ФИО',
            'first_name' => 'Имя',
            'status5_dt' => 'Дата записи',
            'book_id' => 'ID записи (внешний)',
            'email' => 'E-mail',
            'telephone' => 'Телефон',
            'uid' => 'Клиент',
            'category_id' => 'Услуга',
            'description' => 'Сообщение',
            'comment' => 'Отзыв',
            'status' => 'Статус',
            'new_pacient' => 'Новый пациент',
            'status_time' => 'Время смены статуса',
            'reception_time' => 'На время',
            'formattedRecTime' => 'На время',
            'formattedLeadTime' => 'Время лида',
            'timeslot_id' => 'Таймслот',
            'doctor_id' => 'Доктор',
            'doctor_name' => 'Доктор',
            'telephoneNext' => 'Отзвон',
            'during' => 'Длительность',
            'clinic_id' => 'Клиника',
            'category_name' => 'Категория',
            'source' => 'Источник',
            'author_id' => 'Автор',
            'account_author_id' => 'Автор аккаунта',
            'first_uid' => 'Редактор',
            'account_first_id' => 'Редактор аккаунта',
            'create_time' => 'Создано',
            'formattedCreateTime' => 'Создано',
            'formattedUpdateTime' => 'Редактировано',
            'update_time' => 'Редактировано',
            'callproove' => 'Запись на колцентр',
            'userphone' => 'Телефон пользователя',
            'userproove' => 'Создание пользователя',
            'timecode' => 'Код из СМС',
            'timesession' => 'Код возврата',
            'statusName' => 'Статус',
            'fromsite' => 'С сайта',
            'fromcall' => 'Из ОС',
            'house' => 'Из обратной связи',
            'reason' => 'Причина отказа',
            'review' => 'Отзыв',
            'tool_checked' => 'Причина отказа',
            'rerecord' => 'Повторно пришел',
            'koll_repeat' => 'Повторно(неоплачений)',
            'status_record' => 'статус API',
            'name_none_service' => 'Услуга вне списка',
            'fail_sms' => 'Отправить смс с сообщением о сорвавшемся звонке ( вместо обычного )',
            'call_time' => 'Время звонка',
            'tracking_url' => 'Страница заказа звонка',
            'referer' => 'URL'
        );
    }

    /**
     * Поведения
     * @return type
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
        );
    }

    /**
     * Пол
     * @return string
     */
    public function getSex()
    {
        if (!empty($this->gender) && ($this->gender == 1)) {
            return 'Мужчина';
        } elseif (!empty($this->gender) && ($this->gender == 2)) {
            return 'Женщина';
        } else {
            return 'n/a';
        }
    }

    /**
     * Домен
     * @return type
     */
    public function getDomainId()
    {
        if (!empty($this->domain)) {
            $_addDomain = $this->addDomain;
            $domain = $_addDomain::model()->findByAttributes(array('title' => $this->domain));
            if (!empty($domain->id)) {
                return $domain->id;
            }
        }
    }

    /**
     * Хост
     * @return type
     */
    public function getHost()
    {
        if (!empty($this->domain)) {
            $_addDomain = $this->addDomain;
            $domain = $_addDomain::model()->findByAttributes(array('title' => $this->domain));
            if (!empty($domain->id)) {
                return $domain->id;
            }
        }
    }

    /**
     * До валидации
     * @return type
     */
    public function beforeValidate()
    {
        if (!empty($_POST[__CLASS__]['tool_checked']) && $_POST[__CLASS__]['tool_checked'] == 1) {
            $this->tool_checked = 1;
        } else {
            $this->tool_checked = 0;
        }
        // пользователь или админ
        if (!empty(Yii::app()->user->id)) {
            if (empty($this->author_id)) {
                $this->author_id = Yii::app()->user->id;
            }
            if (!Yii::app()->user->checkAccess('NDasha') || !Yii::app()->user->checkAccess('Sellteam') || !Yii::app()->user->checkAccess('Administrator')) { // если не сотрудник аккаунт отдела
                $this->first_uid = Yii::app()->user->id;
            }
        }
        // пользователь или админ из аккаунт отдела
        if (!empty(Yii::app()->user->id) && Yii::app()->user->checkAccess('NDasha') && !Yii::app()->user->checkAccess('Sellteam')) {
            if (empty($this->account_author_id)) {
                $this->account_author_id = Yii::app()->user->id;
            }
            $this->account_first_id = Yii::app()->user->id;
        }
        // на всякий случай приводим к нормальному значению телефон
        if (!empty($this->telephone)) {
            $this->telephone = str_replace(array("+7", "(", ")", " ", "-", "."), array("", "", "", "", "", ""), $this->telephone);
        }

        // если меняем статус
        if ((isset($this->oldstatus) && $this->oldstatus != $this->status) || $this->isNewRecord) {
            $this->status_time = date("Y-m-d H:i:s");
        }
        // при наличие метки приводит к нормальному значению
        if (!empty($this->reception_time) && $this->reception_time != '0000-00-00 00:00') {
            $this->setScenario('noreception');
            $_time = strtotime($this->reception_time);
            $this->reception_time = date("Y-m-d H:i:00", $_time);
        } else {
            $this->reception_time = '0000-00-00 00:00';
        }
        if (!empty($this->create_time)) {
            $_time = strtotime($this->create_time);
            $this->create_time = date("Y-m-d H:i:00", $_time);
        }
        // актуально только для новой записи при регистрации пользователем из ЛК
        if ($this->isNewRecord) {
            // new_pacient ничего по сути не дает, кроме галочки. Все равно проверку делать надо.
            // Не новый пациент, так же является переключателем создание нового пользователя
            // если пользователь регистрируется себя сам
            if (Yii::app()->user->id) {
                $this->uid = Yii::app()->user->id;
            }
            // если есть UID или TELEPHONE
            if (!empty($this->uid)) {
                $user = MbUser::model()->findByPk($this->uid);
            } elseif (!empty($this->telephone)) {
                $user = MbUser::model()->findByAttributes(array('telephone' => $this->telephone));
            }
            // если пользователя удалось вытащить
            if (!empty($user->primaryKey)) { // назначаем автоматически, если значений не передано
                $this->uid = $user->primaryKey;
                // если не введен e-mail, забираем, если есть, из учетной записи
                if (empty($this->email) && !empty($user->email)) {
                    $this->email = $user->email;
                }
                // если не введен логин, забираем, если есть, из учетной записи
                if (empty($this->name) && !empty($user->nameperson)) {
                    $this->name = $user->nameperson;
                }
                // если не введен пол, забираем, если есть, из учетной записи
                if (empty($this->gender) && !empty($user->gender)) {
                    $this->gender = $user->gender;
                }
            }
        }
        // для повторности записи уже дошедшего
        if (!empty($this->telephone)) {
            $flag = self::model()->findByAttributes(array('telephone' => $this->telephone, 'status' => 3));
            if (!empty($flag)) {
                $this->rerecord = 1;
            }
        }
        return parent::beforeValidate();
    }

    /**
     * beforeSave()
     * @return type
     */
    public function beforeSave()
    {

        if ($this->isNewRecord) {
            if ($this->status == 3 OR $this->status == 104) {
                $this->lead_time = $this->reception_time;
            }
        } else {
            if (empty($this->leadCheck)) {
                if (($this->status != $this->oldstatus) || $this->status == 3 || $this->status == 104) { // если статусы меняются
                    if ($this->status == 3 OR $this->status == 104) {
                        if (empty($this->lead_time) || $this->lead_time == '0000-00-00 00:00:00') {
                            $this->lead_time = date("Y-m-d H:i:00", time());
                        }
                    } else {
                        $this->lead_time = '0000-00-00 00:00:00';
                    }
                }
            }
        }
        return parent::beforeSave();
    }

    /**
     * afterSave()
     * @return type
     */
    public function afterSave()
    {
        /**
         * если статус записи "Записать",
         * а время записи в будущем,
         * вносим данные в астерикс.redir_to_clinic,
         * чтобы при звонке пользователя сразу
         * отправлять в клинику и не загружать Операторов
         */
        static $redir_to_clinic_processed = false;
        if (true
            && !$redir_to_clinic_processed
            && ($this->isNewRecord || $this->oldstatus != $this->status)
            && self::STATUS_RECEPT == $this->status
            && !empty($this->reception_time)
            && ($this->reception_time != '0000-00-00 00:00:00')
            && (strtotime($this->reception_time) > time())
        ) {
            $redir_to_clinic_processed = true;
            /* если не вставилось, это обидно, но не критично */
            try {
                do {
                    $client_phone = empty($this->phone_record) ? $this->telephone : $this->phone_record;


                    $client_phone = CPhone::normalizePhone($client_phone);
                    if (empty($client_phone)) {
                        Yii::log("redir_to_clinic :: client phone parsed empty: phone_record '{$this->phone_record}', telephone '{$this->telephone}'");
                        break;
                    }
                    if (preg_match('#^7(495|499|812)#', $client_phone)) {
                        Yii::log("redir_to_clinic :: skip city client phone: '{$client_phone}'");
                        break;
                    }

                    $clinic = MedbookingClinic::model()->findByPk($this->clinic_id);
                    if (empty($clinic)) {
                        Yii::log("redir_to_clinic :: empty clinic model: clinic_id '{$this->clinic_id}'");
                        break;
                    }
                    $clinic_phone_list = CPhone::parsePhoneString($clinic->telephone);
                    if (empty($clinic_phone_list)) {
                        Yii::log("redir_to_clinic :: empty clinic phones: clinic telephone '{$clinic->telephone}'");
                        break;
                    }
                    $clinic_phone = reset($clinic_phone_list);

                    $redir_to_clinic_data = array(
                        'number' => $client_phone,
                        'clinic' => $clinic_phone,
                        'date' => $this->reception_time,
                        'date_from' => date('Y-m-d H:i:s'),
                        'record_id' => (Yii::app()->params['appAlias'] . '.' . $this->id),
                    );
                    Yii::app()->db_ast->createCommand()
                        ->insert('redir_to_clinic', $redir_to_clinic_data);
                    Yii::log("redir_to_clinic :: add redir_to_clinic: " . var_export($redir_to_clinic_data, 1));
                } while (false);
            } catch (\Exception $e) {
                Yii::log("redir_to_clinic :: error inserting to redir_to_clinic: " . $e->getMessage());
            }
        }

        /**
         * Если статус заявки изменился с "Записать"
         * на что-то ещё, удаляем (обновляем дату)
         * записи в астерикс.redir_to_clinic
         */
        static $redir_to_clinic_unrecept_processed = false;
        if (true
            && !$redir_to_clinic_unrecept_processed
            && !$this->isNewRecord
            && self::STATUS_RECEPT == $this->oldstatus
            && $this->oldstatus != $this->status
        ) {
            $redir_to_clinic_unrecept_processed = true;
            try {
                do {
                    $redir_to_clinic_data = array(
                        'date' => new CDbExpression('DATE_FORMAT(NOW() - INTERVAL 1 DAY, "%Y-%m-%d 00:00:00")'),
                    );
                    $redir_to_clinic_conditions = array('and', 'record_id=:record_id', 'date > NOW()');
                    $redir_to_clinic_params = array(':record_id' => (Yii::app()->params['appAlias'] . '.' . $this->id));
                    Yii::app()->db_ast->createCommand()
                        ->update('redir_to_clinic', $redir_to_clinic_data, $redir_to_clinic_conditions, $redir_to_clinic_params);
                    Yii::log("redir_to_clinic :: delete redir_to_clinic: set date 'NOW - 1 DAY' where record_id = " . (Yii::app()->params['appAlias'] . '.' . $this->id));
                } while (false);
            } catch (\Exception $e) {
                Yii::log("redir_to_clinic :: error deleting from redir_to_clinic: " . $e->getMessage());
            }
        }

        //можно внести в MbDomain
        $google_tid = array('medbooking.com' => 'UA-44015389-1', 'timetovisit.ru' => 'UA-5061645-61', 'testpuls.ru' => 'UA-47101230-1', 'diagnostika.medbooking.com' => 'UA-44015389-2');

        if ($this->none_service AND !empty($this->name_none_service)) {
            MbController::mailto('Услуги нету в списке', 'Примечание ' . $this->name_none_service, 'stepan.v@medbooking.com, grood@medbooking.com');
        }
        // ЗДЕСЬ МЕНЯЕМ АДРЕС
        if ($this->addressCheck) {
            $this->addressSubway();
        }


        if ($this->smsCheck == 1 && empty($this->sms_checked)) {
            if ($this->isNewRecord) {
                if (!empty($this->fail_sms)) {
                    $this->sendSmsTemplate('CALL_FAIL2');

                } else if ($this->status == 5 && !empty($this->reception_time) && $this->reception_time != '0000-00-00 00:00:00' && strtotime($this->reception_time) > time()) {
                    if ($this->ifCmdClinic()) {
                        //Yii::app()->log('record from CMD network');
                        $this->sendSmsTemplate('CMD_APROOVE_VISIT.ftl');
//						$this->sendSmsTemplate('APROOVE_VISIT.ftl');
                    } else {
                        $this->sendSmsTemplate('APROOVE_VISIT.ftl');
                    }
                } else if ($this->status == 51) {
                    $this->sendSmsTemplate('CALL_FAIL');
                } elseif ($this->status == 0) {
                    if (!empty($this->sms_send_admin)) {
                        $this->sendSmsTemplate('SENDER');
                    }
                }
            } else {
                if ($this->status != $this->oldstatus) { // если статусы меняются
                    if (!empty($this->fail_sms)) {
                        $this->sendSmsTemplate('CALL_FAIL2');
                    } else if ($this->status == 3) {
                        if (!empty($this->comment)) {
                            // отправка c отзывом
                            $this->sendSmsTemplate('TEMPLATE_5_WITH.ftl');
                        } else {
                            // отправка без отзыва
                            $this->sendSmsTemplate('TEMPLATE_5.ftl');
                        }
                    } elseif ($this->status == 4) {
                        if (!empty($this->reception_time) && $this->reception_time != '0000-00-00 00:00:00' && strtotime($this->reception_time) > time()) {
                            // отправляем уведомление об отмене
                            $this->sendSmsTemplate('CANCEL_VISIT.ftl');
                        }
                    } elseif ($this->status == 5) {
                        if (!empty($this->reception_time) && $this->reception_time != '0000-00-00 00:00:00' && strtotime($this->reception_time) > time()) {
                            // отправляем уведомление о переводе в заявку
                            if ($this->ifCmdClinic()) {
                                //Yii::app()->log('record from CMD network');
                                $this->sendSmsTemplate('CMD_APROOVE_VISIT.ftl');
//								$this->sendSmsTemplate('APROOVE_VISIT.ftl');
                            } else {
                                $this->sendSmsTemplate('APROOVE_VISIT.ftl');
                            }
                        }
                    } else if ($this->status == 51) {
                        $this->sendSmsTemplate('CALL_FAIL');
                    }
                } else {
                    if (!empty($this->old_reception_time) && $this->reception_time != $this->old_reception_time) {
                        if (!empty($this->fail_sms)) {
                            $this->sendSmsTemplate('CALL_FAIL2');
                        } else if ($this->status == 4) {
                            if (!empty($this->reception_time) && $this->reception_time != '0000-00-00 00:00:00' && strtotime($this->reception_time) > time()) {
                                // отправляем уведомление об отмене. Уточнение
                                $this->sendSmsTemplate('CANCEL_VISIT.ftl');
                            }
                        } elseif ($this->status == 5) {
                            if (!empty($this->reception_time) && $this->reception_time != '0000-00-00 00:00:00' && strtotime($this->reception_time) > time()) {
                                // отправляем уведомление о переводе в заявку. Уточнение
                                if ($this->ifCmdClinic()) {
                                    //Yii::app()->log('record from CMD network');
                                    $this->sendSmsTemplate('CMD_APROOVE_VISIT.ftl');
                                } else {
                                    $this->sendSmsTemplate('APROOVE_VISIT.ftl');
                                }
                            }
                        }
                    }
                }
            }

            if ($this->status != $this->oldstatus) { // если статусы меняются
                Yii::app()->db->createCommand("UPDATE " . ($this->tableName()) . " SET oldstatus=status WHERE id='" . $this->id . "'")->execute();
            }
            if (empty($this->old_reception_time) || $this->reception_time != $this->old_reception_time) { // если статусы меняются
                Yii::app()->db->createCommand("UPDATE " . ($this->tableName()) . " SET old_reception_time=reception_time WHERE id='" . $this->id . "'")->execute();
            }
        }
        //$curl = Yii::app()->curl->run(Yii::app()->params['sms_gate_url'].'?status='.$this->status.'&oldstatus='.$this->oldstatus);

        /*if(!$curl->hasErrors()) {
            echo '<pre>';
            print_r($curl->getData());
            echo '</pre>';
            //print_r($curl->getInfo());

        } else {
            echo '<pre>';
            var_dump($curl->getErrors());
            echo '</pre>';
        }
        */
        $this->record();
        if (!empty($this->reason) && ($this->reason == 'Врач не работает')) {
            $doctor = !empty($this->doctor_name) ? $this->doctor_name : 'по заявке ' . $this->primaryKey;
            MbController::mailto('Врач не работает', 'Врач ' . $doctor, 'mt@timetovisit.ru');
        }
        if (!empty($this->_history)) {
            $this->_history->new = serialize($this->attributes);
            $this->_history->date = date('Y-m-d H:i:s');
            $this->_history->uid = Yii::app()->user->id;
            $this->_history->record_id = $this->primaryKey;
            $this->_history->save();
        }
        if (!empty($this->ga)) {
            if ($this->domain)
                $tid = $google_tid[$this->domain];
            if (!$tid)
                $tid = $google_tid['medbooking.com'];
            $ga = explode('.', $this->ga);
            $ga = $ga[2] . "." . $ga[3];
            $data = array(
                'v' => 1,
                'tid' => $tid, //'UA-44015389-1',
                'cid' => $ga,
                't' => 'event'
            );

            $data['ec'] = "apiOrder";
            $data['ea'] = "changedStatus";
            $data['el'] = $this->status;

            $url = 'https://www.google-analytics.com/collect';
            $content = http_build_query($data);
            $content = utf8_encode($content);
            $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
            curl_setopt($ch, CURLOPT_URL, $url . '?' . $content);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_exec($ch);
            curl_close($ch);

        }
        return parent::afterSave();
    }

    /**
     * Отправка и сохранение записи об отправке СМС, вызов класса MbRecordSms
     * @param type $title
     */
    private function sendSmsTemplate($title)
    {
        Yii::log("start sms, \$title=$title");
        $_addTemplate = $this->addTemplate;

        $template = $_addTemplate::model()->findByAttributes(array('title' => $title), "CURTIME()>time_start AND CURTIME()<time_end");
        if (!empty($template->id) && !empty($template->message)) {
            Yii::log("template is loaded, \$template->message=$template->message");
            $_addSms = $this->addSms;
            /** @var MbRecordSms $smsModel */
            $smsModel = new $_addSms;
            $smsModel->sms_send_admin = true;
            $smsModel->message = $template->message;
            if ($this->id) {
                $smsModel->record_id = $this->id;
            }
            if ($template->id) {
                $smsModel->t_id = $template->id;
            }
            $smsModel->status = $this->status;
            if (Yii::app()->user->id) {
                $smsModel->author_id = Yii::app()->user->id;
            }
            Yii::log("isNewRecord=" . $this->isNewRecord . "<br>message=" . $smsModel->message . "<br>record_id=" . $smsModel->record_id . "<br>id=" . $template->id . "<br>status=" . $smsModel->status . "<br>author_id=" . $smsModel->author_id);

            $res = $smsModel->save();
            Yii::log("\$smsModel->save() returned $res");
            if ($smsModel->hasErrors()) {
                Yii::log("\$smsModel->errors = " . CVarDumper::dumpAsString($smsModel->getErrors()));
            }
        }
    }

    /**
     * Обновление через API данных о клинике и враче
     */
    private function addressSubway()
    {
        $modelThis = self::model()->findByPk($this->primaryKey);
        $modelThis->smsCheck = 0;
        if ($this->domain) {
            $_addClass = $this->addClass;
            $model = $_addClass::model()->findByAttributes(array('record_id' => $this->id));
            if (empty($model->id)) {
                $model = new $_addClass;
                $model->record_id = $this->id;
            }
            if (!empty($this->clinic_id)) {
                $response = $this->sendAddress($this->domain, 'clinic', $this->clinic_id);
                if (!empty($this->clinic_name)) {
                    if (!empty($response['clinic_name']) && ($this->clinic_name != $response['clinic_name'])) {
                        $modelThis->addressCheck = false;
                        Yii::app()->db->createCommand("UPDATE " . ($this->tableName()) . " SET clinic_name='" . $response['clinic_name'] . "' WHERE id='" . $this->id . "'")->execute();
                    }
                } else {
                    if (!empty($response['clinic_name'])) {
                        $modelThis->addressCheck = false;
                        Yii::app()->db->createCommand("UPDATE " . ($this->tableName()) . " SET clinic_name='" . $response['clinic_name'] . "' WHERE id='" . $this->id . "'")->execute();
                    }
                }
                if (!empty($this->info->address)) {
                    if (!empty($response['address']) && ($this->info->address != $response['address'])) {
                        $model->address = $response['address'];
                    }
                } else {
                    if (!empty($response['address'])) {
                        $model->address = $response['address'];
                    }
                }
                if (!empty($this->info->lat)) {
                    if (!empty($response['lat']) && ($this->info->lat != $response['lat'])) {
                        $model->lat = $response['lat'];
                    }
                } else {
                    if (!empty($response['lat'])) {
                        $model->lat = $response['lat'];
                    }
                }
                if (!empty($this->info->lng)) {
                    if (!empty($response['lng']) && ($this->info->lng != $response['lng'])) {
                        $model->lng = $response['lng'];
                    }
                } else {
                    if (!empty($response['lng'])) {
                        $model->lng = $response['lng'];
                    }
                }
                if (!empty($this->info->clinic_url)) {
                    if (!empty($response['clinic_url']) && ($this->info->clinic_url != $response['clinic_url'])) {
                        $model->clinic_url = $response['clinic_url'];
                    }
                } else {
                    if (!empty($response['clinic_url'])) {
                        $model->clinic_url = $response['clinic_url'];
                    }
                }
                if (!empty($this->info->clinic_img_url)) {
                    if (!empty($response['clinic_img_url']) && ($this->info->clinic_img_url != $response['clinic_img_url'])) {
                        $model->clinic_img_url = $response['clinic_img_url'];
                    }
                } else {
                    if (!empty($response['clinic_img_url'])) {
                        $model->clinic_img_url = $response['clinic_img_url'];
                    }
                }
                if (!empty($this->info->regime_1)) {
                    if (!empty($response['regime_1']) && ($this->info->regime_1 != $response['regime_1'])) {
                        $model->regime_1 = $response['regime_1'];
                    }
                } else {
                    if (!empty($response['regime_1'])) {
                        $model->regime_1 = $response['regime_1'];
                    }
                }
                if (!empty($this->info->regime_2)) {
                    if (!empty($response['regime_2']) && ($this->info->regime_2 != $response['regime_2'])) {
                        $model->regime_2 = $response['regime_2'];
                    }
                } else {
                    if (!empty($response['regime_2'])) {
                        $model->regime_2 = $response['regime_2'];
                    }
                }
                if (!empty($this->info->regime_3)) {
                    if (!empty($response['regime_3']) && ($this->info->regime_3 != $response['regime_3'])) {
                        $model->regime_3 = $response['regime_3'];
                    }
                } else {
                    if (!empty($response['regime_3'])) {
                        $model->regime_3 = $response['regime_3'];
                    }
                }
                if (!empty($this->info->regime_4)) {
                    if (!empty($response['regime_4']) && ($this->info->regime_4 != $response['regime_4'])) {
                        $model->regime_4 = $response['regime_4'];
                    }
                } else {
                    if (!empty($response['regime_4'])) {
                        $model->regime_4 = $response['regime_4'];
                    }
                }
                if (!empty($this->info->regime_5)) {
                    if (!empty($response['regime_5']) && ($this->info->regime_5 != $response['regime_5'])) {
                        $model->regime_5 = $response['regime_5'];
                    }
                } else {
                    if (!empty($response['regime_5'])) {
                        $model->regime_5 = $response['regime_5'];
                    }
                }
                if (!empty($this->info->regime_6)) {
                    if (!empty($response['regime_6']) && ($this->info->regime_6 != $response['regime_6'])) {
                        $model->regime_6 = $response['regime_6'];
                    }
                } else {
                    if (!empty($response['regime_6'])) {
                        $model->regime_6 = $response['regime_6'];
                    }
                }
                if (!empty($this->info->regime_7)) {
                    if (!empty($response['regime_1']) && ($this->info->regime_7 != $response['regime_7'])) {
                        $model->regime_7 = $response['regime_7'];
                    }
                } else {
                    if (!empty($response['regime_7'])) {
                        $model->regime_7 = $response['regime_7'];
                    }
                }
                if (!empty($this->info->regime_8)) {
                    if (!empty($response['regime_8']) && ($this->info->regime_8 != $response['regime_8'])) {
                        $model->regime_8 = $response['regime_8'];
                    }
                } else {
                    if (!empty($response['regime_8'])) {
                        $model->regime_8 = $response['regime_8'];
                    }
                }
                if (!empty($this->info->subway)) {
                    if (!empty($response['subway']) && ($this->info->subway != $response['subway'])) {
                        $model->subway = $response['subway'];
                    }
                } else {
                    if (!empty($response['subway'])) {
                        $model->subway = $response['subway'];
                    }
                }
            }
            if (!empty($this->doctor_id)) {
                $response = $this->sendAddress($this->domain, 'doctor', $this->doctor_id);
                if (!empty($this->doctor_name)) {
                    if (!empty($response['doctor_name']) && ($this->clinic_name != $response['doctor_name'])) {
                        $modelThis->addressCheck = false;
                        Yii::app()->db->createCommand("UPDATE " . ($this->tableName()) . " SET doctor_name='" . $response['doctor_name'] . "' WHERE id='" . $this->id . "'")->execute();
                    }
                } else {
                    if (!empty($response['doctor_name'])) {
                        $modelThis->addressCheck = false;
                        Yii::app()->db->createCommand("UPDATE " . ($this->tableName()) . " SET doctor_name='" . $response['doctor_name'] . "' WHERE id='" . $this->id . "'")->execute();
                    }
                }
                if (!empty($this->info->speciality)) {
                    if (!empty($response['speciality']) && ($this->info->speciality != $response['speciality'])) {
                        $model->speciality = $response['speciality'];
                    }
                } else {
                    if (!empty($response['speciality'])) {
                        $model->speciality = $response['speciality'];
                    }
                }
                if (!empty($this->info->position)) {
                    if (!empty($response['position']) && ($this->info->position != $response['position'])) {
                        $model->position = $response['position'];
                    }
                } else {
                    if (!empty($response['position'])) {
                        $model->position = $response['position'];
                    }
                }
                if (!empty($this->info->experience)) {
                    if (!empty($response['experience']) && ($this->info->experience != $response['experience'])) {
                        $model->experience = $response['experience'];
                    }
                } else {
                    if (!empty($response['experience'])) {
                        $model->experience = $response['experience'];
                    }
                }
                if (!empty($this->info->doctor_description)) {
                    if (!empty($response['doctor_description']) && ($this->info->doctor_description != $response['doctor_description'])) {
                        $model->doctor_description = $response['doctor_description'];
                    }
                } else {
                    if (!empty($response['doctor_description'])) {
                        $model->doctor_description = $response['doctor_description'];
                    }
                }
                if (!empty($this->info->doctor_url)) {
                    if (!empty($response['doctor_url']) && ($this->info->doctor_url != $response['doctor_url'])) {
                        $model->doctor_url = $response['doctor_url'];
                    }
                } else {
                    if (!empty($response['doctor_url'])) {
                        $model->doctor_url = $response['doctor_url'];
                    }
                }
                if (!empty($this->info->doctor_img_url)) {
                    if (!empty($response['doctor_img_url']) && ($this->info->doctor_img_url != $response['doctor_img_url'])) {
                        $model->doctor_img_url = $response['doctor_img_url'];
                    }
                } else {
                    if (!empty($response['doctor_img_url'])) {
                        $model->doctor_img_url = $response['doctor_img_url'];
                    }
                }
            } else {
                Yii::app()->db->createCommand("UPDATE " . ($this->tableName()) . " SET doctor_name='' WHERE id='" . $this->id . "'")->execute();
                $model->doctor_url = '';
                $model->doctor_img_url = '';
                $model->doctor_description = '';
                $model->speciality = '';
                $model->position = '';
                $model->experience = '';
            }
            $model->save();
        }
    }

    /**
     * Общий функционал запрос данных
     * @param type $host_id
     * @param type $method
     * @param type $id
     */
    private function sendAddress($host, $type, $id)
    {
        $host = 'medbooking.com';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://' . $host . '/check/' . $type . '/id/' . $id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('id' => $id));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : ''));
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        if ($response) {
            return CJSON::decode($response);
        }
    }

    /**
     * Запись через API
     */
    public function record()
    {
        if (!empty($this->review) && !empty($this->domain)) {
            $array = array();
            if ($this->domain == 'child') {
                $host = 'medbooking.com';
            } else {
                $host = $this->domain;
            }
            $array['review'] = $this->review;
            $array['host'] = $this->domain;
            $array['price_value'] = !empty($this->price_value) ? $this->price_value : '';
            $array['doctor_value'] = !empty($this->doctor_value) ? $this->doctor_value : '';
            $array['attention_value'] = !empty($this->attention_value) ? $this->attention_value : '';
            $array['clinic_id'] = !empty($this->clinic_id) ? $this->clinic_id : '';
            $array['doctor_id'] = !empty($this->doctor_id) ? $this->doctor_id : '';
            $array['telephone'] = !empty($this->telephone) ? $this->telephone : '';
            $array['name'] = !empty($this->name) ? $this->name : '';
            $array['first_name'] = !empty($this->first_name) ? $this->first_name : '';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://' . $host . '/check/review/id/' . $this->id);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));
            curl_setopt($ch, CURLOPT_USERAGENT, (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : ''));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_exec($ch);
            curl_close($ch);
        }
    }

    private $_timesession;
    private $_userphone;
    private $_statusName;

    public function getRecordTypeText()
    {
        switch ($this->record_type) {
            case 1:
                return 'О';
                break;
            case 2:
                return 'Д';
                break;
            case 3:
                return 'А';
                break;
            case 4:
                return 'С';
                break;
            default:
                return 'Н';
        }
    }

    public function getPartnerService()
    {
        switch ($this->record_type) {
            case 1:
                return 'Врач';
                break;
            case 2:
                return 'Диагностика';
                break;
            case 3:
                return 'Анализы';
                break;
            case 4:
                return 'Стоматология';
                break;
            default:
                return 'Не указан';
        }
    }

    public function getStatusColor()
    {
        switch ($this->status) {
            // были на приеме
            case self::STATUS_LEAD:
                return 'green';
                break;
            case self::STATUS_LOOSE:
                return 'red';
                break;
            case self::STATUS_RECEPT:
                return 'blue';
                break;
            case self::STATUS_ZOON_ORDER:
                return 'orange';
                break;
            default:
                return 'black';
        }
    }

    /** check if clinic is cmd clinic */
    protected function ifCmdClinic()
    {
        $cmdId = 2752; // should use id
        if (!empty($this->clinic_name)) {

//			$childIds = $this->getNetworkClinicChildIds($cmdId);
//			if($this->domain)
//			$name = trim($this->clinic_name);

            if (strpos($this->clinic_name, ' CMD ') !== false) {
                //Yii::app()->log('record from CMD network');
                return true;
            }
        }
        //Yii::app()->log('record from simple network');
        return false;
    }

    /**
     * @param integer $networkId
     * @return array
     * */
    protected function getNetworkClinicChildIds($networkId)
    {
        $networkChildClincsIds = [];
        /** @var MedbookingClinic $network */
        $network = MedbookingClinic::model()->findByAttributes(['network_id' => $networkId]);
        if (!empty($network)) {
            $queryResult = Yii::app()->db2->createCommand()
                ->select('id')
                ->from('clinic')
                ->where('network_id=:network_id', [':network_id' => $networkId])
                ->queryAll();
            if (!empty($queryResult)) {
                foreach ($queryResult as $key => $value) {
                    $networkChildClincsIds[] = $value['id'];
                }
            }
        }
        return $networkChildClincsIds;
    }

    /**
     * поступила ли оплата по заявке
     *
     * @return bool
     */
    public function getIsPaidStatus()
    {
        if (floatval($this->summ_approved) > 0) {
            $status = 'да';
        } elseif ($this->koll_repeat || $this->insurance) {
            $status = 'вторичный';
        } else {
            $status = 'нет';
        }
        return $status;
    }

}
