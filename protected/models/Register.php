<?php

class Register extends CActiveRecord
{


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'register';
    }
    
    public function afterSave()
    {
        $fileimage = CUploadedFile::getInstance($this, 'file1');
        if (!empty($fileimage)) {
            $filename = $this->primaryKey . '.' . $fileimage->getExtensionName();
            $folder = YiiBase::getPathOfAlias('webroot.files.document');
            if (!is_dir($folder))
                mkdir($folder, 0777, true);
            if ($fileimage->saveAs('files/document/' . $filename))
                self::updateByPk($this->primaryKey, array('file1' => 'files/document/' . $filename));
        }
        $fileimage2 = CUploadedFile::getInstance($this, 'file2');
        if (!empty($fileimage2)) {
            $filename2 = $this->primaryKey . '-2' . '.' . $fileimage2->getExtensionName();
            $folder = YiiBase::getPathOfAlias('webroot.files.document');
            if (!is_dir($folder))
                mkdir($folder, 0777, true);
            if ($fileimage2->saveAs('files/document/' . $filename2))
                self::updateByPk($this->primaryKey, array('file2' => 'files/document/' . $filename2));
        }
        $fileimage3 = CUploadedFile::getInstance($this, 'file3');
        if (!empty($fileimage3)) {
            $filename3 = $this->primaryKey . '-3' . '.' . $fileimage3->getExtensionName();
            $folder = YiiBase::getPathOfAlias('webroot.files.document');
            if (!is_dir($folder))
                mkdir($folder, 0777, true);
            if ($fileimage3->saveAs('files/document/' . $filename3))
                self::updateByPk($this->primaryKey, array('file3' => 'files/document/' . $filename3));
        }
        return parent::afterSave();
    }

    public function rules()
    {
        return array(
            array('title','required','message'=>'Введите название'),
            array('create_time, update_time, data_agree, title, telephone, contact','safe'),
            //array('data_agree', 'type', 'type' => 'date', 'message' => 'Введите корректную дату', 'dateFormat' => 'yyyy-MM-dd hh:mm'),
            array('integrator_id, integrator_all_id','numerical','integerOnly'=>true),
            array('file1', 'file',
                    'types' => 'pdf, jpeg, jpg, png',
                    'allowEmpty' => true,
                    'wrongType' => Yii::t('app', 'Допустимы только файлы следующих форматов: {extensions}.'),
                    'maxSize' => 1024 * 1024 * 50,
                    'tooLarge' => Yii::t('app', 'Указан файл объемом более 10Мб. Пожалуйста, укажите файл меньшего размера.'),
                    'maxFiles' => 4,
            ),
            array('file2', 'file',
                    'types' => 'pdf, jpeg, jpg, png',
                    'allowEmpty' => true,
                    'wrongType' => Yii::t('app', 'Допустимы только файлы следующих форматов: {extensions}.'),
                    'maxSize' => 1024 * 1024 * 50,
                    'tooLarge' => Yii::t('app', 'Указан файл объемом более 10Мб. Пожалуйста, укажите файл меньшего размера.'),
                    'maxFiles' => 4,
            ),
            array('file3', 'file',
                    'types' => 'pdf, jpeg, jpg, png',
                    'allowEmpty' => true,
                    'wrongType' => Yii::t('app', 'Допустимы только файлы следующих форматов: {extensions}.'),
                    'maxSize' => 1024 * 1024 * 50,
                    'tooLarge' => Yii::t('app', 'Указан файл объемом более 10Мб. Пожалуйста, укажите файл меньшего размера.'),
                    'maxFiles' => 4,
            ),
        );
    }
    /**
     * Связт с другими моделями
     * @return type
     */
    public function relations()
    {
        return array(
            'ria'=>array(self::BELONGS_TO,'IntegratorClinicAll','integrator_all_id'),
            'ri'=>array(self::BELONGS_TO,'IntegratorClinic','integrator_id'),
        );
    }
    
    public function attributeLabels() {
        return array(
            'title' => 'Заговок',
            'telephone' => 'Телефон',
            'contact' => 'Контакное лицо',
            'data_agree'=>'Дата реестра',
            'integrator_id'=>'Клиника',
            'integrator_all_id'=>'Юр.Лицо',
            'file1' => 'Файл №1',
            'file2' => 'Файл №2',
            'file3' => 'Файл №3',
        );
    }

}
