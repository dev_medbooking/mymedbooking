<?php

class CallsReport extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
	    return 'api_record_calltouch';
    }

    public $file;
    public $success = false;

	public function checkAdmitad() {
		$fileimage = CUploadedFile::getInstance($this, 'file');
		if (!empty($fileimage)) {
			$filename = 'admitad.'.$fileimage->getExtensionName();
			$folder = YiiBase::getPathOfAlias('webroot.files.admitad');
			if ( ! is_dir($folder))
				mkdir($folder, 0755, true);
			if ($fileimage->saveAs('files/admitad/'.$filename)) {
				$inputFileName = 'files/admitad/admitad.'.$fileimage->getExtensionName();
				$objReader = PHPExcel_IOFactory::load($inputFileName);
				$objWorksheet = $objReader->setActiveSheetIndex(0);
				$highestRow = $objWorksheet->getHighestRow();
				$orders = array();
				$orders2 = array();
				for ($row = 1; $row <= $highestRow; ++ $row) {
					$orders[] = (int)$objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
					$i = substr((int)$objWorksheet->getCellByColumnAndRow(1, $row)->getValue(), 3, strlen((int)$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()) - 4);
					$orders2[] = (( ! empty($i) AND (strlen($i) == 5 OR strlen($i) == 6))? $i : 0);
				}
				$query = Yii::app()->db->createCommand("
					SELECT
						 api_record_calltouch.callId,
						 calltouch_with_api_records.api_record_status
					FROM calltouch_with_api_records
					LEFT JOIN api_record_calltouch ON api_record_calltouch.id = api_record_calltouch_id
					WHERE api_record_calltouch_id IN (
						SELECT id FROM api_record_calltouch WHERE callId IN (".implode(',', $orders).")
					)
					UNION SELECT
						api_record.id,
						api_record.status
					FROM api_record
					WHERE api_record.id IN (".implode(',', $orders2).")
				")->query();
				echo "<pre>";
				foreach($query as $q) {
					print_r($q);
				}
			}
			die;
		}
	}

	public function makeTable() {
		$fileimage = CUploadedFile::getInstance($this, 'file');
		if (!empty($fileimage)) {
			$filename = 'admitad.'.$fileimage->getExtensionName();
			$folder = YiiBase::getPathOfAlias('webroot.files.admitad');
			if ( ! is_dir($folder))
				mkdir($folder, 0755, true);
			if ($fileimage->saveAs('files/admitad/'.$filename)) {
				$inputFileName = 'files/admitad/admitad.'.$fileimage->getExtensionName();
				$objReader = PHPExcel_IOFactory::load($inputFileName);
				$objWorksheet = $objReader->setActiveSheetIndex(1);
				$highestRow = $objWorksheet->getHighestRow();
				$days = array();
				$hours = array();
				for ($row = 2; $row <= $highestRow; ++ $row) {
					$days[$objWorksheet->getCellByColumnAndRow(0, $row)->getValue()][$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['count'] = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
					$days[$objWorksheet->getCellByColumnAndRow(0, $row)->getValue()][$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['ctr'] = $objWorksheet->getCellByColumnAndRow(3, $row)->getFormattedValue();
					$hours[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()][$objWorksheet->getCellByColumnAndRow(0, $row)->getValue()] = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
				}

				$hours = array_keys($hours);
				$hours = array_filter($hours);
				sort($hours);
				$order = array(
					'Monday',
					'Tuesday',
					'Wednesday',
					'Thursday',
					'Friday',
					'Saturday',
					'Sunday'
				);
				$xls = new PHPExcel();
				PHPExcel_Calculation::getInstance()->setCalculationCacheEnabled(FALSE);

				$xls->setActiveSheetIndex(0);
				$sheet = $xls->getActiveSheet();
				$sheet->setTitle('Таблица');
				$i = 1;
				foreach ($hours as $k => $v) {
					if (!empty($v)) {
						$sheet->setCellValueByColumnAndRow($i, 1, $v);
						$i++;
					}
				}
				$i = 2;
				foreach ($order as $v) {
					$j = 1;
					$sheet->setCellValueByColumnAndRow(0, $i, $v);
					foreach ($hours as $k2 => $v2) {
						$sheet->setCellValueByColumnAndRow($j, $i, (isset($days[$v][$v2]['count']) ? $days[$v][$v2]['count'] : 0));
						$j++;
					}
					$i++;
				}

				$xls->createSheet();
				$xls->setActiveSheetIndex(1);
				$sheet = $xls->getActiveSheet();
				$sheet->setTitle('Таблица 2');
				$i = 1;
				foreach ($hours as $k => $v) {
					if (!empty($v)) {
						$sheet->setCellValueByColumnAndRow($i, 1, $v);
						$i++;
					}
				}
				$i = 2;
				foreach ($order as $v) {
					$j = 1;
					$sheet->setCellValueByColumnAndRow(0, $i, $v);
					foreach ($hours as $k2 => $v2) {
						$sheet->setCellValueByColumnAndRow($j, $i, (isset($days[$v][$v2]['ctr']) ? $days[$v][$v2]['ctr'] : 0));
						$j++;
					}
					$i++;
				}


				$xls->setActiveSheetIndex(0);

				header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
				header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
				header ( "Cache-Control: no-cache, must-revalidate" );
				header ( "Pragma: no-cache" );
				header ( "Content-type: application/vnd.ms-excel" );
				header ( "Content-Disposition: attachment; filename=report.xls" );

				$objWriter = new PHPExcel_Writer_Excel5($xls);
				$objWriter->save('php://output');
			}
			die;
		}
	}

    public function attributeLabels() {
        return array(
            'file' => 'xls File',
        );
    }

    public function rules() {
        return array(
            array('file', 'file',
                'types' => 'xls, csv, xlsx',
                'allowEmpty' => true,
                'wrongType' => Yii::t('app', 'Допустимы только файлы следующих форматов: {extensions}.'),
                'maxSize' => 1024 * 1024 * 50,
                'tooLarge' => Yii::t('app', 'Указан файл объемом более 10Мб. Пожалуйста, укажите файл меньшего размера.'),
                'maxFiles' => 1,
            ),
        );
    }

}
