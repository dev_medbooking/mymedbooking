<?php

/**
 * Created by PhpStorm.
 * User: serg
 * Date: 05.02.14
 * Time: 15:34
 */
class AContentSearch extends AContentModel
{

    protected $entityType='search';
    public $id;
    public $meta_title;
    public $url;
    public $is_mtitle;
    public $is_mdesc;
    public $is_mkeys;
    public $is_top;
    public $is_bottom;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('id,meta_title,url,is_mtitle,is_mdesc,is_mkeys,is_top,is_bottom','safe'),
            array('id,meta_title,url,is_mtitle,is_mdesc,is_mkeys,is_top,is_bottom','safe','on'=>'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'meta_title'=>'Наименование',
            'url'=>'URL',
            'is_mtitle'=>'MTitle',
            'is_mdesc'=>'MDesc',
            'is_mkeys'=>'MKeys',
            'is_top'=>'В.текст',
            'is_bottom'=>'Н.текст',
        );
    }

    public function search($domain)
    {
        return $this->getDomainContent($domain);
    }

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array('id','meta_title','url','is_mtitle','is_mdesc','is_mkeys','is_top','is_bottom');
    }

}