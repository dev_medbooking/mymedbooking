<?php

class IntegratorClinicPrice extends CActiveRecord
{

    public $title;
    public $title_company;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public $queryDate='';
    public $queryDateEnd='';

    public function getDateForQuery()
    {
        $_date=date('Y-m-01');
        if( ! empty($this->queryDate)){
            $_date=$this->queryDate;
        }
        return $_date;
    }

    public function getDateEndForQuery()
    {
        $_date = new DateTime($this->getDateForQuery());
        $_date->modify('last day of this month');
        $_date = $_date->format('Y-m-d');
        if( ! empty($this->queryDateEnd)){
            $_date=$this->queryDateEnd;
        }
        return $_date;
    }

    public function tableName()
    {
        return 'integrator_clinic_price';
    }

    public function beforeSave()
    {
        if(empty($this->price)&&!empty($this->all_price)&&!empty($this->koll_success)){
            $this->price=round($this->all_price/$this->koll_success,2);
        }
        return parent::beforeSave();
    }

    public function afterSave()
    {
        return parent::afterSave();
    }

    public function afterDelete()
    {
        parent::afterDelete();

    }

    public function getCount($status)
    {
        return $query = Yii::app()->db->createCommand("
		SELECT
			COUNT(t.ID) AS count,
			t.status,
			ROUND(SUM(t.price)) AS price
		FROM integrator_clinic_price AS t
		WHERE CASE
			WHEN DATE_FORMAT(t.date_status_3, '%Y-%m-%d') = '0000-00-00'
            THEN DATE_FORMAT(t.create_time, '%Y-%m-%d') >= '".$this->getDateForQuery()."' AND DATE_FORMAT(t.create_time, '%Y-%m-%d') <= '".$this->getDateEndForQuery()."'
            ELSE DATE_FORMAT(t.date_status_3, '%Y-%m-%d') >= '".$this->getDateForQuery()."' AND DATE_FORMAT(t.date_status_3, '%Y-%m-%d') <= '".$this->getDateEndForQuery()."'
		END
		".($status ? " AND t.status = ".$status : "")."
		GROUP BY status")->query();
    }


    public function getStatusText()
    {
        $status = '';
        if($this->status==1){
            $status='Отчет';
        } elseif($this->status==2){
            $status='Согл.';
        } elseif($this->status==3){
            $status='Счет';
        } elseif($this->status==4){
            $status='Оплата';
        }elseif($this->status==5){
            $status='Частично';
        }elseif($this->status==6){
            $status='Дебиторка';
        }elseif($this->status==7){
            $status='Отказ';
        }elseif($this->status==11){
            $status='Перезапись';
        }
        return $status;
    }

    public function rules()
    {
        return array(
            array('id, integrator_id, koll_come, koll_success ,status, koll_record, koll_diagnostik, koll_repeat','numerical','integerOnly'=>true),
            array('price, all_price, diagnostik_price','type','type'=>'float'),
            array('create_time, date_status_1, date_status_2, date_status_3, date_status_4, date_status_4, comment, all_price','safe'),
        );
    }

    /**
     * Связт с другими моделями
     * @return type
     */
    public function relations()
    {
        return array(
            'ic_price'=>array(self::BELONGS_TO,'IntegratorClinic','integrator_id'),
        );
    }

}
