<?php

/**
 * This is the model class for table "sessions_tracking_statistic".
 *
 * The followings are the available columns in table 'sessions_tracking_statistic':
 * @property string $id
 * @property string $host
 * @property string $current_url
 * @property string $referer
 * @property string $ip
 * @property string $server_addr
 * @property string $remote_addr
 * @property string $remote_host
 * @property string $sessionid
 * @property string $get
 * @property string $datetime
 * @property string $tracking_phone
 * @property string $project
 * @property string $utm_medium
 * @property string $utm_source
 * @property string $utm_campaign
 * @property string $utm_term
 * @property string $utm_content
 * @property string $k50id
 * @property string $yclid
 * @property string keyword
 * @property string $api_record_id
 * @property string $sessions_tracking_id
 */
class SessionsTrackingStatistic extends CActiveRecord
{

	protected $source_keyword_field = array('yandex.' => 'text',
									 'google.' => array('q','oq'),
									 'mail.ru' => 'q',
									 'yahoo.' => 'p',
								     'rambler.ru' => 'query' );

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SessionsTrackingStatistic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sessions_tracking_statistic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('host, ip, sessionid, datetime, sessions_tracking_id', 'required'),
			array('host, remote_host', 'length', 'max'=>50),
			array('current_url, referer, utm_campaign, utm_content', 'length', 'max'=>512),
			array('keyword', 'length', 'max'=>124),
			array('ip, server_addr, remote_addr, tracking_phone', 'length', 'max'=>15),
			array('sessionid', 'length', 'max'=>64),
			array('datetime, api_record_id, sessions_tracking_id', 'length', 'max'=>11),
			array('utm_medium, utm_source', 'length', 'max'=>10),
			array('utm_term, k50id, yclid', 'length', 'max'=>64),
			array('get, project', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, host, current_url, referer, ip, server_addr, remote_addr, remote_host, sessionid, get, datetime, tracking_phone, project, utm_medium, utm_source, utm_campaign, utm_term, utm_content, k50id, yclid, keyword, api_record_id, sessions_tracking_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'host' => 'Host',
			'current_url' => 'Current Url',
			'referer' => 'Referer',
			'ip' => 'Ip',
			'server_addr' => 'Server Addr',
			'remote_addr' => 'Remote Addr',
			'remote_host' => 'Remote Host',
			'sessionid' => 'Sessionid',
			'get' => 'Get',
			'datetime' => 'Datetime',
			'tracking_phone' => 'Tracking Phone',
			'project' => 'Проект',
			'utm_medium' => 'Utm Medium',
			'utm_source' => 'Utm Source',
			'utm_campaign' => 'Utm Campaign',
			'utm_term' => 'Utm Term',
			'utm_content' => 'Utm Content',
			'k50id' => 'K50id',
			'yclid' => 'Yclid',
			'keyword' => 'keyword',
			'api_record_id' => 'Api Record',
			'sessions_tracking_id' => 'Sessions Tracking',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('host',$this->host,true);
		$criteria->compare('current_url',$this->current_url,true);
		$criteria->compare('referer',$this->referer,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('server_addr',$this->server_addr,true);
		$criteria->compare('remote_addr',$this->remote_addr,true);
		$criteria->compare('remote_host',$this->remote_host,true);
		$criteria->compare('sessionid',$this->sessionid,true);
		$criteria->compare('get',$this->get,true);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('tracking_phone',$this->tracking_phone,true);
		$criteria->compare('project',$this->project,true);
		$criteria->compare('utm_medium',$this->utm_medium,true);
		$criteria->compare('utm_source',$this->utm_source,true);
		$criteria->compare('utm_campaign',$this->utm_campaign,true);
		$criteria->compare('utm_term',$this->utm_term,true);
		$criteria->compare('utm_content',$this->utm_content,true);
		$criteria->compare('k50id',$this->k50id,true);
		$criteria->compare('yclid',$this->yclid,true);
		$criteria->compare('keyword',$this->keyword,true);
		$criteria->compare('api_record_id',$this->api_record_id,true);
		$criteria->compare('sessions_tracking_id',$this->sessions_tracking_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 20,
			),
			'sort' => array(
				'defaultOrder' => 'id DESC',
			),
		));
	}

	public function getKeyword ($utm_term='',$referer='') {
		if (!empty($utm_term)) {
			$this->keyword = $utm_term;
		}
		else if (!empty($referer)) {
			$ref_host = parse_url($referer, PHP_URL_HOST);
			$ref_get = parse_url($referer, PHP_URL_QUERY);
			if ($ref_get) {
				parse_str($ref_get, $_get);
				foreach ($this->source_keyword_field as $k=>$v) {
					if (strpos($ref_host,$k)!==false) {
						if (!is_array($v)) {
							$this->keyword = !empty($_get[$v])?$_get[$v]:"";
						}
						else
							foreach ($v as $in_v) {
								if (!empty($_get[$in_v])) {
									$this->keyword = $_get[$in_v];
									break;
								}
							}
						break;
					}
				}
			}
		}
		return $this->keyword;
	}

	public function getSource ($utm_source='',$referer='') {
		if (!empty($utm_source)) {
			$this->source = $utm_source;
		}
		else if (!empty($referer)) {
			$ref_host = parse_url($referer, PHP_URL_HOST);
			if ($ref_host) {
				foreach ($this->source_keyword_field as $k=>$v) {
					if (strpos($ref_host,$k)!==false) {
						$this->source = substr($k,0,strpos($k,"."));
						break;
					}
				}
			}
		}
		return $this->source;
	}

}