<?php

class IntegratorAccountClinic extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'integrator_account_clinic';
    }

    public function beforeSave()
    {

        if(empty($this->account_id)){
            $this->account_id=Yii::app()->user->id;
        }
        if(Yii::app()->user->checkAccess('NUser')){
            $this->uid=Yii::app()->user->id;
        }
        return parent::beforeSave();
    }

    public function rules()
    {
        return array(
            array('clinic_id, account_id, uid, doctor_count, status','numerical','integerOnly'=>true),
            array('comment_content, comment_account, clinic_name, companyname, url, create_time, close_time, description, title','safe'),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior'=>array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'createAttribute'=>'create_time',
                'updateAttribute'=>'close_time',
            ),
        );
    }

    public function relations()
    {
        return array(
            'u'=>array(self::BELONGS_TO,'MbUser','uid'),
            'a'=>array(self::BELONGS_TO,'MbUser','account_id'),
        );
    }

}
