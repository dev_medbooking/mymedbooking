<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{

    public $username;
    public $password;
    public $rememberMe;
    public $otp_otp;
    public $otp_send;
    public $otp_phone;
    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('username, password','required','on'=>'start'),
            // rememberMe needs to be a boolean
            array('rememberMe','boolean'),
            // password needs to be authenticated
            array('password','authenticate','on'=>'start'),
            // otp needs to be sent
            array('otp_send','sendOtp','on'=>'otp_send'),
            // otp needs to be valid
            array('otp_otp','required','on'=>'otp_check'),
            array('otp_otp','checkOtp','on'=>'otp_check'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'rememberMe'=>'Запомнить меня',
            'password'=>'Пароль',
            'username'=>'Логин',
            'otp_otp'=>'Одноразовый пароль',
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute,$params)
    {
        $this->_identity=new UserIdentity($this->username,$this->password);
        if(!$this->_identity->authenticate()) $this->addError('password','Неверный логин или пароль.');
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether username is successful
     */
    public function login()
    {
        if($this->_identity===null){
            $this->_identity=new UserIdentity($this->username,$this->password);
            $this->_identity->authenticate();
        }
        if($this->_identity->errorCode===UserIdentity::ERROR_NONE){
            $duration=3600*24; // 30 days
            Yii::app()->user->login($this->_identity,$duration);
            return true;
        } else{
            return false;
        }
    }

    public function sendOtp()
    {
        do {
            if (empty(CPhone::normalizePhone($this->otp_phone))) {
                $this->addError('otp_send', 'К вашему аккаунту привязан некорректный телефон');
                break;
            }

            $send_otp = false;
            $now_ts = time();
            $session = Yii::app()->session;

            if (true
                && !empty($this->otp_send)
                && !empty($session['LoginForm.otp'])
                && ($session['LoginForm.otp']['resend_after_ts'] < $now_ts)
            ) {
                /* обновим время переотправки */
                $session['LoginForm.otp'] = array(
                    'password' => $session['LoginForm.otp']['password'],
                    'valid_till_ts' => $session['LoginForm.otp']['valid_till_ts'],
                    'resend_after_ts' => $now_ts + 20,
                );
                $send_otp = true;
            }
            if (empty($session['LoginForm.otp'])) {
                $session['LoginForm.otp'] = array(
                    'password' => mt_rand(100000, 999999),
                    'valid_till_ts' => $now_ts + 300,
                    'resend_after_ts' => $now_ts + 20,
                );
                $send_otp = true;
            }
            if ($send_otp) {
                $phone = $this->otp_phone;
                $message = "Ваш одноразовый пароль для входа в CRM medbooking {$session['LoginForm.otp']['password']}";
                if (!Sms::send($phone, $message)) {
                    /* проставим текущее время переотправки */
                    $session['LoginForm.otp'] = array(
                        'password' => $session['LoginForm.otp']['password'],
                        'valid_till_ts' => $session['LoginForm.otp']['valid_till_ts'],
                        'resend_after_ts' => $now_ts,
                    );
                    $this->addError('otp_send', 'Ошибка отправки СМС. Попробуйте позднее.');
                    break;
                }
            }
        } while(false);
    }

    public function checkOtp()
    {
        $session = Yii::app()->session;
        if (true
            && !empty($session['LoginForm.otp'])
            && ($session['LoginForm.otp']['valid_till_ts'] < time())
        ) {
            $session->remove("LoginForm.otp");
            $this->addError('otp_otp', 'Время действия одноразового пароля истекло');
        } elseif (false
                  || empty($this->otp_otp)
                  || empty($session['LoginForm.otp'])
                  || ($this->otp_otp != $session['LoginForm.otp']['password'])
        ) {
            $this->addError('otp_otp', 'Неверный одноразовый пароль');
        }
    }

}
