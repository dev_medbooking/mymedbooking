<?php

class AdminInvoice extends CActiveRecord
{

    public function getMp3()
    {
        if(!empty($this->id_com)&&!empty($this->id_call)){
            $model=AdminCall::model()->findByAttributes(array('startparam1'=>$this->id_call,'startparam2'=>$this->id_com));
            if(!empty($model->context)&&$model->context!='EMPTY'){
                return Yii::app()->createUrl('/mbRecord/mp3',array('id'=>$model->primaryKey));
            } else{
                return Yii::app()->createUrl('/mbRecord/oktell',array('startparam1'=>$this->id_call,'startparam2'=>$this->id_com));
            }
        }
        return false;
    }

    public function beforeSave()
    {
        // если есть значения для проверки
        if(isset($this->callerid2)&&!empty($this->callerid1)&&isset($this->direction)&&isset($this->id_call)&&isset($this->during)){
            // устраняем дубль
            $model=AdminInvoice::model()->findByAttributes(array('callerid2'=>$this->callerid2,'callerid1'=>$this->callerid1,'direction'=>$this->direction,'id_call'=>$this->id_call,'during'=>$this->during));
            if(!empty($model->primaryKey)){
                return false;
            }
        } elseif(empty($this->callerid1)||empty($this->direction)){ // нет важных значений
            return false;
        }
        return parent::beforeSave();
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'invoice';
    }

    public function rules()
    {
        return array(
            array('secret_key, dt, action, id_com, id_call, date, time, wait, during, name, create_time, str, calledid, callerid1, callerid2, direction','safe'),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior'=>array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'createAttribute'=>'create_time',
                'updateAttribute'=>null,
            ),
        );
    }

    public function afterSave()
    {
        if(!empty($this->callerid1)&&!empty($this->id_call)&&($this->direction=='cdIncoming')&&($this->isNewRecord)){
            $proove=Yii::app()->db->createCommand("SELECT id FROM api_record WHERE telephone='{$this->callerid1}' AND id_call='{$this->id_call}'")->queryScalar();
            if(empty($proove)){
                $model=new MbRecord;
                $model->telephone=$this->callerid1;
                $model->name=!empty($this->calledid)?$this->calledid:'n/a';
                $model->domain='oktell.medbooking.com';
                $model->status=8;
                $model->oktell_id=$this->primaryKey;
                $model->id_call=$this->id_call;
                if(!empty($this->date)&&!empty($this->time)){
                    $model->update_time=$this->date." ".$this->time;
                }
                $model->save();
            } else{
                $prooveModel=MbRecord::model()->findByPk($proove);
                if(!empty($prooveModel->primaryKey)&&!empty($prooveModel->o->callerid2)&&!empty($this->callerid2)&&($prooveModel->o->callerid2!=$this->callerid2)&&($this->callerid2!='IVR')){
                    $prooveModel->oktell_id=$this->primaryKey;
                    $prooveModel->save(false);
                }
            }
        }
        return parent::afterSave();
    }

}
