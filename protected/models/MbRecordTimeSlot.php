<?php

/**
 * Class MbRecordTimeSlot
 * @property int $id
 * @property int $service_id
 * @property int $category_id
 * @property int $clinic_id
 * @property string $date
 * @property string $time
 */
class MbRecordTimeSlot extends CActiveRecord
{
    /**
     * @param string $className
     * @return static
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return '{{api_record_timeslot}}';
    }

    public function rules()
    {
        return [
            ['clinic_id, service_id, date, time, category_id', 'required'],
            ['date', 'date', 'format' => 'yyyy-mm-dd'],
            ['time', 'type', 'type' => 'time'],
        ];
    }
}