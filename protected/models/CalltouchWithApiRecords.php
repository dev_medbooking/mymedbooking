<?php

/**
 * This is the model class for table "calltouch_with_api_records".
 *
 * The followings are the available columns in table 'calltouch_with_api_records':
 * @property string $id
 * @property integer $api_record_calltouch_id
 * @property integer $api_record_id
 * @property integer $api_record_status
 */
class CalltouchWithApiRecords extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CalltouchWithApiRecords the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'calltouch_with_api_records';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('api_record_calltouch_id, api_record_id', 'required'),
			array('api_record_calltouch_id, api_record_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, api_record_calltouch_id, api_record_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'api_record_calltouch_id' => 'Api Record Calltouch',
			'api_record_id' => 'Api Record',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('api_record_calltouch_id',$this->api_record_calltouch_id);
		$criteria->compare('api_record_id',$this->api_record_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}