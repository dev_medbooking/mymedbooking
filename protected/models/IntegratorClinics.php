<?php

class IntegratorClinics extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'integrator_clinic';
	}

	public function relations()
	{
		return array(
			'law' => array(self::BELONGS_TO, 'IntegratorLaws', 'all_id'),
			'clinic_reports' => array(self::HAS_MANY, 'IntegratorClinicsReports', 'clinic_id'),
			'clinic_report' => array(self::HAS_ONE, 'IntegratorClinicsReports', 'clinic_id'),
		);
	}

}
