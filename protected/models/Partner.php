<?php

/**
 * Class Partner
 * @property MbUser $usr
 * @property PartnerFeel $ppf
 * @property MbRecord $record
 */
class Partner extends CActiveRecord
{

    const PARTNER_PAYMENT_TYPE_WMR = 1;
    const PARTNER_PAYMENT_TYPE_CA = 2;

    static public $paymentTypeText = array(
        self::PARTNER_PAYMENT_TYPE_WMR => 'WMR - кошелек',
        self::PARTNER_PAYMENT_TYPE_CA => 'Р/c'
    );

    public $startDate;
    public $endDate;
    public $admin;

    public function getStartDateForQuery()
    {
        $startDate = date('Y-m-01');
        if (!empty($this->startDate)) {
            $startDate = $this->startDate;
        }
        return $startDate;
    }

    public function getEndDateForQuery()
    {
        $endDate = new DateTime($this->getStartDateForQuery());
        $endDate->modify('last day of this month');
        $endDate = $endDate->format('Y-m-d');
        if (!empty($this->endDate)) {
            $endDate = $this->endDate;
        }
        return $endDate;
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'partner';
    }

    public function rules()
    {
        return array(
            array('title', 'required', 'message' => 'Введите Название'),
            array('name', 'required', 'message' => 'Введите  ФИО'),
            //array('phone','required','message'=>'Введите  Телефон'),
            array('price_all', 'required', 'message' => 'Введите цену'),
            array('price_diag', 'required', 'message' => 'Введите цену'),
            array('price_analyz', 'required', 'message' => 'Введите цену'),
            array('price_stomat', 'required', 'message' => 'Введите цену'),
            array('domain', 'required', 'message' => 'Укажите Домен'),
            array('contact_phone', 'required', 'message' => 'Укажите контактный телефон'),
            array('payment_type', 'required', 'message' => 'Укажите тип оплаты'),
            array('payment_number', 'required', 'message' => 'Укажите тип оплаты'),
            array('create_time, update_time, id, body, phone, site', 'safe'),
            array('ident_id,id', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * Связт с другими моделями
     * @return type
     */
    public function relations()
    {
        return array(
            'ppf' => array(self::BELONGS_TO, 'PartnerFeel', '', 'foreignKey' => array('id' => 'partner_id')),
            'usr' => array(self::BELONGS_TO, 'MbUser', 'ident_id'),
            'record' => array(self::HAS_ONE, 'MbRecord', 'ident_id')
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Название',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'price_all' => 'Цена за общую медицину',
            'price_diag' => 'Цена за диагностику',
            'price_analyz' => 'Цена за анализы',
            'price_stomat' => 'Цена за стоматологию',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата изменения',
            'contact_phone' => 'Контактный телефон',
            'wmr' => 'Кошелек',
            'domain' => 'Домен',
            'ident_id' => 'ID Ident',
            'body' => 'Содержание',
        );
    }

    public function getSearchCondition()
    {
        if (!$this->admin) return $this->primaryKey;
        else return "SELECT id FROM partner WHERE id != '9'";
    }

    public function isLimited()
    {
        return $this->usr->getIsLimited() || strpos($this->phone, '0') === 0;
    }

    public function getRecordInfo()
    {
        $array = array();
        $array['all_record'] = 0;
        $array['success_record'] = 0;
        $array['price'] = 0;
        //$partner = Partner::model()->findByAttributes(array('ident_id' => Yii::app()->user->id));
        $array['all_record'] = Yii::app()->db->createCommand("SELECT IFNULL(COUNT(*), 0) FROM api_record WHERE (deleted IS NULL OR deleted = 0) AND DATE_FORMAT(create_time, '%Y-%m-%d') >= '" . $this->getStartDateForQuery() . "' AND DATE_FORMAT(create_time, '%Y-%m-%d') <= '" . $this->getEndDateForQuery() . "'AND create_time <= '" . date("Y-m-d H:i:s") . "' AND ident_id IN (" . $this->getSearchCondition() . ")")->queryScalar();
        $array['success_record'] = Yii::app()->db->createCommand("
				SELECT
				    ap.id,
					ap.record_type,
					ap.create_time,
					ap.telephone,
					p.price_all,
					p.price_diag,
					p.price_analyz,
					p.price_stomat
				FROM api_record AS ap
				LEFT JOIN partner AS p ON p.id = ap.ident_id
				WHERE
					(ap.deleted IS NULL OR ap.deleted = 0)
					AND DATE_FORMAT(ap.create_time, '%Y-%m-%d') >= '" . $this->getStartDateForQuery() . "'
					AND DATE_FORMAT(ap.create_time, '%Y-%m-%d') <= '" . $this->getEndDateForQuery() . "'
					AND ap.create_time <= '" . date("Y-m-d H:i:s") . "'
					AND ap.partner_status = 2
					AND ap.ident_id IN (" . $this->getSearchCondition() . ")
					")->queryAll();

        foreach ($array['success_record'] as $v) {

//            CVarDumper::dump($v['id'],10,1);
//            CVarDumper::dump($cur_record,10,1);exit;

            if (empty($v))
                continue;
            $min_month_time = date('Y.m.', strtotime($v['create_time'])) . '01 00:00:00';
            $max_month_time = date('Y.m.', strtotime($v['create_time'])) . date('t', strtotime($v['create_time'])) . ' 23:59:59';
            $records = Yii::app()->db->createCommand('select id, telephone,ident_id,create_time FROM api_record WHERE ident_id IN (' . $this->getSearchCondition() . ') and create_time>"' . $min_month_time . '" and create_time<"' . $max_month_time . '" AND partner_status = 2 group by telephone')->queryAll();
            if ($v['create_time'] > '2017-09-20 00:00:00') {//костыль что бы не изменились старые цены
                if (count($records) <= 20) {
                    $array['price'] += 100;
                } elseif (count($records) <= 40) {
                    $array['price'] += 150;
                } elseif (count($records) <= 70) {
                    $array['price'] += 200;
                } elseif (count($records) <= 100) {
                    $array['price'] += 250;
                } else {
                    $array['price'] += 300;
                }
            } else {
                if (count($records) <= 20) {
                    $array['price'] += 100;
                } elseif (count($records) <= 40) {
                    $array['price'] += 150;
                } elseif (count($records) <= 70) {
                    $array['price'] += 200;
                } elseif (count($records) <= 100) {
                    $array['price'] += 250;
                } else {
                    $array['price'] += 300;
                }
            }

        }
        $array['success_record'] = count($array['success_record']);
        return $array;
    }

    public function getDetailRecordInfo()
    {
        $array = array();
        //$partner = Partner::model()->findByAttributes(array('ident_id' => Yii::app()->user->id));
        $all_records = Yii::app()->db->createCommand("
			SELECT
				IFNULL(COUNT(ar.id), 0) AS count,
				ar.ident_id,
				p.name,
				p.price_all,
				p.price_diag,
				p.price_analyz,
				p.price_stomat,
				p.site
			FROM api_record AS ar
			LEFT JOIN partner AS p ON p.id = ar.ident_id
			WHERE
				(ar.deleted IS NULL OR ar.deleted = 0)
				AND DATE_FORMAT(ar.create_time, '%Y-%m-%d') >= '" . $this->getStartDateForQuery() . "'
				AND DATE_FORMAT(ar.create_time, '%Y-%m-%d') <= '" . $this->getEndDateForQuery() . "'
				AND ar.ident_id IN (SELECT id FROM partner)
				GROUP BY ar.ident_id
				ORDER BY count DESC")->queryAll();
        foreach ($all_records as $r) {
            $array[$r['ident_id']]['all_records'] = $r['count'];
            $array[$r['ident_id']]['name'] = $r['name'];
            $array[$r['ident_id']]['site'] = $r['site'];
            $array[$r['ident_id']]['price_all'] = $r['price_all'];
            $array[$r['ident_id']]['price_diag'] = $r['price_diag'];
            $array[$r['ident_id']]['price_analyz'] = $r['price_analyz'];
            $array[$r['ident_id']]['price_stomat'] = $r['price_stomat'];
        }
        $success_records = Yii::app()->db->createCommand("
			SELECT
				record_type,
				ident_id
			FROM api_record
			WHERE
				(deleted IS NULL OR deleted = 0)
				AND DATE_FORMAT(create_time, '%Y-%m-%d') >= '" . $this->getStartDateForQuery() . "'
				AND DATE_FORMAT(create_time, '%Y-%m-%d') <= '" . $this->getEndDateForQuery() . "'
				AND partner_status=2
				AND ident_id IN (SELECT id FROM partner)")->queryAll();
        foreach ($success_records as $s) {
            if (empty($array[$s['ident_id']]['success_record'])) $array[$s['ident_id']]['success_record'] = 1;
            else $array[$s['ident_id']]['success_record']++;
            switch ($s['record_type']) {
                case 1:
                    if (empty($array[$s['ident_id']]['price'])) $array[$s['ident_id']]['price'] = $array[$s['ident_id']]['price_all'];
                    else $array[$s['ident_id']]['price'] += $array[$s['ident_id']]['price_all'];
                    break;
                case 2:
                    if (empty($array[$s['ident_id']]['price'])) $array[$s['ident_id']]['price'] = $array[$s['ident_id']]['price_diag'];
                    else $array[$s['ident_id']]['price'] += $array[$s['ident_id']]['price_diag'];
                    break;
                case 3:
                    if (empty($array[$s['ident_id']]['price'])) $array[$s['ident_id']]['price'] = $array[$s['ident_id']]['price_analyz'];
                    else $array[$s['ident_id']]['price'] += $array[$s['ident_id']]['price_analyz'];
                    break;
                case 4:
                    if (empty($array[$s['ident_id']]['price'])) $array[$s['ident_id']]['price'] = $array[$s['ident_id']]['price_stomat'];
                    else $array[$s['ident_id']]['price'] += $array[$s['ident_id']]['price_stomat'];
                    break;
                default:
                    if (empty($array[$s['ident_id']]['price'])) $array[$s['ident_id']]['price'] = 0;
                    else $array[$s['ident_id']]['price'] += 0;
                    break;
            }
        }
        return $array;
    }

    public function getRecordTimeAverage()
    {
        static $info;

        if ($info) {
            return $info;
        }

        $time = 0;
        $record = Yii::app()->db->createCommand("SELECT id FROM api_record WHERE (deleted IS NULL OR deleted = 0) AND DATE_FORMAT(create_time, '%Y-%m-%d') >= '" . $this->getStartDateForQuery() . "' AND DATE_FORMAT(create_time, '%Y-%m-%d') <= '" . $this->getEndDateForQuery() . "' AND status!=0 AND  ident_id IN (" . $this->getSearchCondition() . ")")->queryColumn();
        $count = Yii::app()->db->createCommand("SELECT COUNT(*) FROM api_record WHERE (deleted IS NULL OR deleted = 0) AND DATE_FORMAT(create_time, '%Y-%m-%d') >= '" . $this->getStartDateForQuery() . "' AND DATE_FORMAT(create_time, '%Y-%m-%d') <= '" . $this->getEndDateForQuery() . "' AND status!=0 AND ident_id IN (" . $this->getSearchCondition() . ")")->queryScalar();
        $count = !empty($count) ? $count : 1;
        if (!empty($record)) {
            $criteria = new CDbCriteria;
            $criteria->addInCondition('id', $record);
            $criteria->select = 'first_time_status, create_time';
            $model = MbRecord::model()->findAll($criteria);
            if (!empty($model)) {
                foreach ($model as $key => $value) {
                    $time += (int)$value->timeProcessing;
                }
            }
        }
        $minutes = (int)round($time / $count, 2);
        $hours = (int)floor($minutes / 60);
        if ($hours >= 1) {
            $new_minutes = (int)floor($minutes - ($hours * 60));
            return $info = Yii::t('app', "{n} час|{n} часа|{n} часов", array($hours)) . ' ' . Yii::t('app', "{n} минута|{n} минуты|{n} минут", array($new_minutes));
        } else {
            return $info = Yii::t('app', "{n} минута|{n} минуты|{n} минут", array($minutes));
        }
    }

    public function getVisitsCount()
    {
        return Yii::app()->db->createCommand("SELECT COUNT(id) FROM partner_visits WHERE DATE_FORMAT(date, '%Y-%m-%d') >= '" . $this->getStartDateForQuery() . "' AND DATE_FORMAT(date, '%Y-%m-%d') <= '" . $this->getEndDateForQuery() . "' AND date <= '" . date("Y-m-d H:i:s") . "' AND ident_id IN (" . $this->getSearchCondition() . ")")->queryScalar();
    }
}
