<?php

class MbPrice extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_price';
    }

    public function rules()
    {
        return array(
            array('clinic, title','required'),
            array('alt, clinic, title, subway','length','max'=>255),
            array('create_time, update_time','safe'),
            array('price','numerical'),
            array('status, uid, author_id','numerical','integerOnly'=>true),
            array('uid, author_id','exist','attributeName'=>'uid','className'=>'MbUser'),
        );
    }

    public function beforeValidate()
    {
        if(!empty(Yii::app()->user->id)){
            $this->uid=Yii::app()->user->id;
            if($this->isNewRecord){
                $this->author_id=Yii::app()->user->id;
            }
        }
        return parent::beforeValidate();
    }

    public function relations()
    {
        return array(
            'u'=>array(self::BELONGS_TO,'MbUser','uid'),
            'a'=>array(self::BELONGS_TO,'MbUser','author_id'),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior'=>array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'createAttribute'=>'create_time',
                'updateAttribute'=>null,
            ),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'uid'=>'Редактор',
            'author_id'=>'Автор',
            'price'=>'Стоимость',
            'status'=>'Статус',
            'subway'=>'Метро',
            'clinic'=>'Клиника',
            'create_time'=>'Время создания',
            'update_time'=>'Время редактирования',
            'alt'=>'Альтернативная',
            'title'=>'Услуга',
        );
    }

}