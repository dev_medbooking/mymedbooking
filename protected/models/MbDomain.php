<?php

class MbDomain extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_domain';
    }

    public function rules()
    {
        return array(
            array('title','required'),
            array('title','unigue'),
        );
    }

}