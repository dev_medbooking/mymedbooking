<?php
class MbRecordServices extends CActiveRecord {

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName(){
		return 'api_record_services';
	}

	public function rules()
	{
		return array(
			array('record_id, service_id','safe'),
		);
	}

	public function relations()
	{
		return array(
			'service' => array(self::BELONGS_TO, 'MedbookingNodeService', 'service_id'),
		);
	}

}