<?php

class AdminInvoiceInsert extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'invoice_insert';
    }

    public function rules()
    {
        return array(
            array('name, update_time, domain, telephone, status, oktell_id','safe'),
        );
    }

}
