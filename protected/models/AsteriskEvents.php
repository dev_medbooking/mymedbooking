<?php
require(implode(DIRECTORY_SEPARATOR, array(
	__DIR__,
	'..',
	'vendor',
	'autoload.php'
)));
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\EventMessage;
/**
 * This is the model class for table "asterisk_events".
 *
 * The followings are the available columns in table 'asterisk_events':
 * @property string $id
 * @property string $eventcontent
 * @property integer $clientstatus
 * @property string $datetime
 * @property string $event
 * @property string $timestamp
 * @property string $channel
 * @property integer $channelstate
 * @property string $channelstatedesc
 * @property string $calleridnum
 * @property string $calleridname
 * @property string $accountcode
 * @property string $exten
 * @property string $context
 * @property string $uniqueid
 * @property string $queue
 * @property integer $position
 * @property integer $originalposition
 * @property string $holdtime
 * @property string $status
 * @property integer $count
 * @property string $connectedlinenum
 * @property string $connectedlinename
 * @property integer $cause
 * @property string $causetxt
 * @property string $bridgestate
 * @property string $bridgetype
 * @property string $channel1
 * @property string $channel2
 * @property string $uniqueid1
 * @property string $uniqueid2
 * @property string $callerid1
 * @property string $callerid2
 */
class AsteriskEvents extends CActiveRecord
{

	//private $queueCount;
	private $events=array('Newchannel','QueueCallerAbandon','Hold','Join','Hangup','Leave','Bridge');//,'UserEvent');
	private $queueAction=array('Join'=>'create','Bridge'=>'update','QueueCallerAbandon'=>'delete','Hangup'=>'delete','Leave'=>'delete');

	/*public function getQueueCount()
	{
		if (!$this->queueCount)
			$this->queueCount=0;
		return $this->queueCount;
	}

	public function setQueueCount($value)
	{
		$this->queueCount = $value;
	}
	*/
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AsteriskEvents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'asterisk_events';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('datetime, event, timestamp', 'required'),
			array('clientstatus, channelstate, position, originalposition, count, cause', 'numerical', 'integerOnly'=>true),
			array('datetime, holdtime, connectedlinenum', 'length', 'max'=>11),
			array('event, accountcode, context,timestamp', 'length', 'max'=>20),
			array('channel, calleridname, connectedlinename, causetxt, channel1, channel2', 'length', 'max'=>50),
			array('channelstatedesc, uniqueid, queue, uniqueid1, uniqueid2', 'length', 'max'=>25),
			array('calleridnum, exten, callerid1, callerid2', 'length', 'max'=>15),
			array('status', 'length', 'max'=>3),
			array('bridgestate, bridgetype', 'length', 'max'=>6),
			array('eventcontent', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, eventcontent, clientstatus, datetime, event, timestamp, channel, channelstate, channelstatedesc, calleridnum, calleridname, accountcode, exten, context, uniqueid, queue, position, originalposition, holdtime, status, count, connectedlinenum, connectedlinename, cause, causetxt, bridgestate, bridgetype, channel1, channel2, uniqueid1, uniqueid2, callerid1, callerid2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'eventcontent' => 'Eventcontent',
			'clientstatus' => 'Clientstatus',
			'datetime' => 'Datetime',
			'event' => 'Event',
			'timestamp' => 'Timestamp',
			'channel' => 'Channel',
			'channelstate' => 'Channelstate',
			'channelstatedesc' => 'Channelstatedesc',
			'calleridnum' => 'Calleridnum',
			'calleridname' => 'Calleridname',
			'accountcode' => 'Accountcode',
			'exten' => 'Exten',
			'context' => 'Context',
			'uniqueid' => 'Uniqueid',
			'queue' => 'Queue',
			'position' => 'Position',
			'originalposition' => 'Originalposition',
			'holdtime' => 'Holdtime',
			'status' => 'Status',
			'count' => 'Count',
			'connectedlinenum' => 'Connectedlinenum',
			'connectedlinename' => 'Connectedlinename',
			'cause' => 'Cause',
			'causetxt' => 'Causetxt',
			'bridgestate' => 'Bridgestate',
			'bridgetype' => 'Bridgetype',
			'channel1' => 'Channel1',
			'channel2' => 'Channel2',
			'uniqueid1' => 'Uniqueid1',
			'uniqueid2' => 'Uniqueid2',
			'callerid1' => 'Callerid1',
			'callerid2' => 'Callerid2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('eventcontent',$this->eventcontent,true);
		$criteria->compare('clientstatus',$this->clientstatus);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('event',$this->event,true);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('channel',$this->channel,true);
		$criteria->compare('channelstate',$this->channelstate);
		$criteria->compare('channelstatedesc',$this->channelstatedesc,true);
		$criteria->compare('calleridnum',$this->calleridnum,true);
		$criteria->compare('calleridname',$this->calleridname,true);
		$criteria->compare('accountcode',$this->accountcode,true);
		$criteria->compare('exten',$this->exten,true);
		$criteria->compare('context',$this->context,true);
		$criteria->compare('uniqueid',$this->uniqueid,true);
		$criteria->compare('queue',$this->queue,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('originalposition',$this->originalposition);
		$criteria->compare('holdtime',$this->holdtime,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('count',$this->count);
		$criteria->compare('connectedlinenum',$this->connectedlinenum,true);
		$criteria->compare('connectedlinename',$this->connectedlinename,true);
		$criteria->compare('cause',$this->cause);
		$criteria->compare('causetxt',$this->causetxt,true);
		$criteria->compare('bridgestate',$this->bridgestate,true);
		$criteria->compare('bridgetype',$this->bridgetype,true);
		$criteria->compare('channel1',$this->channel1,true);
		$criteria->compare('channel2',$this->channel2,true);
		$criteria->compare('uniqueid1',$this->uniqueid1,true);
		$criteria->compare('uniqueid2',$this->uniqueid2,true);
		$criteria->compare('callerid1',$this->callerid1,true);
		$criteria->compare('callerid2',$this->callerid2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function log_events(EventMessage $event) {
		if (in_array($event->getName(),$this->events)) {

			$method = false;

			$stat = new AsteriskStatistic();

			$this->attributes = $stat->attributes = $event->getKeys();
			$this->datetime = $stat->datetime = time();
			$this->eventcontent = $stat->eventcontent = $event->getRawContent();

			try {
				$stat->save();
			} catch (Exception $e) {
				echo $e->getMessage() . "\n";
				$this->writeLog("AsteriskStatistic",$e->getMessage());
			}

			if (array_key_exists($event->getName(),$this->queueAction))
				$method=$this->queueAction[$event->getName()]."Row";
			if ($method && method_exists($this,$method)) {
				$this->$method();
			}
		}
	}

	private function createRow() {
		try
		{
			$this->save();
		} catch (Exception $e) {
			echo $e->getMessage() . "\n";
			$this->writeLog('',$e->getMessage());
		}
	}

	private function updateRow(){
		$sql="UPDATE ".$this->tableName()." SET clientstatus=2 WHERE uniqueid in ('".$this->uniqueid1."','".$this->uniqueid2."')";
		try {
			Yii::app()->db->createCommand($sql)->execute();
		} catch(Exception $e){
			echo $e->getMessage() . "\n";
			$this->writeLog('',$e->getMessage());
		}
	}

	private function deleteRow() {
		$sql="DELETE from ".$this->tableName()." WHERE uniqueid ='".$this->uniqueid."'";
		try {
			Yii::app()->db->createCommand($sql)->execute();
		} catch(Exception $e){
			echo $e->getMessage() . "\n";
			$this->writeLog('',$e->getMessage());
		}
	}

	private function writeLog($comment=__CLASS__,$message=false) {
		echo "ОШИБКИ\nДата: ".date("H:i:s d.m.Y")."\n Событие:".$this->eventcontent."\n";
		if ($message)
			echo "ERROR: ".$message."\n";
		if ($comment)
			echo "Комментарий: ".$comment."\n";
		echo "===========================================================================\n\n";
	}

	public function getClientCount($queue=false) {
		if ($queue)
			return self::model()->find()->count();
		else
			return self::model()->count(array(
			'select'=>'id',
			'condition'=>'queue=:queue',
			'params'=>array(':queue'=>$queue),
		));
	}

	public function getClientMaxTime($queue=false) {
		$where="";
		$now=time();
		$mins=0;
		$secs="00";
		$where_params=array();
		if ($queue) {
			$where=" AND queue = :".$queue;
			$where_params=array(":queue" => $queue);
		}
		$sql = "select min(datetime) as datetime from ".$this->tableName()." where clientstatus=1".$where;
		$data = self::model()->findBySql($sql,$where_params);
		if ($data['datetime']) {
			$mins = (int)(($now - $data['datetime']) / 60);
			$secs = sprintf("%02d",$now - $data['datetime'] - $mins * 60);
		}
		return $mins.":".$secs;
	}
}