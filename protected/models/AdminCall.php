<?php

class AdminCall extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'call';
    }

    public function rules()
    {
        return array(
            array('name, startparam1, async, timeout, create_time, mp3, context, startparam2','safe'),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior'=>array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'createAttribute'=>'create_time',
                'updateAttribute'=>null,
            ),
        );
    }
    
}
