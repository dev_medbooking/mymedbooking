<?php
class MbRecordHistory extends CActiveRecord {

	public function tableName(){
		return 'record_history';
	}

	public function rules()
	{
		return array(
			array('record_id, date, uid, old, new','safe'),
		);
	}

	public function relations()
	{
		return array(
			'history' => array(self::BELONGS_TO, 'MbRecord', 'record_id'),
			'user_history' => array(self::BELONGS_TO, 'MbUser', 'uid')
		);
	}

}