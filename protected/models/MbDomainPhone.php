<?php

class MbDomainPhone extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_domain_phone';
    }

    public function rules()
    {
        return array(
            array('domain','required'),
            array('phone','required'),
            array('description , status, domain_name','safe'),
        );
    }

}