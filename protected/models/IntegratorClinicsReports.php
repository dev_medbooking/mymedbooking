<?php

class IntegratorClinicsReports extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'integrator_clinic_reports';
	}

	public function rules()
	{
		return array(
			array('summ, patient_record, patient_success, patient_count, patient_repeat, patient_diag','safe'),
		);
	}

	public function relations()
	{
		return array(
			'law_reports'=>array(self::BELONGS_TO, 'IntegratorLawReports', 'law_report_id'),
			'clinic_reports'=>array(self::BELONGS_TO, 'IntegratorClinics', 'clinic_id'),
			'clinic_report'=>array(self::BELONGS_TO, 'IntegratorClinics', 'clinic_id')
		);
	}

}
