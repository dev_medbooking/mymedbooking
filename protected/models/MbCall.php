<?php

class MbCall extends CActiveRecord
{

    public $addDomain='MbDomain';
    public $qty;
    
    public function getDomainId()
    {
        if(!empty($this->domain)){
            $_addDomain=$this->addDomain;
            $domain=$_addDomain::model()->findByAttributes(array('title'=>$this->domain));
            if(!empty($domain->id)){
                return $domain->id;
            }
        }
    }

    public function getColor()
    {
        $output='row-empty';
        if(!empty($this->domain)){
            switch($this->domain)
            {
                case 'medbooking.com':
                    $output='row-medbooking';
                    break;
                case 'testpuls.ru':
                    $output='row-testpuls';
                    break;
                case 'timetovisit.ru':
                    $output='row-timetovisit';
                    break;
                case 'fromed.ru':
                    $output='row-fromed';
                    break;
            }
        }
        return $output;
    }
    
    public function getHostColor(){
        $color='#fff';
        if($this->domain=='medbooking.com'){
            $color ='#0f70b3';
        }elseif($this->domain=='testpuls.ru'){
            $color ='#b0d083';
        }elseif($this->domain=='timetovisit.ru'){
            $color ='#ffa500';
        }elseif($this->domain=='fromed.ru'){
            $color = '#c66f6a';
        }
        return $color;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_call';
    }

    public function rules()
    {
        return array(
            array('telephone','required','on'=>'api'),
            array('message, domain','required'),
            array('create_time, route','safe'),
            array('note','safe'),
            array('message, domain, telephone, name','length','max'=>255),
            array('uid, author_id','exist','attributeName'=>'uid','className'=>'MbUser'),
            array('message','safe'),
        );
    }

    public function beforeValidate()
    {
        if(!empty($this->telephone)){
            $this->telephone=str_replace(array("+7","(",")"," ","-","_"),array("","","","","",""),$this->telephone);
        }
        // пользователь или админ
        if(!empty(Yii::app()->user->id)){
            if(empty($this->author_id)){
                $this->author_id=Yii::app()->user->id;
            }
            $this->uid=Yii::app()->user->id;
        }
        return parent::beforeValidate();
    }

    public function relations()
    {
        return array(
            'u_call'=>array(self::BELONGS_TO,'MbUser','uid'),
            'a_call'=>array(self::BELONGS_TO,'MbUser','author_id'),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior'=>array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'createAttribute'=>'create_time',
                'updateAttribute'=>null,
            ),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'note'=>'Комментарии',
            'message'=>'Сообщение',
            'create_time'=>'Время создания',
            'formattedCreateTime'=>'Время создания',
            'domain'=>'Хост',
            'telephone'=>'Телефон',
            'route'=>'Путь возврата',
            'name'=>'Имя',
        );
    }

    /**
     * @return string
     */
    public function getFormattedCreateTime()
    {
        return date('d-m-Y H:i',strtotime($this->create_time));
    }

}