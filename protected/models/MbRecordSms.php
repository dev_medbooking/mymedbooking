<?php

class MbRecordSms extends CActiveRecord
{

    public $test_mode=false;
    public $cron_sender;
    public $sms_operation='';
    public $sms_login='bronirovanie';
    public $sms_password='tt289dsn';
    public $sms_shortcode;
    public $sms_msisdn;
    public $sms_text;
    public $sms;
    public $sms_response;
    public $sms_send_admin=false;
    public $sn="medbooking";

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_record_sms';
    }

    public function rules()
    {
        return array(
            array('message, record_id','required'),
//            array('status','length','max'=>1),
            array('message, create_time','safe'),
            array('author_id, t_id','default','setOnEmpty'=>true,'value'=>null),
            array('author_id','exist','attributeName'=>'uid','className'=>'MbUser'),
            array('record_id','exist','attributeName'=>'id','className'=>'MbRecord'),
            array('t_id','exist','attributeName'=>'id','className'=>'MbSmsTemplate'),
            array('t_id, status, record_id, author_id','numerical','integerOnly'=>true),
        );
    }

    public function relations()
    {
        return array(
            'a_ars'=>array(self::BELONGS_TO,'MbUser','author_id'),
            'r_ars'=>array(self::BELONGS_TO,'MbRecord','record_id'),
            't_ars'=>array(self::BELONGS_TO,'MbSmsTemplate','t_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            't_id'=>'Sms template',
            'message'=>'Сообщение',
            'status'=>'Статус',
            'author_id'=>'Автор сообщения',
            'record_id'=>'Запись на прием',
            'create_time'=>'Время создания',
        );
    }
    public function russianDate($date){
        $date=explode("-", $date);
        $m='';
        switch ($date[1]){
            case 1: $m='января'; break;
            case 2: $m='февраля'; break;
            case 3: $m='марта'; break;
            case 4: $m='апреля'; break;
            case 5: $m='мая'; break;
            case 6: $m='июня'; break;
            case 7: $m='июля'; break;
            case 8: $m='августа'; break;
            case 9: $m='сентября'; break;
            case 10: $m='октября'; break;
            case 11: $m='ноября'; break;
            case 12: $m='декабря'; break;
        }
        return $date[0].'&nbsp;'.$m;
    }
    public function beforeSave()
    {
        Yii::log('MbRecordSms beforeSave() start');
        if($this->sms_send_admin){
            if(!empty($this->record_id)){
                $record=MbRecord::model()->with(array('info'))->findByPk($this->record_id);


                if($this->t_id == 32) {
                    $date = !empty($record->reception_time)?date("d-m-Y",strtotime($record->reception_time)):'';
                    $date=$this->russianDate($date);

                } else {
                    $date = !empty($record->reception_time)?date("d-m-Y",strtotime($record->reception_time)):'';
                }
                if(!empty($record->telephone)){
                    if(!empty($this->message)){
                        $record->smsCheck=0; // здесь же обновляем данные по клинике и врачу.
                        $record->save(false);
                        $this->sms_msisdn=$record->telephone;
                        $this->sms_text=strtr($this->message,array(
                            //'${name}'=>!empty($record->name)?$record->name:'',
                            '${name}'=>!empty($record->first_name)?$record->first_name:$record->name,
                            '${clinic}'=>!empty($record->clinic_name)?$record->clinic_name:'',
                            '${inclinic}'=>!empty($record->clinic_name)?'1=в клинику '.$record->clinic_name:'',
                            '${date}'=>$date,
                            '${time}'=>!empty($record->reception_time)?date("H:i",strtotime($record->reception_time)):'',
                            '${intime}'=>!empty($record->reception_time)?'в '.date("H:i",strtotime($record->reception_time)):'',
                            '${doctor}'=>!empty($record->doctor_name)?$record->doctor_name:'',
                            '${indoctor}'=>!empty($record->doctor_name)?'к '.$record->doctor_name:'',
                            '${intodoctor}'=>!empty($record->doctor_name)?'к врачу '.$record->doctor_name:'',
                            '${todoctor}'=>!empty($record->doctor_name)?', врач: '.$record->doctor_name:'',
                            '${station}'=>!empty($record->info->subway)?$record->info->subway:'',
                            '${tostation}'=>!empty($record->info->subway)?', метро: '.$record->info->subway:'',
                            '${address}'=>!empty($record->info->address)?$record->info->address:'',
                            '${toaddress}' => !empty($record->info->address) ? ', адрес: ' . $record->info->address : '',
                            '${host}'=>!empty($record->domain)?$record->domain:'Medbooking.com',
                        ));
                        $this->message=strtr($this->message,array(
                            //'${name}'=>!empty($record->name)?$record->name:'',
                            '${name}'=>!empty($record->first_name)?$record->first_name:$record->name,
                            '${clinic}'=>!empty($record->clinic_name)?$record->clinic_name:'',
                            '${inclinic}'=>!empty($record->clinic_name)?'2=в клинику '.$record->clinic_name:'',
                            '${date}'=>$date,
                            '${time}'=>!empty($record->reception_time)?date("H:i",strtotime($record->reception_time)):'',
                            '${intime}'=>!empty($record->reception_time)?'в '.date("H:i",strtotime($record->reception_time)):'',
                            '${doctor}'=>!empty($record->doctor_name)?$record->doctor_name:'',
                            '${indoctor}'=>!empty($record->doctor_name)?'к '.$record->doctor_name:'',
                            '${intodoctor}'=>!empty($record->doctor_name)?'к врачу '.$record->doctor_name:'',
                            '${todoctor}'=>!empty($record->doctor_name)?', врач: '.$record->doctor_name:'',
                            '${station}'=>!empty($record->info->subway)?$record->info->subway:'',
                            '${tostation}'=>!empty($record->info->subway)?', метро: '.$record->info->subway:'',
                            '${address}'=>!empty($record->info->address)?$record->info->address:'',
                            '${toaddress}'=>!empty($record->info->address)?', адрес:'.$record->info->address:'',
                            '${host}'=>!empty($record->domain)?$record->domain:'Medbooking.com',
                        ));
                    } else{
                        $this->sms_text='Шаблон не установлен';
                    }
                    $this->sms_shortcode=$this->sn;
                    $this->sms_response=$this->sendSms();
                }
            }
        }
        return parent::beforeSave();
    }

    public function afterSave()
    {
        Yii::app()->db->createCommand("UPDATE api_record_sms SET status=1 WHERE id=".$this->id)->execute();
        return parent::afterSave();
    }

    public function sendSms()
    {
        Yii::log("sendSms() start");
        if(!empty($this->sms_login)&&!empty($this->sms_password)&&!empty($this->sms_msisdn)&&!empty($this->sms_shortcode)&&!empty($this->sms_text)){
            $sms_msisdn='7'.str_replace(array("+7","(",")"," ","-","."),array("","","","","",""),$this->sms_msisdn);
            if($this->test_mode){
                $sms_msisdn='380962102011';
            }
            $array=array(
                'login'=>$this->sms_login,
                'psw'=>$this->sms_password,
                'phones'=>$sms_msisdn,
                'mes'=>iconv("utf-8","windows-1251//TRANSLIT",$this->sms_text),
                'sender'=>!empty($this->sms_shortcode)?$this->sms_shortcode:"",
                'cost'=>'0',
                'fmt'=>'0',
            );
            $ch=curl_init();
            $link=http_build_query($array);
            curl_setopt($ch,CURLOPT_URL,"http://smsc.ru/sys/send.php?".$link);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            $response=curl_exec($ch);
            Yii::log("curl response: " . $response);
            if(curl_error($ch)) {
                Yii::log("curl last error: " . CVarDumper::dumpAsString(curl_error($ch)));
            }
            curl_close($ch);
            if(!empty($response)){
                $pos=strripos($response,"error");
                if($pos===false){
                    $list=explode(' - ',$response);
                    if(!empty($list[2])){
                        return $list[2];
                    }
                } else{
                    return false;
                }
            }
        } else {
            Yii::log("sendSms() condition: sms_login($this->sms_login) sms_password($this->sms_password) sms_msisdn($this->sms_msisdn) sms_shortcode($this->sms_shortcode) sms_text($this->sms_text)");
        }
    }

}