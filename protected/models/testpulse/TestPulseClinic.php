<?php

/**
 * This is the model class for table "clinic_diagnostic".
 *
 * The followings are the available columns in table 'clinic_diagnostic':
 * @property integer $id
 * @property string $title
 * @property string $translit
 * @property integer $status
 * @property string $description
 * @property string $address
 * @property string $image
 * @property string $create_time
 * @property string $update_time
 * @property integer $startyear
 * @property double $lat
 * @property double $lng
 * @property string $body
 * @property string $companyname
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $regime_byd
 * @property string $regime_mon
 * @property string $regime_tue
 * @property string $regime_wed
 * @property string $regime_thu
 * @property string $regime_fri
 * @property string $regime_sat
 * @property string $regime_sun
 * @property string $telephone
 * @property string $email
 * @property integer $uid
 * @property string $sames
 * @property double $start_rate
 * @property integer $district_id
 * @property integer $none_status
 * @property double $modificator
 * @property integer $clinic_city_line_id
 * @property integer $clinic_city_id
 * @property integer $euro_price
 * @property integer $depth
 *
 * The followings are the available model relations:
 * @property MedbookingDistrict $district
 * @property ClinicDiagnosticRating[] $clinicDiagnosticRatings
 * @property ClinicPrice[] $clinicPrices
 * @property SubwayClinicDiagnostic[] $subwayClinicDiagnostics
 */
class TestPulseClinic extends CActiveRecord
{
    public $_subway_name;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MedbookingDiagnosticClinic the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return CDbConnection database connection
     */
    public function getDbConnection()
    {
        return Yii::app()->dbTestPulse;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'clinic';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'clinic_subway_s'         => array(self::HAS_MANY, 'TestPulseSubwayClinic', 'clinic_id'),
            'clinic_category_s'       => array(
                self::HAS_MANY,
                'TestPulseClinicPrice',
                'clinic_id',
                'with' => array(
                    'catc_category' => array(
                        'condition' => ((!empty($_REQUEST['only_mrt'])) ? 'catc_category.vladimirov_status=1' : '')
                    )
                )
            ),
            'clinicPrices' => [self::HAS_MANY, 'TestPulseClinicPrice', 'clinic_id']
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'title'            => 'Title',
            'translit'         => 'Translit',
            'status'           => 'Status',
            'description'      => 'Description',
            'address'          => 'Address',
            'image'            => 'Image',
            'create_time'      => 'Create Time',
            'update_time'      => 'Update Time',
            'startyear'        => 'Startyear',
            'lat'              => 'Lat',
            'lng'              => 'Lng',
            'body'             => 'Body',
            'companyname'      => 'Companyname',
            'meta_title'       => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords'    => 'Meta Keywords',
            'regime_byd'       => 'Regime Byd',
            'regime_mon'       => 'Regime Mon',
            'regime_tue'       => 'Regime Tue',
            'regime_wed'       => 'Regime Wed',
            'regime_thu'       => 'Regime Thu',
            'regime_fri'       => 'Regime Fri',
            'regime_sat'       => 'Regime Sat',
            'regime_sun'       => 'Regime Sun',
            'telephone'        => 'Telephone',
            'email'            => 'Email',
            'uid'              => 'Uid',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('translit', $this->translit, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('startyear', $this->startyear);
        $criteria->compare('lat', $this->lat);
        $criteria->compare('lng', $this->lng);
        $criteria->compare('body', $this->body, true);
        $criteria->compare('companyname', $this->companyname, true);
        $criteria->compare('meta_title', $this->meta_title, true);
        $criteria->compare('meta_description', $this->meta_description, true);
        $criteria->compare('meta_keywords', $this->meta_keywords, true);
        $criteria->compare('regime_byd', $this->regime_byd, true);
        $criteria->compare('regime_mon', $this->regime_mon, true);
        $criteria->compare('regime_tue', $this->regime_tue, true);
        $criteria->compare('regime_wed', $this->regime_wed, true);
        $criteria->compare('regime_thu', $this->regime_thu, true);
        $criteria->compare('regime_fri', $this->regime_fri, true);
        $criteria->compare('regime_sat', $this->regime_sat, true);
        $criteria->compare('regime_sun', $this->regime_sun, true);
        $criteria->compare('telephone', $this->telephone, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('uid', $this->uid);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    protected function beforeSave()
    {
        throw new DomainException('Нельзя сохранить');
    }

    public function listPrice()
    {
        $array = array();
        if (!empty($this->clinic_category_s)) {
            foreach ($this->clinic_category_s as $value) {
                if (!empty($value->category_price_id)) {
                    if (!empty($value->catc_category->name_alt)) {
                        $name = $value->catc_category->name_alt;
                    } elseif (!empty($value->catc_category->name)) {
                        $name = $value->catc_category->name;
                    } else {
                        $name = '';
                    }
                    $array[$value->catc_category->mb_id] = array(
                        'id'           => $value->catc_category->mb_id,
                        'name'         => $name,
                        'price'        => !empty($value->old_price) ? $value->old_price : (string)$value->price,
                        'action_price' => !empty($value->old_price) ? $value->price : '',
                    );
                }
            }
        }

        return array_values($array);
    }

    public function getSubway_name()
    {
        $array = array();
        $data = $this->clinic_subway_s;
        if (!empty($data)) {
            foreach ($data as $value) {
                if (!empty($value['sub']['title'])) {
                    $array[] = $value['sub']['title'];
                }
            }
        }
        $this->_subway_name = implode(", ", $array);
        return $this->_subway_name;
    }
}