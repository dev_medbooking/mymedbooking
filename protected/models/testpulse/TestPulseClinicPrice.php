<?php

class TestPulseClinicPrice extends CActiveRecord
{
    public function getDbConnection()
    {
        return Yii::app()->dbTestPulse;
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'clinic_price';
    }

    public function relations()
    {
        return array(
            'catc_category' => array(self::BELONGS_TO, 'TestPulseCategoryPrice', 'category_price_id'),
            'clinic' => array(self::BELONGS_TO, 'TestPulseClinic', 'clinic_id'),
        );
    }

    protected function beforeSave()
    {
        throw new DomainException('Нельзя сохранить');
    }
}
