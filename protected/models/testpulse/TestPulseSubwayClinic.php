<?php

class TestPulseSubwayClinic extends CActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'subway_clinic';
    }

    public function getDbConnection()
    {
        return Yii::app()->dbTestPulse;
    }

    public function relations()
    {
        return array(
            'clinic' => array(self::BELONGS_TO, 'TestPulseClinic', 'clinic_id'),
            'sub'    => array(self::BELONGS_TO, 'TestPulseSubway', 'subway_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'        => 'ID',
            'clinic_id' => 'Клиника',
            'subway_id' => 'Метро',
        );
    }

    protected function beforeSave()
    {
        throw new DomainException('Нельзя сохранить');
    }
}