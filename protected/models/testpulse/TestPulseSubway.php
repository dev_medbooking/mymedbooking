<?php

class TestPulseSubway extends CActiveRecord
{

    private $_coordinate_id;

    public function beforeValidate()
    {
        if(!empty($_POST[__CLASS__]['coordinate_id'])){
            $list=explode(",",$_POST[__CLASS__]['coordinate_id']);
            if(!empty($list[1])){
                $this->lat=$list[0];
                $this->lng=$list[1];
            }
        }
        return parent::beforeValidate();
    }

    public function getCoordinate_id()
    {
        if(!empty($this->lat)&&!empty($this->lng)){
            $this->_coordinate_id=$this->lat.','.$this->lng;
        } else{
            $this->_coordinate_id='';
        }
        return $this->_coordinate_id;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'subway';
    }

    public function getDbConnection()
    {
        return Yii::app()->dbTestPulse;
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'title'=>'Станция',
            'top'=>'Текст вверху',
            'lat'=>'Широта',
            'lng'=>'Долгота',
            'subway_line_id'=>'Ветка метро ID',
            'area_id'=>'Район города',
            'translit'=>'Транслитерация',
            'coordinate_id'=>'Метка на карте',
        );
    }

    public function beforeSave()
    {
        throw new DomainException('Нельзя сохранить');
    }

}