<?php

class IntegratorClinic extends CActiveRecord
{

	private $_moveArray = array(); //массив с данныами для перемещения отчетов

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'integrator_clinic';
    }

	public function beforeSave()
	{
		$clinicReports = IntegratorClinicsReports::model()->findAllByAttributes(array('clinic_id' => $this->primaryKey));
		if ( ! empty($clinicReports)) {
			foreach ($clinicReports as $k => $v) {
				$lawReports = IntegratorLawReports::model()->findByPk($v->law_report_id);
				if ( ! empty($lawReports)) {
					$this->_moveArray['lawReports'][] = $lawReports;
				}
			}
		}
		return parent::beforeSave();
	}

	public function afterSave()
	{
		if (!empty($this->_moveArray['lawReports'])) {
			foreach ($this->_moveArray['lawReports'] as $k => $v) {
				$lawReport = IntegratorLawReports::model()->findByAttributes(array('law_id' => $this->all_id, 'report_date' => $v->report_date));
				if (empty($lawReport)) {
					$lawReport = new IntegratorLawReports();
					$lawReport->report_date = $v->report_date;
					$lawReport->create_date = $v->create_date;
					$lawReport->law_id = $this->all_id;
					$lawReport->save();
				}
				$clinicReports = IntegratorClinicsReports::model()->findByAttributes(array('law_report_id' => $v->primaryKey, 'clinic_id' => $this->primaryKey));
				if (!empty($clinicReports)) {
					$clinicReports->law_report_id = $lawReport->primaryKey;
					$clinicReports->save();
					IntegratorLaws::updateLawFields($v->primaryKey);
					IntegratorLaws::updateLawFields($lawReport->primaryKey);
				}
			}
		}
		return parent::afterSave();
	}

    public function rules()
    {
        return array(
            array('create_time, update_time, title, alias, telephone1, telephone2, telephone3, telephone4, name, title_company, date, comment','safe'),
            array('external_id, fromed_id, timetovisit_id, diagnostica_id, testpuls_id, uid, status, medbooking_id, is_status, all_id','numerical','integerOnly'=>true),
            array('price, all_price','type','type'=>'float'),
            array('price_category, predoplata, cont_person, problems, k2, k1, numb_agree, add_agree, data_agree, po, subway, address, contact, agreement','safe'),
            array('percent, price_value, com_med, com_diagn, com_cosmo','numerical'),
            array('percent, percent_med, percent_diagn, percent_cosmo','numerical'),
            array('koll_success, koll_come','numerical','integerOnly'=>true),
            array('status_site','numerical','integerOnly'=>true),
            array('email','email'),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior'=>array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'createAttribute'=>'create_time',
                'updateAttribute'=>'update_time',
            ),
        );
    }

    /**
     * Связь с другими моделями
     * @return type
     */
    public function relations()
    {
        return array(
            'u' => array(self::BELONGS_TO, 'MbUser', 'uid'),
	        'medbooking_clinic' => array(self::BELONGS_TO, 'MedbookingClinic', 'medbooking_id'),
	        'diagnostic_clinic' => array(self::BELONGS_TO, 'DiagnosticMedbookingClinic', 'diagnostica_id')
        );
    }

}
