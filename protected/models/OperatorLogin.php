<?php

/**
 * This is the model class for table "operator_login".
 *
 * The followings are the available columns in table 'operator_login':
 * @property string $id
 * @property integer $uid
 * @property string $login
 * @property string $date
 * @property string $datetime
 */
class OperatorLogin extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return OperatorLogin the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'operator_login';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('login, date, datetime', 'required'),
            array('uid', 'numerical', 'integerOnly'=>true),
            array('login', 'length', 'max'=>30),
            array('nameperson', 'length', 'max'=>124),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, uid, login, date, datetime, nameperson', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            //'id' => 'ID',
            //'uid' => 'Uid',
            'login' => 'Логин',
            'date' => 'Дата',
            'datetime' => 'Дата/время',
            'nameperson' => 'ФИО'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {

        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria=new CDbCriteria;

        //$criteria->compare('id',$this->id,true);
        //$criteria->compare('uid',$this->uid);
        $criteria->compare('login',$this->login,true);
        $criteria->compare('date',$this->date,true);
        $criteria->compare('nameperson',$this->nameperson,true);
        //$criteria->compare('datetime',$this->datetime,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
            'sort' => array(
                'defaultOrder' => 'datetime DESC',
            ),
        ));
    }
}