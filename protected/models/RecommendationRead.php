<?php

Yii::import('application.models._base.BaseRecommendationRead');

class RecommendationRead extends BaseRecommendationRead
{
    /**
     * @param string $className
     *
     * @return RecommendationRead
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function isUnread($recommendationId, $userId = null)
    {
        /**
         * @var CDbCommand $commandBuilder
         */
        $commandBuilder = Yii::app()->db->createCommand();
        $result = $commandBuilder->select(
            [
                'count(*) as c',
            ]
        )->from('recommendation_read')->where(
            'ident_id=:uid and recommendation_id=:rid', [
                ':uid' => $userId ? $userId : CurrentUser::getId(),
                ':rid' => $recommendationId
            ]
        )->queryAll();

        return (boolean)$result[0]['c'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), []);
    }

    /**
     * @inheritdoc
     */
    public function relations()
    {
        return array_merge(parent::relations(), []);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), []);
    }
}