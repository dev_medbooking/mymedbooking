<?php

Yii::import('application.models._base.BasePartnerSiteUrl');

class PartnerSiteUrl extends BasePartnerSiteUrl
{
    /**
     * @param string $className
     *
     * @return PartnerSiteUrl
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getUrl($action)
    {
        $url = '';
        if ($action == "delete") {
            $url = Url::to('/mbTable/delSiteUrl', ["id" => $this->id]);
        }
        return Url::to($url);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), []);
    }

    public function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            if (!$this->created_at) {
                $this->created_at = new CDbExpression('NOW()');
            }
            $this->updated_at = new CDbExpression('NOW()');
        }
        if ($this->site_id && $this->site) {
            $array1 = parse_url($this->site->host);
            $host = $array1['host'];
            try {
                if (strpos($this->url, '://') === false) {
                    $this->url = 'http://' . $this->url;
                }
                $array = parse_url($this->url);
                if (!isset($array['path'])) {
                    $array['path'] = $array['host'];
                    unset($array['host']);
                }
                if (CArray::get($array, 'host', $host) != $host) {
                    $array['path'] = $array['host'] . '/' . ltrim($array['path'], '/');
                }
                if (!$array || !isset($array['scheme']) || !isset($array['path'])) {
                    throw new Exception();
                }
            } catch (Exception $e) {
                $this->addError('url', 'Необходимо указать ссылку');
                return false;
            }
            $q = CArray::get($array, 'query');
            $this->url = $array['path'] . ($q ? "?$q" : '');
        }
        return parent::beforeSave();
    }

    /**
     * @inheritdoc
     */
    public function relations()
    {
        return array_merge(parent::relations(), []);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), []);
    }

    public function getFullUrl()
    {
        return $this->site->host . '/' . ltrim($this->url, '/');
    }
}