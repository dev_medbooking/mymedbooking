<?php

/**
 * This is the model class for table "sessions_tracking".
 *
 * The followings are the available columns in table 'sessions_tracking':
 * @property string $id
 * @property string $host
 * @property string $current_url
 * @property string $referer
 * @property string $ip
 * @property string $server_addr
 * @property string $remote_addr
 * @property string $remote_host
 * @property string $sessionid
 * @property string $get
 * @property string $datetime
 * @property string $tracking_phone
 */
class SessionsTracking extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SessionsTracking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sessions_tracking';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('host, ip, sessionid, datetime', 'required'),
			array('host, remote_host', 'length', 'max'=>50),
			array('current_url, referer', 'length', 'max'=>512),
			array('ip, server_addr, remote_addr, tracking_phone', 'length', 'max'=>15),
			array('sessionid', 'length', 'max'=>64),
			array('datetime', 'length', 'max'=>11),
			array('get, project', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, host, current_url, referer, ip, server_addr, remote_addr, remote_host, sessionid, get, datetime, tracking_phone, project', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'host' => 'Host',
			'current_url' => 'Current Url',
			'referer' => 'Referer',
			'ip' => 'Ip',
			'server_addr' => 'Server Addr',
			'remote_addr' => 'Remote Addr',
			'remote_host' => 'Remote Host',
			'sessionid' => 'Sessionid',
			'get' => 'Get',
			'datetime' => 'Datetime',
			'tracking_phone' => 'Tracking Phone',
			'project' => 'Проект'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('host',$this->host,true);
		$criteria->compare('current_url',$this->current_url,true);
		$criteria->compare('referer',$this->referer,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('server_addr',$this->server_addr,true);
		$criteria->compare('remote_addr',$this->remote_addr,true);
		$criteria->compare('remote_host',$this->remote_host,true);
		$criteria->compare('sessionid',$this->sessionid,true);
		$criteria->compare('get',$this->get,true);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('tracking_phone',$this->tracking_phone,true);
		$criteria->compare('project',$this->project,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 20,
			),
			'sort' => array(
				'defaultOrder' => 'id DESC',
			),
		));
	}
}