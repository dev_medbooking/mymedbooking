<?php

/**
 * This is the model class for table "missed_calls_callback".
 *
 * The followings are the available columns in table 'missed_calls_callback':
 * @property string $id
 * @property string $datetime
 * @property string $ext
 * @property string $callerid
 * @property string $project
 */
class MissedCallsCallback extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MissedCallsCallback the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'missed_calls_callback';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('datetime, ext, callerid, project', 'required'),
			array('datetime, ext', 'length', 'max'=>11),
			array('callerid, project, did', 'length', 'max'=>15),
			array('uniqueid', 'length', 'max'=>30),
			array('direct','in','range'=>array('in','out'),'allowEmpty'=>false),
			array('id, datetime, ext, callerid, project, direct, uniqueid, did', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'datetime' => 'Datetime',
			'ext' => 'Ext',
			'callerid' => 'Callerid',
			'project' => 'Project',
			'direct' => 'Направление',
			'uniqueid' => 'uniqueid',
			'did' => 'номер, набираемый клиентом'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('callerid',$this->callerid,true);
		$criteria->compare('project',$this->project,true);
		$criteria->compare('direct',$this->direct,true);
		$criteria->compare('uniqueid',$this->uniqueid,true);
		$criteria->compare('did',$this->did,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeValidate()
	{
		$direct_array=array("in","out");
		$this->datetime=time();
		//$this->callerid=substr($_REQUEST['callerid'],1);
		//$this->project="+7(".substr($_REQUEST['project'],1,3).")".substr($_REQUEST['project'],4,3)."-".substr($_REQUEST['project'],7,4);
		$this->direct = (isset($_REQUEST['direct'])?strtolower($_REQUEST['direct']):"in");
		if (!in_array($this->direct,$direct_array))
			$this->direct=$direct_array[0];
		return parent::beforeValidate();
	}

	public function afterSave()
	{
		$pairs="";
		if (substr($this->project,0,4)=='7812'){
			foreach ($this->attributes as $k=>$v) {
				$pairs.=", ".$k." = '".$v."'";
			}
			$pairs=substr($pairs,1);
			$sql="insert into ".$this->tableName()." set ".$pairs;
			try {
				Yii::app()->db_ba->createCommand($sql)->execute();
			} catch (Exception $e) {
				echo ("BA NAK\n");
			}
		}
		return parent::afterSave();
	}

}