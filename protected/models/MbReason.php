<?php

class MbReason extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'mb_reason';
    }

    public function rules()
    {
        return array(
            array('title, color, color1','required'),
        );
    }

}
