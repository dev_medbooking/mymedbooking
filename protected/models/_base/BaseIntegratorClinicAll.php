<?php

/**
 * This is the model base class for the table "integrator_clinic_all".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "IntegratorClinicAll".
 *
 * Columns in table "integrator_clinic_all" available as properties of the model,
 * followed by relations of table "integrator_clinic_all" available as properties of the model.
 *
 * @property integer $id
 * @property string $title
 * @property string $name
 * @property integer $uid
 * @property string $create_time
 * @property string $title_company
 * @property string $comment
 * @property string $address
 * @property string $numb_agree
 * @property string $agreement
 * @property integer $duplicate
 * @property integer $graduation_all
 * @property integer $expensive_services_agreement
 * @property string $email
 * @property integer $mail_template
 *
 * @property IntegratorLawGraduation[] $integratorLawGraduations
 * @property IntegratorLawReports[] $integratorLawReports
 * @method IntegratorClinicAll[] findAll($condition='',$params=array())
 * @method IntegratorClinicAll[] findAllByPk($pk,$condition='',$params=array())
 * @method IntegratorClinicAll[] findAllByAttributes($attributes,$condition='',$params=array())
 * @method IntegratorClinicAll[] findAllBySql($sql,$params=array())
 * @method IntegratorClinicAll find($condition='',$params=array())
 * @method IntegratorClinicAll findByAttributes($attributes,$condition='',$params=array())
 * @method IntegratorClinicAll findBySql($sql,$params=array())
 */
abstract class BaseIntegratorClinicAll extends GxActiveRecord
{
	public $messageNotFound = 'Object not found';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return CDbCriteria
     */
    public static function getDefaultCriteria()
    {
        return new CDbCriteria();
    }

    /**
     * @inheritdoc
     * @return null|BaseIntegratorClinicAll
     */
    public function findByPk($pk, $condition='', $params=array(), $raise = false)
    {
        if ($pk instanceof BaseIntegratorClinicAll){
            $model = $pk;
        }else{
			$model = parent::findByPk($pk, $condition, $params);
        }
		if (!$model && $raise){
			throw new CHttpException(404, $this->messageNotFound);
		}else{
			return $model;
		}
    }

	public function tableName()
	{
		return 'integrator_clinic_all';
	}

	public static function label($n = 1)
	{
		return Yii::t('app', 'IntegratorClinicAll|IntegratorClinicAlls', $n);
	}

	public static function representingColumn()
	{
		return 'title';
	}

	public function rules()
	{
		return array(
			array('title, uid, create_time, numb_agree, agreement', 'required'),
			array('uid, duplicate, graduation_all, expensive_services_agreement, mail_template', 'numerical', 'integerOnly'=>true),
			array('title, title_company, address, numb_agree, email', 'length', 'max'=>255),
			array('agreement', 'length', 'max'=>100),
			array('name, comment', 'safe'),
			array('name, title_company, comment, address, duplicate, graduation_all, expensive_services_agreement, email, mail_template', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, title, name, uid, create_time, title_company, comment, address, numb_agree, agreement, duplicate, graduation_all, expensive_services_agreement, email, mail_template', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'integratorLawGraduations' => array(self::HAS_MANY, 'IntegratorLawGraduation', 'law_id'),
			'integratorLawReports' => array(self::HAS_MANY, 'IntegratorLawReports', 'law_id'),
		);
	}

	public function pivotModels()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'name' => Yii::t('app', 'Name'),
			'uid' => Yii::t('app', 'Uid'),
			'create_time' => Yii::t('app', 'Create Time'),
			'title_company' => Yii::t('app', 'Title Company'),
			'comment' => Yii::t('app', 'Comment'),
			'address' => Yii::t('app', 'Address'),
			'numb_agree' => Yii::t('app', 'Numb Agree'),
			'agreement' => Yii::t('app', 'Agreement'),
			'duplicate' => Yii::t('app', 'Duplicate'),
			'graduation_all' => Yii::t('app', 'Graduation All'),
			'expensive_services_agreement' => Yii::t('app', 'Expensive Services Agreement'),
			'email' => Yii::t('app', 'Email'),
			'mail_template' => Yii::t('app', 'Mail Template'),
			'integratorLawGraduations' => null,
			'integratorLawReports' => null,
		);
	}

    public function getBaseSearchCriteria()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.uid', $this->uid);
        $criteria->compare('t.create_time', $this->create_time, true);
        $criteria->compare('t.title_company', $this->title_company, true);
        $criteria->compare('t.comment', $this->comment, true);
        $criteria->compare('t.address', $this->address, true);
        $criteria->compare('t.numb_agree', $this->numb_agree, true);
        $criteria->compare('t.agreement', $this->agreement, true);
        $criteria->compare('t.duplicate', $this->duplicate);
        $criteria->compare('t.graduation_all', $this->graduation_all);
        $criteria->compare('t.expensive_services_agreement', $this->expensive_services_agreement);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.mail_template', $this->mail_template);
        return $criteria;
    }

	public function search()
	{
        $criteria = $this->getBaseSearchCriteria();
		return new CActiveDataProvider($this, ['criteria' => $criteria]);
	}
}