<?php

/**
 * This is the model base class for the table "partner_site".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PartnerSite".
 *
 * Columns in table "partner_site" available as properties of the model,
 * followed by relations of table "partner_site" available as properties of the model.
 *
 * @property integer $id
 * @property string $host
 * @property integer $ident_id
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PartnerSiteUrl[] $partnerSiteUrls
 * @method PartnerSite[] findAll($condition='',$params=array())
 * @method PartnerSite[] findAllByPk($pk,$condition='',$params=array())
 * @method PartnerSite[] findAllByAttributes($attributes,$condition='',$params=array())
 * @method PartnerSite[] findAllBySql($sql,$params=array())
 * @method PartnerSite find($condition='',$params=array())
 * @method PartnerSite findByAttributes($attributes,$condition='',$params=array())
 * @method PartnerSite findBySql($sql,$params=array())
 */
abstract class BasePartnerSite extends GxActiveRecord
{
	public $messageNotFound = 'Object not found';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return CDbCriteria
     */
    public static function getDefaultCriteria()
    {
        return new CDbCriteria();
    }

    /**
     * @inheritdoc
     * @return null|BasePartnerSite
     */
    public function findByPk($pk, $condition='', $params=array(), $raise = false)
    {
        if ($pk instanceof BasePartnerSite){
            $model = $pk;
        }else{
			$model = parent::findByPk($pk, $condition, $params);
        }
		if (!$model && $raise){
			throw new CHttpException(404, $this->messageNotFound);
		}else{
			return $model;
		}
    }

	public function tableName()
	{
		return 'partner_site';
	}

	public static function label($n = 1)
	{
		return Yii::t('app', 'PartnerSite|PartnerSites', $n);
	}

	public static function representingColumn()
	{
		return 'host';
	}

	public function rules()
	{
		return array(
			array('ident_id', 'numerical', 'integerOnly'=>true),
			array('host', 'length', 'max'=>255),
			array('comment, created_at, updated_at', 'safe'),
			array('host, ident_id, comment, created_at, updated_at', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, host, ident_id, comment, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'partnerSiteUrls' => array(self::HAS_MANY, 'PartnerSiteUrl', 'site_id'),
		);
	}

	public function pivotModels()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'ID'),
			'host' => Yii::t('app', 'Host'),
			'ident_id' => Yii::t('app', 'Ident'),
			'comment' => Yii::t('app', 'Comment'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'partnerSiteUrls' => null,
		);
	}

    public function getBaseSearchCriteria()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.host', $this->host, true);
        $criteria->compare('t.ident_id', $this->ident_id);
        $criteria->compare('t.comment', $this->comment, true);
        $criteria->compare('t.created_at', $this->created_at, true);
        $criteria->compare('t.updated_at', $this->updated_at, true);
        return $criteria;
    }

	public function search()
	{
        $criteria = $this->getBaseSearchCriteria();
		return new CActiveDataProvider($this, ['criteria' => $criteria]);
	}
}