<?php

/**
 * This is the model base class for the table "integrator_law_reports".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "IntegratorLawReports".
 *
 * Columns in table "integrator_law_reports" available as properties of the model,
 * followed by relations of table "integrator_law_reports" available as properties of the model.
 *
 * @property integer $id
 * @property integer $law_id
 * @property string $create_date
 * @property string $report_date
 * @property double $summ
 * @property integer $patient_count
 * @property integer $patient_success
 * @property integer $patient_record
 * @property integer $status
 * @property integer $patient_diag
 * @property double $summ_diag
 * @property integer $patient_analyz
 * @property double $summ_analyz
 * @property integer $patient_repeat
 * @property integer $patient_insurance
 * @property string $comment
 *
 * @property IntegratorClinicReports[] $integratorClinicReports
 * @property IntegratorClinicAll $law
 * @property IntegratorLawReportsPayments[] $integratorLawReportsPayments
 * @property IntegratorLawReportsStatus[] $integratorLawReportsStatuses
 * @method IntegratorLawReports[] findAll($condition='',$params=array())
 * @method IntegratorLawReports[] findAllByPk($pk,$condition='',$params=array())
 * @method IntegratorLawReports[] findAllByAttributes($attributes,$condition='',$params=array())
 * @method IntegratorLawReports[] findAllBySql($sql,$params=array())
 * @method IntegratorLawReports find($condition='',$params=array())
 * @method IntegratorLawReports findByAttributes($attributes,$condition='',$params=array())
 * @method IntegratorLawReports findBySql($sql,$params=array())
 */
abstract class BaseIntegratorLawReports extends GxActiveRecord
{
	public $messageNotFound = 'Object not found';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return CDbCriteria
     */
    public static function getDefaultCriteria()
    {
        return new CDbCriteria();
    }

    /**
     * @inheritdoc
     * @return null|BaseIntegratorLawReports
     */
    public function findByPk($pk, $condition='', $params=array(), $raise = false)
    {
        if ($pk instanceof BaseIntegratorLawReports){
            $model = $pk;
        }else{
			$model = parent::findByPk($pk, $condition, $params);
        }
		if (!$model && $raise){
			throw new CHttpException(404, $this->messageNotFound);
		}else{
			return $model;
		}
    }

	public function tableName()
	{
		return 'integrator_law_reports';
	}

	public static function label($n = 1)
	{
		return Yii::t('app', 'IntegratorLawReports|IntegratorLawReports', $n);
	}

	public static function representingColumn()
	{
		return 'comment';
	}

	public function rules()
	{
		return array(
			array('comment', 'required'),
			array('law_id, patient_count, patient_success, patient_record, status, patient_diag, patient_analyz, patient_repeat, patient_insurance', 'numerical', 'integerOnly'=>true),
			array('summ, summ_diag, summ_analyz', 'numerical'),
			array('create_date, report_date', 'safe'),
			array('law_id, create_date, report_date, summ, patient_count, patient_success, patient_record, status, patient_diag, summ_diag, patient_analyz, summ_analyz, patient_repeat, patient_insurance', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, law_id, create_date, report_date, summ, patient_count, patient_success, patient_record, status, patient_diag, summ_diag, patient_analyz, summ_analyz, patient_repeat, patient_insurance, comment', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'integratorClinicReports' => array(self::HAS_MANY, 'IntegratorClinicReports', 'law_report_id'),
			'law' => array(self::BELONGS_TO, 'IntegratorClinicAll', 'law_id'),
			'integratorLawReportsPayments' => array(self::HAS_MANY, 'IntegratorLawReportsPayments', 'report_id'),
			'integratorLawReportsStatuses' => array(self::HAS_MANY, 'IntegratorLawReportsStatus', 'report_id'),
		);
	}

	public function pivotModels()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'ID'),
			'law_id' => null,
			'create_date' => Yii::t('app', 'Create Date'),
			'report_date' => Yii::t('app', 'Report Date'),
			'summ' => Yii::t('app', 'Summ'),
			'patient_count' => Yii::t('app', 'Patient Count'),
			'patient_success' => Yii::t('app', 'Patient Success'),
			'patient_record' => Yii::t('app', 'Patient Record'),
			'status' => Yii::t('app', 'Status'),
			'patient_diag' => Yii::t('app', 'Patient Diag'),
			'summ_diag' => Yii::t('app', 'Summ Diag'),
			'patient_analyz' => Yii::t('app', 'Patient Analyz'),
			'summ_analyz' => Yii::t('app', 'Summ Analyz'),
			'patient_repeat' => Yii::t('app', 'Patient Repeat'),
			'patient_insurance' => Yii::t('app', 'Patient Insurance'),
			'comment' => Yii::t('app', 'Comment'),
			'integratorClinicReports' => null,
			'law' => null,
			'integratorLawReportsPayments' => null,
			'integratorLawReportsStatuses' => null,
		);
	}

    public function getBaseSearchCriteria()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.law_id', $this->law_id);
        $criteria->compare('t.create_date', $this->create_date, true);
        $criteria->compare('t.report_date', $this->report_date, true);
        $criteria->compare('t.summ', $this->summ);
        $criteria->compare('t.patient_count', $this->patient_count);
        $criteria->compare('t.patient_success', $this->patient_success);
        $criteria->compare('t.patient_record', $this->patient_record);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.patient_diag', $this->patient_diag);
        $criteria->compare('t.summ_diag', $this->summ_diag);
        $criteria->compare('t.patient_analyz', $this->patient_analyz);
        $criteria->compare('t.summ_analyz', $this->summ_analyz);
        $criteria->compare('t.patient_repeat', $this->patient_repeat);
        $criteria->compare('t.patient_insurance', $this->patient_insurance);
        $criteria->compare('t.comment', $this->comment, true);
        return $criteria;
    }

	public function search()
	{
        $criteria = $this->getBaseSearchCriteria();
		return new CActiveDataProvider($this, ['criteria' => $criteria]);
	}
}