<?php

/**
 * This is the model class for table "operator_duty".
 *
 * The followings are the available columns in table 'operator_duty':
 * @property string $id
 * @property integer $extention
 * @property string $date_start
 * @property string $date_end
 * @property string $queue_name
 */
class OperatorDuty extends CActiveRecord
{
	public $_date_end;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OperatorDuty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'operator_duty';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('extention, date_start, _date_end, queue_name', 'required'),
			//array('date_start, date_end', 'date', 'format' => 'yyyy-dd-mm'),
			array('extention', 'numerical', 'integerOnly'=>true),
			array('queue_name', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, extention, date_start, date_end, queue_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'extention' => 'Оператор',
			'date_start' => 'Дата начала',
			'date_end' => 'Дата окончания',
			'queue_name' => 'Очередь',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('extention',$this->extention);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('queue_name',$this->queue_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 20,
			),
			'sort' => array(
				'defaultOrder' => 'date_start DESC',
			),
		));
	}

	public function beforeSave() {
		/*if (isset($_POST['OperatorDuty']['date_start']) && strtotime($_POST['OperatorDuty']['date_start']))
			$_POST['OperatorDuty']['date_start'].=" 00:00:00";
		else
			$_POST['OperatorDuty']['date_start']=date("Y-m-d 00:00:00");
		*/
		if (isset($_POST['OperatorDuty']['_date_end']) && strtotime($_POST['OperatorDuty']['_date_end']))
			$this->date_end=$_POST['OperatorDuty']['_date_end']." 23:59:59";
		else
			$this->date_end=date("Y-m-d 23:59:59");
		return parent::beforeSave();
	}
}