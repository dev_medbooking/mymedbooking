<?php

class MbSmsSession extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_sms_session';
    }

    public function rules()
    {
        return array(
            array('sid','required'),
            array('sid','length','max'=>5),
            array('sid','numerical','integerOnly'=>true),
        );
    }

}