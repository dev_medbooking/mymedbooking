<?php

class MbRecord2 extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_record';
    }

    public function rules()
    {
        return array(
            array('telephone','required'),
            array('telephone','length','max'=>12,'min'=>10),
            array('email','email'),
            array('name, email, domain','length','max'=>128),
            array('gender, new_pacient, fromcall, rerecord, fromsite, house','length','max'=>1),
            array('oktell_id, hour_send_status, day_send_status, rerecord, oldstatus, id_call','numerical','integerOnly'=>true),
            array('attention_value, doctor_value, price_value','numerical','integerOnly'=>true),
            array('review, status_time, reception_time, note, reason, comment, lead_time, update_time, create_time','safe'),
            array('status, new_pacient, category_id, uid, first_uid, author_id, account_author_id, account_first_id, clinic_id, doctor_id, timeslot_id, deleted, house, flag_id','numerical','integerOnly'=>true),
            array('book_id, source, description, clinic_name, doctor_name, category_name','length','max'=>255),
            array('oktell_id','exist','attributeName'=>'id','className'=>'AdminInvoice'),
        );
    }

    public function relations()
    {
        return array(
            'o'=>array(self::BELONGS_TO,'AdminInvoice','oktell_id'),
        );
    }

    public function beforeSave()
    {
        if($this->isNewRecord){
            $this->status_time=date("Y-m-d H:i:s");
            $this->reception_time='0000-00-00 00:00:00';
        }
        return parent::beforeSave();
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior'=>array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'createAttribute'=>'create_time',
                'updateAttribute'=>'update_time',
            ),
        );
    }

}
