<?php

class MbRecordAdd extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_record_add';
    }

    public function rules()
    {
        return array(
            array('record_id','required'),
            array('experience, clinic_url, doctor_url, regime_1, regime_2, regime_3, regime_4, regime_5, regime_6, regime_7, regime_8','length','max'=>255),
            array('speciality, position, doctor_img_url, address, subway, clinic_img_url','length','max'=>255),
            array('lat, lng, price','numerical'),
            array('doctor_description','safe'),
            array('record_id','numerical','integerOnly'=>true),
            array('record_id','exist','attributeName'=>'id','className'=>'MbRecord'),
        );
    }

    public function relations()
    {
        return array(
            'r'=>array(self::BELONGS_TO,'MbRecord','record_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'regime_1'=>'Режим 1',
            'regime_2'=>'Режим 2',
            'regime_3'=>'Режим 3',
            'regime_4'=>'Режим 4',
            'regime_5'=>'Режим 5',
            'regime_6'=>'Режим 6',
            'regime_7'=>'Режим 7',
            'regime_8'=>'Режим 8',
            'doctor_description'=>'Описание',
            'record_id'=>'Запись ID',
            'speciality'=>'Специальность',
            'position'=>'Должность',
            'doctor_img_url'=>'URL фото доктора',
            'address'=>'Адрес',
            'subway'=>'Метро',
            'clinic_img_url'=>'URL фото клиники',
            'lat'=>'Широта',
            'lng'=>'Долгота',
            'price'=>'Стоимость',
            'experience'=>'Стаж работы',
            'clinic_url'=>'URL Клиники',
            'doctor_url'=>'URL Доктора',
        );
    }

}