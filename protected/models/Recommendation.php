<?php

Yii::import('application.models._base.BaseRecommendation');

/**
 * @property RecommendationRead recommendationMyRead
 */
class Recommendation extends BaseRecommendation
{
    /**
     * @param string $className
     *
     * @return Recommendation
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function isUnread()
    {
        return !RecommendationRead::isUnread($this->id);
    }

    public function read()
    {
        if ($this->isUnread()) {
            $pv = new RecommendationRead();
            $pv->ident_id = CurrentUser::getId();
            $pv->recommendation_id = $this->id;
            return $pv->save();
        } else {
            return false;
        }
    }

    public function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            if (!$this->created_at) {
                $this->created_at = new CDbExpression('NOW()');
            }
            $this->updated_at = new CDbExpression('NOW()');
        }
        return parent::beforeSave(); // TODO: Change the autogenerated stub
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), []);
    }

//    public function defaultScope()
//    {
//        return array(
//            'with' => array(
//                'recommendationMyRead' => array(
//                    'alias' => $this->getTableAlias(false, false) . 'recommendationMyRead'
//                )
//            ),
//        );
//    }

    /**
     * @inheritdoc
     */
    public function relations()
    {
        return array_merge(
            parent::relations(), [
                'recommendationMyRead' => array(
                    self::HAS_MANY,
                    'RecommendationRead',
                    '',
                    'on'    => 'rmr.ident_id=' . CurrentUser::getId() . ' AND `rmr`.`recommendation_id` = `t`.`id`',
                    //                    'recommendation_id',
                    //                    'condition' => 'rmr.ident_id=' . CurrentUser::getId(),
                    'alias' => 'rmr'
                ),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), []);
    }

    public function getUrl($action)
    {
        $url = '';
        if ($action == "edit") {
            $url = Url::to('/mbTable/recommendationEdit', ["id" => $this->id]);
        } elseif ($action == "delete") {
            $url = Url::to('/mbTable/recommendationDelete', ["id" => $this->id]);
        }
        return Url::to($url);
    }

    public function haveUnread()
    {
        return self::model()->count($this->getUnreadCriteria());
    }

    public function getUnreadCriteria()
    {
        $criteria = self::model()->getDefaultCriteria();
        $criteria->with['recommendationMyRead'] = ["alias" => "rmr"];
        $criteria->addCondition('rmr.recommendation_id IS NULL');
        return $criteria;
    }

    public function search()
    {
        $criteria = $this->getBaseSearchCriteria();
        $sort = new CSort();
        $sort->defaultOrder = ['created_at' => SORT_DESC];
        return new CActiveDataProvider($this, ['criteria' => $criteria, 'sort' => $sort]);
    }

}