<?php

/**
 * This is the model class for table "auth_item_child".
 *
 * The followings are the available columns in table 'auth_item_child':
 * @property string $parent
 * @property string $child
 */
class RoleChild extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return RoleChild the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'auth_item_child';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('parent, child','required'),
            array('parent, child','length','max'=>64),
        );
    }

}