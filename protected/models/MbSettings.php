<?php

class MbSettings extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_settings';
    }

    public function rules()
    {
        return array(
            array('oktell','numerical','integerOnly'=>true),
            array('create_time, update_time','safe'),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior'=>array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'createAttribute'=>'create_time',
                'updateAttribute'=>null,
            ),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'oktell'=>'Количество',
            'create_time'=>'Время создания',
            'update_time'=>'Время редактирования',
        );
    }

}