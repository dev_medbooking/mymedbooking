<?php

Yii::import('application.models._base.BaseAuthItem');

class AuthItem extends BaseAuthItem
{
    /**
     * @var array
     *
     * Статический метод, массив типов авторизации
     */
    public static $itemTypes
        = array(
            0 => 'action',
            2 => 'role'
        );

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param integer $id
     *
     * @return integer|null
     *
     * Получение объекта по ID
     */
    public static function getItemTypeById($id)
    {
        if (isset(self::$itemTypes[$id])) {
            return self::$itemTypes[$id];
        } else {
            return null;
        }
    }

    /**
     * @param $item_name
     *
     * @return array
     *
     * Получение родителей объекта авторизации
     */
    public static function getItemParents($item_name)
    {
        $items = Yii::app()->db->createCommand()->select('*')->from('Auth_ItemChild')->where(
            'child=:child', array(':child' => $item_name)
        )->queryAll();

        $parents = array();

        foreach ($items as $item) {
            $parents[] = $item['parent'];
        }

        return $parents;
    }

    /**
     * @param $item_name
     *
     * @return array
     *
     * Получение дочерних элементов объекта авторизации
     */
    public static function getItemChilds($item_name)
    {
        $items = Yii::app()->db->createCommand()->select('*')->from('Auth_ItemChild')->where(
            'parent=:parent', array(':parent' => $item_name)
        )->queryAll();

        $childs = array();

        foreach ($items as $item) {
            $childs[] = $item['child'];
        }

        return $childs;
    }

    /**
     * Возвращает массив всех ролей в базе
     *
     * @return array
     */
    public static function getAllRoles()
    {
        $roles = array();

        $auth_items = self::model()->findAll('type=:type', array(':type' => 2));
        foreach ($auth_items as $item) {
            $roles[$item->name] = $item->name;
        }

        return $roles;
    }

    /**
     * @param $role
     *
     * @return array
     *
     * Список всех действий
     */
    public static function getAllActions($role)
    {
        /** @var CAuthManager $auth */
        $auth = Yii::app()->authManager;
        $children = array();
        $items = $auth->getItemChildren($role);

        foreach ($items as $item) {
            if ($item->type == 0) {
                $children[] = $item->name;
            }
            $children = array_merge($children, self::getAllActions($item->name));
        }
        return $children;
    }

    /**
     * Возвращает массив разрешенных действий для роли
     *
     * @param $role
     *
     * @return CArrayDataProvider
     */
    public static function getRolesAccess($role)
    {
        /** @var CAuthManager $auth */
        $out = array();
        $auth = Yii::app()->authManager;
        $id = 1;
        $rules = self::getAllActions($role);

        foreach ($auth->getAuthItems(0) as $item) {

            $row = array();
            $row['id'] = $id++;
            $row['action'] = $item->name;
            $row['access'] = false;
            if (in_array($item->name, $rules)) {
                $row['access'] = true;
            }
            $out[] = $row;
        }

        $dataProvider = new CArrayDataProvider($out);
        $dataProvider->setPagination(array('pageSize' => 100));
        return $dataProvider;
    }

    /**
     * Возвращает массив ролей из базы, за исключением ролей по-умолчанию
     *
     * @return array
     */
    public static function getRolesList()
    {
        $default_roles = Yii::app()->components['authManager']->defaultRoles;
        $criteria = new CDbCriteria();
        $criteria->addNotInCondition('name', $default_roles);
        $criteria->addCondition('type=:type');
        $criteria->params[':type'] = 2;
        $all_roles = AuthItem::model()->findAll($criteria);
        $roles = array();
        foreach ($all_roles as $item) {
            $roles[$item->name] = $item->name;
        }
        return $roles;
    }

    public function search()
    {
        $criteria = parent::search()->getCriteria();
        $criteria->distinct = true;
        $sort = new CSort();
        $sort->defaultOrder = 'name';
        return new CActiveDataProvider(
            $this, array(
                'criteria'   => $criteria,
                'pagination' => ['pageSize' => 50],
                'sort'       => $sort
            )
        );
    }

    public function searchAccesses($user)
    {
        $criteria = new CDbCriteria();
        $criteria->mergeWith(parent::search()->getCriteria());
        $criteria->addInCondition('name', RoleManager::getAccesses($user));
        return new CActiveDataProvider(
            $this, array(
                'criteria'   => $criteria,
                'pagination' => ['pageSize' => 50],
                'sort'       => array(
                    'defaultOrder' => 'name',
                ),
            )
        );
    }

    /**
     * @param array $items
     *
     * установка дочерних элментов для объекта
     */
    public function setChildren(array $items)
    {
        Yii::app()->db->createCommand()->delete('Auth_ItemChild', 'parent=:item', array(':item' => $this->name));

        foreach ($items as $item) {
            Yii::app()->db->createCommand()->insert('Auth_ItemChild', array('parent' => $this->name, 'child' => $item));
        }
    }

    /**
     * @param array $items
     *
     * Установка родителей для объекта
     */
    public function setParents(array $items)
    {
        Yii::app()->db->createCommand()->delete('Auth_ItemChild', 'child=:item', array(':item' => $this->name));

        foreach ($items as $item) {
            Yii::app()->db->createCommand()->insert('Auth_ItemChild', array('child' => $this->name, 'parent' => $item));
        }
    }
}