<?php

class AdminImport extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
	    return 'error';
    }

    public $file;
    public $success = false;

    public function afterSave() {
		//$this->checkReports();
		$this->updateOrders();
		//$this->googleSearch();
    }

    public function attributeLabels() {
        return array(
            'file' => 'xls File',
        );
    }

    public function rules() {
        return array(
            array('file', 'file',
                'types' => 'xls, csv, xlsx',
                'allowEmpty' => true,
                'wrongType' => Yii::t('app', 'Допустимы только файлы следующих форматов: {extensions}.'),
                'maxSize' => 1024 * 1024 * 50,
                'tooLarge' => Yii::t('app', 'Указан файл объемом более 10Мб. Пожалуйста, укажите файл меньшего размера.'),
                'maxFiles' => 1,
            ),
        );
    }

	public function checkReports()
	{
		$fileimage = CUploadedFile::getInstance($this, 'file');
		if (!empty($fileimage)) {
			$filename = 'price.'.$fileimage->getExtensionName();
			$folder = YiiBase::getPathOfAlias('webroot.files.price');
			if ( ! is_dir($folder))
				mkdir($folder, 0755, true);
			if ($fileimage->saveAs('files/price/'.$filename)) {
				$inputFileName = 'files/price/price.'.$fileimage->getExtensionName();
				$objReader = PHPExcel_IOFactory::load($inputFileName);
				$objWorksheet = $objReader->setActiveSheetIndex(0);
				$highestRow = $objWorksheet->getHighestRow();
				$array = array();
				$clinics = array();
				for ($row = 1; $row <= $highestRow; ++ $row) {
					if (empty($clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()])) {
						// $clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['patient_record'] = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
						$clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['patient_success'] = $objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
						$clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['summ'] = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
						$clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['patient_diag'] = $objWorksheet->getCellByColumnAndRow(8, $row)->getValue();
						$clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['summ_diag'] = $objWorksheet->getCellByColumnAndRow(9, $row)->getValue();
					} else {
						// $clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['patient_record'] += $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
						$clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['patient_success'] += $objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
						$clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['summ'] = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
						$clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['patient_diag'] += $objWorksheet->getCellByColumnAndRow(8, $row)->getValue();
						$clinics[$objWorksheet->getCellByColumnAndRow(1, $row)->getValue()]['summ_diag'] += $objWorksheet->getCellByColumnAndRow(9, $row)->getValue();
					}
				}
				foreach($clinics as $key => $value) {
					$model = IntegratorLaws::model()->findByAttributes(array('title' => $key));
					if ( ! empty($model)) {
						$modelIntegratorLawReports = IntegratorLawReports::model()->findByAttributes(array('law_id' => $model->primaryKey, 'report_date' => '2015-09-01'));
						if ( ! empty($modelIntegratorLawReports)) {
							// $array['patient_record'] = $modelIntegratorLawReports->patient_record - $value['patient_record'];
							$array['patient_success'] = $modelIntegratorLawReports->patient_success - $value['patient_success'];
							$array['summ'] = $modelIntegratorLawReports->summ - $value['summ'];
							$array['patient_diag'] = $modelIntegratorLawReports->patient_diag - $value['patient_diag'];
							$array['summ_diag'] = $modelIntegratorLawReports->summ_diag - $value['summ_diag'];
							foreach ($array as $k => $v) {
								if ($v != 0 AND $k != 'not_find')
									echo $model->title.": ".$k." - ".$v."<br>";
							}
						}
					} else {
						$array['not_find'][] = $key;
					}
				}
				echo "<pre>";
				print_r($array['not_find']);
			}
			die;
		}
	}

	public function updateOrders()
	{
		set_time_limit(0);
		$fileimage = CUploadedFile::getInstance($this, 'file');
		if (!empty($fileimage)) {
			$filename = 'price.'.$fileimage->getExtensionName();
			$folder = YiiBase::getPathOfAlias('webroot.files.price');
			if ( ! is_dir($folder))
				mkdir($folder, 0755, true);
			if ($fileimage->saveAs('files/price/'.$filename)) {
				$inputFileName = 'files/price/'.$filename;
				$csvFileName = 'files/price/price.csv';
				exec ("/usr/bin/xls2csv -s cp1251 -d cp1251 -c \; -q 0 ".$inputFileName." > ".$csvFileName);

				$handle = @fopen($csvFileName, "r");
				if ($handle) {
					while (($buffer = fgets($handle)) !== false) {

						$string_array=explode(";",$buffer);
						if (empty($string_array[1]) || !is_numeric($string_array[1]))
							continue;
						$id = trim($string_array[1]);
						$id = ((strlen($id)) >= 10 ? substr($id, 3, strlen($id) - 4) : $id);
						$record = MbRecord::model()->findByPk($id);
						$summ=(is_numeric($string_array[12])? $string_array[12] : 0);

						$visit = iconv('cp1251','utf-8',trim($string_array[11]));
						echo "<br>" . $id . " - " . $visit . " - " . $summ . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						if (!empty($record)) {
							$record->summ_approved = $summ;
							switch (mb_strtolower($visit, 'UTF-8')) {
								case "был":
									$status = 3;
									$ins = 0;
									$repeat = 0;
									break;
								case "не был":
									$status = 2;
									$ins = 0;
									$repeat = 0;
									break;
								case "страховой":
									$status = 3;
									$ins = 1;
									$repeat = 0;
									break;
								case "вторичный":
									$status = 3;
									$ins = 0;
									$repeat = 1;
									break;
								case '90 дней':
									$ins=0;
									$repeat=0;
									$newRecord = MbRecord::model()->find('note LIKE "%' . $record->id . '%"');

									$status=MbRecord::STATUS_NINETY_DAYS;
									$record->status=$status;

									if($summ>0){
										$status=MbRecord::STATUS_LEAD;;
										$record->status=$status;
									}

									if (empty($newRecord)){
										$newRecord = clone($record);
										unset($newRecord['id']);
										$newRecord['note'] = 'Исходная заявка ' . $record->id . ' от ' . $record->create_time;
										$newRecord['reception_time'] = date('Y-m-d H:i', strtotime($newRecord->reception_time . '+90 days'));
										$newRecord['status'] = MbRecord::STATUS_NINETY_DAYS;

										Yii::app()->db->createCommand()->insert(MbRecord::model()->tableName(),$newRecord->getAttributes());
										$record['note'].=' Добавлена запись '.Yii::app()->db->lastInsertID;

									}
									break;
								default:
									$ins = 0;
									$repeat = 0;
									$status = 0;
									break;
							}
							if ($status == 2 OR $status == 3  or $status==MbRecord::STATUS_NINETY_DAYS) {
								$record->_history = new MbRecordHistory();
								$record->_history->old = serialize($record->attributes);
								if (in_array($record->status, array(101, 102, 103, 104)) AND $status == 2) $record->status = 103;
								else if (in_array($record->status, array(101, 102, 103, 104)) AND $status == 3) $record->status = 104;
								else $record->status = $status;
								if (in_array($status, array(3, 104)) AND !in_array($record->status, array(3, 104))) $record->lead_time = date('d-m-Y H:i');
								if ($ins == 1) {
									$record->insurance = 1;
								}
								if ($repeat == 1) {
									$record->koll_repeat = 1;
								}
								echo "обновлено" . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
								$record->save();

							}
						}
						else {
							echo $id." - запись не найдена";
						}
					}
					fclose($handle);
				}
			}
			die;
		}
	}

	public function googleSearch()
	{
		set_time_limit(0);
		$fileimage = CUploadedFile::getInstance($this, 'file');
		if (!empty($fileimage)) {
			$filename = 'price.'.$fileimage->getExtensionName();
			$folder = YiiBase::getPathOfAlias('webroot.files.price');
			if ( ! is_dir($folder))
				mkdir($folder, 0755, true);
			if ($fileimage->saveAs('files/price/'.$filename)) {
				$inputFileName = 'files/price/price.'.$fileimage->getExtensionName();
				$objReader = PHPExcel_IOFactory::load($inputFileName);
				$objWorksheet = $objReader->setActiveSheetIndex(0);
				$highestRow = $objWorksheet->getHighestRow();
				echo "<pre>";
//				for ($row = 1; $row <= $highestRow; ++ $row) {
				for ($row = 1; $row <= 60; ++ $row) {
					$title = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
					//$title = '"Астери-Мед", медицинский центр для всей семьи, вызов педиатра на дом, вакцинация на Владимирской, Москва';
					if (!empty($title)) {
						$url = 'https://ajax.googleapis.com/ajax/services/search/web?v=1.0&q='.urlencode($title).'%20medbooking';
						$ch = curl_init();
						curl_setopt ($ch, CURLOPT_URL, $url);
						//curl_setopt ($ch, CURLOPT_HEADER,1);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 40);
						curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
						curl_setopt ($ch, CURLOPT_REFERER, "http://medbooking.com/");
						curl_setopt ($ch, CURLOPT_VERBOSE,1);
						$c = curl_exec($ch);
						curl_close($ch);
						//$c = file_get_contents($url);
						$c = CJSON::decode($c);
						//print_r($c);
						if (!empty($c['responseData']['results'])) {
							foreach ($c['responseData']['results'] as $r) {
								if (strpos($r['url'], 'medbooking.com/clinic')) echo $title . ' - ' . $r['url'] . " - 1<br>";
							}
						}
						sleep(5);
					}

				}
			}
			die;
		}
	}

}
