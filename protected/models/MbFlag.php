<?php

class MbFlag extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_flag';
    }

    public function rules()
    {
        return array(
            array('title, color','required'),
        );
    }

    public function relations()
    {
        return array(
            'flag_s'=>array(self::HAS_MANY,'MbRecord','flag_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'title'=>'Название',
            'color'=>'Цвет',
        );
    }

}