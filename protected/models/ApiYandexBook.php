<?php

Yii::import('application.models._base.BaseApiYandexBook');

class ApiYandexBook extends BaseApiYandexBook
{
	/**
	 * @param string $className
	 *
	 * @return ApiYandexBook
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return array_merge(parent::rules(), []);
	}

	/**
	 * @inheritdoc
	 */
	public function relations()
	{
		return array_merge(parent::relations(), []);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return array_merge(parent::attributeLabels(), []);
	}
}