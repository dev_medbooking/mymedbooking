<?php

/**
 * This is the model class for table "queue_pause".
 *
 * The followings are the available columns in table 'queue_pause':
 * @property string $id
 * @property string $uid
 * @property integer $action
 * @property string $create_time
 * @property string $reason
 * @property string $ext
 */
class QueuePause extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return QueuePause the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'queue_pause';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, action, create_time, reason, ext', 'required'),
			array('action', 'numerical', 'integerOnly'=>true),
			array('uid, create_time, ext', 'length', 'max'=>11),
			array('reason', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, action, create_time, reason, ext', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'action' => 'PAUSE',
			'create_time' => 'Дата',
			'reason' => 'Причена',
			'ext' => 'Добавочный',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('action',$this->action);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('ext',$this->ext,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}