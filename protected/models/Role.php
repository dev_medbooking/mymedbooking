<?php

/**
 * This is the model class for table "auth_item".
 *
 * The followings are the available columns in table 'auth_item':
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $bizrule
 * @property string $data
 */
class Role extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Role the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'auth_item';
    }

    public function primaryKey()
    {
        return 'name';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, type','required'),
            array('type','numerical','integerOnly'=>true),
            array('name','length','max'=>64),
            array('description, bizrule, data','safe'),
        );
    }

}