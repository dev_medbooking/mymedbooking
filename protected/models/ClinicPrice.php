<?php

class ClinicPrice extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		return array(
			array('name, type','required'),
		);
	}

	public function tableName()
	{
		return 'clinic_price';
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'MbUser', 'user_id'),
		);
	}

	public function getPriceType()
	{
		switch($this->type) {
			case 1:
				return "Общая";
			case 2:
				return "Диагностика";
			case 3:
				return "Анализы";
			case 4:
				return "Стоматология";
			default:
				return "n / a";
		}
	}

	public function getAvailableTypes()
	{
		$return = array(
			"1" => "Общая",
			"2" => "Диагностика",
			"3" => "Анализы",
			"4" => "Стоматология",
		);
		return $return;
 	}
}