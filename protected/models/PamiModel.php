<?php
set_time_limit(0);
if (Yii::app() instanceof CConsoleApplication) {
    $path = new SplFileInfo($_SERVER['SCRIPT_NAME']);
    $vendor = $path->getPath() . '/vendor/autoload.php';
}
else
    $vendor = 'protected/vendor/autoload.php';

require_once($vendor);

use PAMI\Client\Impl\ClientImpl as PamiClient;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Action\QueueStatusAction;
use PAMI\Message\Action\QueuePauseAction;
use PAMI\Message\Action\QueueUnpauseAction;
use PAMI\Message\Action\QueueLogAction;
//use PAMI\Listener\IEventListener;
use PAMI\Message\Action\StatusAction;
use PAMI\Message\Action\OriginateAction;
use PAMI\Message\Action\QueueRemoveAction;
use PAMI\Message\Action\QueueAddAction;
use PAMI\Message\Action\DBGetAction;
use PAMI\Message\Action\DBPutAction;
use PAMI\Message\Action\QueueSummaryAction;


class PamiModel extends CActiveRecord
{

    public $channel;
    public $ext;
    public $action;
    public $uid;
    public $reason;
    public $button_value;
    public $pamiClient;

    public $searchPhone;
    public $operator;
    public $dateTimeFrom;
    public $dateTimeTo;

    private $prefix = "SIP/";
    private $originate_params = ",qE";
    public $queue_name = "medbooking";
    public $queue_name_secondary = array("autodial","test");
    //public $queue_name = "q501";

    public $queue_name_array = array ("medbooking","autoservice","testpuls","ulyanovsk","autodial","test");
    private $asterisk_table = "cdr2";
    private $asterisk_table_prev = "cdr";


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'queue_pause';
    }

    public function rules()
    {
        return array(
            array('uid,action,create_time,reason,ext', 'required'),
            array('uid,action,create_time', 'numerical', 'integerOnly' => true),
            array('reason', 'length', 'max' => 124),
            array('uid,action,create_time,reason,ext', 'safe'),
        );
    }

    private $preg_array=array(
        "channel" => "Channel: SIP\/",
        //asterisk 13
        /*"caller_id_num" => "CallerIDNum: ",
        "dnid" => "DNID: ",
        "connectedlinenum" => "ConnectedLineNum: "*/
        //asterisk 11
        "caller_id_num" => "CallerIDNum: ",
        "connectedlinenum" => "ConnectedLineNum: ",
        "bridgedchannel" => "BridgedChannel: SIP\/"
    );

    public function __construct(){
        try {
            $this->pamiClient = new PamiClient(Yii::app()->params['asterisk_options']);
            $this->pamiClient->open();

            //для консоли
            if (Yii::app() instanceof CConsoleApplication) {
                $this->uid = 0;
                $this->ext = 0;
            }
            else {
                $this->ext = $this->operatorExt();
                $this->uid = Yii::app()->user->getId();
            }
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    public function __destruct(){
        $this->pamiClient->close();
    }

    public function close () {
        $this->pamiClient->close();
    }

    private function getProjectPhones() {
        $project_phones_array_mod=array();
        $project_phones_array = MbDomainPhone::model()->findAll('');
        $skip_symbols = array("+", "(", ")", "-");
        foreach ($project_phones_array as $k => $v) {
            $ph = str_replace($skip_symbols, '', $v->phone);
            //$project_phones_array_mod[] = (strlen($ph)==11?substr($ph,1):$ph);
            $project_phones_array_mod[] = $ph;
        }
        return $project_phones_array_mod;
    }

    public function status () {
        $folder = YiiBase::getPathOfAlias('application.components');
        if (file_exists($folder."/getprojectphones.php"))
            include($folder."/getprojectphones.php");
        else
            $project_phones_array_mod = $this->getProjectPhones();
        array_push($project_phones_array_mod,"74956401485");

        $data=array();

        $events=$this->pamiClient->send(new StatusAction())->getEvents();
        foreach ($events as $k=>$v)
        {
            if ($k<count($events)-1)
            {
                $break=0;
                $content=$v->getrawContent();
//                echo "<pre>"; print_r($content); echo "</pre>";
                foreach ($this->preg_array as $k1=>$v1) {
                    preg_match("/^".$v1.".+$/im",$content, $matches);
                    //echo "<pre>MATCH=".$matches[0]." STLEN=".strlen($matches[0])."</pre><br>";
                    if (!empty($matches[0])) {
                        $data[$k][$k1] = trim(preg_replace("/(" . $v1 . ")|(\r)|(\n)|(\t)|(\r\n)/im", '', $matches[0]));
                        if ($data[$k][$k1]=="<unknown>")
                            $data[$k][$k1]="";

                        if ($k1 == 'channel') {
                            preg_match('/^.+-/', $data[$k][$k1], $op_channel);
                            $data[$k]['operator'] = substr($op_channel[0], 0, -1);
                            $data[$k]['notes'] = "";
                        }
                        if ($k1 == 'bridgedchannel') {
                            preg_match('/^.+-/', $data[$k][$k1], $op_channel);
                            $data[$k]['operator1'] = substr($op_channel[0], 0, -1);
                            $data[$k]['notes1'] = "";
                        }
                    }
                    else
                        $data[$k][$k1]="";
                    //echo "<pre>DATA=".$data[$k][$k1]." STLEN=".strlen($data[$k][$k1])."</pre><br>";
                }
                //asterisk 13
                /*if (!$data[$k]['dnid'] && (!$data[$k]['dnid'] || !$data[$k]['connectedlinenum']))
                    array_pop($data);
                else {
                    asterisk 13
                    if (!in_array($data[$k]['caller_id_num'], $project_phones_array_mod) && !in_array($data[$k]['connectedlinenum'], $project_phones_array_mod) && !in_array($data[$k]['dnid'], $project_phones_array_mod) && strlen($data[$k]['caller_id_num']) >= 10) {
                        $data[$k]['notes'] = "Звонок в клинику";
                    }
                }*/

                //прослушивание звонка. в интерфейс не выводим
                if (!isset($data[$k]['caller_id_num']) && !isset($data[$k]['connectedlinenum'])) {
                    array_pop($data);
                    $break=1;
                }

                if (!$break) {
                    if (!in_array($data[$k]['caller_id_num'], $project_phones_array_mod) && !in_array($data[$k]['connectedlinenum'], $project_phones_array_mod) && strlen($data[$k]['caller_id_num']) >= 10) {
                        $data[$k]['notes'] = "Звонок в клинику";
                    }
                }
//                echo "<pre>"; print_r($data[$k]); echo "</pre><hr>";
            }
        }
        //добавляем номер ConnectedLineName
        foreach ($data as $k=>$v) {
            if (empty($v['connectedlinenum'])) {
                $bridgedchannel=$v['bridgedchannel'];
                foreach ($data as $k1=>$v1) {
                    if (empty($v1['connectedlinenum']))
                        continue;
                    else {
                        //переворачиваем направление
                        if ($bridgedchannel==$v1['channel']) {
                            //echo $bridgedchannel."<br>";
                            $data[$k]['connectedlinenum']=(!empty($v1['caller_id_num'])?$v1['caller_id_num']:$v1['connectedlinenum']);
                            $data[$k]['caller_id_num']=(!empty($v1['connectedlinenum'])?$v1['connectedlinenum']:$v1['caller_id_num']);
                            break;
                        }
                    }
                }
            }
            else
                continue;
            if (!in_array($data[$k]['caller_id_num'], $project_phones_array_mod) && !in_array($data[$k]['connectedlinenum'], $project_phones_array_mod) && strlen($data[$k]['caller_id_num']) >= 10) {
                $data[$k]['notes'] = "Звонок в клинику";
            }
        }
//        echo "<pre>"; print_r($data); echo "</pre>";
        return $data;
    }

    public function queuePause () {
        try {
            /* ставим паузу в очередях */
            $response = $this->pamiClient->send(new QueuePauseAction($this->prefix.$this->ext));
            /* ставим паузу глобально */
            $global_pause_response = $this->setPauseGlobal(true);
            if (null === $global_pause_response) {
                throw new \Exception("Error setting global pause");
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    public function queueUnpause () {
        try {
            /* ставим паузу в очередях */
            $response = $this->pamiClient->send(new QueueUnpauseAction($this->prefix.$this->ext));
            /* снимаем паузу глобально */
            $global_pause_response = $this->setPauseGlobal(false);
            if (null === $global_pause_response) {
                throw new \Exception("Error unsetting global pause");
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    public function operatorExt() {
        $sql="select extention from api_operator_extention where auth_user_id=".Yii::app()->user->getId();
        $ext = Yii::app()->db->createCommand($sql)->queryScalar();
        return $ext;
    }

    public function statusOperator() {
        $sql="select action from ".$this->tableName()." where uid=".Yii::app()->user->getId()." order by id desc limit 1";
        $action = Yii::app()->db->createCommand($sql)->queryScalar();
        if (!$action)
            $action=2;
        return $action;
    }

    public function originate($channel)
    {
        $originateMsg = new OriginateAction($this->prefix.$this->ext);
        $originateMsg->setApplication("ChanSpy");
        $originateMsg->setData($this->prefix.$channel.$this->originate_params);
        $originateMsg->setCodecs(array('alaw'));
        $originateMsg->setCallerId("Прослушивание ".$this->ext);
        return $this->pamiClient->send($originateMsg);
    }

    public function getFtpList($dateStart,$phone=false) {
        $scanDir="/".$dateStart;
        $ret_array=array();
        try
        {
            $conn_id = ftp_connect(Yii::app()->params['asterisk_ftp_options']['server'],Yii::app()->params['asterisk_ftp_options']['port'],Yii::app()->params['asterisk_ftp_options']['timeout']);

            if (ftp_login($conn_id, Yii::app()->params['asterisk_ftp_options']['user'], Yii::app()->params['asterisk_ftp_options']['pass'])) {
                ftp_pasv($conn_id, true);
                $ftp_file_list = ftp_nlist($conn_id, $scanDir);
                if (is_array($ftp_file_list)) {
                    array_shift($ftp_file_list);
                    array_shift($ftp_file_list);

                    $i=0;
                    foreach ($ftp_file_list as $v){
                        $arr=explode("/",$v);
                        if ( (empty($phone) || (!empty($phone) && strpos($arr[4],$phone)>0)) ) {
                            $ret_array[$i]['print']=$arr[4];
                            $ret_array[$i]['name']=$v;
                            $i++;
                        }
                    }
                }
            }
            ftp_close($conn_id);
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
        return $ret_array;
    }

    public function getFtpListDb($model) {
        $folder = YiiBase::getPathOfAlias('application.components');
        if (file_exists($folder."/getprojectphones.php"))
            include($folder."/getprojectphones.php");
        else
            $project_phones_array_mod = $this->getProjectPhones();

        $dateTimeFrom=str_replace("/","-",$model->dateTimeFrom);
        $dateTimeTo=str_replace("/","-",$model->dateTimeTo);
        $sql_select="select DATE_FORMAT(start, '/%Y/%m/%d/') as date,DATE_FORMAT(start, '%d.%m.%Y') as dateprint,src,dst,DATE_FORMAT(start, '%H:%i:%s') as time,duration,billsec,SUBSTRING_INDEX(userfield,';',-1) as userfield,channel,dstchannel from ";
        $sql_first=$this->asterisk_table;
        $sql_second=$this->asterisk_table_prev;
        $sql_where =" force key(start) where userfield!='' AND duration>0 AND start > '".$dateTimeFrom."' AND start < '".$dateTimeTo."'" ;
        if ($model->searchPhone)
            $sql_where.=" AND (src LIKE '%".trim($model->searchPhone)."%' OR dst like '%".trim($model->searchPhone)."%')";
        if ($model->operator)
            $sql_where.=" AND (channel like '".$this->prefix.trim($model->operator)."-%' OR dstchannel like '".$this->prefix.trim($model->operator)."-%')";
        $sql_order = " order by start desc";
        $sql = $sql_select.$sql_first.$sql_where.$sql_order;
        //echo $sql."<br>";
        $ret_array = Yii::app()->db_ast->createCommand($sql)->queryAll();
        $i=0;
        if (!isset($ret_array[0])) {
            $sql = $sql_select.$sql_second.$sql_where.$sql_order;
            $ret_array = Yii::app()->db_ast->createCommand($sql)->queryAll();
        }
        foreach ($ret_array as $k=>$v){
            if (in_array($v['dst'], $project_phones_array_mod)) {
                $ret_array[$k]['pamiDomain']=$v['dst'];
                $ret_array[$k]['pamiTelephone']=$v['src'];
            }
            $pos=strpos($v['channel'],"-");
            $pref_strlen = strlen($this->prefix);
            if ($pos)
                $ret_array[$k]['pamiExt']=substr($v['channel'],$pref_strlen,$pos-$pref_strlen);
            if (!is_numeric($ret_array[$k]['pamiExt'])) {
                $pos=strpos($v['dstchannel'],"-");
                $ret_array[$k]['pamiExt']=substr($v['dstchannel'],$pref_strlen,$pos-$pref_strlen);
            }

            $i++;
        }
        return $ret_array;
    }

    public function queueAdd($ext,$login,$pause=0,$queue_name=false,$penalty=0) {
        if (!$queue_name) {
            $queue_name = $this->queue_name;
        }

        try {
            $addMsg = new QueueAddAction($queue_name,$this->prefix.$ext);
            $addMsg -> setPaused($pause);
            $addMsg -> setMemberName($login);
            $addMsg -> setPenalty($penalty);
            $response = $this->pamiClient->send($addMsg);
            $paused = $this->hasQueuePause();
            if (null === $paused) {
                throw new \Exception("Error detecting ext queues pause status");
            }
            $global_pause_response = $this->setPauseGlobal($paused);
            if (null === $global_pause_response) {
                throw new \Exception("Error setting global pause");
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    public function queueRemove($ext,$queue_name=false) {
        if (!$queue_name) {
            $queue_name = $this->queue_name;
        }
        try {
            $response = $this->pamiClient->send(new QueueRemoveAction($queue_name,$this->prefix.$ext));
            $paused = $this->hasQueuePause();
            if (null === $paused) {
                throw new \Exception("Error detecting ext queues pause status");
            }
            $global_pause_response = $this->setPauseGlobal($paused);
            if (null === $global_pause_response) {
                throw new \Exception("Error setting global pause");
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    public function getPauseGlobal()
    {
        try {
            $response = $this->pamiClient->send(new DBGetAction('Q_STAT', $this->ext));
            $on = (bool)($response->getEvents()[0]->getValue());
            return $on;
        } catch (\Exception $e) {
            echo $e->getMessage() . "\n";
            return null;
        }
    }

    public function setPauseGlobal($on)
    {
        $on = $on ? 1 : 0;
        try {
            /* ставим/снимаем паузу глобально */
            return $this->pamiClient->send(new DBPutAction('Q_STAT', $this->ext, $on));
        } catch (\Exception $e) {
            echo $e->getMessage() . "\n";
            return null;
        }
    }

    /**
     * проверяем, есть ли очереди, где номер (ext) в паузе
     *
     * @return null|bool
     */
    public function hasQueuePause()
    {
        $queue_list = $this->getExtQueueStatus();
        if (null === $queue_list) {
            return null;
        }
        $paused = false;
        foreach ($queue_list as $queue) {
            if ($queue['paused']) {
                $paused = true;
                break;
            }
        }
        return $paused;
    }

    /**
     * получаем статус номера (ext) в очередях
     *
     * @param string|bool $queue
     * @return array|null
     */
    public function getExtQueueStatus($queue = false)
    {
        try {
            $response = $this->pamiClient->send(new QueueStatusAction($queue, $this->prefix . $this->ext));
            $queue_list = [];
            foreach ($response->getEvents() as $event) {
                if ('QueueMember' == $event->getKey('event')) {
                    $queue_list[$event->getKey('queue')] = $event->getKeys();
                    unset($queue_list[$event->getKey('queue')]['event']);
                    unset($queue_list[$event->getKey('queue')]['actionid']);
                }
            }
            return $queue_list;
        } catch (\Exception $e) {
            echo $e->getMessage() . "\n";
            return null;
        }
    }

}
