<?php

class IntegratorLawReportsStatus extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'integrator_law_reports_status';
	}

	public function relations()
	{
		return array(
			'law_report_status' => array(self::BELONGS_TO, 'IntegratorLawReports', 'report_id'),
		);
	}

	public function rules()
	{
		return array(
			array('report_date, summ','safe'),
		);
	}

	/**
	 * Возвращает текстовый статус
	 * @param $status
	 * @return string
	 */
	public function getReportStatusText($status = 0)
	{
		if (isset(IntegratorLawReports::$statusText[$status]))
			return IntegratorLawReports::$statusText[$status];
		else return '';
	}
}
