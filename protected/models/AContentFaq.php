<?php

/**
 * Created by PhpStorm.
 * User: serg
 * Date: 03.02.14
 * Time: 12:20
 */
class AContentFaq extends AContentModel
{

    protected $entityType='faq';
    public $id;
    public $title;
    public $status;
    public $author;
    public $cat_title;
    public $is_desc;
    public $is_det_desc;
    public $is_mtitle;
    public $is_mdesc;
    public $is_mkeys;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('id,title,status,author,cat_title,is_desc,is_det_desc,is_mtitle,is_mdesc,is_mkeys','safe'),
            array('id,title,status,author,cat_title,is_desc,is_det_desc,is_mtitle,is_mdesc,is_mkeys','safe','on'=>'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'title'=>'Наименование',
            'status'=>'Статус',
        );
    }

    public function search($domain)
    {
        return $this->getDomainContent($domain);
    }

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array('id','title','status','author','cat_title','is_desc','is_det_desc','is_mtitle','is_mdesc','is_mkeys');
    }

}