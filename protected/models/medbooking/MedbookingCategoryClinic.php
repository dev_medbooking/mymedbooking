<?php

/**
 * This is the model class for table "category_clinic".
 *
 * The followings are the available columns in table 'category_clinic':
 * @property integer $id
 * @property integer $category_id
 * @property integer $clinic_id
 * @property integer $parent_id
 * @property integer $action_id
 * @property string $name
 * @property double $price
 * @property double $rate5
 * @property double $modificator
 *
 * The followings are the available model relations:
 * @property Clinic $clinic
 * @property MedbookingCategoryClinic $parent
 * @property MedbookingCategoryClinic[] $categoryClinics
 * @property Category $category
 */
class MedbookingCategoryClinic extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingCategoryClinic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category_clinic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, clinic_id, action_id', 'required'),
			array('category_id, clinic_id, parent_id, action_id', 'numerical', 'integerOnly'=>true),
			array('price, rate5, modificator', 'numerical'),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category_id, clinic_id, parent_id, action_id, name, price, rate5, modificator', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clinic' => array(self::BELONGS_TO, 'MedbookingClinic', 'clinic_id'),
			'parent' => array(self::BELONGS_TO, 'MedbookingCategoryClinic', 'parent_id'),
			'categoryClinics' => array(self::HAS_MANY, 'MedbookingCategoryClinic', 'parent_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'catc_category'=>array(self::BELONGS_TO,'MedbookingCategory','category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'clinic_id' => 'Clinic',
			'parent_id' => 'Parent',
			'action_id' => 'Action',
			'name' => 'Name',
			'price' => 'Price',
			'rate5' => 'Rate5',
			'modificator' => 'Modificator',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('clinic_id',$this->clinic_id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('action_id',$this->action_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('rate5',$this->rate5);
		$criteria->compare('modificator',$this->modificator);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}