<?php

/**
 * This is the model class for table "doctor_add".
 *
 * The followings are the available columns in table 'doctor_add':
 * @property integer $id
 * @property integer $nid
 * @property string $services
 *
 * The followings are the available model relations:
 * @property MedbookingDoctor $n
 */
class MedbookingDoctorAdd extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingDoctorAdd the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'doctor_add';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nid', 'required'),
			array('nid', 'numerical', 'integerOnly'=>true),
			array('services', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nid, services', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'n' => array(self::BELONGS_TO, 'MedbookingDoctor', 'nid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nid' => 'Nid',
			'services' => 'Services',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nid',$this->nid);
		$criteria->compare('services',$this->services,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}