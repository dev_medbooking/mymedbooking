<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property integer $root
 * @property string $name
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property string $translit
 * @property string $name_clinic
 * @property string $name_spec
 * @property string $name_service
 * @property integer $home
 * @property integer $home_spb
 * @property string $child
 * @property integer $popular
 * @property integer $nation
 * @property integer $main
 * @property integer $link
 * @property integer $link2
 *
 * The followings are the available model relations:
 * @property CategoryClinic[] $categoryClinics
 * @property MedbookingCategoryDoctor[] $categoryDoctors
 * @property ClasterSpeciality[] $clasterSpecialities
 * @property NodeIllness[] $nodeIllnesses
 * @property MedbookingNodeService[] $nodeServices
 * @property NodeServicePrice[] $nodeServicePrices
 * @property Price[] $prices
 */
class MedbookingCategory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, lft, rgt, level, name_service, home_spb, child, nation, main, link, link2', 'required'),
			array('root, lft, rgt, level, home, home_spb, popular, nation, main, link, link2', 'numerical', 'integerOnly'=>true),
			array('name, translit, name_clinic, name_spec, name_service', 'length', 'max'=>255),
			array('child', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, root, name, lft, rgt, level, translit, name_clinic, name_spec, name_service, home, home_spb, child, popular, nation, main, link, link2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categoryClinics' => array(self::HAS_MANY, 'MedbookingCategoryClinic', 'category_id'),
			'categoryDoctors' => array(self::HAS_MANY, 'MedbookingCategoryDoctor', 'category_id'),
			'clasterSpecialities' => array(self::HAS_MANY, 'ClasterSpeciality', 't_id'),
			'nodeIllnesses' => array(self::HAS_MANY, 'NodeIllness', 'category_id'),
			'nd' => array(self::HAS_MANY, 'MedbookingNodeService', 'category_id'),
            'cds' => array(self::HAS_MANY, 'MedbookingCategoryDiagnosticService', 'category_id'),
			'nodeServicePrices' => array(self::HAS_MANY, 'NodeServicePrice', 'category_id'),
			'prices' => array(self::HAS_MANY, 'MedbookingPrice', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'root' => 'Root',
			'name' => 'Name',
			'lft' => 'Lft',
			'rgt' => 'Rgt',
			'level' => 'Level',
			'translit' => 'Translit',
			'name_clinic' => 'Name Clinic',
			'name_spec' => 'Name Spec',
			'name_service' => 'Name Service',
			'home' => 'Home',
			'home_spb' => 'Home Spb',
			'child' => 'Child',
			'popular' => 'Popular',
			'nation' => 'Nation',
			'main' => 'Main',
			'link' => 'Link',
			'link2' => 'Link2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('root',$this->root);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('lft',$this->lft);
		$criteria->compare('rgt',$this->rgt);
		$criteria->compare('level',$this->level);
		$criteria->compare('translit',$this->translit,true);
		$criteria->compare('name_clinic',$this->name_clinic,true);
		$criteria->compare('name_spec',$this->name_spec,true);
		$criteria->compare('name_service',$this->name_service,true);
		$criteria->compare('home',$this->home);
		$criteria->compare('home_spb',$this->home_spb);
		$criteria->compare('child',$this->child,true);
		$criteria->compare('popular',$this->popular);
		$criteria->compare('nation',$this->nation);
		$criteria->compare('main',$this->main);
		$criteria->compare('link',$this->link);
		$criteria->compare('link2',$this->link2);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}