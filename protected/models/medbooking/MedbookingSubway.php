<?php

/**
 * This is the model class for table "subway".
 *
 * The followings are the available columns in table 'subway':
 * @property integer $id
 * @property string $title
 * @property string $translit
 * @property double $lat
 * @property double $lng
 * @property integer $subway_line_id
 * @property integer $district_id
 * @property integer $city
 *
 * The followings are the available model relations:
 * @property ChildSubwayClinic[] $childSubwayClinics
 * @property MedbookingSubwayLine $subwayLine
 * @property MedbookingDistrict $district
 * @property MedbookingSubwayClinic[] $subwayClinics
 * @property SubwayClinicDiagnostic[] $subwayClinicDiagnostics
 */
class MedbookingSubway extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingSubway the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'subway';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, subway_line_id', 'required'),
			array('subway_line_id, district_id, city', 'numerical', 'integerOnly'=>true),
			array('lat, lng', 'numerical'),
			array('title, translit', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, translit, lat, lng, subway_line_id, district_id, city', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'childSubwayClinics' => array(self::HAS_MANY, 'ChildSubwayClinic', 'subway_id'),
			'subwayLine' => array(self::BELONGS_TO, 'MedbookingSubwayLine', 'subway_line_id'),
			'district' => array(self::BELONGS_TO, 'MedbookingDistrict', 'district_id'),
			'subwayClinics' => array(self::HAS_MANY, 'MedbookingSubwayClinic', 'subway_id'),
			'subwayClinicDiagnostics' => array(self::HAS_MANY, 'SubwayClinicDiagnostic', 'subway_id'),
            'cityObject' => array(self::BELONGS_TO, 'MedbookingCity', 'city'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'translit' => 'Translit',
			'lat' => 'Lat',
			'lng' => 'Lng',
			'subway_line_id' => 'Subway Line',
			'district_id' => 'District',
			'city' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('translit',$this->translit,true);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lng',$this->lng);
		$criteria->compare('subway_line_id',$this->subway_line_id);
		$criteria->compare('district_id',$this->district_id);
		$criteria->compare('city',$this->city);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}