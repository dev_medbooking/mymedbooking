<?php

/**
 * This is the model class for table "doctor".
 *
 * The followings are the available columns in table 'doctor':
 * @property integer $id
 * @property integer $status
 * @property integer $external_id
 * @property string $translit
 * @property string $fname
 * @property string $lname
 * @property string $sname
 * @property string $description
 * @property string $speciality
 * @property string $position
 * @property integer $startyear
 * @property integer $gender
 * @property string $degree
 * @property string $telephone
 * @property string $email
 * @property string $professional
 * @property string $associated
 * @property string $education
 * @property string $awards
 * @property string $meta_title
 * @property integer $doctor_city_id
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $update_time
 * @property string $create_time
 * @property string $image
 * @property integer $uid
 * @property integer $view_status
 * @property string $price
 * @property double $start_rate
 * @property string $sames
 * @property integer $house
 * @property integer $child
 * @property double $modificator
 * @property double $rate10
 * @property double $rate_new
 * @property integer $rate_show
 * @property integer $comm
 * @property integer $recommend
 * @property string $dreviews
 * @property string $dservices
 * @property string $drecords
 * @property string $ddayrecords
 * @property string $price_title
 * @property integer $nation
 * @property string $note
 *
 * The followings are the available model relations:
 * @property MedbookingCategoryDoctor[] $categoryDoctors
 * @property MedbookingClinicDoctor[] $clinicDoctors
 * @property MedbookingUser $u
 * @property MedbookingDoctorAdd[] $doctorAdds
 * @property DoctorRating2[] $doctorRating2s
 * @property NodeIllnessDoctor[] $nodeIllnessDoctors
 * @property NodeServiceDoctor[] $nodeServiceDoctors
 * @property Price[] $prices
 * @property MedbookingReviews[] $reviews
 */
class MedbookingDoctor extends CActiveRecord
{
    public $_fio;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingDoctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'doctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fname, lname, create_time, rate_new, rate_show, note', 'required'),
			array('status, external_id, startyear, gender, doctor_city_id, uid, view_status, house, child, rate_show, comm, recommend, nation', 'numerical', 'integerOnly'=>true),
			array('start_rate, modificator, rate10, rate_new', 'numerical'),
			array('translit, fname, lname, sname, speciality, position, degree, telephone, email, meta_title, meta_keywords, image, price, sames, dreviews, dservices, drecords, ddayrecords, price_title, note', 'length', 'max'=>255),
			array('description, professional, associated, education, awards, meta_description, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, status, external_id, translit, fname, lname, sname, description, speciality, position, startyear, gender, degree, telephone, email, professional, associated, education, awards, meta_title, doctor_city_id, meta_keywords, meta_description, update_time, create_time, image, uid, view_status, price, start_rate, sames, house, child, modificator, rate10, rate_new, rate_show, comm, recommend, dreviews, dservices, drecords, ddayrecords, price_title, nation, note', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categoryDoctors' => array(self::HAS_MANY, 'MedbookingCategoryDoctor', 'doctor_id'),
            'doctor_category_s' => array(self::HAS_MANY, 'MedbookingCategoryDoctor', 'doctor_id'),
			'clinicDoctors' => array(self::HAS_MANY, 'MedbookingClinicDoctor', 'doctor_id'),
			'user' => array(self::BELONGS_TO, 'MedbookingUser', 'uid'),
			'doctorAdds' => array(self::HAS_MANY, 'MedbookingDoctorAdd', 'nid'),
			'doctorRating2s' => array(self::HAS_MANY, 'DoctorRating2', 'nid'),
			'nodeIllnessDoctors' => array(self::HAS_MANY, 'NodeIllnessDoctor', 'doctor_id'),
			'nodeServiceDoctors' => array(self::HAS_MANY, 'NodeServiceDoctor', 'doctor_id'),
			'doctor_price_s' => array(self::HAS_MANY, 'MedbookingPrice', 'doctor_id'),
			'reviews' => array(self::HAS_MANY, 'MedbookingReviews', 'doctor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Status',
			'external_id' => 'External',
			'translit' => 'Translit',
			'fname' => 'Fname',
			'lname' => 'Lname',
			'sname' => 'Sname',
			'description' => 'Description',
			'speciality' => 'Speciality',
			'position' => 'Position',
			'startyear' => 'Startyear',
			'gender' => 'Gender',
			'degree' => 'Degree',
			'telephone' => 'Telephone',
			'email' => 'Email',
			'professional' => 'Professional',
			'associated' => 'Associated',
			'education' => 'Education',
			'awards' => 'Awards',
			'meta_title' => 'Meta Title',
			'doctor_city_id' => 'Doctor City',
			'meta_keywords' => 'Meta Keywords',
			'meta_description' => 'Meta Description',
			'update_time' => 'Update Time',
			'create_time' => 'Create Time',
			'image' => 'Image',
			'uid' => 'Uid',
			'view_status' => 'View Status',
			'price' => 'Price',
			'start_rate' => 'Start Rate',
			'sames' => 'Sames',
			'house' => 'House',
			'child' => 'Child',
			'modificator' => 'Modificator',
			'rate10' => 'Rate10',
			'rate_new' => 'Rate New',
			'rate_show' => 'Rate Show',
			'comm' => 'Comm',
			'recommend' => 'Recommend',
			'dreviews' => 'Dreviews',
			'dservices' => 'Dservices',
			'drecords' => 'Drecords',
			'ddayrecords' => 'Ddayrecords',
			'price_title' => 'Price Title',
			'nation' => 'Nation',
			'note' => 'Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('status',$this->status);
		$criteria->compare('external_id',$this->external_id);
		$criteria->compare('translit',$this->translit,true);
		$criteria->compare('fname',$this->fname,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('sname',$this->sname,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('speciality',$this->speciality,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('startyear',$this->startyear);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('degree',$this->degree,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('professional',$this->professional,true);
		$criteria->compare('associated',$this->associated,true);
		$criteria->compare('education',$this->education,true);
		$criteria->compare('awards',$this->awards,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('doctor_city_id',$this->doctor_city_id);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('view_status',$this->view_status);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('start_rate',$this->start_rate);
		$criteria->compare('sames',$this->sames,true);
		$criteria->compare('house',$this->house);
		$criteria->compare('child',$this->child);
		$criteria->compare('modificator',$this->modificator);
		$criteria->compare('rate10',$this->rate10);
		$criteria->compare('rate_new',$this->rate_new);
		$criteria->compare('rate_show',$this->rate_show);
		$criteria->compare('comm',$this->comm);
		$criteria->compare('recommend',$this->recommend);
		$criteria->compare('dreviews',$this->dreviews,true);
		$criteria->compare('dservices',$this->dservices,true);
		$criteria->compare('drecords',$this->drecords,true);
		$criteria->compare('ddayrecords',$this->ddayrecords,true);
		$criteria->compare('price_title',$this->price_title,true);
		$criteria->compare('nation',$this->nation);
		$criteria->compare('note',$this->note,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function getFio()
    {
        if(empty($this->_fio)){
            $this->_fio='';
            if(!empty($this->lname)){
                $this->_fio.=!empty($this->_fio)?" ".trim($this->lname):trim($this->lname);
            }
            if(!empty($this->fname)){
                $this->_fio.=!empty($this->_fio)?" ".trim($this->fname):trim($this->fname);
            }
            if(!empty($this->sname)){
                $this->_fio.=!empty($this->_fio)?" ".trim($this->sname):trim($this->sname);
            }
        }
        return $this->_fio;
    }
    public function getClinicsName()
    {
        $array=array();
        if(!empty($this->id)){
//            $data=MedbookingClinicDoctor::model()->findAllByAttributes(array('doctor_id'=>$this->id));
            $data = $this->clinicDoctors;
            if(!empty($data)){
                foreach($data as $value){
                    $array[$value['clinic']['id']]=$value['clinic']['title'];
                }
            }
        }
        return $array;
    }
	public function getReviews()
	{
		$array=array();
		if(!empty($this->id)){
			$criteria= New CDbCriteria;
			$criteria->condition = 't.doctor_id=:doctor_id AND t.status=1';
			$criteria->order = 't.create_time desc';
			$criteria->select = 'id, name, attention_value, doctor_value, price_value, description';
			$criteria->params = array(':doctor_id'=>$this->id);
            $data=MedbookingReviews::model()->findAll($criteria);
//			$data = $this->reviews;
			if(!empty($data)){
				foreach($data as $value){
					$array[]=$value;
				}
			}
		}
		return $array;
	}
	public function getClinicsSubways()
	{
		$array=array();$array2=array();
		if(!empty($this->id)){
//            $data=MedbookingClinicDoctor::model()->findAllByAttributes(array('doctor_id'=>$this->id));
			$data = $this->clinicDoctors;
			if(!empty($data)){
				foreach($data as $value){
					$sub = unserialize($value['clinic']['subways_data']);
					if(!empty($sub))
						foreach($sub as $s) {
							$array[$s['translit']] = $s['title'];
							$array2[]=$s;
						}
				}
			}
		}
		sort($array);
		return [$array,$array2];
	}
}