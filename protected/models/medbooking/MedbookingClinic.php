<?php

/**
 * This is the model class for table "clinic".
 *
 * The followings are the available columns in table 'clinic':
 * @property integer $id
 * @property integer $status
 * @property string $title
 * @property string $alias
 * @property integer $external_id
 * @property integer $gallery_id
 * @property string $translit
 * @property string $description
 * @property string $address
 * @property string $email
 * @property string $telephone
 * @property string $inn
 * @property string $url
 * @property double $lat
 * @property double $lng
 * @property integer $uid
 * @property string $regime_sun
 * @property string $regime_mon
 * @property string $regime_tue
 * @property string $regime_wed
 * @property string $regime_thu
 * @property string $regime_fri
 * @property string $regime_sat
 * @property string $regime_byd
 * @property string $body
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $companyname
 * @property string $create_time
 * @property string $update_time
 * @property string $image
 * @property integer $view_status
 * @property string $district
 * @property double $start_rate
 * @property string $sames
 * @property integer $network_id
 * @property integer $district_id
 * @property integer $relevant
 * @property integer $none_status
 * @property double $modificator
 * @property integer $clinic_city_line_id
 * @property integer $clinic_city_id
 * @property integer $district_line_id
 * @property double $rate10
 * @property integer $same_rate
 * @property integer $recommend
 * @property integer $rate_show
 * @property integer $comm
 * @property string $subways_data
 * @property string $price_title
 * @property string $note
 * @property integer $nation
 * @property integer $callcenter_fio
 * @property integer $diagnostic_id
 *
 * The followings are the available model relations:
 * @property CategoryClinic[] $categoryClinics
 * @property MedbookingUser $u
 * @property MedbookingClinic $network
 * @property MedbookingClinic[] $clinics
 * @property MedbookingDistrict $district0
 * @property DistrictLine $districtLine
 * @property ClinicAction[] $clinicActions
 * @property ClinicAdd[] $clinicAdds
 * @property MedbookingClinicDoctor[] $clinicDoctors
 * @property ClinicRating2[] $clinicRating2s
 * @property NodeServicePrice[] $nodeServicePrices
 * @property Price[] $prices
 * @property MedbookingReviews[] $reviews
 * @property MedbookingSubwayClinic[] $subwayClinics
 */
class MedbookingClinic extends CActiveRecord
{
    public $_specialistAll;
    public $_subway_name;
    public $_detAll;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MedbookingClinic the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return CDbConnection database connection
     */
    public function getDbConnection()
    {
        return Yii::app()->db2;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'clinic';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, create_time, clinic_city_id, rate_show, diagnostic_id', 'required'),
            array(
                'status, external_id, gallery_id, uid, view_status, network_id, district_id, relevant, none_status, clinic_city_line_id, clinic_city_id, district_line_id, recommend, rate_show, comm, nation, callcenter_fio, diagnostic_id',
                'numerical',
                'integerOnly' => true
            ),
            array('lat, lng, start_rate, modificator, rate10', 'numerical'),
            array(
                'title, alias, translit, address, email, telephone, inn, url, regime_byd, meta_title, meta_keywords, companyname, image, district, sames, price_title, note',
                'length',
                'max' => 255
            ),
            array(
                'regime_sun, regime_mon, regime_tue, regime_wed, regime_thu, regime_fri, regime_sat',
                'length',
                'max' => 50
            ),
            array('description, body, meta_description, update_time, subways_data, note, same_rate', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, status, title, alias, external_id, gallery_id, translit, description, address, email, telephone, inn, url, lat, lng, uid, regime_sun, regime_mon, regime_tue, regime_wed, regime_thu, regime_fri, regime_sat, regime_byd, body, meta_title, meta_keywords, meta_description, companyname, create_time, update_time, image, view_status, district, start_rate, sames, network_id, district_id, relevant, none_status, modificator, clinic_city_line_id, clinic_city_id, district_line_id, rate10, same_rate, recommend, rate_show, comm, subways_data, price_title, note, nation, callcenter_fio, diagnostic_id',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'categoryClinics' => array(self::HAS_MANY, 'MedbookingCategoryClinic', 'clinic_id'),
            'user' => array(self::BELONGS_TO, 'MedbookingUser', 'uid'),
            'network' => array(self::BELONGS_TO, 'MedbookingClinic', 'network_id'),
            'clinics' => array(self::HAS_MANY, 'MedbookingClinic', 'network_id'),
            'district0' => array(self::BELONGS_TO, 'MedbookingDistrict', 'district_id'),
            'districtLine' => array(self::BELONGS_TO, 'MedbookingDistrictLine', 'district_line_id'),
            'clinicActions' => array(self::HAS_MANY, 'ClinicAction', 'clinic_id'),
            'clinicAdds' => array(self::HAS_MANY, 'ClinicAdd', 'nid'),
            'clinicDoctors' => array(self::HAS_MANY, 'MedbookingClinicDoctor', 'clinic_id'),
            'clinicRating2s' => array(self::HAS_MANY, 'ClinicRating2', 'nid'),
            'nodeServicePrices' => array(self::HAS_MANY, 'NodeServicePrice', 'clinic_id'),
            'prices' => array(self::HAS_MANY, 'Price', 'clinic_id'),
            'reviews' => array(self::HAS_MANY, 'MedbookingReviews', 'clinic_id'),
            'subwayClinics' => array(self::HAS_MANY, 'MedbookingSubwayClinic', 'clinic_id'),
            'clinic_subway_s' => array(self::HAS_MANY, 'MedbookingSubwayClinic', 'clinic_id'),
            'clinic_doctor_sa' => array(
                self::HAS_MANY,
                'MedbookingClinicDoctor',
                'clinic_id',
                'with' => array('doctor')
            ),
            'clinic_doctor_single' => array(
                self::HAS_MANY,
                'MedbookingClinicDoctor',
                'clinic_id',
                'group' => 'doctor_id',
                'with' => array('doctor'),
                'order' => 'doctor.rate10 DESC'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'status' => 'Status',
            'title' => 'Title',
            'alias' => 'Alias',
            'external_id' => 'External',
            'gallery_id' => 'Gallery',
            'translit' => 'Translit',
            'description' => 'Description',
            'address' => 'Address',
            'email' => 'Email',
            'telephone' => 'Telephone',
            'inn' => 'Inn',
            'url' => 'Url',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'uid' => 'Uid',
            'regime_sun' => 'Regime Sun',
            'regime_mon' => 'Regime Mon',
            'regime_tue' => 'Regime Tue',
            'regime_wed' => 'Regime Wed',
            'regime_thu' => 'Regime Thu',
            'regime_fri' => 'Regime Fri',
            'regime_sat' => 'Regime Sat',
            'regime_byd' => 'Regime Byd',
            'body' => 'Body',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'companyname' => 'Companyname',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'image' => 'Image',
            'view_status' => 'View Status',
            'district' => 'District',
            'start_rate' => 'Start Rate',
            'sames' => 'Sames',
            'network_id' => 'Network',
            'district_id' => 'District',
            'relevant' => 'Relevant',
            'none_status' => 'None Status',
            'modificator' => 'Modificator',
            'clinic_city_line_id' => 'Clinic City Line',
            'clinic_city_id' => 'Clinic City',
            'district_line_id' => 'District Line',
            'rate10' => 'Rate10',
            'same_rate' => 'Same Rate',
            'recommend' => 'Recommend',
            'rate_show' => 'Rate Show',
            'comm' => 'Comm',
            'subways_data' => 'Subways Data',
            'price_title' => 'Price Title',
            'note' => 'Note',
            'nation' => 'Nation',
            'callcenter_fio' => 'Callcenter Fio',
            'diagnostic_id' => 'Diagnostic',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('status', $this->status);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('external_id', $this->external_id);
        $criteria->compare('gallery_id', $this->gallery_id);
        $criteria->compare('translit', $this->translit, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('telephone', $this->telephone, true);
        $criteria->compare('inn', $this->inn, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('lat', $this->lat);
        $criteria->compare('lng', $this->lng);
        $criteria->compare('uid', $this->uid);
        $criteria->compare('regime_sun', $this->regime_sun, true);
        $criteria->compare('regime_mon', $this->regime_mon, true);
        $criteria->compare('regime_tue', $this->regime_tue, true);
        $criteria->compare('regime_wed', $this->regime_wed, true);
        $criteria->compare('regime_thu', $this->regime_thu, true);
        $criteria->compare('regime_fri', $this->regime_fri, true);
        $criteria->compare('regime_sat', $this->regime_sat, true);
        $criteria->compare('regime_byd', $this->regime_byd, true);
        $criteria->compare('body', $this->body, true);
        $criteria->compare('meta_title', $this->meta_title, true);
        $criteria->compare('meta_keywords', $this->meta_keywords, true);
        $criteria->compare('meta_description', $this->meta_description, true);
        $criteria->compare('companyname', $this->companyname, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('view_status', $this->view_status);
        $criteria->compare('district', $this->district, true);
        $criteria->compare('start_rate', $this->start_rate);
        $criteria->compare('sames', $this->sames, true);
        $criteria->compare('network_id', $this->network_id);
        $criteria->compare('district_id', $this->district_id);
        $criteria->compare('relevant', $this->relevant);
        $criteria->compare('none_status', $this->none_status);
        $criteria->compare('modificator', $this->modificator);
        $criteria->compare('clinic_city_line_id', $this->clinic_city_line_id);
        $criteria->compare('clinic_city_id', $this->clinic_city_id);
        $criteria->compare('district_line_id', $this->district_line_id);
        $criteria->compare('rate10', $this->rate10);
        $criteria->compare('same_rate', $this->same_rate);
        $criteria->compare('recommend', $this->recommend);
        $criteria->compare('rate_show', $this->rate_show);
        $criteria->compare('comm', $this->comm);
        $criteria->compare('subways_data', $this->subways_data, true);
        $criteria->compare('price_title', $this->price_title, true);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('nation', $this->nation);
        $criteria->compare('callcenter_fio', $this->callcenter_fio);
        $criteria->compare('diagnostic_id', $this->diagnostic_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getRegimeClinic()
    {
        $data = array();
        if (!empty($this->regime_byd)) {
            $data['буд.'] = $this->regime_byd;
        }
        if (!empty($this->regime_mon)) {
            $data['пн.'] = $this->regime_mon;
        }
        if (!empty($this->regime_tue)) {
            $data['вт.'] = $this->regime_tue;
        }
        if (!empty($this->regime_wed)) {
            $data['ср.'] = $this->regime_wed;
        }
        if (!empty($this->regime_thu)) {
            $data['чт.'] = $this->regime_thu;
        }
        if (!empty($this->regime_fri)) {
            $data['пт.'] = $this->regime_fri;
        }
        if (!empty($this->regime_sat)) {
            $data['сб.'] = $this->regime_sat;
        }
        if (!empty($this->regime_sun)) {
            $data['вс.'] = $this->regime_sun;
        }
        return $data;
    }

    public function getSpecialistAll()
    {
        $array = array();
        if (!empty($this->clinic_doctor_single)) {
            foreach ($this->clinic_doctor_single as $value) {
                if (!empty($value->doctor->categoryDoctors)) {
                    foreach ($value->doctor->categoryDoctors as $item) {
                        if (!empty($item->category->name)) {
                            $array[$item->category->id] = $item->category->name;
                        }
                    }
                }
            }
        }
        $this->_specialistAll = implode(", ", $array);
        return $this->_specialistAll;
    }

    public function getCountDoctors()
    {
        return Yii::app()->db2->createCommand("SELECT COUNT(DISTINCT doctor_id) as qty FROM clinic_doctor WHERE clinic_id=" . $this->id)->queryScalar();
    }

    public function getSubway_name()
    {
        $array = array();
        $array2 = array();
        $data = MedbookingSubwayClinic::model()->findAllByAttributes(array('clinic_id' => $this->primaryKey));
        if (!empty($data)) {
            foreach ($data as $value) {
                if (!empty($value['subway']['title'])) {
                    $array[] = $value['subway']['title'];
                    $array2[] = array(
                        'city' => $value['subway']['city'],
                        'district_id' => $value['subway']['district_id'],
                        'id' => $value['subway']['id'],
                        'lat' => $value['subway']['lat'],
                        'lng' => $value['subway']['lng'],
                        'subway_line_id' => $value['subway']['subway_line_id'],
                        'title' => $value['subway']['title'],
                        'translit' => $value['subway']['translit'],
                        'lin1' => $value->lin1,
                        'lin2' => $value->lin2
                    );
                }
            }
        }
        $this->_subway_name = implode(", ", $array);
        return [$this->_subway_name, $array2];
    }

    public function getReviewsCount()
    {
        $criteria = New CDbCriteria;
        $criteria->condition = 't.clinic_id=:clinic_id AND t.status=1';
        $criteria->params = array(':clinic_id' => $this->id);

        return MedbookingReviews::model()->count($criteria);
    }

    public function getReviews($offset = 0, $limit = 10)
    {
        $array = array();
        if (!empty($this->id)) {
            $criteria = New CDbCriteria;
            $criteria->condition = 't.clinic_id=:clinic_id AND t.status=1';
            $criteria->order = 't.create_time desc';
            $criteria->select = 'id, name, attention_value, doctor_value, price_value, description';
            $criteria->limit = $limit;
            $criteria->offset = $offset;
            $criteria->params = array(':clinic_id' => $this->id);
            $data = MedbookingReviews::model()->findAll($criteria);
            if (!empty($data)) {
                foreach ($data as $value) {
                    $array[] = $value->getAttributes([
                        'name',
                        'doctor_value',
                        'attention_value',
                        'price_value',
                        'description'
                    ]);
                }
            }
        }
        return $array;
    }

    public function getDetAll()
    {
        $array = array();
        if (!empty($this->clinic_doctor_single_d)) {
            foreach ($this->clinic_doctor_single_d as $value) {
                if (!empty($value->d->doctor_category_s)) {
                    foreach ($value->d->doctor_category_s as $item) {
                        if (!empty($item->catd_category->name)) {
                            $array[$item->catd_category->id] = $item->catd_category->name;
                        }
                    }
                }
            }
        }
        $this->_detAll = implode(", ", $array);
        return $this->_detAll;
    }

    protected function afterSave()
    {
        if (!empty(Yii::app()->user)) {
            $userId = Yii::app()->user->id;
        }

        $clinicId = $this->id;

        if (!empty($_SERVER['HTTP_REFERER'])) {
            $referrer = $_SERVER['HTTP_REFERER'];
        }

        Yii::log(" User with id $userId, came from $referrer and changed clinic with id $clinicId , rate is $this->rate10 . ",
            CLogger::LEVEL_INFO);

        parent::afterSave(); // TODO: Change the autogenerated stub
    }


}