<?php

/**
 * Class AdminNodeServiceTimeSlot
 * @property int $id
 * @property int $category_id
 * @property int $clinic_id
 * @property int $period
 * @property string $date
 * @property string $begin
 * @property string $end
 */
class MedbookingCategoryClinicTimeSlot extends CActiveRecord
{
    /**
     * @param string $className
     * @return static
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return CDbConnection
     */
    public function getDbConnection()
    {
        return Yii::app()->db2;
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return '{{category_clinic_timeslot}}';
    }

    /**
     * @return bool
     */
    final public function beforeSave()
    {
        return false;
    }

    public function getMinDate($categoryId, $clinicId)
    {
        return $this->findByAttributes([
            'category_id' => $categoryId,
            'clinic_id' => $clinicId
        ], [
            'select' => 'MIN(date) date'
        ])->date;
    }

    public function getMaxDate($categoryId, $clinicId)
    {
        return $this->findByAttributes([
            'category_id' => $categoryId,
            'clinic_id' => $clinicId
        ], [
            'select' => 'MAX(date) date'
        ])->date;
    }
}