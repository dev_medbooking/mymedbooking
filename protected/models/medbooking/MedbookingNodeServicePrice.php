<?php

/**
 * This is the model class for table "node_service_price".
 *
 * The followings are the available columns in table 'node_service_price':
 * @property integer $id
 * @property string $price
 * @property string $prefix
 * @property integer $discount
 * @property integer $clinic_id
 * @property integer $category_id
 * @property integer $action_id
 * @property integer $status
 * @property integer $service_id
 *
 * The followings are the available model relations:
 * @property Clinic $clinic
 * @property Category $category
 * @property NodeService $service
 */
class MedbookingNodeServicePrice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingNodeServicePrice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'node_service_price';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prefix, discount, clinic_id, action_id, service_id', 'required'),
			array('discount, clinic_id, category_id, action_id, status, service_id', 'numerical', 'integerOnly'=>true),
			array('price', 'length', 'max'=>255),
			array('prefix', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, price, prefix, discount, clinic_id, category_id, action_id, status, service_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clinic' => array(self::BELONGS_TO, 'MedbookingClinic', 'clinic_id'),
			'category' => array(self::BELONGS_TO, 'MedbookingCategory', 'category_id'),
			'service' => array(self::BELONGS_TO, 'MedbookingNodeService', 'service_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'price' => 'Price',
			'prefix' => 'Prefix',
			'discount' => 'Discount',
			'clinic_id' => 'Clinic',
			'category_id' => 'Category',
			'action_id' => 'Action',
			'status' => 'Status',
			'service_id' => 'Service',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('prefix',$this->prefix,true);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('clinic_id',$this->clinic_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('action_id',$this->action_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('service_id',$this->service_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}