<?php

/**
 * This is the model class for table "subway_clinic_diagnostic".
 *
 * The followings are the available columns in table 'subway_clinic_diagnostic':
 * @property integer $id
 * @property integer $clinic_id
 * @property integer $subway_id
 * @property string $lin1
 * @property string $lin2
 *
 * The followings are the available model relations:
 * @property ClinicDiagnostic $clinic
 * @property Subway $subway
 */
class MedbookingDiagnosticSubwayClinic extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingDiagnosticSubwayClinic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'subway_clinic_diagnostic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('clinic_id, subway_id', 'required'),
			array('clinic_id, subway_id', 'numerical', 'integerOnly'=>true),
			array('lin1, lin2', 'length', 'max'=>16),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, clinic_id, subway_id, lin1, lin2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clinic' => array(self::BELONGS_TO, 'MedbookingClinicDiagnostic', 'clinic_id'),
			'sub' => array(self::BELONGS_TO, 'MedbookingSubway', 'subway_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'clinic_id' => 'Clinic',
			'subway_id' => 'Subway',
			'lin1' => 'Lin1',
			'lin2' => 'Lin2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('clinic_id',$this->clinic_id);
		$criteria->compare('subway_id',$this->subway_id);
		$criteria->compare('lin1',$this->lin1,true);
		$criteria->compare('lin2',$this->lin2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}