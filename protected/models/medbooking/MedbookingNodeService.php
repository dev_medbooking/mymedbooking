<?php

/**
 * This is the model class for table "node_service".
 *
 * The followings are the available columns in table 'node_service':
 * @property integer $id
 * @property string $title
 * @property string $translit
 * @property string $body
 * @property string $teaser
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $subdescription
 * @property integer $status
 * @property string $image
 * @property integer $category_id
 * @property string $description
 * @property integer $views
 * @property string $note
 *
 * The followings are the available model relations:
 * @property ClasterServices[] $clasterServices
 * @property MedbookingCategory $category
 * @property NodeServiceDoctor[] $nodeServiceDoctors
 * @property NodeServicePrice[] $nodeServicePrices
 * @property SymptomsArticleService[] $symptomsArticleServices
 */
class MedbookingNodeService extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingNodeService the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'node_service';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, translit, teaser, image, views, note', 'required'),
			array('status, category_id, views', 'numerical', 'integerOnly'=>true),
			array('title, translit, meta_title, meta_keywords, subdescription, image, note', 'length', 'max'=>255),
			array('body, meta_description, description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, translit, body, teaser, meta_title, meta_description, meta_keywords, subdescription, status, image, category_id, description, views, note', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clasterServices' => array(self::HAS_MANY, 'ClasterServices', 't_id'),
			'category' => array(self::BELONGS_TO, 'MedbookingCategory', 'category_id'),
			'nodeServiceDoctors' => array(self::HAS_MANY, 'NodeServiceDoctor', 'service_id'),
			'nodeServicePrices' => array(self::HAS_MANY, 'MedbookingNodeServicePrice', 'service_id'),
			'symptomsArticleServices' => array(self::HAS_MANY, 'SymptomsArticleService', 'service_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'translit' => 'Translit',
			'body' => 'Body',
			'teaser' => 'Teaser',
			'meta_title' => 'Meta Title',
			'meta_description' => 'Meta Description',
			'meta_keywords' => 'Meta Keywords',
			'subdescription' => 'Subdescription',
			'status' => 'Status',
			'image' => 'Image',
			'category_id' => 'Category',
			'description' => 'Description',
			'views' => 'Views',
			'note' => 'Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('translit',$this->translit,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('teaser',$this->teaser,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('subdescription',$this->subdescription,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('views',$this->views);
		$criteria->compare('note',$this->note,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}