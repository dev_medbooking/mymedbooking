<?php

/**
 * This is the model class for table "clinic_doctor".
 *
 * The followings are the available columns in table 'clinic_doctor':
 * @property integer $id
 * @property integer $clinic_id
 * @property integer $doctor_id
 * @property string $speciality
 * @property string $position
 *
 * The followings are the available model relations:
 * @property MedbookingClinic $clinic
 * @property MedbookingDoctor $doctor
 * @property Timeslot[] $timeslots
 */
class MedbookingClinicDoctor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingClinicDoctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clinic_doctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('clinic_id, doctor_id', 'required'),
			array('clinic_id, doctor_id', 'numerical', 'integerOnly'=>true),
			array('speciality', 'length', 'max'=>255),
			array('position', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, clinic_id, doctor_id, speciality, position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clinic' => array(self::BELONGS_TO, 'MedbookingClinic', 'clinic_id'),
			'doctor' => array(self::BELONGS_TO, 'MedbookingDoctor', 'doctor_id'),
			'timeslots' => array(self::HAS_MANY, 'Timeslot', 'dcid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'clinic_id' => 'Clinic',
			'doctor_id' => 'Doctor',
			'speciality' => 'Speciality',
			'position' => 'Position',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('clinic_id',$this->clinic_id);
		$criteria->compare('doctor_id',$this->doctor_id);
		$criteria->compare('speciality',$this->speciality,true);
		$criteria->compare('position',$this->position,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}