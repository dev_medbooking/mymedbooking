<?php

/**
 * This is the model class for table "clinic_diagnostic".
 *
 * The followings are the available columns in table 'clinic_diagnostic':
 * @property integer $id
 * @property string $title
 * @property string $translit
 * @property integer $status
 * @property string $description
 * @property string $address
 * @property string $image
 * @property string $create_time
 * @property string $update_time
 * @property integer $startyear
 * @property double $lat
 * @property double $lng
 * @property string $body
 * @property string $companyname
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $regime_byd
 * @property string $regime_mon
 * @property string $regime_tue
 * @property string $regime_wed
 * @property string $regime_thu
 * @property string $regime_fri
 * @property string $regime_sat
 * @property string $regime_sun
 * @property string $telephone
 * @property string $email
 * @property integer $uid
 * @property string $sames
 * @property double $start_rate
 * @property integer $district_id
 * @property integer $none_status
 * @property double $modificator
 * @property integer $clinic_city_line_id
 * @property integer $clinic_city_id
 * @property integer $euro_price
 * @property integer $depth
 *
 * The followings are the available model relations:
 * @property MedbookingDistrict $district
 * @property ClinicDiagnosticRating[] $clinicDiagnosticRatings
 * @property ClinicPrice[] $clinicPrices
 * @property SubwayClinicDiagnostic[] $subwayClinicDiagnostics
 */
class MedbookingDiagnosticClinic extends CActiveRecord
{
    public $_subway_name;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MedbookingDiagnosticClinic the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return CDbConnection database connection
     */
    public function getDbConnection()
    {
        return Yii::app()->db2;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'clinic_diagnostic';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, create_time, companyname, clinic_city_id, euro_price, depth', 'required'),
            array('status, startyear, uid, district_id, none_status, clinic_city_line_id, clinic_city_id, euro_price, depth', 'numerical', 'integerOnly' => true),
            array('lat, lng, start_rate, modificator', 'numerical'),
            array('title, translit, address, image, companyname, meta_title, meta_description, meta_keywords, sames', 'length', 'max' => 255),
            array('regime_byd, regime_mon, regime_tue, regime_wed, regime_thu, regime_fri, regime_sat, regime_sun', 'length', 'max' => 32),
            array('telephone, email', 'length', 'max' => 128),
            array('description, update_time, body', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, translit, status, description, address, image, create_time, update_time, startyear, lat, lng, body, companyname, meta_title, meta_description, meta_keywords, regime_byd, regime_mon, regime_tue, regime_wed, regime_thu, regime_fri, regime_sat, regime_sun, telephone, email, uid, sames, start_rate, district_id, none_status, modificator, clinic_city_line_id, clinic_city_id, euro_price, depth', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'district' => array(self::BELONGS_TO, 'MedbookingDistrict', 'district_id'),
            'clinicDiagnosticRatings' => array(self::HAS_MANY, 'MedbookingClinicDiagnosticRating', 'nid'),
            'clinicPrices' => array(self::HAS_MANY, 'MedbookingClinicPrice', 'clinic_id'),
            'sub' => array(self::HAS_MANY, 'MedbookingSubwayClinicDiagnostic', 'clinic_id'),
            'clinic_subway_s' => array(self::HAS_MANY, 'MedbookingDiagnosticSubwayClinic', 'clinic_id'),
            'clinic_category_s' => array(self::HAS_MANY, 'DiagnosticClinicPrice', 'clinic_id', 'with' => array('catc_category' => array('condition' => ((!empty($_REQUEST['only_mrt'])) ? 'catc_category.vladimirov_status=1' : '')))),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'translit' => 'Translit',
            'status' => 'Status',
            'description' => 'Description',
            'address' => 'Address',
            'image' => 'Image',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'startyear' => 'Startyear',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'body' => 'Body',
            'companyname' => 'Companyname',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'regime_byd' => 'Regime Byd',
            'regime_mon' => 'Regime Mon',
            'regime_tue' => 'Regime Tue',
            'regime_wed' => 'Regime Wed',
            'regime_thu' => 'Regime Thu',
            'regime_fri' => 'Regime Fri',
            'regime_sat' => 'Regime Sat',
            'regime_sun' => 'Regime Sun',
            'telephone' => 'Telephone',
            'email' => 'Email',
            'uid' => 'Uid',
            'sames' => 'Sames',
            'start_rate' => 'Start Rate',
            'district_id' => 'District',
            'none_status' => 'None Status',
            'modificator' => 'Modificator',
            'clinic_city_line_id' => 'Clinic City Line',
            'clinic_city_id' => 'Clinic City',
            'euro_price' => 'Euro Price',
            'depth' => 'Depth',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('translit', $this->translit, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('startyear', $this->startyear);
        $criteria->compare('lat', $this->lat);
        $criteria->compare('lng', $this->lng);
        $criteria->compare('body', $this->body, true);
        $criteria->compare('companyname', $this->companyname, true);
        $criteria->compare('meta_title', $this->meta_title, true);
        $criteria->compare('meta_description', $this->meta_description, true);
        $criteria->compare('meta_keywords', $this->meta_keywords, true);
        $criteria->compare('regime_byd', $this->regime_byd, true);
        $criteria->compare('regime_mon', $this->regime_mon, true);
        $criteria->compare('regime_tue', $this->regime_tue, true);
        $criteria->compare('regime_wed', $this->regime_wed, true);
        $criteria->compare('regime_thu', $this->regime_thu, true);
        $criteria->compare('regime_fri', $this->regime_fri, true);
        $criteria->compare('regime_sat', $this->regime_sat, true);
        $criteria->compare('regime_sun', $this->regime_sun, true);
        $criteria->compare('telephone', $this->telephone, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('uid', $this->uid);
        $criteria->compare('sames', $this->sames, true);
        $criteria->compare('start_rate', $this->start_rate);
        $criteria->compare('district_id', $this->district_id);
        $criteria->compare('none_status', $this->none_status);
        $criteria->compare('modificator', $this->modificator);
        $criteria->compare('clinic_city_line_id', $this->clinic_city_line_id);
        $criteria->compare('clinic_city_id', $this->clinic_city_id);
        $criteria->compare('euro_price', $this->euro_price);
        $criteria->compare('depth', $this->depth);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getSubway_name()
    {
        $array = array();
        $data = MedbookingDiagnosticSubwayClinic::model()->findAllByAttributes(array('clinic_id' => $this->primaryKey));
        if (!empty($data)) {
            foreach ($data as $value) {
                if (!empty($value['sub']['title'])) {
                    $array[] = $value['sub']['title'];
                }
            }
        }
        $this->_subway_name = implode(", ", $array);
        return $this->_subway_name;
    }

    public function listPrice()
    {
        $array = array();
        if (!empty($this->clinic_category_s)) {
            foreach ($this->clinic_category_s as $value) {
                if (!empty($value->category_price_id)) {
                    if (!empty($value->catc_category->name_alt)) {
                        $name = $value->catc_category->name_alt;
                    } elseif (!empty($value->catc_category->name)) {
                        $name = $value->catc_category->name;
                    } else {
                        $name = '';
                    }
                    $array[] = array(
                        'id' => $value->id,
                        'name' => $name,
                        'price' => !empty($value->price) ? $value->price : '0',
                        'action_price' => !empty($value->action_price) ? $value->action_price : '',
                    );
                }
            }
        }
        return $array;
    }
}