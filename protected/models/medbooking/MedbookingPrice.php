<?php

class MedbookingPrice extends CActiveRecord
{

    public $check;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MedbookingCategory the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return CDbConnection database connection
     */
    public function getDbConnection()
    {
        return Yii::app()->db2;
    }

    public function tableName()
    {
        return 'price';
    }

    public function beforeSave()
    {
        if(!empty($this->price_action)){
            if($this->check==2){
                $this->price_action=ceil($this->price-(($this->price*$this->price_action)/100));
            }
        }
        return parent::beforeSave();
    }

    public function rules()
    {
        return array(
            array('doctor_id','required'),
            array('clinic_id','required'),
            array('category_id','required'),
            array('doctor_id, category_id, clinic_id, check, valute','numerical','integerOnly'=>true),
            array('price, price_action','length','max'=>255),
        );
    }

    public function relations()
    {
        return array(
            'clinic'=>array(self::BELONGS_TO,'MedbookingClinic','clinic_id'),
            'doctor'=>array(self::BELONGS_TO,'MedbookingDoctor','doctor_id'),
            'category'=>array(self::BELONGS_TO,'MedbookingCategory','category_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'clinic_id'=>'Клиника ID',
            'clinic_name'=>'Клиника (название)',
            'doctor_id'=>'Доктор ID',
            'doctor_name'=>'Доктор (ФИО)',
            'category_id'=>'Категория ID',
            'category_name'=>'Категория (название)',
            'price'=>'Стоимость',
            'price_action'=>'Аукционная Стоимость',
            'check'=>'Условия акции',
            'valute'=>'В Евро'
        );
    }

}
