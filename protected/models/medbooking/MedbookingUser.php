<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $uid
 * @property string $password
 * @property string $update_time
 * @property string $create_time
 * @property string $telephone
 * @property string $lastvisit
 * @property string $email
 * @property integer $gender
 * @property string $username
 * @property string $position
 * @property string $about
 * @property string $image
 * @property string $translit
 * @property integer $bloger
 * @property integer $social
 *
 * The followings are the available model relations:
 * @property Alias[] $aliases
 * @property ChildClinic[] $childClinics
 * @property ChildDoctor[] $childDoctors
 * @property ChildReviews[] $childReviews
 * @property MedbookingClinic[] $clinics
 * @property ContentHistory[] $contentHistories
 * @property ContentRoles[] $contentRoles
 * @property ContentTask[] $contentTasks
 * @property ContentVisibility[] $contentVisibilities
 * @property MedbookingDoctor[] $doctors
 * @property NodeFaq[] $nodeFaqs
 * @property NodeFaqCategory[] $nodeFaqCategories
 * @property NodeIllness[] $nodeIllnesses
 * @property NodeIllnessCategory[] $nodeIllnessCategories
 * @property MedbookingReviews[] $reviews
 */
class MedbookingUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('password, telephone, position, about, image, translit, social', 'required'),
			array('gender, bloger, social', 'numerical', 'integerOnly'=>true),
			array('password', 'length', 'max'=>100),
			array('telephone, email, username, position, image, translit', 'length', 'max'=>255),
			array('update_time, create_time, lastvisit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('uid, password, update_time, create_time, telephone, lastvisit, email, gender, username, position, about, image, translit, bloger, social', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aliases' => array(self::HAS_MANY, 'Alias', 'uid'),
			'childClinics' => array(self::HAS_MANY, 'ChildClinic', 'uid'),
			'childDoctors' => array(self::HAS_MANY, 'ChildDoctor', 'uid'),
			'childReviews' => array(self::HAS_MANY, 'ChildReviews', 'uid'),
			'clinics' => array(self::HAS_MANY, 'MedbookingClinic', 'uid'),
			'contentHistories' => array(self::HAS_MANY, 'ContentHistory', 'uid'),
			'contentRoles' => array(self::HAS_MANY, 'ContentRoles', 'uid'),
			'contentTasks' => array(self::HAS_MANY, 'ContentTask', 'uid'),
			'contentVisibilities' => array(self::HAS_MANY, 'ContentVisibility', 'uid'),
			'doctors' => array(self::HAS_MANY, 'MedbookingDoctor', 'uid'),
			'nodeFaqs' => array(self::HAS_MANY, 'NodeFaq', 'uid'),
			'nodeFaqCategories' => array(self::HAS_MANY, 'NodeFaqCategory', 'uid'),
			'nodeIllnesses' => array(self::HAS_MANY, 'NodeIllness', 'uid'),
			'nodeIllnessCategories' => array(self::HAS_MANY, 'NodeIllnessCategory', 'uid'),
			'reviews' => array(self::HAS_MANY, 'MedbookingReviews', 'uid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'password' => 'Password',
			'update_time' => 'Update Time',
			'create_time' => 'Create Time',
			'telephone' => 'Telephone',
			'lastvisit' => 'Lastvisit',
			'email' => 'Email',
			'gender' => 'Gender',
			'username' => 'Username',
			'position' => 'Position',
			'about' => 'About',
			'image' => 'Image',
			'translit' => 'Translit',
			'bloger' => 'Bloger',
			'social' => 'Social',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uid',$this->uid);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('lastvisit',$this->lastvisit,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('about',$this->about,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('translit',$this->translit,true);
		$criteria->compare('bloger',$this->bloger);
		$criteria->compare('social',$this->social);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}