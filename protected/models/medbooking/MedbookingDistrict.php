<?php

/**
 * This is the model class for table "district".
 *
 * The followings are the available columns in table 'district':
 * @property integer $id
 * @property string $title
 * @property string $translit
 * @property integer $district_line_id
 * @property integer $city
 *
 * The followings are the available model relations:
 * @property ChildClinic[] $childClinics
 * @property MedbookingClinic[] $clinics
 * @property ClinicDiagnostic[] $clinicDiagnostics
 * @property MedbookingSubway[] $subways
 */
class MedbookingDistrict extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingDistrict the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'district';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, district_line_id', 'required'),
			array('district_line_id, city', 'numerical', 'integerOnly'=>true),
			array('title, translit', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, translit, district_line_id, city', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'childClinics' => array(self::HAS_MANY, 'ChildClinic', 'district_id'),
			'clinics' => array(self::HAS_MANY, 'MedbookingClinic', 'district_id'),
			'clinicDiagnostics' => array(self::HAS_MANY, 'ClinicDiagnostic', 'district_id'),
			'subways' => array(self::HAS_MANY, 'MedbookingSubway', 'district_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'translit' => 'Translit',
			'district_line_id' => 'District Line',
			'city' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('translit',$this->translit,true);
		$criteria->compare('district_line_id',$this->district_line_id);
		$criteria->compare('city',$this->city);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}