<?php

/**
 * This is the model class for table "reviews".
 *
 * The followings are the available columns in table 'reviews':
 * @property integer $id
 * @property integer $status
 * @property integer $status_rss
 * @property string $title_rss
 * @property integer $clinic_id
 * @property integer $doctor_id
 * @property integer $service_id
 * @property integer $blog_id
 * @property string $name
 * @property string $telephone
 * @property string $email
 * @property string $description
 * @property string $image
 * @property double $attention_value
 * @property double $doctor_value
 * @property double $price_value
 * @property integer $uid
 * @property string $create_time
 * @property string $update_time
 * @property integer $record_id
 * @property integer $social
 *
 * The followings are the available model relations:
 * @property MedbookingClinic $clinic
 * @property MedbookingDoctor $doctor
 * @property MedbookingUser $u
 */
class MedbookingReviews extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedbookingReviews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reviews';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status_rss, title_rss, service_id, blog_id, name, email, image, create_time, social', 'required'),
			array('status, status_rss, clinic_id, doctor_id, service_id, blog_id, uid, record_id, social', 'numerical', 'integerOnly'=>true),
			array('attention_value, doctor_value, price_value', 'numerical'),
			array('title_rss, name, telephone, image', 'length', 'max'=>255),
			array('email', 'length', 'max'=>155),
			array('description, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, status, status_rss, title_rss, clinic_id, doctor_id, service_id, blog_id, name, telephone, email, description, image, attention_value, doctor_value, price_value, uid, create_time, update_time, record_id, social', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clinic' => array(self::BELONGS_TO, 'MedbookingClinic', 'clinic_id'),
			'doctor' => array(self::BELONGS_TO, 'MedbookingDoctor', 'doctor_id'),
			'u' => array(self::BELONGS_TO, 'MedbookingUser', 'uid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Status',
			'status_rss' => 'Status Rss',
			'title_rss' => 'Title Rss',
			'clinic_id' => 'Clinic',
			'doctor_id' => 'Doctor',
			'service_id' => 'Service',
			'blog_id' => 'Blog',
			'name' => 'Name',
			'telephone' => 'Telephone',
			'email' => 'Email',
			'description' => 'Description',
			'image' => 'Image',
			'attention_value' => 'Attention Value',
			'doctor_value' => 'Doctor Value',
			'price_value' => 'Price Value',
			'uid' => 'Uid',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'record_id' => 'Record',
			'social' => 'Social',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('status',$this->status);
		$criteria->compare('status_rss',$this->status_rss);
		$criteria->compare('title_rss',$this->title_rss,true);
		$criteria->compare('clinic_id',$this->clinic_id);
		$criteria->compare('doctor_id',$this->doctor_id);
		$criteria->compare('service_id',$this->service_id);
		$criteria->compare('blog_id',$this->blog_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('attention_value',$this->attention_value);
		$criteria->compare('doctor_value',$this->doctor_value);
		$criteria->compare('price_value',$this->price_value);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('record_id',$this->record_id);
		$criteria->compare('social',$this->social);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}