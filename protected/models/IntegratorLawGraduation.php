<?php

class IntegratorLawGraduation extends CActiveRecord
{

	const GRADUATION_ALL = 1;
	const GRADUATION_DIAG = 2;
	const GRADUATION_ANALYZ = 3;

	static public $graduationText = array(
		self::GRADUATION_ALL => 'Общая',
		self::GRADUATION_DIAG => 'Диагностика',
		self::GRADUATION_ANALYZ => 'Анализы'
	);

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'integrator_law_graduation';
	}

	public function relations()
	{
		return array(
			'law_graduation' => array(self::BELONGS_TO, 'IntegratorLaws', 'law_id'),
			'laws' => array(self::HAS_MANY, 'IntegratorLaws', 'law_id'),
		);
	}

	public static function getPricesArray($prices)
	{
		$return = array();
		foreach ($prices as $key => $value) {
			$return[$value->price] = $value->price;
		}
		return $return;
	}

	public function rules()
	{
		return array(
			array('price, count, type','safe'),
		);
	}

}
