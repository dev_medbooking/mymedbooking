<?php

class IntegratorLawReports extends CActiveRecord
{

	public $activeGraduation; //активная градация
	public $activeGraduationPatient; //количество пациентов дял активной градации

	const STATUS_REPORT = 1; // отчет создан
	const STATUS_AGREE = 2; // отчет согласован
	const STATUS_BILL = 3; // счет выставлен
	const STATUS_PAY = 4; // счет оплачен
	const STATUS_PART = 5; // счет оплачен частично
	const STATUS_DEBT = 6; // долг
	const STATUS_DECLINE = 7; // отказ
	const STATUS_NONE = 8; // нет записей

	static public $statusText = array(
		self::STATUS_REPORT => 'Отчет',
		self::STATUS_AGREE => 'Соглас.',
		self::STATUS_BILL => 'Счет',
		self::STATUS_PAY => 'Оплачен',
		self::STATUS_PART => 'Частично',
		self::STATUS_DECLINE => 'Отказ',
		self::STATUS_DEBT => 'Дебиторка',
		self::STATUS_NONE => 'Нет записей',
	);

	static public $allowModifyStatus = array(
		self::STATUS_REPORT,
		self::STATUS_AGREE,
		self::STATUS_BILL,
		self::STATUS_DEBT,
		self::STATUS_DECLINE,
		self::STATUS_NONE
	);

	static public $statusHeaders = array(
		self::STATUS_REPORT => 'Отчеты',
		self::STATUS_AGREE => 'Согласование',
		self::STATUS_BILL => 'Счета',
		self::STATUS_PAY => 'Оплаченные',
		self::STATUS_PART => 'Частично оплаченные',
		self::STATUS_DECLINE => 'Отказ',
		self::STATUS_DEBT => 'Дебиторка',
		self::STATUS_NONE => 'Нет записей',
	);

	public static function getAllowedStatus()
	{
		$result = array();
		foreach (self::$allowModifyStatus as $s) {
			$result[$s] = self::$statusText[$s];
		}
		return $result;
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'integrator_law_reports';
	}

	public function relations()
	{
		return array(
			'law' => array(self::BELONGS_TO, 'IntegratorLaws', 'law_id'),
			'law_report_status' => array(self::HAS_MANY, 'IntegratorLawReportsStatus', 'report_id'),
			'law_report_payments' => array(self::HAS_MANY, 'IntegratorLawReportsPayments', 'report_id'),
			'clinics_reports' => array(self::HAS_ONE, 'IntegratorClinicsReports', 'law_report_id')
		);
	}

	public function rules()
	{
		return array(
			array('report_date, summ, patient_record, patient_success, patient_count','safe'),
		);
	}

	/**
	 * Проверка счетов за месяц для юр. лица
	 * @param $id int id юр. лица
	 * @param $date datetime месяц на который надо проверить
	 * @return array список счетов
	 */
	public function getReportsInMonth($id, $date)
	{
		$date = new DateTime($date);
		$queryDate = $date->format('Y-m-01');
		$date->modify('last day of this month');
		$queryDateEnd = $date->format('Y-m-d');
		$query = Yii::app()->db->createCommand("SELECT * FROM integrator_law_reports WHERE law_id=:id AND DATE_FORMAT(report_date, '%Y-%m-%d') >= :queryDate AND DATE_FORMAT(report_date, '%Y-%m-%d') <= :queryDateEnd")
			->queryAll(TRUE, array(
				':id' => $id,
				':queryDate' => $queryDate,
				':queryDateEnd' => $queryDateEnd
			));
		return $query;
	}

	public function getReportBalance()
	{
		$query['reportBalance'] = Yii::app()->db->createCommand("SELECT IFNULL(SUM(summ), 0) FROM integrator_law_reports_payments WHERE report_id = :id")
			->queryScalar(array(
				':id' => $this->primaryKey,
			));
		return $query;
	}

	public function getGraduationPatient($type)
	{
		switch($type) {
			case IntegratorLawGraduation::GRADUATION_ALL:
				if ($this->law->graduation_all == 1)
					$result = $this->patient_success;
				else
					$result = $this->patient_success - $this->patient_diag - $this->patient_analyz;
				return ($result > 0 ? $result : 0);
			case IntegratorLawGraduation::GRADUATION_DIAG:
				$result = $this->patient_diag;
				return ($result > 0 ? $result : 0);
			case IntegratorLawGraduation::GRADUATION_ANALYZ:
				$result = $this->patient_analyz;
				return ($result > 0 ? $result : 0);
			default:
				return 0;
		}
	}

	public function getSuccessPercentPatient()
	{
		return ($this->patient_record != 0) ? round(100 * $this->patient_success / $this->patient_record) : 0;
	}

	public function getSuccessPercentdDiagPatient()
	{
		return ($this->patient_success != 0) ? round(100 * $this->patient_diag / $this->patient_success) : 0;
	}

	public function getPercentSumm()
	{
		return ($this->patient_success != 0) ? round($this->summ / $this->patient_success) : 0;
	}

	public function getNextGraduation($type)
	{
		$query = Yii::app()->db->createCommand("SELECT MIN(price) AS price, count FROM integrator_law_graduation WHERE '".$this->activeGraduation."' < price AND law_id = '".$this->law_id."' AND type = '".$type."'")->queryRow();
		return $query;
	}
}
