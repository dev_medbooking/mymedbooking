<?php

class MbLaw extends MbRecord
{

    const STATUS_CHECK=0; // заявка
    const STATUS_REVERT=1; // отказ
    const STATUS_LOOSE=2; // не явился
    const STATUS_LEAD=3; // были на приеме
    const STATUS_DECLINE=4; // отмена
    const STATUS_RECEPT=5; // записи
    const STATUS_PREVENT=6; // преварительная отмена
    const STATUS_LIST=7; // лист ожидания
    const STATUS_OKTELL=8; // oktell ожидания

    public $addClass='MbRecordAdd';
    public $addTemplate='MbSmsTemplate';
    public $addSms='MbRecordSms';
    public $addSession='MbSmsSession';
    public $addUser='MbUser';
    public $addDomain='MbDomain';
    public $callerid2;
    public $callerid1;
    public $dt;
    public $qty;
    public $tool_checked=0;
    public $allqty;
    public $infospec;
    public $callproove; // возможность добавить запись в коллцентр
    public $userproove; // возможность получить пользователя
    public $sogl; // отмечено ли пользовательское соглашение для некоторых сценариев
    public $sms='sms'; // ID-key session для проверки SMS кода ответа    private $_timecode;
    public $leadCheck=0;
    public $addressCheck=true;
    public $smsCheck=1;
    public $_during;
    public $_countTel;
    public $_countTelAll;
    public $_telephoneNext;
    public $_telephoneOut;
    public $qtyTel;
    public $title_company;
    /**
     * Длительность звонка
     * @return type
     */
    public function getDuring()
    {
        if(!empty($this->o->during)){
            $this->_during=$this->o->during;
        }
        return $this->_during;
    }

    /**
     * Длительность звонка
     * @param type $value
     */
    public function setDuring($value)
    {
        $this->_during=$value;
    }

    /**
     * Телефон вхождения
     * @return type
     */
    public function getTelephoneNext()
    {
        $this->_telephoneNext='Не обработан';
        if(!empty($this->oktell_id)){
            $dateFrom=date("Y-m-d",strtotime($this->create_time)-3600*24);
            $dateTo=date("Y-m-d",strtotime($this->create_time)+3600*24);
            $_telephone=str_replace(array("+","(",")"," ","-","."),array("","","","","",""),$this->telephone);
            $telephone=substr($_telephone,1);
            $flag=Yii::app()->db->createCommand("SELECT api_record.id FROM api_record LEFT JOIN invoice ON invoice.id=api_record.oktell_id WHERE (invoice.callerid1 LIKE '%{$telephone}%' AND invoice.callerid2!='IVR' AND DATE(invoice.dt)<='{$dateTo}' AND DATE(invoice.dt)>='{$dateFrom}') OR (invoice.callerid1 LIKE '%{$telephone}%' AND invoice.direction='cdOutcoming' AND DATE(invoice.dt)<='{$dateTo}' AND DATE(invoice.dt)>='{$dateFrom}') OR (api_record.telephone LIKE '%{$telephone}%' AND api_record.status!=8 AND DATE(api_record.create_time)<='{$dateTo}' AND DATE(api_record.create_time)>='{$dateFrom}')")->queryRow();
            if($flag){
                $this->_telephoneNext='Обработан';
            }
        }
        return $this->_telephoneNext;
    }

    /**
     * Телефон вхождения
     * @param type $value
     */
    public function setTelephoneNext($value)
    {
        $this->_telephoneNext=$value;
    }

    /**
     * Количество номеров телефона
     * @return type
     */
    public function getCountTel()
    {
        $this->_countTel='1';
        if(!empty($this->telephone)){
            $dateFrom=date("Y-m-d",strtotime($this->create_time)-3600*24);
            $dateTo=date("Y-m-d",strtotime($this->create_time)+3600*24);
            $_telephone=str_replace(array("+","(",")"," ","-","."),array("","","","","",""),$this->telephone);
            $telephone=substr($_telephone,1);
            $flag=Yii::app()->db->createCommand("SELECT COUNT(*) AS qty FROM invoice WHERE invoice.callerid1 LIKE '%{$telephone}%' AND DATE(invoice.dt)<='{$dateTo}' AND DATE(invoice.dt)>='{$dateFrom}'")->queryRow();
            if($flag['qty']){
                $this->_countTel=$flag['qty'];
            }
        }
        return $this->_countTel;
    }

    /**
     * Количество номеров телефона
     * @param type $value
     */
    public function setCountTel($value)
    {
        $this->_countTel=$value;
    }

    /**
     * Количество телефонов
     * @return type
     */
    public function getCountTelAll()
    {
        $this->_countTelAll='1';
        if(!empty($this->telephone)){
            $_telephone=str_replace(array("+","(",")"," ","-","."),array("","","","","",""),$this->telephone);
            $telephone=substr($_telephone,1);
            $flag=Yii::app()->db->createCommand("SELECT COUNT(*) AS qty FROM invoice WHERE invoice.callerid1 LIKE '%{$telephone}%'")->queryRow();
            if($flag['qty']){
                $this->_countTelAll=$flag['qty'];
            }
        }
        return $this->_countTelAll;
    }

    /**
     * Количество телефонов
     * @param type $value
     */
    public function setCountTelAll($value)
    {
        $this->_countTelAll=$value;
    }

    /**
     * Телефон
     * @return type
     */
    public function getTelephoneOut()
    {
        $this->_telephoneOut='&mdash;';
        if(!empty($this->oktell_id)){
            $_telephone=str_replace(array("+","(",")"," ","-","."),array("","","","","",""),$this->telephone);
            $telephone=substr($_telephone,1);
            $flag=Yii::app()->db->createCommand("SELECT * FROM invoice WHERE callerid1 LIKE '%{$telephone}%' AND direction='cdOutcoming' AND id>'{$this->oktell_id}'")->queryRow();
            if($flag){
                $this->_telephoneOut='Исходящий';
            }
        }
        return $this->_telephoneOut;
    }

    /**
     * Телефон
     * @param type $value
     */
    public function setTelephoneOut($value)
    {
        $this->_telephoneOut=$value;
    }
    
    /**
     * Модель
     * @param type $className
     * @return type
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Таблица в базе данных
     * @return string
     */
    public function tableName()
    {
        return 'api_record';
    }

    /**
     * Установить $statusName
     * @param type $value
     */
    public function setStatusName($value)
    {
        $this->_statusName=$value;
    }

    /**
     * @return string
     * const STATUS_CHECK=0; // заявка
     * const STATUS_REVERT=1; // отказ
     * const STATUS_LOOSE=2; // не явился
     * const STATUS_LEAD=3; // были на приеме
     * const STATUS_DECLINE=4; // отмена
     * const STATUS_RECEPT=5; // записи 
     */
    public function getStatusName()
    {
        if(empty($this->_statusName)){
            if(isset($this->status)){
                switch($this->status)
                {
                    case self::STATUS_CHECK:
                        $this->_statusName='Заявка';
                        break;
                    case self::STATUS_REVERT:
                        $this->_statusName='Отказ';
                        break;
                    case self::STATUS_LOOSE:
                        $this->_statusName='Не явился';
                        break;
                    case self::STATUS_LEAD:
                        $this->_statusName='Были на приеме';
                        break;
                    case self::STATUS_DECLINE:
                        $this->_statusName='Отменена';
                        break;
                    case self::STATUS_RECEPT:
                        $this->_statusName='Записи';
                        break;
                    case self::STATUS_PREVENT:
                        $this->_statusName='Окончательный отказ';
                        break;
                    case self::STATUS_LIST:
                        $this->_statusName='Лист ожидания';
                        break;
                    case self::STATUS_OKTELL:
                        $this->_statusName='Oktell';
                        break;
                    default:
                        $this->_statusName='n/a';
                }
            }
        }
        return $this->_statusName;
    }

    /**
     * Цвет
     * @return string
     */
    public function getColor()
    {
        $output='row-empty';
        if(!empty($this->domain)){
            switch($this->domain)
            {
                case 'medbooking.com':
                    $output='row-medbooking';
                    break;
                case 'testpuls.ru':
                    $output='row-testpuls';
                    break;
                case 'timetovisit.ru':
                    $output='row-timetovisit';
                    break;
                case 'fromed.ru':
                    $output='row-fromed';
                    break;
            }
        }
        return $output;
    }

    /**
     * Цвет хоста
     * @return string
     */
    public function getHostColor()
    {
        $color='#fff';
        if($this->host==1){
            $color='#0f70b3';
        } elseif($this->host==2){
            $color='#b0d083';
        } elseif($this->host==3){
            $color='#ffa500';
        } elseif($this->host==4){
            $color='#c66f6a';
        }
        return $color;
    }

    /**
     * Тип заявки
     * @return string
     */
    public function getType()
    {
        if(!empty($this->fromcall)){
            $output='ico-call-status-application';
        } elseif(!empty($this->fromsite)){
            $output='ico-application';
        } else{
            $output='ico-phone-status-application';
        }
        return $output;
    }

    /**
     * $formattedLeadTime
     * @return string
     */
    public function getFormattedLeadTime()
    {
        if(!empty($this->lead_time)&&$this->lead_time!='0000-00-00 00:00:00'){
            return date('d-m-Y H:i',strtotime($this->lead_time));
        } else{
            return '0000-00-00 00:00';
        }
    }

    /**
     * Фрматировнное время lead
     * @param type $formattedLeadTime
     */
    public function setFormattedLeadTime($formattedLeadTime)
    {
        $this->lead_time=date("Y-m-d H:i:s",strtotime($formattedLeadTime.":00"));
    }

    /**
     * $formattedRecTime
     * @return string
     */
    public function getFormattedRecTime()
    {
        if(!empty($this->reception_time)&&$this->reception_time!='0000-00-00 00:00:00'){
            return date('d-m-Y H:i',strtotime($this->reception_time));
        } else{
            return '0000-00-00 00:00';
        }
    }

    /**
     * $formattedRecTime
     * @param mixed $formattedRecTime
     */
    public function setFormattedRecTime($formattedRecTime)
    {
        $this->reception_time=date("Y-m-d H:i:s",strtotime($formattedRecTime.":00"));
    }

    /**
     * $formattedCreateTime
     * @return string
     */
    public function getFormattedCreateTime()
    {
        if(!empty($this->create_time)&&$this->create_time!='0000-00-00 00:00:00'){
            return date('d-m-Y H:i',strtotime($this->create_time));
        } else{
            return '0000-00-00 00:00';
        }
    }

    /**
     * $formattedCreateTime
     * @param mixed $formattedCreateTime
     */
    public function setFormattedCreateTime($formattedCreateTime)
    {
        $this->create_time=date("Y-m-d H:i:s",strtotime($formattedCreateTime.":00"));
    }

    /**
     * $formattedUpdateTime
     * @return type
     */
    public function getFormattedUpdateTime()
    {
        if(!empty($this->update_time)&&$this->update_time!='0000-00-00 00:00:00'){
            return date('d-m-Y H:i',strtotime($this->update_time));
        } else{
            return '0000-00-00 00:00';
        }
    }

    /**
     * $formattedUpdateTime
     * @param type $formattedUpdateTime
     */
    public function setFormattedUpdateTime($formattedUpdateTime)
    {
        $this->update_time=date("Y-m-d H:i:s",strtotime($formattedUpdateTime.":00"));
    }

    /**
     * Код из SMS
     * @return type
     */
    public function getTimecode()
    {
        return $this->_timecode;
    }

    /**
     * Код из SMS
     * @param type $value
     */
    public function setTimecode($value)
    {
        $this->_timecode=$value;
    }

    /**
     * Сессия
     * @return type
     */
    public function getTimesession()
    {
        if(!empty($this->timecode)&&!empty($_POST['sms'])){
            $_addSession=$this->addSession;
            $sms=$_addSession::model()->findByAttributes(array('sid'=>$this->timecode));
            if(!empty($sms->id)&&$sms->id==$_POST['sms']){
                $this->_timesession=$this->timecode;
            }
        }
        if(empty($this->_timesession)){
            $this->_timesession=false;
        }
        return $this->_timesession;
    }

    /**
     * Сессия
     * @param type $value
     */
    public function setTimesession($value)
    {
        $this->_timesession=$value;
    }

    /**
     * Телефон пользователя
     * @return type
     */
    public function getUserphone()
    {
        if(Yii::app()->user->id){ // переписываем при наличии пользовательского ID
            $_addUser=$this->addUser;
            $_userModel=$_addUser::model()->findByPk(Yii::app()->user->id);
            if(!empty($_userModel->telephone)){
                $this->_userphone=$_userModel->telephone;
            }
        }
        if(empty($this->_userphone)){
            $this->_userphone=false;
        }
        return $this->_userphone;
    }

    /**
     * Телефон пользователя
     * @param type $value
     */
    public function setUserphone($value)
    {
        $this->_userphone=$value;
    }

    /**
     * Правила валидации
     * @return type
     */
    public function rules()
    {
        return array(
            array('gender','required','on'=>'guest','message'=>'Укажите пол'),
            array('sogl','required','on'=>'guest','message'=>'Пользовательское соглашение'),
            array('timecode','required','on'=>'user','message'=>'Телефон, введенный Вами не соответствует телефону в Вашем профиле, проверьте его значение или смените телефон в профиле'),
            array('telephone','compare','compareAttribute'=>'userphone','on'=>'user','message'=>'Укажите телефон'),
            array('tool_checked','compare','compareValue'=>0),
            array('userphone','required','on'=>'user','message'=>'Телефон, введенный Вами не соответствует телефону в Вашем профиле, проверьте его значение или смените телефон в профиле'),
            array('uid','required','on'=>'user','message'=>'Пользователь в системе не обнаружен'),
            array('author_id','required','on'=>'user','message'=>'Автор в системе не обнаружен'),
            array('telephone','required','on'=>'admin','message'=>'Укажите телефон'),
            array('author_id','required','on'=>'admin','message'=>'Укажите автора'),
            array('reception_time','required','message'=>'Время для записи не указано'),
            array('telephone','required','message'=>'Укажите свой телефон'),
            array('userproove, callproove','boolean'),
            array('timesession, userphone, callproove, userproove, note, reason, comment, lead_time, update_time, create_time, reception_time','safe'),
            array('gender, new_pacient, fromcall, rerecord, fromsite, house, sms_checked','length','max'=>1),
            array('status','length','max'=>2),
            array('hour_send_status, day_send_status, rerecord, oldstatus, id_call','numerical','integerOnly'=>true),
            array('attention_value, doctor_value, price_value','numerical','integerOnly'=>true),
            array('gender, status, new_pacient, category_id, uid, author_id, account_author_id, account_first_id, clinic_id, doctor_id, timeslot_id, deleted, house, flag_id','numerical','integerOnly'=>true),
            array('name, email, domain','length','max'=>128),
            array('telephone','length','max'=>12,'min'=>10),
            array('book_id, source, description, clinic_name, doctor_name, category_name','length','max'=>255),
            array('email','email'),
            array('review','safe'),
            array('uid, author_id, first_uid, account_author_id, account_first_id','exist','attributeName'=>'uid','className'=>'MbUser'),
            array('oktell_id','exist','attributeName'=>'id','className'=>'AdminInvoice'),
            array('flag_id','exist','attributeName'=>'id','className'=>'MbFlag'),
            array('status_time','type','type'=>'datetime','allowEmpty'=>true,'datetimeFormat'=>'yyyy-mm-dd hh:mm:ss'),
            array('description, email, timeslot_id, new_pacient, category_id, doctor_id, author_id, first_uid, uid','default','setOnEmpty'=>true,'value'=>null),
            array('description, telephone, name, clinic_name, doctor_name, category_name','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
        );
    }

    /**
     * Связт с другими моделями
     * @return type
     */
    public function relations()
    {
        return array(
            'integrator'=>array(self::BELONGS_TO, 'IntegratorClinic', '', 'foreignKey' => array('clinic_id'=>'medbooking_id')),
            'fl'=>array(self::BELONGS_TO,'MbFlag','flag_id'),
        );
    }
 /**
     * Метки аттрибутов
     * @return type
     */
    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'sms_checked'=>'Без SMS',
            'id_call'=>'Call ID',
            'flag_id'=>'Флаг',
            'oktell_id'=>'Oktell ID',
            'deleted'=>'Корзина',
            'infospec'=>'Специальности',
            'note'=>'Комментарии',
            'hour_send_status'=>'Уведомление за час',
            'day_send_status'=>'Уведомление за день',
            'domain'=>'Домен',
            'doctor_value'=>'Оценка доктора',
            'price_value'=>'Оценка цены',
            'attention_value'=>'Оценка внимания',
            'gender'=>'Пол',
            'name'=>'ФИО',
            'book_id'=>'ID записи (внешний)',
            'email'=>'E-mail',
            'telephone'=>'Телефон',
            'uid'=>'Клиент',
            'category_id'=>'Услуга',
            'description'=>'Сообщение',
            'comment'=>'Отзыв',
            'status'=>'Статус',
            'new_pacient'=>'Новый пациент',
            'status_time'=>'Время смены статуса',
            'reception_time'=>'На время',
            'formattedRecTime'=>'На время',
            'formattedLeadTime'=>'Время лида',
            'timeslot_id'=>'Таймслот',
            'doctor_id'=>'Доктор',
            'doctor_name'=>'Доктор',
            'telephoneNext'=>'Отзвон',
            'during'=>'Длительность',
            'clinic_id'=>'Клиника',
            'clinic_name'=>'Клиника',
            'category_name'=>'Категория',
            'source'=>'Источник',
            'author_id'=>'Автор',
            'account_author_id'=>'Автор аккаунта',
            'first_uid'=>'Редактор',
            'account_first_id'=>'Редактор аккаунта',
            'create_time'=>'Создано',
            'formattedCreateTime'=>'Создано',
            'formattedUpdateTime'=>'Редактировано',
            'update_time'=>'Редактировано',
            'callproove'=>'Запись на колцентр',
            'userphone'=>'Телефон пользователя',
            'userproove'=>'Создание пользователя',
            'timecode'=>'Код из СМС',
            'timesession'=>'Код возврата',
            'statusName'=>'Статус',
            'fromsite'=>'С сайта',
            'fromcall'=>'Из ОС',
            'house'=>'Из обратной связи',
            'reason'=>'Причина отказа',
            'review'=>'Отзыв',
            'tool_checked'=>'Причина отказа',
            'rerecord'=>'Повторно пришел',
        );
    }

    /**
     * Поведения
     * @return type
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior'=>array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'createAttribute'=>'create_time',
                'updateAttribute'=>'update_time',
            ),
        );
    }

    /**
     * Пол
     * @return string
     */
    public function getSex()
    {
        if(!empty($this->gender)&&($this->gender==1)){
            return 'Мужчина';
        } elseif(!empty($this->gender)&&($this->gender==2)){
            return 'Женщина';
        } else{
            return 'n/a';
        }
    }

    /**
     * Домен
     * @return type
     */
    public function getDomainId()
    {
        if(!empty($this->domain)){
            $_addDomain=$this->addDomain;
            $domain=$_addDomain::model()->findByAttributes(array('title'=>$this->domain));
            if(!empty($domain->id)){
                return $domain->id;
            }
        }
    }

    /**
     * Хост
     * @return type
     */
    public function getHost()
    {
        if(!empty($this->domain)){
            $_addDomain=$this->addDomain;
            $domain=$_addDomain::model()->findByAttributes(array('title'=>$this->domain));
            if(!empty($domain->id)){
                return $domain->id;
            }
        }
    }

    /**
     * До валидации
     * @return type
     */
    public function beforeValidate()
    {
        if(!empty($_POST[__CLASS__]['tool_checked'])&&$_POST[__CLASS__]['tool_checked']==1){
            $this->tool_checked=1;
        } else {
            $this->tool_checked=0;
        }
        // пользователь или админ
        if(!empty(Yii::app()->user->id)){
            if(empty($this->author_id)){
                $this->author_id=Yii::app()->user->id;
            }
            if(!Yii::app()->user->checkAccess('NDasha')||Yii::app()->user->checkAccess('Sellteam')){ // если не сотрудник аккаунт отдела
                $this->first_uid=Yii::app()->user->id;
            }
        }
        // пользователь или админ из аккаунт отдела
        if(!empty(Yii::app()->user->id)&&Yii::app()->user->checkAccess('NDasha')&&!Yii::app()->user->checkAccess('Sellteam')){
            if(empty($this->account_author_id)){
                $this->account_author_id=Yii::app()->user->id;
            }
            $this->account_first_id=Yii::app()->user->id;
        }
        // на всякий случай приводим к нормальному значению телефон
        if(!empty($this->telephone)){
            $this->telephone=str_replace(array("+7","(",")"," ","-","."),array("","","","","",""),$this->telephone);
        }
        // если меняем статус
        if((isset($this->oldstatus)&&$this->oldstatus!=$this->status)||$this->isNewRecord){
            $this->status_time=date("Y-m-d H:i:s");
        }
        // при наличие метки приводит к нормальному значению
        if(!empty($this->reception_time)&&$this->reception_time!='0000-00-00 00:00'){
            $this->setScenario('noreception');
            $_time=strtotime($this->reception_time);
            $this->reception_time=date("Y-m-d H:i:00",$_time);
        } else{
            $this->reception_time='0000-00-00 00:00';
        }
        if(!empty($this->create_time)){
            $_time=strtotime($this->create_time);
            $this->create_time=date("Y-m-d H:i:00",$_time);
        }
        // актуально только для новой записи при регистрации пользователем из ЛК
        if($this->isNewRecord){
            // new_pacient ничего по сути не дает, кроме галочки. Все равно проверку делать надо.
            // Не новый пациент, так же является переключателем создание нового пользователя
            // если пользователь регистрируется себя сам
            if(Yii::app()->user->id){
                $this->uid=Yii::app()->user->id;
            }
            // если есть UID или TELEPHONE
            if(!empty($this->uid)){
                $user=MbUser::model()->findByPk($this->uid);
            } elseif(!empty($this->telephone)){
                $user=MbUser::model()->findByAttributes(array('telephone'=>$this->telephone));
            }
            // если пользователя удалось вытащить
            if(!empty($user->primaryKey)){ // назначаем автоматически, если значений не передано
                $this->uid=$user->primaryKey;
                // если не введен e-mail, забираем, если есть, из учетной записи
                if(empty($this->email)&&!empty($user->email)){
                    $this->email=$user->email;
                }
                // если не введен логин, забираем, если есть, из учетной записи
                if(empty($this->name)&&!empty($user->nameperson)){
                    $this->name=$user->nameperson;
                }
                // если не введен пол, забираем, если есть, из учетной записи
                if(empty($this->gender)&&!empty($user->gender)){
                    $this->gender=$user->gender;
                }
            }
        }
        // для повторности записи уже дошедшего
        if(!empty($this->telephone)){
            $flag=self::model()->findByAttributes(array('telephone'=>$this->telephone,'status'=>3));
            if(!empty($flag)){
                $this->rerecord=1;
            }
        }
        return parent::beforeValidate();
    }

    /**
     * beforeSave()
     * @return type
     */
    public function beforeSave()
    {
        if($this->isNewRecord){
            if($this->status==3){
                $this->lead_time=$this->reception_time;
            }
        } else{
            if(empty($this->leadCheck)){
                if(($this->status!=$this->oldstatus)||$this->status==3){ // если статусы меняются
                    if($this->status==3){
                        if(empty($this->lead_time)||$this->lead_time=='0000-00-00 00:00:00'){
                            $this->lead_time=date("Y-m-d H:i:00",time());
                        }
                    } else{
                        $this->lead_time='0000-00-00 00:00:00';
                    }
                }
            }
        }
        return parent::beforeSave();
    }

    /**
     * afterSave()
     * @return type
     */
    public function afterSave()
    {
        // ЗДЕСЬ МЕНЯЕМ АДРЕС
        if($this->addressCheck){
            $this->addressSubway();
        }
        if($this->smsCheck==1&&empty($this->sms_checked)){
            if($this->isNewRecord){
                if($this->status==5&&!empty($this->reception_time)&&$this->reception_time!='0000-00-00 00:00:00'&&strtotime($this->reception_time)>time()){
                    $this->sendSmsTemplate('APROOVE_VISIT.ftl');
                } elseif($this->status==0){
                    if(!empty($this->sms_send_admin)){
                        $this->sendSmsTemplate('SENDER');
                    }
                }
            } else{
                if($this->status!=$this->oldstatus){ // если статусы меняются
                    if($this->status==3){
                        if(!empty($this->comment)){
                            // отправка c отзывом
                            $this->sendSmsTemplate('TEMPLATE_5_WITH.ftl');
                        } else{
                            // отправка без отзыва
                            $this->sendSmsTemplate('TEMPLATE_5.ftl');
                        }
                    } elseif($this->status==4){
                        if(!empty($this->reception_time)&&$this->reception_time!='0000-00-00 00:00:00'&&strtotime($this->reception_time)>time()){
                            // отправляем уведомление об отмене
                            $this->sendSmsTemplate('CANCEL_VISIT.ftl');
                        }
                    } elseif($this->status==5){
                        if(!empty($this->reception_time)&&$this->reception_time!='0000-00-00 00:00:00'&&strtotime($this->reception_time)>time()){
                            // отправляем уведомление о переводе в заявку
                            $this->sendSmsTemplate('APROOVE_VISIT.ftl');
                        }
                    }
                } else{
                    if(!empty($this->old_reception_time)&&$this->reception_time!=$this->old_reception_time){
                        if($this->status==4){
                            if(!empty($this->reception_time)&&$this->reception_time!='0000-00-00 00:00:00'&&strtotime($this->reception_time)>time()){
                                // отправляем уведомление об отмене. Уточнение
                                $this->sendSmsTemplate('CANCEL_VISIT.ftl');
                            }
                        } elseif($this->status==5){
                            if(!empty($this->reception_time)&&$this->reception_time!='0000-00-00 00:00:00'&&strtotime($this->reception_time)>time()){
                                // отправляем уведомление о переводе в заявку. Уточнение
                                $this->sendSmsTemplate('APROOVE_VISIT.ftl');
                            }
                        }
                    }
                }
            }
            if($this->status!=$this->oldstatus){ // если статусы меняются
                Yii::app()->db->createCommand("UPDATE ".($this->tableName())." SET oldstatus=status WHERE id='".$this->id."'")->execute();
            }
            if(empty($this->old_reception_time)||$this->reception_time!=$this->old_reception_time){ // если статусы меняются
                Yii::app()->db->createCommand("UPDATE ".($this->tableName())." SET old_reception_time=reception_time WHERE id='".$this->id."'")->execute();
            }
        }
        $this->record();
        if(!empty($this->reason)&&($this->reason=='Врач не работает')){
            $doctor=!empty($this->doctor_name)?$this->doctor_name:'по заявке '.$this->primaryKey;
            MbController::mailto('Врач не работает','Врач '.$doctor,'mt@timetovisit.ru');
        }
        return parent::afterSave();
    }

    /**
     * Отправка и сохранение записи об отправке СМС, вызов класса MbRecordSms
     * @param type $title
     */
    private function sendSmsTemplate($title)
    {
        $_addTemplate=$this->addTemplate;
        $template=$_addTemplate::model()->findByAttributes(array('title'=>$title),"CURTIME()>time_start AND CURTIME()<time_end");
        if(!empty($template->id)&&!empty($template->message)){
            $_addSms=$this->addSms;
            $smsModel=new $_addSms;
            $smsModel->sms_send_admin=true;
            $smsModel->message=$template->message;
            $smsModel->record_id=$this->id;
            if(Yii::app()->user->id){
                $smsModel->author_id=Yii::app()->user->id;
            }
            $smsModel->save();
        }
    }

    /**
     * Обновление через API данных о клинике и враче
     */
    private function addressSubway()
    {
        $modelThis=self::model()->findByPk($this->primaryKey);
        $modelThis->smsCheck=0;
        if($this->domain){
            $_addClass=$this->addClass;
            $model=$_addClass::model()->findByAttributes(array('record_id'=>$this->id));
            if(empty($model->id)){
                $model=new $_addClass;
                $model->record_id=$this->id;
            }
            if(!empty($this->clinic_id)){
                $response=$this->sendAddress($this->domain,'clinic',$this->clinic_id);
                if(!empty($this->clinic_name)){
                    if(!empty($response['clinic_name'])&&($this->clinic_name!=$response['clinic_name'])){
                        $modelThis->addressCheck=false;
                        Yii::app()->db->createCommand("UPDATE ".($this->tableName())." SET clinic_name='".$response['clinic_name']."' WHERE id='".$this->id."'")->execute();
                    }
                } else{
                    if(!empty($response['clinic_name'])){
                        $modelThis->addressCheck=false;
                        Yii::app()->db->createCommand("UPDATE ".($this->tableName())." SET clinic_name='".$response['clinic_name']."' WHERE id='".$this->id."'")->execute();
                    }
                }
                if(!empty($this->info->address)){
                    if(!empty($response['address'])&&($this->info->address!=$response['address'])){
                        $model->address=$response['address'];
                    }
                } else{
                    if(!empty($response['address'])){
                        $model->address=$response['address'];
                    }
                }
                if(!empty($this->info->lat)){
                    if(!empty($response['lat'])&&($this->info->lat!=$response['lat'])){
                        $model->lat=$response['lat'];
                    }
                } else{
                    if(!empty($response['lat'])){
                        $model->lat=$response['lat'];
                    }
                }
                if(!empty($this->info->lng)){
                    if(!empty($response['lng'])&&($this->info->lng!=$response['lng'])){
                        $model->lng=$response['lng'];
                    }
                } else{
                    if(!empty($response['lng'])){
                        $model->lng=$response['lng'];
                    }
                }
                if(!empty($this->info->clinic_url)){
                    if(!empty($response['clinic_url'])&&($this->info->clinic_url!=$response['clinic_url'])){
                        $model->clinic_url=$response['clinic_url'];
                    }
                } else{
                    if(!empty($response['clinic_url'])){
                        $model->clinic_url=$response['clinic_url'];
                    }
                }
                if(!empty($this->info->clinic_img_url)){
                    if(!empty($response['clinic_img_url'])&&($this->info->clinic_img_url!=$response['clinic_img_url'])){
                        $model->clinic_img_url=$response['clinic_img_url'];
                    }
                } else{
                    if(!empty($response['clinic_img_url'])){
                        $model->clinic_img_url=$response['clinic_img_url'];
                    }
                }
                if(!empty($this->info->regime_1)){
                    if(!empty($response['regime_1'])&&($this->info->regime_1!=$response['regime_1'])){
                        $model->regime_1=$response['regime_1'];
                    }
                } else{
                    if(!empty($response['regime_1'])){
                        $model->regime_1=$response['regime_1'];
                    }
                }
                if(!empty($this->info->regime_2)){
                    if(!empty($response['regime_2'])&&($this->info->regime_2!=$response['regime_2'])){
                        $model->regime_2=$response['regime_2'];
                    }
                } else{
                    if(!empty($response['regime_2'])){
                        $model->regime_2=$response['regime_2'];
                    }
                }
                if(!empty($this->info->regime_3)){
                    if(!empty($response['regime_3'])&&($this->info->regime_3!=$response['regime_3'])){
                        $model->regime_3=$response['regime_3'];
                    }
                } else{
                    if(!empty($response['regime_3'])){
                        $model->regime_3=$response['regime_3'];
                    }
                }
                if(!empty($this->info->regime_4)){
                    if(!empty($response['regime_4'])&&($this->info->regime_4!=$response['regime_4'])){
                        $model->regime_4=$response['regime_4'];
                    }
                } else{
                    if(!empty($response['regime_4'])){
                        $model->regime_4=$response['regime_4'];
                    }
                }
                if(!empty($this->info->regime_5)){
                    if(!empty($response['regime_5'])&&($this->info->regime_5!=$response['regime_5'])){
                        $model->regime_5=$response['regime_5'];
                    }
                } else{
                    if(!empty($response['regime_5'])){
                        $model->regime_5=$response['regime_5'];
                    }
                }
                if(!empty($this->info->regime_6)){
                    if(!empty($response['regime_6'])&&($this->info->regime_6!=$response['regime_6'])){
                        $model->regime_6=$response['regime_6'];
                    }
                } else{
                    if(!empty($response['regime_6'])){
                        $model->regime_6=$response['regime_6'];
                    }
                }
                if(!empty($this->info->regime_7)){
                    if(!empty($response['regime_1'])&&($this->info->regime_7!=$response['regime_7'])){
                        $model->regime_7=$response['regime_7'];
                    }
                } else{
                    if(!empty($response['regime_7'])){
                        $model->regime_7=$response['regime_7'];
                    }
                }
                if(!empty($this->info->regime_8)){
                    if(!empty($response['regime_8'])&&($this->info->regime_8!=$response['regime_8'])){
                        $model->regime_8=$response['regime_8'];
                    }
                } else{
                    if(!empty($response['regime_8'])){
                        $model->regime_8=$response['regime_8'];
                    }
                }
                if(!empty($this->info->subway)){
                    if(!empty($response['subway'])&&($this->info->subway!=$response['subway'])){
                        $model->subway=$response['subway'];
                    }
                } else{
                    if(!empty($response['subway'])){
                        $model->subway=$response['subway'];
                    }
                }
            }
            if(!empty($this->doctor_id)){
                $response=$this->sendAddress($this->domain,'doctor',$this->doctor_id);
                if(!empty($this->doctor_name)){
                    if(!empty($response['doctor_name'])&&($this->clinic_name!=$response['doctor_name'])){
                        $modelThis->addressCheck=false;
                        Yii::app()->db->createCommand("UPDATE ".($this->tableName())." SET doctor_name='".$response['doctor_name']."' WHERE id='".$this->id."'")->execute();
                    }
                } else{
                    if(!empty($response['doctor_name'])){
                        $modelThis->addressCheck=false;
                        Yii::app()->db->createCommand("UPDATE ".($this->tableName())." SET doctor_name='".$response['doctor_name']."' WHERE id='".$this->id."'")->execute();
                    }
                }
                if(!empty($this->info->speciality)){
                    if(!empty($response['speciality'])&&($this->info->speciality!=$response['speciality'])){
                        $model->speciality=$response['speciality'];
                    }
                } else{
                    if(!empty($response['speciality'])){
                        $model->speciality=$response['speciality'];
                    }
                }
                if(!empty($this->info->position)){
                    if(!empty($response['position'])&&($this->info->position!=$response['position'])){
                        $model->position=$response['position'];
                    }
                } else{
                    if(!empty($response['position'])){
                        $model->position=$response['position'];
                    }
                }
                if(!empty($this->info->experience)){
                    if(!empty($response['experience'])&&($this->info->experience!=$response['experience'])){
                        $model->experience=$response['experience'];
                    }
                } else{
                    if(!empty($response['experience'])){
                        $model->experience=$response['experience'];
                    }
                }
                if(!empty($this->info->doctor_description)){
                    if(!empty($response['doctor_description'])&&($this->info->doctor_description!=$response['doctor_description'])){
                        $model->doctor_description=$response['doctor_description'];
                    }
                } else{
                    if(!empty($response['doctor_description'])){
                        $model->doctor_description=$response['doctor_description'];
                    }
                }
                if(!empty($this->info->doctor_url)){
                    if(!empty($response['doctor_url'])&&($this->info->doctor_url!=$response['doctor_url'])){
                        $model->doctor_url=$response['doctor_url'];
                    }
                } else{
                    if(!empty($response['doctor_url'])){
                        $model->doctor_url=$response['doctor_url'];
                    }
                }
                if(!empty($this->info->doctor_img_url)){
                    if(!empty($response['doctor_img_url'])&&($this->info->doctor_img_url!=$response['doctor_img_url'])){
                        $model->doctor_img_url=$response['doctor_img_url'];
                    }
                } else{
                    if(!empty($response['doctor_img_url'])){
                        $model->doctor_img_url=$response['doctor_img_url'];
                    }
                }
            }
            $model->save();
        }
    }

    /**
     * Общий функционал запрос данных
     * @param type $host_id
     * @param type $method
     * @param type $id
     */
    private function sendAddress($host,$type,$id)
    {
        $ch=curl_init();
        curl_setopt($ch,CURLOPT_URL,'http://'.$host.'/check/'.$type.'/id/'.$id);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
        curl_setopt($ch,CURLOPT_POSTFIELDS,array('id'=>$id));
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_USERAGENT,$_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch,CURLOPT_VERBOSE,1);
        curl_setopt($ch,CURLINFO_HEADER_OUT,1);
        $response=curl_exec($ch);
        curl_close($ch);
        if($response){
            return CJSON::decode($response);
        }
    }

    /**
     * Запись через API
     */
    public function record()
    {
        if(!empty($this->review)&&!empty($this->domain)){
            $array=array();
            if($this->domain=='child'){
                $host='medbooking.com';
            } else{
                $host=$this->domain;
            }
            $array['review']=$this->review;
            $array['host']=$this->domain;
            $array['price_value']=!empty($this->price_value)?$this->price_value:'';
            $array['doctor_value']=!empty($this->doctor_value)?$this->doctor_value:'';
            $array['attention_value']=!empty($this->attention_value)?$this->attention_value:'';
            $array['clinic_id']=!empty($this->clinic_id)?$this->clinic_id:'';
            $array['doctor_id']=!empty($this->doctor_id)?$this->doctor_id:'';
            $array['telephone']=!empty($this->telephone)?$this->telephone:'';
            $array['name']=!empty($this->name)?$this->name:'';
            $ch=curl_init();
            curl_setopt($ch,CURLOPT_URL,'http://'.$host.'/check/review/id/'.$this->id);
            curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
            curl_setopt($ch,CURLOPT_POST,1);
            curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($array));
            curl_setopt($ch,CURLOPT_USERAGENT,$_SERVER['HTTP_USER_AGENT']);
            curl_setopt($ch,CURLOPT_HEADER,0);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
            curl_exec($ch);
            curl_close($ch);
        }
    }

    private $_timesession;
    private $_userphone;
    private $_statusName;

}
