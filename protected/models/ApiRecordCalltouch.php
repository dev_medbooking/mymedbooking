<?php

class ApiRecordCalltouch extends CActiveRecord
{

	public $utmSourceCount; // количество после группировки по источникам
	public $utmCampaignCount; // количество после группировки по кампаниям
	public $recordsCount; // количество записей
    public $recordsDCount; // количество "дошедших" записей
    public $utmContentCount; // количество после группировки по контенту

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		return array(
			array('id, callId, callerNumber, date, duration, phoneNumber, source, medium, keyword, city, url, ref, utmSource, utmMedium, utmCampaign, utmContent, utmTerm, clientId, callClientUniqueId, userAgent, ip, mapVisits, api_record_ids, order, additionalTags, sessionId', 'safe'),
		);
	}

	public function tableName()
	{
		return 'api_record_calltouch';
	}

}