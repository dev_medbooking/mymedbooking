<?php

/**
 * Created by PhpStorm.
 * User: serg
 * Date: 03.02.14
 * Time: 14:05
 */
abstract class AContentModel extends CModel
{

    abstract public function search($domain);

    public function getDomainContent($domain)
    {
        if(empty($domain)){
            return new CArrayDataProvider(array());
        }
        $localDomain='my.lcl';
        if($_SERVER['HTTP_HOST']==$localDomain){
            $localDomains=array(
                'timetovisit.ru'=>'my.lcl/TimeToVisitNew',
                'medbooking.com'=>'my.lcl/medbooking',
            );
            if(strpos($domain->title,$localDomain)===false){
                if(isset($localDomains[$domain->title])==false){
                    $domain->title='example.com';
                } else{
                    $domain->title=$localDomains[$domain->title];
                }
            }
        }
        $url='http://'.$domain->title.'/integration/';
        $result=new CArrayDataProvider($this->getContent($url.$this->entityType));
        $result->setPagination(array('pageSize'=>10));
        return $result;
    }

    private function getContent($url)
    {
        if(empty($url)){
            return null;
        }
        $ch=curl_init($url);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query(array(get_class($this)=>$this->getAttributes())));
        curl_setopt($ch,CURLOPT_HEADER,0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
        $response=curl_exec($ch);
        curl_close($ch);
        $response=preg_replace('/^[^{]+{/','{',$response);
        $data=json_decode($response,true);
        if($data===null||is_array($data)==false){
            return array();
        }
        return $data;
    }

}