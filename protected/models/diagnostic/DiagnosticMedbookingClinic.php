<?php

class DiagnosticMedbookingClinic extends CActiveRecord {

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getDbConnection(){
		return Yii::app()->db2;
	}

	public function tableName(){
		return 'clinic_diagnostic';
	}

	public function relations()
	{
		return array(
			'diagnostic_clinic' => array(self::HAS_ONE, 'IntegratorClinic', 'diagnostica_id'),
			'rating' => array(self::HAS_ONE, 'DiagnosticClinicRating2', 'nid'),
			'clinic_category_s'=>array(self::HAS_MANY,'DiagnosticClinicPrice','clinic_id', 'with' => array('catc_category' => array('condition' => ((!empty($_REQUEST['only_mrt'])) ? 'catc_category.vladimirov_status=1' : '')))),
		);
	}

	public function rules()
	{
		return array(
			array('','safe')
		);
	}

}