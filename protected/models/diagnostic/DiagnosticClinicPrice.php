<?php

class DiagnosticClinicPrice extends CActiveRecord
{
    public function getDbConnection(){
        return Yii::app()->db2;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'clinic_price';
    }


    public function relations()
    {
        return array(
            'catc_clinic'=>array(self::BELONGS_TO,'MedbookingDiagnosticClinic','clinic_id'),
            'catc_service'=>array(self::BELONGS_TO,'DiagnosticClinicPrice','parent_id'),
            'catc_services'=>array(self::HAS_MANY,'DiagnosticClinicPrice','parent_id'),
            'catc_category'=>array(self::BELONGS_TO,'DiagnosticCategoryPrice','category_price_id' ),
        );
    }


}
