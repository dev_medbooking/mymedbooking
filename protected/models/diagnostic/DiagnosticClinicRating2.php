<?php

class DiagnosticClinicRating2 extends CActiveRecord {

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getDbConnection(){
		return Yii::app()->db2;
	}

	public function tableName(){
		return 'clinic_diagnostic_rating2';
	}

	public function relations()
	{
		return array(
			'rating' => array(self::BELONGS_TO, 'DiagnosticMedbookingClinic', 'nid')
		);
	}

	public function rules()
	{
		return array(
			array('rate', 'safe')
		);
	}

}