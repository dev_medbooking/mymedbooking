<?php

class DiagnosticCategoryPrice extends CActiveRecord
{

    public function getDbConnection(){
        return Yii::app()->db2;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'category_price';
    }

    public static function modelName($className=__CLASS__)
    {
        return $className;
    }

    public function relations()
    {
        return array(
            'category_clinic_s'=>array(self::HAS_MANY,'DiagnosticClinicPrice','category_price_id'),
        );
    }

}
