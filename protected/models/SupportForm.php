<?php

class SupportForm extends CFormModel
{

	public $name;
	public $phone;
	public $message;

	public function rules()
	{
		return array(
			array('name, phone, message', 'required'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'name' => 'Имя',
			'phone' => 'Телефон',
			'message' => 'Сообщение',
		);
	}

	public function sendMessage()
	{
        $ss = new Sendsay();
        $ss->auth['one_time_auth'] = array(
            'login'    => 'x_14999756051001138',
            'sublogin' => 'apiuser',
            'passwd'   => 'YHqo6oxHBF'
        );
        $groups = $ss->group_list();
        $group_id = '';
        $group_name = '';
        if(empty($groups['list'])){
            return;
        }

        foreach ($groups['list'] as $group) {
            if($group['name']=='HelpDesk_partners'){
                $group_id = $group['id'];
                $group_name = $group['name'];
            }
        }
        if (!empty($group_id)){
            $ss->group_clean($group_id);
        }else{
            $new_gr = $ss->group_create('HelpDesk_partners');
            $group_id = $new_gr['id'];
            $group_name = 'HelpDesk_partners';
        }
        $subscribe_email_list = ['caption'=>[['anketa'=>'member','quest'=>'email']],'rows'=>[['HelpDesk@medbooking.com']]];
        $gr = ['id'=>$group_id,'name'=>$group_name];
        $ss->member_import($subscribe_email_list,$gr);
        $ss->issue_send($group_id,'mailing@medbooking.com','medbooking','Партнерская программа Medbooking','Имя: ' .$this->name.'<br>'. 'Телефон: ' . $this->phone .'<br>'. 'Сообщение: ' . $this->message,$subscribe_email_list);
return true;

	}


}