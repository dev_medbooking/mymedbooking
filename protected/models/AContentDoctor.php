<?php

/**
 * Created by PhpStorm.
 * User: serg
 * Date: 03.02.14
 * Time: 12:19
 */
class AContentDoctor extends AContentModel
{

    protected $entityType='doctors';
    public $id;
    public $fio;
    public $status;
    public $author;
    public $rating;
    public $is_desc;
    public $is_img;
    public $is_exp;
    public $is_mtitle;
    public $is_mdesc;
    public $is_mkeys;
    public $is_clinic;
    public $is_subway;
    public $reviews;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('id,fio,status,author,rating,is_desc,is_img,is_exp,is_mtitle,is_mdesc,is_mkeys,is_clinic,is_subway,reviews','safe'),
            array('id,fio,status,author,rating,is_desc,is_img,is_exp,is_mtitle,is_mdesc,is_mkeys,is_clinic,is_subway,reviews','safe','on'=>'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'title'=>'Наименование',
            'status'=>'Статус',
        );
    }

    public function search($domain)
    {
        return $this->getDomainContent($domain);
    }

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array('id','fio','status','author','rating','is_desc','is_img','is_exp','is_mtitle','is_mdesc','is_mkeys','is_clinic','is_subway','reviews');
    }

}