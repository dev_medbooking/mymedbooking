<?php

Yii::import('application.models._base.BaseApiRecordTimeslot');

class ApiRecordTimeslot extends BaseApiRecordTimeslot
{
	/**
	 * @param string $className
	 *
	 * @return ApiRecordTimeslot
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return array_merge(parent::rules(), []);
	}

	/**
	 * @inheritdoc
	 */
	public function relations()
	{
		return array_merge(parent::relations(), []);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return array_merge(parent::attributeLabels(), []);
	}
}