<?php

class IntegratorLaws extends CActiveRecord
{

	public $count; // общая сумма записей
	public $patient_record; // общая сумма пациентов
	public $patient_count; // общая сумма записанных пациентов
	public $patient_success; // общая сумма дошедших пациентов
	public $patient_repeat; // общая сумма повторных пациентов
	public $patient_diag; // общая сумма диаг пациентов
	public $patient_analyz; // общая сумма пациентов на анализы
	public $patient_insurance; // общая сумма пациентов на анализы
	public $summ; // общая сумма счетов
	public $summ_diag; // общая сумма счетов диаг
	public $summ_analyz; // общая сумма счетов анализы
	public $group_status; // группировка статусов
	public $paymentsRating; // рейтинг по оплатам
	public $agreeRating; // рейтинг по согласованию

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		return array(
			array('create_time, title, name, title_company, agreement, comment, duplicate, graduation_all, expensive_services_agreement', 'safe'),
			array('uid', 'numerical', 'integerOnly' => true),
			array('numb_agree, address, agreement', 'safe'),
		);
	}


	public function tableName()
	{
		return 'integrator_clinic_all';
	}

	public function relations()
	{
		return array(
			'clinics' => array(self::HAS_MANY, 'IntegratorClinics', 'all_id'),
			'law_reports' => array(self::HAS_MANY, 'IntegratorLawReports', 'law_id'),
			'law_report' => array(self::HAS_ONE, 'IntegratorLawReports', 'law_id'),
			'iu' => array(self::BELONGS_TO, 'MbUser', 'uid'),
			'law_graduation' => array(self::HAS_MANY, 'IntegratorLawGraduation', 'law_id'),
			'graduation' => array(self::BELONGS_TO, 'IntegratorLawGraduation', 'law_id'),
		);
	}

	public function getLawBalance($id = NULL)
	{
		if ( ! $id) $id = $this->primaryKey;
		$query['reportsSumm'] = Yii::app()->db->createCommand("SELECT IFNULL(SUM(summ), 0) FROM integrator_law_reports WHERE law_id = :id")
			->queryScalar(array(
				':id' => $id,
			));
		$query['paymentsReportsSumm'] = Yii::app()->db->createCommand("SELECT IFNULL(SUM(summ), 0) FROM integrator_law_reports_payments WHERE report_id IN (SELECT id FROM integrator_law_reports WHERE law_id = :id)")
			->queryScalar(array(
				':id' => $id,
			));
		$query['balance'] = $query['paymentsReportsSumm'] - $query['reportsSumm'];
		return $query;
	}

	static public function updateLawFields($id)
	{
		$query = Yii::app()->db->createCommand("
				SELECT
					IFNULL(SUM(summ), 0) AS summ,
					IFNULL(SUM(patient_record), 0) AS patient_record,
					IFNULL(SUM(patient_count), 0) AS patient_count,
					IFNULL(SUM(patient_repeat), 0) AS patient_repeat,
					IFNULL(SUM(patient_insurance), 0) AS patient_insurance,
					IFNULL(SUM(patient_success), 0) AS patient_success,
					IFNULL(SUM(patient_diag), 0) AS patient_diag,
					IFNULL(SUM(patient_analyz), 0) AS patient_analyz,
					IFNULL(SUM(summ_diag), 0) AS summ_diag,
					IFNULL(SUM(summ_analyz), 0) AS summ_analyz
				FROM integrator_clinic_reports
				WHERE law_report_id = :id")
			->queryRow(TRUE, array(
				':id' => $id,
			));
		if ( ! empty($query)) {
			$model = IntegratorLawReports::model()->findByPk($id);
			foreach ($query as $key => $value)
				$model->$key = $value;
			$model->save();
			return $model;
		}
	}

	public function getGraduation($price, $type)
	{
		$query = Yii::app()->db->createCommand("SELECT IFNULL(count, 0) FROM integrator_law_graduation WHERE law_id = '".$this->primaryKey."' AND price = '".$price."' AND type = '".$type."'")->queryScalar();
		if (empty($query)) return '';
		return $query;
	}

	public function getSupposedPayment($rating)
	{
		$queryDate = new DateTime();
		$queryDate = $queryDate->sub(new DateInterval('P1M'))->format('Y-m');
		$query = Yii::app()->db->createCommand("
			SELECT
				IFNULL(summ, 0) AS sum,
				status,
				'".$rating."' AS rating
			FROM integrator_law_reports
			WHERE
				law_id = '".$this->primaryKey."'
				AND report_date LIKE '%".$queryDate."%'")
			->queryRow();
		$return = array();
		$return['sum'] = 0;
		$return['status'] = 0;
		if ($this->paymentsRating <= 6 AND $this->paymentsRating > 5 AND $rating == 6) {
			return (( ! empty($query) AND $query['rating'] == 6) ? $query : $return);
		} else if ($this->paymentsRating <= 5 AND $this->paymentsRating > 4 AND $rating == 5) {
			return (( ! empty($query) AND $query['rating'] == 5) ? $query : $return);
		} else if ($this->paymentsRating <= 4 AND $this->paymentsRating > 3 AND $rating == 4) {
			return (( ! empty($query) AND $query['rating'] == 4) ? $query : $return);
		} else if ($this->paymentsRating <= 3 AND $this->paymentsRating > 2 AND $rating == 3) {
			return (( ! empty($query) AND $query['rating'] == 3) ? $query : $return);
		} else if ($this->paymentsRating <= 2 AND $this->paymentsRating > 1 AND $rating == 2) {
			return (( ! empty($query) AND $query['rating'] == 2) ? $query : $return);
		} else if ($this->paymentsRating <= 1 AND $this->paymentsRating >= 0 AND $rating == 1) {
			return (( ! empty($query) AND $query['rating'] == 1) ? $query : $return);
		} else return $return;
	}

	public function getSupposedAgree($rating)
	{
		$queryDate = new DateTime();
		$queryDate = $queryDate->sub(new DateInterval('P1M'))->format('Y-m');
		$return = array();
		$return['sum'] = 0;
		$return['status'] = 0;
		$agreeStatusArray = array(
			IntegratorLawReports::STATUS_BILL,
			IntegratorLawReports::STATUS_PAY,
			IntegratorLawReports::STATUS_PART,
			IntegratorLawReports::STATUS_DEBT,
		);
		$query = Yii::app()->db->createCommand("
			SELECT
				'1' AS sum,
				ilr.status,
				'".$rating."' AS rating
			FROM integrator_law_reports AS ilr
			LEFT JOIN integrator_law_reports_status AS ilrs ON ilrs.report_id = ilr.id
			WHERE
				ilr.law_id = '".$this->primaryKey."'
				AND ilrs.status IN (".implode(',', $agreeStatusArray).")
				AND ilr.report_date LIKE '%".$queryDate."%'
				AND ilrs.status IS NOT NULL
				GROUP BY ilr.id")
			->queryRow();
		if ($this->agreeRating <= 6 AND $this->agreeRating > 5 AND $rating == 6) {
			$return['sum'] = '1';
			return (( ! empty($query) AND $query['rating'] == 6) ? $query : $return);
		} else if ($this->agreeRating <= 5 AND $this->agreeRating > 4 AND $rating == 5) {
			$return['sum'] = '1';
			return (( ! empty($query) AND $query['rating'] == 5) ? $query : $return);
		} else if ($this->agreeRating <= 4 AND $this->agreeRating > 3 AND $rating == 4) {
			$return['sum'] = '1';
			return (( ! empty($query) AND $query['rating'] == 4) ? $query : $return);
		} else if ($this->agreeRating <= 3 AND $this->agreeRating > 2 AND $rating == 3) {
			$return['sum'] = '1';
			return (( ! empty($query) AND $query['rating'] == 3) ? $query : $return);
		} else if ($this->agreeRating <= 2 AND $this->agreeRating > 1 AND $rating == 2) {
			$return['sum'] = '1';
			return (( ! empty($query) AND $query['rating'] == 2) ? $query : $return);
		} else if ($this->agreeRating <= 1 AND $this->agreeRating >= 0 AND $rating == 1) {
			$return['sum'] = '1';
			return (( ! empty($query) AND $query['rating'] == 1) ? $query : $return);
		} else return $return;
	}
}
