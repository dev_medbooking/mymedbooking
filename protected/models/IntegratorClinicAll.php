<?php

class IntegratorClinicAll extends CActiveRecord
{

    public $email_callcent;
    public $status;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public $queryDate='';
    public $queryDateEnd='';

    public function getDateForQuery()
    {
        $_date=date('Y-m-d');
        if(!empty($this->queryDate)){
            $_date=$this->queryDate;
        }
        return $_date;
    }

    public function getDateEndForQuery()
    {
        $_date = new DateTime($this->getDateForQuery());
        $_date->modify('last day of this month');
        $_date = $_date->format('Y-m-d');
        if(!empty($this->queryDateEnd)){
            $_date=$this->queryDateEnd;
        }
        return $_date;
    }

    public function getColumnForDateQuery()
    {
        // fixme - Пофиксить $_REQUEST в модели
        if (isset($_REQUEST['queryDate']) OR isset($_REQUEST['date'])) {
            if (isset($_REQUEST['status'])) {
                switch ($_REQUEST['status']) {
                    case 7:
                        return 'date_status_7';
                        break;
                    case 4:
                        return 'date_status_4';
                        break;
                    case 3:
                        return 'date_status_3';
                        break;
                    case 2:
                        return 'date_status_2';
                        break;
                    default:
                        return 'create_time';
                        break;
                }
            } else {
                return 'create_time';
            }
        } else {
            return 'create_time';
        }
    }

    public function tableName()
    {
        return 'integrator_clinic_all';
    }

    public function getCountKollAll()
    {
        $status=1;
        if(!empty($this->status)){
            $status='status='.$this->status;
        }
        return $query=Yii::app()->db->createCommand('SELECT IFNULL(SUM(koll_come),0) AS koll_come , IFNULL(SUM(koll_record),0) AS koll_record ,IFNULL(SUM(koll_success),0) AS koll_success , IFNULL(SUM(all_price),0) AS all_price , IFNULL(SUM(koll_diagnostik),0) AS diagnostik , IFNULL(SUM(koll_repeat),0) AS repeat_koll , IFNULL(SUM(diagnostik_price),0) AS d_price FROM integrator_clinic_price WHERE DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") <= "'.$this->getDateEndForQuery().'" AND '.$status)->queryRow();
    }

    public function getPrice4()
    {
        $query='';
        if(empty($this->status)||$this->status==4){
            return $query=Yii::app()->db->createCommand('SELECT IFNULL(SUM(all_price), 0) FROM integrator_clinic_price WHERE DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") <= "'.$this->getDateEndForQuery().'" AND status=4')->queryScalar();
        }
        return $query;
    }

    public function getPriceNo4()
    {
        $query='';
        if(empty($this->status)){
            return $query=Yii::app()->db->createCommand('SELECT IFNULL(SUM(all_price), 0) FROM integrator_clinic_price WHERE DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") <= "'.$this->getDateEndForQuery().'" AND status!=4')->queryScalar();
        } else if(!empty($this->status)&&$this->status!=4){
            return $query=Yii::app()->db->createCommand('SELECT IFNULL(SUM(all_price), 0) FROM integrator_clinic_price WHERE DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") <= "'.$this->getDateEndForQuery().'" AND status='.$this->status)->queryScalar();
        }
        return $query;
    }

    public function getCountStatus4()
    {
        $query='';
        if(empty($this->status)||$this->status==4){
            return $query=Yii::app()->db->createCommand('SELECT COUNT(*) FROM integrator_clinic_price WHERE DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") <= "'.$this->getDateEndForQuery().'" AND status=4')->queryScalar();
        }
        return $query;
    }

    public function getCountStatusAll()
    {
        $status=1;
        if(!empty($this->status)){
            $status='status='.$this->status;
        }
        return $query=Yii::app()->db->createCommand('SELECT COUNT(*) FROM integrator_clinic_price WHERE DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") <= "'.$this->getDateEndForQuery().'" AND '.$status)->queryScalar();
    }

    public function getCountClinic()
    {
        $query=Yii::app()->db->createCommand('SELECT  COUNT(*) FROM integrator_clinic WHERE all_id='.$this->primaryKey)->queryScalar();
        return $query;
    }

    public function getCountAll()
    {
        $_clinicArr=Yii::app()->db->createCommand('SELECT id FROM integrator_clinic WHERE all_id='.$this->primaryKey)->queryColumn();
        $query=0;
        if(!empty($_clinicArr)){
            $query=Yii::app()->db->createCommand('SELECT SUM(koll_come) AS koll_come , SUM(koll_record) AS koll_record ,SUM(koll_success) AS koll_success , SUM(all_price) AS all_price , SUM(koll_diagnostik) AS diagnostik , SUM(koll_repeat) AS repeat_koll , SUM(diagnostik_price) AS d_price FROM integrator_clinic_price WHERE integrator_id IN('.implode(',',$_clinicArr).') AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") <= "'.$this->getDateEndForQuery().'"')->queryRow();
        }
        return $query;
    }

    /* показывает в какое время текущего статуса заявки */
    public function getAllKollPrice()
    {
        $title=1;
        $title_company=1;
        if(!empty($this->title)){
            $title='ica.title LIKE"%'.$this->title.'%"';
        }
        if(!empty($this->title_company)){
            $title_company='ica.title_company LIKE"%'.$this->title_company.'%"';
        }
        return $query=Yii::app()->db->createCommand('SELECT SUM(icp.all_price) FROM integrator_clinic_price AS icp LEFT JOIN integrator_clinic AS ic ON(icp.integrator_id=ic.id) LEFT JOIN `integrator_clinic_all` AS ica ON(ic.all_id=ica.id) WHERE '.$title.' AND '.$title_company.' AND DATE_FORMAT(icp.create_time, "%Y-%m") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT(icp.create_time, "%Y-%m") <= "'.$this->getDateEndForQuery().'"')->queryScalar();
    }

    /* показывает в какое время текущего статуса заявки */
    public function getStatusDate()
    {
        $query='';
        if(!empty($this->statusInteger())){
            $_clinicArr=Yii::app()->db->createCommand('SELECT id FROM integrator_clinic WHERE all_id='.$this->primaryKey)->queryColumn();
            if(!empty($_clinicArr)){
                $query=Yii::app()->db->createCommand('SELECT date_status_'.$this->statusInteger().' FROM integrator_clinic_price WHERE integrator_id IN('.implode(',',$_clinicArr).') AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") <= "'.$this->getDateEndForQuery().'"')->queryScalar();
            }
        }
        return $query;
    }

    /* показывает в какое время сделали  в заявке, в зависимости от статуса */
    public function statusDateQuery($status)
    {
        $query='';
        if(!empty($this->statusInteger())){
            $_clinicArr=Yii::app()->db->createCommand('SELECT id FROM integrator_clinic WHERE all_id='.$this->primaryKey)->queryColumn();
            if(!empty($_clinicArr)){
                $query=Yii::app()->db->createCommand('SELECT date_status_'.$status.' FROM integrator_clinic_price WHERE integrator_id IN('.implode(',',$_clinicArr).') AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") <= "'.$this->getDateEndForQuery().'"')->queryScalar();
            }
        }
        if($query=='0000-00-00'){
            $query='';
        }
        return $query;
    }

    /* показывает тесктовый статус в заявке */
    public function getStatusText()
    {
        $status='';
        if($this->statusInteger()==1){
            $status='Отчет';
        } elseif($this->statusInteger()==2){
            $status='Согл.';
        } elseif($this->statusInteger()==3){
            $status='Счет';
        } elseif($this->statusInteger()==4){
            $status='Оплатили';
        }elseif($this->statusInteger()==5){
            $status='Частично';
        }elseif($this->statusInteger()==6){
            $status='Дебиторка';
        }elseif($this->statusInteger()==7){
            $status='Отказ';
        }elseif($this->statusInteger()==11){
            $status='Перезапись';
        }
        return $status;
    }

    public function rules()
    {
        return array(
            array('create_time, title, name, title_company, agreement, comment','safe'),
            array('uid','numerical','integerOnly'=>true),
            array('numb_agree, address, agreement','safe'),
        );
    }

    /**
     * Связт с другими моделями
     * @return type
     */
    public function relations()
    {
        return array(
            'clinics'=>array(self::HAS_MANY,'IntegratorClinic','all_id'),
            'iu'=>array(self::BELONGS_TO,'MbUser','uid'),
        );
    }

    /* показывает числовой статус в заявке */
    public function statusInteger()
    {
        $_clinicArr=Yii::app()->db->createCommand('SELECT id FROM integrator_clinic WHERE all_id='.$this->primaryKey)->queryColumn();
        $query=0;
        if(!empty($_clinicArr)){
            $query=Yii::app()->db->createCommand('SELECT status FROM integrator_clinic_price WHERE integrator_id IN('.implode(',',$_clinicArr).') AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") >= "'.$this->getDateForQuery().'" AND DATE_FORMAT('.$this->getColumnForDateQuery().', "%Y-%m-%d") <= "'.$this->getDateEndForQuery().'"')->queryScalar();
        }
        return $query;
    }

}
