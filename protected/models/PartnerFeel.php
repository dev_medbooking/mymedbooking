<?php

class PartnerFeel extends CActiveRecord
{


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'partner_feel';
    }

    public function rules()
    {
        return array(
            array('partner_id','required','message'=>'Укажите партнера'),
            array('create_time, id , note','safe'),
            array('price','type','type'=>'float'),
            array('partner_id','exist','attributeName'=>'uid','className'=>'MbUser'),
            array('status ,id','numerical','integerOnly'=>true),
        );
    }
    /**
     * Связи с другими моделями
     * @return type
     */
    public function relations()
    {
        return array(
            'pfp'=>array(self::BELONGS_TO,'Partner','partner_id'),
        );
    }
    
    public function attributeLabels() {
        return array(
            'id'=>'ID',
            'partner_id' => 'Партнер',
            'price' => 'Цена',
            'status' => 'Статус',
            'create_time'=>'Дата создания',
            'note' => 'Коммент',
        );
    }

}
