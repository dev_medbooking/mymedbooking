<?php

class MbRecordStep1 extends CActiveRecord
{

    public $sogl;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'api_record';
    }

    public function beforeSave()
    {
        return false;
    }

    public function rules()
    {
        return array(
            array('reception_time','required','on'=>'step1'),
            array('reception_time','type','type'=>'datetime','allowEmpty'=>true,'datetimeFormat'=>'yyyy-mm-dd hh:mm:ss','on'=>'step1'),
            array('sogl','compare','compareValue'=>true,'on'=>'step2','message'=>'Соглашение обязательно требует подтверждения'),
            array('gender','in','range'=>array(1,2),'on'=>'step2'),
            array('name, gender, sogl','required','on'=>'step2'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'=>'ID',
            'gender'=>'Пол',
            'sogl'=>'Соглашение',
            'name'=>'ФИО',
            'email'=>'E-mail',
            'telephone'=>'Телефон',
            'uid'=>'Клиент',
            'description'=>'Сообщение',
            'status'=>'Статус',
            'new_pacient'=>'Новый пациент',
            'status_time'=>'Время смены статуса',
            'reception_time'=>'На время',
            'timeslot_id'=>'Таймслот',
            'doctor_id'=>'Доктор',
            'clinic_id'=>'Клиника',
            'author_id'=>'Автор',
            'create_time'=>'Создано',
            'update_time'=>'Редактировано',
            'callproove'=>'Запись на колцентр',
            'userphone'=>'Телефон пользователя',
            'userproove'=>'Создание пользователя',
            'timecode'=>'Код из СМС',
            'timesession'=>'Код возврата',
        );
    }

    public function beforeValidate()
    {
        if(!empty($this->reception_time)&&empty($this->timeslot_id)&&$this->scenario!='admin'){
            $_time=strtotime($this->reception_time);
            $this->reception_time=date("Y-m-d H:i:s",$_time);
        }
        return parent::beforeValidate();
    }

}