<?php

class IntegratorLawReportsPayments extends CActiveRecord
{

	const PAYMENT = 1;
	const PAYMENT_DEBT = 2;
	const PAYMENT_PART = 3;

	static public $paymentText = array(
		self::PAYMENT => 'Оплата',
		self::PAYMENT_DEBT => 'Оплата по дебиторке',
		// self::PAYMENT_PART=> 'Частичная'
	);

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'integrator_law_reports_payments';
	}

	public function relations()
	{
		return array(
			'law_report_payments' => array(self::BELONGS_TO, 'IntegratorLawReports', 'report_id'),
		);
	}

	public function rules()
	{
		return array(
			array('payment_date, summ','safe'),
		);
	}

	/**
	 * Возвращает текстовый статус
	 * @param $status
	 * @return string
	 */
	public function getReportStatusText($status = 0)
	{
		if (isset(IntegratorLawReports::$statusText[$status]))
			return IntegratorLawReports::$statusText[$status];
		else return '';
	}
}
