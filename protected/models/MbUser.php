<?php

class MbUser extends CActiveRecord
{

    public $verifyPassword;
	public $agreement;
    public $city;
    private $_role;

    public function getRole()
    {
        if(empty($this->_role)){
            $this->_role='Guest';
            if(!$this->isNewRecord){
                $_role=Yii::app()->db->createCommand("SELECT * FROM auth_assignment WHERE userid={$this->primaryKey}")->queryRow();
                if(!empty($_role)) $this->_role=$_role['itemname'];
            }
        }
        return $this->_role;
    }

    public function setRole($value)
    {
        $this->_role=$value;
    }

    public function getIsLimited()
    {
        return !$this->domainPhone || strpos($this->domainPhone->phone, '0') === 0;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if(empty($this->password)) $this->password=MbController::generate(6,2);
        if(!empty($this->telephone)) $this->telephone=str_replace(array("+7","(",")"," ","-"),array("","","","",""),$this->telephone);
        return parent::beforeValidate();
    }

    public function afterSave()
    {
        if(!empty($this->_role)){
            Yii::app()->db->createCommand("DELETE FROM auth_assignment WHERE userid={$this->uid}")->execute();
            $auth=Yii::app()->authManager;
            $auth->assign($this->_role,$this->uid);
        }

        return parent::afterSave();
    }

    public function tableName()
    {
        return 'auth_user';
    }

    public function rules()
    {
        return array(
            array('password, telephone, email, fio','required'),
			array('verifyPassword','compare','compareAttribute'=>'password','on'=>'registration','message'=>'Пароли не совпадают.'),
			array('agreement', 'required', 'on'=>'registration', 'message'=>'Необходимо принять соглашение'),
            array('role','required','message'=>'Роль должна быть определена'),
            array('telephone, nameperson, fio, lastvisit','length','max'=>255),
            array('telephone','length','min'=>10,'max'=>10),
            array('telephone, apicall','numerical','integerOnly'=>true),
            array('gender','numerical','integerOnly'=>true),
            array('telephone','default','setOnEmpty'=>true,'value'=>null),
            array('email','email','message'=>'Не правильный формат email'),
            array('email','unique','message'=>'Пользователь с таким email уже зарегистрирован'),
            array('telephone','unique','message'=>'Пользователь с таким telephone уже зарегистрирован'),
            array('password, activekey','length','max'=>100),
            array('telephone,color','safe'),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior'=>array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'createAttribute'=>'create_time',
                'updateAttribute'=>'update_time',
            ),
        );
    }

    public function relations()
    {
        return array(
            'user_call_s1'=>array(self::HAS_MANY,'MbCall','uid'),
            'user_call_s2'=>array(self::HAS_MANY,'MbCall','author_id'),
            'user_record_s1'=>array(self::HAS_MANY,'MbRecord','uid'),
            'user_record_s2'=>array(self::HAS_MANY,'MbRecord','author_id'),
            'user_record_s3'=>array(self::HAS_MANY,'MbRecord','first_uid'),
            'user_sms_s'=>array(self::HAS_MANY,'MbRecordSms','author_id'),
	        'user_clinic_price'=>array(self::HAS_MANY,'ClinicPrice','user_id'),
	        'user_history' => array(self::HAS_ONE, 'MbRecordHistory', 'uid'),
            'domainPhone' => [self::HAS_ONE, 'MbDomainPhone', 'partner_id']
        );
    }

    public function attributeLabels()
    {
        return array(
            'uid'=>'ID',
            'role'=>'Роль',
            'password'=>'Пароль',
            'activekey'=>'Активация',
            'update_time'=>'Редактирование',
            'create_time'=>'Создание',
            'telephone'=>'Телефон',
            'lastvisit'=>'Визит',
            'email'=>'E-mail',
            'gender'=>'Пол',
            'nameperson'=>'Логин',
            'fio'=>'ФИО',
            'image'=>'Фото',
            'apicall'=>'Статус оператора Call-центра',
        );
    }
}