<?php


class CHttpForbidden extends CHttpException
{
    public function __construct($message = "Forbidden", $code = 403, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}