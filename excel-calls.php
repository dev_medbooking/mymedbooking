<?php


define('YII_DEBUG', true);

require_once 'framework/yii.php';

new CWebApplication(CMap::mergeArray(call_user_func(function() {
    if(is_file('protected/config/db.php')) {
        require 'protected/config/db.php';
        return $config;
    }
    else {
        require 'protected/config/main.php';
        return ['components' => ['db' => $config['components']['db']]];
    }
}), [
    'basePath' => __DIR__ . '/protected',
    'language' => 'ru',
    'timezone' => 'Europe/Moscow'
]));

spl_autoload_unregister(['YiiBase', 'autoload']);
require_once 'protected/vendors/phpexcel/PHPExcel.php';
spl_autoload_register(['YiiBase', 'autoload']);

if((!isset($_SERVER['PHP_AUTH_USER']) or $_SERVER['PHP_AUTH_USER'] !== '111') or (!isset($_SERVER['PHP_AUTH_PW']) or $_SERVER['PHP_AUTH_PW'] !== '222')) {
    header('WWW-Authenticate: Basic realm="Авторизуйтесь"');
    header('HTTP/1.0 401 Unauthorized');
    die('Ну и не надо! :-(');
}

$file = CUploadedFile::getInstanceByName('excel');

if(($file === null)) {
    echo <<<HTML
<div style="text-align: center">
<form action="" method="post" enctype="multipart/form-data">
<input type="file" name="excel"><input type="submit" value="Загрузить">
</form>
</div>
HTML;
exit;
}

$filePath = Yii::app()->runtimePath . '/' . $file->name;

$file->saveAs($filePath);


/**
 * @var PHPExcel $excel
 */
$excel = PHPExcel_IOFactory::createReaderForFile($filePath)->load($filePath);
$sheet = $excel->getSheet();

for($i = 0; $i <= 200; $i ++) {
    switch(mb_strtolower(trim($sheet->getCellByColumnAndRow($i)->getValue()), 'utf-8')) {
        case 'звонивший':
            $phoneColumn = $i;
            break;
        case 'дата':
            $dateColumn = $i;
            break;

        case 'статус записи':
            $statusColumn = $i;
        break;

        case 'клиника':
            $clinicColumn = $i;
            break;

        case 'примечание':
            $noteColumn = $i;
            break;
        case 'номер заявки':
            $idColumn = $i;
            break;

    }
}

if(!isset($phoneColumn)) {
    echo <<<HTML
<script type="text/javascript">
alert('Не найден столбец "Телефон"');
window.location.href = '{$_SERVER['PHP_SELF']}';
</script>
HTML;
    exit;
}

if(!isset($dateColumn)) {
    echo <<<HTML
<script type="text/javascript">
alert('Не найден столбец "Дата"');
window.location.href = '{$_SERVER['PHP_SELF']}';
</script>
HTML;
    exit;
}

if(!isset($statusColumn)) {
    echo <<<HTML
<script type="text/javascript">
alert('Не найден столбец "Статус записи"');
window.location.href = '{$_SERVER['PHP_SELF']}';
</script>
HTML;
    exit;
}

if(!isset($clinicColumn)) {
    echo <<<HTML
<script type="text/javascript">
alert('Не найден столбец "Название клиники"');
window.location.href = '{$_SERVER['PHP_SELF']}';
</script>
HTML;
    exit;
}

$command = Yii::app()->db->createCommand()
    ->select([
        'status',
        'clinic_name clinic',
        'note',
        'id'
    ])
    ->from('api_record')
    ->where([
        'AND',
        'DATE(create_time) = :date',
        [
            'OR',
            'telephone = :phone',
            'telephone = CONCAT(:plus, :phone)',
            'telephone = CONCAT(:seven, :phone)',
            'telephone = CONCAT(:eight, :phone)',
            'telephone = CONCAT(:plus, :seven, :phone)',
            'telephone = CONCAT(:plus, :eight, :phone)'
        ]
    ], [
        ':plus' => '+',
        ':seven' => '7',
        ':eight' => '8'
    ])
    ->order('create_time DESC')
;

for($i = 2; $i <= $sheet->getHighestRow(); $i ++) {
    $phone = ltrim($sheet->getCellByColumnAndRow($phoneColumn, $i)->getValue(), '78');
    $date  = (new DateTime(trim($sheet->getCellByColumnAndRow($dateColumn, $i)->getValue(), '\'')))->format('Y-m-d');

    $records = $command->queryAll(true, [
        ':phone' => $phone,
        ':date' => $date
    ]);



    if(count($records) === 1) {
        $status = $records[0]['status'];
        $clinic = $records[0]['clinic'];
        $note=$records[0]['note'];
        $id=$records[0]['id'];
    }
    else if(count($records) > 1) {
        $statusFound = false;

        foreach($records as $record) {
//            if(in_array($record['status'], [3, 5])) {
                $status = $record['status'];
                $clinic = $record['clinic'];
                $statusFound = true;
                $note=$record['note'];
                $id=$record['id'];
//                break;
//            }
        }

        if($statusFound === false) {
            $status = $records[0]['status'];
            $clinic = $records[0]['clinic'];
        }
    }
    else {
        #echo $phone . ' - ' . $date . '<br/>';
         continue;
    }



    $sheet->getCellByColumnAndRow($statusColumn, $i)->setValue($status);
    $sheet->getCellByColumnAndRow($clinicColumn, $i)->setValue($clinic);
    $sheet->getCellByColumnAndRow($noteColumn, $i)->setValue($note);
    $sheet->getCellByColumnAndRow($idColumn, $i)->setValue($id);
}

header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=" . $file->name);
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private", false);

(new PHPExcel_Writer_Excel5($excel))->save('php://output');

unlink($filePath);
