var apiPartner = (function () {

    var createExel = function () {
        $('body').on('click', '.js-exel-button', function () {
            window.open($(this).attr('data-href') + '&' + $('.js-search-form').serialize(), '_blank');
            return false;
        });
    };

    var calendarStart = function () {
        $('#js-date-start, #js-date-end').datepicker({
            lang: "ru",
            dateFormat: "yy/mm/dd"
        });
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'yyyy/mm/dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Текущее время',
            closeText: 'Закрыть',
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);

    };

    var searchInput = function () {
        $('body').on('keyup', '.js-search-form input', function () {
            $.ajax({
                url: $('.js-search-form').attr('action'),
                type: 'POST',
                data: $('.js-search-form').serialize(),
                dataType: "json",
                async: true,
                success: function (r) {
                    if (r.tbody) {
                        $('.js-update-data').html(r.tbody);
                    } else {
                        $('.js-update-data').html('');
                    }
                    if (r.pages) {
                        $('#pages').html(r.pages);
                    } else {
                        $('#pages').html('');
                    }
                }

            });
            return false;
        });
    };

    var support = function () {
        $('body').on('click', '.btn_support', function () {
            $.ajax({
                url: $(this).attr('data-href'),
                type: 'POST',
                dataType: "json",
                async: true,
                success: function (data) {
                    if (location.href.split("/")[3] === "mbTable") {
                        $("body").append("<div class='mbTable__popup'></div>");
                        $(".mbTable__popup").append(data.html);
                        $("body").append("<div class='overlay_popup'></div>");
                    } else {
                        $("body").append(data.html);
                        $("body").append("<div class='overlay_popup'></div>");
                    }

                }
            });

            return false;
        });
    };
    var changeSelect = function () {
        $('body').on('change', '.js-select-box', function () {
            window.location.href = $('.js-select-box option:selected').val();
        });
    };

    return {
        cs: changeSelect,
        ce: createExel,
        calendarStart: calendarStart,
        si: searchInput,
        su: support
    }

}());

jQuery(function ($) {


    $("body").on("focus", "#SupportForm_phone", function () {
        $("#SupportForm_phone").mask("+7(999) 999-99-99");
    });
    $("#js-date-start, #js-date-end").mask("9999/99/99");
    $("#MbUser_telephone, #SupportForm_phone").mask("+7(999) 999-99-99");
    $("#tin").mask("99-9999999");
    $("#ssn").mask("999-99-9999");
    if ($("#Partner_payment_type option:selected").val() === "1") {
        $(".mask").addClass("maskR")
        $(".maskR").mask("R999999999999");
    }
    $("body").on("focusin", ".cpa_form_support textarea", function () {
        $(this).animate({height: "200px"}, 500);
    });
    try {
        ymaps.ready(init);
    } catch (err) {

    }

    function init() {
        ymaps.geolocation.get({
            provider: 'yandex',
            mapStateAutoApply: true
        }).then(function (result) {
            $('input[name="loc"]').val(result.geoObjects.position)
        });

        ymaps.geolocation.get({
            provider: 'browser',
            mapStateAutoApply: true
        }).then(function (result) {
            $('input[name="loc"]').val(result.geoObjects.position)
        });
    }


    $("body").on("focusout", ".cpa_form_support textarea", function () {
        $(this).animate({height: "45px"}, 500);
    });

    $('a[data-type="edit-input"]').on('click', function (e) {
        e.preventDefault();
        var input = $('#' + $(this).data('target'));
        input.prop('disabled', function (i, v) {
            return !v;
        });
        if (input.closest('li').hasClass('active')) {
            $.ajax({
                url: input.data('href'),
                type: 'POST',
                data: {
                    field: $(this).data('target'),
                    value: input.val()
                },
                dataType: "json",
                async: true,
                success: function (r) {
                    if (r.success) {
                        alert('Данные изменены.');
                    }
                }

            });
        }
        input.closest('li').toggleClass('active norm');
    });
    apiPartner.ce();
    apiPartner.calendarStart();
    apiPartner.si();
    apiPartner.cs();
    apiPartner.su();

});

$(function () {
    $("body").on("click", ".popup-reg-js", function () {
        if (!$(this).hasClass("active")) {
            $(this).addClass("active");
            $("#popup_reg").show();
            $("body").append("<div class='overlay_agreement'></div>");
            return false;
        }
    });
    $("body").on("click", ".overlay_agreement, .popup_head i", function () {
        $(".overlay_agreement").remove();
        $("#popup_reg").hide();
        $(".popup-reg-js").removeClass("active");
        return false;
    });

    $("body").on("click", "#admin-form input[type='submit']", function () {
        var check = true;
        $('#admin-form .request').each(function () {
            if (!$(this).val()) {
                check = false;
            }
        })
        if (!check) {
            return false;
        } else {
            $.ajax({
                url: $("#admin-form").attr("action"),
                type: 'POST',
                data: $("#admin-form").serialize(),
                success: function () {
                    $("body").append("<div class='success_popup'><p>Спасибо!</p></div>");
                    $(".mbTable__popup .form_content form").hide();
                    setTimeout(function () {
                        $(".success_popup, .overlay_popup").remove();
                    }, 1000);
                }
            });
            $(".mbTable__popup").remove();
            return false;
        }
    });
    $("body").on("click", ".overlay_popup, .close_support", function () {
        $(".overlay_popup").remove();
        if ($(".mbTable__popup").length) {
            $(".mbTable__popup").remove();
        }
        if ($(".support_form").length) {
            $(".mbTable__popup").remove();
        }
    });
    var validateEmail = function (email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
    $("body").on("change", "#Partner_payment_type", function () {
        console.log(typeof $(this).val());
        if ($(this).val() === "1") {
            $(".maskR").mask("R999999999999");
        } else {
            $(".mask").removeClass("maskR");
            $(".mask").mask("Р/c99999999999999999999");
        }
    });
    $("body").on("click", ".cpa_form_register input[type='submit']", function () {
        var check = true;
        $('.is_error').hide();
        $('.preview_text').show();
        $('.request').each(function () {
            if (!$(this).val() || !validateEmail($(".mail_request").val())) {
                $(this).next().show();
                $(this).next().next().next().hide();
                check = false;
            }
        });
        if (!check) {
            return false;
        }
    });
    $(document).ready(function () {
        var touch = $('#touch-menu');
        var menu = $('.nav');

        $(touch).on('click', function (e) {
            $(this).toggleClass("active");
            e.preventDefault();
            menu.slideToggle();

        });

    });
    $(document).ready(function () {
        var touch = $('#touch-menu-status');
        var menu = $('.menu_left');

        $(touch).on('click', function (e) {
            $(this).toggleClass("active");
            e.preventDefault();
            menu.slideToggle();

        });

    });
    $(document).ready(function () {
        var touch = $('#touch-menu-times');
        var menu = $('.sort_times');

        $(touch).on('click', function (e) {
            $(this).toggleClass("active");
            e.preventDefault();
            menu.slideToggle();

        });

    });
    $(document).ready(function () {
        var touch = $('#touch-menu-content');
        var menu = $('.cpa_sidebar');

        $(touch).on('click', function (e) {
            $(this).toggleClass("active");
            e.preventDefault();
            menu.slideToggle();

        });

    });
    if ($('.footable').length) {
        $('.footable').footable();
    }


});
$(document).ready(function () {
    $('.confirm-delete').on('click', function (e) {
        if (!confirm('Удалить объект?')) {
            e.preventDefault();
        }
    });

    setInterval(blink, 4000);
    function blink() {
        $('.nav_star.unread:not(.active)').fadeTo(500, 0).fadeTo(500, 1);
    }
    blink();

    $('#showReferrer').on('shown.bs.modal', function (e) {
        $('.modal-body').text($(e.relatedTarget).text());
    })
});

function showReferrer(elem) {
    alert($(elem).text());
}