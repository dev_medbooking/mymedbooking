$(function(){
    $('html').keydown(function(eventObject){
      if (event.keyCode == 27) { 
        if($("#popup_mod").css('display') === "block"){
            $("#popup_close").click();
        }
      }
    });
        var settingMod = {
        num: num,
        mb_fwT: "normal",
        mb_fzT: "18px",
        mb_tdT: "none",
        mb_fsT: "normal",
        mb_snippetWidth:  800,
        mb_dragForm: "true",
        mb_postion : "position_non",
        mb_textT: "Проконсультируйтесь у специалиста",
        tmp: function(){ 
          return 	'<div id="medbooking-doctor"></div>|'+
              '<script> | @'+
                    'var id_client = '+mbId+',|'+
                        'num = "'+this.num+'",|'+
                        'mb_fwT = "'+this.mb_fwT+'",|'+
                        'mb_fzT = "'+this.mb_fzT+'",|'+
                        'mb_tdT = "'+this.mb_tdT+'",|'+
                        'mb_postion = "'+this.mb_postion +'",|'+
                        'mb_fsT = "'+this.mb_fsT+'",|'+
                        'mb_snippetWidth = "'+this.mb_snippetWidth+'",|'+
                        'mb_textT = "'+this.mb_textT+'",|'+
                        'mb_dragForm = "'+this.mb_dragForm+'",|'+
                        'scriptJs = document.createElement("script");|'+
                    'scriptJs.src = "//medbooking.com/scripts/integrator/moduleForm/script.js";|'+
                    'document.head.appendChild(scriptJs);| !'+
                  '</script>| '
        }
	};
	$("body").on("click", "#btn_code", function(){
    $("#copy").removeClass("btn_copy").val("Скопировать");
		$("#code_msg").text(settingMod.tmp());
		for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
		}
		for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
		}
		for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
		}
		$('#popup_mod').show();
		$('body').append('<div class="overlay"></div>');
	});
    $("body").on("click", "#popup_close, .overlay", function(){
        $('#popup_mod').hide();
        $(".overlay").remove();
    });
    var client = new ZeroClipboard( $("#copy") );
      client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        } );
        client.on( 'aftercopy', function(event) {
            //
        } );
      } );
      client.on( 'error', function(event) {
         console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
      } );
    var height_form_modul = $(".js_form_modul").offset().top;
    $(window).scroll(function(){
        if($(window).scrollTop() >  height_form_modul){
            $(".js_form_modul").addClass("scrollFixed");
        }
        else{
            $(".js_form_modul").removeClass("scrollFixed");
        }   
    });
    $("body").on("keyup", "#name_titleView", function(){
       $(".symptom_doctor_snippet_title").text($(this).val());
       settingMod.mb_textT = $(this).val();
    });
    $("body").on("click", ".js-underline", function(){
        if(!$(this).hasClass("active")){
           $(this).addClass("active");
           $(".symptom_doctor_snippet_title").css({"text-decoration": "underline"});
           settingMod.mb_tdT = "underline";
       }else{
           $(this).removeClass("active");
           $(".symptom_doctor_snippet_title").css({"text-decoration": "none"});
           settingMod.mb_tdT = "none";
       }
    });
    $("body").on("click", ".js-bold", function(){
        if(!$(this).hasClass("active")){
           $(this).addClass("active");
           $(".symptom_doctor_snippet_title").css({"font-weight": "bold"});
           settingMod.mb_fwT = "bold";
       }else{
           $(this).removeClass("active");
           $(".symptom_doctor_snippet_title").css({"font-weight": "normal"});
           settingMod.mb_fwT = "normal";
       }
    });
    $("body").on("click", ".js-italic", function(){
        if(!$(this).hasClass("active")){
           $(this).addClass("active");
           $(".symptom_doctor_snippet_title").css({"font-style": "italic"});
           settingMod.mb_fsT = "italic";
       }else{
           $(this).removeClass("active");
           $(".symptom_doctor_snippet_title").css({"font-style": "normal"});
           settingMod.mb_fsT = "normal";
       }
    });
    $("body").on("click", "#form", function(){
       if($(this).is(":checked")){
           $("#mb_minimized").show();
           settingMod.mb_dragForm = "true";

       }else{
           $("#mb_minimized").hide();
           settingMod.mb_dragForm = "false";
       }
    });
    $("body").on("change", ".js-font_size", function(){
        $(".symptom_doctor_snippet_title").css({"font-size": $(this).val() + "px"});
        settingMod.mb_fzT = $(this).val() + "px";
    });
    $("body").on("change", ".js-width_size", function(){
        $(".top_illness_doctor_snippet").removeClass("mb_wrap_snippet-200 mb_wrap_snippet-280 mb_wrap_snippet-320 mb_wrap_snippet-480 mb_wrap_snippet-640 mb_wrap_snippet-800");
        $(".top_illness_doctor_snippet").addClass("mb_wrap_snippet-" + $(this).val());
        settingMod.mb_snippetWidth = $(this).val();
    });
    $("body").on("change", ".js-form-position", function(){
      settingMod.mb_postion = $(".js-form-position option:selected").attr("value");
    });
});