$(function(){
    $('html').keydown(function(eventObject){
      if (event.keyCode == 27) { 
        if($("#popup_mod").css('display') === "block"){
            $("#popup_close").click();
        }
      }
    });
  var settingMod = {
    modW: 350,
    mb_titleForm: "Подобрать врача",
    tmp: function(){ 
      return  $(".active_banner").parent().html();



      // '<div class="medbooking-minimized medbooking-minimized__static" id="mb_minimized"></div>|'+
      //     '<script> | @'+
      //           'var id_client = '+mbId+',|'+
      //               'num = '+mbPhone+',|'+
      //               'modW = "'+this.modW+'",|'+
      //               'mb_line_txt = "'+this.mb_titleForm+'",|'+
      //               'scriptJs = document.createElement("script");|'+
      //           'scriptJs.src = "//medbooking.com/scripts/integrator/staticPopup.js";|'+
      //           'document.head.appendChild(scriptJs);| !'+
      //         '</script>| '
      }
  };
  $("body").on("click", "#btn_code", function(){
    $("#copy").removeClass("btn_copy").val("Скопировать");
    $("#code_msg").text(settingMod.tmp());
    for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
      $("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
    }
    for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
      $("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
    }
    for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
      $("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
    }
    $('#popup_mod').show();
    $('body').append('<div class="overlay"></div>');
  });
    $("body").on("click", "#popup_close, .overlay", function(){
        $('#popup_mod').hide();
        $(".overlay").remove();
    });

    $("body").on("change", "#asideWidth", function(){
    	$(".active_banner").width($(this).val());
    });
    $("body").on("click", "input[name='orientation']", function(){
    	var idActive = $(this).attr("id");
    	$(".opacity_banner").removeClass("opacity_banner");
    	$(".active_banner").removeClass("active_banner");
    	$("#asideWidth").empty();
    	if(idActive ==="horizontal"){
    		$("#b-horizontal").width(1200);
    		$("#b-vertical").addClass("opacity_banner");
    		$("#b-horizontal").addClass("active_banner");
    		$("#asideWidth").html('<option selected="selected" value="1200">1200</option><option value="1100">1100</option><option value="1000">1000</option><option value="900">900</option><option value="800">800</option><option value="700">700</option>');
    	}else if(idActive ==="vertical"){
    		$("#b-vertical").width(240);
    		$("#b-horizontal").addClass("opacity_banner");
    		$("#b-vertical").addClass("active_banner");
    		$("#asideWidth").html('<option selected="selected" value="240">240</option><option value="220">200</option><option value="200">200</option><option value="180">180</option><option value="160">160</option>');
    	}
    });
     var client = new ZeroClipboard( $("#copy") );
      client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        } );
        client.on( 'aftercopy', function(event) {
            //
        } );
      } );
      client.on( 'error', function(event) {
         console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
      } );
});



					
					
					
					
					