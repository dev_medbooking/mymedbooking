$(function(){
    $('html').keydown(function(eventObject){
      if (event.keyCode == 27) { 
        if($("#popup_mod").css('display') === "block"){
            $("#popup_close").click();
        }
      }
    });
    var noCheckStyle = function(){
        if($("#allStylebtn").is(":checked")){
            $("#allStylebtn").click();
        }
    };
  var settingMod = {
    modW: 350,
    mb_titleForm: "Подобрать врача",
    tmp: function(){ 
      return '<script> | @'+
                'var id_client = '+mbId+',|'+
                    'num = "'+mbPhone+'",|'+
                    'modW = "'+this.modW+'",|'+
                    'mb_line_txt = "'+this.mb_titleForm+'",|'+
                    'scriptJs = document.createElement("script");|'+
                'scriptJs.src = "//medbooking.com/scripts/integrator/one_drag_mod.js";|'+
                'scriptJs.charset="UTF-8";' +
                'document.head.appendChild(scriptJs);| !'+
              '</script>| '
      }
  };
  $("body").on("click", "#btn_code", function(){
    $("#copy").removeClass("btn_copy").val("Скопировать");
    $("#code_msg").text(settingMod.tmp());
    for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
      $("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
    }
    for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
      $("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
    }
    for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
      $("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
    }
    $('#popup_mod').show();
    $('body').append('<div class="overlay"></div>');
  });
    $("body").on("click", "#popup_close, .overlay", function(){
        $('#popup_mod').hide();
        $(".overlay").remove();
    });
    $("body").on("click", "#allStylebtn", function(){
      if($(this).is(":checked")){
          settingMod.modW = 350;
          settingMod.mb_titleForm = "Подобрать врача"
          $("#name_btn").val("");
          $("#asideWidth option:first").prop("selected", true);
          $(".mb-formWrapper__top__title").text("Подобрать врача");
          $(".medbooking-minimized").removeClass("wm_300 wm_250").addClass("wm_350");
          
      }
    });
    $("body").on("keyup", "#name_btn", function(){
        noCheckStyle();
        settingMod.mb_titleForm = $(this).val();
        $(".mb-formWrapper__top__title").text($(this).val());
    });
    $("body").on("change", "#asideWidth", function(){
        noCheckStyle();
        var modW = $(this).find("option:selected").val(),
            ell = $(".medbooking-minimized");
        settingMod.modW = modW;
        if(modW === "350"){
          ell.removeClass("wm_300 wm_250");
          ell.addClass("wm_350");
        }else if(modW === "300"){
          ell.removeClass("wm_350 wm_250");
          ell.addClass("wm_300");
        }else if(modW === "250"){
          ell.removeClass("wm_350 wm_300");
          ell.addClass("wm_250");
        }

    });
     var client = new ZeroClipboard( $("#copy") );
      client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        } );
        client.on( 'aftercopy', function(event) {
            //
        } );
      } );
      client.on( 'error', function(event) {
         console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
      } );
});