$(function(){
    $('html').keydown(function(eventObject){
      if (event.keyCode == 27) { 
        if($("#popup_mod").css('display') === "block"){
            $("#popup_close").click();
        }
      }
    });
    // Сохранение параметров на переходах по страницам
    if(sessionStorage.getItem("category") === "doctors"){
        $("input[id=search_clinic]").prop("checked", false);
        $("input[id=search_doctor]").prop("checked", true);
    }else if(sessionStorage.getItem("category") === "clinics"){
        $("input[id=search_doctor]").prop("checked", false);
        $("input[id=search_clinic]").prop("checked", true);
    }
    if(sessionStorage.getItem("category")){
        $("input[id="+sessionStorage.getItem("category")+"]").prop("checked", true)
    }    
    if(sessionStorage.getItem("sidebar") && sessionStorage.getItem("sidebar") === "false"){
         $("#toggle_sidebar").click();
            if(!document.getElementById("styleTmp")){
                var styleTmp = document.createElement("style");
                styleTmp.id =  "styleTmp";
                document.head.appendChild(styleTmp);
            }
            document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:none; } .medbooking-content{width: 870px !important;}.mb_tabs{width: 870px !important;}";
    }
    if(sessionStorage.getItem("drag") && sessionStorage.getItem("drag") === "false"){
        $("#toggle_drag").click();
         if(!document.getElementById("styleTmpDrag")){
                var styleTmpDrag = document.createElement("style");
                styleTmpDrag.id =  "styleTmpDrag";
                document.head.appendChild(styleTmpDrag);
            }
        document.getElementById("styleTmpDrag").innerHTML = "#mb_minimized {display:none; }";
    }
    if(sessionStorage.getItem("special")){
        $("#selectSpecial option").attr("selected", false);
        $("#selectSpecial option[value="+sessionStorage.getItem("special")+"]").prop("selected", true);
    }
    $("#toggle_width option[value="+sessionStorage.getItem("contentWidth")+"]").prop('selected', true);

        $("#medbooking-doctor").removeClass("mb_contentWidth_870 mb_contentWidth_800 mb_contentWidth_750 mb_contentWidth_700");
        $("#medbooking-doctor").addClass("mb_contentWidth_" + sessionStorage.getItem("contentWidth"));
        $(".medbooking__doctor_list, .medbooking__clinic_list, .medbooking__tems, #medbooking__blog_service_list, #medbooking__blog_service_single, #doctor_card, .doctor_single .top_left_inner_doctor_single, .top_inner_clinik_single, .top_inner_clinik_single, .bottom_inner_clinik_single")
            .css({"width": sessionStorage.getItem("contentWidth")+"px"});

    $("#js-site-folder").val(sessionStorage.getItem("folder"));
    $("#selectSpecial").chosen({no_results_text: "Нет такой специальности"}); 
    var height_form_modul = $(".js_form_modul").offset().top;
    $(window).scroll(function(){
        if($(window).scrollTop() >  height_form_modul){
            $(".js_form_modul").addClass("scrollFixed");
        }
        else{
            $(".js_form_modul").removeClass("scrollFixed");
        }   
    });
	var settingMod = {
    dragForm: (sessionStorage.getItem("drag") || true),
    aside: (sessionStorage.getItem("sidebar") || true),
    floder: sessionStorage.getItem("folder"),
    contentWidth: sessionStorage.getItem("contentWidth"),
    mb_category: (sessionStorage.getItem("category") || ""),
    mb_special: (sessionStorage.getItem("special") || ""),
		tmp: function(){
			return '<div id="medbooking-doctor"> * @'+
                    '<?php * @'+
                        '$partner_url = "'+(this.folder ||  sessionStorage.getItem("folder"))+'"; *'+
                        '$_SERVER["REQUEST_URI"] = substr($_SERVER["REQUEST_URI"], strlen($partner_url)); *'+
                        'if(empty($_SERVER["REQUEST_URI"]) || $_SERVER["REQUEST_URI"] == "/") { * @'+
                            '$url="//medbooking.com'+(this.mb_category || "/search_doctor")+"/"+ (this.mb_special || "") +'"; * %'+
                        '} else { * @'+
                            '$url="//medbooking.com/".$_SERVER["REQUEST_URI"]; * %'+
                        '} *'+
                        '$ch=curl_init(); *'+
                        'curl_setopt($ch,CURLOPT_URL,$url."?ajax=curl&partner_url=".($partner_url == "/" ? "" : $partner_url));*'+
                        'curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/x-www-form-urlencoded")); *'+
                        'curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1); *'+
                        'curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); *'+
                        'curl_setopt($ch,CURLOPT_VERBOSE,1); *'+
                        'curl_setopt($ch,CURLINFO_HEADER_OUT,1); *'+
                        '$response=curl_exec($ch); *'+
                        'if(!empty($response)){ * @ '+
                            'echo $response; * %'+
                        '} * %'+
                    '?> *'+
			        '<script> * @'+
				        'var id_client = '+mbId+',*'+
				            'num = "'+mbPhone+'",*'+
				            'dragForm = '+this.dragForm+',*'+
                            'contentWidth = '+this.contentWidth+',*'+
                            'folder = "'+sessionStorage.getItem("folder")+'",*'+
				            'aside = '+this.aside+',*'+
				            'mb_category = "'+this.mb_category+'",*'+
				            'mb_special = "'+this.mb_special+'",*'+
				            'scriptJs = document.createElement("script");*'+
				        'scriptJs.id = "initScript";*'+
				        'scriptJs.src = "//medbooking.com/scripts/integrator/part2/part2.js";*'+
				        'document.head.appendChild(scriptJs);* %'+
			        '</script>* %'+
                '</div>'
		}
	};






	$("body").on("click", "#btn_code", function(){
        if(!sessionStorage.getItem("folder")){
            $("#err-site-folder").css({"display": "block"});
            return false;
        }
    $("#err-site-folder").css({"display": "none"});
    $("#copy").removeClass("btn_copy").val("Скопировать");
		$("#code_msg").text(settingMod.tmp());
		for (var i = 0; i < settingMod.tmp().split("*").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("*", '<br>'))
		}
		for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
		}
		for (var i = 0; i < settingMod.tmp().split("%").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("%", '</section>'));
		}
		$('#popup_mod').show();
		$('body').append('<div class="overlay"></div>');
	});

	$("body").on("change", "#toggle_drag", function(){
		if($(this).is(":checked")){
			$("#mb_minimized").show();
			settingMod.dragForm = "true";
            sessionStorage.setItem("drag", "true");
		}else{
            sessionStorage.setItem("drag", "false");
			$("#mb_minimized").hide();
			settingMod.dragForm = "false";
		}
	});
    $("body").on("click", "#toggle_sidebar", function(){
        if(!document.getElementById("styleTmp")){
            var styleTmp = document.createElement("style");
            styleTmp.id =  "styleTmp";
            document.head.appendChild(styleTmp);
        }
        if($(this).is(":checked")){
            sessionStorage.setItem("sidebar", "true");
            settingMod.aside = "true";
            document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:block}.medbooking-content{width: 1090px !important;}.mb_tabs{width: 1090px !important;}";
        }else{
            sessionStorage.setItem("sidebar", "false");
            document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:none; } .medbooking-content{width: 870px !important;}.mb_tabs{width: 870px !important;}";
            settingMod.aside = "false";
        }
    });
    $("body").on("change", "#toggle_width", function(){
        settingMod.contentWidth = $(this).val();
        sessionStorage.setItem("contentWidth", $(this).val());
        for(var i = 0; i < document.querySelectorAll("#toggle_width option").length; i++){
            document.getElementById("medbooking-doctor").classList.remove("mb_contentWidth_" + document.querySelectorAll("#toggle_width option")[i].value);
        }
        $("#medbooking-doctor").addClass("mb_contentWidth_" + settingMod.contentWidth);
        $(".medbooking__doctor_list, .medbooking__clinic_list, .medbooking__tems, #medbooking__blog_service_list, #medbooking__blog_service_single, #doctor_card, .doctor_single .top_left_inner_doctor_single, .top_inner_clinik_single, .top_inner_clinik_single, .bottom_inner_clinik_single")
            .css({"width": settingMod.contentWidth + "px"})
    });
	$("body").on("click", "#popup_close, .overlay", function(){
		$('#popup_mod').hide();
		$(".overlay").remove();
	});
    $("body").on("click", "input[name=category_toogle]", function(){
        
        if($(this).attr("id") === "search_clinic"){
             sessionStorage.setItem("category", "clinics");
             location = location.pathname.split("2")[0] + "2/" + "search_clinic";
        }else if($(this).attr("id") === "search_doctor"){
            settingMod.mb_category = "search_doctor";
            sessionStorage.setItem("category", "doctors");
            location = location.pathname.split("2")[0] + "2/" + "search_doctor";
        }
    });
    
    $("#selectSpecial").chosen().change(function(){
        sessionStorage.setItem("special", $(this).val());
        settingMod.mb_special = $(this).val();
            if(location.pathname.split("2")[1]){
                if(location.pathname.split("2")[1] === "/search_doctor" || location.pathname.split("2")[1].split("/")[1] === "doctors"){
                    location.href= location.pathname.split("2")[0]+"2/doctors/" + $(this).val();
                }
                if(location.pathname.split("2")[1] === "/search_clinic" || location.pathname.split("2")[1].split("/")[1] === "clinics"){
                    location.href= location.pathname.split("2")[0]+"2/clinics/" + $(this).val();
                }
           }
           else{
                location.href= location.pathname.split("2")[0]+"2/doctors/" + $(this).val();
           }
    });
    $("body").on("keyup", "#js-site-folder", function(){
        var btn = $("#btn_code");
        if($(this).val() === ""){
            btn.addClass("btn_opacity");
        }else{
            btn.removeClass("btn_opacity");
        }
        new_folder_url = $(this).val();
        new_folder_url += '/';
        new_folder_url = new_folder_url.replace('//','/');
        settingMod.folder = new_folder_url;
        sessionStorage.setItem("folder", new_folder_url);
    });
   var client = new ZeroClipboard( $("#copy") );
      client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        } );
        client.on( 'aftercopy', function(event) {
            //
        } );
      } );
      client.on( 'error', function(event) {
         console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
      } );

});


