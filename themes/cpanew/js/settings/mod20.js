$(function () {
    $('.select3').multipleSelect({
        filter: true
    });

    var settingMod = {
        num: num,
        limit: 10 ,
        url: "//" + medbookingDiagnosticServicesModule_moduleDomain + "/servicesPartner?ajax=partner_list2&",
        dragForm: "true",
        toggleTitle: "",
        categories: "",
        contentWidth: "auto",
        buttonColor: "#E54550",
        tmp: function () {
            return '<div id="medbooking-module-doc-sidebar"></div> | ' +
                '<script> | @' +
                'var ' +
                'medbookingDiagnosticServicesModule_id_client = ' + mbId + ',|' +
                'medbookingDiagnosticServicesModule_number = "' + this.num + '",|' +
                //'medbookingDiagnosticServicesModule_limit = ' + medbookingDiagnosticServicesModule_limit + ',|' +
                'medbookingDiagnosticServicesModule_contentWidth = "' + this.contentWidth + '",|' +
                'medbookingDiagnosticServicesModule_dragForm = ' + this.dragForm + ',|' +
                'medbookingDiagnosticServicesModule_buttonColor = "' + this.buttonColor + '",|' +
                'medbookingDiagnosticServicesModule_categories = "' + medbookingDiagnosticServicesModule_categories + '",|' +
                'medbookingDiagnosticServicesModule_moduleDomain = "' + medbookingDiagnosticServicesModule_moduleDomain + '";|' +
                'scriptJs = document.createElement("script");|' +
                'scriptJs.id = "initScript";|' +
                'scriptJs.charset = "UTF-8";' +
                'scriptJs.src = "//' + medbookingDiagnosticServicesModule_moduleDomain + '/scripts/integrator/diagnosticServicesModule/script.js";|' +
                'document.head.appendChild(scriptJs);| !' +
                '</script>| '
        }
    };

    $("body").on("click", "#btn_code", function () {
        $("#copy").removeClass("btn_copy").val("Скопировать");
        $("#code_msg").text(settingMod.tmp());
        for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
        }
        for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
        }
        for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
        }
        $('#popup_mod').show();
        $('body').append('<div class="overlay"></div>');
    });

    var client = new ZeroClipboard($("#copy"));
    client.on('ready', function (event) {
        client.on('copy', function (event) {
            event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        });
        client.on('aftercopy', function (event) {
            //
        });
    });
    client.on('error', function (event) {
        console.log('ZeroClipboard error of type "' + event.name + '": ' + event.message);
        ZeroClipboard.destroy();
    });

    $("body").on("change", "#toggle_drag", function () {
        if ($(this).is(":checked")) {
            $("#mb_minimized").show();
            settingMod.dragForm = "true";
            medbookingDiagnosticServicesModule_dragForm = true;
        } else {
            $("#mb_minimized").hide();
            settingMod.dragForm = "false";
            dragForm = false;
            medbookingDiagnosticServicesModule_dragForm = false;
        }
    });
    
    $('#picker_button').colpick({
        layout:'hex',
        submit:0,
        colorScheme:'dark',
        onChange:function(hsb,hex,rgb,el,bySetColor) {
            $(el).css('border-color','#'+hex);
            if(!bySetColor) $(el).val(hex);
            $(".js-colorPick").css({"background-color": '#'+hex});
            //$(".moduleDoctor__info-rate-rating").css({"background-color": '#'+hex});
            settingMod.buttonColor = '#'+hex;
            medbookingDiagnosticServicesModule_buttonColor = '#'+hex;
            //noCheckStyle();
        }
    });
    $("#picker_button").css({"border-color":"#E54550"});
    $("#picker_button").val("#E54550");

    $("body").on("keyup", "#toggle_title", function () {
        var text = $(this).val();
        $("#js-moduleDocSidebar_title").html(text);
        settingMod.toggleTitle = text;
        medbookingDiagnosticServicesModule_title = text;
    });
    $("#apply_categories").on('click', function () {
        var selectedCategories = [];
        $('#MedbookingNodeService_category_id option').each(function () {
            var value = $(this).attr('value');
            var selected = $(this).prop('selected');
            if (selected == true) {
                selectedCategories.push(value);
            }

        });
        settingMod.categories = selectedCategories;
        medbookingDiagnosticServicesModule_categories = selectedCategories.toString();
        loadContent_medbookingDiagnosticServicesModule();
    });


    $("body").on("change", "#toggle_width", function () {

        if (document.getElementsByClassName("js-toggleWidth").length) {
            for (var i = 0; i < document.getElementsByClassName("js-toggleWidth").length; i++) {
                document.getElementsByClassName("js-toggleWidth").item(i).classList.remove("mb_contentWidth_" + settingMod.contentWidth);
                document.getElementsByClassName("js-toggleWidth").item(i).classList.add("mb_contentWidth_" + $(this).val());
            }
        }

        settingMod.contentWidth = $(this).val();
        medbookingDiagnosticServicesModule_contentWidth = $(this).val();
    });
    $("body").on("click", "#popup_close, .overlay", function () {
        $('#popup_mod').hide();
        $(".overlay").remove();
    });

    var client = new ZeroClipboard($("#copy"));
    client.on('ready', function (event) {
        client.on('copy', function (event) {
            event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        });
        client.on('aftercopy', function (event) {
            //
        });
    });
    client.on('error', function (event) {
        console.log('ZeroClipboard error of type "' + event.name + '": ' + event.message);
        ZeroClipboard.destroy();
    });

    $("#recordsCount").text(10);
    $("#recordsCountShow").text(10);
});


function limitChange(event, ui) {
    var limit = ui.values[0];
    $("#recordsCountShow").text(limit);
    medbookingDiagnosticServicesModule_limit = limit;
    if (typeof settingMod != 'undefined') {
        settingMod.limit = limit;
    }
    loadContent_medbookingDiagnosticServicesModule();
}

