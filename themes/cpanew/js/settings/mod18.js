$(function () {
    $('.select3').multipleSelect({
        filter: true
    });

    $('html').keydown(function (eventObject) {
        var e = e || window.event;
        if ( e && e.keyCode == 27) {
            if ($("#popup_mod").css('display') === "block") {
                $("#popup_close").click();
            }
        }
    });
    $("#selectSpecial").chosen({no_results_text: "Нет такой специальности"});
    var height_form_modul = $(".js_form_modul").offset().top;
    $(window).scroll(function () {
        if ($(window).scrollTop() > height_form_modul) {
            $(".js_form_modul").addClass("scrollFixed");
        }
        else {
            $(".js_form_modul").removeClass("scrollFixed");
        }
    });
    var limit;
    var settingMod = {
        num: num,
        limit: 10,
        url:  "//" + moduleDomain +"/servicesPartner?ajax=partner_list2&",
        dragForm: "true",
        aside: "true",
        toggleServices: "true",
        toggleClinics: "true",
        toggleDoctors:"true",
        toggleForm: "true",
        toggleTitle: "",
        contentWidth: "'auto'",
        //mb_category: "search_doctor",
        //mb_special: "lfk",
        tmp: function () {
            return '<div id="medbooking-service"></div> | ' +
                '<script> | @' +
                'var id_client = ' + mbId + ',|' +
                'num = "'+this.num+'",|'+
//                'MedbookingServicesModule2_limit = ' + $("#new_infopage").attr("data-limit") + ',|' +
                'MedbookingModuleServices2_limit = ' + MedbookingModuleServices2_limit + ',|' +
                'MedbookingModuleServices2_clinicLimit = ' + MedbookingModuleServices2_clinicLimit + ',|' +
                'MedbookingModuleServices2_doctorLimit = ' + MedbookingModuleServices2_doctorLimit + ',|' +
                'MedbookingModuleServices2_dragForm = ' + this.dragForm + ',|' +
                'MedbookingModuleServices2_contentWidth = ' + this.contentWidth + ',|' +
                'MedbookingModuleServices2_toggleServices = ' + this.toggleServices + ',|' +
                'MedbookingModuleServices2_toggleClinics = ' + this.toggleDoctors + ',|' +
                'MedbookingModuleServices2_toggleDoctors = ' + this.toggleDoctors + ',|' +
                'MedbookingModuleServices2_toggleForm = ' + this.toggleForm + ',|' +
                'MedbookingModuleServices2_toggleTitle = "' + this.toggleTitle + '",|' +
                'MedbookingModuleServices2_url = "' + addConditions(this.url)+'",|' +
                'MedbookingModuleServices2_realLimit = true,|' +
                'moduleDomain = "' + moduleDomain + '";|' +
                'scriptJs = document.createElement("script");|' +
                'scriptJs.id = "initScript";|' +
                'scriptJs.charset = "UTF-8";' +
                'scriptJs.src = "//' + moduleDomain + '/scripts/integrator/moduleServices2/script.js";|' +
                'document.head.appendChild(scriptJs);| !' +
                '</script>| '
        }
    };

    $("body").on("click", "#btn_code", function () {
        $("#copy").removeClass("btn_copy").val("Скопировать");
        $("#code_msg").text(settingMod.tmp());
        for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
        }
        for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
        }
        for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
        }
        $('#popup_mod').show();
        $('body').append('<div class="overlay"></div>');
    });

    $("body").on("change", "#toggle_drag", function () {
        if ($(this).is(":checked")) {
            $("#mb_minimized").show();
            settingMod.dragForm = "true";
            MedbookingModuleServices2_dragForm = true;
        } else {
            $("#mb_minimized").hide();
            settingMod.dragForm = "false";
            dragForm = false;
            MedbookingModuleServices2_dragForm = false;
        }
    });

    $("body").on("change", "#toggle_services", function () {
        if ($(this).is(":checked")) {
            MedbookingModuleServices2_toggleServices = true;
            settingMod.toggleServices = "true";
            $(".services-tab").addClass('active');
            $(".services-tabs").show();
            $(".services-tabs").click();
            tabStateChange(false);
            //$("#search_doctors_way_services_list").show();
        } else {
            MedbookingModuleServices2_toggleServices = false;
            settingMod.toggleServices = "false";
            $("#search_doctors_way_services_list").hide();
            $(".services-tabs").hide();
            tabStateChange(true);
        }
    });
    $("body").on("change", "#toggle_clinics", function () {
        if ($(this).is(":checked")) {
            MedbookingModuleServices2_toggleClinics = true;
            settingMod.toggleClinics = "true";
            $(".clinic-tabs").show();
            $(".clinic-tabs").click();
            tabStateChange(false);
            //$("#search_doctors_way_clinic_list").show();
        } else {
            MedbookingModuleServices2_toggleClinics = false;
            settingMod.toggleClinics = "false";
            $(".clinic-tabs").hide();
            tabStateChange(true);
            $("#search_doctors_way_clinic_list").hide();
        }
    });
    $("body").on("change", "#toggle_doctors", function () {
        if ($(this).is(":checked")) {
            MedbookingModuleServices2_toggleDoctors = true;
            settingMod.toggleDoctors = "true";
            $(".doc-tabs").show();
            $(".doc-tabs").click();
            tabStateChange(false);
            //$("#search_doctors_way_doc_list").show();
        } else {
            MedbookingModuleServices2_toggleDoctors = false;
            settingMod.toggleDoctors = "false";
            $(".doc-tabs").hide();
            tabStateChange(true);
            $("#search_doctors_way_doc_list").hide();
        }
    });
    $("body").on("change", "#toggle_form", function () {
        if ($(this).is(":checked")) {
            //$("#search").show();
            $('#search').show();
            settingMod.toggleForm = "true";
            MedbookingModuleServices2_toggleForm = true;
        } else {
            //$("#search").hide();
            $('#search').hide();
            settingMod.toggleForm = "false";
            MedbookingModuleServices2_toggleForm = false;
        }
    });
    $("body").on("keyup", "#toggle_title", function () {
            var text = $(this).val();
            $(".module-title_js").html(text);
            settingMod.toggleTitle = text;
            MedbookingModuleServices2_toggleTitle = text;
    });
    $("#use_categories").on('click', function () {
        $('#MedbookingNodeService_category_id option').each(function () {
            var value = $(this).attr('value');
            var selected = $(this).prop('selected');
            if (selected == true) {
                $("a[data-translit='" + value + "']").parent().show();
            } else {
                $("a[data-translit='" + value + "']").parent().hide();
            }

        });
        scriptRequest(MedbookingModuleServices2_url, ok, fail, id_client);
    });
    $("body").on("change", "#toggle_sidebar", function () {
        if (!document.getElementById("styleTmp")) {
            var styleTmp = document.createElement("style");
            styleTmp.id = "styleTmp";
            document.head.appendChild(styleTmp);
        }
        if ($(this).is(":checked")) {
            settingMod.aside = "true";
            document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:block}.medbooking-content{width: 1090px !important;}.mb_tabs{width: 1090px !important;}";
        } else {
            document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:none; } .medbooking-content{width: 870px !important;}.mb_tabs{width: 870px !important;}";
            settingMod.aside = "false";
        }
    });

    $("body").on("change", "#toggle_width", function () {

        $(".module18, #medbooking-service, .new_infopage").removeClass("mb_contentWidth_" + settingMod.contentWidth)
        settingMod.contentWidth = $(this).val();
        MedbookingModuleServices2_contentWidth = $(this).val();
        $(".module18, #medbooking-service, .new_infopage").addClass("mb_contentWidth_" + MedbookingModuleServices2_contentWidth)

    });
    $("body").on("click", "#popup_close, .overlay", function () {
        $('#popup_mod').hide();
        $(".overlay").remove();
    });
    $("body").on("click", "input[name=category_toogle]", function () {
        if ($(this).attr("id") === "search_clinic") {
            settingMod.mb_category = "search_clinic";
            url = "//" + moduleDomain + "/" + settingMod.mb_category + "/" + settingMod.mb_special + "?ajax=ref";
            scriptRequest(url, ok, fail, id_client, function () {
            });
        } else if ($(this).attr("id") === "search_doctor") {
            settingMod.mb_category = "search_doctor";
            url = "//" + moduleDomain + "/" + settingMod.mb_category + "/" + settingMod.mb_special + "?ajax=ref";
            scriptRequest(url, ok, fail, id_client, function () {
            });
        }
    });

    $("#selectSpecial").chosen().change(function () {
        url = "//" + moduleDomain + "/" + settingMod.mb_category + "/" + $(this).val() + "?ajax=ref";
        scriptRequest(url, ok, fail, id_client, function () {
        });
        settingMod.mb_special = $(this).val();
    });
    var client = new ZeroClipboard($("#copy"));
    client.on('ready', function (event) {
        client.on('copy', function (event) {
            event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        });
        client.on('aftercopy', function (event) {
            //
        });
    });
    client.on('error', function (event) {
        console.log('ZeroClipboard error of type "' + event.name + '": ' + event.message);
        ZeroClipboard.destroy();
    });


});
$(function () {
    var availableTags  = {};
    $.getJSON( '//' + moduleDomain + '/servicesPartner/getServices?callback=?', function ( data ) {
        var i = 0;
        availableTags = data.data;
        $("#tags").autocomplete({
            source: availableTags,
            //autoFill: true,
            matchContains: 1,
            //selectFirst: true,
            select: function (event, ui) {
                event.preventDefault();
                var label = ui.item.label;
                var value = ui.item.value;
                $("#tags").val('');
                $("#services_box").append('<li data-id="' + value+ '" ><a data-translit="' + value+ '"><span class="tab">' + label + '<div class="tab_close" onclick="$(this).closest(\'li\').remove();scriptRequest(url, ok, fail, id_client);">x</div></span></a></li>');
                if (document.getElementsByClassName("myScript").length) {
                    url = document.getElementsByClassName("myScript")[0].src;
                    url = delPrm(url, "percentLimit");
                    url = delPrm(url, "callback");
                    scriptRequest(url, ok, fail, id_client);
                }
                //store in session
            },
            focus: function(event, ui) {
                event.preventDefault();
                $("#tags").val(ui.item.label);
            }
        });

    } );
});

function tabStateChange(hide){
    var tabs = 0;
    if(MedbookingModuleServices2_toggleDoctors){
        tabs++; if(hide) $(".doc-tabs").click();
    }
    if(MedbookingModuleServices2_toggleClinics){
        tabs++; if(hide) $(".clinic-tabs").click();
    }
    if(MedbookingModuleServices2_toggleServices){
        tabs++; if(hide) $(".services-tabs").click();
    }
    if(tabs>1) $(".module18__menu").show();
    else $(".module18__menu").hide();
}

function limitChange(event, ui) {
    var limit = ui.values[0];
    MedbookingModuleServices2_limit = limit;
    url = MedbookingModuleServices2_url;
    if (document.getElementsByClassName("myScript").length) {
        url = delPrm(url, "percentLimit");
        url = delPrm(url, "callback");
    }
    scriptRequest(url, ok, fail, id_client);

};
function clinicLimitChange(event, ui) {
    var clinicLimit = ui.values[0];
    //$("#new_infopage").attr("data-limit", limit);
    MedbookingModuleServices2_clinicLimit = clinicLimit;
    url = MedbookingModuleServices2_url;
    if (document.getElementsByClassName("myScript").length) {
        url = delPrm(url, "clinicPercentLimit");
        url = delPrm(url, "callback");
    }
    scriptRequest(url, ok, fail, id_client);

};
function doctorLimitChange(event, ui) {
    var doctorLimit = ui.values[0];
    //$("#new_infopage").attr("data-limit", limit);
    MedbookingModuleServices2_doctorLimit = doctorLimit;
    url = MedbookingModuleServices2_url;
    if (document.getElementsByClassName("myScript").length) {
        url = delPrm(url, "doctorPercentLimit");
        url = delPrm(url, "callback");
    }
    scriptRequest(url, ok, fail, id_client);

};
function limitSlide(event, ui) {
    var limit = ui.values[0];
    var recordsCount = $("#recordsCount").text();
    var recordsCountShow = limit * recordsCount / 100;
    $("#recordsCountShow").html(recordsCountShow);
};