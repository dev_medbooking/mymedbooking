$(function () {
    $("#selectSpecial").multipleSelect({filter: true});

    var settingMod = {
        specialities: medbookingDoctorsModule_specialities,
        buttonColor: "#00baff",
        rateColor: "#27cdb7",
        limit: medbookingDoctorsModule_limit,
        tmp: function () {
            return '<div id="medbookingDoctorsModule"></div> | ' +
                '<script> | @' +
                'var ' +
                'medbookingDoctorsModule_id_client = ' + mbId + ',|' +
                'medbookingDoctorsModule_phone = "' + medbookingDoctorsModule_phone + '",|' +
                'medbookingDoctorsModule_limit = ' + this.limit + ',|' +
                'medbookingDoctorsModule_number = "' + medbookingDoctorsModule_number + '",|' +
                'medbookingDoctorsModule_buttonColor = "' + this.buttonColor + '",|' +
                'medbookingDoctorsModule_rateColor = "' + this.rateColor + '",|' +
                'medbookingDoctorsModule_moduleDomain = "' + medbookingDoctorsModule_moduleDomain + '",|' +
                'medbookingDoctorsModule_apiDomain = "' + medbookingDoctorsModule_apiDomain + '",|' +
                'medbookingDoctorsModule_specialities = "' + this.specialities + '",|' +
                'medbookingDoctorsModule_scriptJs = document.createElement("script");|' +
                'medbookingDoctorsModule_scriptJs.id = "medbookingDoctorsModule_initScript";|' +
                'medbookingDoctorsModule_scriptJs.charset = "UTF-8";|' +
                'medbookingDoctorsModule_scriptJs.src = "//" + medbookingDoctorsModule_moduleDomain + "/scripts/integrator/medbookingDoctorsModule/script.js";|' +
                'document.head.appendChild(medbookingDoctorsModule_scriptJs);| !' +
                '</script>| '
        }
    };

    $("body").on("click", "#btn_code", function () {
        $("#copy").removeClass("btn_copy").val("Скопировать");
        $("#code_msg").text(settingMod.tmp());
        for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
        }
        for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
        }
        for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
        }
        $('#popup_mod').show();
        $('body').append('<div class="overlay"></div>');
    });

    var client = new ZeroClipboard($("#copy"));
    client.on('ready', function (event) {
        client.on('copy', function (event) {
            event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        });
        client.on('aftercopy', function (event) {
            //
        });
    });
    client.on('error', function (event) {
        console.log('ZeroClipboard error of type "' + event.name + '": ' + event.message);
        ZeroClipboard.destroy();
    });

    $('#picker_button').colpick({
        layout:'hex',
        submit:0,
        colorScheme:'dark',
        onChange:function(hsb,hex,rgb,el,bySetColor) {
            $(el).css('border-color','#'+hex);
            if(!bySetColor) $(el).val(hex);
            $(".picker .js-colorPick").css({"background-color": '#'+hex});
            $(".doctorList__orderBtn").css({"background-color": '#'+hex});
            settingMod.buttonColor = '#'+hex;
            medbookingDoctorsModule_buttonColor = '#'+hex;
        }
    });
    $("#picker_button").css({"border-color":"#00baff"});
    $("#picker_button").val("#00baff");

    $('#picker_button2').colpick({
        layout:'hex',
        submit:0,
        colorScheme:'dark',
        onChange:function(hsb,hex,rgb,el,bySetColor) {
            $(el).css('border-color','#'+hex);
            if(!bySetColor) $(el).val(hex);
            $(".picker2 .js-colorPick").css({"background-color": '#'+hex});
            $(".doctorList__rating").css({"background-color": '#'+hex});
            settingMod.rateColor = '#'+hex;
            medbookingDoctorsModule_rateColor = '#'+hex;
        }
    });
    $("#picker_button2").css({"border-color":"#27cdb7"});
    $("#picker_button2").val("#27cdb7");

    var sliderTimeout;
    $( "#slider-range-max" ).slider({
        range: "max",
        min: 3,
        max: 15,
        value: 12,
        slide: function( event, ui ) {
            $("#amount").val( ui.value );
            settingMod.limit = ui.value;
            medbookingDoctorsModule_limit = ui.value;
            clearTimeout(sliderTimeout);
            sliderTimeout = setTimeout(function () {
                reloadModule();
            }, 100);
        }
    });
    $( "#amount" ).val( $( "#slider-range-max" ).slider( "value" ) );

    $("#selectSpecial").change(function(){
        settingMod.specialities = ($(this).val()===null)?'':$(this).val().join(',');
        medbookingDoctorsModule_specialities = settingMod.specialities;
        reloadModule();
    });

    $("body").on("click", "#popup_close, .overlay", function () {
        $('#popup_mod').hide();
        $(".overlay").remove();
    });

    function reloadModule(){
        $('#medbookingDoctorsModule').html('');
        medbookingDoctorsModule_AfterInit();
    }

});

