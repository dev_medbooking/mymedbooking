$(function () {
    $("#selectSpecial").multipleSelect({filter: true});

    var settingMod = {
        specialities: medbookingListModule_specialities,
        buttonColor: "#7dc855",
        iconColor: "#f44336",
        subways: medbookingListModule_subways,
        limit: medbookingListModule_limit,
        tmp: function () {
            return '<div id="medbookingListModule"></div> | ' +
                '<script> | @' +
                'window.medbookingListModule_id_client = ' + mbId + ',|' +
                'window.medbookingListModule_phone = "' + medbookingListModule_phone + '",|' +
                'window.medbookingListModule_limit = ' + this.limit + ',|' +
                'window.medbookingListModule_buttonColor = "' + this.buttonColor + '",|' +
                'window.medbookingListModule_iconColor = "' + this.iconColor + '",|' +
                'window.medbookingListModule_tabDoctor = ' + medbookingListModule_tabDoctor + ',|' +
                'window.medbookingListModule_tabClinic = ' + medbookingListModule_tabClinic + ',|' +
                'window.medbookingListModule_filter = ' + medbookingListModule_filter + ',|' +
                'window.medbookingListModule_number = "' + medbookingListModule_number + '",|' +
                'window.medbookingListModule_child = ' + medbookingListModule_child + ',|' +
                'window.medbookingListModule_house = ' + medbookingListModule_house + ',|' +
                'window.medbookingListModule_net = ' + medbookingListModule_net + ',|' +
                'window.medbookingListModule_sorting = "' + medbookingListModule_sorting + '",|' +
                'window.medbookingListModule_moduleDomain = "' + medbookingListModule_moduleDomain + '",|' +
                'window.medbookingListModule_apiDomain = "' + medbookingListModule_apiDomain + '",|' +
                'window.medbookingListModule_specialities = "' + this.specialities + '",|' +
                'window.medbookingListModule_subways = "' + this.subways + '",|' +
                'window.medbookingListModule_charset = "' + medbookingListModule_charset + '",|' +
                'medbookingListModule_scriptJs = document.createElement("script");|' +
                'medbookingListModule_scriptJs.id = "medbookingListModule_initScript";|' +
                'medbookingListModule_scriptJs.charset = "UTF-8";|' +
                'medbookingListModule_scriptJs.src = "//" + medbookingListModule_moduleDomain + "/scripts/integrator/medbookingListModule/script.js";|' +
                'document.head.appendChild(medbookingListModule_scriptJs);| !' +
                '</script>| '
        }
    };

    $('#picker_button').colpick({
        layout:'hex',
        submit:0,
        colorScheme:'dark',
        onChange:function(hsb,hex,rgb,el,bySetColor) {
            $(el).css('border-color','#'+hex);
            if(!bySetColor) $(el).val(hex);
            $(".picker .js-colorPick").css({"background-color": '#'+hex});
            $(".mbm__card__orderBtn, .mbm__search__btn, .mbm__modalSubway__metroApplyBtn").css({"background-color": '#'+hex});
            settingMod.buttonColor = '#'+hex;
            medbookingListModule_buttonColor = '#'+hex;
        }
    });
    $("#picker_button").css({"border-color":"#7dc855"});
    $("#picker_button").val("#7dc855");

    $('#picker_button2').colpick({
        layout:'hex',
        submit:0,
        colorScheme:'dark',
        onChange:function(hsb,hex,rgb,el,bySetColor) {
            $(el).css('border-color','#'+hex);
            if(!bySetColor) $(el).val(hex);
            $(".picker2 .js-colorPick").css({"background-color": '#'+hex});
            $(".mbm__card__rating").css({"color": '#'+hex,"border-color": '#'+hex});
            $(".mbm__card__ratingIcon").css({"background-color": '#'+hex});
            $(".mbm__card__reviewsIcon").css({"background-color": '#'+hex});
            $(".mbm__card__clinicIcon").css({"background-color": '#'+hex});
            settingMod.iconColor = '#'+hex;
            medbookingListModule_iconColor = '#'+hex;
        }
    });
    $("#picker_button2").css({"border-color":"#f44336"});
    $("#picker_button2").val("#f44336");

    $("body").on("click", "input[name=category_toogle]", function(){
        if($(this).attr("id") === "search_clinic"){
            medbookingListModule_tabClinic = medbookingListModule_tabClinic ? false : true;
        } else if($(this).attr("id") === "search_filter"){
            medbookingListModule_filter = medbookingListModule_filter ? false : true;
        } else {
            medbookingListModule_tabDoctor = medbookingListModule_tabDoctor ? false : true;
        }
        reloadModule();
    });

    $("body").on("click", "#btn_code", function () {
        $("#copy").removeClass("btn_copy").val("Скопировать");
        $("#code_msg").text(settingMod.tmp());
        for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
        }
        for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
        }
        for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
        }
        $('#popup_mod').show();
        $('body').append('<div class="overlay"></div>');
    });

    var client = new ZeroClipboard($("#copy"));
    client.on('ready', function (event) {
        client.on('copy', function (event) {
            event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        });
        client.on('aftercopy', function (event) {
            //
        });
    });
    client.on('error', function (event) {
        console.log('ZeroClipboard error of type "' + event.name + '": ' + event.message);
        ZeroClipboard.destroy();
    });

    var sliderTimeout;
    $( "#slider-range-max" ).slider({
        range: "max",
        min: 3,
        max: 15,
        value: 12,
        slide: function( event, ui ) {
            $("#amount").val( ui.value );
            settingMod.limit = ui.value;
            medbookingListModule_limit = ui.value;
            clearTimeout(sliderTimeout);
            sliderTimeout = setTimeout(function () {
                reloadModule();
            }, 100);
        }
    });
    $( "#amount" ).val( $( "#slider-range-max" ).slider( "value" ) );
    $( "#amount" ).change(function(){
        settingMod.limit = this.value;
        medbookingListModule_limit = this.value;
        clearTimeout(sliderTimeout);
        sliderTimeout = setTimeout(function () {
            reloadModule();
        }, 100);
    });

    $("#selectSpecial").change(function(){
        settingMod.specialities = ($(this).val()===null)?'':$(this).val().join(',');
        medbookingListModule_specialities = settingMod.specialities;
        reloadModule();
    });

    $("body").on("click", "#popup_close, .overlay", function () {
        $('#popup_mod').hide();
        $(".overlay").remove();
    });

    function reloadModule(){
        $('#medbookingListModule').html('');
        medbookingListModule_AfterInit();
    }

    $('.js-search_charset').on('click', function () {
        medbookingListModule_charset = $(this).val();
    });

});

