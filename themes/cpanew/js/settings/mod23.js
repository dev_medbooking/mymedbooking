$(function () {
    $('.select3').multipleSelect({
        filter: true
    });

    $('html').keydown(function (eventObject) {
        var e = e || window.event;
        if ( e && e.keyCode == 27) {
            if ($("#popup_mod").css('display') === "block") {
                $("#popup_close").click();
            }
        }
    });
    $("#selectSpecial").chosen({no_results_text: "Нет такой специальности"});
    var height_form_modul = $(".js_form_modul").offset().top;
    $(window).scroll(function () {
        if ($(window).scrollTop() > height_form_modul) {
            $(".js_form_modul").addClass("scrollFixed");
        }
        else {
            $(".js_form_modul").removeClass("scrollFixed");
        }
    });
    var limit;
    var settingMod = {
        num: num,
        limit: 10,
        settings: settings,
        //mb_category: "search_doctor",
        //mb_special: "lfk",
        tmp: function () {
            return '<div id="medbooking-service"></div> | ' +
                '<script> | '
            + 'var settings = { |'
            + '    ident_id: ' + this.settings.ident_id + ', |'
            + '    callbackPhone: ' + (this.settings.callbackPhone ? '"' + this.settings.callbackPhone + '"' : 'null') + ', |'
            + '    services: [' + this.settings.services.toString() + '], |'
            + '    categories: [' + this.settings.categories.toString() + '], |'
            + '    hide_clinics: ' + this.settings.hide_clinics + ' |'
            + '}, |'
            + 'moduleDomain = "medbooking.com",'
            + 'scriptJs = document.createElement("script"); |'

            + 'scriptJs.id = "initScript"; |'
            + 'scriptJs.charset = "UTF-8"; |'
            + 'scriptJs.src = "//" + moduleDomain + "/scripts/integrator/module23/script.js"; |'
            + 'document.head.appendChild(scriptJs); |'
            + 'scriptJs.onload = function () { |'
            + '    medbookingModule23.init( |'
            + '       moduleDomain, |'
            + '       settings |'
            + '   ) |'
            + '}; |'
            + '</script>| '
        }
    };

    $("body").on("click", "#btn_code", function () {
        $("#copy").removeClass("btn_copy").val("Скопировать");
        $("#code_msg").text(settingMod.tmp());
        for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
        }
        for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
        }
        for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
            $("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
        }
        $('#popup_mod').show();
        $('body').append('<div class="overlay"></div>');
    });

    function getSelected(element) {
        var result = [];
        $(element).find(':selected').each(function (i, selected) {
            result.push($(selected).val());
        });
        return result;
    }

    function applyWidget() {
        var container = $('#medbooking-service');

        container.html('');

        medbookingModule23.init(
            moduleDomain,
            settingMod.settings
        );
    }

    $("#use_categories").on('click', function () {
        settingMod.settings.categories = getSelected($('#MedbookingNodeService_category_id'));
        settingMod.settings.services = getSelected($('#MedbookingNodeService_service_id'));
        applyWidget();
    });

    $("#hide_clinics").on('change', function () {
        settingMod.settings.hide_clinics = $('#hide_clinics').is(':checked')?1:0;
        applyWidget();
    });

    $('#MedbookingNodeService_category_id').on('change', function () {
        var categories = [];
        $(this).find(':selected').each(function (i, selected) {
            categories.push($(selected).val());
        });
        var services = $('#MedbookingNodeService_service_id');
        if (!categories.length) {
            return;
        }
        $.get('/api/services', {category_id: categories, ident_id: settings.ident_id}).done(function (response) {
            services.html('');
            if (response.data) {
                console.log(response.data.data);
                $.each(response.data.data, function (i, v) {
                    $.each(v.services, function (i, v) {
                        $('<option>').val(v.id).text(v.title).appendTo(services).prop('selected', true);
                    });
                });
            }
            services.multipleSelect({
                filter: true
            });
        })
    });

    $("body").on("click", "#popup_close, .overlay", function () {
        $('#popup_mod').hide();
        $(".overlay").remove();
    });


    $("#selectSpecial").chosen().change(function () {
        url = "//" + moduleDomain + "/" + settingMod.mb_category + "/" + $(this).val() + "?ajax=ref";
        scriptRequest(url, ok, fail, id_client, function () {});
        settingMod.mb_special = $(this).val();
    });
    var client = new ZeroClipboard($("#copy"));
    client.on('ready', function (event) {
        client.on('copy', function (event) {
            event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        });
        client.on('aftercopy', function (event) {
            //
        });
    });
    client.on('error', function (event) {
        console.log('ZeroClipboard error of type "' + event.name + '": ' + event.message);
        ZeroClipboard.destroy();
    });

    $('#MedbookingNodeService_category_id').trigger('change');
});
$(function () {
    var availableTags  = {};
    $.getJSON( '//' + moduleDomain + '/servicesPartner/getServices?callback=?', function ( data ) {
        availableTags = data.data;
        $("#tags").autocomplete({
            source: availableTags,
            //autoFill: true,
            matchContains: 1,
            //selectFirst: true,
            select: function (event, ui) {
                event.preventDefault();
                var label = ui.item.label;
                var value = ui.item.value;
                $("#tags").val('');
                $("#services_box").append('<li data-id="' + value+ '" ><a data-translit="' + value+ '"><span class="tab">' + label + '<div class="tab_close" onclick="$(this).closest(\'li\').remove();scriptRequest(url, ok, fail, id_client);">x</div></span></a></li>');
                if (document.getElementsByClassName("myScript").length) {
                    url = document.getElementsByClassName("myScript")[0].src;
                    url = delPrm(url, "percentLimit");
                    url = delPrm(url, "callback");
                    scriptRequest(url, ok, fail, id_client);
                }
                //store in session
            },
            focus: function(event, ui) {
                event.preventDefault();
                $("#tags").val(ui.item.label);
            }
        });

    } );
});

