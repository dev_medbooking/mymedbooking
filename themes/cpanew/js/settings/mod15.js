var settingMod = {
    authors: "true",
    categories: "true",
    form: "true",
    width_widtget: "w_960",
    main_title:"",
    tmp: function() {
        return  '<div class="medbooking-minimized medbooking-minimized__static" id="medbooking_faq"></div>|' +
                '<script> | @' +
                'var num = "' + mbPhone + '",|' +
                '    id_client = ' + mbId + ',|' +
                '    authors = "' + this.authors + '",|' +
                '    categories = "' + this.categories + '",|' +
                '    form = "' + this.form + '",|' +
                '    width_widtget = "' + this.width_widtget + '",|' +
                '    main_title = "' + this.main_title + '",|' +
                'scriptJs = document.createElement("script");|' +
                'scriptJs.src = "//medbooking.com/scripts/integrator/blogList/script.js";|' +
                'document.head.appendChild(scriptJs);| !' +
                '</script>| ';
    }
};
$('html').keydown(function(eventObject) {
    if (event.keyCode == 27) {
        if ($("#popup_mod").css('display') === "block") {
            $("#popup_close").click();
        }
    }
});

$("body").on("click", "#btn_code", function() {
    $("#copy").removeClass("btn_copy").val("Скопировать");
    $("#code_msg").text(settingMod.tmp());
    for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
        $("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
    }
    for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
        $("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
    }
    for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
        $("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
    }
    $('#popup_mod').show();
    $('body').append('<div class="overlay"></div>');
});
$("body").on("click", "#popup_close, .overlay", function() {
    $('#popup_mod').hide();
    $(".overlay").remove();
});
var client = new ZeroClipboard($("#copy"));
client.on('ready', function(event) {
    client.on('copy', function(event) {
        event.clipboardData.setData('text/plain', $("code_msg").text());
        $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
    });
    client.on('aftercopy', function(event) {
        //
    });
});
client.on('error', function(event) {
    console.log('ZeroClipboard error of type "' + event.name + '": ' + event.message);
    ZeroClipboard.destroy();
});
$("body").on("change", "#showForm", function() {
    if ($(this).is(":checked")) {
        $("#minimized").hide();
        settingMod.form = "false";
        form="false";
    } else {
        $("#minimized").show();
        settingMod.form = "true";
        form="true";
    }
});
$("body").on("change", "#showAuthors", function() {
    if ($(this).is(":checked")) {
        $(".blog_post_info").hide();
        settingMod.authors = "false";
        authors="false";
    } else {
        $(".blog_post_info").show();
        settingMod.authors = "true";
        authors="true";
    }
});
$("body").on("change", "#showCategories", function() {
    if ($(this).is(":checked")) {
        $(".new_infopage_aside").hide();
        settingMod.categories = "false";
        categories="false";
    } else {
        $(".new_infopage_aside").show();
        settingMod.categories = "true";
        categories="true";
    }
});
$("body").on("change", "#widthWidtget", function() {
    $('#width_widtget').removeClass("w_960 w_800 w_640 w_460 w_320").addClass($(this).val());
    settingMod.width_widtget = $(this).val();
    width_widtget=$(this).val();
});
$("body").on("keyup keypress keydown", "#name_btn", function() {
    $('#mb_main_title').text($(this).val());
    settingMod.main_title = $(this).val();
    main_title=$(this).val();
});