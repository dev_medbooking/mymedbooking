<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]--><!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]--><!--[if IE 8]><html class="no-js lt-ie9"><![endif]--><!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1">
        <title><?=(preg_match('#^https?://crm#', Yii::app()->request->hostInfo) ? 'CRM.' : 'API.');?>MYMEDBOOKING</title>
        <link rel="stylesheet" href="<?=Yii::app()->theme->baseUrl;?>/css/main.css" type="text/css">
        <link rel="stylesheet" href="<?=Yii::app()->theme->baseUrl;?>/css/media-queries.css"  type="text/css">
        <link rel="stylesheet" href="<?=Yii::app()->theme->baseUrl;?>/css/chosen.css" type="text/css">
        <script src="<?=Yii::app()->theme->baseUrl;?>/js/jquery-1.11.2.min.js"></script>
        <script src="<?=Yii::app()->theme->baseUrl;?>/js/chosen.jquery.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
        <script src="<?=Yii::app()->theme->baseUrl;?>/js/maskedinput.min.js"></script>
        <script src="<?=Yii::app()->theme->baseUrl;?>/js/struts2/jquery-ui-timepicker-addon.min.js"></script>
        <script src="<?=Yii::app()->theme->baseUrl;?>/js/dew-scrumy/jquery-ui-sliderAccess.js"></script>
        <script src="<?=Yii::app()->theme->baseUrl;?>/js/footable.js"></script>
        <script src="<?=Yii::app()->theme->baseUrl;?>/js/colpick.js"></script>
        <script src="https://api-maps.yandex.ru/2.1.68/?lang=ru_RU"></script>
        <script src="<?=Yii::app()->theme->baseUrl;?>/js/main.js"></script>
        <link rel="stylesheet" href="<?=Yii::app()->theme->baseUrl;?>/ui/jquery-ui.min.css">
        <link rel="stylesheet" href="<?=Yii::app()->theme->baseUrl;?>/ui/jquery-ui.structure.min.css">
        <link rel="stylesheet" href="<?=Yii::app()->theme->baseUrl;?>/ui/jquery-ui.theme.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/colpick.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/footable.core.min.css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.png" rel="shortcut icon" type="image/x-icon" />
    </head>
    <body>
        <?=$content;?>
    </body>
</html>