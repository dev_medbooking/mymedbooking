
<div class="form_content support_form clearfix">
<a href="#" id="touch-menu-content" class=""><span>Задать вопрос</span></a>
<ul class="cpa_sidebar">
	<li><a href="/">Войти в личный кабинет</a></li>
</ul>
<div class="close_support"></div>	
	<?php
	$form = $this->beginWidget('CActiveForm',array(
		'id' => 'admin-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'htmlOptions' => array(
			'class' => 'cpa_form_support',
		)
	));
	?>
	<?=$form->textField($model, 'name', array('placeholder' => 'Ваше имя', 'autocomplete' => 'off', 'class' => 'request')); ?>
	<?=$form->error($model, 'name'); ?>
	<?=$form->textField($model, 'phone', array('placeholder' => 'Номер телефона', 'autocomplete' => 'off', 'class' => 'request')); ?>
	<?=$form->error($model, 'phone'); ?>
	<?=$form->textArea($model, 'message', array('placeholder' => '...', 'autocomplete' => 'off', 'type' => 'text', 'class' => 'request')); ?>
	<?=$form->error($model, 'message'); ?>
	<?php echo CHtml::submitButton('Отправить'); ?>
	<?php $this->endWidget(); ?>
	<?php if( ! Yii::app()->request->isAjaxRequest) :?>

	<?php else :?>
		<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
	<?php endif;?>
</div>