<div class="form_content">
	<a href="#" id="touch-menu-content"><span>Войти в личный кабинет</span></a>
	<ul class="cpa_sidebar">
		<li><a class="active">Войти в личный кабинет</a></li>
	</ul>
	<?php
	$form=$this->beginWidget('CActiveForm', array(
		'htmlOptions' => array(
			'class' => 'cpa_form_enter'
		),
		'enableAjaxValidation' => false,
	));
	?>
	<?php if ('otp_check' == $model->scenario) { ?>
		<div>
			На номер <?=CPhone::getMaskedPhone($model->otp_phone);?> отправлен одноразовый пароль.<br/>
			<a href="#resend" class="js-otp_resend">перевыслать ОТП</a>
		</div>
	<?=$form->hiddenField($model, 'otp_send', array('value' => '0'));?>
	<?php if ($model->hasErrors('otp_send')) { ?>
	<br/>
	<?=$form->error($model, 'otp_send'); ?>
	<?php } ?>
	<?=$form->textField($model, 'otp_otp', array('placeholder' => 'Одноразовый пароль', 'autocomplete' => 'off'));?>
	<?=$form->error($model, 'otp_otp'); ?>
		<script>
			$('.js-otp_resend').on('click', function () {
				$('#LoginForm_otp_send').val('1');
				$('#LoginForm_otp_send').closest('form').submit();
				return false;
			});
		</script>
	<?php } else { ?>
		<?=$form->hiddenField($model, 'scenario', array('value' => 'start'));?>
		<?=$form->textField($model, 'username', array('placeholder' => 'E-mail', 'type1' => 'email', 'autocomplete' => 'off'));?>
		<?=$form->passwordField($model, 'password', array('placeholder' => 'Пароль', 'type' => 'password'));?>
		<input type="hidden" name="loc" value="">
		<?=$form->error($model, 'password'); ?>
	<?php } ?>
	<?=CHtml::submitButton("Войти в личный кабинет");?>
	<?php $this->endWidget();?>
</div>
