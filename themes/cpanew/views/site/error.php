<div class="error_page_title">
    <div class="error_content_wrap">
        <strong><?php echo $code;?></strong>
        <p class="not_found">СТРАНИЦА НЕ НАЙДЕНА</p>
        <p class="title">Приносим свои извинения и желаем Вам крепкого здоровья!</p>
        <div class="right_coloumn">
            <a href="/">Вернуться на главную</a>
        </div>
    </div>
</div>
<p class=" error_color"><?php echo CHtml::encode($message);?></p>