<tr>
    <th data-sort-initial="descending" data-class="expand"><?=$sort->link('id', null, array('class'=>'number'));?></th>
    <th><?=$sort->link('create_time', null, array('class'=>'date'));?></th>
    <th data-hide="phone,tablet"><?=$sort->link('status', null, array('class'=>'status'));?></th>
    <?php if (Yii::app()->user->checkAccess('Administrator')) :?>
	    <th data-hide="phone,tablet"><span>Партнер</span></th>
    <?php endif;?>
</tr>
