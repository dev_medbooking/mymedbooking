<section class="wrapper_p">
    <header class="header_p">
        <?php $this->renderPartial('_header', array(
            'partner' => $partner,
            'partners' => $partners,
            'phone' => $phone,
        ));?>
    </header>
    <section class="menu">
        <div class="container">
            <a href="#" id="touch-menu-status" class=""><span>Статус</span></a>
            <ul class="menu_left">
                <li><a class="<?=(empty($status) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => $limit, 'status' => ''));?>">Все</a></li>
                <li><a class="<?=(( ! empty($status) AND $status == 1) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => $limit, 'status' => 1));?>">Требующие ответа</a></li>
                <li><a class="<?=(( ! empty($status) AND $status == 2) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => $limit, 'status' => 2));?>">Записанные</a></li>
                <li><a class="<?=(( ! empty($status) AND $status == 3) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => $limit, 'status' => 3));?>">Отклоненные</a></li>
            </ul>
        </div>
    </section>
    <section class="content_p">
        <div class="agreement">
            <div class="container">
                Градация по выплате партнерам:<br>
                до 20 лидов в месяц - 100р/лид<br>
                от 21 до 40 лидов -150р/лид за каждого последующего после 20 лидов<br>
                от 40 до 70 лидов -200р за каждого последующего после 40 лидов<br>
                от 71 до 100 лидов - 250р за каждого последующего после 70 лидов<br>
                свыше 100 лидов - 300р за каждого последующего свыше 100 лидов
            </div>
        </div>

    </section>
    <?php $this->renderPartial('_footer');?>
</section>
