<?php
/**
 * @var Partner             $partner
 * @var Partner[]           $partners
 * @var string              $phone
 * @var Recommendation      $dataProvider
 * @var CActiveDataProvider $dataProviderSiteUrls
 * @var PartnerSite         $partnerSite
 * @var PartnerSite         $model
 * @var Recommendation      $recommendation
 */
?>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste "
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter      alignright alignjustify | bullist numlist outdent indent | link image"
    });
</script>

<section class="wrapper_p">
    <header>
        <?php $this->renderPartial(
            '_header', array(
                'partner'  => $partner,
                'partners' => $partners,
                'phone'    => $phone,
            )
        ); ?>
    </header>
    <?php $this->renderPartial('_recommendation-header') ?>
    <section class="groop-table">
        <div class="container">
            <?php $this->widget(
                'zii.widgets.CListView', array(
                    'dataProvider' => $dataProvider,
                    'itemView'     => '_recommendation-item',
                )
            );
            ?>
        </div>
    </section>
    <?php $this->renderPartial('_footer') ?>
</section>
