<?php
$unreadClass = Recommendation::model()->haveUnread() ? 'unread' : '';
?>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<div class="preview_stat">
	<div class="container">
		<div class="left_stat">Всего визитов: <span class="preview_bold"><?=( ! empty($partner) ? $partner->visitsCount : 0);?></span></div>
		<div class="left_stat">Привлеченных <span class="preview_bold">пациентов <?=( ! empty($partner) ? $partner->recordInfo['all_record'] : '0');?></span>, к оплате <?=( ! empty($partner) ? $partner->recordInfo['success_record'] : '0');?>, на сумму <span class="preview_bold"><?=( ! empty($partner) ? $partner->recordInfo['price'] : '0');?> руб.</span></div>
	</div>
</div>
<div class="personal_menu">
	<div class="container">
        <?php if ($partner) :?>
            <span class="userInfo"><?=$partner->name?> 
                <span class="userEmail"><span class="userPhone">Телефон: <?=$partner->phone;?></span>&nbsp;&nbsp;&nbsp;<?=$partner->usr->email?> – ID: <?=$partner->ident_id?></span>
            </span>
        <?php endif;?>
        <?php if ( ! empty($partners) AND ! Yii::app()->session->get("partnerID")) :?>
            <div class="gotoUser">
                <form action="<?=Yii::app()->createUrl('mbTable/viewAsPartner');?>" method="post">
                    <select name="ident_id">
                        <?php foreach ($partners as $v) :?>
                            <option value="<?=$v->ident_id;?>"><?=$v->name;?></option>
                        <?php endforeach;?>
                    </select>
                    <script text="text/javascript">
                        $("select[name='ident_id']").select2();
                    </script>
                    <input type="submit" name="gouser" value="">
                </form>
            </div>
        <?php elseif (Yii::app()->session->get("partnerID")) :?>
            <div class="gotoUser back">
	            <form action="<?=Yii::app()->createUrl('mbTable/logoutAsPartner');?>" method="post">
		            <input type="submit" name="gouserback" value="Вернуться к исходному юзеру">
	            </form>
            </div>
        <?php endif;?>
	</div>
</div>
<div class="personal_panel">
	<div class="container">
		<a class="cpa_logo" href="/mbTable"><img src="<?=Yii::app()->theme->baseUrl;?>/img/logo.png" alt="CPA Medbooking"></a>
		<a href="#" id="touch-menu"><span></span></a>
		<ul class="nav">
			<li><a class="nav_contract <?=((Yii::app()->controller->action->id == 'agreement' AND Yii::app()->controller->id == 'mbTable') ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/agreement');?>"></a></li>
			<li><a class="nav_docs <?=((Yii::app()->controller->action->id == 'admin' AND Yii::app()->controller->id == 'mbTable') ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable');?>"></a></li>
			<li><a class="nav_modul <?=((Yii::app()->controller->action->id == 'module' AND Yii::app()->controller->id == 'mbTable') ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/module');?>"></a></li>
			<li><a class="nav_disallow <?=((Yii::app()->controller->action->id == 'disallow' AND Yii::app()->controller->id == 'mbTable') ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/disallow');?>"></a></li>
			<li><a class="nav_gear <?=((Yii::app()->controller->action->id == 'settings' AND Yii::app()->controller->id == 'mbTable') ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/settings');?>"></a></li>
			<li><a class="nav_exit" href="/logout"></a></li>
		</ul>
	</div>
</div>