<?php
/**
 * @var Partner             $partner
 * @var Partner[]           $partners
 * @var string              $phone
 * @var CActiveDataProvider $dataProviderSite
 * @var CActiveDataProvider $dataProviderSiteUrls
 * @var PartnerSite         $partnerSite
 * @var PartnerSite         $model
 * @var PartnerSiteUrl      $partnerSiteUrl
 */
?>
<section class="wrapper_p">
    <header class="header_p">
        <?php $this->renderPartial(
            '_header', array(
                'partner'  => $partner,
                'partners' => $partners,
                'phone'    => $phone,
            )
        ); ?>
    </header>
</section>

<section class="groop-table sites-block">
    <div class="inline-block">
        <div>
            <?= Yii::app()->user->getFlash('error') ?>
        </div>
        <div class="col-lg-3 col-lg-offset-1">
            <form method="post">
                <table data-filter="#filter" class="cpa_table footable">
                    <thead>
                    <tr>
                        <th colspan="2">Список сайтов</th>
                    </tr>
                    </thead>
                    <tbody class="js-update-data">
                    <tr>
                        <td colspan="2">
                            <input type="text" name="PartnerSite[host]" class="form-control"
                                   placeholder="Введите адрес сайта">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-success full">
                                <i class="glyphicon glyphicon-plus"></i>добавить ещё
                            </button>
                        </td>
                    </tr>
                    <?php
                    foreach ($dataProviderSite->data as $model) {
                        ?>
                        <tr class="<?= $partnerSite && $partnerSite->id == $model->id ? 'active' : '' ?>">
                            <td>
                                <?= Html::a($model->host, Url::to('mbTable/sites', ['id' => $model->id])) ?>
                            </td>
                            <td class="text-center hided">
                                <?= Html::a(
                                    '', $model->getUrl('delete'),
                                    ["class" => "glyphicon glyphicon-trash confirm-delete"]
                                ) ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="col-lg-7">

            <form method="post">
                <table data-filter="#filter" class="cpa_table footable">
                    <thead>
                    <tr>
                        <th>URL</th>
                        <th colspan="2">Примечание
                        <td>
                    </tr>
                    </thead>
                    <tbody class="js-update-data">
                    <?php if ($partnerSite) { ?>
                        <tr>
                            <td colspan="3" class="text-center">
                                <div><h2><?= $partnerSite->host ?></h2></div>
                                <div>Добавьте URL на страницу, где Вы разместили наш модуль(-и) или API</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="PartnerSiteUrl[url]" class="form-control"
                                       placeholder="Введите URL страницы">
                            </td>
                            <td colspan="2">
                                <input type="text" name="PartnerSiteUrl[comment]" class="form-control"
                                       placeholder="Примечание">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <button type="submit" class="btn btn-success">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    добавить ещё
                                </button>
                            </td>
                        </tr>
                        <?php
                        foreach ($dataProviderSiteUrls->data as $partnerSiteUrl) {
                            ?>
                            <tr>
                                <td>
                                    <?= $partnerSiteUrl->getFullUrl() ?>
                                </td>
                                <td>
                                    <?= $partnerSiteUrl->comment ?>
                                </td>
                                <td class="text-center hided">
                                    <?= Html::a(
                                        '', $partnerSiteUrl->getUrl('delete'),
                                        ["class" => "glyphicon glyphicon-trash confirm-delete"]
                                    ) ?>
                                </td>
                            </tr>
                            <?php
                        }
                    } ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</section>
<?php $this->renderPartial('_footer'); ?>
