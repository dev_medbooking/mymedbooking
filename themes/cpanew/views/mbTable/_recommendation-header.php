<section class="menu">
    <div class="container">
        <ul class="menu_left">
            <li>
                <a class="<?= Url::is('mbTable/recommendations', 'active') ?>"
                   href="<?= Url::to('mbTable/recommendations') ?>">
                    Все рекомендации
                </a>
            </li>
            <?php if (Yii::app()->user->checkAccess("Administrator")) { ?>
                <li>
                    <a class="<?= Url::is('mbTable/recommendationAdd', 'active') ?>"
                       href="<?= Url::to('mbTable/recommendationAdd') ?>">
                        Добавить новую
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
</section>