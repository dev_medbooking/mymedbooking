<section class="modul">
    <div class="js_form_modul">
         <div class="wrapper_mod clearfix">
          <div class="form_modul">
           <div class="form_modul_preview">
            <p>«Форма подбора» - представляет собой форму запроса обратной связи для онлайн подбора врачей. Наиболее актуальное размещение под основным контентом. Например, пользователь, только что, изучив Вашу статью об отбеливании зубов, заинтересован как можно скорее подобрать центр поближе к дому, учитывая желаемую стоимость. После заполнения подобной формы – мы обязательно свяжемся для подбора стоматологии  в течении 15 минут.</p>
            <input id="btn_code" type="button" value="Получить код">
            </div>
               <div class="form_setting">
                <fieldset class="form_size">
                    <h2>Настройки</h2>
                    <h3>Выберите размер:</h3>
                    <select class="js-width_size" name="menu" size="1">
                        <option selected="selected" value="800">800</option>
                        <option value="640">640</option>
                        <option value="480">480</option>
                        <option value="320">320</option>
                        <option value="200">200</option>
                    </select><label for="menu">PX</label>
                    <h3>Выберите размер шрифта:</h3>
                    <select class="js-font_size" name="menu" size="1">
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option selected="selected" value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                    </select><label for="menu">PX</label>
                </fieldset>
                <fieldset class="form_size">
                          <h3>Название заголовка:</h3>
                           <input id="name_titleView" type="text" placeholder="Проконсультируйтесь у спец...">
                            <h3>Выберите начертание:</h3>
                    <span class="bold js-bold">B</span>
                    <span class="italic js-italic">I</span>
                    <span class="underline js-underline">U</span>
                </fieldset>                
                <fieldset class="form_size_6">
                    <h3>Выбрать специальность</h3>
                    <select id="selectSpecial" class="selectSpecial">
                    <option value="">Все специальности</option>
                        <option value="akusher-ginekolog">Акушер-гинеколог</option>
                        <option value="allergolog">Аллерголог</option>
                        <option value="androlog">Андролог</option>
                        <option value="anesteziolog">Анестезиолог</option>
                        <option value="venerolog">Венеролог</option>
                        <option value="vertebronevrolog">Вертеброневролог</option>
                        <option value="lfk">Врач ЛФК</option>
                        <option value="gastroenterolog">Гастроэнтеролог</option>
                        <option value="gematolog">Гематолог</option>
                        <option value="genetik">Генетик</option>
                        <option value="gepatolog">Гепатолог</option>
                        <option value="ginekolog">Гинеколог</option>
                        <option value="girudoterapevt">Гирудотерапевт</option>
                        <option value="gomeopat">Гомеопат</option>
                        <option value="dermatolog">Дерматолог</option>
                        <option value="dijetolog">Диетолог</option>
                        <option value="immunolog">Иммунолог</option>
                        <option value="infekcionnist">Инфекционист</option>
                        <option value="kardiolog">Кардиолог</option>
                        <option value="kosmetolog">Косметолог</option>
                        <option value="logoped">Логопед</option>
                        <option value="mammolog">Маммолог</option>
                        <option value="manualynii-terapevt">Мануальный терапевт</option>
                        <option value="massazhist">Массажист</option>
                        <option value="mikolog">Миколог</option>
                        <option value="narkolog">Нарколог</option>
                        <option value="nevrolog">Невролог</option>
                        <option value="nefrolog">Нефролог</option>
                        <option value="onkolog">Онколог</option>
                        <option value="ortodont">Ортодонт</option>
                        <option value="ortoped">Ортопед</option>
                        <option value="osteopat">Остеопат</option>
                        <option value="otolaringolog">Отоларинголог (лор)</option>
                        <option value="oftalymolog">Офтальмолог (окулист)</option>
                        <option value="pediatr">Педиатр</option>
                        <option value="plasticheskii-khirurg">Пластический хирург</option>
                        <option value="proktolog">Проктолог</option>
                        <option value="psychiatr">Психиатр</option>
                        <option value="psiholog">Психолог</option>
                        <option value="psihoterapevt">Психотерапевт</option>
                        <option value="pulymonolog">Пульмонолог</option>
                        <option value="revmatolog">Ревматолог</option>
                        <option value="rentgenolog">Рентгенолог</option>
                        <option value="reproduktolog">Репродуктолог</option>
                        <option value="seksolog">Сексолог</option>
                        <option value="stomatolog">Стоматолог</option>
                        <option value="terapevt">Терапевт</option>
                        <option value="travmatolog">Травматолог</option>
                        <option value="triholog">Трихолог</option>
                        <option value="uz-diagnost">УЗИ-диагност</option>
                        <option value="urolog">Уролог</option>
                        <option value="flebolog">Флеболог</option>
                        <option value="hirurg">Хирург</option>
                        <option value="endokrinolog">Эндокринолог</option>
                    </select>
                    <div class="slider">
                            <h3>Количество врачей:</h3>
                            <input type="text" id="amount" readonly>
                        <div id="slider-range-max"></div>
                    </div>
                </fieldset>
                <fieldset class="form_tool_6"> 
                    <input checked type="checkbox" id="form" />
                    <label for="form">Показывать форму<br>&nbsp;&nbsp;&nbsp;записи онлайн</label>
                    <span class="text_click">Всплывающая форма позволяет записаться онлайн всего одним щелчком мыши.</span> 
                </fieldset>
            </div>
           </div>
        </div>
    </div>
    <div class="wrapper_mod clearfix">
		<div class="modul_preview">
        <div class='browser-wrapper'>
            <div class='top-bar'>
                <div class='row small'>
                    <div class='circle close'></div>
                    <div class='circle minify'></div>
                    <div class='circle zoom'></div>
                    <div class='tab'>
                    
                    </div>
                </div>
                <div class='row flex active large'>
                    <div class='button back'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button forward'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button reload-button'>
                        <div class='arrow-down'></div>
                    </div>
                    <div class='address-bar'>
                        <span class='favicon'></span>
                        
                    </div>
                    <div class='sandwich-button'>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                    </div>
                </div>
            </div>
            	<div class="site_block">
					<div class="site_block_logo">LOGO</div>
					<div class="site_block_log"></div>
					<div class="site_block_search"></div>
				</div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="content_browser_wrapper">
				<div class='content_browser'>


	<div id="medbooking-doctor"></div>
	<script>  var id_client = <?=$partner->id;?>,num = "<?=$partner->phone;?>",mb_fwT = "normal",mb_fzT = "18px",mb_tdT = "none",mb_fsT = "normal",mb_snippetWidth = "800",mb_countDoc = "10",mb_specialSnippet = "lfk",mb_textT = "Проконсультируйтесь у специалиста",mb_dragForm = "true",url = "//medbooking.com/search_doctor/lfk?ajax_type=line&ajax=ref", scriptJs = document.createElement("script");scriptJs.src = "//medbooking.com/scripts/integrator/moduleDocCategoryForm/script.js";document.head.appendChild(scriptJs); </script>


				</div>
           </div>
            </div>
        </div>
		</div>
	</div>
</section>
<div id="popup_mod">
    <div class="popup_head_mod">
        <h2>Код</h2>
        <i id="popup_close"></i>
        <span>Вставьте код на сайт</span>
    </div>
    <div class="popup_body-mod">
        <form>
        <div class="code_msg" id="code_msg">
        </div>
            <input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
        </form>
    </div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>";
</script>