<section class="modul">
  <div class="js_form_modul">
   <div class="wrapper_mod clearfix">
            <div class="form_modul">
               <div class="form_modul_preview">
                <p>«Список врачей/клиник/услуг» - виджет, который позволяет выводить на вашем сайте каталоги врачей, как по всем специализациям, так и узким – например, если на Вашем сайте присутствует контент о программах снижения веса, у Вас есть возможность разместить диетологов, тем самым пользователю не придется выбирать среди всех специализаций. 
Так же, возможно разместить только те медицинские центры, где принимают диетологи.  Для пользователя предоставляется телефон + форма записи на прием онлайн.</p>
                <input type="button" class="btn_opacity"  id="btn_code" value="Получить код">
                </div>
                   <div class="form_setting">
                    <fieldset class="form_size_2"> 
                        <h2>Настройки</h2>
                        <h3>Путь к скрипту<sup>*</sup>:</h3>
                        <input type="text"  id="js-site-folder">
                        <span id="err-site-folder">Заполните поле</span>
                <p>Данное поле обязательно для заполнения. Для получения дополнительной информации по настройке модуля, обратитесь за помощью к нашему менеджеру по телефону <span>+7 (963) 667-56-65</span>.</p>
                    </fieldset>
                    <fieldset class="form_size">
                        <h3>Выбрать категорию:</h3>
                        <input checked name="category_toogle" type="radio" id="search_doctor">
                        <label  for="search_doctor">Врачи</label>
                        <input  name="category_toogle" type="radio" id="search_clinic">
                        <label for="search_clinic">Клиники</label>
                        <h3>Выберите размер:</h3>
                        <select id="toggle_width" name="menu" size="1">
                            <option selected="selected" value="870">870</option>
                            <option value="800">800</option>
                            <option value="750">750</option>
                            <option value="700">700</option>
                        </select><label for="menu">PX</label>
                    <h3>Выбрать специальность</h3>
                    <select id="selectSpecial" class="selectSpecial">
                    <option value="">Все специальности</option>
                        <option value="akusher-ginekolog">Акушер-гинеколог</option>
                        <option value="allergolog">Аллерголог</option>
                        <option value="androlog">Андролог</option>
                        <option value="anesteziolog">Анестезиолог</option>
                        <option value="venerolog">Венеролог</option>
                        <option value="vertebronevrolog">Вертеброневролог</option>
                        <option value="lfk">Врач ЛФК</option>
                        <option value="gastroenterolog">Гастроэнтеролог</option>
                        <option value="gematolog">Гематолог</option>
                        <option value="genetik">Генетик</option>
                        <option value="gepatolog">Гепатолог</option>
                        <option value="ginekolog">Гинеколог</option>
                        <option value="girudoterapevt">Гирудотерапевт</option>
                        <option value="gomeopat">Гомеопат</option>
                        <option value="dermatolog">Дерматолог</option>
                        <option value="dijetolog">Диетолог</option>
                        <option value="immunolog">Иммунолог</option>
                        <option value="infekcionnist">Инфекционист</option>
                        <option value="kardiolog">Кардиолог</option>
                        <option value="kosmetolog">Косметолог</option>
                        <option value="logoped">Логопед</option>
                        <option value="mammolog">Маммолог</option>
                        <option value="manualynii-terapevt">Мануальный терапевт</option>
                        <option value="massazhist">Массажист</option>
                        <option value="mikolog">Миколог</option>
                        <option value="narkolog">Нарколог</option>
                        <option value="nevrolog">Невролог</option>
                        <option value="nefrolog">Нефролог</option>
                        <option value="onkolog">Онколог</option>
                        <option value="ortodont">Ортодонт</option>
                        <option value="ortoped">Ортопед</option>
                        <option value="osteopat">Остеопат</option>
                        <option value="otolaringolog">Отоларинголог (лор)</option>
                        <option value="oftalymolog">Офтальмолог (окулист)</option>
                        <option value="pediatr">Педиатр</option>
                        <option value="plasticheskii-khirurg">Пластический хирург</option>
                        <option value="proktolog">Проктолог</option>
                        <option value="psychiatr">Психиатр</option>
                        <option value="psiholog">Психолог</option>
                        <option value="psihoterapevt">Психотерапевт</option>
                        <option value="pulymonolog">Пульмонолог</option>
                        <option value="revmatolog">Ревматолог</option>
                        <option value="rentgenolog">Рентгенолог</option>
                        <option value="reproduktolog">Репродуктолог</option>
                        <option value="seksolog">Сексолог</option>
                        <option value="stomatolog">Стоматолог</option>
                        <option value="terapevt">Терапевт</option>
                        <option value="travmatolog">Травматолог</option>
                        <option value="triholog">Трихолог</option>
                        <option value="uz-diagnost">УЗИ-диагност</option>
                        <option value="urolog">Уролог</option>
                        <option value="flebolog">Флеболог</option>
                        <option value="hirurg">Хирург</option>
                        <option value="endokrinolog">Эндокринолог</option>
                    </select>
                </fieldset>
                    <fieldset class="form_tool_2"> 
                        <input type="checkbox" id="toggle_sidebar" checked/>
                        <label for="toggle_sidebar">Показывать side bar<br>&nbsp;</label>
                        <input type="checkbox" id="toggle_drag" checked/>
                        <label for="toggle_drag">Показывать форму<br>&nbsp;&nbsp;&nbsp;записи онлайн</label>
                        <span class="text_click">Всплывающая форма позволяет записаться онлайн всего одним щелчком мыши.</span> 
                    </fieldset>
                    </div>
                 </div>
            </div>
        </div>
        <div class="wrapper_mod clearfix">
		<div class="modul_preview">	
		<div class='browser-wrapper'>
            <div class='top-bar'>
                <div class='row small'>
                    <div class='circle close'></div>
                    <div class='circle minify'></div>
                    <div class='circle zoom'></div>
                    <div class='tab'>
                    
                    </div>
                </div>
                <div class='row flex active large'>
                    <div class='button back'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button forward'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button reload-button'>
                        <div class='arrow-down'></div>
                    </div>
                    <div class='address-bar'>
                        <span class='favicon'></span>
                        
                    </div>
                    <div class='sandwich-button'>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                    </div>
                </div>
            </div>
            <div class="site_block">
                <div class="site_block_logo">LOGO</div>
                <div class="site_block_log"></div>
                <div class="site_block_search"></div>
            </div> 
            <div class='content_browser'>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
				<div class="modul_preview" id="medbooking-doctor">
					<?php
					$partner_url = "/mbTable/module/2";
					$_SERVER["REQUEST_URI"] = substr($_SERVER["REQUEST_URI"], strlen($partner_url));
					if(empty($_SERVER["REQUEST_URI"]) || $_SERVER["REQUEST_URI"] == "/") {
						$url='//medbooking.com/search_doctor';
					} else {
						$url='//medbooking.com/'.$_SERVER['REQUEST_URI'];
					}
					$ch=curl_init();
					curl_setopt($ch,CURLOPT_URL,$url."?ajax=curl&partner_url=".($partner_url == "/" ? "" : $partner_url));
					curl_setopt($ch,CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
					curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);
					curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
					curl_setopt($ch,CURLOPT_VERBOSE,1);
					curl_setopt($ch,CURLINFO_HEADER_OUT,1);
					$response=curl_exec($ch);
					if(!empty($response)){
						echo $response;
					}
					?>
					<script>
                    var id_client =  <?=$partner->id;?>,num = "<?=$partner->phone;?>",
                        dragForm = true,
                        contentWidth = null,
                        aside = true,
                        folder = "/mbTable/module/2/",
                        mb_category = "",
                        mb_special = "",
                        scriptJs = document.createElement("script");
                        scriptJs.id = "initScript";
                        scriptJs.src = "//medbooking.com/scripts/integrator/part2/part2.js";
                        document.head.appendChild(scriptJs);
                    </script>
				</div>
            </div>
        </div>
		</div>
	</div>
</section>
<div id="popup_mod">
	<div class="popup_head_mod">
		<h2>Код</h2>
		<i id="popup_close"></i>
		<span>Вставьте код на сайт</span>
	</div>
	<div class="popup_body-mod">
		<form>
		<div class="code_msg" id="code_msg">
		</div>
			<input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
		</form>
	</div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>"; 
</script>