<section class="modul">
    <div class="js_form_modul">
         <div class="wrapper_mod clearfix">
          <div class="form_modul">
           <div class="form_modul_preview">
            <p>Разместив код модуля «Партнерский телефон», на Вашем сайте появится информер с индивидуальным номером телефона, привязанным к вашему аккаунту. За каждую запись на прием к врачу или в клинику, которая поступит по данному номеру телефона, Вы будите получать вознаграждение. Каждый звонок на «индивидуальный номер партнера» автоматически формируется в заявку и отображается в личном кабинете.</p>
            <input id="btn_code" type="button" value="Получить код">
            </div>
           </div>
        </div>
    </div>
    <div class="wrapper_mod clearfix">
		<div class="modul_preview">
        <div class='browser-wrapper'>
            <div class='top-bar'>
                <div class='row small'>
                    <div class='circle close'></div>
                    <div class='circle minify'></div>
                    <div class='circle zoom'></div>
                    <div class='tab'>
                    
                    </div>
                </div>
                <div class='row flex active large'>
                    <div class='button back'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button forward'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button reload-button'>
                        <div class='arrow-down'></div>
                    </div>
                    <div class='address-bar'>
                        <span class='favicon'></span>
                        
                    </div>
                    <div class='sandwich-button'>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                    </div>
                </div>
            </div>
            	<div class="site_block">
					<div class="site_block_logo">LOGO</div>
					<div class="site_block_log"></div>
					<div class="site_block_search"></div>
				</div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="content_browser_wrapper">
				<div class='content_browser'>


				</div>
           </div>
            </div>
        </div>
		</div>
	</div>
</section>
<div id="popup_mod">
    <div class="popup_head_mod">
        <h2>Код</h2>
        <i id="popup_close"></i>
        <span>Вставьте код на сайт</span>
    </div>
    <div class="popup_body-mod">
        <form>
        <div class="code_msg" id="code_msg">
        </div>
            <input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
        </form>
    </div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>";
</script>