<section class="modul">
    <div class="js_form_modul">
         <div class="wrapper_mod clearfix">
          <div class="form_modul">
           <div class="form_modul_preview">
            <p>Инструмент для поиска врача: запрос обратной связи для подбора медицинского центра. Модуль настраивается как под сайдбар, так и для размещения в основном контенте. Данную форму можно зафиксировать в любой части сайта, а так же прижать к верхней или нижней границе Вашего сайта.</p>
            </div>
               <div class="form_setting">
                <fieldset class="form_size">
                    <h2>Настройки</h2>
                    <h3>Выберите размер:</h3>
                    <select class="js-width_size" name="menu" size="1">
                        <option selected="selected" value="800">800</option>
                        <option value="640">640</option>
                        <option value="480">480</option>
                        <option value="320">320</option>
                        <option value="280">280</option>
                        <option value="200">200</option>
                    </select><label for="menu">PX</label>
            	<input id="btn_code" type="button" value="Получить код">
                </fieldset>
                <fieldset class="form_size">
                    <h3>Выберите размер шрифта:</h3>
                    <select class="js-font_size" name="menu" size="1">
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option selected="selected" value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                    </select><label for="menu">PX</label>
					<h3>Название заголовка:</h3>
					<input id="name_titleView" type="text" placeholder="Проконсультируйтесь у спец...">
                </fieldset>                
		         <fieldset class="form_size">
						<h3>Выберите начертание:</h3>
						<span class="bold js-bold">B</span>
						<span class="italic js-italic">I</span>
						<span class="underline js-underline">U</span>
                        <h3>Прижимать к:</h3>
						<select class="js-form-position" name="form_position" size="1">
							<option selected="selected" value="position_non">Не прижимать</option>
							<option value="position_up">Вверху</option>
							<option value="position_down">Низу</option>
						</select>
                    </fieldset>  
                <fieldset class="form_tool_6"> 
                    <input checked type="checkbox" id="form" />
                    <label for="form">Показывать форму<br>&nbsp;&nbsp;&nbsp;записи онлайн</label>
                    <span class="text_click">Всплывающая форма позволяет записаться онлайн всего одним щелчком мыши.</span> 
                </fieldset>
            </div>
           </div>
        </div>
    </div>
    <div class="wrapper_mod clearfix">
		<div class="modul_preview">
        <div class='browser-wrapper'>
            <div class='top-bar'>
                <div class='row small'>
                    <div class='circle close'></div>
                    <div class='circle minify'></div>
                    <div class='circle zoom'></div>
                    <div class='tab'>
                    
                    </div>
                </div>
                <div class='row flex active large'>
                    <div class='button back'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button forward'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button reload-button'>
                        <div class='arrow-down'></div>
                    </div>
                    <div class='address-bar'>
                        <span class='favicon'></span>
                        
                    </div>
                    <div class='sandwich-button'>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                    </div>
                </div>
            </div>
            	<div class="site_block">
					<div class="site_block_logo">LOGO</div>
					<div class="site_block_log"></div>
					<div class="site_block_search"></div>
				</div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="content_browser_wrapper">
				<div class='content_browser'>

<div id="medbooking-doctor"></div>
<script> 
var id_client = 18,
num = "",
mb_fwT = "normal",
mb_fzT = "18px",
mb_tdT = "none",
mb_postion = "position_non",
mb_fsT = "normal",
mb_snippetWidth = "800",
mb_textT = "Ищите врача?",
mb_dragForm = "true",
scriptJs = document.createElement("script");
scriptJs.src = "//medbooking.com/scripts/integrator/moduleForm/script.js";
document.head.appendChild(scriptJs);
</script>
				</div>
           </div>
            </div>
        </div>
		</div>
	</div>
</section>
<div id="popup_mod">
    <div class="popup_head_mod">
        <h2>Код</h2>
        <i id="popup_close"></i>
        <span>Вставьте код на сайт</span>
    </div>
    <div class="popup_body-mod">
        <form>
        <div class="code_msg" id="code_msg">
        </div>
            <input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
        </form>
    </div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>";
</script>