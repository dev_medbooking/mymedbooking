<section class="modul">
  <div class="js_form_modul">
   <div class="wrapper_mod clearfix">
       <div class="form_modul">
            <div class="form_modul_preview">
                <p>«Подбор врача» - дает возможность аудитории Вашего сайта оставить заявку на онлайн подбор врача, телефон для записи так же присутствует. Идеально подходит для размещения в сайтбаре.</p>
                <input id="btn_code" type="button" value="Получить код">
                </div>
               <div class="form_setting">
                <fieldset class="form_size_top">
                    <h3>Название заголовка:</h3>
                    <input id="name_btn" type="text" placeholder="Записаться на прием">
                </fieldset>                 
                   <fieldset class="form_size_top">
					<h3>Выберите размер формы:</h3>
					<select id="asideWidth" name="menu" size="1">
                    <option selected="selected" value="350">350</option>
                    <option value="300">300</option>
					<option value="250">250</option>
					</select><label for="menu">PX</label>
                </fieldset> 
                <fieldset class="form_tool_9"> 
                    <input type="checkbox" id="allStylebtn" />
                    <label for="allStylebtn">Без редакции кнопки<br>&nbsp;</label>
                </fieldset>
            </div>
        </div>
    </div>
</div>
    <div class="wrapper_mod clearfix">
        <div class="modul_preview">
            <div class='browser-wrapper'>
                <div class='top-bar'>
                    <div class='row small'>
                    <div class='circle close'></div>
                    <div class='circle minify'></div>
                    <div class='circle zoom'></div>
                    <div class='tab'>
   
                    </div>
                </div>
                <div class='row flex active large'>
                    <div class='button back'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button forward'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button reload-button'>
                        <div class='arrow-down'></div>
                    </div>
                    <div class='address-bar'>
                        <span class='favicon'></span>
                    </div>
                    <div class='sandwich-button'>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                    </div>
                </div>
            </div>
            <div class="site_block">
            <div class="site_block_logo">LOGO</div>
            <div class="site_block_log"></div>
            <div class="site_block_search"></div>
            </div>
            <div class='content_browser'>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
				<div class="medbooking-minimized medbooking-minimized__static" id="popup-mb_minimized"></div>
				<script> 
				var id_client = 18,
				num = "7 (495) 230 79 77",
				modW = "350",
				mb_line_txt = "Подобрать врача",
				scriptJs = document.createElement("script");
				scriptJs.src = "//medbooking.com/scripts/integrator/staticPopup.js";
				document.head.appendChild(scriptJs);
				</script>
            </div>
        </div>
        </div>
        </div> 
</section>
<div id="popup_mod">
    <div class="popup_head_mod">
        <h2>Код</h2>
        <i id="popup_close"></i>
        <span>Вставьте код на сайт</span>
    </div>
    <div class="popup_body-mod">
        <form>
        <div class="code_msg" id="code_msg">
        </div>
            <input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
        </form>
    </div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>";
</script>