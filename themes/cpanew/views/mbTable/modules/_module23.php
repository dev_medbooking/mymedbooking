<link rel="stylesheet" href="//<?= Helper::getYiiParam('moduleDomain') ?>/styles/integrator/all_developer.css">
<section class="modul">
    <div class="js_form_modul">
        <div class="wrapper_mod clearfix">
            <div class="form_modul module16">
                <div class="form_modul_preview" style="width: 24%;">
                    <p>«Список врачей/клиник/услуг» – виджет, который позволяет выводить на вашем сайте каталоги врачей,
                        как по всем специализациям, так и узким – например, если Ваш сайт направлен на лечение
                        гайморита, у Вас есть возможность разметить только отоларингологов. Так же, возможно вывести
                        только те клиники, где принимают отоларингологи. Для пользователя предоставляется телефон +
                        форма записи на прием онлайн.</p>
                    <input type="button" id="btn_code" value="Получить код">
                </div>
                <div class="form_setting l-col" style="width: 70%;">
                    <div class="l-col-40p" style="width: 310px;">
                        <div class=" form_size">
                            <?php
                            $cats = Yii::app()->db2->createCommand(
                                'SELECT c.id, c.name_clinic  FROM category AS c LEFT JOIN node_service AS ns ON (c.id=ns.category_id)  WHERE ns.status=1 AND ns.translit IS NOT NULL GROUP BY c.name ORDER BY c.name'
                            )->queryAll();
                            $result = array();
                            $selected = array();
                            foreach ($cats as $cat) {
                                //                            $result[$cat['translit']] = mb_strtolower($cat['name_service']);
                                $result[$cat['id']] = $cat['name_clinic'];
                                $selected[] = $cat['id'];
                            }
                            ?>
                            <!-- Вместо style сделать css класс, чтоб интервал был между элементами -->
                            <h3>Категории услуг:</h3>
                            <?php
                            echo CHtml::dropDownList(
                                'MedbookingNodeService[category_id[]]', $selected, $result,
                                array('class' => 'select3', 'multiple' => 'multiple')
                            );
                            ?>

                            <h3 class="js-title-select-wrapper">Выберите услугу:</h3>

                            <?= CHtml::dropDownList(
                                'MedbookingNodeService[service_id]', [], [],
                                ['class' => 'select3', 'multiple' => 'multiple']
                            ); ?>
                            <br>
                            <br>
                            <input type="submit" class="btn" id="use_categories" value="Применить категории"
                                   style="background-color: #5abe9d; border-color: #5abe9d;">


                        </div>
                    </div>
                    <div class="form_size" style="margin-right: 80px;">
                        <br><br>
                        <?php
                        echo CHtml::checkBox(
                            'MedbookingNodeService[hide_clinics]', false,
                            array('id' => 'hide_clinics')
                        );
                        ?>
                        <label for="hide_clinics">Свернуть клиники</label>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="wrapper_mod clearfix">
        <div class="modul_preview">
            <div class='browser-wrapper'>
                <div class='top-bar'>
                    <div class='row small'>
                        <div class='circle close'></div>
                        <div class='circle minify'></div>
                        <div class='circle zoom'></div>
                        <div class='tab'>

                        </div>
                    </div>
                    <div class='row flex active large'>
                        <div class='button back'>
                            <div class='top-block'></div>
                            <div class='bottom-block'></div>
                            <div class='straight-block'></div>
                        </div>
                        <div class='button forward'>
                            <div class='top-block'></div>
                            <div class='bottom-block'></div>
                            <div class='straight-block'></div>
                        </div>
                        <div class='button reload-button'>
                            <div class='arrow-down'></div>
                        </div>
                        <div class='address-bar'>
                            <span class='favicon'></span>

                        </div>
                        <div class='sandwich-button'>
                            <div class='sandwich-line'></div>
                            <div class='sandwich-line'></div>
                            <div class='sandwich-line'></div>
                        </div>
                    </div>
                </div>
                <div class="site_block">
                    <div class="site_block_logo">LOGO</div>
                    <div class="site_block_log"></div>
                    <div class="site_block_search"></div>
                </div>
                <div class='content_browser'>
                    <div class="site_block_1"></div>
                    <div class="site_block_1"></div>
                    <div class="site_block_1"></div>
                    <section class="codeInit">
                        <div id="medbooking-service"></div>
                        <script>
                            var id_client = <?= $partner->id ?>,
                                moduleDomain = "<?= Helper::getYiiParam('moduleDomain') ?>",
                                num = "<?= $partner->phone ?>",
                                settings = {
                                    ident_id: id_client,
                                    callbackPhone: num,
                                    services: [],
                                    categories: [],
                                    hide_clinics: 0
                                },
                                scriptJs = document.createElement("script");

                            scriptJs.id = "initScript";
                            scriptJs.charset = "UTF-8";
                            scriptJs.src = "//" + moduleDomain + "/scripts/integrator/module23/script.js";
                            document.head.appendChild(scriptJs);
                            scriptJs.onload = function () {
                                medbookingModule23.init(
                                    moduleDomain,
                                    settings
                                )
                            };
                            var MedbookingModuleServices2_url = "//" + moduleDomain + "/servicesPartner?ajax=partner_list2&c[]=allergolog&c[]=analizy&c[]=androlog&c[]=anesteziolog&c[]=venerolog&c[]=vertebolog&c[]=vertebronevrolog&c[]=lfk&c[]=gastroenterolog&c[]=gematolog&c[]=genetik&c[]=gepatolog&c[]=ginekolog&c[]=gomeopat&c[]=gospitalizacija&c[]=dermatokosmetolog&c[]=dermatolog&c[]=dijetolog&c[]=immunolog&c[]=infekcionnist&c[]=kardiolog&c[]=koloproktolog&c[]=kosmetolog&c[]=logoped&c[]=mammolog&c[]=manualynii-terapevt&c[]=massazhist&c[]=mikolog&c[]=narkolog&c[]=nevrolog&c[]=nejrohirurg&c[]=nefrolog&c[]=onkolog&c[]=ortoped&c[]=osteopat&c[]=otolaringolog&c[]=otonevrolog&c[]=oftalymolog&c[]=parodontolog&c[]=pediatr&c[]=plasticheskii-khirurg&c[]=proktolog&c[]=psychiatr&c[]=psiholog&c[]=psihoterapevt&c[]=pulymonolog&c[]=revmatolog&c[]=refleksoterapevt&c[]=seksolog&c[]=serdechno-sosudistii-khirurg&c[]=stomatolog&c[]=surdologija&c[]=terapevt&c[]=travmatolog-ortoped&c[]=triholog&c[]=urolog&c[]=fizioterapevt&c[]=flebolog&c[]=funkcionalynii-diagnost&c[]=hirurg&c[]=endokrinolog&";
                            var MedbookingModuleServices2_realLimit = true;
                        </script>
                    </section>
                    <!--                    hot fixing display jumping when 1 service is selected, do something with it-->
                    <div style="height: 600px">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="popup_mod">
    <div class="popup_head_mod">
        <h2>Код</h2>
        <i id="popup_close"></i>
        <span>Вставьте код на сайт</span>
    </div>
    <div class="popup_body-mod">
        <form>
            <div class="code_msg" id="code_msg">
            </div>
            <input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
        </form>
    </div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>";
</script>
<link rel="stylesheet" href="//medbooking.com/styles/multiple-select.css">
<script src="//medbooking.com/scripts/jquery.multiple.select.js"></script>

