
<section class="modul">
	<div class="js_form_modul">
		<div class="wrapper_mod clearfix">
			<div class="form_modul module16">
				<div class="form_modul_preview">
					<p>«Список врачей/клиник/услуг» – виджет, который позволяет выводить на вашем сайте каталоги врачей,
						как по всем специализациям, так и узким – например, если Ваш сайт направлен на лечение
						гайморита, у Вас есть возможность разметить только отоларингологов. Так же, возможно вывести
						только те клиники, где принимают отоларингологи. Для пользователя предоставляется телефон +
						форма записи на прием онлайн.</p>
					<input type="button" id="btn_code" value="Получить код">
				</div>
				<div class="form_setting">
					<fieldset class="form_size">
						<h3>Категории услуг:</h3>
						<!-- <input type="text" checked="search"> -->

						<!-- <select multiple="multiple" class="select3">
							<option value="1">January</option>
							<option value="12">December</option><option value="1">January</option>
						</select> -->


						<?php
							$cats = Yii::app()->db2->createCommand('SELECT c.translit, c.name_service  FROM category AS c LEFT JOIN node_service AS ns ON (c.id=ns.category_id)  WHERE ns.status=1 AND ns.translit IS NOT NULL GROUP BY c.name ORDER BY c.name')->queryAll();
							$result = array();
							$selected = array();

						foreach($cats as $cat) {
//                            $result[$cat['translit']] = mb_strtolower($cat['name_service']);
							$result[$cat['translit']] = $cat['name_service'];
							$selected[] = $cat['translit'];
						}

						echo CHtml::dropDownList('MedbookingNodeService[category_id[]]', $selected, $result, array('class' => 'select3', 'multiple'=>'multiple'));
						?>

						<h3>Колличество записей :  <span id="recordsCountShow"></span>/ <span id="recordsCount"></span></h3>
						<?php

						$form = new CActiveForm();
						$form->widget('zii.widgets.jui.CJuiSliderInput', array(
//                        'model'=>$model,
//                        'attribute'=>'budget',
							'name' => 'limit',
//							'maxAttribute' => 'budgetMax',
//                            'event' => 'change',
							'options' => array(
								'range' => false,
								'min' => 10,
								'max' => 100,
								'step' => 5,
								'values' => array(10),

								'change' => 'js:function(event,ui){limitChange(event,ui)}',
								'slide' => 'js:function(event,ui){limitSlide(event,ui)}',
							),
							'htmlOptions' => array(
								'id' => 'limitSlider'
							)
							));

						?>
					</fieldset>

					<fieldset class="form_size">
						<h3>Выберите размер:</h3>
						<select id="toggle_width" name="menu" size="1">
							<option selected="selected" value="840">840</option>
							<option value="800">800</option>
							<option value="750">750</option>
							<option value="700">700</option>
						</select><label for="menu">PX</label>
						<div>
							<input type="checkbox" id="toggle_sidebar" checked/>
							<label for="toggle_sidebar">Разделять на категории </label>
						</div>
						<div>
							<input type="checkbox" id="toggle_drag" checked/>
							<label for="toggle_drag">Показывать форму записи онлайн</label>
							<span class="form_size__tip">
								<span class="form_size__tip__head">?</span>
								<span class="form_size__tip__body">TextTextTextTextTextTextTextText</span>
							</span>
						</div>
					</fieldset>
				</div>
			</div>
			<fieldset class="form-search">
				<span class="form-search__title">Выберете услугу</span>
				<input id="tags" class="search__text">
				<ul id="services_box"></ul>
				<!-- <ul id="services_box"><li data-id="1225"><a data-translit="1225"><span class="tab">Анализ краснyха Ig М</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li></ul> -->
			</fieldset>
		</div>
	</div>
	<div class="wrapper_mod clearfix">
		<div class="modul_preview">
			<div class='browser-wrapper'>
				<div class='top-bar'>
					<div class='row small'>
						<div class='circle close'></div>
						<div class='circle minify'></div>
						<div class='circle zoom'></div>
						<div class='tab'>

						</div>
					</div>
					<div class='row flex active large'>
						<div class='button back'>
							<div class='top-block'></div>
							<div class='bottom-block'></div>
							<div class='straight-block'></div>
						</div>
						<div class='button forward'>
							<div class='top-block'></div>
							<div class='bottom-block'></div>
							<div class='straight-block'></div>
						</div>
						<div class='button reload-button'>
							<div class='arrow-down'></div>
						</div>
						<div class='address-bar'>
							<span class='favicon'></span>

						</div>
						<div class='sandwich-button'>
							<div class='sandwich-line'></div>
							<div class='sandwich-line'></div>
							<div class='sandwich-line'></div>
						</div>
					</div>
				</div>
				<div class="site_block">
					<div class="site_block_logo">LOGO</div>
					<div class="site_block_log"></div>
					<div class="site_block_search"></div>
				</div>
				<div class='content_browser'>
					<div class="site_block_1"></div>
					<div class="site_block_1"></div>
					<div class="site_block_1"></div>
					<section class="codeInit">
						<div id="medbooking-service"></div>
						<script>
							var id_client = <?=$partner->id;?>,
								num = "",
								dragForm = true,
								contentWidth = 870,
								aside = true,
//                                mb_category = "servicesPartner",
//                                mb_special = "lfk",
								url = "//<?= Helper::getYiiParam('moduleDomain', 'medbooking.com')?>/servicesPartner?ajax=partner_list",
                                moduleDomain = "<?= Helper::getYiiParam('moduleDomain', 'medbooking.com')?>",
								scriptJs = document.createElement("script");
							scriptJs.id = "initScript";
							scriptJs.src = "//<?= Helper::getYiiParam('moduleDomain')?>/scripts/integrator/moduleServices/script.js";
							document.head.appendChild(scriptJs);
						</script>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
<div id="popup_mod">
	<div class="popup_head_mod">
		<h2>Код</h2>
		<i id="popup_close"></i>
		<span>Вставьте код на сайт</span>
	</div>
	<div class="popup_body-mod">
		<form>
			<div class="code_msg" id="code_msg">
			</div>
			<input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
		</form>
	</div>
</div>
<script>
	mbId = <?=$partner->id;?>;
	mbPhone = "<?=$partner->phone;?>";
</script>
<link rel="stylesheet" href="//medbooking.com/styles/multiple-select.css">
<script src="//medbooking.com/scripts/jquery.multiple.select.js"></script>
