<section class="modul">
  <div class="js_form_modul">
   <div class="wrapper_mod clearfix">
            <div class="form_modul">
               <div class="form_modul_preview">
                <p>«Список врачей/клиник/услуг» – виджет, который позволяет выводить на вашем сайте каталоги врачей, как по всем специализациям, так и узким – например, если Ваш сайт направлен на лечение гайморита, у Вас есть возможность разметить только отоларингологов. Так же, возможно вывести только те клиники, где принимают отоларингологи.  Для пользователя предоставляется телефон + форма записи на прием онлайн.</p>
                </div>
                   <div class="form_setting">
                    <fieldset class="form_size">
                        <h3>Выберите размер:</h3>
                        <select id="toggle_width" name="menu" size="1">
                            <option selected="selected" value="870">870</option>
                            <option value="800">800</option>
                            <option value="750">750</option>
                            <option value="700">700</option>
                            <option value="0">Авто</option>
                        </select><label for="menu">PX</label>
                        <input type="button"  id="btn_code" value="Получить код">
                    </fieldset>
                    <fieldset class="form_size">
                        <h3>Выбрать категорию:</h3>
                        <input checked name="category_toogle" type="checkbox" id="search_doctor">
                        <label  for="search_doctor">Врачи</label>
                        <input  name="category_toogle" type="checkbox" id="search_clinic">
                        <label for="search_clinic">Клиники</label>
                        <?php
                        $cats = Yii::app()->db2->createCommand('SELECT * FROM category WHERE level=2  AND link=0 AND link2=0 ORDER BY name ASC')->queryAll();
                        $result = array();
                        $selected = array();

                        foreach ($cats as $cat) {
                            $result[$cat['translit']] = $cat['name'];
                            $selected[] = $cat['translit'];
                        }
                        ?>
                    <h3>Выбрать специальность</h3>
                        <?php
                        echo CHtml::dropDownList('selectSpecial', $selected, $result, array('class' => 'selectSpecial', 'multiple' => 'multiple'));
                        ?>

                </fieldset>
                    <fieldset class="form_tool_1">
                        <input type="checkbox" id="toggle_sidebar" checked/>
                        <label for="toggle_sidebar">Показывать side bar<br>&nbsp;</label>
                        <input type="checkbox" id="toggle_drag" checked/>
                        <label for="toggle_drag">Показывать форму<br>&nbsp;&nbsp;&nbsp;записи онлайн</label>
                        <span class="text_click">Всплывающая форма позволяет записаться онлайн всего одним щелчком мыши.</span>
                    </fieldset>
                    </div>
                 </div>
            </div>
        </div>
        <div class="wrapper_mod clearfix">
		<div class="modul_preview">
		<div class='browser-wrapper'>
            <div class='top-bar'>
                <div class='row small'>
                    <div class='circle close'></div>
                    <div class='circle minify'></div>
                    <div class='circle zoom'></div>
                    <div class='tab'>

                    </div>
                </div>
                <div class='row flex active large'>
                    <div class='button back'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button forward'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button reload-button'>
                        <div class='arrow-down'></div>
                    </div>
                    <div class='address-bar'>
                        <span class='favicon'></span>

                    </div>
                    <div class='sandwich-button'>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                    </div>
                </div>
            </div>
            <div class="site_block">
                <div class="site_block_logo">LOGO</div>
                <div class="site_block_log"></div>
                <div class="site_block_search"></div>
            </div>
            <div class='content_browser'>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
				<section class="codeInit">
<div id="medbooking-doctor"></div>
<script>
var id_client = 18,
num = "",
dragForm = true,
contentWidth = 870,
aside = true,
mb_category = "search_doctor",
mbDocCategoryL_tabs = true,
mbCliCategoryL_tabs = false,
mb_special = "lfk",
url = "//" + "<?= Helper::getYiiParam('moduleDomain')?>" + "/search_doctor/?ajax=ref",
scriptJs = document.createElement("script");
scriptJs.id = "initScript";
scriptJs.src = "//" + "<?= Helper::getYiiParam('moduleDomain')?>" + "/scripts/integrator/moduleDocCategoryL/script.js";
document.head.appendChild(scriptJs);
</script>
			     </section>
            </div>
        </div>
		</div>
	</div>
</section>
<div id="popup_mod">
	<div class="popup_head_mod">
		<h2>Код</h2>
		<i id="popup_close"></i>
		<span>Вставьте код на сайт</span>
	</div>
	<div class="popup_body-mod">
		<form>
		<div class="code_msg" id="code_msg">
		</div>
			<input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
		</form>
	</div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>";
</script>
<link rel="stylesheet" href="//medbooking.com/styles/multiple-select.css">
<script src="//medbooking.com/scripts/jquery.multiple.select.js"></script>