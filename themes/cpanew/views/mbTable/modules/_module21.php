<section class="modul">
    <div class="js_form_modul">
        <div class="wrapper_mod clearfix">
            <div class="form_modul module16">
                <div class="form_modul_preview" style="width: 24%;">
                    <p>«Список врачей» – виджет, который позволяет выводить на вашем сайте каталоги врачей,
                        как по всем специализациям, так и узким – например, если Ваш сайт направлен на лечение
                        гайморита, у Вас есть возможность разметить только отоларингологов. Для пользователя предоставляется телефон +
                        форма записи на прием онлайн.</p>
                    <input type="button" id="btn_code" value="Получить код">
                </div>
                <div class="form_setting l-col" style="width: 70%;">
                    <div class="form_size" style="margin-right: 80px;">

                        <?php
                        $cats = Yii::app()->db2->createCommand('SELECT * FROM category WHERE level=2  AND link=0 AND link2=0 ORDER BY name ASC')->queryAll();
                        $result = array();
                        $selected = array();

                        foreach ($cats as $cat) {
                            $result[$cat['id']] = $cat['name'];
                            $selected[] = $cat['id'];
                        }
                        ?>
                        <h3>Выбрать специальность</h3>
                        <?php
                        echo CHtml::dropDownList('selectSpecial', $selected, $result, array('class' => 'selectSpecial', 'multiple' => 'multiple'));
                        ?>

                        <div class="slider">
                            <h3>Количество врачей:</h3>
                            <input type="text" id="amount" readonly="">
                            <div id="slider-range-max"></div>
                        </div>
                    </div>
                    <div class="form_size">
                        <h3 style="margin-top:36px;">Цвет кнопки:</h3>
                        <div class="color_picker picker">
                            <input type="text" value="FFFFFF" id="picker_button"><label for="picker_button"></label>
                        </div>
                    </div>
                    <div class="form_size" style="margin-right: 80px;">
                        <h3 style="margin-top:36px;">Цвет флага:</h3>
                        <div class="color_picker picker2">
                            <input type="text" value="FFFFFF" id="picker_button2"><label for="picker_button2"></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper_mod clearfix">
        <div class="modul_preview">
            <div class='browser-wrapper'>
                <div class='top-bar'>
                    <div class='row small'>
                        <div class='circle close'></div>
                        <div class='circle minify'></div>
                        <div class='circle zoom'></div>
                        <div class='tab'>

                        </div>
                    </div>
                    <div class='row flex active large'>
                        <div class='button back'>
                            <div class='top-block'></div>
                            <div class='bottom-block'></div>
                            <div class='straight-block'></div>
                        </div>
                        <div class='button forward'>
                            <div class='top-block'></div>
                            <div class='bottom-block'></div>
                            <div class='straight-block'></div>
                        </div>
                        <div class='button reload-button'>
                            <div class='arrow-down'></div>
                        </div>
                        <div class='address-bar'>
                            <span class='favicon'></span>

                        </div>
                        <div class='sandwich-button'>
                            <div class='sandwich-line'></div>
                            <div class='sandwich-line'></div>
                            <div class='sandwich-line'></div>
                        </div>
                    </div>
                </div>
                <div class="site_block">
                    <div class="site_block_logo">LOGO</div>
                    <div class="site_block_log"></div>
                    <div class="site_block_search"></div>
                </div>
                <div class='content_browser'>
                    <div class="site_block_1"></div>
                    <div class="site_block_1"></div>
                    <div class="site_block_1"></div>
                    <section class="codeInit">
                        <div id="medbookingDoctorsModule"></div>
                        <script>
                            var medbookingDoctorsModule_id_client = 18,
                            medbookingDoctorsModule_phone = "<?=$partner->phone;?>",
                            medbookingDoctorsModule_limit = 12,
                            medbookingDoctorsModule_number = "21",
                            medbookingDoctorsModule_buttonColor = "#00baff",
                            medbookingDoctorsModule_rateColor = "#27cdb7",
                            medbookingDoctorsModule_specialities = "",
                            medbookingDoctorsModule_moduleDomain = "medbooking.com",
                            medbookingDoctorsModule_apiDomain = "a.medbooking.com",
                            medbookingDoctorsModule_scriptJs = document.createElement("script");
                            medbookingDoctorsModule_scriptJs.id = "medbookingDoctorsModule_initScript";
                            medbookingDoctorsModule_scriptJs.charset = "UTF-8";
                            medbookingDoctorsModule_scriptJs.src = "//" + medbookingDoctorsModule_moduleDomain + "/scripts/integrator/medbookingDoctorsModule/script.js";
                            document.head.appendChild(medbookingDoctorsModule_scriptJs);
                        </script>
                    </section>
                    <!--                    hot fixing display jumping when 1 service is selected, do something with it-->
                    <div style="height: 600px">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="popup_mod">
    <div class="popup_head_mod">
        <h2>Код</h2>
        <i id="popup_close"></i>
        <span>Вставьте код на сайт</span>
    </div>
    <div class="popup_body-mod">
        <form>
            <div class="code_msg" id="code_msg">
            </div>
            <input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
        </form>
    </div>
</div>
<script>
    var mbId = <?=$partner->id;?>;
    var mbPhone = "<?=$partner->phone;?>";
</script>
<link rel="stylesheet" href="//medbooking.com/styles/multiple-select.css">
<script src="//medbooking.com/scripts/jquery.multiple.select.js"></script>
