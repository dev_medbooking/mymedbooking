<section class="modul">
    <div class="js_form_modul">
        <div class="wrapper_mod clearfix">
            <div class="form_modul module16">
                <div class="form_modul_preview" style="width: 24%;">
                    <p>«Список врачей/клиник/услуг» – виджет, который позволяет выводить на вашем сайте каталоги врачей,
                        как по всем специализациям, так и узким – например, если Ваш сайт направлен на лечение
                        гайморита, у Вас есть возможность разметить только отоларингологов. Так же, возможно вывести
                        только те клиники, где принимают отоларингологи. Для пользователя предоставляется телефон +
                        форма записи на прием онлайн.</p>
                    <input type="button" id="btn_code" value="Получить код">
                </div>
                <div class="form_setting l-col" style="width: 70%;">
                    <div class="l-col-40p" style="width: 310px;">
                        <div class=" form_size">

<?php
                            $cats = Yii::app()->db2->createCommand('SELECT c.translit, c.name_clinic  FROM category AS c LEFT JOIN node_service AS ns ON (c.id=ns.category_id)  WHERE ns.status=1 AND ns.translit IS NOT NULL GROUP BY c.name ORDER BY c.name')->queryAll();
                            $result = array();
                            $selected = array();

                            foreach ($cats as $cat) {
                                //                            $result[$cat['translit']] = mb_strtolower($cat['name_service']);
                                $result[$cat['translit']] = $cat['name_clinic'];
                                $selected[] = $cat['translit'];
                            }
                            ?>
                            <!-- Вместо style сделать css класс, чтоб интервал был между элементами -->
                            <h3>Категории услуг:</h3>
                            <?php
                            echo CHtml::dropDownList('MedbookingNodeService[category_id[]]', $selected, $result, array('class' => 'select3', 'multiple' => 'multiple'));
                            ?>
                            <br>
                            <br>
                            <input type="submit" class="btn" id="use_categories" value="Применить категории" style="background-color: #5abe9d; border-color: #5abe9d;">
                            <!-- <input type="text" checked="search"> -->

                            <!-- <select multiple="multiple" class="select3">
                                <option value="1">January</option>
                                <option value="12">December</option><option value="1">January</option>
                            </select> -->
                            

                            
                    </fieldset>

                    <fieldset class="form_size" style="margin-left: 0;">
                        <h3>Выберите размер:</h3>
                        <select class="left" id="toggle_width" name="menu" size="1">
                            <option selected="selected" value="auto">На всю страницу</option>
                            <option value="840">840</option>
                            <option value="800">800</option>
                            <option value="750">750</option>
                            <option value="700">700</option>
                            <option value="650">650</option>
                            <option value="540">540</option>
                        </select>
                        <label class="left" for="menu">PX</label>
                    </div>
                    </div>

                    <!-- <fieldset class="form_size"> -->

                    <!--                        <div>-->
                    <!--                            <input type="checkbox" id="toggle_sidebar" checked/>-->
                    <!--                            <label for="toggle_sidebar">Разделять на категории </label>-->
                    <!--                        </div>-->

                    <!-- </fieldset> -->
                    <div class="form_size" style="margin-right: 80px;">
                        <h3>Колличество услуг : <span id="recordsCountShow"></span>/ <span id="recordsCount"></span></h3>
                            <?php

                            $form = new CActiveForm();
                            $form->widget('zii.widgets.jui.CJuiSliderInput', array(
                                //                        'model'=>$model,
                                //                        'attribute'=>'budget',
                                'name' => 'limit',
                                //							'maxAttribute' => 'budgetMax',
                                //                            'event' => 'change',
                                'options' => array(
                                    'range' => false,
                                    'min' => 1,
                                    'max' => 100,
                                    'step' => 1,
                                    'values' => array(10),

                                    'change' => 'js:function(event,ui){limitChange(event,ui)}',
                                    'slide' => 'js:function(event,ui){limitSlide(event,ui)}',
                                ),
                                'htmlOptions' => array(
                                    'id' => 'limitSlider',
                                    'style' => 'width: auto;'
                                )
                            ));

                            ?>
                        
                        <h3>Колличество клиник : <span id="clinicRecordsCountShow"></span>/ <span id="clinicRecordsCount"></span>
                        </h3>
                        <?php

                        $form = new CActiveForm();
                        $form->widget('zii.widgets.jui.CJuiSliderInput', array(
//                        'model'=>$model,
//                        'attribute'=>'budget',
                            'name' => 'clinicLimit',
//                          'maxAttribute' => 'budgetMax',
//                            'event' => 'change',
                            'options' => array(
                                'range' => false,
                                'min' => 1,
                                'max' => 100,
                                'step' => 1,
                                'values' => array(10),

                                'change' => 'js:function(event,ui){clinicLimitChange(event,ui)}',
//                                'slide' => 'js:function(event,ui){clinicLimitSlide(event,ui)}',
                            ),
                            'htmlOptions' => array(
                                'id' => 'clinicLimitSlider'
                            )
                        ));

                        ?>
                        <h3>Колличество врачей : <span id="doctorRecordsCountShow"></span>/ <span id="doctorRecordsCount"></span>
                        </h3>
                        <?php

                        $form = new CActiveForm();
                        $form->widget('zii.widgets.jui.CJuiSliderInput', array(
//                        'model'=>$model,
//                        'attribute'=>'budget',
                            'name' => 'doctorLimit',
//                          'maxAttribute' => 'budgetMax',
//                            'event' => 'change',
                            'options' => array(
                                'range' => false,
                                'min' => 1,
                                'max' => 100,
                                'step' => 1,
                                'values' => array(10),

                                'change' => 'js:function(event,ui){doctorLimitChange(event,ui)}',
//                                'slide' => 'js:function(event,ui){doctorLimitSlide(event,ui)}',
                            ),
                            'htmlOptions' => array(
                                'id' => 'doctorLimitSlider'
                            )
                        ));

                        ?>
                    </div>
                    <div class="form_size">
                        <div>
                            <div>
                                <h3>Заголовок</h3>
                                <input type="text" id="toggle_title" checked/>
                            </div>
                            <div>
                                <input type="checkbox" id="toggle_services" checked/>
                                <label for="toggle_services">Вкладка услуги</label><br>

                                <input type="checkbox" id="toggle_clinics" checked/>
                                <label for="toggle_clinics">Вкладка клиники</label><br>

                                <input type="checkbox" id="toggle_doctors" checked/>
                                <label for="toggle_doctors">Вкладка врачи</label><br>
                            </div>
                            <div>
                                <input type="checkbox" id="toggle_form" checked/>
                                <label for="toggle_form">Форма</label><br>
                            </div>
                            <div>
                                <input type="checkbox" id="toggle_drag" checked/>
                                <label for="toggle_drag">Показывать форму записи онлайн</label>
								<span class="form_size__tip">
									<span class="form_size__tip__head">?</span>
									<span class="form_size__tip__body"></span>
								</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <fieldset class="form-search">
                <span class="form-search__title">Выберете услугу</span>
                <input id="tags" class="search__text">
                <ul id="services_box"></ul>
                <!-- <ul id="services_box"><li data-id="1225"><a data-translit="1225"><span class="tab">Анализ краснyха Ig М</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li><li data-id="1286"><a data-translit="1286"><span class="tab">Токсоплазмоз Ig М (антитела класса М к Toxoplasma gondii)</span></a></li></ul> -->
            </fieldset>
        </div>
    </div>
    <div class="wrapper_mod clearfix">
        <div class="modul_preview">
            <div class='browser-wrapper'>
                <div class='top-bar'>
                    <div class='row small'>
                        <div class='circle close'></div>
                        <div class='circle minify'></div>
                        <div class='circle zoom'></div>
                        <div class='tab'>

                        </div>
                    </div>
                    <div class='row flex active large'>
                        <div class='button back'>
                            <div class='top-block'></div>
                            <div class='bottom-block'></div>
                            <div class='straight-block'></div>
                        </div>
                        <div class='button forward'>
                            <div class='top-block'></div>
                            <div class='bottom-block'></div>
                            <div class='straight-block'></div>
                        </div>
                        <div class='button reload-button'>
                            <div class='arrow-down'></div>
                        </div>
                        <div class='address-bar'>
                            <span class='favicon'></span>

                        </div>
                        <div class='sandwich-button'>
                            <div class='sandwich-line'></div>
                            <div class='sandwich-line'></div>
                            <div class='sandwich-line'></div>
                        </div>
                    </div>
                </div>
                <div class="site_block">
                    <div class="site_block_logo">LOGO</div>
                    <div class="site_block_log"></div>
                    <div class="site_block_search"></div>
                </div>
                <div class='content_browser'>
                    <div class="site_block_1"></div>
                    <div class="site_block_1"></div>
                    <div class="site_block_1"></div>
                    <section class="codeInit">
                        <div id="medbooking-service"></div>
                        <script>  var id_client = 18,
                                MedbookingModuleServices2_clinicLimit = 10,
                                MedbookingModuleServices2_doctorLimit = 10,
                                MedbookingModuleServices2_limit = 10,
                                dragForm = true, MedbookingModuleServices2_contentWidth = 'auto',
                                MedbookingModuleServices2_dragForm = true,
                                MedbookingModuleServices2_toggleServices = true,
                                MedbookingModuleServices2_toggleClinics = true,
                                MedbookingModuleServices2_toggleDoctors = true,
                                MedbookingModuleServices2_toggleForm = true,
                                MedbookingModuleServices2_toggleTitle = "",
                                moduleDomain = "<?= Helper::getYiiParam('moduleDomain') ?>";
                            var MedbookingModuleServices2_url = "//" + moduleDomain + "/servicesPartner?ajax=partner_list2&c[]=allergolog&c[]=analizy&c[]=androlog&c[]=anesteziolog&c[]=venerolog&c[]=vertebolog&c[]=vertebronevrolog&c[]=lfk&c[]=gastroenterolog&c[]=gematolog&c[]=genetik&c[]=gepatolog&c[]=ginekolog&c[]=gomeopat&c[]=gospitalizacija&c[]=dermatokosmetolog&c[]=dermatolog&c[]=dijetolog&c[]=immunolog&c[]=infekcionnist&c[]=kardiolog&c[]=koloproktolog&c[]=kosmetolog&c[]=logoped&c[]=mammolog&c[]=manualynii-terapevt&c[]=massazhist&c[]=mikolog&c[]=narkolog&c[]=nevrolog&c[]=nejrohirurg&c[]=nefrolog&c[]=onkolog&c[]=ortoped&c[]=osteopat&c[]=otolaringolog&c[]=otonevrolog&c[]=oftalymolog&c[]=parodontolog&c[]=pediatr&c[]=plasticheskii-khirurg&c[]=proktolog&c[]=psychiatr&c[]=psiholog&c[]=psihoterapevt&c[]=pulymonolog&c[]=revmatolog&c[]=refleksoterapevt&c[]=seksolog&c[]=serdechno-sosudistii-khirurg&c[]=stomatolog&c[]=surdologija&c[]=terapevt&c[]=travmatolog-ortoped&c[]=triholog&c[]=urolog&c[]=fizioterapevt&c[]=flebolog&c[]=funkcionalynii-diagnost&c[]=hirurg&c[]=endokrinolog&";
                            var MedbookingModuleServices2_realLimit= true;
                            var num = "<?=$partner->phone;?>";
                            scriptJs = document.createElement("script");
                            scriptJs.id = "initScript";
                            scriptJs.charset = "UTF-8";
                            scriptJs.src = "//" + moduleDomain + "/scripts/integrator/moduleServices2/script.js";
                            document.head.appendChild(scriptJs); </script>
                    </section>
<!--                    hot fixing display jumping when 1 service is selected, do something with it-->
                    <div style="height: 600px">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="popup_mod">
    <div class="popup_head_mod">
        <h2>Код</h2>
        <i id="popup_close"></i>
        <span>Вставьте код на сайт</span>
    </div>
    <div class="popup_body-mod">
        <form>
            <div class="code_msg" id="code_msg">
            </div>
            <input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
        </form>
    </div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>";
</script>
<link rel="stylesheet" href="//medbooking.com/styles/multiple-select.css">
<script src="//medbooking.com/scripts/jquery.multiple.select.js"></script>
