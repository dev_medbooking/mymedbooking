<section class="modul">
    <div class="js_form_modul">
        <div class="wrapper_mod clearfix">
            <div class="form_modul module16">
                <div class="form_modul_preview" style="width: 24%;">
                    <p>«Список врачей» – виджет, который позволяет выводить на вашем сайте каталоги врачей,
                        как по всем специализациям, так и узким – например, если Ваш сайт направлен на лечение
                        гайморита, у Вас есть возможность разметить только отоларингологов. Так же, возможно вывести
                        только те клиники, где принимают отоларингологи. Для пользователя предоставляется телефон +
                        форма записи на прием онлайн.</p>
                    <input type="button" id="btn_code" value="Получить код">
                </div>
                <div class="form_setting l-col" style="width: 70%;">
                    <div class="l-col-40p" style="width: 310px;">
                        <div class=" form_size">

                            <?php
                            $cats = Yii::app()->db2->createCommand('
                            SELECT t.id, t.name_alt
                            from category_price as t
                            left join clinic_price as cp on cp.category_price_id = t.id
                            where cp.id is not null
                            and cp.price > 0
                            and t.name_alt !=\'\'
                            group by t.id order by t.name_alt;')->queryAll();
                            $result = array();
                            $selected = array();

                            foreach ($cats as $cat) {
                                $result[$cat['id']] = $cat['name_alt'];
                                $selected[] = $cat['id'];
                            }
                            ?>
                            <h3>Категории услуг:</h3>
                            <?php
                            echo CHtml::dropDownList('MedbookingNodeService[category_id[]]', $selected, $result, array('class' => 'select3', 'multiple' => 'multiple'));
                            ?>
                            <br>
                            <br>
                            <input type="submit" class="btn" id="apply_categories" value="Применить категории"
                                   style="background-color: #5abe9d; border-color: #5abe9d;">

                            </fieldset>


                        </div>
                    </div>
                    <div class="form_size" style="margin-right: 80px;">
<!--                        <h3>Колличество врачей : <span id="recordsCountShow"></span>/ <span id="recordsCount"></span>-->
<!--                        </h3>-->
<!---->
<!--                        --><?php
//
//                        $form = new CActiveForm();
//                        $form->widget('zii.widgets.jui.CJuiSliderInput', array(
//                            'name' => 'limit',
//                            'options' => array(
//                                'range' => false,
//                                'min' => 1,
//                                'max' => 10,
//                                'step' => 1,
//                                'values' => array(10),
//                                'change' => 'js:function(event,ui){limitChange(event,ui)}',
//                                'slide' => 'js:function(event,ui){limitSlide(event,ui)}',
//                            ),
//                            'htmlOptions' => array(
//                                'id' => 'limitSlider',
//                                'style' => 'width: auto;'
//                            )
//                        ));
//                        ?>
<!--                        <div>-->
<!--                            <h3>Заголовок</h3>-->
<!--                            <input type="text" id="toggle_title" checked/>-->
<!--                        </div>-->
                        <div>
                            <fieldset class="form_size" style="margin-left: 0;">
                                <h3 style="margin-top:10px;">Выберите размер:</h3>
                                <select class="left" id="toggle_width" name="menu" size="1">
                                    <!--                                    <option selected="selected" value="auto">На всю ширину</option>-->
                                    <option value="auto">на всю ширину</option>
                                    <option value="800">800</option>
                                    <option value="750">750</option>
                                    <option selected value="700">700</option>
                                    <option selected value="auto">На всю страницу</option>
                                    <option value="750">750</option>
                                    <option value="700">700</option>
                                    <option value="650">650</option>
                                    <option value="600">600</option>
                                </select>
                                <label class="left" for="menu">PX</label>
                                <h3 style="margin-top:36px;">Цвет кнопки:</h3>
                                <div class="color_picker">
                                    <input type="text" value="FFFFFF" id="picker_button"><label for="picker_button"></label>
                                </div>
                            </fieldset>
                        </div>

                    </div>
                    <div class="form_size">
                        <div>
                            <input type="checkbox" id="toggle_drag" checked/>
                            <label for="toggle_drag">Показывать форму записи онлайн</label>
								<span class="form_size__tip">
									<span class="form_size__tip__head">?</span>
									<span class="form_size__tip__body"></span>
								</span>
                        </div>

                    </div>
                    <script type="text/javascript">
                        var num = "<?=$partner->phone;?>"
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper_mod clearfix">
        <div class="modul_preview">
            <div class='browser-wrapper'>
                <div class='top-bar'>
                    <div class='row small'>
                        <div class='circle close'></div>
                        <div class='circle minify'></div>
                        <div class='circle zoom'></div>
                        <div class='tab'>

                        </div>
                    </div>
                    <div class='row flex active large'>
                        <div class='button back'>
                            <div class='top-block'></div>
                            <div class='bottom-block'></div>
                            <div class='straight-block'></div>
                        </div>
                        <div class='button forward'>
                            <div class='top-block'></div>
                            <div class='bottom-block'></div>
                            <div class='straight-block'></div>
                        </div>
                        <div class='button reload-button'>
                            <div class='arrow-down'></div>
                        </div>
                        <div class='address-bar'>
                            <span class='favicon'></span>

                        </div>
                        <div class='sandwich-button'>
                            <div class='sandwich-line'></div>
                            <div class='sandwich-line'></div>
                            <div class='sandwich-line'></div>
                        </div>
                    </div>
                </div>
                <div class="site_block">
                    <div class="site_block_logo">LOGO</div>
                    <div class="site_block_log"></div>
                    <div class="site_block_search"></div>
                </div>
                <div class='content_browser'>
                    <div class="site_block_1"></div>
                    <div class="site_block_1"></div>
                    <div class="site_block_1"></div>
                    <section class="codeInit">
                        <div id="medbooking-module-doc-sidebar"></div>
                        <script>
                            var medbookingDiagnosticServicesModule_id_client = 18,
                                medbookingDiagnosticServicesModule_number = "",
//                                medbookingDiagnosticServicesModule_limit = 10,
                                medbookingDiagnosticServicesModule_contentWidth = "auto",
                                medbookingDiagnosticServicesModule_buttonColor = "#E54550",
                                medbookingDiagnosticServicesModule_categories = "",
                                medbookingDiagnosticServicesModule_dragForm = true,
                                medbookingDiagnosticServicesModule_moduleDomain = "<?= Helper::getYiiParam('moduleDomain') ?>";
                            scriptJs = document.createElement("script");
                            scriptJs.id = "initScript";
                            scriptJs.charset = "UTF-8";scriptJs.src = "//" + medbookingDiagnosticServicesModule_moduleDomain + "/scripts/integrator/diagnosticServicesModule/script.js";
                            document.head.appendChild(scriptJs);
                        </script>
                    </section>
                    <!--                    hot fixing display jumping when 1 service is selected, do something with it-->
                    <div style="height: 600px">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="popup_mod">
    <div class="popup_head_mod">
        <h2>Код</h2>
        <i id="popup_close"></i>
        <span>Вставьте код на сайт</span>
    </div>
    <div class="popup_body-mod">
        <form>
            <div class="code_msg" id="code_msg">
            </div>
            <input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
        </form>
    </div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>";
</script>
<link rel="stylesheet" href="//medbooking.com/styles/multiple-select.css">
<script src="//medbooking.com/scripts/jquery.multiple.select.js"></script>
