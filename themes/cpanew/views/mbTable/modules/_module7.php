<section class="modul">
  <div class="js_form_modul">
   <div class="wrapper_mod clearfix">
            <div class="form_modul">
               <div class="form_modul_preview_7">
                <p>«Список диагностических центров» – за счет данного виджета, у Вас есть возможность вывести на сайте каталог диагностических центров. Присутствует сортировка по рейтингу, цене и услугам. Для пользователя предоставляется телефон + форма записи на прием онлайн.</p>
                </div>
                   <div class="form_setting">
                    <fieldset class="form_size">
                        <h3>Выберите размер:</h3>
                        <select id="toggle_width" name="menu" size="1">
                            <option selected="selected" value="750">750</option>
                            <option value="700">700</option>
                            <option value="650">650</option>
                            <option value="600">600</option>
                        </select><label for="menu">PX</label>
                        <input type="button"  id="btn_code" value="Получить код">
                    </fieldset>
                    <fieldset class="form_size_7">
                        <h3>Выберите тип диагностики:</h3>
                        <div class="type_diagnostics">
                            <select id="selectSpecial" class="selectSpecial">
                               <option value="uzi">Ультразвуковое исследование (УЗИ)</option>
                               <option value="komputernaya-tomografiya">Компьютерная томография (КТ)</option>
                               <option value="mrt">Магнитно-резонансная томография (МРТ)</option>
                               <option value="rentgen">Рентген (рентгенография)</option>
                               <option value="dupleksnoe-skanirovanie">Дуплексное сканирование (ДС)</option>
                               <option value="func-diagnostika">Функциональная диагностика</option>
                               <option value="enmg">Электронейромиография (ЭНМГ)</option>
                               <option value="endoskopicheskie-issledovaniya">Эндоскопия</option>
                               <option value="fluorografiya">Флюорография грудной клетки</option>
                            </select>
                        </div>
                        <h3 class="js-title-select-wrapper">Выберите услугу:</h3>
                        <div class="services_diagnostics">
                           <div class="select_wrapper">
                                <select class="selectServices js-uzi">
                                   <option value="uzi">Все услуги</option>
                                   <option value="kompleksnoje">Комплексное УЗИ</option>
                                   <option value="uzi-limfaticheskih-uzlov">лимфатических узлов</option>
                                   <option value="uzi-glaz">глаза</option>
                                   <option value="uzi-shitovidnoi-zhelezy">щитовидной железы</option>
                                   <option value="uzi-golovnogo-mozga">головы</option>
                                   <option value="uzi-organov-moshonki">органов мошонки</option>
                                   <option value="uzi-myagkih-tkanej">мягких тканей</option>
                                   <option value="uzi-molochnyh-zhelez">молочных желез</option>
                                   <option value="semennyh-puzyrykov">семенных пузырьков</option>
                                   <option value="uzi-podjeludochnoi-jelezi">поджелудочной железы</option>
                                   <option value="uzi-selezenki">селезенки</option>
                                   <option value="uzi-matki">матки</option>
                                   <option value="uzi-zhelchnogo-puzyrya">желчного пузыря</option>
                                   <option value="uzi-mochevogo-puzyrya">мочевого пузыря</option>
                                   <option value="uzi-pecheni">печени</option>
                                   <option value="uzi-pochek">почек</option>
                                   <option value="uzi_nadpochechnikov">надпочечников</option>
                                   <option value="uzi_predstatelnoi_zhelezy">предстательной железы (простаты)</option>
                                   <option value="uzi-malogo-taza">малого таза</option>
                                   <option value="uzi-brushnoi-polosti">брюшной полости</option>
                                   <option value="uzi-sosudov">сосудов</option>
                                   <option value="uzi-serdca-ehokardiografija">сердца (эхокардиография)</option>
                                   <option value="4d-uzi">4D</option>
                                   <option value="3d-uzi">3D</option>
                                   <option value="uzi-dlya-beremennih">при беременности</option>
                                   <option value="uzi_sustavov">суставов</option>
                                </select>
                            </div>
                            <div class="select_wrapper">
                                <select  class="selectServices js-komputernaya-tomografiya">
                                   <option value="komputernaya-tomografiya">Все услуги</option>
                                   <option value="kt-pecheny">печени</option>
                                   <option value="kt-nadpochechnikov">надпочечников</option>
                                   <option value="kt-pochek">почек</option>
                                   <option value="kt-brushnoi-polosti">брюшной полости</option>
                                   <option value="kt-glaz">глаз</option>
                                   <option value="kt-gortani">гортани</option>
                                   <option value="kt-pazuh-nosa">пазух носа</option>
                                   <option value="kt-mozga">головного мозга</option>
                                   <option value="kt-malogo-taza">малого таза</option>
                                   <option value="kt-serdca">сердца</option>
                                   <option value="kt-grudnoj-kletki">грудной клетки</option>
                                   <option value="kt-legkih">легких</option>
                                   <option value="kt-pozvonochnika">позвоночника</option>
                                   <option value="kt-stopy-ily-kisti">стопы</option>
                                   <option value="kt-sustava">сустава</option>
                                </select>
                            </div>
                            <div class="select_wrapper">
                                <select  class="selectServices js-mrt">
                                   <option value="mrt">Все услуги</option>
                                   <option value="mrt-dlya-zhenshhin">Комплексное</option>
                                   <option value="mrt-vsego-organizma">всего организма</option>
                                   <option value="mrt-visochno-nizhechelustnogo-sustava">височно-нижнечелюстного сустава</option>
                                   <option value="mrt-sosudov-shei">сосудов шеи</option>
                                   <option value="mrt-sosudov-golovnogo-mozga">сосудов головного мозга</option>
                                   <option value="mrt-orbit">глазных орбит</option>
                                   <option value="mrt-pridatochnyh-pazuh-nosa">придаточных пазух носа</option>
                                   <option value="mrt-tureckogo-sedla">турецкого седла</option>
                                   <option value="mrt-gipofiza">гипофиза</option>
                                   <option value="mrt-golovnogo-mozga">головного мозга</option>
                                   <option value="mrt-molochnyh-zhelez">молочных желез</option>
                                   <option value="mrt-matki">матки и яичников</option>
                                   <option value="mrt-naruzhnyh-polovyh-organov">наружных половых органов у мужчин</option>
                                   <option value="mrt-myagkih-tkanej">мягких тканей</option>
                                   <option value="mrt-brushnoy-polosti">брюшной полости</option>
                                   <option value="mrt-nadpochechnikov">надпочечников</option>
                                   <option value="mrt-podzheludochnoj-zhelezy">поджелудочной железы</option>
                                   <option value="mrt-pecheni">печени</option>
                                   <option value="mrt-selezenki">селезенки</option>
                                   <option value="mrt-pochek">почек</option>
                                   <option value="mrt-grudnoj-kletki">грудной клетки</option>
                                   <option value="mrt-serdca">сердца</option>
                                   <option value="mrt-organov-malogo-taza">малого таза</option>
                                   <option value="mrt-kopchika">копчика</option>
                                   <option value="mrt-poyasnichno-krestsovogo-otdela">пояснично-крестцового отдела позвоночника</option>
                                   <option value="mrt-grudnogo-otdela-pozvonochnika">грудного отдела позвоночника</option>
                                   <option value="mrt-pozvonochnika">позвоночника</option>
                                   <option value="mrt-shejnogo-otdela">шейного отдела</option>
                                   <option value="mrt-luchezapyastongo-sustava">лучезапястного сустава</option>
                                   <option value="mrt-kisti-ruki">кисти руки</option>
                                   <option value="mrt-golenostopnogo-susutava">голеностопного сустава</option>
                                   <option value="mrt-kolennogo-sustava">коленного сустава</option>
                                   <option value="mrt-tazobedrennogo-sustava">тазобедренного сустава</option>
                                   <option value="mrt-loktevogo-sustava">локтевого сустава</option>
                                   <option value="mrt-plechevogo-sustava">плечевого сустава</option>
                                   <option value="mrt-stopy">стопы</option>
                                </select>
                            </div>
                            <div class="select_wrapper">
                                <select  class="selectServices js-rentgen">
                                   <option value="rentgen">Все услуги</option>
                                   <option value="ortopantogramma">ортопантомограмма</option>
                                   <option value="rentgen-visochnoy-kosti">височной кости</option>
                                   <option value="rentgen-turetskogo-sedla">турецкого седла</option>
                                   <option value="rentgen-kostey-nosa">костей носа</option>
                                   <option value="rentgen-pridatochnyh-pazuh-nosa">придаточных пазух носа</option>
                                   <option value="rengen_sheinogo_otdela_pozvonochnika">шеи</option>
                                   <option value="rentgen-zuba">зубов</option>
                                   <option value="rentgen-cherepa">черепа</option>
                                   <option value="rentgen-paltsev">пальцев ног и рук</option>
                                   <option value="rentgen-kostey-taza">костей таза</option>
                                   <option value="rentgen-pozvonochnika">позвоночника</option>
                                   <option value="rentgen-kostej">костей</option>
                                   <option value="rentgen-mjagkih-tkaney">мягких тканей</option>
                                   <option value="rentgen-molochnyh-zhelez-mammografija">молочной железы (маммография)</option>
                                   <option value="rentgen-pishevoda">пищевода</option>
                                   <option value="ductogafiya">протоков молочной железы (дуктография)</option>
                                   <option value="rentgen-pochek">почек (антеградная пиелография)</option>
                                   <option value="rentgen-mochevoy-sistemy-ugogafiya">мочевыделительной системы (урография)</option>
                                   <option value="rentgen-mochevogo-puzirya">мочевого пузыря</option>
                                   <option value="rentgen_tolstoj_kishki">толстой кишки</option>
                                   <option value="rentgen-jeludka">желудка</option>
                                   <option value="rentgen-pishevoda">пищевода</option>
                                   <option value="rentgen-reber">ребер</option>
                                   <option value="rentgen_serdtsa">сердца</option>
                                   <option value="rentgen-grudnoi-kletki">грудной клетки</option>
                                   <option value="rentgen-legkih">легких</option>
                                   <option value="rentgen-konechnostey">конечностей (нижних и верхних)</option>
                                   <option value="rentgen-stopy">стопы</option>
                                   <option value="rentgen-kisti">кисти</option>
                                   <option value="rentgen-sustavov">суставов</option>
                                </select>
                            </div>
                            <div class="select_wrapper">
                                <select  class="selectServices js-dupleksnoe-skanirovanie">
                                   <option value="dupleksnoe-skanirovanie">Все услуги</option>
                                   <option value="ds-matochno-placentarnogo-krovotoka">маточно-плацентарного кровотока</option>
                                   <option value="ds-sosudov-pochek">сосудов почек</option>
                                   <option value="ds-bryushnoi-aorty">брюшной аорты</option>
                                   <option value="ds-konechnostej">конечностей</option>
                                   <option value="transkranialynoje-ds">транскраниальное</option>
                                   <option value="ds-arterii-golovy">головы</option>
                                </select>
                            </div>
                            <div class="select_wrapper">
                                <select  class="selectServices js-func-diagnostika">
                                   <option value="func-diagnostika">Все услуги</option>
                                   <option value="doplerografija-sosudov-mozga">транскраниальная допплерография</option>
                                   <option value="spirografiya">спирография</option>
                                   <option value="ekg-s-fiz-nagruzkoi">тредмил-тесты</option>
                                   <option value="sutochnoe_AD">суточное мониторирование ад</option>
                                   <option value="sutochnoe-ekg">холтеровское экг</option>
                                   <option value="elektroentsefalografia">Электроэнцефалография (ЭЭГ)</option>
                                   <option value="ehokardiografiya-chpehokg">Чреспищеводная эхокардиография (Чп ЭхоКГ)</option>
                                   <option value="elektrokardiografiya">Электрокардиография (ЭКГ)</option>
                                </select>
                            </div>
                            <div class="select_wrapper">
                                <select  class="selectServices js-enmg">
                                   <option value="enmg">Все услуги</option>
                                   <option value="enmg-issledovanije-nervno-myshechnoj-peredachi">исследование нервно-мышечной передачи</option>
                                   <option value="enmg-licevogo-nerva">лицевого нерва</option>
                                   <option value="enmg-konechnostey">конечностей</option>
                                </select>
                            </div>
                            <div class="select_wrapper">
                                <select  class="selectServices js-endoskopicheskie-issledovaniya">
                                   <option value="endoskopicheskie-issledovaniya">Все услуги</option>
                                   <option value="bronhoskopiya">Бронхоскопия</option>
                                   <option value="gastroskopiya">Гастроскопия</option>
                                   <option value="intestinoskopiya">Интестиноскопия</option>
                                   <option value="kolonoskopiya">Колоноскопия</option>
                                   <option value="cistoskopiya">Цистоскопия</option>
                                   <option value="artroskopiya">Артроскопия</option>
                                   <option value="gisteroskopiya">Гистероскопия</option>
                                   <option value="traheobronhoskopiya">Трахеобронхоскопия</option>
                                   <option value="efgds">Эзофагогастродуоденоскопия (ЭФГДС)</option>
                                   <option value="rectromanoskopiya">Ректороманоскопия</option>
                                   <option value="kolposkopia">Кольпоскопия</option>
                                   <option value="ezofagoskopiya">Эзофагоскопия</option>
                                </select>
                            </div>
                            <div class="select_wrapper">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form_tool_1">
                        <input type="checkbox" id="toggle_sidebar" checked/>
                        <label for="toggle_sidebar">Показывать side bar<br>&nbsp;</label>
                        <input type="checkbox" id="toggle_drag" checked/>
                        <label for="toggle_drag">Показывать форму<br>&nbsp;&nbsp;&nbsp;записи онлайн</label>
                        <span class="text_click">Всплывающая форма позволяет записаться онлайн всего одним щелчком мыши.</span>
                    </fieldset>
                    </div>
                 </div>
            </div>
        </div>
        <div class="wrapper_mod_7 clearfix">
		<div class="modul_preview">
		<div class='browser-wrapper'>
            <div class='top-bar'>
                <div class='row small'>
                    <div class='circle close'></div>
                    <div class='circle minify'></div>
                    <div class='circle zoom'></div>
                    <div class='tab'>

                    </div>
                </div>
                <div class='row flex active large'>
                    <div class='button back'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button forward'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button reload-button'>
                        <div class='arrow-down'></div>
                    </div>
                    <div class='address-bar'>
                        <span class='favicon'></span>

                    </div>
                    <div class='sandwich-button'>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                    </div>
                </div>
            </div>
            <div class="site_block">
					<div class="site_block_logo">LOGO</div>
					<div class="site_block_log"></div>
					<div class="site_block_search"></div>
				</div>
                <div class="site_block_1"></div>
                <div class="site_block_1"></div>
                <div class="site_block_1"></div>
            <div class='content_browser'>
            <div id="medbooking-doctor"></div>
            <script>
                var id_client =  <?=$partner->id;?>,num = "<?=$partner->phone;?>",dragForm = true,aside = true,contentWidth = 750,url = "//diagnostika.medbooking.com/diagnostics?ajax=ref",scriptJs = document.createElement("script");scriptJs.id = "initScript";scriptJs.src = "//medbooking.com/scripts/integrator/diagnosticModule/script.js";document.head.appendChild(scriptJs); </script>
            </div>
        </div>
		</div>
	</div>
</section>
<div id="popup_mod">
	<div class="popup_head_mod">
		<h2>Код</h2>
		<i id="popup_close"></i>
		<span>Вставьте код на сайт</span>
	</div>
	<div class="popup_body-mod">
		<form>
		<div class="code_msg" id="code_msg">
		</div>
			<input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
		</form>
	</div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>";
</script>