<section class="modul">
  <div class="js_form_modul">
   <div class="wrapper_mod clearfix">
       <div class="form_modul">
            <div class="form_modul_preview_11">
                <p>Сервис Medbooking предоставляет услугу для будущих мам. При нажатии на кнопку «узнать подробнее» пользователь попадает в раздел нашего сайта «ведение беременности», где предложено более 50 центров с широким спектром услуг. Есть возможность подобрать индивидуальную программу. Перед аудиторией, нуждающейся в данной услуге, стоит непростая задача, принять правильное решение, тем  более, когда речь идет о будущем ребенке. Medbooking предложит множество путей решения данного вопроса.</p>
                <input id="btn_code" type="button" value="Получить код">
                </div>
               <div class="form_setting">
				<fieldset class="form_size_11">
					<h2>Настройки</h2>
					<h3>Выбрать ориентацию баннера:</h3>
					<input checked="checked" name="orientation" type="radio" id="horizontal">
					<label for="horizontal">Горизонтальный</label>
					<input name="orientation" type="radio" id="vertical">
					<label for="vertical">Вертикальный</label>
				</fieldset>                
                <fieldset class="form_size_11">
					<h3>Выберите размер баннера:</h3>
					<select id="asideWidth" name="menu" size="1">
					<option selected="selected" value="1200">1200</option>
					<option value="1100">1100</option>
					<option value="1000">1000</option>
					<option value="900">900</option>
					<option value="800">800</option>
					<option value="700">700</option>
					</select><label for="menu">PX</label>
                </fieldset> 
            </div>
        </div>
    </div>
</div>
    <div class="wrapper_mod_4 clearfix">
        <div class="modul_preview_11">
            <div class='browser-wrapper'>
                <div class='top-bar'>
                    <div class='row small'>
                    <div class='circle close'></div>
                    <div class='circle minify'></div>
                    <div class='circle zoom'></div>
                    <div class='tab'>
   
                    </div>
                </div>
                <div class='row flex active large'>
                    <div class='button back'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button forward'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button reload-button'>
                        <div class='arrow-down'></div>
                    </div>
                    <div class='address-bar'>
                        <span class='favicon'></span>
                    </div>
                    <div class='sandwich-button'>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                    </div>
                </div>
            </div>
            <div class="site_block">
            <div class="site_block_logo">LOGO</div>
            <div class="site_block_log"></div>
            <div class="site_block_search"></div>
            </div>
            <div class='content_browser'>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div id="bannerWrapHor" style=" height: 200px; width: 1200px;">
				<a id="b-horizontal" class="active_banner" style="display: block; width: 1200px;" href="//medbooking.com/services/category/vedenije-beremennosti?pid=<?=$partner->id;?>" target="_blank"><img src="//medbooking.com/img/integrator/banner_02.jpg" style="max-width: 100%;" alt=""></a>
            </div>
<!--
            <div class="site_block_line">
				<div class="site_line"></div>
				<div class="site_line"></div>
				<div class="site_line"></div>
            </div>
-->
			<div id="bannerWrapVer" style=" height: 400px; width: 240px; float: right; margin-top: 20px;">
				<a id="b-vertical" class="opacity_banner" style="display: block; width: 240px;" href="//medbooking.com/services/category/vedenije-beremennosti?pid=<?=$partner->id;?>" target="_blank"><img src="//medbooking.com/img/integrator/banner_01.jpg" style="max-width: 100%;" alt=""></a>
			</div>
				<script>
					mbId = <?=$partner->id;?>;
					mbPhone = "<?=$partner->phone;?>";
				</script>
<!--
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
-->

            </div>
        </div>
        </div>
        </div> 
</section>
<div id="popup_mod">
    <div class="popup_head_mod">
        <h2>Код</h2>
        <i id="popup_close"></i>
        <span>Вставьте код на сайт</span>
    </div>
    <div class="popup_body-mod">
        <form>
        <div class="code_msg" id="code_msg">
        </div>
            <input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
        </form>
    </div>
</div>
