<?php
/**
 * @var Recommendation $data
 */
$purifier = new CHtmlPurifier();
?>
    <div class="recommendation-item <?= $data->isUnread() ? 'unread' : '' ?>">
        <div class="recommendation-title <?= $data->title ? '' : 'hide' ?>">
            <h2><?= Html::encode($data->title) ?><h2>
        </div>
        <div class="recommendation-message"><?= $data->message ?></div>
        <div class="recommendation-toolbox">
            <?php if (Yii::app()->user->checkAccess("Administrator")) { ?>
                <?= Html::a('Удалить', $data->getUrl('delete'), ['class' => 'btn btn-danger confirm-delete']) ?>
                <?= Html::a('Редактировать', $data->getUrl('edit'), ['class' => 'btn btn-primary']) ?>
            <?php } ?>
        </div>
    </div>
<?php $data->read(); ?>