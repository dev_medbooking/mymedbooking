<?php
/**
 * @var Partner             $partner
 * @var Partner[]           $partners
 * @var string              $phone
 * @var Recommendation      $dataProvider
 * @var CActiveDataProvider $dataProviderSiteUrls
 * @var PartnerSite         $partnerSite
 * @var PartnerSite         $model
 * @var Recommendation      $recommendation
 * @var CAssetManager       $as
 */
$as = Yii::app()->assetManager;
$assets = $as->publish(Yii::getPathOfAlias('webroot.vendor.tinymce.tinymce'));
Yii::app()->clientScript->registerScriptFile($assets . '/tinymce.min.js');

?>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter      alignright alignjustify | bullist numlist outdent indent | link image"
    });
</script>

<section class="wrapper_p">
    <header>
        <?php $this->renderPartial(
            '_header', array(
                'partner'  => $partner,
                'partners' => $partners,
                'phone'    => $phone,
            )
        ); ?>
    </header>
    <?php $this->renderPartial('_recommendation-header') ?>
    <section class="groop-table">
        <div class="container">
            <form method="post">
                <br>
                <?= Html::activeTextField(
                    $recommendation, 'title', ['class' => 'form-control', 'placeholder' => 'Заголовок рекомендации']
                ) ?>
                <?= Html::activeTextArea($recommendation, 'message', ['rows' => 30]) ?>
                <br>
                <?php if ($recommendation->getIsNewRecord()) { ?>
                    <?= Html::submitButton('Добавить рекомендацию', ['class' => 'btn btn-primary pull-right']) ?>
                <?php } else { ?>
                    <?= Html::submitButton('Сохранить рекомендацию', ['class' => 'btn btn-success pull-right']) ?>
                <?php } ?>
            </form>
        </div>
    </section>
    <?php $this->renderPartial('_footer') ?>
</section>
