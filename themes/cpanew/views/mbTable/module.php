<?php
/**
 * @var Partner   $partner
 * @var Partner[] $partners
 * @var string    $phone
 * @var integer   $id
 */
?>
<section class="wrapper_modul">
	<header class="header_p">
		<?php $this->renderPartial('_header', array(
			'partner' => $partner,
			'partners' => $partners,
			'phone' => $phone,
		));?>
		
	</header>
	<section class="nav_modules">
		<div class="nav_modules_wrap">
			<ul class="menu_modules clearfix">
				<li class="<?=($id == 22 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 22));?>">Список</a></li>
				<li class="<?=($id == 23 ? 'active' : '');?>">
					<a href="<?=Url::to('mbTable/module', ['id' => 23]);?>">Список клиник</a>
				</li>
                <li class="<?=($id == 5 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 5));?>">Информер телефон</a></li>
                <li class="<?=($id == 7 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 7));?>">Диагностика</a></li>
                <li class="<?=($id == 8 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 8));?>">Кнопка</a></li>
                <li class="<?=($id == 9 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 9));?>">Форма drag&amp;drop</a></li>
				<?php /*<li class="<?=($id == 10 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 10));?>">Подбор врача</a></li> */ ?>
                <li class="<?=($id == 11 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 11));?>">Беременность</a></li>
                <li class="<?=($id == 13 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 13));?>">Форма (fixed)</a></li>
                <li class="<?=($id == 21 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 21));?>">Слайдер</a></li>
			</ul>
		</div>
	</section>
	<script src="<?=Yii::app()->theme->baseUrl;?>/js/ZeroClipboard.js"></script> 
	<script src="<?=Yii::app()->theme->baseUrl;?>/js/settings/mod<?=$id;?>.js"></script>
	<?php $this->renderPartial("modules/_module{$id}", array('partner' => $partner));?>
	<?php $this->renderPartial('_footer');?>
</section>
