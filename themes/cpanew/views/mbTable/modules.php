<?php
/**
 * @var Partner   $partner
 * @var Partner[] $partners
 * @var string    $phone
 */
?>
<section class="wrapper_p">
	<header class="header_p">
		<?php $this->renderPartial('_header', array(
			'partner' => $partner,
			'partners' => $partners,
			'phone' => $phone,
		));?>
	</header>
	<section class="nav_modules">
		<div class="nav_modules_wrap">
			<ul class="menu_modules clearfix">
				<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 22));?>">Список</a></li>
				<li><a href="<?= Url::to('mbTable/module', ['id' => 23]); ?>">Список клиник</a></li>
				<?/*?><li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 2));?>">2 Список (PHP)</a></li>
				<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 3));?>">3 Список (JS)</a></li><?*/?>

				<?/*?><li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 4));?>">4 Форма&nbsp;и список</a></li>
				<?*/?>
				<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 5));?>">Информер телефон</a></li>
				<?/*?><li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 6));?>">6 Консультанты</a></li>
<?*/?>
				<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 12));?>">Консультанты и запись</a></li>
				<?/*?><li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 7));?>">8 Диагностика</a></li>
<?*/?>
				<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 8));?>">Кнопка</a></li>
				<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 9));?>">Форма drag&amp;drop</a></li>
				<?php /*				<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 10));?>">Подбор врача</a></li> */ ?>
				<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 11));?>">Беременность</a></li>
				<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 13));?>">Форма (fixed)</a></li>
				<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 21));?>">Слайдер</a></li>

			</ul>
		</div>
	</section>	
	<?php $this->renderPartial("modules/_modules", array('partner' => $partner));?>
	<?php $this->renderPartial('_footer');?>
</section>
