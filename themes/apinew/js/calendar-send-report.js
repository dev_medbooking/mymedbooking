$(document).ready(function(){ 
    $("body").on("click", ".control-submit", function() {
        var url = $('.calendar-submit').attr('data-href') + "&" + $('.hidden-form').serialize();
        $.ajax({
            url: url,
            type: 'get',
            data: $('.failure-form').serialize(),
            dataType: "json",
            success: function(r) {
                $("#admin-tbody").html('');
                $('#pages').html('');
                if (r.tbody) {
                    $("#admin-tbody").html(r.tbody);
                    if (r.pages) {
                        $('#pages').html(r.pages);
                    } else {
                        $('#pages').html('');
                    }
                }
                if (r.filter) {
                    updateFilter(r.filter);
                }
                if (r.count) {
                    $("#summary").html(r.count);
                }
            }
        });
    });
    function find(array, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] === value) {
                return i;
            }
        }
        return -1;
    }
    $("body").on("click", ".calendar", function(){
        $(".pmu-today").each(function(){
            $(this).nextAll(".pmu-button").addClass("pmu-disabled-n"); 
            $(this).parents(".pmu-instance").nextAll(".pmu-instance").find(".pmu-days .pmu-button").addClass("pmu-disabled-n");
            $(this).prevAll(".pmu-button").removeClass("pmu-disabled-n");
            $(".pmu-button").removeClass("pmu-selected");
            $(".pmu-today").addClass("pmu-selected");
            $(".pmu-today").removeClass("pmu-disabled-n");
        });
    });
    $("body").on("click", ".pmu-button", function(){
        $(".pmu-today").each(function(){
            $(this).nextAll(".pmu-button").addClass("pmu-disabled-n"); 
            $(this).parents(".pmu-instance").nextAll(".pmu-instance").find(".pmu-days .pmu-button").addClass("pmu-disabled-n");
            $(this).prevAll(".pmu-button").removeClass("pmu-disabled-n");
            $(".pmu-today").removeClass("pmu-disabled-n");
        });
    });
    $("body").on("click", ".yesterday", function(){
        $(".pmu-today").removeClass("pmu-selected").css({"color":"#000"});
    });
    var monthArr = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
    $("body").on("click", ".pmu-button", function() {
        $(".pmu-not-in-month").removeClass("pmu-selected");
        $(".top-calendars .right span").removeClass("active");
        var startDate = $(".pmu-selected.pmu-button:first");
        var endDate = $(".pmu-selected.pmu-button:last").not(".pmu-not-in-month");
        var startMonth = startDate.parents(".pmu-instance").find(".pmu-month").text().split(",")[0];
        var endMonth = endDate.parents(".pmu-instance").find(".pmu-month").text().split(",")[0];
        startMonth = find(monthArr, startMonth) + 1;
        if (startMonth < 10) {
            startMonth = "0" + startMonth
        }
        endMonth = find(monthArr, endMonth) + 1;
        if (endMonth < 10) {
            endMonth = "0" + endMonth;
        }
        startDate = startDate.text();
        endDate = endDate.text();
        if (startDate < 10) {
            startDate = "0" + startDate;
        }
        if (endDate < 10) {
            endDate = "0" + endDate;
        }
    });
    $('body').on('click', '.calendar-submit', function() {
        if($('input[name="report"]').val() == 'report'){
                console.log('ddadad');
                var href =location.href.split('?&');
                if ($('.week').hasClass('active')){
                    var day = $('.pmu-today').html();
                    if (day < 10) {
                        day = "0" + day
                    }
                    day2 = Number(day) - 6
                    var year = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().substr(-4);
                    var month = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().split(",")[0];
                    var month2 = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().split(",")[0];
                    month = find(monthArr, month) + 1;
                    if(day2 >= 26){
                        day2 = day2 - 30;
                        month2 = find(monthArr, month2) + 2;
                    }
                    else{
                        month2 = find(monthArr, month2) + 1;
                    }
                    if(Number(day2) < 0){
                        $(".pmu-today").prevAll(".pmu-button").addClass("week");
                        var d =$(".pmu-today").parents(".pmu-instance").last().find(".pmu-days .pmu-button").not(".pmu-not-in-month").last().text();
                        day2 = day2 - (d*(-1));
                        month2 = month2 -1;
                    
                    }
                    if (day2 < 10) {
                        day2 = "0" + day2;
                    }
                    if (month2 < 10) {
                        month2 = "0" + month2;
                    }
                    if (month < 10) {
                        month = "0" + month;
                    }
                    var todayStr2 = year+"-"+month+"-"+day; 
                    var todayStr = year+"-"+month2+"-"+day2;
                    var author = $('input[name="author_id"]').val();
                    var url = href[0]+'?&data-from='+todayStr+'&data-to='+todayStr2+'&author_id='+author;
                }else if ($('.yesterday').hasClass('active')) {
                    var day = $('.pmu-today').prev().html();
                    var year = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().substr(-4);
                    var month = $('.pmu-selected').parents('.pmu-instance').find('.pmu-month').text().split(",")[0];
                    function find(array, value) {
                        for (var i = 0; i < array.length; i++) {
                            if (array[i] === value) {
                                return i;
                            }
                        }
                        return -1;
                    }
                    month = find(monthArr, month) + 1;
                    if(day == "31"){
                        month = month - 1;
                    }
                    if (month < 10) {
                        month = "0" + month
                    }
                    if (day < 10) {
                        day = "0" + day
                    }
                    var todayStr = year+"-"+month+"-"+day;
                    var author = $('input[name="author_id"]').val();
                    var url = href[0]+'?&data-from='+todayStr+'&author_id='+author;
                }else if ($('.today').hasClass('active')) {
                    var day = $('.pmu-today').html();
                    if (day < 10) {
                        day = "0" + day
                    }
                    var year = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().substr(-4);
                    var month = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().split(",")[0];

                    function find(array, value) {
                        for (var i = 0; i < array.length; i++) {
                            if (array[i] === value) {
                                return i;
                            }
                        }
                        return -1;
                    }
                    month = find(monthArr, month) + 1;
                    if (month < 10) {
                        month = "0" + month
                    }
                    var todayStr = year+"-"+month+"-"+day; 
                    var author = $('input[name="author_id"]').val();
                    var url = href[0]+'?&data-from='+todayStr +'&author_id='+author;
                }else{
                    var startDate = $(".pmu-selected.pmu-button:first");
                    var endDate = $(".pmu-selected.pmu-button:last").not(".pmu-not-in-month");
                    var startMonth = startDate.parents(".pmu-instance").find(".pmu-month").text().split(",")[0];
                    var endMonth = endDate.parents(".pmu-instance").find(".pmu-month").text().split(",")[0];
                    var year = endDate.parents(".pmu-instance").find(".pmu-month").text().split(",")[1].substr(-4);
                    startMonth = find(monthArr, startMonth) + 1;
                    if (startMonth < 10) {
                        startMonth = "0" + startMonth
                    }
                    endMonth = find(monthArr, endMonth) + 1;
                    if (endMonth < 10) {
                        endMonth = "0" + endMonth;
                    }
                    startDate = startDate.text();
                    endDate = endDate.text();
                    if (startDate < 10) {
                        startDate = "0" + startDate;
                    }
                    if (endDate < 10) {
                        endDate = "0" + endDate;
                    }
                    var strStart = year+"-"+startMonth+"-"+startDate;
                    var strEnd =   year+"-"+endMonth+"-"+endDate;
                    var author = $('input[name="author_id"]').val();
                    var url = href[0]+'?&data-from='+strStart+'&data-to='+strEnd +'&author_id='+author;
                }
                window.location=url;
        } else if ($('input[name="report"]').val() == 'failure') {
            var date = getDate();
            $('.failure-form input[name="time1"]').val('').val(date[1]);
            $('.failure-form input[name="time2"]').val('').val(date[2]);
            var url = $('.failure-form').attr('action') + "&" + $('.failure-form').serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: $('.failure-form').serialize(),
                dataType: "json",
                success: function(r) {
                    if (r.body) {
                        $('.wrapper-calendars').slideUp(300);
                        $('.failure_body').html(r.body);
                    }
                }
            });
        } else if ($('input[name="report"]').val() == 'failure') {
            var date = getDate();
            $('.failure-form input[name="time1"]').val('').val(date[1]);
            $('.failure-form input[name="time2"]').val('').val(date[2]);
            var url = $('.failure-form').attr('action') + "&" + $('.failure-form').serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: $('.failure-form').serialize(),
                dataType: "json",
                success: function(r) {
                    if (r.body) {
                        $('.wrapper-calendars').slideUp(300);
                        $('.failure_body').html(r.body);
                    }
                }
            });
        } else if ($('input[name="report"]').val() == 'report_new') {
            var date = getDate();
            $('.failure-form input[name="data-from"]').val('').val(date[1]);
            $('.failure-form input[name="data-to"]').val('').val(date[2]);
            var url = $('.failure-form').attr('action') + "?" +$('.failure-form').serialize();
            window.location=url;
        }else if($('input[name="report"]').val() == 'statistic'){
            var date = getDate();
            $('.failure-form input[name="data_from"]').val('').val(date[1]);
            $('.failure-form input[name="data_to"]').val('').val(date[2]);
            var url = $('.failure-form').attr('action')+'?&'+$('.failure-form').serialize();
            window.location=url;
        }else if ($('input[name="report"]').val()=='call'){
            var date = getDate();
            $('.call-form input[name="data-from"]').val('').val(date[1]);
            $('.call-form input[name="data-to"]').val('').val(date[2]);
            var url = $('.call-form').attr('action')+'?&'+$('.call-form').serialize();
            window.location=url;
        } else {
            clearDate();
            if(!$(".right span").hasClass("active")){
                $('.wrapper-calendars').slideUp(300);
                var startDate = $(".pmu-selected.pmu-button:first");
                var endDate = $(".pmu-selected.pmu-button:last").not(".pmu-not-in-month");
                var startMonth = startDate.parents(".pmu-instance").find(".pmu-month").text().split(",")[0];
                var endMonth = endDate.parents(".pmu-instance").find(".pmu-month").text().split(",")[0];
                var year = endDate.parents(".pmu-instance").find(".pmu-month").text().split(",")[1].substr(-4);
                startMonth = find(monthArr, startMonth) + 1;
                if (startMonth < 10) {
                    startMonth = "0" + startMonth
                }
                endMonth = find(monthArr, endMonth) + 1;
                if (endMonth < 10) {
                    endMonth = "0" + endMonth;
                }
                startDate = startDate.text();
                endDate = endDate.text();
                if (startDate < 10) {
                    startDate = "0" + startDate;
                }
                if (endDate < 10) {
                    endDate = "0" + endDate;
                }
                var strStart = year+"-"+startMonth+"-"+startDate;
                var strEnd =   year+"-"+endMonth+"-"+endDate; 
                    var leftMenu = parseInt($('.left .active').attr('data-number'));
                    if (leftMenu === 0) {
                        $('.dn1 a').text(strStart);
                        $('.dn2 a').text(strEnd);
                        $('.hidden-date-create1').val(strStart);
                        $('.hidden-date-create2').val(strEnd);
                    } else if (leftMenu === 1) {
                        $('.dn3 a').text(strStart);
                        $('.dn4 a').text(strEnd);
                        $('.time1').val(strStart);
                        $('.time2').val(strEnd);
                    } else if (leftMenu === 2) {
                        $('.dn5 a').text(strStart);
                        $('.dn6 a').text(strEnd);
                        $('.edition1').val(strStart);
                        $('.edition2').val(strEnd);
                    }
                var linkStr = $(this).attr('data-href') + "&" + $('.hidden-form').serialize();
                $(".lstr").attr("href", linkStr);
                    $.ajax({
                        url: $(this).attr('data-href'),
                        type: 'get',
                        data: $('.hidden-form').serialize(),
                        dataType: "json",
                        success: function(r) {
                            $("#admin-tbody").html('');
                            $('#pages').html('');
                            if (r.tbody) {
                                $("#admin-tbody").html(r.tbody);
                                if (r.pages) {
                                    $('#pages').html(r.pages);
                                } else {
                                    $('#pages').html('');
                                }
                            }
                            if(r.filter){
                                updateFilter(r.filter);
                            }
                            if(r.count){
                                $("#summary").html(r.count);
                            }
                        }
                    });
            } else{
                $('.wrapper-calendars').slideUp(300);
                if ($('.week').hasClass('active')) {
                          var day = $('.pmu-today').html();
                    if (day < 10) {
                        day = "0" + day
                    }
                    day2 = Number(day) - 6
                    var year = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().substr(-4);
                    var month = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().split(",")[0];
                    var month2 = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().split(",")[0];
                    month = find(monthArr, month) + 1;
                    if(day2 >= 26){
                        day2 = day2 - 30;
                        month2 = find(monthArr, month2) + 2;
                    }
                    else{
                        month2 = find(monthArr, month2) + 1;
                    }
                    if(Number(day2) < 0){
                        $(".pmu-today").prevAll(".pmu-button").addClass("week");
                        var d =$(".pmu-today").parents(".pmu-instance").last().find(".pmu-days .pmu-button").not(".pmu-not-in-month").last().text();
                        day2 = day2 - (d*(-1));
                        month2 = month2 -1;
                    }
                    if (day2 < 10) {
                        day2 = "0" + day2;
                    }
                    if (month2 < 10) {
                        month2 = "0" + month2;
                    }
                    if (month < 10) {
                        month = "0" + month;
                    }
                    var todayStr2 = year+"-"+month+"-"+day; 
                    var todayStr = year+"-"+month2+"-"+day2; 
                    var leftMenu = parseInt($('.left .active').attr('data-number'));
                    if (leftMenu === 0) {
                        $('.dn1 a').text(todayStr);
                        $('.dn2 a').text(todayStr2);
                        $('.hidden-date-create1').val(todayStr);
                        $('.hidden-date-create2').val(todayStr2);
                    } else if (leftMenu === 1) {
                        $('.dn3 a').text(todayStr);
                        $('.dn4 a').text(todayStr2);
                        $('.time1').val(todayStr);
                        $('.time2').val(todayStr2);
                    } else if (leftMenu === 2) {
                        $('.dn5 a').text(todayStr);
                        $('.dn6 a').text(todayStr2);
                        $('.edition1').val(todayStr);
                        $('.edition2').val(todayStr2);
                    }
                } else if ($('.yesterday').hasClass('active')) {
                    var day = $('.pmu-today').prev().html();
                    var year = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().substr(-4);
                    var month = $('.pmu-selected').parents('.pmu-instance').find('.pmu-month').text().split(",")[0];
                    console.log(month);
                    function find(array, value) {
                        for (var i = 0; i < array.length; i++) {
                            if (array[i] === value) {
                                return i;
                            }
                        }
                        return -1;
                    }
                    month = find(monthArr, month) + 1;
                    if(day == "31"){
                        month = month - 1;
                    }
                    if (month < 10) {
                        month = "0" + month
                    }
                    if (day < 10) {
                        day = "0" + day
                    }
                    var todayStr = year+"-"+month+"-"+day; 
                    var leftMenu = parseInt($('.left .active').attr('data-number'));
                    if (leftMenu === 0) {
                        $('.dn1 a').text(todayStr);
                        $('.dn2 a').text(todayStr);
                        $('.hidden-date-create1').val(todayStr);
                        $('.hidden-date-create2').val(todayStr);
                    } else if (leftMenu === 1) {
                        $('.dn3 a').text(todayStr);
                        $('.dn4 a').text(todayStr);
                        $('.time1').val(todayStr);
                        $('.time2').val(todayStr);
                    } else if (leftMenu === 2) {
                        $('.dn5 a').text(todayStr);
                        $('.dn6 a').text(todayStr);
                        $('.edition1').val(todayStr);
                        $('.edition2').val(todayStr);
                    }
                } else if ($('.today').hasClass('active')) {
                    var day = $('.pmu-today').html();
                    if (day < 10) {
                        day = "0" + day
                    }
                    var year = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().substr(-4);
                    var month = $('.pmu-today').parents('.pmu-instance').find('.pmu-month').text().split(",")[0];
                    function find(array, value) {
                        for (var i = 0; i < array.length; i++) {
                            if (array[i] === value) {
                                return i;
                            }
                        }
                        return -1;
                    }
                    month = find(monthArr, month) + 1;
                    if (month < 10) {
                        month = "0" + month
                    }
                    var todayStr = year+"-"+month+"-"+day; 
                    var leftMenu = parseInt($('.left .active').attr('data-number'));
                    if (leftMenu === 0) {
                        $('.dn1 a').text(todayStr);
                        $('.dn2 a').text(todayStr);
                        $('.hidden-date-create1').val(todayStr);
                        $('.hidden-date-create2').val(todayStr);
                    } else if (leftMenu === 1) {
                        $('.dn3 a').text(todayStr);
                        $('.dn4 a').text(todayStr);
                        $('.time1').val(todayStr);
                        $('.time2').val(todayStr);
                    } else if (leftMenu === 2) {
                        $('.dn5 a').text(todayStr);
                        $('.dn6 a').text(todayStr);
                        $('.edition1').val(todayStr);
                        $('.edition2').val(todayStr);
                    }
                }
                var linkStr = $(this).attr('data-href') + "&" + $('.hidden-form').serialize();
                $(".lstr").attr("href", linkStr);
                $.ajax({
                    url: $(this).attr('data-href'),
                    type: 'get',
                    data: $('.hidden-form').serialize(),
                    dataType: "json",
                    success: function(r) {
                        $("#admin-tbody").html('');
                        $('#pages').html('');
                        if (r.tbody) {
                            $("#admin-tbody").html(r.tbody);
                            if (r.pages) {
                                $('#pages').html(r.pages);
                            } else {
                                $('#pages').html('');
                            }
                        }
                        if (r.filter) {
                            updateFilter(r.filter);
                        }
                        if (r.count) {
                            $("#summary").html(r.count);
                        }
                    }
                });
            }
        }
    });
    $('body').on('click', '.fake_select_operator', function(){
        $(this).find('.fake_select_operator_title').hide();
        $(this).find('#report_list').show();
    });
    $('body').on('click', '#report_list option', function(){
$(this).not('.added').parent().after("<span class='fio_item' value='"+$(this).attr('value')+"'><input type='hidden' name='operators[]' value='"+$(this).attr('value')+"' >"+$(this).text()+"</li>");
        $(this).addClass('added');
        $('.fake_select_operator_title').show();
        $('#report_list').hide();
        $('body').on('click', '.fio_item', function(){
            var value = $(this).attr('value');
            $("#report_list option[value='"+value+"']").removeClass('added');
            $(this).remove();
        });
    });
});
function updateFilter(filter) {
    $('.hidden-block').html(filter);
    $('.table-api-main thead td').each(function(i, val) {
        $(this).find('a').attr('href', $('.' + $(this).attr('class') + '_2').find('a').attr('href'));
    });
}
function clearDate() {
    $('.dn1 a').text('');
    $('.dn2 a').text('');
    $('.hidden-date-create1').val('');
    $('.hidden-date-create2').val('');
    $('.dn3 a').text('');
    $('.dn4 a').text('');
    $('.time1').val('');
    $('.time2').val('');
    $('.dn5 a').text('');
    $('.dn6 a').text('');
    $('.edition1').val('');
    $('.edition2').val('');
}
function getDate() {
    var month= {Январь:'01', Февраль:'02', Март:'03', Апрель:'04', Май:'05', Июнь:'06', Июль:'07', Август:'08', Сентябрь:'09', Октябрь:'10', Ноябрь:'11', Декабрь:'12'};
    var day1 =  $('.pmu-selected :first').text();
    var day2 =  $('.pmu-selected :last').text();
    var month1 = $('.pmu-selected :first').parents('.pmu-instance').find('.pmu-month').text().split(',')[0];
    var month2 = $('.pmu-selected :last').parents('.pmu-instance').find('.pmu-month').text().split(',')[0];
    month1 = month[month1];
    month2 = month[month2];
    var year1=$('.pmu-selected :first').parents('.pmu-instance').find('.pmu-month').text().split(',')[1];
    var year2=$('.pmu-selected :first').parents('.pmu-instance').find('.pmu-month').text().split(',')[1];
    var time1=$.trim(year1+'-'+month1+'-'+day1);
    var time2=$.trim(year2+'-'+month2+'-'+day2);
    var times={1:time1,2:time2};
    return times;
}