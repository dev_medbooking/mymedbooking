function ajaxShowSourceReport(self) {
    //var href = '/mbCallsReport';
    var href = self.data('ref');
    var source = self.data('source');
    var dateStart = self.data('start');
    var dateEnd = self.data('end');
    var medium = self.data('medium');
    var request = self.data('request');
    $.ajax({
        url: href,
        type: 'get',
        data: {
            source: source,
            dateStart: dateStart,
            dateEnd: dateEnd,
            medium: medium,
            request: request
        },
        dataType: 'json',
        success: function(r) {
            $("#loader").hide();
            if(!medium)
                $(".js-source-report-table-"+source).html(r.tbody);
            else
                $("[class='js-source-report-table-"+source + "'][ data-medium='"+medium+"'][ data-request='"+request+"']").html(r.tbody);
        },
        beforeSend: function(xhr) {
            $("#loader").show();
        }
    }).done(function(msg) {
        $("#loader").hide();
    });
}

function ajaxShowCampaignReport(self) {
    //var href = '/mbCallsReport';
    var href = self.data('ref');
    var campaign = self.data('campaign');
    var dateStart = self.data('start');
    var dateEnd = self.data('end');
    var source = self.data('source');
    var medium = self.data('medium');
    var request = self.data('request');
    $.ajax({
        url: href,
        type: 'get',
        data: {
            campaign: campaign,
            dateStart: dateStart,
            dateEnd: dateEnd,
            campaign_source: source,
            medium: medium,
            request: request
        },
        dataType: 'json',
        success: function(r) {
            $("#loader").hide();
            if(!medium)
                $("tr[data-table-name='js-campaign-report-table-"+campaign+"'").html(r.tbody);
            else {
                $("tr[data-table-name='js-campaign-report-table-"+campaign+"'][  data-medium='"+medium+ "'][ data-request='"+request+"']").html(r.tbody);
            }
        },
        beforeSend: function(xhr) {
            $("#loader").show();
        }
    }).done(function(msg) {
        $("#loader").hide();
    });
}

$(document).ready(function() {
    $('body').on('click', '.js-show-source-report', function() {
        ajaxShowSourceReport($(this));
        return false;
    });
    $('body').on('click', '.js-show-campaign-report', function() {
        ajaxShowCampaignReport($(this));
        return false;
    });
});
