$(document).ready(function(){






/*$(".table.graduation .body-table").prepend("<div class='wrapper_nameCell' style='position: absolute;'></div>");
$(".table.graduation .body-table .row").each(function(){
    var r = $(this).find(".cell:first").html()
    $(this).find(".cell:first").empty()
    $(".wrapper_nameCell").append("<div class='call cellDinamic name' style='position: relative; z-index:9; border-bottom: 1px solid #ebebeb; width:340px; padding: 10px 0 14px 20px;  white-space: nowrap;text-overflow: ellipsis;overflow: hidden;'>"+r+"</div>");
});




    var full_weight = 40;
    $(".table.graduation .caption .cell").each(function(){
          full_weight += $(this).innerWidth();
    });
    $(".table.graduation,  .js-graduation-filter .caption").width(full_weight);

if($(".js-graduation-filter").length){
    var height_moneyInfo = $(".js-graduation-filter").offset().top;
}

    $(window).scroll(function(){
        if($(window).scrollTop() >  height_moneyInfo){
            $(".js-graduation-filter").addClass("moneyFixed");
        }
        else{
            $(".js-graduation-filter").removeClass("moneyFixed");
            console.log(1);
        }   
    });
$(".center").scroll(function(){
    $(".js-graduation-filter").scrollLeft($(this).scrollLeft())
})
$(".js-graduation-filter").scroll(function(){
    $(".center").scrollLeft($(this).scrollLeft())
})*/
        
//    $(".table.graduation .body-table .cell.name:first-child").each(function(){
//        var firstCellName = $(this);
//        var firstCellNameLeft = firstCellName.offset().left;
//        var firstCellNameTop = firstCellName. offset().top;
//        firstCellName.css({
//            "left": firstCellNameLeft,
//            "top": firstCellNameTop,
//            "position": 'absolute'
////            "display": 'block',
////            "height": '44px'
////            "margin-top" : '15px'
//        });
//    });
});



var apiMed = (function(){
    var difference = function(){
        $("body").on("click", ".hiddenAffiliates__left__options__cell__num  label", function(){
            var that = $(this),
                values,
                total_num = that.parents(".hiddenAffiliates__left__options__row").find(".main_total").text();
            if(parseInt(that.prev().val())){
                values = that.prev().val();
            }else{
                values = 0;
            }
            that.parents(".hiddenAffiliates__left__options__cell__num").find("a").show().text(values);
            that.parents(".hiddenAffiliates__left__options__cell__num").find(".difference").show();

            that.parents(".ajax_link").find(".hiddenAffiliates__left__options__cell__num").each(function(){
                $(this).find(".difference").text(total_num - $(this).find("a").text());
            });
            that.parents(".hiddenAffiliates__textWrapper").hide();
        });
    };

	var graduationDifference = function(){
			for(var i =0; i < $(".graduation .body-table .row").length; i++){
				var summRow = $(".graduation .body-table .row").eq(i).find(".count_patient").text();
				for(var k = 0; k < $(".graduation .body-table .row").eq(i).find(".next_count").length; k++){
					var difference = summRow - Number($(".graduation .body-table .row").eq(i).find(".next_count").eq(k).text());
					if(difference != summRow){
						$(".graduation .body-table .row").eq(i).find(".next_count").eq(k).append("<span class='grad_index'>"+difference+"</span>")
					}

				}
			}
		};

    var noteForm = function(){
        $("body").on("click", ".business-form-wrapper ._save_btn, ._create_clinic ._save_btn", function(){
            var check = true;
            $('.is_error').remove();
            $('.note-form').each(function(){
                if(!$(this).find(":text").val()){
                    $(this).append('<span class="is_error">заполните поле</span>');
                    check = false;
                }
            });
            if(!check){
                return false;
            }
        });
    }
    var createLawReport = function() {
        $("body").on("click", ".js-save-law-report", function() {
            var checkFields = true, // проверка полей
                checkLawReport = false; // проверка счета за месяц
            $('.is_error').remove();
            $('.note-form').each(function() {
                if(!$(this).find(":text").val()) {
                    $(this).append('<span class="is_error">заполните поле</span>');
                    checkFields = false;
                }
            });
            if($('.request_date').is(':visible') && $('.request_date').val()) {
                $.ajax({
                    url: $('.request_date').attr('data-href'),
                    type: 'POST',
                    data: {
                        id:$('#create-block').attr('data-id'),
                        date:$('.request_date').val()
                    },
                    dataType: "json",
                    async:false,
                    success: function(r) {
                        if(r.success==1){
                            checkLawReport = false;
                            alert('Счет на этот месяц уже существует');
                        }else{
                            checkLawReport = true;
                        }

                    }

                });
            }
            if(!checkFields || !checkLawReport){
                return false;
            }
        });
    };
    var createClinicReport = function() {
        $("body").on("click", ".js-save-clinic-report", function() {
            var checkFields = true, // проверка полей
                checkLawReport = false; // проверка счета за месяц
            $('.is_error').remove();
            $('.note-form').each(function() {
                if(!$(this).find(":text").val()) {
                    $(this).append('<span class="is_error">заполните поле</span>');
                    checkFields = false;
                }
            });
            if($('.request_date').is(':visible') && $('.request_date').val()) {
                $.ajax({
                    url: $('.request_date').attr('data-href'),
                    type: 'POST',
                    data: {
                        id:$('#create-block').attr('data-id'),
                        date:$('.request_date').val()
                    },
                    dataType: "json",
                    async:false,
                    success: function(r) {
                        if(r.success==1){
                            checkLawReport = false;
                            alert('Счет на этот месяц уже существует');
                        }else{
                            checkLawReport = true;
                        }

                    }

                });
            }
            if(!checkFields || !checkLawReport){
                return false;
            }
        });
    };
    var deleteForm = function (){
        $("body").on('click','.row_deleted',function(){
            if (!confirm('Продолжить удаление?')) {
                return false;
            }

        });
    };

    var createExel = function(){
        var reportsForExport = [];

        $('body').on('click','.import_exel',function(){
            var fields = $('.exel_form').serializeArray(),
                date = $('#js-query-date-start').val(),
                dateEnd = $('#js-query-date-end').val();
            $.ajax({
                url: $(this).attr('data-href'),
                type: 'POST',
                data: {
                    fields: fields,
                    date: date,
                    dateEnd: dateEnd
                },
                dataType: "json",
                success: function(r) {
                    if(r.href){
                        $('.exel_form').append(r.href);
                    }
                }
            });
            return false;
        });

        $('body').on('click','.js-excel-report',function(){
            var url = $(this);
            $.ajax({
                url: $(this).attr('data-href'),
                dataType: "json",
                success: function(r) {
                    if(r.href){
                        url.parent().parent().find('.download_file_text').append(r.href);
                    }
                }
            });
            return false;
        });


        $('body').on('click','.import_exel_single',function(){
            reportsForExport.push($(this).data('id'));
            //$('.content_prices .moneyInfo').append('<a class="js-reports-for-export" target="_blank" href="/mbLawReports/ExelAdminLawSingle/?id='+reportsForExport+'">Скачать отчеты</a>');
            return false;
        });
        $('body').on('click','.exel_download',function(){
            $(this).remove();
        });

    };

    var check = function(){
        $(".exel_form input").iCheck({
            checkboxClass: 'icheckbox_polaris',
            radioClass: 'iradio_polaris',
            increaseArea: '-10%'
        });
    };

    var deletePrice = function(){
        $('body').on('click','.js-delete-price',function(){
            if (confirm('Продолжить удаление?')) {
                var self = $(this);
                $.ajax({
                    url: self.attr('href'),
                    type: 'POST',
                    data: {data:1},
                    dataType: "json",
                    success: function(r) {
                        if(r.success){
                            $('.column_rows_'+self.attr('data-id')).remove();
                        }
                    }
                });
            }
            return false;
        });
    };

    var CalendarStart = function(){
        $('#js-query-date-start, #js-query-date-end').datepicker({
            lang: "ru",
            dateFormat: "yy-mm-dd"
        });
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Текущее время',
            closeText: 'Закрыть',
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);

    };

    return {
        difference: difference,
	    graduationDifference: graduationDifference,
        //noteForm: noteForm,
        //deleteForm:deleteForm,
        //check:check,
        createExel:createExel,
        createLawReport:createLawReport,
        //clonePrice:clonePrice,
        //deletePrice:deletePrice,
        CalendarStart:CalendarStart
    }
}());

var calendarShow = function(){
    $("body").on("click", ".hiddenAffiliates__right__top__calendarWrap__selectDate", function(){
//        $(this).hide();
        $(this).parents(".hiddenAffiliates__right__top").find(".hiddenAffiliates__right__top__calendarWrap").show();
        $(this).parents(".hiddenAffiliates__right__top").find(".calendar_status").focus();
    });
};
$(function() {

    $("body").on("click", ".js-sub-menu", function(){
       if($(this).hasClass("active")){
           $(".menuWrap__reportMenu__sub__menu").slideUp(300);
           $(this).removeClass("active");
       }else{
           $(".menuWrap__reportMenu__sub__menu").slideDown(300);
           $(this).addClass("active");
       }
    });
    
    
    
    
    
  var height_moneyInfo = $(".moneyInfo, .filter-table").offset().top;
    $(window).scroll(function(){
        if($(window).scrollTop() >  height_moneyInfo){
            $(".filter-table").addClass("moneyFixed");
            var height_fix = $(".filter-table").css('height');
            $(".height-fix-div").css('height',height_fix);
        }
        else{
            $(".filter-table").removeClass("moneyFixed");
        }   
    });

    $(".content_prices .body-table .row")
    $("body").on("change",".chosen-select", function(){
        var calStart = $("#js-query-date-start");
        var calEnd = $("#js-query-date-end");
        if($(this).val().length === 4){
            var year = $(this).val();
            if($("#js-query-date-start").val().split("-")[0] == "2016" && $("#js-query-date-start").val().split("-")[1] == "02"){
                calStart.val(year + "-" + calStart.val().split("-")[1] + "-" + calStart.val().split("-")[2]);
                calEnd.val(year + "-" + calEnd.val().split("-")[1] + "-" + "29");   
            }else{
                calStart.val(year + "-" + calStart.val().split("-")[1] + "-" + calStart.val().split("-")[2]);
                calEnd.val(year + "-" + calEnd.val().split("-")[1] + "-" + "28");      
            }
        }
        else if($(this).val().length === 2){
            var month = $(this).val(),
                day = "qwe";
            switch (month) {
                case "01":
                    day = 31;
                    break;
                case "02":
                    if(calStart.val().split("-")[0] != 2016){
                        day = 28;
                    }else{
                        day = 29;
                    }
                    break;
                case "03":
                    day = 31;
                    break;
                case "04":
                    day = 30;
                    break;
                case "05":
                    day = 31;
                    break;
                case "06":
                    day = 30;
                    break;
                case "07":
                    day = 31;
                    break;
                case "08":
                    day = 31;
                    break;
                case "09":
                    day = 30;
                    break;
                case "10":
                    day = 31;
                    break;
                case "11":
                    day = 30;
                    break;
                case "12":
                    day = 31;
                    break;
                default:
                    break;
            }
            calStart.val(calStart.val().split("-")[0] + "-" + month + "-" + calStart.val().split("-")[2]);
            calEnd.val(calEnd.val().split("-")[0] + "-" + month + "-" + day);
        }
    });
    
    var $body = $("body");
    //apiMed.noteForm();
    //apiMed.deleteForm();
    //apiMed.check();
    apiMed.createExel();
    apiMed.createLawReport();
    //apiMed.clonePrice();
    //apiMed.deletePrice();
    apiMed.CalendarStart();

    $('.text-phone input, .text-email input, .datepicker-box input, .text-uid input, textarea').focusin(function() {
        $(this).siblings('label').addClass('active-label')
    });
    $('.text-phone input, .text-email input, .datepicker-box input, .text-uid input, textarea').focusout(function() {
        $(this).siblings('label').removeClass('active-label')
    });

    $body.on("click", ".headerBtnMenu", function() {
        var $hiddenMenu = $(".menuWrap__hiddenMenu");
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $hiddenMenu.stop().slideUp(300);
        } else {
            $(this).addClass("active");
            $hiddenMenu.stop().slideDown(300);
        }
        return false;
    });

// Открыть закрыть меню Филиалы
    $body.on("click", ".affiliates a", function() {

        if($(this).parents(".row.line").hasClass("active")){
            return false;
        }


        var self = $(this);
        $.ajax({
            url: self.attr('href'),
            type: "post",
            dataType: "json",
            data: {
                reportId: self.attr('data-report-id'),
                id: self.attr('data-id')
            },
            success: function(r) {
                if (r.company) {
                    $(".row.active .hiddenAffiliates__right__top__btnClose").click();
                    self.parents('.row').addClass('active').html(r.company);
                    callCalendar();
                    $(".hiddenAffiliates__right__top  select").chosen({disable_search_threshold: 10});
                    $(".line.active .affiliates").height($(".line.active .cell.legalName").outerHeight() - 20);

                    if(!$(".body-table").attr("data-status")){
                        $(".body-table").attr("data-status","active");
                        $(".row.active .hiddenRow").slideDown(300);
                    }else{
                        $(".body-table").removeAttr("data-status");
                    }
                    
                    $(".row.active .hiddenRow").slideDown(300);
                    setTimeout(function(){
                        if($(".hiddenAffiliates__left").height() > $(".hiddenAffiliates__right").height()){
                            $(".hiddenAffiliates__right").height($(".hiddenAffiliates__left").height());
                        }
                    },350);


                }
            },
            complete: function(){
                if($(".line.active").length){
                    $(".line.active .affiliates").height($(".line.active .cell.legalName").outerHeight() - 20);
                }
                calendarShow();
            }
        });
        return false;
    });
    $body.on("click", ".hiddenAffiliates__right__top__btnClose", function() {
        var self = $(this);
        $(".row.active .hiddenRow").slideUp(300, function(){
            $(".body-table").removeAttr("data-status");
            $.ajax({
                url: self.attr('href'),
                type: "post",
                dataType: "json",
                data: {
                    reportId: self.attr('data-report-id'),
                    id: self.attr('data-id')
                },
                success: function(r) {
                    if (r.company) {
                        self.parents('.row').removeClass('active').removeClass('status_color_3').removeClass('status_color_2').removeClass('status_color_1').removeClass('status_color_4').addClass('status_color_'+r.status).html(r.company);
                    }
                }
            });
        });
        return false;
    });

    $body.on("click", ".js-update-comment", function() {
        var self = $(this);
        $.ajax({
            url: self.attr('href'),
            type: "post",
            dataType: "json",
            data: {
                comment: self.parent().find('#comment').val()
            },
            success: function(r) {
            }
        });
        return false;
    });

// circle
    var circleSelect = function(selectId){
        $body.on("change", selectId, function(){
            var txt = $(".hiddenAffiliates__right__top .chosen-container-single .chosen-single span:first").text();
            switch (txt) {
                case "Отчет":
                    $(this).next().find(".circle").remove();
                    $(this).next().find(".chosen-single").append("<span class='circle gray'></span>");

                    break
                case "Согласование":
                    $(this).next().find(".circle").remove();
                    $(this).next().find(".chosen-single").append("<span class='circle yelow'></span>");

                    break
                case "Счет":
                    $(this).next().find(".circle").remove();
                    $(this).next().find(".chosen-single").append("<span class='circle red'></span>");

                    break
                case "Оплата":
                    $(this).next().find(".circle").remove();
                    $(this).next().find(".chosen-single").append("<span class='circle green'></span>");

                    break
                default:
                    return false;
            }
        })
    }



// Филиалы меню -> (Записи, Дошедшие)
    $body.on("click", ".hiddenAffiliates__left__options__cell__num a, .hiddenAffiliates__right__top__price a", function() {
        if($(this).hasClass('no-correct')) return false;
        var num = $(this).text();
        var tmp = '<div class="hiddenAffiliates__textWrapper"><input value="' + num + '" type="text" data-id="' + $(this).attr('data-id') + '" data-column="' + $(this).attr('data-column') + '"><label for=""></label></div>';
        $(this).siblings(".difference").hide();
        $(this).hide().parent().append(tmp);
        return false;
    });

//    apiMed.difference();
// label закрытие формы
    $body.on("click", ".hiddenAffiliates__left__options__cell__num label, .hiddenAffiliates__right__top__price label", function() {
        var self = $(this),
            href = $('.ajax_link').attr('data-href'),
            value,
            column = self.prev().attr('data-column'),
            id = self.prev().attr('data-id');
        if(parseInt(self.prev().val())) {
            value = self.prev().val();
        } else {
            value = 0;
        }
        $.ajax({
            url: href,
            type: "post",
            dataType: "json",
            data: {
                id: id,
                column: column,
                value: value
            },
            success: function(r) {
                if (r.success) {
                    self.parents('.row.active').find('.recording').html(r.modelLaw.patient_record);
                    self.parents('.row.active').find('.surviving').html('<span>'+r.modelLaw.patient_count+'</span>'+'<i class="difference"></i>');
                    self.parents('.row.active').find('.successing').html('<span>'+r.modelLaw.patient_success+'</span>'+'<i class="difference"></i>');
                    self.parents('.row.active').find('.generalSum').text(r.modelLaw.summ+' руб.');
                    self.parents('.row.active').find('.diagnostic').text(r.modelLaw.patient_diag);
                    self.parents('.row.active').find('.diagnosticSum').text(r.modelLaw.summ_diag+' руб.');
                    self.parents('.row.active').find('.analyz').text(r.modelLaw.patient_analyz);
                    self.parents('.row.active').find('.analyzSum').text(r.modelLaw.summ_analyz+' руб.');
                    self.parents('.row.active').find('.repeat').text(r.modelLaw.patient_repeat);
                    self.parents('.row.active').find('.insurance').text(r.modelLaw.patient_insurance);
                }

                apiMed.difference();
                $(".main_number .cell").each(function(){
                    if($(this).find(".difference").length){
                        $(this).find(".difference").text($(this).parents(".main_number").find(".recording").text() - $(this).find("span").text());

                    }
                });
                self.parents(".hiddenAffiliates__textWrapper").remove();
            }
        });
        return false;
    });

    apiMed.difference();
	//apiMed.graduationDifference();

    $body.on('keyup', '.sort input[type="text"]', function() {
        var href = createUrl();
        ajaxUpdate(href,$(this));
        return false;
    });

    $body.on('change', '#law_year, #law_month', function() {
        var href = createUrl();
        ajaxUpdate(href);
        return false;
    });

	$body.on('change', '.sort .js-graduation-select', function() {
        var href = createUrl();
        location.href = href;
        return false;
    });

    $body.on('change', '.sort .js-active-status', function() {
        var href = createUrl();
        ajaxUpdate(href);
        return false;
    });

    $body.on('click', '#show_result', function() {
        var href = createUrl();
        ajaxUpdate(href);
        return false;
    });

    $body.on('click', '#js-show-date', function() {
        var href = createUrl();
        location.href = href;
        return false;
    });

    $body.on('click','.js-save-status, .js-make-payment', function() {
        var date = $(this).parents('.hiddenAffiliates__right__top').find('.calendar_status').val().split(' ')[0],
            href = $(this).attr('data-href'),
            self = $(this),
            repStatus = $(this).parents('.hiddenAffiliates__right__top').find('#company_status').val(),
            id = $(this).attr('data-report-id'),
            summ =$(this).parents('.hiddenAffiliates__right__top').find('#status_sum').val();
        if ( ! parseInt(summ)) summ = 0;
        if( ! date) {
            alert('Укажите дату');
            return false;
        }
        if( ! id) {
            alert('Заявок нет');
            return false;
        }
        $.ajax({
            url: href,
            type: "post",
            dataType: "json",
            data: {
                date: date,
                repStatus: repStatus,
                id: id,
                summ: summ
            },
            success: function(r) {
                if(r.success){
                    //self.parents('.hiddenAffiliates__right').find('.hiddenAffiliates__right__top').find('.calendar_status').val('');
                    if(r.date){
                        self.parents('.hiddenAffiliates__right').html(r.date);
                    }
                }
               // $('.hiddenAffiliates__right').find(".hiddenAffiliates__right__top__calendarWrap").hide();
                //$('.hiddenAffiliates__right').find(".hiddenAffiliates__right__top__calendarWrap__selectDate").show();
            }
        });
    });

    /*delete status and time*/
    $body.on('click','.del_status, .del_payment', function(){
        var href= $(this).attr('data-href'),
            id = $(this).attr('data-id');
        if (confirm('Продолжить удаление?')) {
            $.ajax({
                url: href,
                type: "post",
                dataType: "json",
                data: {
                    id: id
                },
                success: function(r) {
                    if(r.success){
                        if(r.date){
                            $('.hiddenAffiliates__right').html(r.date);
                        }
                    }
                }
            });
        }
    });

    function createUrl() {
        var href = $('.table').attr('data-href');
        $('.sort :text, .js-active-sort, .js-active-page, .js-graduation-select, .js-active-status').each(function(i, e) {
            href += '&' + $(this).attr('name') + '=' + $(this).val();
        })
        if ($('#law_year').val() !== '0' && $('#law_month').val() !== '0') {
            if($('#js-query-date-start').val()&&$('#js-query-date-end').val()){
                href += '&queryDate=' + $('#js-query-date-start').val() + '&queryDateEnd=' + $('#js-query-date-end').val();
            }else{
                href += '&queryDate=' + $('#law_year').val() + '-' + $('#law_month').val();
            }

        }
        return href;
    }
    function ajaxUpdate(href, self) {
        $.ajax({
            url: href,
            type: "post",
            dataType: "json",
            success: function(r) {
                $('.body-table').html(r.tbody);
                $('.summary').html(r.count);
                if (r.params.ratingSum && r.params.ratingSum[1] >= 0) $('.hidden-form').find('.ratingSum1').html(r.params.ratingSum[1] + '<sup>' + r.params.ratingPaymentSum[1] + '</sup>');
                if (r.params.ratingSum && r.params.ratingSum[2] >= 0) $('.hidden-form').find('.ratingSum2').html(r.params.ratingSum[2] + '<sup>' + r.params.ratingPaymentSum[2] + '</sup>');
                if (r.params.ratingSum && r.params.ratingSum[3] >= 0) $('.hidden-form').find('.ratingSum3').html(r.params.ratingSum[3] + '<sup>' + r.params.ratingPaymentSum[3] + '</sup>');
                if (r.params.patient_record) $('.hidden-form').find('.recording').text(r.params.patient_record);
                if (r.params.patient_count) $('.hidden-form').find('.surviving').text(r.params.patient_count);
                if (r.params.patient_success) $('.hidden-form').find('.successing').html(r.params.patient_success + '<sup>' + r.params.percent.success + '%</sup>');
                if (r.params.summRed) {
                    if (r.params.percent && r.params.percent.average_bill) {
                        $('.hidden-form').find('.generalSum .red').html(r.params.summRed + ' руб.' + '<sup>' + r.params.percent.average_bill + '</sup>');
                    } else {
                        $('.hidden-form').find('.generalSum .red').text(r.params.summRed + ' руб.');
                    }
                } else {
	                $('.hidden-form').find('.generalSum .red').text('0 руб.');
                }
                if (r.params.summGreen) {
	                $('.hidden-form').find('.generalSum .green').text(r.params.summGreen + ' руб.');
                } else {
	                $('.hidden-form').find('.generalSum .green').text('0 руб.');
                }
                if (r.params.percent && r.params.percent.diag > 0) {
                    $('.hidden-form').find('.diagnostika').html(r.params.patient_diag + ' руб.' + '<sup>' + r.params.percent.diag + '%</sup>');
                } else {
                    $('.hidden-form').find('.diagnostika').html(r.params.patient_diag);
                }
                if (r.params.percent && r.params.percent.summ_diag > 0) {
                    $('.hidden-form').find('.diagnostikaSum').html(r.params.summ_diag + ' руб.' + '<sup>' + r.params.percent.summ_diag + '%</sup>');
                } else {
                    $('.hidden-form').find('.diagnostikaSum').html(r.params.summ_diag + ' руб.');
                }
                if (r.params.percent && r.params.percent.analyz > 0) {
                    $('.hidden-form').find('.analyz').html(r.params.patient_analyz + '<sup>' + r.params.percent.analyz + '%</sup>');
                } else {
                    $('.hidden-form').find('.analyz').html(r.params.patient_analyz);
                }
                if (r.params.patient_repeat) $('.hidden-form').find('.repeat').text(r.params.patient_repeat);
                if (r.params.percent && r.params.percent.summ_analyz > 0) {
                    $('.hidden-form').find('.analyzSum').html(r.params.summ_analyz  + ' руб.' + '<sup>' + r.params.percent.summ_analyz + '%</sup>');
                } else {
                    $('.hidden-form').find('.analyzSum').html(r.params.summ_analyz);
                }
                if (r.params.patient_insurance) $('.hidden-form').find('.insurance').text(r.params.patient_insurance);
                $('#pages').html(r.pages);
                $("select").chosen({disable_search_threshold: 10});
	            //apiMed.graduationDifference();
            }
        });
        return false;
    }

    function callCalendar() {
        $('.calendar_status').datepicker({
            lang: "ru",
            dateFormat: "dd-mm-yy"
        });
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Текущее время',
            closeText: 'Закрыть',
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);
    }



});
$(".sort .chosen-container-single .chosen-single span").text("Выберите");
/*! iCheck v1.0.2 by Damir Sultanov, http://git.io/arlzeA, MIT Licensed */
(function(f){function A(a,b,d){var c=a[0],g=/er/.test(d)?_indeterminate:/bl/.test(d)?n:k,e=d==_update?{checked:c[k],disabled:c[n],indeterminate:"true"==a.attr(_indeterminate)||"false"==a.attr(_determinate)}:c[g];if(/^(ch|di|in)/.test(d)&&!e)x(a,g);else if(/^(un|en|de)/.test(d)&&e)q(a,g);else if(d==_update)for(var f in e)e[f]?x(a,f,!0):q(a,f,!0);else if(!b||"toggle"==d){if(!b)a[_callback]("ifClicked");e?c[_type]!==r&&q(a,g):x(a,g)}}function x(a,b,d){var c=a[0],g=a.parent(),e=b==k,u=b==_indeterminate,
    v=b==n,s=u?_determinate:e?y:"enabled",F=l(a,s+t(c[_type])),B=l(a,b+t(c[_type]));if(!0!==c[b]){if(!d&&b==k&&c[_type]==r&&c.name){var w=a.closest("form"),p='input[name="'+c.name+'"]',p=w.length?w.find(p):f(p);p.each(function(){this!==c&&f(this).data(m)&&q(f(this),b)})}u?(c[b]=!0,c[k]&&q(a,k,"force")):(d||(c[b]=!0),e&&c[_indeterminate]&&q(a,_indeterminate,!1));D(a,e,b,d)}c[n]&&l(a,_cursor,!0)&&g.find("."+C).css(_cursor,"default");g[_add](B||l(a,b)||"");g.attr("role")&&!u&&g.attr("aria-"+(v?n:k),"true");
    g[_remove](F||l(a,s)||"")}function q(a,b,d){var c=a[0],g=a.parent(),e=b==k,f=b==_indeterminate,m=b==n,s=f?_determinate:e?y:"enabled",q=l(a,s+t(c[_type])),r=l(a,b+t(c[_type]));if(!1!==c[b]){if(f||!d||"force"==d)c[b]=!1;D(a,e,s,d)}!c[n]&&l(a,_cursor,!0)&&g.find("."+C).css(_cursor,"pointer");g[_remove](r||l(a,b)||"");g.attr("role")&&!f&&g.attr("aria-"+(m?n:k),"false");g[_add](q||l(a,s)||"")}function E(a,b){if(a.data(m)){a.parent().html(a.attr("style",a.data(m).s||""));if(b)a[_callback](b);a.off(".i").unwrap();
    f(_label+'[for="'+a[0].id+'"]').add(a.closest(_label)).off(".i")}}function l(a,b,f){if(a.data(m))return a.data(m).o[b+(f?"":"Class")]}function t(a){return a.charAt(0).toUpperCase()+a.slice(1)}function D(a,b,f,c){if(!c){if(b)a[_callback]("ifToggled");a[_callback]("ifChanged")[_callback]("if"+t(f))}}var m="iCheck",C=m+"-helper",r="radio",k="checked",y="un"+k,n="disabled";_determinate="determinate";_indeterminate="in"+_determinate;_update="update";_type="type";_click="click";_touch="touchbegin.i touchend.i";
    _add="addClass";_remove="removeClass";_callback="trigger";_label="label";_cursor="cursor";_mobile=/ipad|iphone|ipod|android|blackberry|windows phone|opera mini|silk/i.test(navigator.userAgent);f.fn[m]=function(a,b){var d='input[type="checkbox"], input[type="'+r+'"]',c=f(),g=function(a){a.each(function(){var a=f(this);c=a.is(d)?c.add(a):c.add(a.find(d))})};if(/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(a))return a=a.toLowerCase(),g(this),c.each(function(){var c=
        f(this);"destroy"==a?E(c,"ifDestroyed"):A(c,!0,a);f.isFunction(b)&&b()});if("object"!=typeof a&&a)return this;var e=f.extend({checkedClass:k,disabledClass:n,indeterminateClass:_indeterminate,labelHover:!0},a),l=e.handle,v=e.hoverClass||"hover",s=e.focusClass||"focus",t=e.activeClass||"active",B=!!e.labelHover,w=e.labelHoverClass||"hover",p=(""+e.increaseArea).replace("%","")|0;if("checkbox"==l||l==r)d='input[type="'+l+'"]';-50>p&&(p=-50);g(this);return c.each(function(){var a=f(this);E(a);var c=this,
        b=c.id,g=-p+"%",d=100+2*p+"%",d={position:"absolute",top:g,left:g,display:"block",width:d,height:d,margin:0,padding:0,background:"#fff",border:0,opacity:0},g=_mobile?{position:"absolute",visibility:"hidden"}:p?d:{position:"absolute",opacity:0},l="checkbox"==c[_type]?e.checkboxClass||"icheckbox":e.radioClass||"i"+r,z=f(_label+'[for="'+b+'"]').add(a.closest(_label)),u=!!e.aria,y=m+"-"+Math.random().toString(36).substr(2,6),h='<div class="'+l+'" '+(u?'role="'+c[_type]+'" ':"");u&&z.each(function(){h+=
        'aria-labelledby="';this.id?h+=this.id:(this.id=y,h+=y);h+='"'});h=a.wrap(h+"/>")[_callback]("ifCreated").parent().append(e.insert);d=f('<ins class="'+C+'"/>').css(d).appendTo(h);a.data(m,{o:e,s:a.attr("style")}).css(g);e.inheritClass&&h[_add](c.className||"");e.inheritID&&b&&h.attr("id",m+"-"+b);"static"==h.css("position")&&h.css("position","relative");A(a,!0,_update);if(z.length)z.on(_click+".i mouseover.i mouseout.i "+_touch,function(b){var d=b[_type],e=f(this);if(!c[n]){if(d==_click){if(f(b.target).is("a"))return;
        A(a,!1,!0)}else B&&(/ut|nd/.test(d)?(h[_remove](v),e[_remove](w)):(h[_add](v),e[_add](w)));if(_mobile)b.stopPropagation();else return!1}});a.on(_click+".i focus.i blur.i keyup.i keydown.i keypress.i",function(b){var d=b[_type];b=b.keyCode;if(d==_click)return!1;if("keydown"==d&&32==b)return c[_type]==r&&c[k]||(c[k]?q(a,k):x(a,k)),!1;if("keyup"==d&&c[_type]==r)!c[k]&&x(a,k);else if(/us|ur/.test(d))h["blur"==d?_remove:_add](s)});d.on(_click+" mousedown mouseup mouseover mouseout "+_touch,function(b){var d=
        b[_type],e=/wn|up/.test(d)?t:v;if(!c[n]){if(d==_click)A(a,!1,!0);else{if(/wn|er|in/.test(d))h[_add](e);else h[_remove](e+" "+t);if(z.length&&B&&e==v)z[/ut|nd/.test(d)?_remove:_add](w)}if(_mobile)b.stopPropagation();else return!1}})})}})(window.jQuery||window.Zepto);