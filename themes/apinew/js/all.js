var apiMed = (function(){
    var difference = function(){
        $("body").on("click", ".hiddenAffiliates__left__options__cell__num  label", function(){
            var that = $(this),
                values,
                total_num = that.parents(".hiddenAffiliates__left__options__row").find(".main_total").text();
            if(parseInt(that.prev().val())){
                values = that.prev().val();
            }else{
                values = 0;
            }
            that.parents(".hiddenAffiliates__left__options__cell__num").find("a").show().text(values);
            that.parents(".hiddenAffiliates__left__options__cell__num").find(".difference").show();

            that.parents(".ajax_link").find(".hiddenAffiliates__left__options__cell__num").each(function(){
                   $(this).find(".difference").text(total_num - $(this).find("a").text());
            });
            that.parents(".hiddenAffiliates__textWrapper").hide();
        });
    }
    var noteForm = function(){
        $("body").on("click", ".business-form-wrapper ._save_btn, ._create_clinic ._save_btn", function(){
           var check = true;
           $('.is_error').remove();
           $('.note-form').each(function(){
               if(!$(this).find(":text").val()){
                   $(this).append('<span class="is_error">заполните поле</span>');
                   check = false;
               }
           });
           if(!check){
               return false;
           }
       });
    }
    var noteFormPrice = function(){
        $("body").on("click", "._create-block ._save_btn", function(){
           var check = true,
               check2 = false;
           $('.is_error').remove();
           $('.note-form').each(function(){
               if(!$(this).find(":text").val()){
                   $(this).append('<span class="is_error">заполните поле</span>');
                   check = false;
               }
           });
           if($('.request_date').is(':visible')&&$('.request_date').val()){
                $.ajax({
                    url: $('.request_date').attr('data-href'),
                    type: 'POST',
                    data: {id:$('#create-block').attr('data-id'),date:$('.request_date').val()},
                    dataType: "json",
                    async:false,
                    success: function(r) {
                        if(r.success==1){
                            check2 = false;
                            alert('Заявка на этот месяц уже сущевствует');
                        }else{
                           check2 = true; 
                        }

                    }

                });
            }
           if(!check||!check2){
               return false;
           }
       });
    }
    var deleteForm = function (){
        $("body").on('click','.row_deleted',function(){
           if (!confirm('Продолжить удаление?')) {
             return false;
           }
          
        });
    }
    var createExel = function(){
     $('body').on('click','.import_exel',function(){
        var fields = $('.exel_form').serializeArray();
        var condition = $('.hidden-form').serializeArray();
        $.ajax({
             url: $(this).attr('data-href'),
             type: 'POST',
             data: {fields:fields,condition:condition},
             dataType: "json",
             success: function(r) {
                if(r.href){
                   $('.exel_form').append(r.href);
                }
             }
         });
       return false;
       });
       $('body').on('click','.import_exel_single',function(){
           $('.exel_download').remove();
           var self = $(this),
               date =$('.hidden-form input[name="create_time"]').val(),
               dateEnd =$('.hidden-form input[name="create_time_end"]').val();
        $.ajax({
             url: $(this).attr('href'),
             type: 'POST',
             data: {date:date, dateEnd:dateEnd},
             dataType: "json",
             success: function(r) {
                if(r.href){
                   self.parents('.row').find('.download_file_text').html(r.href);
                   self.parents('.hiddenAffiliates__left__options__cellName').find('.download_file_text2').html(r.href);
                }
             }
         });
       return false;
       });
       $('body').on('click','.exel_download',function(){
           $(this).remove();
       });
       
    }
    
    var check = function(){
        $(".exel_form input").iCheck({
            checkboxClass: 'icheckbox_polaris',
            radioClass: 'iradio_polaris',
            increaseArea: '-10%'
        });
    }
    
    var clonePrice = function(){
     $('body').on('click','.js-clone-price',function(){
        var self = $(this);
        $.ajax({
             url: self.attr('href'),
             type: 'POST',
             data: {data:1},
             dataType: "json",
             success: function(r) {
                if(r.column){
                   self.parents('.column_block_price').append(r.column);
                }
             }
         });
       return false;
       });
    }
    
    var deletePrice = function(){
     $('body').on('click','.js-delete-price',function(){
         if (confirm('Продолжить удаление?')) {
            var self = $(this);
            $.ajax({
                 url: self.attr('href'),
                 type: 'POST',
                 data: {data:1},
                 dataType: "json",
                 success: function(r) {
                    if(r.success){
                       $('.column_rows_'+self.attr('data-id')).remove();
                    }
                 }
             });
        }
       return false;
       });
    }
    
    var CalendarStart = function(){
        $('#js-query-date-start, #js-query-date-end').datepicker({
            lang: "ru",
            dateFormat: "yy-mm-dd"
        });
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Текущее время',
            closeText: 'Закрыть',
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);
    
    }
    
    return {
        difference: difference,
        noteForm: noteForm,
        deleteForm:deleteForm,
        check:check,
        createExel:createExel,
        noteFormPrice:noteFormPrice,
        clonePrice:clonePrice,
        deletePrice:deletePrice,
        CalendarStart:CalendarStart
    }
}());

var calendarShow = function(){
    $("body").on("click", ".hiddenAffiliates__right__top__calendarWrap__selectDate", function(){
        $(this).hide();
        $(".hiddenAffiliates__right__top__calendarWrap").show();
        $(".calendar_status").focus();
    });
};
$(function() {
    var $body = $("body");
    apiMed.noteForm();
    apiMed.deleteForm();
    apiMed.check();
    apiMed.createExel();
    apiMed.noteFormPrice();
    apiMed.clonePrice();
    apiMed.deletePrice();
    apiMed.CalendarStart();
    
    $('.text-phone input, .text-email input, .datepicker-box input, .text-uid input, textarea').focusin(function() {
        $(this).siblings('label').addClass('active-label')
    });
    $('.text-phone input, .text-email input, .datepicker-box input, .text-uid input, textarea').focusout(function() {
        $(this).siblings('label').removeClass('active-label')
    });
    
    $body.on("click", ".headerBtnMenu", function() {
        var $hiddenMenu = $(".menuWrap__hiddenMenu");
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $hiddenMenu.stop().slideUp(300);
        } else {
            $(this).addClass("active");
            $hiddenMenu.stop().slideDown(300);
        }
        return false;
    });

// Открыть закрыть меню Филиалы
    $body.on("click", ".affiliates a", function() {
        
        if($(this).parents(".row.line").hasClass("active")){
            return false;
        }
        
        
        var self = $(this);
        $.ajax({
            url: self.attr('href'),
            type: "post",
            dataType: "json",
            data: {id: self.attr('data-id'), date: self.attr('data-date'), dateEnd: self.attr('data-dateEnd'), period: self.attr('data-period'), status: self.attr('data-status')},
            success: function(r) {
                if (r.company) {
                    $(".row.active .hiddenAffiliates__right__top__btnClose").click();
                    self.parents('.row').addClass('active').html(r.company);
                    callCalendar();
                    $(".hiddenAffiliates__right__top  select").chosen({disable_search_threshold: 10});
                   // circleSelect("#company_status");
                    $(".line.active .affiliates").height($(".line.active .cell.legalName").outerHeight() - 20);
//                    switch ($(".hiddenAffiliates__right__top .chosen-container-single .chosen-single span:first").text()) {
//                      case "Отчет":
//                        $("#company_status").next().find(".circle").remove();
//                        $("#company_status").next().find(".chosen-single").append("<span class='circle gray'></span>");
//                        break
//                      case "Согласование":
//                        $("#company_status").next().find(".circle").remove();
//                        $("#company_status").next().find(".chosen-single").append("<span class='circle yelow'></span>");
//                        break
//                      case "Счет":
//                        $("#company_status").next().find(".circle").remove();
//                        $("#company_status").next().find(".chosen-single").append("<span class='circle red'></span>");
//                        break
//                      case "Оплата":
//                        $("#company_status").next().find(".circle").remove();
//                        $("#company_status").next().find(".chosen-single").append("<span class='circle green'></span>");
//                        break
//                      default:
//                        return false;
//                    }
          
                    if(!$(".body-table").attr("data-status")){
                        $(".body-table").attr("data-status","active");
                        $(".row.active .hiddenRow").slideDown(300);
                    }else{
                         $(".body-table").removeAttr("data-status");
                    }
                   
                    $(".row.active .hiddenRow").slideDown(300);
                    
                    
                }
            },
            complete: function(){
                if($(".line.active").length){
                    $(".line.active .affiliates").height($(".line.active .cell.legalName").outerHeight() - 20);
                }
                calendarShow();
            }
        });
        return false;
    });
    $body.on("click", ".hiddenAffiliates__right__top__btnClose", function() {
        var self = $(this);
        $(".row.active .hiddenRow").slideUp(300, function(){
            $(".body-table").removeAttr("data-status");
            $.ajax({
                url: self.attr('href'),
                type: "post",
                dataType: "json",
                data: {id: self.attr('data-id'), date: self.attr('data-date'), dateEnd: self.attr('data-dateEnd'), period: self.attr('data-period'), status: self.attr('data-status')},
                success: function(r) {
                    if (r.company) {
                        self.parents('.row').removeClass('active').removeClass('status_color_3').removeClass('status_color_2').removeClass('status_color_1').removeClass('status_color_4').addClass('status_color_'+r.status).html(r.company);
                    }
                }
            });
        });
        return false;
    });
// circle
    var circleSelect = function(selectId){
        $body.on("change", selectId, function(){
            var txt = $(".hiddenAffiliates__right__top .chosen-container-single .chosen-single span:first").text();
            switch (txt) {
              case "Отчет":
                $(this).next().find(".circle").remove();
                $(this).next().find(".chosen-single").append("<span class='circle gray'></span>");

                break
              case "Согласование":
                $(this).next().find(".circle").remove();
                $(this).next().find(".chosen-single").append("<span class='circle yelow'></span>");

                break
              case "Счет":
                $(this).next().find(".circle").remove();
                $(this).next().find(".chosen-single").append("<span class='circle red'></span>");

                break
              case "Оплата":
                $(this).next().find(".circle").remove();
                $(this).next().find(".chosen-single").append("<span class='circle green'></span>");

                break
              default:
                return false;
            }
        })
    }



// Филиалы меню -> (Записи, Дошедшие)
    $body.on("click", ".hiddenAffiliates__left__options__cell__num a, .hiddenAffiliates__right__top__price a", function() {
        if($(this).hasClass('no-correct')) return false;
        var num = $(this).text();
        var tmp = '<div class="hiddenAffiliates__textWrapper"><input value="' + num + '" type="text" data-id="' + $(this).attr('data-id') + '" data-column="' + $(this).attr('data-column') + '"><label for=""></label></div>';
        $(this).siblings(".difference").hide();
        $(this).hide().parent().append(tmp);
        return false;
    });

//    apiMed.difference();
// label закрытие формы
    $body.on("click", ".hiddenAffiliates__left__options__cell__num label, .hiddenAffiliates__right__top__price label", function() {
        var self = $(this),
            href = $('.ajax_link').attr('data-href'),
            value,
            column = self.prev().attr('data-column'),
            id = self.prev().attr('data-id'),
            date = $('.ajax_link').attr('data-date'),
            dateEnd = $('.ajax_link').attr('data-dateEnd');
        if(parseInt(self.prev().val())){
            value = self.prev().val();
        }
        else{
            value = 0;
        }
        $.ajax({
            url: href,
            type: "post",
            dataType: "json",
            data: {id: id, column: column, value: value,date:date,dateEnd:dateEnd},
            success: function(r) {
                if (r.success) {
                    if(r.koll_come){
                        self.parents('.row.active').find('.surviving').html('<span>'+r.koll_come+'</span>'+'<i class="difference"></i>');
                        self.parents('.table.admin_0').find('.row.sort').find('.surviving').html('<span class="js-update-koll_come">'+r.countClinicAll.koll_come+'<span><a class="sortBtn" href="#"></a></span></span>');
                    }
                    if(r.koll_record){
                        self.parents('.row.active').find('.recording').html(r.koll_record);
                        self.parents('.table.admin_0').find('.row.sort').find('.recording').html('<span class="js-update-koll_record">'+r.countClinicAll.koll_record+'<span><a class="sortBtn" href="#"></a></span></span>');
                    }
                    if(r.koll_success){
                        self.parents('.row.active').find('.successing').html('<span>'+r.koll_success+'</span>'+'<i class="difference"></i>');
                        self.parents('.table.admin_0').find('.row.sort').find('.successing').html(r.countClinicAll.koll_success);
                    }
                    if(r.price){
                        self.parents('.row.active').find('.price').text(r.price+' руб.');
                        self.parents('.row.active').find('.generalSum span').text(r.price);
                        self.parents('.table.admin_0').find('.row.sort').find('.generalSum').find('.green').html(r.countClinicAll.price.generalSumGreen);
                        self.parents('.table.admin_0').find('.row.sort').find('.generalSum').find('.red').html(r.countClinicAll.price.generalSumRed);
                        self.parents('.table.admin_0').find('.row.sort').find('.repeat').html(r.countClinicAll.diagnostik);
                        self.parents('.table.admin_0').find('.row.sort').find('.diagnostika').html(r.countClinicAll.d_price);
                        self.parents('.table.admin_0').find('.row.sort').find('.date').html(r.countClinicAll.repeat_koll);
                    }
                }
                                        
                  apiMed.difference();
                    $(".main_number .cell").each(function(){
                        if($(this).find(".difference").length){
                            $(this).find(".difference").text($(this).parents(".main_number").find(".recording").text() - $(this).find("span").text());
                            
                        }
                    });
                   self.parents(".hiddenAffiliates__textWrapper").remove();
            }
        });
        
        return false;
    });
      apiMed.difference();

    $body.on('keyup', '.sort input[type="text"]', function() {
        var href = createUrl();
        ajaxUpdate(href,$(this));
        return false;
    });

    $body.on('change', '.sort select, #law_year, #law_month', function() {
        var href = createUrl();
        ajaxUpdate(href);
        return false;
    });

    $body.on('click', '#show_result', function() {
        var href = createUrl();
        ajaxUpdate(href);
        return false;
    });

    $body.on('click', '#js-show-date', function() {
        var href = createUrl();
        location.href = href;
        return false;
    });
    
    $body.on('click','.save_status', function(){
         var date = $(this).parents('.hiddenAffiliates__right__top').find('.calendar_status').val().split(' ')[0],
             current_date = $(this).parents('.hiddenAffiliates').find('.ajax_link').attr('data-date'),
             href= $(this).attr('data-href'),
             self=$(this),
             status = $(this).parents('.hiddenAffiliates__right__top').find('#company_status').val(),
             mass =[];
             $('.for_calendar_status').each(function(){
                 mass.push($(this).attr('data-id'));
             });
         if(!date){
             alert('Укажите дату');
             return false;
         }
         if(!mass){
             alert('Заявок нет');
             return false;
         }
         $.ajax({
            url: href,
            type: "post",
            dataType: "json",
            data: {date: date, status: status,mass:mass,current_date:current_date},
            success: function(r) {
               if(r.success){
                   self.parents('.hiddenAffiliates__right__top').find('.calendar_status').val('');
                   if(r.date){
                       $('.hiddenAffiliates__right__bottom').html(r.date);
                   }
               }
                $(".hiddenAffiliates__right__top__calendarWrap").hide();
                $(".hiddenAffiliates__right__top__calendarWrap__selectDate").show();
            }
        });
    });
    
    /*delete status and time*/
    $body.on('click','.del_status', function(){
         var href= $(this).attr('data-href'),
            current_date = $(this).parents('.hiddenAffiliates').find('.ajax_link').attr('data-date'),
             status = $(this).attr('data-status'),
             mass =[];
             $('.for_calendar_status').each(function(){
                 mass.push($(this).attr('data-id'));
             });
         if (confirm('Продолжить удаление?')) {
            $.ajax({
               url: href,
               type: "post",
               dataType: "json",
               data: {status: status,mass:mass,current_date:current_date},
               success: function(r) {
                  if(r.success){
                      if(r.date){
                          $('.hiddenAffiliates__right__bottom').html(r.date);
                      }
                  }
               }
           });
       }
    });

    function createUrl() {
        var href = $('.table').attr('data-href');
        $('.sort :text,select').each(function(i, e) {
            href += '&' + $(this).attr('name') + '=' + $(this).val();
        })
        if ($('#law_year').val() !== '0' && $('#law_month').val() !== '0') {
            if($('#js-query-date-start').val()&&$('#js-query-date-end').val()){
                 href += '&queryDate=' + $('#js-query-date-start').val() + '&queryDateEnd=' + $('#js-query-date-end').val();
            }else{
                 href += '&queryDate=' + $('#law_year').val() + '-' + $('#law_month').val();
            }
            
        }
        return href;
    }
    function ajaxUpdate(href, self) {
        $.ajax({
            url: href,
            type: "post",
            dataType: "json",
            success: function(r) {
                $('.body-table').html(r.tbody);
                $('#summary').html(r.count);
                //$('.filter-table').html(r.filter);
                if(r.queryDate){
                    $('input[name="create_time"]').val(r.queryDate);
                }
                if(r.status_all){
                    $('.count_status_all').text(r.status_all);
                }
                if(r.status_4){
                    $('.count_status_4').text(r.status_4);
                }
                if(self){
                    $('#'+self.attr('id')).val($('#'+self.attr('id')).val()).focus();
                }
                if(r.koll_come){
                    $('.js-update-koll_come').text(r.koll_come);
                }
                if(r.koll_record){
                    $('.js-update-koll_record').text(r.koll_record);
                }
                if(r.koll_success){
                    $('.hidden-form').find('.successing').text(r.koll_success);
                }
                if(r.koll_repeat){
                    $('.hidden-form').find('.date').text(r.koll_repeat);
                }
                if(r.koll_diagnostik){
                    $('.hidden-form').find('.repeat').text(r.koll_diagnostik);
                }
                if(r.koll_d_price){
                    $('.hidden-form').find('.diagnostika').text(r.koll_d_price);
                    $('.hidden-form').parent().find('.row.caption').find('.diagnostika').html(r.koll_d_price+' <sup>руб.</sup>');
                }
                if(r.generalSum){
                    $('.hidden-form').parent().find('.row.caption').find('.generalSum').html(r.generalSum+' <sup>руб.</sup>');
                }
                if(r.generalSumGreen){
                    $('.hidden-form').find('.generalSum').find('.green').text(r.generalSumGreen);
                }
                if(r.generalSumRed){
                    $('.hidden-form').find('.generalSum').find('.red').text(r.generalSumRed);
                }
                $('#pages').html(r.pages);
                $("select").chosen({disable_search_threshold: 10});
                switch ($(".sort .chosen-container-single .chosen-single span:first").text()) {
                  case "Отчет":
                    $("#IntegratorClinicAll_status").next().find(".circle").remove();
                    $("#IntegratorClinicAll_status").next().find(".chosen-single").append("<span class='circle gray'></span>");
                    break
                  case "Согласование":
                    $("#IntegratorClinicAll_status").next().find(".circle").remove();
                    $("#IntegratorClinicAll_status").next().find(".chosen-single").append("<span class='circle yelow'></span>");
                    break
                  case "Счет":
                    $("#IntegratorClinicAll_status").next().find(".circle").remove();
                    $("#IntegratorClinicAll_status").next().find(".chosen-single").append("<span class='circle red'></span>");
                    break
                  case "Оплата":
                    $("#IntegratorClinicAll_status").next().find(".circle").remove();
                    $("#IntegratorClinicAll_status").next().find(".chosen-single").append("<span class='circle green'></span>");
                    break
                  default:
                    return false;
                }
            }
        });
        return false;
    }

    function callCalendar() {
        $('.calendar_status').datepicker({
            lang: "ru",
            dateFormat: "dd-mm-yy"
        });
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Текущее время',
            closeText: 'Закрыть',
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);
    }
    


});
$(".sort .chosen-container-single .chosen-single span").text("Выберите");
/*! iCheck v1.0.2 by Damir Sultanov, http://git.io/arlzeA, MIT Licensed */
(function(f){function A(a,b,d){var c=a[0],g=/er/.test(d)?_indeterminate:/bl/.test(d)?n:k,e=d==_update?{checked:c[k],disabled:c[n],indeterminate:"true"==a.attr(_indeterminate)||"false"==a.attr(_determinate)}:c[g];if(/^(ch|di|in)/.test(d)&&!e)x(a,g);else if(/^(un|en|de)/.test(d)&&e)q(a,g);else if(d==_update)for(var f in e)e[f]?x(a,f,!0):q(a,f,!0);else if(!b||"toggle"==d){if(!b)a[_callback]("ifClicked");e?c[_type]!==r&&q(a,g):x(a,g)}}function x(a,b,d){var c=a[0],g=a.parent(),e=b==k,u=b==_indeterminate,
    v=b==n,s=u?_determinate:e?y:"enabled",F=l(a,s+t(c[_type])),B=l(a,b+t(c[_type]));if(!0!==c[b]){if(!d&&b==k&&c[_type]==r&&c.name){var w=a.closest("form"),p='input[name="'+c.name+'"]',p=w.length?w.find(p):f(p);p.each(function(){this!==c&&f(this).data(m)&&q(f(this),b)})}u?(c[b]=!0,c[k]&&q(a,k,"force")):(d||(c[b]=!0),e&&c[_indeterminate]&&q(a,_indeterminate,!1));D(a,e,b,d)}c[n]&&l(a,_cursor,!0)&&g.find("."+C).css(_cursor,"default");g[_add](B||l(a,b)||"");g.attr("role")&&!u&&g.attr("aria-"+(v?n:k),"true");
    g[_remove](F||l(a,s)||"")}function q(a,b,d){var c=a[0],g=a.parent(),e=b==k,f=b==_indeterminate,m=b==n,s=f?_determinate:e?y:"enabled",q=l(a,s+t(c[_type])),r=l(a,b+t(c[_type]));if(!1!==c[b]){if(f||!d||"force"==d)c[b]=!1;D(a,e,s,d)}!c[n]&&l(a,_cursor,!0)&&g.find("."+C).css(_cursor,"pointer");g[_remove](r||l(a,b)||"");g.attr("role")&&!f&&g.attr("aria-"+(m?n:k),"false");g[_add](q||l(a,s)||"")}function E(a,b){if(a.data(m)){a.parent().html(a.attr("style",a.data(m).s||""));if(b)a[_callback](b);a.off(".i").unwrap();
    f(_label+'[for="'+a[0].id+'"]').add(a.closest(_label)).off(".i")}}function l(a,b,f){if(a.data(m))return a.data(m).o[b+(f?"":"Class")]}function t(a){return a.charAt(0).toUpperCase()+a.slice(1)}function D(a,b,f,c){if(!c){if(b)a[_callback]("ifToggled");a[_callback]("ifChanged")[_callback]("if"+t(f))}}var m="iCheck",C=m+"-helper",r="radio",k="checked",y="un"+k,n="disabled";_determinate="determinate";_indeterminate="in"+_determinate;_update="update";_type="type";_click="click";_touch="touchbegin.i touchend.i";
    _add="addClass";_remove="removeClass";_callback="trigger";_label="label";_cursor="cursor";_mobile=/ipad|iphone|ipod|android|blackberry|windows phone|opera mini|silk/i.test(navigator.userAgent);f.fn[m]=function(a,b){var d='input[type="checkbox"], input[type="'+r+'"]',c=f(),g=function(a){a.each(function(){var a=f(this);c=a.is(d)?c.add(a):c.add(a.find(d))})};if(/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(a))return a=a.toLowerCase(),g(this),c.each(function(){var c=
        f(this);"destroy"==a?E(c,"ifDestroyed"):A(c,!0,a);f.isFunction(b)&&b()});if("object"!=typeof a&&a)return this;var e=f.extend({checkedClass:k,disabledClass:n,indeterminateClass:_indeterminate,labelHover:!0},a),l=e.handle,v=e.hoverClass||"hover",s=e.focusClass||"focus",t=e.activeClass||"active",B=!!e.labelHover,w=e.labelHoverClass||"hover",p=(""+e.increaseArea).replace("%","")|0;if("checkbox"==l||l==r)d='input[type="'+l+'"]';-50>p&&(p=-50);g(this);return c.each(function(){var a=f(this);E(a);var c=this,
        b=c.id,g=-p+"%",d=100+2*p+"%",d={position:"absolute",top:g,left:g,display:"block",width:d,height:d,margin:0,padding:0,background:"#fff",border:0,opacity:0},g=_mobile?{position:"absolute",visibility:"hidden"}:p?d:{position:"absolute",opacity:0},l="checkbox"==c[_type]?e.checkboxClass||"icheckbox":e.radioClass||"i"+r,z=f(_label+'[for="'+b+'"]').add(a.closest(_label)),u=!!e.aria,y=m+"-"+Math.random().toString(36).substr(2,6),h='<div class="'+l+'" '+(u?'role="'+c[_type]+'" ':"");u&&z.each(function(){h+=
        'aria-labelledby="';this.id?h+=this.id:(this.id=y,h+=y);h+='"'});h=a.wrap(h+"/>")[_callback]("ifCreated").parent().append(e.insert);d=f('<ins class="'+C+'"/>').css(d).appendTo(h);a.data(m,{o:e,s:a.attr("style")}).css(g);e.inheritClass&&h[_add](c.className||"");e.inheritID&&b&&h.attr("id",m+"-"+b);"static"==h.css("position")&&h.css("position","relative");A(a,!0,_update);if(z.length)z.on(_click+".i mouseover.i mouseout.i "+_touch,function(b){var d=b[_type],e=f(this);if(!c[n]){if(d==_click){if(f(b.target).is("a"))return;
        A(a,!1,!0)}else B&&(/ut|nd/.test(d)?(h[_remove](v),e[_remove](w)):(h[_add](v),e[_add](w)));if(_mobile)b.stopPropagation();else return!1}});a.on(_click+".i focus.i blur.i keyup.i keydown.i keypress.i",function(b){var d=b[_type];b=b.keyCode;if(d==_click)return!1;if("keydown"==d&&32==b)return c[_type]==r&&c[k]||(c[k]?q(a,k):x(a,k)),!1;if("keyup"==d&&c[_type]==r)!c[k]&&x(a,k);else if(/us|ur/.test(d))h["blur"==d?_remove:_add](s)});d.on(_click+" mousedown mouseup mouseover mouseout "+_touch,function(b){var d=
        b[_type],e=/wn|up/.test(d)?t:v;if(!c[n]){if(d==_click)A(a,!1,!0);else{if(/wn|er|in/.test(d))h[_add](e);else h[_remove](e+" "+t);if(z.length&&B&&e==v)z[/ut|nd/.test(d)?_remove:_add](w)}if(_mobile)b.stopPropagation();else return!1}})})}})(window.jQuery||window.Zepto);