$(function(){
    $('html').keydown(function(eventObject){
      if (event.keyCode == 27) { 
        if($("#popup_mod").css('display') === "block"){
            $("#popup_close").click();
        }
      }
    });
    var height_form_modul = $(".js_form_modul").offset().top;
    $(window).scroll(function(){
        if($(window).scrollTop() >  height_form_modul){
            $(".js_form_modul").addClass("scrollFixed");
        }
        else{
            $(".js_form_modul").removeClass("scrollFixed");
        }   
    });
    var noCheckStyle = function(){
        if($("#allStylebtn").is(":checked")){
            $("#allStylebtn").click();
        }
    };
	var settingMod = {
		mb_fw: "normal",
		mb_fz: "16px",
		mb_td: "none",
		mb_fs: "normal",
		mb_bg: "#00418B",
		mb_col: "#fff",
		mb_dragForm: "true",
		mb_line_txt: "Записаться на прием. Онлайн запись к врачам, в клиники",
		tmp: function(){ 
			return 	'<div id="moduleLineWrapper"></div>|'+
					'<script> | @'+
				        'var id_client = '+mbId+',|'+
				            'mb_line_num = '+mbPhone+',|'+
				            'mb_fw = "'+this.mb_fw+'",|'+
				            'mb_fz = "'+this.mb_fz+'",|'+
				            'mb_td = "'+this.mb_td+'",|'+
				            'mb_fs = "'+this.mb_fs+'",|'+
				            'mb_bg = "'+this.mb_bg+'",|'+
				            'mb_dragForm = "'+this.mb_dragForm+'",|'+
				            'mb_col = "'+this.mb_col+'",|'+
				            'mb_line_txt = "'+this.mb_line_txt+'",|'+
				            'scriptJs = document.createElement("script");|'+
				        'scriptJs.src = "http://medbooking.com/scripts/integrator/moduleLine/script.js";|'+
				        'document.head.appendChild(scriptJs);| !'+
			        '</script>| '
		}
	};
    
	$("body").on("click", "#btn_code", function(){
    $("#copy").removeClass("btn_copy").val("Скопировать");
		$("#code_msg").text(settingMod.tmp());
		for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
		}
		for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
		}
		for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
		}
		$('#popup_mod').show();
		$('body').append('<div class="overlay"></div>');
	});
    $("body").on("click", "#popup_close, .overlay", function(){
        $('#popup_mod').hide();
        $(".overlay").remove();
    });
   $('#picker_back').colpick({
        layout:'hex',
        submit:0,
        colorScheme:'dark',
        onChange:function(hsb,hex,rgb,el,bySetColor) {
            $(el).css('border-color','#'+hex);
            if(!bySetColor) $(el).val(hex);
            
            $("#moduleLineWrapper").css({"background": '#'+hex});
            settingMod.mb_bg = '#'+hex;
                noCheckStyle();
        }
    });
    $("#picker_back").css({"border-color":"#00418B"});
    $("#picker_back").val("00418B");
    $('#picker_text').colpick({
        layout:'hex',
        submit:0,
        colorScheme:'dark',
        onChange:function(hsb,hex,rgb,el,bySetColor) {
            $(el).css('border-color','#'+hex);
            if(!bySetColor) $(el).val(hex);
            $(".mb_line_txt, .mb_line_num").css({"color": '#'+hex});
                settingMod.mb_col = '#'+hex;
                noCheckStyle();
        }
    });
    $("#picker_text").css({"border-color":"#ffffff"});
    $("#picker_text").val("ffffff");
    $("body").on("change", ".js-font_size", function(){
        noCheckStyle();
        $(".mb_line_txt, .mb_line_num").css({"font-size": $(this).val() + "px"});
        settingMod.mb_fz = $(this).val() + "px";
        
    });
    $("body").on("click", ".js-bold", function(){
        noCheckStyle();
       if(!$(this).hasClass("active")){
           $(this).addClass("active");
           $(".mb_line_txt, .mb_line_num").css({"font-weight": "bold"});
           settingMod.mb_fw = "bold";
           
       }else{
           $(this).removeClass("active");
           $(".mb_line_txt, .mb_line_num").css({"font-weight": "normal"});
           settingMod.mb_fw = "normal";
       }
    });
        $("body").on("click", ".js-italic", function(){
            noCheckStyle();
       if(!$(this).hasClass("active")){
           $(this).addClass("active");
           $(".mb_line_txt, .mb_line_num").css({"font-style": "italic"});
           settingMod.mb_fs = "italic";
       }else{
           $(this).removeClass("active");
           $(".mb_line_txt, .mb_line_num").css({"font-style": "normal"});
           settingMod.mb_fs = "normal";
       }
    });
        $("body").on("click", ".js-underline", function(){
            noCheckStyle();
       if(!$(this).hasClass("active")){
           $(this).addClass("active");
           $(".mb_line_txt, .mb_line_num").css({"text-decoration": "underline"});
           settingMod.mb_td = "underline";
       }else{
           $(this).removeClass("active");
           $(".mb_line_txt, .mb_line_num").css({"text-decoration": "none"});
           settingMod.mb_td = "none";
       }
    });
    $("body").on("click", "#form", function(){
       if($(this).is(":checked")){
           settingMod.mb_dragForm = "true";
           $("#mb_minimized").show();

       }else{
           settingMod.mb_dragForm = "false";
           $("#mb_minimized").hide();
       }
    });
        $("body").on("click", "#allStylebtn", function(){
            if($(this).is(":checked")){
                settingMod.mb_bg = "#00418B";
                settingMod.mb_col = "#ffffff";
                settingMod.mb_fz = "16px";
                settingMod.mb_fw = "normal";
                settingMod.mb_fs = "normal";
                settingMod.mb_td = "none";
                $("#picker_back").css({"border-color":"#00418B"});
                $("#picker_back").val("00418B");            
                $("#picker_text").css({"border-color":"#ffffff"});
                $("#picker_text").val("fffff");

                $("#moduleLineWrapper").css({"background":settingMod.mb_bg});

                $(".mb_line_txt, .mb_line_num").css({"color":settingMod.mb_col, "font-size": settingMod.mb_fz, "font-weight": settingMod.mb_fw, "font-style": settingMod.mb_fs, "text-decoration": settingMod.mb_td});
                $(".js-bold, .js-italic, .js-underline").removeClass("active");

                $(".mb_line_txt").text("Записаться на прием. Онлайн запись к врачам, в клиники");
                settingMod.mb_line_txt = "Записаться на прием. Онлайн запись к врачам, в клиники";
                $("#name_btn").val("");
                $(".js-font_size option[value='16']").prop('selected', true);
            }
    });
    $("body").on("keyup", "#name_btn", function(){
        noCheckStyle();
        settingMod.mb_line_txt = $(this).val();
        $(".mb_line_txt").text($(this).val());
    });
     var client = new ZeroClipboard( $("#copy") );
      client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        } );
        client.on( 'aftercopy', function(event) {
            //
        } );
      } );
      client.on( 'error', function(event) {
         console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
      } );
});