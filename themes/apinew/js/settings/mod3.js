$(function(){
    $('html').keydown(function(eventObject){
      if (event.keyCode == 27) { 
        if($("#popup_mod").css('display') === "block"){
            $("#popup_close").click();
        }
      }
    });
    $("#selectSpecial").chosen({no_results_text: "Нет такой специальности"}); 
    var height_form_modul = $(".js_form_modul").offset().top;
    $(window).scroll(function(){
        if($(window).scrollTop() >  height_form_modul){
            $(".js_form_modul").addClass("scrollFixed");
        }
        else{
            $(".js_form_modul").removeClass("scrollFixed");
        }   
    });
	var settingMod = {
    dragForm: "true",
    aside: "true",
    contentWidth: sessionStorage.getItem("mod3_contentWidth"),
    mb_category: (sessionStorage.getItem("mod3_category") || ""),
    mb_special:  (sessionStorage.getItem("mod3_special") || ""),
    mb_scriptUrl: "/",
		tmp: function(){
			return '<div id="medbooking-doctor"></div> | '+
			        '<script> | @'+
				        'var id_client = '+mbId+',|'+
				            'num = '+mbPhone+',|'+
				            'dragForm = '+this.dragForm+',|'+
                            'contentWidth = '+this.contentWidth+',|'+
				            'aside = '+this.aside+',|'+
				            'mb_category = "'+this.mb_category+'",|'+
                            'mb_special = "'+this.mb_special+'",|'+
				            'mb_scriptUrl = "'+this.mb_scriptUrl+'",|'+
				            'url = "http://medbooking.com/'+this.mb_category+'/'+this.mb_special+'?ajax=ref",|'+
				            'scriptJs = document.createElement("script");|'+
				        'scriptJs.id = "initScript";|'+
				        'scriptJs.src = "http://medbooking.com/scripts/integrator/part3/part3.js";|'+
				        'document.head.appendChild(scriptJs);| !'+
			        '</script>| '
		}
	};

    setTimeout(function(){
        $("#medbooking-doctor").addClass("mb_contentWidth_" + settingMod.contentWidth);
        $(".medbooking__doctor_list, .medbooking__clinic_list, .medbooking__tems, #medbooking__blog_service_list, #medbooking__blog_service_single, #doctor_card, .doctor_single .top_left_inner_doctor_single, .top_inner_clinik_single, .top_inner_clinik_single, .bottom_inner_clinik_single")
            .css({"width": settingMod.contentWidth + "px"});

        }, 1500);
    // Сохранение параметров на переходах по страницам
    if(sessionStorage.getItem("mod3_category") === "doctors"){
        $("input[id=search_clinic]").prop("checked", false);
        $("input[id=search_doctor]").prop("checked", true);
        settingMod.mb_category = "doctors";
    }else if(sessionStorage.getItem("mod3_category") === "clinics"){
        $("input[id=search_doctor]").prop("checked", false);
        $("input[id=search_clinic]").prop("checked", true);
        settingMod.mb_category = "clinics";
    }
    if(sessionStorage.getItem("mod3_sidebar") && sessionStorage.getItem("mod3_sidebar") === "false"){
        settingMod.aside = "false";
         $("#toggle_sidebar").click();
            if(!document.getElementById("styleTmp")){
                var styleTmp = document.createElement("style");
                styleTmp.id =  "styleTmp";
                document.head.appendChild(styleTmp);
            }
            document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:none; } .medbooking-content{width: 870px !important;}.mb_tabs{width: 870px !important;}";
    }
    if(sessionStorage.getItem("mod3_drag") && sessionStorage.getItem("mod3_drag") === "false"){
        settingMod.dragForm = "false";
        $("#toggle_drag").click();
         if(!document.getElementById("styleTmpDrag")){
                var styleTmpDrag = document.createElement("style");
                styleTmpDrag.id =  "styleTmpDrag";
                document.head.appendChild(styleTmpDrag);
            }
        document.getElementById("styleTmpDrag").innerHTML = "#mb_minimized {display:none; }";
    }
    if(sessionStorage.getItem("mod3_folder")){
        $("#js-site-folder").val(sessionStorage.getItem("mod3_folder"));
        settingMod.mb_scriptUrl = sessionStorage.getItem("mod3_folder");
    }
    if(sessionStorage.getItem("mod3_special")){
        $("#selectSpecial option").attr("selected", false);
        $("#selectSpecial option[value="+sessionStorage.getItem("mod3_special")+"]").prop("selected", true);
    }
    $("#toggle_width option[value="+sessionStorage.getItem("mod3_contentWidth")+"]").prop('selected', true);
    
        $("#medbooking-doctor").removeClass("mb_contentWidth_870 mb_contentWidth_800 mb_contentWidth_750 mb_contentWidth_700");
        $("#medbooking-doctor").addClass("mb_contentWidth_" + sessionStorage.getItem("mod3_contentWidth"));
        $(".medbooking__doctor_list, .medbooking__clinic_list, .medbooking__tems, #medbooking__blog_service_list, #medbooking__blog_service_single, #doctor_card, .doctor_single .top_left_inner_doctor_single, .top_inner_clinik_single, .top_inner_clinik_single, .bottom_inner_clinik_single")
            .css({"width": sessionStorage.getItem("mod3_contentWidth")+"px"});
    

	$("body").on("click", "#btn_code", function(){
    $("#copy").removeClass("btn_copy").val("Скопировать");
		$("#code_msg").text(settingMod.tmp());
		for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
		}
		for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
		}
		for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
		}
		$('#popup_mod').show();
		$('body').append('<div class="overlay"></div>');
	});

	$("body").on("change", "#toggle_drag", function(){
		if($(this).is(":checked")){
			$("#mb_minimized").show();
			settingMod.dragForm = "true";
            sessionStorage.setItem("mod3_drag", true);
		}else{
			$("#mb_minimized").hide();
			settingMod.dragForm = "false";
            sessionStorage.setItem("mod3_drag", false);
		}
	});
    $("body").on("click", "#toggle_sidebar", function(){
        if(!document.getElementById("styleTmp")){
            var styleTmp = document.createElement("style");
            styleTmp.id =  "styleTmp";
            document.head.appendChild(styleTmp);
        }
        if($(this).is(":checked")){
            sessionStorage.setItem("mod3_sidebar", true);
            settingMod.aside = "true";
            document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:block}.medbooking-content{width: 1090px !important;}.mb_tabs{width: 1090px !important;}";
        }else{
            sessionStorage.setItem("mod3_sidebar", false);
             document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:none; } .medbooking-content{width: 870px !important;}.mb_tabs{width: 870px !important;}";
            settingMod.aside = "false";
        }
    });
    


    $("body").on("change", "#toggle_width", function(){
        settingMod.contentWidth = $(this).val();
        sessionStorage.setItem("mod3_contentWidth", $(this).val());
        for(var i = 0; i < document.querySelectorAll("#toggle_width option").length; i++){
            document.getElementById("medbooking-doctor").classList.remove("mb_contentWidth_" + document.querySelectorAll("#toggle_width option")[i].value);
        }
        $("#medbooking-doctor").addClass("mb_contentWidth_" + settingMod.contentWidth);
        $(".medbooking__doctor_list, .medbooking__clinic_list, .medbooking__tems, #medbooking__blog_service_list, #medbooking__blog_service_single, #doctor_card, .doctor_single .top_left_inner_doctor_single, .top_inner_clinik_single, .top_inner_clinik_single, .bottom_inner_clinik_single")
            .css({"width": settingMod.contentWidth + "px"})
    });
	$("body").on("click", "#popup_close, .overlay", function(){
		$('#popup_mod').hide();
		$(".overlay").remove();
	});
    $("body").on("click", "input[name=category_toogle]", function(){
        if($(this).attr("id") === "search_clinic"){
            sessionStorage.setItem("mod3_category", "clinics");
             settingMod.mb_category = "search_clinic";
            url = "http://medbooking.com/"+settingMod.mb_category+"/"+settingMod.mb_special+"?ajax=ref";
            scriptRequest(url, ok, fail, id_client, function(){});
        }else if($(this).attr("id") === "search_doctor"){
            sessionStorage.setItem("mod3_category", "doctors");
            settingMod.mb_category = "search_doctor";
            url = "http://medbooking.com/"+settingMod.mb_category+"/"+settingMod.mb_special+"?ajax=ref";
            scriptRequest(url, ok, fail, id_client, function(){});
        }
    });
    $("body").on("keyup", "#js-site-folder", function(){
        settingMod.mb_scriptUrl = $(this).val();
        sessionStorage.setItem("mod3_folder", $(this).val());
        
    });
    $("#selectSpecial").chosen().change(function(){
        sessionStorage.setItem("mod3_special", $(this).val());
        url = "http://medbooking.com/"+settingMod.mb_category+"/"+$(this).val()+"?ajax=ref";
        scriptRequest(url, ok, fail, id_client, function(){});
        settingMod.mb_special = $(this).val();
    });
   var client = new ZeroClipboard( $("#copy") );
      client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        } );
        client.on( 'aftercopy', function(event) {
            //
        } );
      } );
      client.on( 'error', function(event) {
         console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
      } );
});


