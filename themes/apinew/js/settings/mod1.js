$(function(){
    $('html').keydown(function(eventObject){
      if (event.keyCode == 27) { 
        if($("#popup_mod").css('display') === "block"){
            $("#popup_close").click();
        }
      }
    });
    $("#selectSpecial").chosen({no_results_text: "Нет такой специальности"}); 
    var height_form_modul = $(".js_form_modul").offset().top;
    $(window).scroll(function(){
        if($(window).scrollTop() >  height_form_modul){
            $(".js_form_modul").addClass("scrollFixed");
        }
        else{
            $(".js_form_modul").removeClass("scrollFixed");
        }   
    });
	var settingMod = {
    dragForm: "true",
    aside: "true",
    contentWidth: 870,
    mb_category: "search_doctor",
    mb_special: "lfk",
		tmp: function(){
			return '<div id="medbooking-doctor"></div> | '+
			        '<script> | @'+
				        'var id_client = '+mbId+',|'+
				            'num = '+mbPhone+',|'+
				            'dragForm = '+this.dragForm+',|'+
                    'contentWidth = '+this.contentWidth+',|'+
				            'aside = '+this.aside+',|'+
				            'mb_category = "'+this.mb_category+'",|'+
				            'mb_special = "'+this.mb_special+'",|'+
				            'url = "http://medbooking.com/'+this.mb_category+'/'+this.mb_special+'?ajax=ref",|'+
				            'scriptJs = document.createElement("script");|'+
				        'scriptJs.id = "initScript";|'+
				        'scriptJs.src = "http://medbooking.com/scripts/integrator/moduleDocCategoryL/script.js";|'+
				        'document.head.appendChild(scriptJs);| !'+
			        '</script>| '
		}
	};

	$("body").on("click", "#btn_code", function(){
    $("#copy").removeClass("btn_copy").val("Скопировать");
		$("#code_msg").text(settingMod.tmp());
		for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
		}
		for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
		}
		for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
		}
		$('#popup_mod').show();
		$('body').append('<div class="overlay"></div>');
	});

	$("body").on("change", "#toggle_drag", function(){
		if($(this).is(":checked")){
			$("#mb_minimized").show();
			settingMod.dragForm = "true";
		}else{
			$("#mb_minimized").hide();
			settingMod.dragForm = "false";
		}
	});
    $("body").on("change", "#toggle_sidebar", function(){
        if(!document.getElementById("styleTmp")){
            var styleTmp = document.createElement("style");
            styleTmp.id =  "styleTmp";
            document.head.appendChild(styleTmp);
        }
        if($(this).is(":checked")){
            settingMod.aside = "true";
            document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:block}.medbooking-content{width: 1090px !important;}.mb_tabs{width: 1090px !important;}";
        }else{
             document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:none; } .medbooking-content{width: 870px !important;}.mb_tabs{width: 870px !important;}";
            settingMod.aside = "false";
        }
    });
    $("body").on("change", "#toggle_width", function(){
        settingMod.contentWidth = $(this).val();
        for(var i = 0; i < document.querySelectorAll("#toggle_width option").length; i++){
            document.getElementById("medbooking-doctor").classList.remove("mb_contentWidth_" + document.querySelectorAll("#toggle_width option")[i].value);
        }
        document.getElementById("medbooking-doctor").classList.add("mb_contentWidth_" + settingMod.contentWidth);
        document.getElementById("styleContentWidthParam").innerHTML = ".medbooking__doctor_list, .medbooking__clinic_list, .medbooking__tems, #medbooking__blog_service_list, #medbooking__blog_service_single, #doctor_card, .doctor_single .top_left_inner_doctor_single, .top_inner_clinik_single, .top_inner_clinik_single, .bottom_inner_clinik_single {width: "+settingMod.contentWidth+"px !important}";
    });
	$("body").on("click", "#popup_close, .overlay", function(){
		$('#popup_mod').hide();
		$(".overlay").remove();
	});
    $("body").on("click", "input[name=category_toogle]", function(){
        if($(this).attr("id") === "search_clinic"){
             settingMod.mb_category = "search_clinic";
            url = "http://medbooking.com/"+settingMod.mb_category+"/"+settingMod.mb_special+"?ajax=ref";
            scriptRequest(url, ok, fail, id_client, function(){});
        }else if($(this).attr("id") === "search_doctor"){
            settingMod.mb_category = "search_doctor";
            url = "http://medbooking.com/"+settingMod.mb_category+"/"+settingMod.mb_special+"?ajax=ref";
            scriptRequest(url, ok, fail, id_client, function(){});
        }
    });
    
    $("#selectSpecial").chosen().change(function(){
        url = "http://medbooking.com/"+settingMod.mb_category+"/"+$(this).val()+"?ajax=ref";
        scriptRequest(url, ok, fail, id_client, function(){});
        settingMod.mb_special = $(this).val();
    });
   var client = new ZeroClipboard( $("#copy") );
      client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        } );
        client.on( 'aftercopy', function(event) {
            //
        } );
      } );
      client.on( 'error', function(event) {
         console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
      } );
});


