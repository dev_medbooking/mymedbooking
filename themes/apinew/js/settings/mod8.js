$(function(){
  $('html').keydown(function(eventObject){
      if (event.keyCode == 27) { 
        if($("#popup_mod").css('display') === "block"){
            $("#popup_close").click();
        }
      }
    });
	var height_form_modul = $(".js_form_modul").offset().top;
    $(window).scroll(function(){
        if($(window).scrollTop() >  height_form_modul){
            $(".js_form_modul").addClass("scrollFixed");
        }
        else{
            $(".js_form_modul").removeClass("scrollFixed");
        }   
    });
	var settingMod = {
		mb_fw: "normal",
		mb_fz: "16px",
		mb_td: "none",
		mb_fs: "normal",
		mb_brs: "0",
		mb_bg: "#b1d478",
		mb_col: "#fff",
		mb_dragForm: "true",
    mb_dragFormB: "true",
		mb_titlePop: "Записаться на прием",
		mb_nameBtn: "Записаться на прием",
		tmp: function(){ 
			return 	'<button class="mb_mod_button">'+this.mb_nameBtn+'</buttom> |'+
					'<script> | @'+
				        'var id_client = '+mbId+',|'+
				            'num = '+mbPhone+',|'+
				            'mb_fw = "'+this.mb_fw+'",|'+
				            'mb_fz = "'+this.mb_fz+'",|'+
				            'mb_td = "'+this.mb_td+'",|'+
				            'mb_fs = "'+this.mb_fs+'",|'+
				            'mb_brs = "'+this.mb_brs+'",|'+
				            'mb_bg = "'+this.mb_bg+'",|'+
                    'mb_dragForm = "'+this.mb_dragForm+'",|'+
				            'mb_dragFormB = "'+this.mb_dragFormB+'",|'+
				            'mb_col = "'+this.mb_col+'",|'+
				            'mb_titlePop = "'+this.mb_titlePop+'",|'+
				            'scriptJs = document.createElement("script");|'+
				        'scriptJs.src = "http://medbooking.com/scripts/integrator/moduleBtn/script.js";|'+
				        'document.head.appendChild(scriptJs);| !'+
			        '</script>| '
		}
	};
	$("body").on("click", "#btn_code", function(){
    $("#copy").removeClass("btn_copy").val("Скопировать");
		$("#code_msg").text(settingMod.tmp());
		for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
		}
		for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
		}
		for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
		}
		$('#popup_mod').show();
		$('body').append('<div class="overlay"></div>');
	});
    var noCheckStyle = function(){
        if($("#allStylebtn").is(":checked")){
            $("#allStylebtn").click();
        }
    };
        $('#picker_back').colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                if(!bySetColor) $(el).val(hex);
                $(".btn_module").css({"background": '#'+hex});
                settingMod.mb_bg = '#'+hex;
                noCheckStyle();
            }
        });
        $('#picker_text').colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                if(!bySetColor) $(el).val(hex);
                $(".btn_module").css({"color": '#'+hex});
                settingMod.mb_col = '#'+hex;
                noCheckStyle();
            }
        })
   $( "#slider-range-max" ).slider({
      range: "max",
      min: 0,
      max: 50,
      value: 0,
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.value );
      }
    });
    $( "#amount" ).val( $( "#slider-range-max" ).slider( "value" ) );
	   $( "#slider-range-max" ).on( "slide", function( event, ui ) {
		      $(".btn_module").css({"border-radius": $("#amount").val() + "px"});
		      settingMod.mb_brs = $("#amount").val() + "px";
		      settingMod.mb_brs = (parseInt($("#amount").val()) + 1) + "px";
          noCheckStyle();
	   } );
    $("body").on("click", ".js-bold", function(){
       if(!$(this).hasClass("active")){
           $(this).addClass("active");
           $(".btn_module").css({"font-weight": "bold"});
           settingMod.mb_fw = "bold";
           noCheckStyle();
       }else{
           $(this).removeClass("active");
           $(".btn_module").css({"font-weight": "normal"});
           settingMod.mb_fw = "normal";
           noCheckStyle();
       }
    });
        $("body").on("click", ".js-italic", function(){
       if(!$(this).hasClass("active")){
           $(this).addClass("active");
           $(".btn_module").css({"font-style": "italic"});
           settingMod.mb_fs = "italic";
           noCheckStyle();
       }else{
           $(this).removeClass("active");
           $(".btn_module").css({"font-style": "normal"});
           settingMod.mb_fs = "normal";
           noCheckStyle();
       }
    });
        $("body").on("click", ".js-underline", function(){
       if(!$(this).hasClass("active")){
           $(this).addClass("active");
           $(".btn_module").css({"text-decoration": "underline"});
           settingMod.mb_td = "underline";
           noCheckStyle();
       }else{
           $(this).removeClass("active");
           $(".btn_module").css({"text-decoration": "none"});
           settingMod.mb_td = "none";
           noCheckStyle();
       }
    });
    $("body").on("change", ".js-font_size", function(){
        $(".btn_module").css({"font-size": $(this).val() + "px"});
        settingMod.mb_fz = $(this).val() + "px";
        noCheckStyle();
    });

	$("body").on("click", "#popup_close, .overlay", function(){
		$('#popup_mod').hide();
		$(".overlay").remove();
	});
    $("body").on("click", "#allStylebtn", function(){
        var tmpBtn = ".btn_module { background: #b1d478; border-radius: 0; color: #fff; font-size: 16px; padding: 9px 20px; cursor: pointer; outline: none; border: none; }";
        if($(this).is(":checked")){
            $(".btn_module").attr("style", tmpBtn);
            // Сброс к первоисточнику
            if($(".js-bold").hasClass("active")){
                $(".js-bold").click();
            }
            if($(".js-italic").hasClass("active")){
                $(".js-italic").click();
            }
            if($(".js-underline").hasClass("active")){
                $(".js-underline").click();
            }
            $(".js-font_size option[value='16']").prop('selected', true);
            $( "#slider-range-max" ).slider({
                value: 0
            });
            $( "#amount" ).val( $( "#slider-range-max" ).slider( "value" ) );
            
            $("#picker_back").css({"border-color":"#b1d478"});
            $("#picker_back").val("b1d478");
            $("#picker_text").css({"border-color":"#ffffff"});
            $("#picker_text").val("ffffff");
            
            $(".mb_mod_button").text("Записаться на прием");
            settingMod.mb_nameBtn = "Записаться на прием";
            $("#name_btn").val("");
            
            $(".mb-formWrapper__top__title").text("Записаться на прием");
            settingMod.mb_titlePop = "Записаться на прием";
            $("#name_popup").val("");
        }
    });
    $("body").on("click", "#form", function(){
       if($(this).is(":checked")){
           settingMod.mb_dragForm = "true";
           settingMod.mb_dragFormB = "true";
           $("#mb_minimized").show();

       }else{
           settingMod.mb_dragForm = "false";
           settingMod.mb_dragFormB = "false";
           $("#mb_minimized").hide();
       }
    });
    $("body").on("keyup", "#name_btn", function(){
        $(".mb_mod_button").text($(this).val());
        settingMod.mb_nameBtn = $(this).val();
        noCheckStyle();
    });
    $("body").on("keyup", "#name_popup", function(){
        settingMod.mb_titlePop = $(this).val();
        $(".mb-formWrapper__top__title").text($(this).val());
        noCheckStyle();
    });
    $("body").on("click", ".mb_mod_button", function(){
       console.log($(".mb-formWrapper__top__title").text(settingMod.mb_titlePop));
    });
   var client = new ZeroClipboard( $("#copy") );
      client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        } );
        client.on( 'aftercopy', function(event) {
            //
        } );
      } );
      client.on( 'error', function(event) {
         console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
      } );
})