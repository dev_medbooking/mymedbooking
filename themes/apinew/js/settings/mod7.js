$(function(){
    $('html').keydown(function(eventObject){
      if (event.keyCode == 27) { 
        if($("#popup_mod").css('display') === "block"){
            $("#popup_close").click();
        }
      }
    });
    $("#selectSpecial, .selectServices").chosen({no_results_text: "Нет такой специальности"});
    $(".select_wrapper:not(.select_wrapper:first)").hide();
    var height_form_modul = $(".js_form_modul").offset().top;
    $(window).scroll(function(){
        if($(window).scrollTop() >  height_form_modul){
            $(".js_form_modul").addClass("scrollFixed");
        }
        else{
            $(".js_form_modul").removeClass("scrollFixed");
        }   
    });
	var settingMod = {
    dragForm: "true",
    aside: "true",
    contentWidth: 750,
    mb_special: "uzi",
		tmp: function(){
			return '<div id="medbooking-doctor"></div> | '+
			        '<script> | @'+
				        'var id_client = '+mbId+',|'+
				            'num = '+mbPhone+',|'+
				            'dragForm = '+this.dragForm+',|'+
                            'aside = '+this.aside+',|'+
                            'contentWidth = '+this.contentWidth+',|'+
				            'mb_special = "'+this.mb_special+'",|'+
				            'url = "http://diagnostika.medbooking.com/diagnostics/'+this.mb_special+'?ajax=ref",|'+
				            'scriptJs = document.createElement("script");|'+
				        'scriptJs.id = "initScript";|'+
				        'scriptJs.src = "http://medbooking.com/scripts/integrator/diagnosticModule/script.js";|'+
				        'document.head.appendChild(scriptJs);| !'+
			        '</script>| '
		}
	};
	$("body").on("click", "#btn_code", function(){
    $("#copy").removeClass("btn_copy").val("Скопировать");
		$("#code_msg").text(settingMod.tmp());
		for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
		}
		for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
		}
		for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
		}
		$('#popup_mod').show();
		$('body').append('<div class="overlay"></div>');
	});
	$("body").on("change", "#toggle_drag", function(){
		if($(this).is(":checked")){
			$("#mb_minimized").show();
			settingMod.dragForm = "true";
		}else{
			$("#mb_minimized").hide();
			settingMod.dragForm = "false";
		}
	});
    $("body").on("change", "#toggle_sidebar", function(){
        if(!document.getElementById("styleTmp")){
            var styleTmp = document.createElement("style");
            styleTmp.id =  "styleTmp";
            document.head.appendChild(styleTmp);
        }
        if($(this).is(":checked")){
            settingMod.aside = "true";
            document.getElementById("styleTmp").innerHTML =  "#medbooking-doctor aside{display: block !important;} #medbooking-doctor .content{float: left;}";
        }else{
             document.getElementById("styleTmp").innerHTML = "#medbooking-doctor aside{display: none !important;} #medbooking-doctor .content{float: none;}";
            settingMod.aside = "false";
        }
    });
	$("body").on("click", "#popup_close, .overlay", function(){
		$('#popup_mod').hide();
		$(".overlay").remove();
	});
    $("body").on("change", "#toggle_width", function(){
        settingMod.contentWidth = $(this).val();
        for(var i = 0; i < document.querySelectorAll("#toggle_width option").length; i++){
            document.getElementById("medbooking-doctor").classList.remove("mb_contentWidth_" + document.querySelectorAll("#toggle_width option")[i].value);
        }
        document.getElementById("medbooking-doctor").classList.add("mb_contentWidth_" + settingMod.contentWidth);
        document.getElementById("styleContentWidthParam").innerHTML = "#medbooking-doctor .content {width: "+settingMod.contentWidth+"px !important}";
    });    
    $("body").on("change", "#selectSpecial", function(){
        switch ($(this).val()) {
            case "uzi":
                $(".select_wrapper").hide();
                $(".js-" + $(this).val()).parents(".select_wrapper").show();
                $(".js-title-select-wrapper").show();
                break;
            case "komputernaya-tomografiya":
                $(".select_wrapper").hide();
                $(".js-" + $(this).val()).parents(".select_wrapper").show();
                $(".js-title-select-wrapper").show();
                break;
            case "mrt":
                $(".select_wrapper").hide();
                $(".js-" + $(this).val()).parents(".select_wrapper").show();
                $(".js-title-select-wrapper").show();
                break;
            case "rentgen":
                $(".select_wrapper").hide();
                $(".js-" + $(this).val()).parents(".select_wrapper").show();
                $(".js-title-select-wrapper").show();
                break;
            case "dupleksnoe-skanirovanie":
                $(".select_wrapper").hide();
                $(".js-" + $(this).val()).parents(".select_wrapper").show();
                $(".js-title-select-wrapper").show();
                break;
            case "func-diagnostika":
                $(".select_wrapper").hide();
                $(".js-" + $(this).val()).parents(".select_wrapper").show();
                $(".js-title-select-wrapper").show();
                break;
            case "enmg":
                $(".select_wrapper").hide();
                $(".js-" + $(this).val()).parents(".select_wrapper").show();
                $(".js-title-select-wrapper").show();
                break;
            case "endoskopicheskie-issledovaniya":
                $(".select_wrapper").hide();
                $(".js-" + $(this).val()).parents(".select_wrapper").show();
                $(".js-title-select-wrapper").show();
                break;
            case "fluorografiya":
                $(".select_wrapper").hide();
                $(".js-title-select-wrapper").hide();
                break;
           default:
                break;
        }
        settingMod.mb_special = $(this).val();
        scriptRequest("http://diagnostika.medbooking.com/diagnostics/"+$(this).val()+"?ajax=ref", ok, fail, id_client);
    });    
    $("body").on("change", ".selectServices ", function(){
        settingMod.mb_special = $(this).val();
        scriptRequest("http://diagnostika.medbooking.com/diagnostics/"+$(this).val()+"?ajax=ref", ok, fail, id_client);
    });
	var client = new ZeroClipboard( $("#copy") );
      client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        } );
        client.on( 'aftercopy', function(event) {
            //
        } );
      } );
      client.on( 'error', function(event) {
         console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
      } );
});



