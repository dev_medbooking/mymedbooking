$(function(){
	$('html').keydown(function(eventObject){
      if (event.keyCode == 27) { 
        if($("#popup_mod").css('display') === "block"){
            $("#popup_close").click();
        }
      }
    });
	$("#selectSpecial").chosen({no_results_text: "Нет такой специальности"}); 
	var settingMod = {
	aside_class: "bb",
    dragForm: "true",
    mb_asideView: "false",
    aside: "true",
    contentWidth: 870,
    searchFlag: "doc",
    searchCategory: "kardiolog",
		tmp: function(){
			return '<div id="moduleSidebar" class="'+this.aside_class+'"></div> | '+
				   '<div id="medbooking-doctor"></div> | '+
			        '<script> | @'+
				        'var id_client = '+mbId+',|'+
				            'num = '+mbPhone+',|'+
				            'aside_class = "'+this.aside_class+'",|'+
				            'dragForm = '+this.dragForm+',|'+
				            'mb_asideView = '+this.mb_asideView+',|'+
                    		'contentWidth = '+this.contentWidth+',|'+
				            'aside = '+this.aside+',|'+
				            'searchFlag = "'+this.searchFlag+'",|'+
				            'searchCategory  = "'+this.searchCategory +'",|'+
				            'scriptJs = document.createElement("script");|'+
				        'scriptJs.id = "initScript";|'+
				        'scriptJs.src = "http://medbooking.com/scripts/integrator/sidebarModule/script.js";|'+
				        'document.head.appendChild(scriptJs);| !'+
			        '</script>| '
		}
	};
	$("body").on("click", "#btn_code", function(){
    $("#copy").removeClass("btn_copy").val("Скопировать");
		$("#code_msg").text(settingMod.tmp());
		for (var i = 0; i < settingMod.tmp().split("|").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("|", '<br>'))
		}
		for (var i = 0; i < settingMod.tmp().split("@").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("@", '<section class="code_msg_script_inner">'));
		}
		for (var i = 0; i < settingMod.tmp().split("!").length; i++) {
			$("#code_msg").html($("#code_msg").html().replace("!", '</section>'));
		}
		$('#popup_mod').show();
		$('body').append('<div class="overlay"></div>');
	});
	$("body").on("change", "#toggle_drag", function(){
		if($(this).is(":checked")){
			$("#mb_minimized").show();
			settingMod.dragForm = "true";
		}else{
			$("#mb_minimized").hide();
			settingMod.dragForm = "false";
		}
	});
	$("body").on("click", "#popup_close, .overlay", function(){
		$('#popup_mod').hide();
		$(".overlay").remove();
	});
	 $("body").on("click", "#form", function(){
       if($(this).is(":checked")){
           settingMod.dragForm  = "true";
           $("#mb_minimized").show();

       }else{
           settingMod.dragForm  = "false";
           $("#mb_minimized").hide();
       }
    });
	 $("body").on("click", "input[name='orientation']", function(){
	 	console.log($(this).attr("id"));
	 });
	 $("body").on("click", "input[name='color']", function(){
	 	var aside_w = $("#asideWidth option:selected").val(),
	 		aside_col = $(this).attr("id"),
	 		aside_el = $("#moduleSidebar");
	 		if(aside_w === "300" && aside_col === "blue"){
	 			aside_el.attr("class", "");
	 			aside_el.addClass("bb");
	 			settingMod.aside_class = "bb";
	 		}
	 		if(aside_w === "300" && aside_col === "white"){
	 			aside_el.attr("class", "");
	 			aside_el.addClass("wb");
	 			settingMod.aside_class = "wb";
	 		}
	 		if(aside_w === "250" && aside_col === "blue"){
	 			aside_el.attr("class", "");
	 			aside_el.addClass("bs");
	 			settingMod.aside_class = "bs";
	 		}
	 		if(aside_w === "250" && aside_col === "white"){
	 			aside_el.attr("class", "");
	 			aside_el.addClass("ws");
	 			settingMod.aside_class = "ws";
	 		}
	 });
	 $("body").on("change", "#asideWidth", function(){
	 	console.log($("#asideWidth option:selected").val());
     	var aside_w = $("#asideWidth option:selected").val(),
	 		aside_col = $("input[name='color']:checked").attr("id"),
	 		aside_el = $("#moduleSidebar");
	 		if(aside_w === "300" && aside_col === "blue"){
	 			aside_el.attr("class", "");
	 			aside_el.addClass("bb");
	 			settingMod.aside_class = "bb";
	 		}
	 		if(aside_w === "300" && aside_col === "white"){
	 			aside_el.attr("class", "");
	 			aside_el.addClass("wb");
	 			settingMod.aside_class = "wb";
	 		}
	 		if(aside_w === "250" && aside_col === "blue"){
	 			aside_el.attr("class", "");
	 			aside_el.addClass("bs");
	 			settingMod.aside_class = "bs";
	 		}
	 		if(aside_w === "250" && aside_col === "white"){
	 			aside_el.attr("class", "");
	 			aside_el.addClass("ws");
	 			settingMod.aside_class = "ws";
	 		}
    });
    $("body").on("click", "input[name=category_toogle]", function(){
        if($(this).attr("id") === "search_clinic"){
        	$(".searchTitle").text("Поиск клиники Москва");
        	searchType = "/search_clinic";
        	settingMod.searchFlag = "clinic";
        }else if($(this).attr("id") === "search_doctor"){
        	$(".searchTitle").text("Поиск врача Москва");
        	searchType = "/doctors";
        	settingMod.searchFlag = "doc";
        }

		if(!f_searchDoc){
			f_searchDoc = "";
		}
        if(!f_searchSubway){
            f_searchSubway = "";
        }
		if(searchType === "/doctors"){
		    if(f_searchSubway){
		        url = "http://medbooking.com"+searchType+""+f_searchDoc+"/metro/"+f_searchSubway+"?ajax=ref";
		    }
		    else{
		        if(searchCategory){
		            url = "http://medbooking.com"+searchType+"/"+searchCategory+"/"+f_searchDoc+"?ajax=ref";
		        }else{
		            url = "http://medbooking.com"+searchType+""+f_searchDoc+"?ajax=ref";
		        }
		    }
		    window.history.pushState("#" + "doctors" ,"qwe", "#" + "/doctors/");
		    }else if(searchType === "/search_clinic"){

		           if(f_searchSubway){
		            url = "http://medbooking.com"+searchType+""+f_searchDoc+"/metro/"+f_searchSubway+"?ajax=ref";
		        }
		        else{
		            url = "http://medbooking.com"+searchType+"/"+$("#selectSpecial").val()+"?ajax=ref";
		        }
		        window.history.pushState("#" + "search_clinic" ,"qwe", "#" + "/search_clinic/");
		   }
          scriptRequest(url, ok, fail, id_client);
    });
    $("#selectSpecial").chosen().change(function(){
        f_searchDoc = "";
        f_searchSubway = "";
        $("#specialTitle strong").text("Выберите специальность");
        $("#subwayTitle strong").text("Выберите метро");
        searchCategory = $(this).val();
        settingMod.searchCategory = $(this).val();
		if(!f_searchDoc){
			f_searchDoc = "";
		}
        if(!f_searchSubway){
            f_searchSubway = "";
        }
		if(searchType === "/doctors"){
			if(f_searchSubway){
				url = "http://medbooking.com"+searchType+""+f_searchDoc+"/metro/"+f_searchSubway+"?ajax=ref";
			}
            else{
                if(searchCategory){
                    url = "http://medbooking.com"+searchType+"/"+searchCategory+"/"+f_searchDoc+"?ajax=ref";
                }else{
                    url = "http://medbooking.com"+searchType+""+f_searchDoc+"?ajax=ref";
                }
            }
        window.history.pushState("#" + "doctors" ,"qwe", "#" + "/doctors/");
        }else if(searchType === "/search_clinic"){
               if(f_searchSubway){
                url = "http://medbooking.com"+searchType+"/"+$(this).val()+"/metro/"+f_searchSubway+"?ajax=ref";
            }
            else{
                url = "http://medbooking.com"+searchType+"/"+$("#selectSpecial").val()+"?ajax=ref";
            }
            window.history.pushState("#" + "search_clinic" ,"qwe", "#" + "/search_clinic/");
        }
        scriptRequest(url, ok, fail, id_client);
    });
	$("body").on("click", "#toggle_all", function(){
		 if($(this).is(":checked")){
		 	settingMod.mb_asideView = "true";
         	$(".vh").css({"visibility":"visible"});
         	if(!f_searchDoc){
                        f_searchDoc = "";
                    }
                    if(!f_searchSubway){
                        f_searchSubway = "";
                    }
                if(searchType === "/doctors"){
                    if(f_searchSubway){
                        url = "http://medbooking.com"+searchType+""+f_searchDoc+"/metro/"+f_searchSubway+"?ajax=ref";
                    }
                    else{
                        if(searchCategory){
                            url = "http://medbooking.com"+searchType+"/"+searchCategory+"/"+f_searchDoc+"?ajax=ref";
                        }else{
                            url = "http://medbooking.com"+searchType+""+f_searchDoc+"?ajax=ref";
                        }
                    }
                    window.history.pushState("#" + "doctors" ,"qwe", "#" + "/doctors/");
                    }else if(searchType === "/search_clinic"){
                           if(f_searchSubway){
                            url = "http://medbooking.com"+searchType+""+f_searchDoc+"/metro/"+f_searchSubway+"?ajax=ref";
                        }
                        else{
                            url = "http://medbooking.com"+searchType+""+f_searchDoc+"?ajax=ref";
                        }
                        window.history.pushState("#" + "search_clinic" ,"qwe", "#" + "/search_clinic/");
                    }
            scriptRequest(url, ok, fail, id_client);

       }else{
       		settingMod.mb_asideView = "false";
       		document.getElementById("medbooking-doctor").innerHTML = "";
            location.hash = "";
       		$(".vh").css({"visibility":"hidden"});
       }
	});
    $("body").on("change", "#toggle_sidebar", function(){
        if(!document.getElementById("styleTmp")){
            var styleTmp = document.createElement("style");
            styleTmp.id =  "styleTmp";
            document.head.appendChild(styleTmp);
        }
        if($(this).is(":checked")){
            settingMod.aside = "true";
            document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:block}.medbooking-content{width: 1090px !important;}.mb_tabs{width: 1090px !important;}";
        }else{
             document.getElementById("styleTmp").innerHTML = ".medbooking__aside{display:none; } .medbooking-content{width: 870px !important;}.mb_tabs{width: 870px !important;}";
            settingMod.aside = "false";
        }
    });
    $("body").on("change", "#toggle_width", function(){
        settingMod.contentWidth = $(this).val();
        for(var i = 0; i < document.querySelectorAll("#toggle_width option").length; i++){
            document.getElementById("medbooking-doctor").classList.remove("mb_contentWidth_" + document.querySelectorAll("#toggle_width option")[i].value);
        }
        document.getElementById("medbooking-doctor").classList.add("mb_contentWidth_" + settingMod.contentWidth);
        document.getElementById("styleContentWidthParam").innerHTML = ".medbooking__doctor_list, .medbooking__clinic_list, .medbooking__tems, #medbooking__blog_service_list, #medbooking__blog_service_single, #doctor_card, .doctor_single .top_left_inner_doctor_single, .top_inner_clinik_single, .top_inner_clinik_single, .bottom_inner_clinik_single {width: "+settingMod.contentWidth+"px !important}";
    });
	var client = new ZeroClipboard( $("#copy") );
      client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', $("code_msg").text());
            $("#copy").addClass("btn_copy").val("Код скопирован в буфер");
        } );
        client.on( 'aftercopy', function(event) {
            //
        } );
      } );
      client.on( 'error', function(event) {
         console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
     });
});