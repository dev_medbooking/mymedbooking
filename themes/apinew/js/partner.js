var apiPartner = (function(){
    var changeSelect = function (){
        $('body').on('change','.js-select-box', function(){
           window.location.href = $('.js-select-box option:selected').val();
        });
    }
    
    var searchInput = function (){
        $('body').on('keyup','.js-search-form input', function(){
                $.ajax({
                    url: $('.js-search-form').attr('action'),
                    type: 'POST',
                    data: $('.js-search-form').serialize(),
                    dataType: "json",
                    async:true,
                    success: function(r) {
                        if(r.tbody){
                            $('.js-update-data').html(r.tbody);
                        }else{
                            $('.js-update-data').html('');
                        }
                        if(r.pages){
                            $('#pages').html(r.pages);
                        }else{
                            $('#pages').html('');
                        }
                    }

                });
            return false;
        });
    }
    
    var updateName = function (){
        $('body').on('click','.js-partner-form input[type="submit"]', function(){
            if($('.js-partner-form input[name="PartnerName"]').val()){
                $.ajax({
                    url: $('.js-partner-form').attr('action'),
                    type: 'POST',
                    data: $('.js-partner-form').serialize(),
                    dataType: "json",
                    async:true,
                    success: function(r) {
                        if(r.success){
                            $('.js-partner-form input[type="text"]').val('');
                            alert('Имя изменено.');
                        }
                    }

                });
            }else{
                alert('Заполните поле');
            }
            return false;
        });
    }

    var savePhone = function (){
        $('body').on('click','.js-partner-phone input[type="submit"]', function(){
            if($('.js-partner-phone select[name="phone"]').val()){
                $.ajax({
                    url: $('.js-partner-phone').attr('action'),
                    type: 'POST',
                    data: $('.js-partner-phone').serialize(),
                    dataType: "json",
                    async:true,
                    success: function(r) {
                        if(r.success){
                            alert('Номер сохранен');
                            $('.js-partner-phone').replaceWith(r.data);
                        }
                    }

                });
            }else{
                alert('Заполните поле');
            }
            return false;
        });
    }

    var CalendarStart = function(){
        $('#js-calend-date_start, #js-calend-date_end').datepicker({
            lang: "ru",
            dateFormat: "yy-mm-dd"
        });
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Текущее время',
            closeText: 'Закрыть',
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);
    
    }
    
    var createExel = function(){
        $('body').on('click','.js-exel-button', function(){
            window.open($(this).attr('data-href')+'&'+$('.js-search-form').serialize(), '_blank');
            return false;
        });
        
        
    }
    
    
    
    return {
       cs:changeSelect,
       si:searchInput,
       un:updateName,
       sp:savePhone,
       caledarStart:CalendarStart,
       ce:createExel,
    }
}());
$(function(){
   apiPartner.cs();
   apiPartner.si();
   apiPartner.un();
   apiPartner.sp();
   apiPartner.caledarStart();
   apiPartner.ce();
});
