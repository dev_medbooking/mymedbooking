$(function() {
    $("#plus_service").on('click', function() {
        var div = $("#services_select_div").html();
        $("#services_select_div").after(div);
        $('.services_select').attr('name', 'MbRecordServices[service_id][]');
        $('.services_select').select2();
        $('div > span.select2-container--default:nth-child(4)').remove();
        //alert(true);
    });
    $("#minus_service").on('click', function() {
        //alert(true);
        $(".services_select_div_inner:last").remove();
    });
    $("body").on("click", ".ui-menu-item", function(){
        if ($("#MbRecord_record_type").val() != '0'){
            $("#admin-form").append("<input id='hiddenSelest' name='MbRecord[record_type]' type='hidden' value='"+$("#MbRecord_record_type option:selected").val()+"'>");
            $("#MbRecord_record_type").prop("disabled", true);
        } 
    });
    $("body").on("keyup", "#service_auto", function(){
        $("#MbRecord_record_type").prop("disabled", false);
        $("#hiddenSelest").remove();
    });

   $("body").on("click", "._create_block ._save_btn, ._create-block ._save_btn", function(){
    var check = true;
       $('.tips-box').remove();
       $('.request').each(function(){
           if(!$(this).val()){
               $(this).parents(".row").append('<div class="tips-box">Заполните поле</div>');
               check = false;
           }
       });
       if($('.request_date').is(':visible')&&$('.request_date').val()){
            $.ajax({
                url: $('.request_date').attr('data-href'),
                type: 'POST',
                data: {id:$('#create-block').attr('data-id'),date:$('.request_date').val()},
                dataType: "json",
                async:false,
                success: function(r) {
                    if(r.success==1){
                        check = false;
                        alert('Заявка на этот месяц уже сущевствует');
                    }else{
                       check = true; 
                    }

                }

            });
        }
       if(!check){
           return false;
       }
   });
    $('body').on('change', '#domain_id', function(){
       $("#clinic_name_auto, #clinic_name_auto_id, #doctor_name_auto, #doctor_name_auto_id").val("");
       $('#MbRecord_domain_phone').val($('#domain_id option:selected').attr("data-phone"));
    });
    
    $('body').on('keyup', '#clinic_name_auto', function(){
       $("#clinic_name_auto_id").val("");
        if (-1 < $("#clinic_name_auto").val().toLowerCase().indexOf('медси')) {
            if ($("#comment_for_none_date").length == 0)
                $("#clinic_name_auto").parents(".row").after(
                    "<div class='row clearfix'>" +
                    "<label for='comment_for_none_date'>Коммент</label>" +
                    "<select id='comment_for_none_date' name='MbRecord[comment_for_none_date]'>"+
                        "<option value=''></option>"+
                        "<option value='Свяжуться напрямую'>Свяжуться напрямую</option>"+
                    "</select>"+
                    "</div>");
        } else {
            $("#comment_for_none_date").parents(".row").remove();
        }
    });

    $('body').on('change', '#MbRecord_none_service', function(){
        if ($('#MbRecord_none_service').attr('checked') == 'checked') {
            $('#MbRecord_name_none_service').parents(".row").show();
        } else {
            $('#MbRecord_name_none_service').parents(".row").hide();
        }
    });

    var hr = window.location.href.substr(51);
//    if(hr === "1&status="){
//        $(".items-project li").removeClass("active");
//        $(".items-project .medbooking").addClass("active");
//    }
//    else if(hr === "2&status="){
//        $(".items-project li").removeClass("active");
//        $(".items-project .timetovisit").addClass("active");
//    }
//    else if(hr === "3&status="){
//        $(".items-project li").removeClass("active");
//        $(".items-project .testpuls").addClass("active");
//    }
//    else if(hr === "4&status="){
//        $(".items-project li").removeClass("active");
//        $(".items-project .fromed").addClass("active");
//    }
//    else{
//        $(".items-project li").removeClass("active");
//    }
    $("body").on("click", ".time1", function(){
        $("#ui_tpicker_hour_label_timeContainer").text("часы");
        $("#ui_tpicker_minute_label_timeContainer").text("минуты");
        $(".ui_tpicker_time_label").text("время");
    })
    $("body").on("click", ".fast-change", function(){
       $(this).parents("tr").find("div").hide(); 
    });
    $("body").on("click", ".items-project li", function() {
        $(".items-project li").removeClass("active")
        $(this).addClass("active");
        //$(".restart").show();
    });
    $("body").on("click", ".items-project li", function(){
        $(".restart").show();
    })
    if($(".items-project li").hasClass("active")){
        $(".restart").show();
    }
//    $(".restart").click(function() {
//        $(".items-project li").removeClass("active");
//        $(this).hide();
//        var hr = "/index.php?r=mbRecord";
//        window.location.href = hr;
//    });
    $("body").on("click", ".content-menu li", function() {
        $(".content-menu li").removeClass("active")
        $(this).addClass("active");
        $(".restart2").show();
    });
    $(".restart2").click(function() {
        $(".content-menu li").removeClass("active");
        $(this).hide();
        $('input[name="fromsite"]').val('0');
        $('input[name="fromcall"]').val('0');
        $('input[name="feedback"]').val('0');
        $('input[name="house"]').val('0');
        var url= $('.feedback').attr('data-href') + "&" + $('.hidden-form').serialize();
        $.ajax({
            url: url,
            type: 'POST',
            data: "sort-feedback",
            dataType: "json",
            success: function(r) {
                if (r.tbody) {
                    $("#admin-tbody").html(r.tbody);
                    if (r.pages) {
                        $('#pages').html(r.pages);
                    } else {
                        $('#pages').html('');
                    }
                } else {
                    $("#admin-tbody").html('');
                    $('#pages').html('');
                }
                if(r.count){
                    $("#summary").html(r.count);
                }
                if (r.filter) {
                    updateFilter(r.filter);
                }
            }
        });
    });
    $('body').on('click', '.js-show-history', function() {
        $(this).next().toggle();
        return false;
    });
    $("body").on("click", ".rewrite_btn", function(e) {
        e.preventDefault();
        $(this).hide();
        $(".time2").show();
    });
    $("#timeContainer").focusin(function() {
        $("#ui_tpicker_time_label_timeContainer").text("Время")
    });
    $("body").on("click", ".remove_btn", function(e) {
        e.preventDefault();
        $(".overlay").show();
        $(".popup-wrapper").show();
        $(".overlay,  .close, .yes-popup, .yes-popup , .no-popup").click(function() {
            $(".overlay").hide();
            $(".popup-wrapper").hide();
        });
    });
    
    $("body").on("click", ".save_btn", function(e) {
        $(".tips-box").remove();
        $('.note-form,.row').removeClass('error-text');
        $('.error').hide();
        var checked =true;
        //if ($("#MbRecord_first_name").val() == ""){
        //    $("#MbRecord_first_name").parents('.note-form').addClass('error-text');
        //    $("<div class='tips-box'>Заполните поле</div>").appendTo($("#MbRecord_first_name").parents(".note-form"));
        //    checked=false;
        //    $("html,body").animate({"scrollTop":230},200);
        //}

        if ($("#MbRecord_name").val() == ""){
            $("#MbRecord_name").parents('.note-form').addClass('error-text');
            $("<div class='tips-box'>Заполните поле</div>").appendTo($("#MbRecord_name").parents(".note-form"));
            checked=false;
            $("html,body").animate({"scrollTop":230},200); 
        } else if ($(".select-status select").val() == 5 && $("#MbRecord_name").val().split(" ").length < 3 && -1 < $("#clinic_name_auto").val().toLowerCase().indexOf('медси')) {
            console.log(1);
            $("<div class='tips-box'>Заполните ФИО</div>").appendTo($("#MbRecord_name").parents(".note-form"));
            checked=false;
            $("html,body").animate({"scrollTop":230},200);
        }
        if ($("#phone").val() == ""){
            $("#phone").parents('.note-form').addClass('error-text');
            $("<div class='tips-box'>Заполните поле</div>").appendTo($("#phone").parents(".note-form"));
            checked=false;
            $("html,body").animate({"scrollTop":230},200); 
        }
        if ($("#timeContainer").val() == ""){
            $(".time1").addClass('error-text');
            $("<div class='tips-box'>Заполните поле</div>").appendTo($(".time1"));
            checked=false;
            $("html,body").animate({"scrollTop":230},200); 
        }
        if ($(".select-new-project select").val() === ""){
            $(".select-new-project select").parents('.note-form').addClass('error-text');
            $("<div class='tips-box'>Заполните поле</div>").appendTo($(".select-new-project"));
            checked=false;
            $("html,body").animate({"scrollTop":230},200); 
        }
        if ($(".select-status select").val() === ""){
            $(".select-status select").parents('.note-form').addClass('error-text');
            $("<div class='tips-box'>Заполните поле</div>").appendTo($(".select-status"));
            checked=false;
            $("html,body").animate({"scrollTop":230},200); 
        }

        if ($(".select-new-record-type select").val() === ""){
            $(".select-new-record-type select").parents('.note-form').addClass('error-text');
            $("<div class='tips-box'>Заполните поле</div>").appendTo($(".select-new-record-type"));
            checked=false;
            $("html,body").animate({"scrollTop":230},200);
        }

        if( $('#MbRecord_status :selected').text() == 'Был на приеме' || $('#MbRecord_status :selected').text() == 'Не явился' ){
           if($('#MbRecord_clinic_id').val() === '0' && $('#MbRecord_none_service').attr('checked') !== 'checked'){
               $('#MbRecord_clinic_id').parents('.row').addClass('error-text');
               $('.clinic_error').show();
               checked=false;
               $("html,body").animate({"scrollTop":230},200);
           }
           
            if ($("#clinic_name_auto_id").val() == ""){
                alert('Такой клиники не существует у этого домена');
                checked=false;
            }

	        if ($(".services_select:first").val() === "0" && $('#MbRecord_none_service').attr('checked') !== 'checked'){
		        $(".services_select:first").parent().addClass('error-text');
		        $("<div class='tips-box'>Выберите услугу</div>").appendTo( $(".services_select:first").parent());
		        checked=false;
		        $("html,body").animate({"scrollTop":230},200);
	        }

            if ($(".services_select:first").val() === "0" && $('#MbRecord_none_service').attr('checked') !== 'checked'){
                $(".services_select:first").parent().addClass('error-text');
                $("<div class='tips-box'>Выберите услугу</div>").appendTo( $(".services_select:first").parent());
                checked=false;
                $("html,body").animate({"scrollTop":230},200);
            }
        }
        if(!checked){
            return false;
        }
    });
    
        
    $("body").on("click", ".add-comment", function() {
        $(".hid").slideDown(400);
        $(this).hide();
    });
    $('.text-phone input, .text-email input, .datepicker-box input, .text-uid input').focusin(function() {
        $(this).siblings('label').addClass('active-label')
    });
    $('.text-phone input, .text-email input, .datepicker-box input, .text-uid input').focusout(function() {
        $(this).siblings('label').removeClass('active-label')
    });
    $('body').on('click', '.ico-delete', function() {
        $(this).parent().remove();
    });
    $('body').on('keydown', '.table-api-main input', function(e) {
        if ($(this).parents('td').find('div').hasClass('text-inner') === true) {
        } else {
            $(this).parents('div').addClass('text-inner')
        }
    });
    $('body').on('change', '.table-api-main input', function(e) {
        if ($(this).parents('td').find('div').hasClass('text-inner') === true) {
        } else {
            $(this).parents('div').addClass('text-inner')
        }
    });
    // gender
    $("body").on("change", "#MbRecord_gender", function(){
        if($(this).val() == 1){
            $(".woman").removeClass("active")
            $(".man").addClass("active")
        }
        else if($(this).val() == 2){
            $(".man").removeClass("active")
            $(".woman").addClass("active")
        }
        else{
            $(".woman").removeClass("active")
            $(".man").removeClass("active")
        }
    });
        $("body").on("click", ".select-new label", function(){
        if($(this).hasClass("man")){
            $(".woman").removeClass("active");
            $(this).addClass("active");
            $("#MbRecord_gender option").eq(2).attr("checked", false);
            $("#MbRecord_gender option").eq(1).attr("checked", "checked");
            $("#MbRecord_gender_chosen .chosen-single span").text("мужчина");
        }
        else if($(this).hasClass("woman")){
            $(".man").removeClass("active");
            $(this).addClass("active");
            $("#MbRecord_gender option").eq(1).attr("checked", false);
            $("#MbRecord_gender option").eq(2).attr("checked", "checked");
            $("#MbRecord_gender_chosen .chosen-single span").text("женщина");
        }
        
    })
    $('body').on('focusout', '.table-api-main input', function(e) {
        $(this).parent().removeClass('text-inner');
    });
    $('body').on('click', '.today', function() {
        $('.pmu-button').removeClass('pmu-selected');
        $('.pmu-button').css({'background': 'none'});
        $('.pmu-today').prev().css({"color":"#000"});
        $('.pmu-today').css({'background': '#0F70B3', "color":"#fff"}).addClass('pmu-selected');
        $('.pmu-not-in-month').removeClass('pmu-selected');
    });
    $('body').on('click', '.yesterday', function() {
        $('.pmu-button').removeClass('pmu-selected');
        $('.pmu-button').css({'background': 'none'});
        $('.pmu-today').css({"color":"#000"});
        $('.pmu-today').prev().css({'background': '#0F70B3', "color":"#fff"}).addClass('pmu-selected');
        $('.pmu-not-in-month').removeClass('pmu-selected');

    });

    $('body').on('click', '.week', function() {
        $('.pmu-button').removeClass('pmu-selected');
        $('.pmu-today').prev().css({"color":"#000"});
        $('.pmu-today').css({"color":"#000"});
        $('.pmu-button').css({'background': 'none'});
        $('.pmu-today').css({'background': '#0F70B3', "color":"#fff"}).addClass('pmu-selected');
        $('.pmu-today').prev().css({'background': '#0F70B3', "color":"#fff"}).addClass('pmu-selected');
        $('.pmu-today').prev().prev().css({'background': '#0F70B3', "color":"#fff"}).addClass('pmu-selected');
        $('.pmu-today').prev().prev().prev().css({'background': '#0F70B3', "color":"#fff"}).addClass('pmu-selected');
        $('.pmu-today').prev().prev().prev().prev().css({'background': '#0F70B3', "color":"#fff"}).addClass('pmu-selected');
        $('.pmu-today').prev().prev().prev().prev().prev().css({'background': '#0F70B3', "color":"#fff"}).addClass('pmu-selected');
        $('.pmu-today').prev().prev().prev().prev().prev().prev().css({'background': '#0F70B3', "color":"#fff"}).addClass('pmu-selected');
    });
    $('.calendar-box .calendar').on('click', function() {
        if ($('.wrapper-calendars').css('display') === "none") {
            $('.wrapper-calendars').slideDown(300);
        } else {
            $('.wrapper-calendars').slideUp(300);
        }
    });
    $('body').on('click', '.top-calendars .left span', function() {
        $('.top-calendars .left span').removeClass('active');
        $(this).addClass('active');
    });
    $('body').on('click', '.top-calendars .right span', function() {
        $('.top-calendars .right span').removeClass('active');
        $(this).addClass('active');
    });
    // click for filter buttons
    $('body').on('click', '.call', function() {
        $('input[name="fromsite"]').val('0');
        $('input[name="fromcall"]').val('0');
        $('input[name="feedback"]').val('1');
        $('input[name="house"]').val('0');
        var url= $(this).attr('data-href') + "&" + $('.hidden-form').serialize();
        $.ajax({
            url: url,
            type: 'POST',
            data: "sort-call",
            dataType: "json",
            success: function(r) {
                if (r.tbody) {
                    $("#admin-tbody").html(r.tbody);
                    if (r.pages) {
                        $('#pages').html(r.pages);
                    } else {
                        $('#pages').html('');
                    }
                } else {
                    $("#admin-tbody").html('');
                    $('#pages').html('');
                }
                if(r.count){
                    $("#summary").html(r.count);
                }
                if (r.filter) {
                    updateFilter(r.filter);
                }
            }
        });
    });
    $('body').on('click', '.application-site', function() {
        $('input[name="fromsite"]').val('1');
        $('input[name="fromcall"]').val('0');
        $('input[name="feedback"]').val('0');
        $('input[name="house"]').val('0');
        var url= $(this).attr('data-href') + "&" + $('.hidden-form').serialize();
        $.ajax({
            url: url,
            type: 'POST',
            data: "sort-application-site",
            dataType: "json",
            success: function(r) {
                if (r.tbody) {
                    $("#admin-tbody").html(r.tbody);
                    if (r.pages) {
                        $('#pages').html(r.pages);
                    } else {
                        $('#pages').html('');
                    }
                } else {
                    $("#admin-tbody").html('');
                    $('#pages').html('');
                }
                if(r.count){
                    $("#summary").html(r.count);
                }
                if (r.filter) {
                    updateFilter(r.filter);
                }
            }
        });
    });
    $('body').on('click', '.feedback', function() {
        $('input[name="fromsite"]').val('0');
        $('input[name="fromcall"]').val('1');
        $('input[name="feedback"]').val('0');
        $('input[name="house"]').val('0');
        var url= $(this).attr('data-href') + "&" + $('.hidden-form').serialize();
        $.ajax({
            url: url,
            type: 'POST',
            data: "sort-feedback",
            dataType: "json",
            success: function(r) {
                if (r.tbody) {
                    $("#admin-tbody").html(r.tbody);
                    if (r.pages) {
                        $('#pages').html(r.pages);
                    } else {
                        $('#pages').html('');
                    }
                } else {
                    $("#admin-tbody").html('');
                    $('#pages').html('');
                }
                if(r.count){
                    $("#summary").html(r.count);
                }
                if (r.filter) {
                    updateFilter(r.filter);
                }
            }
        });
    });
    
    $('body').on('click', '.house', function() {
        $('input[name="fromsite"]').val('0');
        $('input[name="fromcall"]').val('0');
        $('input[name="feedback"]').val('0');
        $('input[name="house"]').val('1');
        var url= $(this).attr('data-href') + "&" + $('.hidden-form').serialize();
        $.ajax({
            url: url,
            type: 'POST',
            data: "sort-feedback",
            dataType: "json",
            success: function(r) {
                if (r.tbody) {
                    $("#admin-tbody").html(r.tbody);
                    if (r.pages) {
                        $('#pages').html(r.pages);
                    } else {
                        $('#pages').html('');
                    }
                } else {
                    $("#admin-tbody").html('');
                    $('#pages').html('');
                }
                if(r.count){
                    $("#summary").html(r.count);
                }
                if (r.filter) {
                    updateFilter(r.filter);
                }
            }
        });
    });
    
    $('body').on('click','.name-link',function(){
        $('#filter_author_id').val($(this).text()).keyup();
        return false;
    });
    
    $('body').on('click','.status-application div',function(){
        $(this).parents('form').find('input[type="text"]').val('').keyup();
    });
    
    /**
     * @name {ExelRequest} 
     * запрос для генерации Exel файла 
     * @param {int}fields - поля из таблицы 
     * @param {string}condition - условия выборки
     * */
     $('body').on('click','.import_exel',function(){
        var fields = $('.exel_form').serializeArray();
        var condition = $('.hidden-form').serializeArray();
        $.ajax({
             url: $(this).attr('data-href'),
             type: 'POST',
             data: {fields:fields,condition:condition},
             dataType: "json",
             success: function(r) {
                if(r.href){
                   $('.exel_form').append(r.href);
                }
             }
         });
       return false;
       
    });
    
    
    $('body').on('click','.exel_download',function(){
        $(this).remove();
    })
    /**
     * @name {PhoneCheked} 
     * запрос для проверки записи
     * @param {string}telephone - телефон 
     * */
    $('body').on('blur','#phone',function(){
        if($(this).val().indexOf('_') === -1){
            $.ajax({
             url: $(this).attr('data-href'),
             type: 'POST',
             data: {phone:$(this).val()},
             dataType: "json",
             success: function(r) {
               if(r){
                   alert('Заявка с таким телефоном была создана '+r.time);
               }
             }
         });
        }
    });

    $('body').on('blur','#phone_record',function(){
        if($(this).val().indexOf('_') === -1){
            $.ajax({
                url: $(this).attr('data-href'),
                type: 'POST',
                data: {phone:$(this).val()},
                dataType: "json",
                success: function(r) {
                    if(r){
                        alert('Заявка с таким телефоном была создана '+r.time);
                    }
                }
            });
        }
    });
    setInterval(function(){ //фича для показа числа ожидающих вызовов
        //периодический опрос сервиса, отдающего {"count":44,"duration":"23.5"} - число ожидающих и макс. время ожидания
        var $callsWait = $('.header-user-box .callsWait');
        if($callsWait[0])
            $.ajax({
                url: '/pamiClient/queueStatistic'
                ,dataType:'json'
            })
            .done(function(dat){
                console.log('Число ожидающих вызовов: ', dat); //TODO ? Если ожидающих нет, не выводить кнопку?
                $('.waits', $callsWait).html(dat.count);
                $('.waitTimeMax', $callsWait).html(dat.duration);
            });
    }, 1200);
});

function updateFilter(filter) {
    $('.hidden-block').html(filter);
    $('.table-api-main thead td').each(function(i, val) {
        $(this).find('a').attr('href', $('.' + $(this).attr('class') + '_2').find('a').attr('href'));
    });
}


/*! iCheck v1.0.2 by Damir Sultanov, http://git.io/arlzeA, MIT Licensed */
(function(f){function A(a,b,d){var c=a[0],g=/er/.test(d)?_indeterminate:/bl/.test(d)?n:k,e=d==_update?{checked:c[k],disabled:c[n],indeterminate:"true"==a.attr(_indeterminate)||"false"==a.attr(_determinate)}:c[g];if(/^(ch|di|in)/.test(d)&&!e)x(a,g);else if(/^(un|en|de)/.test(d)&&e)q(a,g);else if(d==_update)for(var f in e)e[f]?x(a,f,!0):q(a,f,!0);else if(!b||"toggle"==d){if(!b)a[_callback]("ifClicked");e?c[_type]!==r&&q(a,g):x(a,g)}}function x(a,b,d){var c=a[0],g=a.parent(),e=b==k,u=b==_indeterminate,
    v=b==n,s=u?_determinate:e?y:"enabled",F=l(a,s+t(c[_type])),B=l(a,b+t(c[_type]));if(!0!==c[b]){if(!d&&b==k&&c[_type]==r&&c.name){var w=a.closest("form"),p='input[name="'+c.name+'"]',p=w.length?w.find(p):f(p);p.each(function(){this!==c&&f(this).data(m)&&q(f(this),b)})}u?(c[b]=!0,c[k]&&q(a,k,"force")):(d||(c[b]=!0),e&&c[_indeterminate]&&q(a,_indeterminate,!1));D(a,e,b,d)}c[n]&&l(a,_cursor,!0)&&g.find("."+C).css(_cursor,"default");g[_add](B||l(a,b)||"");g.attr("role")&&!u&&g.attr("aria-"+(v?n:k),"true");
    g[_remove](F||l(a,s)||"")}function q(a,b,d){var c=a[0],g=a.parent(),e=b==k,f=b==_indeterminate,m=b==n,s=f?_determinate:e?y:"enabled",q=l(a,s+t(c[_type])),r=l(a,b+t(c[_type]));if(!1!==c[b]){if(f||!d||"force"==d)c[b]=!1;D(a,e,s,d)}!c[n]&&l(a,_cursor,!0)&&g.find("."+C).css(_cursor,"pointer");g[_remove](r||l(a,b)||"");g.attr("role")&&!f&&g.attr("aria-"+(m?n:k),"false");g[_add](q||l(a,s)||"")}function E(a,b){if(a.data(m)){a.parent().html(a.attr("style",a.data(m).s||""));if(b)a[_callback](b);a.off(".i").unwrap();
    f(_label+'[for="'+a[0].id+'"]').add(a.closest(_label)).off(".i")}}function l(a,b,f){if(a.data(m))return a.data(m).o[b+(f?"":"Class")]}function t(a){return a.charAt(0).toUpperCase()+a.slice(1)}function D(a,b,f,c){if(!c){if(b)a[_callback]("ifToggled");a[_callback]("ifChanged")[_callback]("if"+t(f))}}var m="iCheck",C=m+"-helper",r="radio",k="checked",y="un"+k,n="disabled";_determinate="determinate";_indeterminate="in"+_determinate;_update="update";_type="type";_click="click";_touch="touchbegin.i touchend.i";
    _add="addClass";_remove="removeClass";_callback="trigger";_label="label";_cursor="cursor";_mobile=/ipad|iphone|ipod|android|blackberry|windows phone|opera mini|silk/i.test(navigator.userAgent);f.fn[m]=function(a,b){var d='input[type="checkbox"], input[type="'+r+'"]',c=f(),g=function(a){a.each(function(){var a=f(this);c=a.is(d)?c.add(a):c.add(a.find(d))})};if(/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(a))return a=a.toLowerCase(),g(this),c.each(function(){var c=
        f(this);"destroy"==a?E(c,"ifDestroyed"):A(c,!0,a);f.isFunction(b)&&b()});if("object"!=typeof a&&a)return this;var e=f.extend({checkedClass:k,disabledClass:n,indeterminateClass:_indeterminate,labelHover:!0},a),l=e.handle,v=e.hoverClass||"hover",s=e.focusClass||"focus",t=e.activeClass||"active",B=!!e.labelHover,w=e.labelHoverClass||"hover",p=(""+e.increaseArea).replace("%","")|0;if("checkbox"==l||l==r)d='input[type="'+l+'"]';-50>p&&(p=-50);g(this);return c.each(function(){var a=f(this);E(a);var c=this,
        b=c.id,g=-p+"%",d=100+2*p+"%",d={position:"absolute",top:g,left:g,display:"block",width:d,height:d,margin:0,padding:0,background:"#fff",border:0,opacity:0},g=_mobile?{position:"absolute",visibility:"hidden"}:p?d:{position:"absolute",opacity:0},l="checkbox"==c[_type]?e.checkboxClass||"icheckbox":e.radioClass||"i"+r,z=f(_label+'[for="'+b+'"]').add(a.closest(_label)),u=!!e.aria,y=m+"-"+Math.random().toString(36).substr(2,6),h='<div class="'+l+'" '+(u?'role="'+c[_type]+'" ':"");u&&z.each(function(){h+=
        'aria-labelledby="';this.id?h+=this.id:(this.id=y,h+=y);h+='"'});h=a.wrap(h+"/>")[_callback]("ifCreated").parent().append(e.insert);d=f('<ins class="'+C+'"/>').css(d).appendTo(h);a.data(m,{o:e,s:a.attr("style")}).css(g);e.inheritClass&&h[_add](c.className||"");e.inheritID&&b&&h.attr("id",m+"-"+b);"static"==h.css("position")&&h.css("position","relative");A(a,!0,_update);if(z.length)z.on(_click+".i mouseover.i mouseout.i "+_touch,function(b){var d=b[_type],e=f(this);if(!c[n]){if(d==_click){if(f(b.target).is("a"))return;
        A(a,!1,!0)}else B&&(/ut|nd/.test(d)?(h[_remove](v),e[_remove](w)):(h[_add](v),e[_add](w)));if(_mobile)b.stopPropagation();else return!1}});a.on(_click+".i focus.i blur.i keyup.i keydown.i keypress.i",function(b){var d=b[_type];b=b.keyCode;if(d==_click)return!1;if("keydown"==d&&32==b)return c[_type]==r&&c[k]||(c[k]?q(a,k):x(a,k)),!1;if("keyup"==d&&c[_type]==r)!c[k]&&x(a,k);else if(/us|ur/.test(d))h["blur"==d?_remove:_add](s)});d.on(_click+" mousedown mouseup mouseover mouseout "+_touch,function(b){var d=
        b[_type],e=/wn|up/.test(d)?t:v;if(!c[n]){if(d==_click)A(a,!1,!0);else{if(/wn|er|in/.test(d))h[_add](e);else h[_remove](e+" "+t);if(z.length&&B&&e==v)z[/ut|nd/.test(d)?_remove:_add](w)}if(_mobile)b.stopPropagation();else return!1}})})}})(window.jQuery||window.Zepto);
