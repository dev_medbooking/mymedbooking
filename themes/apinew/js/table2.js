function ajaxAutocomplete() {
    $('input.autocomplete').each(function(i, el) {
        var host = $('#' + $(el).attr("data-host") + ' option:selected').val();
        var url = $(el).attr("data-href");
        $(el).autocomplete({
            minChars: 2,
            deferRequestBy: 300,
            source: function(request, response) {
                $.ajax({
                    url: url,
                    dataType: "json",
                    data: {term: request.term, host_id: host},
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.title,
                                value: item.title,
                                id: item.id
                            }
                        }));
                    }
                });
            },
            select: function(event, ui) {
                if (ui.item) {
                    $("#" + $(el).attr("data-aid")).val(ui.item.id);
                }
            }
        });
    });
}
function ajaxSend() {
    $("#loader").show();
    $.ajax({
        url: $("#admin-admin").attr("data-href"),
        type: 'get',
        dataType: 'json',
        success: function(r) {
            $("#admin-tbody").html('');
            $(".pagination").html('');
            if (r.tbody) {
                $("#admin-tbody").html(r.tbody);
                ajaxAutocomplete();
            }
            if (r.pages) {
                $(".pagination").html(r.pages);
            }
            if(r.count){
                $("#summary").html(r.count);
            }
            $("#loader").hide();
        },
        beforeSend: function(xhr) {
            $("#loader").show();
        }
    }).done(function(msg) {
        $("#loader").hide();
    });
}
function ajaxPageSend(href) {
    $("#loader").show();
    $.ajax({
        url: href,
        type: 'get',
        dataType: 'json',
        success: function(r) {
            $("#admin-tbody").html('');
            $(".pagination").html('');
            if (r.tbody) {
                $("#admin-tbody").html(r.tbody);
                ajaxAutocomplete();
            }
            if (r.pages) {
                $(".pagination").html(r.pages);
            }
            if(r.count){
                $("#summary").html(r.count);
            }
            $("#loader").hide();
        },
        beforeSend: function(xhr) {
            $("#loader").show();
        }
    }).done(function(msg) {
        $("#loader").hide();
    });
}
function ajaxSearch() {
    var list = {};
    if ($("#search-free").val()) {
        $("#search-free").each(function(index) {
            var key = $(this).attr("name");
            list[key] = $(this).attr("value");
        });
    } else {
        $(".search").each(function(index) {
            var key = $(this).attr("name");
            list[key] = $(this).attr("value");
        });
        $(".search-single").each(function(index) {
            var key = $(this).attr("name");
            list[key] = $(this).attr("value");
        });
        $(".select").each(function(index) {
            var key = $(this).attr("name");
            list[key] = $(this).find("option:selected").val();
        });
    }
    $.ajax({
        url: $("#admin-admin").attr("data-href"),
        type: 'get',
        data: list,
        dataType: 'json',
        success: function(r) {
            $("#admin-tbody").html('');
            $(".pagination").html('');
            if (r.tbody) {
                $("#admin-tbody").html(r.tbody);
                ajaxAutocomplete();
            }
            if(r.count){
                $("#summary").html(r.count);
            }
            if (r.pages) {
                $(".pagination").html(r.pages);
            }
            $("#loader").hide();
        },
        beforeSend: function(xhr) {
            $("#loader").show();
        }
    }).done(function(msg) {
        $("#loader").hide();
    });
}
function ajaxSort(href) {
    var list = {};
    $(".search").each(function(index) {
        var key = $(this).attr("name");
        list[key] = $(this).attr("value");
    });
    $.ajax({
        url: href,
        type: 'get',
        data: list,
        dataType: 'json',
        success: function(r) {
            $("#admin-tbody").html('');
            $(".pagination").html('');
            if (r.tbody) {
                $("#admin-tbody").html(r.tbody);
                ajaxAutocomplete();
            }
            if(r.count){
                $("#summary").html(r.count);
            }
            if (r.pages) {
                $(".pagination").html(r.pages);
            }
            if (r.filter) {
                console.log(r.filter);
                updateFilter(r.filter);
            }
            $("#loader").hide();
        },
        beforeSend: function(xhr) {
            $("#loader").show();
        }
    }).done(function(msg) {
        $("#loader").hide();
    });
}
$(document).ready(function() {
    block = 1;
    ajaxAutocomplete();
    $('body').on('keyup', '.search', function() {
        $('#search-free').val('');
        var term = $(this).val();
        if (term.length == 0 || term.length > 2) {
            ajaxSearch();
        }
        return false;
    });
    $('body').on('keyup', '#search-free', function() {
        var term = $(this).val();
        if (term.length == 0 || term.length > 2) {
            ajaxSearch();
        }
        return false;
    });
    $('body').on('keyup', '.search-single', function() {
        $('#search-free').val('');
        ajaxSearch();
        return false;
    });
    $('body').on('change', '.select', function() {
        $('#search-free').val('');
        ajaxSearch();
        return false;
    });
    $('body').on('click', '.page', function() {
        var href = $(this).find('a').attr('href');
        ajaxPageSend(href);
        return false;
    });
    $('body').on('click', '.js-update-first-time-status', function() {
        var href = $(this).attr('data-href');
        var self = $(this);
        $.ajax({
            url: href,
            type: 'get',
            dataType: 'json',
            success: function(r) {
                if (r.success) {
                    self.hide();
                }
            }
        });
        return false;
    });
	$('body').on('click', '.js-excel-export-call', function() {
		var href = $(this).attr('data-href');
		var dateEnd = $('[name=create_time_from]').val();
		var dateStart = $('[name=create_time_to]').val();
		window.open(href+'?dateStart='+dateStart+'&dateEnd='+dateEnd, '_blank');
		return false;
	});
    $('body').on('click', '.js-show-history', function() {
        $(this).next().toggle();
        return false;
    });
    $('body').on('click', '#add-metro', function() {
        var id = block++;
        var fields = $('.new-block').html();
        $('.more-metro').prepend('<div class="subway-block-new-' + id + '">' + fields + '<input type="button" value="удалить" data-id="' + id + '" class="del-metro-new"></div>');
        $('.subway-block-new-' + id).find('#subway_name').attr('id', 'new-' + id).attr('name', 'AdminClinic[subway_array]["new' + id + '"][title]');
        $('.subway-block-new-' + id).find('#min_man').attr('id', 'new-min_man' + id).attr('name', 'AdminClinic[subway_array]["new' + id + '"][lin1]');
        $('.subway-block-new-' + id).find('#min_car').attr('id', 'new-min_car' + id).attr('name', 'AdminClinic[subway_array]["new' + id + '"][lin2]');
        $('input.autocomplete-subway').each(function(i, el) {
            var host = $('#' + $(el).attr("data-host") + ' option:selected').val();
            var url = $(el).attr("data-href");
            $(el).autocomplete({
                minChars: 2,
                deferRequestBy: 300,
                source: function(request, response) {
                    $.ajax({
                        url: url,
                        dataType: "json",
                        data: {term: request.term.split(",").slice(-1)[0], host_id: host},
                        success: function(data) {
                            response($.map(data, function(item) {
                                return {
                                    label: item.title,
                                    value: item.title,
                                    id: item.id
                                }
                            }));
                        }
                    });
                }
            });
        });
    });
    $('body').on('click', '.del-metro-new', function() {
        $('.subway-block-new-' + $(this).attr('data-id')).remove();
    });
    $('body').on('click', '.del-metro', function() {
        $('.subway-block-' + $(this).attr('data-id')).remove();
    });
    $('input.autocomplete-subway').each(function(i, el) {
        var host = $('#' + $(el).attr("data-host") + ' option:selected').val();
        var url = $(el).attr("data-href");
        $(el).autocomplete({
            minChars: 2,
            deferRequestBy: 300,
            source: function(request, response) {
                $.ajax({
                    url: url,
                    dataType: "json",
                    data: {term: request.term.split(",").slice(-1)[0], host_id: host},
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.title,
                                value: item.title,
                                id: item.id
                            }
                        }));
                    }
                });
            }
        });
    });
    $('body').on('click', '.fast-change', function() {
        ajaxAutocomplete();
        $(".visihid[data-hid=" + $(this).attr("data-id") + "]").hide();
        $(".hid[data-hid=" + $(this).attr("data-id") + "]").show();
        return false;
    });
    $('body').on('click', '.unedit', function() {
        $(".hid[data-hid=" + $(this).attr("data-id") + "]").hide();
        $(".visihid[data-hid=" + $(this).attr("data-id") + "]").show();
        return false;
    });
    $('body').on('click', '.csorting', function() {
        ajaxSort($(this).attr('href'));
        return false;
    });
    $('body').on('click', '.update', function() {
        $('.error').empty();
        var list = {};
        var id = $(this).attr("data-id");
        $("[data-model=" + $(this).attr("data-id") + "]").each(function(index) {
            var key = $(this).attr("name");
            list[key] = $(this).attr("value");
        });
        $.ajax({
            url: $(this).attr("href"),
            type: 'post',
            dataType: 'json',
            data: list,
            success: function(r) {
                if (r.message == 1) {
                    $(':input', '#create-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                    $("#tr-item-" + id).html(r.data);
                } else {
                    if (r != null && typeof r == 'object') {
                        $.each(r, function(key, value) {
                            $('#' + key + "_" + id).next().append('<p>' + value + '</p>');
                        });
                    }
                }
            }
        });
        return false;
    });
    $('body').on('click', '.delete', function() {
        if (confirm('Продолжить удаление?')) {
            $.ajax({
                url: $(this).attr("href"),
                type: 'post',
                dataType: 'json',
                success: function(r) {
                    if (r.message == '1') {
                        ajaxSend();
                    }
                }
            });
        }
        return false;
    });
    $('body').on('click', '.ico-delete a', function() {
        var self = $(this);
        var date = $('.dn2').text();
        if (confirm('Продолжить удаление?')) {
            $.ajax({
                url: $(this).attr("href"),
                type: 'post',
                dataType: 'json',
                success: function(r) {
                    if (r.status == '1' && r.url) {
                        if(date.length>1){
                            self.parents('tr').remove();
                        }else{
                            window.location=r.url;  
                        }
                    }
                }
            });
        }
        return false;
    });
});
function updateFilter(filter) {
    $('.hidden-block').html(filter);
    $('.table-api-main thead td').each(function(i, val) {
        $(this).find('a').attr('href', $('.' + $(this).attr('class') + '_2').find('a').attr('href'));
    });
}
var client = new ZeroClipboard( $(".js-copy-phone a"));
$("body").on("click", ".js-copy-phone a", function(){
    var that = $(this);
     client.on( 'ready', function(event) {
        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', that.text());
        } );
    } );
    return false;
})




