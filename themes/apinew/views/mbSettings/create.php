<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/table2.js',CClientScript::POS_END);?>
<?php $this->renderPartial('navbar'); ?>
<div class="row-fluid" id="form-ajax">
    <?php $this->renderPartial('_createForm',array('model'=>$model)); ?>
</div>