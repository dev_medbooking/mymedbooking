<div id="create">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'create-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'form-horizontal'),
    )); ?>
        <?php if($model->errors): ?>
        <div class="well">
            <?php echo $form->errorSummary($model); ?>
        </div>
        <?php endif; ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'oktell',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'oktell',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="form-actions">
            <?php echo CHtml::submitButton("Сохранить",array("class"=>"btn btn-info"));?>
        </div>
    <?php $this->endWidget(); ?>
</div>