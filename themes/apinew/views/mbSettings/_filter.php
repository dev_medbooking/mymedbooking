<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id', null, array('class' => 'csorting')); ?></td>
        <td class="time_start<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('oktell', null, array('class' => 'csorting')); ?></td>
        <td class="edit"></td>
        <td class="view"></td>
        <td class="delete"></td>
        <td class="icon-share"></td>
    </tr>
    <tr class="filters">
        <td><?php echo CHtml::activeTextField($model, 'id', array('class' => 'search-single', 'id' => 'filter_id')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'oktell', array('class' => 'search', 'id' => 'filter_oktell')); ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</thead>