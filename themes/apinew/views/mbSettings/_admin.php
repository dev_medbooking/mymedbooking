<div class="grid-view" id="admin-admin" data-href="<?php echo Yii::app()->createUrl("/".Yii::app()->controller->id."/admin",array('page'=>(!empty($_REQUEST['page'])?$_REQUEST['page']:'1'))); ?>">
    <div class="summary" id="summary">
        <?php  $this->renderPartial('//layouts/_admin_count',array('dataCount'=>$dataCount,'count'=>$count)); ?>
    </div>
    <div class="hidden-block" style="display: none"></div>
    <table class="table table-striped table-bordered table-hover table-api-main">
        <?php $this->renderPartial('_filter',array('sort'=>$sort,'model'=>$model,'status'=>'1')); ?>
        <tbody id="admin-tbody">
            <?php $this->renderPartial('//layouts/_admin_tbody',array('data'=>$data)); ?>
        </tbody>
    </table>
    <?php if(!empty($pages)&&!empty($count)): ?>
    <div class="pagination row">
        <?php $this->renderPartial('//layouts/_admin_pages',array('sort'=>$sort,'pages'=>$pages,'count'=>$count)); ?>
    </div>
    <?php endif; ?>
</div>