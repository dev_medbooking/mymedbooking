<?php if(empty($type)): ?>
<tr id="tr-item-<?php echo $model->id; ?>" class="edit" data-id="<?php echo $model->id; ?>">
<?php endif; ?>
    <td>
        <?php echo $model->id; ?>
    </td>
    <td>
        <div data-hid="<?php echo $model->id; ?>">
            <?php echo !empty($model->oktell)?$model->oktell:''; ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::activeTextField($model,'oktell',array('id'=>$this->contrModel.'_oktell_'.$model->id,'class'=>'span12','data-model'=>$model->id)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td class="ico-edit">
        <?php echo CHtml::link('',Yii::app()->createUrl('/'.Yii::app()->controller->id.'/createForm',array('id'=>$model->id)),array("data-href"=>Yii::app()->createUrl('/'.Yii::app()->controller->id.'/form',array('id'=>$model->id)),"class"=>'form','data-id'=>$model->id)); ?>
    </td>
    <td class="ico-view">
        <?php echo CHtml::link('',array('/'.Yii::app()->controller->id.'/view','id'=>$model->id),array('data-id'=>$model->id)); ?>
    </td>
    <td class="ico-delete">
        <?php echo CHtml::link('',array('/'.Yii::app()->controller->id.'/delete','id'=>$model->id),array('data-id'=>$model->id,'class'=>'delete')); ?>
    </td>
    <td>
        <div data-hid="<?php echo $model->id; ?>" class="visihid">
            <?php echo CHtml::link('<i class="icon-share"></i>',array('#','id'=>$model->id),array('data-id'=>$model->id,'class'=>'fast-change')); ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::link('<i class="icon-check-empty"></i>',"#",array("class"=>'unedit','data-id'=>$model->id)); ?>
            <?php echo CHtml::link('<i class="icon-pencil"></i>',array('/'.Yii::app()->controller->id.'/update','id'=>$model->id),array("class"=>'update','data-id'=>$model->id)); ?>
        </div>
    </td>
<?php if(empty($type)): ?>
</tr>
<?php endif; ?>