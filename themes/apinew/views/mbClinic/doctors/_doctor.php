<section class="items center_block doctor_list left">
    <?php if(!empty($data)): ?><?php $this->renderPartial('//clinic/doctors/_doctors_data_start',array('data'=>$data,'doctors'=>$doctors,'model'=>$model)); ?><?php endif; ?>
</section>
<aside class="right">
    <div id="category_link" class="right_block block_list category_link">
        <div class="aside_h3">Выберите специальность:</div>
        <form action="<?php echo Yii::app()->createUrl('/clinic/specialist',array('clinic_id'=>$model->id)); ?>" class="check-items-tab">
            <?php if(!empty($doctors)): ?><ul>
                <?php foreach($doctors as $value): ?>
                <?php if(!empty($value->speciality)): ?><li>
                    <input name="specialist[<?php echo $value->id; ?>]" type="checkbox" id="spec-<?php echo $value->id; ?>" value="<?php echo $value->speciality; ?>" />
                    <label for="spec-<?php echo $value->id; ?>" data-hash="<?php echo Translit::transliterate($value->speciality,'-'); ?>"><?php echo $value->speciality; ?></label>
                </li><?php endif; ?>
                <?php endforeach; ?>
            </ul><?php endif; ?>
        </form>
        <?php if(!empty($doctors)): ?><a href="#" class="empty-checkbox"><span>Очистить выбор</span></a><?php endif; ?>
    </div>
</aside>