<div class="photo_doctor_list">
    <a data-fio="<?php echo $model->fio; ?>" data-img="<?php if(!empty($model->image)){ ?>http://medbooking.com/<?php echo $model->getImagePath('photo'); ?><?php } else{  ?><?php if(!empty($model->gender)&&$model->gender==2){ ?>http://medbooking.com/img/no_img_woman.png<?php } else{ ?>http://medbooking.com/img/no_img_man.png<?php } ?><?php } ?>" data-href="/doctor/<?php echo $model->translit; ?>"></a>
    <?php if(!empty($model->startyear)): ?><strong class="experience"><?php echo Yii::t('app',"стаж {n} год|стаж {n} года|стаж {n} лет",array(date("Y")-$model->startyear)); ?></strong><?php endif; ?>
</div>
<a class="doctor_name_link link" href="/doctor/<?php echo $model->translit; ?>" itemprop="members"><?php echo !empty($model->lname)?$model->lname:''; ?> <?php echo !empty($model->fname)?$model->fname:''; ?> <?php echo !empty($model->sname)?$model->sname:''; ?></a>
<div class="doctor_category"><?php echo !empty($model->speciality)?$model->speciality:''; ?></div>
<?php if(!empty($model->child)): ?><?php if(!empty($model->doctor_clinic_single[0]->c->id)): ?><div class="doctor_child_check make-appointment" data-href="/childAjax/index" data-clinic="<?php echo $model->doctor_clinic_single[0]->c->id;?>" data-doctor="<?php echo $model->id; ?>">Принимает детей</div><?php endif; ?><?php endif; ?>
<?php if(!empty($model->nation)): ?><div class="state_doctor">Работает в гос. клинике</div><?php endif; ?>
<?php $this->renderPartial('//clinic/elements/_rate_doctor',array('model'=>$model)); ?>
<?php if(!empty($model->description)): ?><div class="doctor_description description clearfix">
    <?php echo Controller::cutString($model->description,400); ?>
</div><?php endif; ?>
<div class="doctor_order_block order_block clearfix">
    <span data-href="/ajax/index" data-clinic="<?php echo $clinic; ?>" data-doctor="<?php echo $model->id; ?>" data-gaq="doctor_list" class="make-appointment btn order_btn" >Записаться на прием</span>
        <?php if(!empty($this->category)): ?>
        <?php $_price=$model->firstPriceCategorySingle($this->category); if(!empty($_price->price)): ?>
        <?php if($_price->price!='00'):?>
        <span class="doctor_price">
            <span class="doctor_price_caption">Консультация специалиста</span>
            <?php if(!empty($_price->price_action)):?><span class="doctor_discount"><span class="new"><?php echo $_price->price_action=='00'?'бесплатно':$_price->price_action.' руб.'; ?></span> только на Medbooking</span><?php else:?><span class="doctor_price_value"><?php echo $_price->price?$_price->price.' Р':'&mdash;'; ?></span><?php endif;?>
        </span>
        <?php else:?>
        <span class="doctor_price">
            <span class="doctor_price_caption">Консультация специалиста</span>
            <span class="doctor_price_value">Бесплатно</span>
        </span>
        <?php endif;?>
        <?php endif; ?>
        <?php else: ?>
        <?php $_price=$model->firstPriceClinic($clinic); if(!empty($_price->price)): ?><?php if($_price->price!='00'):?>
        <span class="doctor_price"><span class="doctor_price_caption">Консультация специалиста</span>
	    <?php if(!empty($_price->price_action)):?><span class="doctor_discount"><span class="new"><?php echo $_price->price_action=='00'?'бесплатно':$_price->price_action.' руб.'; ?></span> только на Medbooking</span><?php else:?><span class="doctor_price_value"><?php echo $_price->price?$_price->price.' Р':'&mdash;'; ?></span><?php endif;?>
        </span><?php else:?>
        <span class="doctor_price">
            <span class="doctor_price_caption">Консультация специалиста</span>
            <span class="doctor_price_value">Бесплатно</span>
        </span><?php endif;?>
        <?php endif; ?>
        <?php endif; ?>
        <?php if(!empty($model->house)): ?><?php if(!empty($model->doctor_clinic_single[0]->c->id)): ?><span class="call_home_btn" data-href="/ajax/home" data-clinic="<?php echo $model->doctor_clinic_single[0]->c->id;?>" data-doctor="<?php echo $model->id; ?>" data-gaq="doctor_list">Вызвать на дом</span><?php endif; ?><?php endif; ?>
    <div class="doctor_order_block_note">
	<?php $this->renderPartial('//doctor/elements/_additionally',array('model'=>$model)); ?>    
    </div>
</div>
