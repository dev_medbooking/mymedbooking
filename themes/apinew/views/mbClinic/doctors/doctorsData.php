var dataDoctors = {
<?php foreach($data as $key=>$value): ?>
<?php if($value->d['status']!=2): ?>
        <?php echo $key; ?>: {
            id: <?php echo !empty($value->d->id)?$value->d->id:''; ?>,
            clinic_id: <?php echo !empty($value->clinic_id)?$value->clinic_id:''; ?>,
            lname: '<?php echo !empty($value->d->lname)?$value->d->lname:''; ?>',
            fname: '<?php echo !empty($value->d->fname)?$value->d->fname:''; ?> <?php echo !empty($value->d->sname)?$value->d->sname:''; ?>',
            src: 'http://medbooking.com/<?php echo $value->d->getImagePath('photo'); ?>',
            href: '/doctor/<?php echo $value->d->translit; ?>',
            speciality: '<?php echo !empty($value->d->speciality)?$value->d->speciality:''; ?>',
            class: 'rate<?php if(empty($value->d->comm)): ?> no-reviews<?php endif; ?> clearfix',
            number: '<?php echo str_replace(".",",",sprintf("%.2f",!empty($value->d->rate10)?round($value->d->rate10,2):0)); ?>',
            rate: 'stars rate_<?php echo str_replace(".",",",sprintf("%d",!empty($value->d->rate10)?round($value->d->rate10*10,1):0)); ?>',
            countSpan: '<?php echo !empty($value->d->comm)?$value->d->comm:'0'; ?>',
            startyear: '<?php echo Yii::t('app',"стаж {n} год|стаж {n} года|стаж {n} лет",array(date("Y")-$value->d->startyear)); ?>',
            description: '<?php echo CJSON::encode(Controller::cutString($value->d->description,500)); ?>'
        }<?php echo ($key+1)!=count($data)?',':''; ?>
<?php endif; ?>
<?php endforeach; ?>
};
<?php /* countReview: '<?php echo !empty($value->d->comm)?'/clinic/'.$value->d->translit."#bord":"#"; ?>', */ ?>
