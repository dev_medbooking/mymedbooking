<?php //if(!empty($model->status)&&$model->status==3): ?><!--<a class="photo_list_item" href="/--><?php //echo $model->translit; ?><!--">--><?php //$this->renderPartial('elements/_photo',array('model'=>$model)); ?><!--</a>--><?php //else: ?><!--<a class="photo_list_item" href="/clinic/--><?php //echo $model->translit; ?><!--">--><?php //$this->renderPartial('elements/_photo',array('model'=>$model)); ?><!--</a>--><?php //endif; ?>
<?php if(!empty($model->status)&&$model->status==3): ?><a class="clinik_name_link left" href="/<?php echo $model->translit; ?>"><?php echo $model->title; ?></a><?php else: ?><a class="clinik_name_link left" href="/clinic/<?php echo $model->translit; ?>"><?php echo $model->title; ?></a><?php endif; ?>
<?php if(!empty($model->network_id)): ?><a class="clinik_group_link left" href="/<?php echo $model->network->translit; ?>"><?php echo $model->network->title; ?></a><?php endif; ?>
<?php if(empty($model->nation)):?>
    <?php $this->renderPartial('elements/_rate',array('model'=>$model)); ?>
<?php endif;?>
<?php if(!empty($model->nation)): ?><div class="clinik_state left">Государственная клиника</div><?php endif; ?>
<div class="clinik_description description clearfix"><?php echo MbController::cutString(!empty($model->description)?$model->description:'',300); ?></div>
<div class="doctor_order_block order_block clearfix">
    <span class="make-appointment btn order_btn left" data-gaq="clinic_list" data-clinic="<?php echo $model->id; ?>" data-href="/ajax/index">Записаться на прием</span>
    <?php if(!empty($model->regime_byd)||!empty($model->regime_mon)||!empty($model->regime_tue)||!empty($model->regime_wed)||!empty($model->regime_thu)||!empty($model->regime_fri)||!empty($model->regime_sat)||!empty($model->regime_sun)): ?><div class="clinik_time left items">
        <span class="clinik_time_label">График работы:</span>
        <?php if(!empty($model->regime_byd)): ?><span class="clinik_time_item"><span>Пн-Пт &mdash; </span><span><?php echo $model->regime_byd; ?></span></span><?php else: ?>
        <?php if(!empty($model->regime_mon)): ?><span class="clinik_time_item"><span>Пн &mdash; </span><span><?php echo $model->regime_mon; ?></span></span><?php endif; ?>
        <?php if(!empty($model->regime_tue)): ?><span class="clinik_time_item"><span>Вт &mdash; </span><span><?php echo $model->regime_tue; ?></span></span><?php endif; ?>
        <?php if(!empty($model->regime_wed)): ?><span class="clinik_time_item"><span>Ср &mdash; </span><span><?php echo $model->regime_wed; ?></span></span><?php endif; ?>
        <?php if(!empty($model->regime_thu)): ?><span class="clinik_time_item"><span>Чт &mdash; </span><span><?php echo $model->regime_thu; ?></span></span><?php endif; ?>
        <?php if(!empty($model->regime_fri)): ?><span class="clinik_time_item"><span>Пт &mdash; </span><span><?php echo $model->regime_fri; ?></span></span><?php endif; ?><?php endif; ?>
        <?php if(!empty($model->regime_sat)): ?><span class="clinik_time_item"><span>Сб &mdash; </span><span><?php echo $model->regime_sat; ?></span></span><?php endif; ?>
        <?php if(!empty($model->regime_sun)): ?><span class="clinik_time_item sunday"><span>Вс &mdash; </span><span><?php echo $model->regime_sun; ?></span></span><?php endif; ?>
    </div><?php endif; ?>
</div>
<div class="clinik_name_adress clearfix">
    <div class="clinik_adress map-link left"><?php echo !empty($model->address)?$model->address:''; ?></div>
    <ul class="clinik_subway_link clinik_subway right">
        <?php if(empty($model->subways_data)): ?>
        <?php $this->toCache($model->id); ?>
        <?php if(!empty($model->clinic_subway_s)): ?><?php $this->renderPartial('//clinic/elements/_subway',array('data'=>$model->clinic_subway_s)); ?><?php endif; ?>
        <?php endif; ?>
        <?php if(!empty($model->subways_data)): $dhs=unserialize($model->subways_data); ?>
        <?php if(!empty($dhs)): $contr=Yii::app()->controller->id=='clinic'?'clinic':'doctor'; ?>
        <?php foreach($dhs as $vhs): ?>
        <li><?php if(!empty($this->category)): ?>
            <a class="col-color-<?php echo !empty($vhs['subway_line_id'])?$vhs['subway_line_id']:''; ?>" href="<?php echo Yii::app()->createUrl('/'.$contr.'/index',array('category'=>$this->category,'subway'=>$vhs['translit'])); ?>"><?php echo !empty($vhs['title'])?$vhs['title']:''; ?></a>
            <?php else: ?>
            <a class="col-color-<?php echo !empty($vhs['subway_line_id'])?$vhs['subway_line_id']:''; ?>" href="<?php echo Yii::app()->createUrl('/'.$contr.'/index',array('subway'=>$vhs['translit'])); ?>"><?php echo !empty($vhs['title'])?$vhs['title']:''; ?></a>
            <?php endif; ?>
            <span class="time_to">(<span class="foot"><?php echo !empty($vhs['lin1'])?$vhs['lin1']:' - '; ?></span> мин., <span class="car"><?php echo !empty($vhs['lin2'])?$vhs['lin2']:' - '; ?></span> мин.)</span>
        </li>
        <?php endforeach; ?>
        <?php endif; ?>
        <?php endif; ?>
    </ul>
</div>
<?php if(!empty($model->lat)&&!empty($model->lng)): ?><div class="map">
    <div style="width:660px;height:273px;" data-lat="<?php echo $model->lat; ?>" data-lng="<?php echo $model->lng; ?>" id="map-<?php echo $model->id; ?>" class="yandex-map-list"></div>
</div><?php endif; ?>
