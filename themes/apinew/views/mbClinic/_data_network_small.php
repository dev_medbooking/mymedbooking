<div class="clinik_name_adress item clearfix">
    <div class="branch_clinik_name_adress left clearfix">
        <a class="clinik_name_link left" href="/clinic/<?php echo $model->translit; ?>"><?php echo $model->title; ?></a>
        <div class="clinik_adress map-link left"><?php echo !empty($model->address)?$model->address:''; ?></div>
    </div>
    <div class="clinik_rate_comment_block right ">
        <?php if(!empty($model->rate10)): ?>
			<?php if($model->rate10 >= 10):?>
				<div class="rate clinic_rate <?php if(empty($model->comm)): ?> no_reviews<?php endif; ?>">10</div>
			<?php else:?>
				<div class="rate clinic_rate <?php if(empty($model->comm)): ?> no_reviews<?php endif; ?>"><?php echo str_replace(",",".",sprintf("%.1f",!empty($model->rate10)?round($model->rate10,2):0)); ?></div>
			<?php endif;?>
		<?php endif; ?>
        <?php if(!empty($model->comm)): ?>
            <a class="clinic_comment comment right reviews" href="/clinic/<?php echo $model->translit; ?>#comment-block"><strong><?php echo $model->comm; ?></strong></a>
        <?php endif; ?>
    </div>
    <ul class="clinik_subway_link clinik_subway right">
        <?php if(empty($model->subways_data)): ?>
        <?php $this->toCache($model->id); ?>
        <?php if(!empty($model->clinic_subway_s)): ?>
            <?php $this->renderPartial('elements/_subway',array('data'=>$model->clinic_subway_s)); ?>
        <?php endif; ?>
        <?php endif; ?>
        <?php if(!empty($model->subways_data)): $dhs=unserialize($model->subways_data); ?>
        <?php if(!empty($dhs)): $contr=Yii::app()->controller->id=='clinic'?'clinic':'doctor'; ?>
        <?php foreach($dhs as $vhs): ?><li>
            <?php if(!empty($this->category)): ?><a class="col-color-<?php echo !empty($vhs['subway_line_id'])?$vhs['subway_line_id']:''; ?>" href="<?php echo Yii::app()->createUrl('/'.$contr.'/index',array('category'=>$this->category,'subway'=>$vhs['translit'])); ?>">
                    <?php echo !empty($vhs['title'])?$vhs['title']:''; ?>
                </a><?php else: ?>
                <a class="col-color-<?php echo !empty($vhs['subway_line_id'])?$vhs['subway_line_id']:''; ?>" href="<?php echo Yii::app()->createUrl('/'.$contr.'/index',array('subway'=>$vhs['translit'])); ?>">
                    <?php echo !empty($vhs['title'])?$vhs['title']:''; ?>
                </a>
            <?php endif; ?>
            <span class="time_to">(<span class="foot"><?php echo !empty($vhs['lin1'])?$vhs['lin1']:' - '; ?></span> мин., <span class="car"><?php echo !empty($vhs['lin2'])?$vhs['lin2']:' - '; ?></span> мин.)</span>
        </li><?php endforeach; ?>
        <?php endif; ?>
        <?php endif; ?>
    </ul>
</div>
<div class="map">
    <div style="width:780px;height:273px;" data-lat="<?php echo !empty($model->lat)?$model->lat:''; ?>" data-lng="<?php echo !empty($model->lng)?$model->lng:''; ?>" id="map-<?php echo $model->id; ?>" class="yandex-map-list"></div>
</div>