<?php if(!empty($this->filterSubwayData)): ?><div id="subway_link" class="hide_aside right_block block_list">
    <div class="aside_h3"><span><?php echo Yii::app()->controller->id=='doctor'?'Врачи':'Клиники'; ?> рядом с метро</span></div>
    <ul>
        <?php foreach($this->filterSubwayData as $value): ?>
        <li><a href="<?php echo $value['url']; ?>"><?php echo $value['title']; ?></a></li>
        <?php endforeach; ?>
    </ul>
</div><?php endif; ?>
<?php /*if(!empty($this->filterDistrictData)): ?><div id="district_link" class="hide_aside right_block block_list">
    <h3><span><?php echo Yii::app()->controller->id=='doctor'?'Врачи':'Клиники'; ?> в районе</span></h3>
    <ul>
        <?php foreach($this->filterDistrictData as $value): ?>
        <li><a href="<?php echo $value['url']; ?>"><?php echo $value['title']; ?></a></li>
        <?php endforeach; ?>
    </ul>
</div><?php endif;*/ ?>
<?php /*if(!empty($this->filterSpecialityData)): ?><div id="disease_link" class="hide_aside right_block block_list">
    <h3><span>Заболевания</span></h3>
    <ul>
        <?php foreach($this->filterSpecialityData as $value): ?>
        <li><a href="<?php echo $value['url']; ?>"><?php echo $value['title']; ?></a></li>
        <?php endforeach; ?>
    </ul>
</div><?php endif;*/ ?>
