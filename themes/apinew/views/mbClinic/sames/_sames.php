<section class="clinik_similar_items similar_items clearfix">
    <div class="top_clinik_similar clearfix">Похожие клиники:</div>
    <?php foreach($data[0] as $key=>$model): ?>
    <div class="clinik_similar_item left">
        <?php if(!empty($model->status)&&$model->status==3): ?><a class="clinik_name_link" href="/<?php echo $model->translit; ?>"><?php echo $model->title; ?></a><?php else: ?><a class="clinik_name_link" href="/clinic/<?php echo $model->translit; ?>"><?php echo $model->title; ?></a><?php endif; ?>
        <div class="clinic_rate_comment_block">
            <?php if($model->rate10==10):?><div class="rate clinic_rate <?php if(empty($model->comm)): ?> no_reviews<?php endif; ?>">
                <meta itemprop="bestRating" content="10">
                <meta itemprop="ratingValue" content="<?php echo str_replace(".",",",sprintf("%.2f",!empty($model->rate10)?round($model->rate10,2):0)); ?>">
                <meta itemprop="ratingCount" content="<?php echo !empty($model->comm)?$model->comm:''; ?>" />
                <span class="rating_caption">рейтинг</span><?php echo $model->rate10;?></div>
            <?php else:?><div class="rate clinic_rate <?php if(empty($model->comm)): ?> no_reviews<?php endif; ?>">
                <meta itemprop="bestRating" content="10">
                <meta itemprop="ratingValue" content="<?php echo str_replace(".",",",sprintf("%.2f",!empty($model->rate10)?round($model->rate10,2):0)); ?>">
                <meta itemprop="ratingCount" content="<?php echo !empty($model->comm)?$model->comm:''; ?>" />
                <span class="rating_caption">рейтинг</span><?php  echo str_replace(",",".",sprintf("%.1f",!empty($model->rate10)?round($model->rate10,2):0)); ?></div>
            <?php endif;?>
            <?php if(!empty($model->comm)): ?>
                <?php if(!empty($model->status)&&$model->status==3): ?>
                    <a class="clinic_comment comment left reviews" href="/<?php echo $model->translit."#".'comment-block' ?>"<?php echo !empty($model->comm)?"":" onclick='return false;'"; ?>>
                        <strong><?php echo $model->comm; ?></strong> <?php echo Yii::t('app',"отзыв|отзыва|отзывов",array($model->comm)); ?>
                    </a>
                <?php else: ?>
                    <a class="clinic_comment comment left reviews" href="/clinic/<?php echo $model->translit."#".'comment-block'; ?>"<?php echo !empty($model->comm)?"":" onclick='return false;'"; ?>>
                        <strong><?php echo $model->comm; ?></strong> <?php echo Yii::t('app',"отзыв|отзыва|отзывов",array($model->comm)); ?>
                    </a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <?php if(!empty($model->address)): ?><div class="clinik_adress map-link"><?php echo $model->address; ?></div>
        <ul class="clinik_subway">
            <?php if(empty($model->subways_data)): ?><?php $this->toCache($model->id); ?><?php if(!empty($model->clinic_subway_s)): ?><?php $this->renderPartial('elements/_subway',array('data'=>$model->clinic_subway_s)); ?><?php endif; ?><?php endif; ?>
            <?php if(!empty($model->subways_data)): $dhs=unserialize($model->subways_data); ?><?php if(!empty($dhs)): $contr=Yii::app()->controller->id=='clinic'?'clinic':'doctor'; ?><?php foreach($dhs as $vhs): ?><li><span class="col-color-<?php echo !empty($vhs['subway_line_id'])?$vhs['subway_line_id']:''; ?>"><?php echo !empty($vhs['title'])?$vhs['title']:''; ?></span><span class="time_to">(<span class="foot"><?php echo !empty($vhs['lin1'])?$vhs['lin1']:' - '; ?></span> мин., <span class="car"><?php echo !empty($vhs['lin2'])?$vhs['lin2']:' - '; ?></span> мин.)</span></li><?php endforeach; ?><?php endif; ?><?php endif; ?>
        </ul><?php endif; ?>
    </div>
    <?php endforeach; ?>
</section>
