<?php if(!empty($title)): ?><div class="line"><span>Другие специализации клиник возле метро <?php echo $title; ?></span></div><?php endif; ?>
<?php if(!empty($data)): ?><ul class="name-specialistes clearfix">
    <?php foreach($data as $key=>$value): ?>
    <li>
        <?php if(!empty($value)): ?><ul>
            <?php foreach($value as $v): ?>
            <li><a href="<?php echo Yii::app()->createUrl('/clinic/index',array('subway'=>$translit,'category'=>$v['translit'])); ?>"><?php echo $v['title']; ?></a></li>
            <?php endforeach; ?>
        </ul><?php endif; ?>
    </li>
    <?php endforeach; ?>
</ul><?php endif; ?>