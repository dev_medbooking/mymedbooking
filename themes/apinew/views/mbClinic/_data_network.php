<div class="top_middle_inner clearfix">
    <a class="photo_list_item" href="/<?php echo $model->translit; ?>"><?php $this->renderPartial('elements/_photo',array('model'=>$model)); ?></a>
    <a class="clinik_name_link left <?php empty($model->description)? 'no_desc' : ''?>" href="/<?php echo $model->translit; ?>"><?php echo $model->title; ?></a>
    <?php if(!empty($model->network_id)): ?><a class="clinik_group_link left" href="<?php echo $model->clinic_network->translit; ?>"><?php echo $model->clinic_network->title; ?></a><?php endif; ?>
    <?php $this->renderPartial('elements/_rate_network',array('model'=>$model)); ?>
    <div class="clinik_description description clearfix">
        <?php echo Controller::cutString(!empty($model->description)?$model->description:'',300); ?>
    </div>
    <div class="doctor_order_block order_block clearfix">
        <span class="make-appointment btn order_btn left" data-gaq="clinic_list" data-clinic="<?php echo $model->id; ?>" data-href="/ajax/index">Записаться на прием</span>
        <div class="phone_block left">
            <span class="label">Запись по телефону</span>
            <span class="phone call_phone_1">+7 (499) 705-39-99</span>
        </div>
    </div>
</div>
<div class="bottom_middle_inner_network_items items clearfix">
    <div class="clinik_group_link"><?php echo $model->clinicNetworkCount; ?> <?php echo Yii::t('app',"клиника|клиники|клиник",array($model->clinicNetworkCount)); ?> в сети</div>
    <?php if($model->clinic_network_s3): ?><?php foreach($model->clinic_network_s3 as $value): ?><?php $this->renderPartial('_data_network_small',array('model'=>$value)); echo "\r\n"; ?><?php endforeach; ?><?php if(($model->clinicNetworkCount-3)>0): $filCount=$model->clinicNetworkCount-3; ?><div class="more_network_clinic_inner inner"><a href="/<?php echo $model->translit; ?>" class="more_network_clinic">еще <span class="count"><?php echo $filCount; ?></span> <?php echo Yii::t('app',"клиника|клиники|клиник",array($filCount)); ?></a></div><?php endif; ?><?php endif; ?>
</div>