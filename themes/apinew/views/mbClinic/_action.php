<?php if(!empty($model)):?>
    <section class="clinic_action_list">
        <div class="aclinic_action_list_count">
            В клинике <?php echo Yii::t('app',"действует|действуют|действуют",array(count($model))) ?> <?php echo count($model);?> <?php echo Yii::t('app',"акция|акции|акций",array(count($model))) ?>:
        </div>
        <div class="action_list_items items clearfix">
            <?php foreach($model as $key=>$val):?>
                <div class="action_list_item item clearfix  <?php echo empty($val->clinic_action_action->image)?'no_photo':'';?>">
                    <div class="left_action_list_item left">
                        <?php if($val->clinic_action_action->getImagePath('photo')): ?>
                            <?php echo CHtml::image("http://medbooking.com/".$val->clinic_action_action->getImagePath('photo'),$val->clinic_action_action->title, array('itemprop'=>'image')); ?>
                        <?php endif; ?>
                    </div>
                    <div class="right_action_list_item right">
                        <div class="action_name"><?php echo !empty($val->clinic_action_action->title)?$val->clinic_action_action->title:'';?></div>
                        <?php if (!empty($val->clinic_action_action->start_time) && !empty($val->clinic_action_action->end_time)): ?>
                            <div class="action_time_info">Акция действует: c <?php echo Controller::normalDate($val->clinic_action_action->start_time);?> по <?php echo Controller::normalDate($val->clinic_action_action->end_time);?></div>
                        <?php endif; ?>
                        <?php if (!empty($val->clinic_action_action->description)) {$text = explode('<!--more-->', $val->clinic_action_action->description);} ?>
                        <div class="action_description description">
                            <?php if (!empty($text[1])): ?>
                                <?php echo $text[0]; ?>
                                <div class="less_text"><?php echo $text[1]; ?></div>
                                <span class="more_text">Читать подробнее »</span>
                            <?php else: ?>
                                <?php echo!empty($val->clinic_action_action->description) ? $val->clinic_action_action->description : ''; ?>
                            <?php endif; ?>
                        </div>
                        <div class="action_order_block order_block clearfix">
                            <span data-href="<?php echo Yii::app()->createUrl('/ajax/actionInfo'); ?>" data-action="<?php echo $val->clinic_action_action->primaryKey;?>" data-open="make-appointment-modal" class="make-appointment btn left" >Записаться на акцию</span>
                            <div class="phone_block left">
                                <span class="label">Телефон для справок:</span>
                                <div class="phone call_phone_1">+7 (499) 705-39-99</div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </section>
<?php endif; ?>
