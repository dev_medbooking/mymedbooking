<section class="bottom-social-menu-container">
    <div class="inner-container clearfix">
        <ul class="menu-aside-page">
            <li class="directory-item"><a href="<?php echo Yii::app()->createUrl('/site/illnessList'); ?>"><span>Справочник<br /> заболеваний</span></a></li>
            <li class="blog-item"><a href="<?php echo Yii::app()->createUrl('/site/blogList'); ?>"><span>Медицинский<br /> блог</span></a></li>
        </ul>
        <?php $this->renderPartial('//layouts/elements/_groups'); ?>
    </div>
</section>