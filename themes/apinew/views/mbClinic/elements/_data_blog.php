<a class="photo_list_item" href="/clinic/<?php echo $model->translit; ?>"><?php $this->renderPartial('//clinic/elements/_photo', array('model' => $model)); ?></a>
<a class="clinik_name_link left" href="/clinic/<?php echo $model->translit; ?>"><?php echo $model->title; ?></a><?php if (!empty($model->network_id)): ?><?php if (!empty($model->clinic_network->title)): ?><a class="clinik_group_link left" href="/<?php echo $model->clinic_network->translit; ?>"><?php echo $model->clinic_network->title; ?></a>
<?php endif; ?><?php endif; ?>
<?php $this->renderPartial('//clinic/elements/_rate',array('model'=>$model)); ?>
<div class="clinik_description description clearfix"><?php echo Controller::cutString(!empty($model->description) ? $model->description : '', 300); ?></div>
<div class="doctor_order_block order_block clearfix">
    <div class="left">
        <span class="make-appointment btn order_btn" data-gaq="clinic_list" data-clinic="<?php echo $model->id; ?>" data-href="<?php echo Yii::app()->createUrl('/ajax/index'); ?>">Записаться на прием</span>
    </div> 
    <?php if (!empty($model->regime_byd) || !empty($model->regime_mon) || !empty($model->regime_tue) || !empty($model->regime_wed) || !empty($model->regime_thu) || !empty($model->regime_fri) || !empty($model->regime_sat) || !empty($model->regime_sun)): ?>
    <div class="clinik_time left items">
        <span class="clinik_time_label">График работы:</span>
        <?php if(!empty($model->regime_byd)): ?><span class="clinik_time_item"><span>Пн-Пт &mdash; </span><time datetime="<?php echo $model->regime_byd; ?>" itemprop="openingHours"><?php echo $model->regime_byd; ?></time></span><?php else: ?>
        <?php if(!empty($model->regime_mon)): ?><span class="clinik_time_item"><span>Пн &mdash; </span><time datetime="<?php echo $model->regime_byd; ?>" itemprop="openingHours"><?php echo $model->regime_mon; ?></time></span><?php endif; ?>
        <?php if(!empty($model->regime_tue)): ?><span class="clinik_time_item"><span>Вт &mdash; </span><time datetime="<?php echo $model->regime_byd; ?>" itemprop="openingHours"><?php echo $model->regime_tue; ?></time></span><?php endif; ?>
        <?php if(!empty($model->regime_wed)): ?><span class="clinik_time_item"><span>Ср &mdash; </span><time datetime="<?php echo $model->regime_byd; ?>" itemprop="openingHours"><?php echo $model->regime_wed; ?></time></span><?php endif; ?>
        <?php if(!empty($model->regime_thu)): ?><span class="clinik_time_item"><span>Чт &mdash; </span><time datetime="<?php echo $model->regime_byd; ?>" itemprop="openingHours"><?php echo $model->regime_thu; ?></time></span><?php endif; ?>
        <?php if(!empty($model->regime_fri)): ?><span class="clinik_time_item"><span>Пт &mdash; </span><time datetime="<?php echo $model->regime_byd; ?>" itemprop="openingHours"><?php echo $model->regime_fri; ?></time></span><?php endif; ?><?php endif; ?>
        <?php if(!empty($model->regime_sat)): ?><span class="clinik_time_item"><span>Сб &mdash; </span><time datetime="<?php echo $model->regime_byd; ?>" itemprop="openingHours"><?php echo $model->regime_sat; ?></time></span><?php endif; ?>
        <?php if(!empty($model->regime_sun)): ?><span class="clinik_time_item sunday"><span>Вс &mdash; </span><time datetime="<?php echo $model->regime_byd; ?>" itemprop="openingHours"><?php echo $model->regime_sun; ?></time></span><?php endif; ?>
    </div>
    <?php endif; ?>
</div>
<div class="clinik_name_adress clearfix">
    <?php if (!empty($model->address)): ?>
    <div class="clinik_adress map-link left"><?php echo $model->address; ?></div>
    <ul class="clinik_subway_link clinik_subway right">
        <?php $this->renderPartial('//clinic/elements/_subway', array('data' => $model->clinic_subway_s)); ?>
    </ul>
    <?php endif; ?>
</div>
<div class="map">
    <div style="width:660px;height:273px;" data-lat="<?php echo!empty($model->lat) ? $model->lat : ''; ?>" data-lng="<?php echo!empty($model->lng) ? $model->lng : ''; ?>" id="map-<?php echo $model->id; ?>" class="yandex-map-list"></div>
</div>
