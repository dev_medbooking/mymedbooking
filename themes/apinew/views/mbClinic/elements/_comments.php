<section class="items left center_block_clinic_comment_list">
    <?php if(!empty($model->clinic_review_s_limit)): ?><h2 class="cb_comment_tytle">Отзывы о клинике:</h2><?php else: ?>Отзывов нет<?php endif; ?>
    <?php echo $this->renderPartial('elements/_comment',array('clinic'=>$model,'data'=>$model->clinic_review_s_limit,'count_rating'=>count($model->clinic_review_s1))); ?>
</section>
<section class="center_block_clinic_add_comment right add_comment">
    <?php echo CHtml::beginForm(array('/clinic/rating','id'=>$model->id),'post',array("class"=>"comment_form_items_clinik form-doctor-comment items",'id'=>'form-com')); ?>
        <h3>Добавить отзыв:</h3>
            <div class="rate-items clinik-rate-items-comment rate-items-comment clearfix">
                <ul class="clearfix">
                    <li>
                        <div class="label">Профессионализм</div>
                        <div class="groupe_rank professionalism">
                            <div class="svg_wrap" data-val="0">
                                <?php for($x=1;$x<=10;$x++):?><div class="icon_rate" data-val="<?php echo $x;?>"></div><?php endfor;?>
                            </div>
                            <span class="rate_res">0</span>									
                        </div>
                    </li>
                    <li>
                        <div class="label">Внимание</div>
                        <div class="groupe_rank attention">
                            <div class="svg_wrap" data-val="0">
                                <?php for($x=1;$x<=10;$x++):?><div class="icon_rate" data-val="<?php echo $x;?>"></div><?php endfor;?>
                            </div>											
                            <span class="rate_res">0</span>									
                        </div>
                    </li>
                    <li>
                        <div class="label">Цена / Качество</div>
                        <div class="groupe_rank price">
                            <div class="svg_wrap" data-val="0">
                                <?php for($x=1;$x<=10;$x++):?><div class="icon_rate" data-val="<?php echo $x;?>"></div><?php endfor;?>
                            </div>	
                            <span class="rate_res">0</span>									
                        </div>
                    </li>
                </ul>
                <span class="error error_marks">Рейтинг не может быть меньше единицы</span>
            </div>
        <div id="error-rating"></div>
        <div class="name-doctor-comment-container clearfix">
            <label for="name" class="name-doctor-comment"></label>
            <?php echo CHtml::textField("AdminReview[name]","",array("placeholder"=>"Введите свое имя",'class'=>'row','id'=>'name')); ?>
            <div id="AdminReview_name_error" class="AdminRating_error error-phone-box" style="display:none;"></div>
        </div>
        <div class="phone-mask-container phone-mask-container-blog clearfix">
            <label class="name-doctor-comment phone-doctor-comment" for="sms_telephone"></label>
            <?php echo CHtml::textField("AdminReview[telephone]","",array("placeholder"=>"(———) ———.——.——",'class'=>'phone ibleft acenter','id'=>'sms_telephone')); ?>
            <div id="AdminReview_telephone_error" class="AdminRating_error error-phone-box" style="display:none;"></div>
        </div>
        <label for="description">Напишите свой отзыв о специалисте:</label>
        <?php echo CHtml::textArea("AdminReview[description]","",array("class"=>"row mgt10","placeholder"=>"Ваш отзыв","id"=>"description")); ?>
        <div id="AdminReview_description_error" class="AdminRating_error error-phone-box" style="display:none;"></div>
        <div class="submit-wrapper">
            <input type="submit" id="sbt-comment" class="btn" value="Добавить отзыв" data-href="<?php echo Yii::app()->createUrl('/ajax/commentary'); ?>" />
        </div>
    <?php echo CHtml::endForm(); ?>
</section>