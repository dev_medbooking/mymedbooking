<?php if(!empty($data)): ?><div id="comment-list" class="comment_list items center_block">
    <?php foreach($data as $key=>$value): ?>
    <div class="comment_item item comment_<?php echo $key;?>">
        <?php if(!empty($value->name)):?><div class="user_name"><?php echo $value->name; ?>:</div><?php endif;?>
        <div class="comment_text"><?php echo !empty($value->description)?$value->description:''; ?></div>
        <div class="comment_rank items clearfix">
            <div class="comment_rank_0 comment_rank_item" data-rank="<?php echo !empty($value->doctor_value)?$value->doctor_value:'0'; ?>">
                <div class="caption">Профессионализм<span class="hidden"> <?php echo !empty($value->doctor_value)?$value->doctor_value:'0'; ?></span></div>
            </div>
            <div class="comment_rank_1 comment_rank_item" data-rank="<?php echo !empty($value->attention_value)?$value->attention_value:'0'; ?>">
                <div class="caption">Внимание<span class="hidden"> <?php echo !empty($value->attention_value)?$value->attention_value:'0'; ?></span></div>
            </div>
            <div class="comment_rank_2 comment_rank_item" data-rank="<?php echo !empty($value->price_value)?$value->price_value:'0'; ?>">
                <div class="caption">Цена / Качество<span class="hidden"> <?php echo !empty($value->price_value)?$value->price_value:'0'; ?></span></div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
    <?php if((empty($limit)&&$count_rating>$this->comment_limit)||(!empty($limit)&&$count_rating>$limit*$this->comment_limit)): ?><div class="more_comments row acenter">
        <span data-href="<?php echo Yii::app()->createUrl('/clinic/comment',array('id'=>$clinic->id,'limit'=>!empty($limit)?$limit+1:2)); ?>" class="more_comment_btn relative">
            <?php $_count=empty($count)?($count_rating-$this->comment_limit):$count; ?>
            <?php echo Yii::t('app',"еще&nbsp;{n}&nbsp;отзыв|еще&nbsp;{n}&nbsp;отзыва|еще&nbsp;{n}&nbsp;отзывов",array($_count)); ?>
        </span>
    </div><?php endif; ?>
</div>
<?php endif; ?>


    