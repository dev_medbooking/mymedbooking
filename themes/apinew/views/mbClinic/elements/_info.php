<article>
    <h2>Информация о клинике</h2>
    <?php echo !empty($model->underbody)?$model->underbody:''; ?>
    <?php if(!empty($model->gImages)): ?>
        <div class="gallery-tab-wrapper">
            <?php if(!empty($model->gallery_id)): ?><?php if(!empty($model->gImages)): ?>
            <?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/jquery.jcarousel.js',CClientScript::POS_END); ?>
            <?php Yii::app()->clientScript->registerCssFile('http://'.$_SERVER['HTTP_HOST'].'/styles/jquery.fancybox.css'); ?>
            <?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/jquery.fancybox.js',CClientScript::POS_END); ?>
            <?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/pages/clinic_info.js',CClientScript::POS_END); ?>
            <div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul><?php foreach($model->gImages as $key=>$value): ?><?php if(!empty($value['small'])): ?><li><a class="fancybox" rel="gallery" data-fancybox-group="group" data-title="<?php echo $value['title']; ?>" data-content="<?php echo $value['alt']; ?>" alt="" rel="example_group1" href="/<?php echo $value['big']; ?>"><?php echo CHtml::image('http://medbooking.com/'.$value['big'],""); ?></a></li><?php endif; ?><?php endforeach; ?></ul>
                </div>
            </div>
            <?php endif; ?><?php endif; ?>
        </div>
    <?php endif; ?>
    <?php echo !empty($model->body)?$model->body:''; ?>
</article>