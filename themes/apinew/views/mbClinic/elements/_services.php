<?php if(!empty($model)||!empty($clinic->diagnosticService)):?>
    <section class="center_block_clinic_price_list center_block_clinic_service_list items clearfix">
        <div class="clinic_services_list left">
            <div class="order_search_block">
                <span class="search_btn"></span><span class="js-sort sort_asc active">А-Я</span><span class="js-sort sort_desc">Я-А</span>
                <form class="clinic_service_search_form clearfix" action="/" method="">
                    <label for="clinic_service_search" class="left">Название услуги</label>
                    <input type="text" id="clinic_service_search" class="left" name="search" data-href="" placeholder="Найти услугу..." value="" autocomplete="off">
                    <input type="submit" id="clinic_service_search_submit" value="">
                    <ul class="dropdown clinic_service_search_list">
                        <li><a href="#">Клинический анализ крови</a></li>
                        <li><a href="#">Определение сахара в крови</a></li>
                        <li><a href="#">Тест на толерантность к глюкозе</a></li>
                        <li><a href="#">Субтотальная резекция щитовидной железы</a></li>
                    </ul>
                </form>
            </div>
            <div class="js-service-container">
                <?php if(!empty($model)):?>
                    <?php foreach ($model as $key=>$value):?>
                        <?php if(!empty($value)):?>
                            <?php foreach ($value as $val):?>
                                <div class="clinic_service_list_item clearfix services-<?php echo $val['translit'];?>">
                                    <div class="clinic_service_title left"><span class="js-title-key"><?php echo $val['title']?></span><span class="clinic_service_category js-category-key"><?php echo $key;?></span></div>
                                    <span data-gaq="clinic_single" data-clinic="<?php echo $clinic->id;?>" data-href="/ajax/index" data-services="<?php echo $val['id']?>" class="make-appointment btn right">Записаться</span>
                                    <?php if($val['price']=='00'):?>
                                        <span class="no_clinic_service_price right btn_call_price" data-href="/ajax/price" data-clinic="<?php echo $clinic->id;?>" data-services="<?php echo $val['id']?>">по запросу</span>
                                    <?php else:?>
                                        <?php if(empty($val['discount'])):?>
                                            <span class="clinic_service_price price right"><?php echo $val['price']?></span>
                                        <?php else:?>
                                            <div class="clinic_service_discount_price right"><span class="new_price"><?php echo $val['discount']?></span><span class="old_price price"><?php echo $val['price']?></span><span class="discount_note">только<br> на Medbooking</span></div>
                                        <?php endif;?>
                                    <?php endif;?>
                                    <?php if(!empty($val['description'])):?><div class="clinic_service_description"><noindex><?php echo strip_tags($val['description']);?></noindex></div><?php endif;?>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
                <?php if(!empty($clinic->diagnosticService)):$diagnostic=$clinic->diagnosticService;?>
                    <?php foreach ($diagnostic as $key=>$value):?>
                        <div class="clinic_service_list_item clinic_service_diagnostic_list_item clearfix">
                            <div class="clinic_service_title left"><span class="clinic_service_category js-category-key">Диагностика</span><span class="js-title-key"><?php if(!empty($value->name)):?> <?php echo ViewController::my_mb_ucfirst($value->name);?> <?php else:?> <?php echo!empty($value->catc_category->name_alt)?ViewController::my_mb_ucfirst($value->catc_category->name_alt):ViewController::my_mb_ucfirst($value->catc_category->name);?><?php endif;?></span></div>
                                <span data-gaq="clinic_single" data-clinic="<?php echo $clinic->id;?>" data-href="/ajax/index" class="make-appointment btn right" data-diagnostic="<?php echo $value->category_price_id;?>">Записаться</span>
                                <?php if(empty($value->price)):?>
                                    <span class="no_clinic_service_price right btn_call_price" data-href="/ajax/price" data-clinic="<?php echo $clinic->id;?>" data-diagnostic="<?php echo $value->category_price_id;?>">по запросу</span>
                                <?php else:?>
                                    <?php if(empty($value->action_price)):?>
                                        <span class="clinic_service_price price right"><?php echo $value->price;?></span>
                                    <?php else:?>
                                        <div class="clinic_service_discount_price right"><span class="new_price"><?php echo $value->price;?></span><span class="old_price price"><?php echo $value->action_price;?></span><span class="discount_note">только<br> на Medbooking</span></div>
                                    <?php endif;?>
                                <?php endif;?>
                            </div>
                    <?php endforeach;?>
                <?php endif?>
            </div>
        </div>
        <aside class="clinic_service_categories right">
            <div class="clinic_service_categories_title">Все услуги</div>
            <ul class="clinic_service_categories_list">
                <?php if(!empty($model)):?>
                    <?php foreach ($model as $key=>$value):?>
                        <li class="js-category-change" data-key="<?php echo $key?>"><span><?php echo $key;?></span></li>
                    <?php endforeach;?>
                <?php endif;?>
                <?php if(!empty($clinic->diagnosticService)):?>
                    <li class="js-category-change" data-key="Диагностика"><span>Диагностика</span></li>
                <?php endif;?>
            </ul>
            <span class="clear js-clear-button" style="display: none">Очистить выбор</span>
        </aside>
    </section>
<?php endif; ?>