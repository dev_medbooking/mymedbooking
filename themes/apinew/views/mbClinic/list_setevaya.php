<?php $ua = $this->browser_info(); if(!empty($ua['msie'])&&(float)$ua['msie']<=9): ?><?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/searchmsie.js',CClientScript::POS_END); ?><?php else: ?><?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/searcher.js',CClientScript::POS_END); ?><?php endif; ?>
<?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/pages/clinic_list.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile("http://api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU",CClientScript::POS_HEAD); ?>
<?php $this->renderPartial('//layouts/elements/_search'); echo "\r\n"; ?>
<section id="content" class="clearfix">
    <section id="clinic_list" class="clinic_list list inner left general_list">
        <?php $this->renderPartial('_ajax',array('data'=>$data,'pages'=>$pages,'count'=>$count,'sort'=>$sort)); echo "\r\n"; ?>
    </section>
    <aside class="right">
        <div id="category_link" class="right_block block_list category_link">
            <div class="aside_h3">Все специализации:</div>
            <?php $this->renderPartial('//layouts/elements/_filter',array('controller'=>'clinic','data'=>$categories)); echo "\r\n"; ?>
        </div>
        <div id="icon_link" class="right_block block_list">
             <?php $this->renderPartial('//layouts/elements/_right'); echo "\r\n"; ?>
        </div>
    </aside>
</section>