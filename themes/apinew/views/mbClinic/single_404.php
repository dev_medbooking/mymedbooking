<?php Yii::app()->clientScript->registerCssFile('http://'.$_SERVER['HTTP_HOST'].'/styles/polaris.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/icheck.min.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/pages/clinic.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile("http://api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU",CClientScript::POS_HEAD); ?>
<?php $this->renderPartial('//layouts/elements/_breadcrumbs'); echo "\r\n"; ?>
<?php if($this->beginCache('single_'.$model->id)) { ?>
<section id="clinik_single" class="clinik_single inner single not_moderated">
    <header class="top_inner_clinik_single clearfix" itemscope itemtype="http://schema.org/LocalBusiness">
        <h1 itemprop="name" class="clinik_name"><?php echo $model->title; ?></h1>
        <div class="photo_clinik_single" itemscope itemtype="http://schema.org/ImageObject">
            <?php $this->renderPartial('elements/_photo',array('model'=>$model)); echo "\r\n"; ?>
        </div>
        <?php if(!empty($model->network_id)): ?><?php if(!empty($model->clinic_network->title)): ?><a class="clinik_group_link" href="/<?php echo $model->clinic_network->translit; ?>"><?php echo $model->clinic_network->title; ?></a><?php endif; ?><?php endif; ?>
        <div class="clinik_name_adress">
            <div class="clinik_adress" itemprop="address">
                <?php echo !empty($model->address)?$model->address:''; ?>
            </div>
            <ul class="clinik_subway">
                <?php if(empty($model->subways_data)): ?><?php $this->toCache($model->id); ?><?php if(!empty($model->clinic_subway_s)): ?><?php $this->renderPartial('elements/_subway',array('data'=>$model->clinic_subway_s)); ?><?php endif; ?><?php endif; ?>
                <?php if(!empty($model->subways_data)): $dhs=unserialize($model->subways_data); ?><?php if(!empty($dhs)): $contr=Yii::app()->controller->id=='clinic'?'clinic':'doctor'; ?><?php foreach($dhs as $vhs): ?><li><a class="col-color-<?php echo !empty($vhs['subway_line_id'])?$vhs['subway_line_id']:''; ?>" href="<?php echo Yii::app()->createUrl('/'.$contr.'/index',array('subway'=>$vhs['translit'])); ?>"><?php echo !empty($vhs['title'])?$vhs['title']:''; ?></a><span class="time_to">(<span class="foot"><?php echo !empty($vhs['lin1'])?$vhs['lin1']:' - '; ?></span> мин., <span class="car"><?php echo !empty($vhs['lin2'])?$vhs['lin2']:' - '; ?></span> мин.)</span></li><?php endforeach; ?><?php endif; ?><?php endif; ?>
            </ul>
        </div>
        <?php if(!empty($model->description)): ?><div class="clinik_description description clearfix">
            <?php echo $model->description; ?>
        </div><?php endif; ?>
        <?php if(!empty($model->regime_byd)||!empty($model->regime_mon)||!empty($model->regime_tue)||!empty($model->regime_wed)||!empty($model->regime_thu)||!empty($model->regime_fri)||!empty($model->regime_sat)||!empty($model->regime_sun)): ?>
        <div class="clinik_time">
            <?php if(!empty($model->regime_byd)): ?><span class="shedule_clinic">Пн-Пт<time datetime="Пн-Пт <?php echo $model->regime_byd; ?>" itemprop="openingHours"><?php echo $model->regime_byd; ?></time></span><?php else: ?>
            <?php if(!empty($model->regime_mon)): ?><span class="shedule_clinic">Пн<time datetime="Пн <?php echo $model->regime_mon; ?>" itemprop="openingHours"><?php echo $model->regime_mon; ?></time></span><?php endif; ?>
            <?php if(!empty($model->regime_tue)): ?><span class="shedule_clinic">Вт<time datetime="Вт <?php echo $model->regime_tue; ?>" itemprop="openingHours"><?php echo $model->regime_tue; ?></time></span><?php endif; ?>
            <?php if(!empty($model->regime_wed)): ?><span class="shedule_clinic">Ср<time datetime="Ср <?php echo $model->regime_wed; ?>" itemprop="openingHours"><?php echo $model->regime_wed; ?></time></span><?php endif; ?>
            <?php if(!empty($model->regime_thu)): ?><span class="shedule_clinic">Чт<time datetime="Чт <?php echo $model->regime_thu; ?>" itemprop="openingHours"><?php echo $model->regime_thu; ?></time></span><?php endif; ?>
            <?php if(!empty($model->regime_fri)): ?><span class="shedule_clinic">Пт<time datetime="Пт <?php echo $model->regime_fri; ?>" itemprop="openingHours"><?php echo $model->regime_fri; ?></time></span><?php endif; ?>
            <?php endif; ?><?php if(!empty($model->regime_sat)): ?><span class="shedule_clinic">Сб<time datetime="Сб <?php echo $model->regime_sat; ?>" itemprop="openingHours"><?php echo $model->regime_sat; ?></time></span><?php endif; ?>
            <?php if(!empty($model->regime_sun)): ?><span class="shedule_clinic sunday">Вс<time datetime="Вс <?php echo $model->regime_sun; ?>" itemprop="openingHours"><?php echo $model->regime_sun; ?></time></span><?php endif; ?>
        </div>
        <?php endif; ?>
        <div class="top_right_inner_clinik_single map" itemscope itemtype="http://schema.org/GeoCoordinates">
            <meta itemprop="latitude" content="<?php echo !empty($model->lat)?$model->lat:''; ?>" />
            <meta itemprop="longitude" content="<?php echo !empty($model->lng)?$model->lng:''; ?>" />
            <div style="width:280px;height:422px;" data-lat="<?php echo !empty($model->lat)?$model->lat:''; ?>" data-lng="<?php echo !empty($model->lng)?$model->lng:''; ?>" id="map"></div>
        </div>
    </header>
    <section class="bottom_inner_clinik_single bottom_inner_clinik_single_not_moderated tabs_container clearfix">
        <?php if(!empty($model->clinic_doctor_s)||!empty($model->clinic_review_s_limit)||!empty($model->body)): ?>
        <header class="bi_clinic_single_cut inner clearfix">
            <?php if(!empty($model->clinic_doctor_s)): ?><span data-href="doctor-tab">Доктора</span><?php endif; ?>
            <?php if(!empty($model->clinic_review_s_limit)): ?><span data-href="reviews-tab">Отзывы пациентов<sup class="count"> (<?php echo !empty($model->clinic_review_s)?count($model->clinic_review_s):'0';?>)</sup></span><?php endif; ?>
            <?php if(!empty($model->body)): ?><span data-href="tabs-gallery">Информация о клинике</span><?php endif; ?>
        </header>
        <?php endif; ?>
        <ul class="tabs-control clearfix">
            <?php if(!empty($model->clinic_doctor_s)): ?><li class="tabs-control-item"><a class="doctor-tab" href="#">Доктора</a></li><?php endif; ?>
            <?php if(!empty($model->clinic_review_s1)):?><li class="tabs-control-item"><a class="reviews-tab" href="#comment-block">Отзывы<sup class="count"> (<?php echo !empty($model->clinic_review_s)?count($model->clinic_review_s):'0';?>)</sup></a></li><?php endif; ?>
            <?php if(!empty($model->body)):?><li class="tabs-control-item tab-control-info"><a class="tabs-gallery" href="#">О клинике</a></li><?php endif; ?>
        </ul>
        <ul class="tabs-list clearfix">
            <?php if(!empty($model->clinic_doctor_s)): ?><li class="tabs-item doctor-tab clearfix"><?php $this->renderPartial('//clinic/doctors/_doctor',array('doctors'=>$doctors,'model'=>$model,'data'=>$model->clinic_doctor_single)); echo "\r\n"; ?></li><?php endif; ?>
            <?php if(!empty($model->clinic_review_s1)):?><li class="tabs-item tab_1 reviews-tab clearfix" id="comment-block"><?php $this->renderPartial('//clinic/elements/_comments_404',array('model'=>$model)); echo "\r\n"; ?></li><?php endif; ?>
            <?php if(!empty($model->body)):?><li class="tabs-item tab_2 tabs-gallery clearfix"><?php $this->renderPartial('//clinic/elements/_info',array('model'=>$model)); echo "\r\n"; ?></li><?php endif; ?>
        </ul>
    </section>
</section>
<?php $this->renderPartial('//clinic/sames/_single_sames_404',array('value'=>$model)); echo "\r\n"; ?>
<?php $this->endCache(); } ?>