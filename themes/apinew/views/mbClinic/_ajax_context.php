<?php $this->renderPartial('//layouts/elements/_breadcrumbs'); echo "\r\n"; ?>
<?php if(empty($_GET['title'])): ?><section class="attribute top_inner_clinik_list clearfix" data-category="<?php echo $this->category_title; ?>" data-subway="<?php echo $this->subway_title; ?>">
    <div class="clinic_count left">
        <?php echo Yii::t('app',"Найден|Найдено|Найдено",array($count)); ?> <span class="count"><?php echo $count; ?></span> <?php echo Yii::t('app',"клиника|клиники|клиник",array($count)); ?>
    </div>
    <div class="sort clearfix right">
        <span class="way_sort">Сортировать по: </span>
        <span><noindex><?php echo $sort->link('rating',null,array('rel'=>'nofollow', 'class'=>'nolink')); ?></noindex></span>
        <?php if(empty($this->setevaya)):?><a href="<?php echo Yii::app()->createUrl('/clinic/setevaya');?>" class="setevaya">Сетевые клиники</a><?php else: ?><a href="<?php echo Yii::app()->createUrl('/clinic');?>" class="active-setevaya">Сетевые клиники</a><?php endif;?>
    </div>
</section><?php endif; ?>
<?php if(!empty($data)): ?><section class="items middle_inner_clinik_list">
    <?php foreach($data as $value): ?>
    <?php if($this->setevaya): ?><div class="clearfix item clinic_item <?php echo !empty($value->network_id)?'setevay':''?>" <?php echo !(empty($value->view_status))?'class="vip"':''; ?>>
        <?php $this->renderPartial('_data_network',array('model'=>$value)); echo "\r\n"; ?>
    </div><?php else: ?>
    <div class="clearfix item clinic_item <?php echo !empty($value->network_id)?'setevay':''?>" <?php echo !(empty($value->view_status))?'class="vip"':''; ?>>
        <?php $this->renderPartial('_data',array('model'=>$value)); echo "\r\n"; ?>
    </div><?php endif; ?>
    <?php endforeach; ?>
</section><?php endif; ?>
<?php if(!empty($pages)&&!empty($count)): ?><div class="paging clearfix">
    <?php if(empty($this->category)&&empty($this->category_title)):?>
        <a href="/search_clinic">Все клиники</a>
    <?php else:?>
        <a href="/search_clinic/<?php echo $this->category;?>">Показать все центры</a>
    <?php endif; ?>
</div><?php endif; ?>