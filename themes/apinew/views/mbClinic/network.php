<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/polaris.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/icheck.min.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/jquery.mCustomScrollbar.min.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('http://'.$_SERVER['HTTP_HOST'].'/scripts/pages/clinic.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile("http://api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU",CClientScript::POS_HEAD); ?>
<?php $this->renderPartial('//layouts/elements/_breadcrumbs'); echo "\r\n"; ?>
<section id="clinik_single_network" class="clinik_single_network inner single" itemprop="http://schema.org/MedicalClinic" itemscope="">
    <header class="top_inner_clinik_single top_inner_clinik_single_network clearfix" itemprop="http://schema.org/Organization" itemscope="">
        <div class="left_tli_clinik_single left_tli_clinik_single_network left">
            <div class="clinik_photo" itemtype="http://schema.org/ImageObject" itemscope="">
                <?php $this->renderPartial('elements/_photo',array('model'=>$model)); echo "\r\n"; ?>
            </div>
            <div class="phone_block" itemprop="telephone">
                <span class="label">Запись по телефону</span>
                <div class="phone call_phone_1">+7 (499) 705-39-99</div>
            </div>
            <span data-gaq="clinic_single" data-clinic="<?php echo $model->id; ?>" data-href="/ajax/index" data-open="make-appointment-modal" class="make-appointment btn">Записаться на прием</span>
            <?php if(!empty($model->regime_byd)||!empty($model->regime_mon)||!empty($model->regime_tue)||!empty($model->regime_wed)||!empty($model->regime_thu)||!empty($model->regime_fri)||!empty($model->regime_sat)||!empty($model->regime_sun)): ?><div class="clinic_shedule_block inner">
                <span class="clinik_time_title">График работы:</span>
                <div class="clinik_time clearfix">
                    <?php if(!empty($model->regime_byd)): ?><div class="shedule_clinic">
                        <span>Пн-Пт</span><time datetime="<?php echo $model->regime_byd; ?>" itemprop="openingHours"><?php echo $model->regime_byd; ?></time>
                    </div><?php else: ?>
                    <?php if(!empty($model->regime_mon)): ?><div class="shedule_clinic">
                        <span>Пн</span><time datetime="<?php echo $model->regime_mon; ?>" itemprop="openingHours"><?php echo $model->regime_mon; ?></time>
                    </div><?php endif; ?>
                    <?php if(!empty($model->regime_tue)): ?><div class="shedule_clinic">
                        <span>Вт</span><time datetime="<?php echo $model->regime_tue; ?>" itemprop="openingHours"><?php echo $model->regime_tue; ?></time>
                    </div><?php endif; ?>
                    <?php if(!empty($model->regime_wed)): ?><div class="shedule_clinic">
                        <span>Ср</span><time datetime="<?php echo $model->regime_wed; ?>" itemprop="openingHours"><?php echo $model->regime_wed; ?></time>
                    </div><?php endif; ?>
                    <?php if(!empty($model->regime_thu)): ?><div class="shedule_clinic">
                        <span>Чт</span><time datetime="<?php echo $model->regime_thu; ?>" itemprop="openingHours"><?php echo $model->regime_thu; ?></time>
                    </div><?php endif; ?>
                    <?php if(!empty($model->regime_fri)): ?><div class="shedule_clinic">
                        <span>Пт</span><time datetime="<?php echo $model->regime_fri; ?>" itemprop="openingHours"><?php echo $model->regime_fri; ?></time>
                    </div><?php endif; ?><?php endif; ?>
                    <?php if(!empty($model->regime_sat)): ?><div class="shedule_clinic">
                        <span>Сб</span><time datetime="<?php echo $model->regime_sat; ?>" itemprop="openingHours"><?php echo $model->regime_sat; ?></time>
                    </div><?php endif; ?>
                    <?php if(!empty($model->regime_sun)): ?><div class="shedule_clinic sunday">
                        <span>Вс</span><time datetime="<?php echo $model->regime_sun; ?>" itemprop="openingHours"><?php echo $model->regime_sun; ?></time>
                    </div><?php endif; ?>
                </div>
            </div><?php endif; ?>
        </div>
        <h1 itemprop="name"><?php echo $model->title; ?></h1>
        <div class="clinic_info_statistic_block inner left clearfix">
            <span class="clinic_statistic left"><?php echo $model->clinicNetworkCount; ?>&nbsp;<?php echo Yii::t('app',"клиника|клиники|клиник",array($model->clinicNetworkCount)); ?></span>
            <span class="doctor_statistic left"><?php echo $model->doctorNetworkCount; ?>&nbsp;<?php echo Yii::t('app',"доктор|доктора|докторов",array($model->doctorNetworkCount)); ?></span>
        </div>
        <?php $this->renderPartial('elements/_rate_network',array('model'=>$model)); echo "\r\n"; ?>
        <?php if(!empty($model->body)): ?><div class="clinik_description description clearfix" itemprop="description">
            <?php echo $model->body; ?>
        </div><?php endif; ?>
    </header>
    <?php if(!empty($model->clinic_network_s)): ?><section class="middle_inner_clinik_single_network bottom_middle_inner_network_items inner clearfix">
        <div class="clinik_group_link"><?php echo $model->clinicNetworkCount; ?> <?php echo Yii::t('app',"клиника|клиники|клиник",array($model->clinicNetworkCount)); ?> в сети</div>
        <?php foreach($model->clinic_network_s as $value): ?>
        <div class="clinik_name_adress item address clearfix">
            <div class="branch_clinik_name_adress left">
                <a class="clinik_name_link" href="/clinic/<?php echo $value->translit; ?>"><?php echo $value->title; ?></a>
                <div class="clinik_time clearfix">
                    <?php if(!empty($value->regime_byd)): ?><div class="shedule_clinic">
                        <span>Пн-Пт</span><time datetime="<?php echo $value->regime_byd; ?>" itemprop="openingHours"><?php echo $value->regime_byd; ?></time>
                    </div><?php else: ?>
                    <?php if(!empty($value->regime_mon)): ?><div class="shedule_clinic">
                        <span>Пн</span><time datetime="<?php echo $value->regime_mon; ?>" itemprop="openingHours"><?php echo $value->regime_mon; ?></time>
                    </div><?php endif; ?>
                    <?php if(!empty($value->regime_tue)): ?><div class="shedule_clinic">
                        <span>Вт</span><time datetime="<?php echo $value->regime_tue; ?>" itemprop="openingHours"><?php echo $value->regime_tue; ?></time>
                    </div><?php endif; ?>
                    <?php if(!empty($value->regime_wed)): ?><div class="shedule_clinic">
                        <span>Ср</span><time datetime="<?php echo $value->regime_wed; ?>" itemprop="openingHours"><?php echo $value->regime_wed; ?></time>
                    </div><?php endif; ?>
                    <?php if(!empty($value->regime_thu)): ?><div class="shedule_clinic">
                        <span>Чт</span><time datetime="<?php echo $value->regime_thu; ?>" itemprop="openingHours"><?php echo $value->regime_thu; ?></time>
                    </div><?php endif; ?>
                    <?php if(!empty($value->regime_fri)): ?><div class="shedule_clinic">
                        <span>Пт</span><time datetime="<?php echo $value->regime_fri; ?>" itemprop="openingHours"><?php echo $value->regime_fri; ?></time>
                    </div><?php endif; ?><?php endif; ?>
                    <?php if(!empty($value->regime_sat)): ?><div class="shedule_clinic">
                        <span>Сб</span><time datetime="<?php echo $value->regime_sat; ?>" itemprop="openingHours"><?php echo $value->regime_sat; ?></time>
                    </div><?php endif; ?>
                    <?php if(!empty($value->regime_sun)): ?><div class="shedule_clinic sunday">
                        <span>Вс</span><time datetime="<?php echo $value->regime_sun; ?>" itemprop="openingHours"><?php echo $value->regime_sun; ?></time>
                    </div><?php endif; ?>
                </div>
                <?php if(!empty($value->address)): ?><div class="clinik_adress map-link"><?php echo $value->address; ?></div><?php endif; ?>
            </div>
            <span data-gaq="clinic_single" data-clinic="<?php echo $value->id; ?>" data-href="/ajax/index" data-open="make-appointment-modal" class="make-appointment btn right">Записаться</span>
            <div class="clinik_rate_comment_block right ">
                <?php if(!empty($value->rate10)): ?>
                    <?php if($value->rate10==10):?>
                        <div class="rate clinic_rate <?php if(empty($model->comm)): ?> no_reviews<?php endif; ?>" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                            <meta itemprop="bestRating" content="10">
                            <meta itemprop="ratingValue" content="<?php echo str_replace(".",",",sprintf("%.2f",!empty($model->rate10)?round($model->rate10,2):0)); ?>">
                            <meta itemprop="ratingCount" content="<?php echo !empty($model->comm)?$model->comm:''; ?>" /><?php echo $value->rate10; ?></div>
                    <?php else:?>
                        <div class="rate clinic_rate <?php if(empty($model->comm)): ?> no_reviews<?php endif; ?>" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                            <meta itemprop="bestRating" content="10">
                            <meta itemprop="ratingValue" content="<?php echo str_replace(".",",",sprintf("%.2f",!empty($model->rate10)?round($model->rate10,2):0)); ?>">
                            <meta itemprop="ratingCount" content="<?php echo !empty($model->comm)?$model->comm:''; ?>" /><?php echo str_replace(",",".",sprintf("%.1f",!empty($value->rate10)?round($value->rate10,2):0)); ?></div>
                    <?php endif;?>
                <?php endif; ?>
                <?php if(!empty($value->comm)): ?>
                    <a class="clinic_comment comment right reviews" href="/clinic/<?php echo $value->translit; ?>#comment-block"><strong><?php echo $value->comm; ?></strong></a>
                <?php endif; ?>
            </div>
            <ul class="clinik_subway_link clinik_subway right">
                <?php if(empty($value->subways_data)): ?>
                    <?php $this->toCache($value->id); ?>
                    <?php if(!empty($value->clinic_subway_s)): ?>
                        <?php $this->renderPartial('elements/_subway',array('data'=>$value->clinic_subway_s)); ?>
                    <?php endif; ?>
                    <?php endif; ?>
                        <?php if(!empty($value->subways_data)): $dhs=unserialize($value->subways_data); ?>
                    <?php if(!empty($dhs)): $contr=Yii::app()->controller->id=='clinic'?'clinic':'doctor'; ?>
                        <?php foreach($dhs as $vhs): ?>
                            <li><?php if(!empty($this->category)): ?>
                                <span class="col-color-<?php echo !empty($vhs['subway_line_id'])?$vhs['subway_line_id']:''; ?>">
                                    <?php echo !empty($vhs['title'])?$vhs['title']:''; ?>
                                </span>
                                <?php else: ?>
                                <span class="col-color-<?php echo !empty($vhs['subway_line_id'])?$vhs['subway_line_id']:''; ?>">
                                    <?php echo !empty($vhs['title'])?$vhs['title']:''; ?>
                                </span>
                                <?php endif; ?>
                                <span class="time_to">(<span class="foot"><?php echo !empty($vhs['lin1'])?$vhs['lin1']:' - '; ?></span> мин., <span class="car"><?php echo !empty($vhs['lin2'])?$vhs['lin2']:' - '; ?></span> мин.)</span>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </ul>        
        </div>
        <div class="map">
            <div style="width:660px;height:273px;" data-lat="<?php echo !empty($value->lat)?$value->lat:''; ?>" data-lng="<?php echo !empty($value->lng)?$value->lng:''; ?>" id="map-<?php echo $value->id; ?>" class="yandex-map-list"></div>
        </div>        
        <?php endforeach; ?>
    </section><?php endif; ?>
    <section class="bottom_inner_clinik_single tabs_container clearfix">
        <ul class="tabs-control clearfix">
            <?php $dnc=false; if(!empty($model->doctorNetworkCount)): $dnc=true; ?><li class="tabs-control-item"><a class="doctor-tab" href="#">Доктора<sup class="count"> (<?php echo !empty($model->doctorNetworkCount)?$model->doctorNetworkCount:'0';?>)</sup></a></li><?php endif; ?>
            <?php $rnc=true; if(!empty($model->rateNetworkCount)): $rnc=true; ?><li class="tabs-control-item"><a class="reviews-tab" href="#comment-block">Отзывы<sup class="count"> (<?php echo !empty($model->clinicNetworkReviewsCount)?$model->clinicNetworkReviewsCount:'0';?>)</sup></a></li><?php endif; ?>
            <?php if(!empty($services)||!empty($model->diagnosticService)):?><li class="tabs-control-item tab-control-info"><a class="tabs-services" href="#services_list_block">Цены на услуги</a></li><?php endif;?>
        </ul>
        <ul class="tabs-list clearfix">
            <?php if($dnc): ?><li class="tabs-item tab_0 clearfix"><?php $this->renderPartial('//clinic/doctors/_doctor_network',array('model'=>$model,'doctors'=>$model->clinicNetworkDoctorsSpeciality,'data'=>$model->clinicNetworkDoctors)); ?></li><?php endif; ?>
            <?php if($rnc): ?><li class="tabs-item tab_1 clearfix" id="comment-block"><?php $this->renderPartial('elements/_comments_network',array('model'=>$model)); ?></li><?php endif; ?>
            <?php if(!empty($services)||!empty($model->diagnosticService)):?><li class="tabs-item tab_3 clearfix" id="services_list_block"><?php $this->renderPartial('elements/_services',array('model'=>$services,'clinic'=>$model)); ?></li><?php endif; ?>
        </ul>
    </section>
</section>