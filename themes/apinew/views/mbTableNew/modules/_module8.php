<section class="modul">
  <div class="js_form_modul">
   <div class="wrapper_mod clearfix">
       <div class="form_modul">
            <div class="form_modul_preview">
                <h2>Кнопка записи на прием</h2>
                <p>«Кнопка записи на прием» - данный инструмент может фигурировать в любой части Вашего сайта. Коммуникабельная кнопка, при нажатии которой, пользователь оставит заявку на подбор врача.</p>
                <input id="btn_code" type="button" value="Получить код">
                </div>
               <div class="form_setting">
                <fieldset class="form_size">
                          <h2>Настройки</h2>
                          <h3>Название кнопки:</h3>
                           <input id="name_btn" type="text" placeholder="Записаться на прием">
                          <h3>Название попапа:</h3>
                           <input id="name_popup" type="text" placeholder="Записаться на прием">
                </fieldset> 
                <fieldset class="form_size"> 
                    <h3>Выберите размер шрифта:</h3>
                    <select class="js-font_size" name="menu" size="1">
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option selected="selected" value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                    </select><label for="menu">PX</label>
                    <h3>Выберите начертание:</h3>
                    <span class="bold js-bold">B</span>
                    <span class="italic js-italic">I</span>
                    <span class="underline js-underline">U</span>
                </fieldset> 
                <fieldset class="form_size"> 
                    <h3>Цвет фона:</h3>
                    #<input type="text" value="bad478" id="picker_back"><label for="picker_back"></label>
                    <h3>Цвет надписи:</h3>
                    #<input type="text" value="FFFFFF" id="picker_text"><label for="picker_text"></label>
                    <div class="slider">
                            <h3>Скругление углов:</h3>
                            <input type="text" id="amount" readonly>
                        <div id="slider-range-max"></div>
                    </div>
                </fieldset>                   
                <fieldset class="form_tool_8"> 
                    <input type="checkbox" id="allStylebtn" />
                    <label for="allStylebtn">Без редакции кнопки<br>&nbsp;</label>
                    <input checked type="checkbox" id="form" />
                    <label for="form">Показывать форму<br>&nbsp;&nbsp;&nbsp;записи онлайн</label>
                    <span class="text_click">Всплывающая форма позволяет записаться онлайн всего одним щелчком мыши.</span> 
                </fieldset>
            </div>
        </div>
    </div>
</div>
    <div class="wrapper_mod clearfix">
        <div class="modul_preview">
            <div class='browser-wrapper'>
                <div class='top-bar'>
                    <div class='row small'>
                    <div class='circle close'></div>
                    <div class='circle minify'></div>
                    <div class='circle zoom'></div>
                    <div class='tab'>
   
                    </div>
                </div>
                <div class='row flex active large'>
                    <div class='button back'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button forward'>
                        <div class='top-block'></div>
                        <div class='bottom-block'></div>
                        <div class='straight-block'></div>
                    </div>
                    <div class='button reload-button'>
                        <div class='arrow-down'></div>
                    </div>
                    <div class='address-bar'>
                        <span class='favicon'></span>
                    </div>
                    <div class='sandwich-button'>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                        <div class='sandwich-line'></div>
                    </div>
                </div>
            </div>
            <div class="site_block">
            <div class="site_block_logo">LOGO</div>
            <div class="site_block_log"></div>
            <div class="site_block_search"></div>
            </div>
            <div class='content_browser'>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
            <div class="site_block_1"></div>
                <button class="mb_mod_button">Записаться на прием</buttom> <script>  var id_client =  <?=$partner->id;?>,num = "<?=$partner->phone;?>",mb_fw = "normal",mb_fz = "16px",mb_td = "none",mb_fs = "normal",mb_brs = "0",mb_bg = "#b1d478",mb_dragForm = "true",mb_dragFormB = "true",mb_col = "#fff",mb_titlePop = "Записаться на прием",scriptJs = document.createElement("script");scriptJs.src = "http://medbooking.com/scripts/integrator/moduleBtn/script.js";document.head.appendChild(scriptJs); </script> 
            </div>
        </div>
        </div>
        </div> 
</section>
<div id="popup_mod">
    <div class="popup_head_mod">
        <h2>Код</h2>
        <i id="popup_close"></i>
        <span>Вставьте код на сайт</span>
    </div>
    <div class="popup_body-mod">
        <form>
        <div class="code_msg" id="code_msg">
        </div>
            <input data-clipboard-target="code_msg" id="copy" type="button" value="Скопировать">
        </form>
    </div>
</div>
<script>
    mbId = <?=$partner->id;?>;
    mbPhone = "<?=$partner->phone;?>";
</script>