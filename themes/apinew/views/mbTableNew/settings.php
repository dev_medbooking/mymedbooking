<section class="setting">
	<div class="container">
		<h3>Персональные данные партнера:</h3>
		<ul class="setting_bar">
			<li class="norm">
				<input name="name" disabled id="name" type="text" data-href="<?=Yii::app()->createUrl('mbTable/updatePartnerFields');?>" value="<?=$partner->name;?>"><a href="#" data-type="edit-input" data-target="name">Изменить</a>
				<span class="inf">Фамилия и имя контактного лица - партнера</span>
			</li>
			<li class="norm">
				<input name="email" disabled id="email" type="email" value="<?=$partner->usr->email;?>"><a href="#" data-type="edit-input" data-target="email">Изменить</a>
				<span class="inf">Электронный адрес контактного лица - партнера</span>
			</li>
			<li class="norm">
				<input name="phone" readonly
 id="phone"  type="phone" data-href="<?=Yii::app()->createUrl('mbTable/updatePartnerFields');?>"  value="<?=$partner->contact_phone;?>">
				<span class="inf">Контактный телефон партнера</span>
			</li>
			<li class="norm">
				<input name="money" disabled id="money" type="text" data-href="<?=Yii::app()->createUrl('mbTable/updatePartnerFields');?>"  value="<?=$partner->payment_number;?>" autocomplete="off"><a href="#" data-type="edit-input" data-target="money">Изменить</a>
				<span class="inf">Введите номер счета</span>
			</li>
			<li class="norm">
				<input name="password" disabled id="password" type="password" data-href="<?=Yii::app()->createUrl('mbTable/updatePartnerFields');?>"  value="<?=$partner->usr->password;?>"><a href="#" data-type="edit-input" data-target="password">Изменить</a>
				<span class="last personal_Info">Ваш пароль</span>
			</li>
		</ul>
	</div>
</section>