<section class="wrapper_modul">
	<header class="header_p">
		<?php $this->renderPartial('_header', array(
			'partner' => $partner,
			'partners' => $partners,
			'phone' => $phone,
		));?>
		
	</header>
	<section class="nav_modules">
		<div class="nav_modules_wrap">
			<ul class="menu_modules clearfix">
					<li class="<?=($id == 1 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 1));?>">Список
врачей/клиник/услуг(Аякс)</a></li>
					<li class="<?=($id == 2 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 2));?>">Список
врачей/клиник/услуг(PHP)</a></li>
					<li class="<?=($id == 3 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 3));?>">Список
врачей/клиник/услуг(JS)</a></li>
					<li class="<?=($id == 4 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 4));?>">Форма
поиска</a></li>
					<li class="<?=($id == 5 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 5));?>">Телефон
единого центра</a></li>
					<li class="<?=($id == 6 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 6));?>">Форма
подбора</a></li>
					<li class="<?=($id == 7 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 7));?>">Диагностические
центры</a></li>
					<li class="<?=($id == 8 ? 'active' : '');?>"><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 8));?>">Кнопка записи
на прием</a></li>
		
			</ul>
		</div>
	</section>
	<script src="<?=Yii::app()->theme->baseUrl;?>/js/ZeroClipboard.js"></script> 
	<script src="<?=Yii::app()->theme->baseUrl;?>/js/settings/mod<?=$id;?>.js"></script>
	<?php $this->renderPartial("modules/_module{$id}", array('partner' => $partner));?>
	<?php $this->renderPartial('_footer');?>
</section>
