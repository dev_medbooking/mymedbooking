<?php if( ! empty($model)):?>
    <?php foreach ($model as $key=>$value):?>
        <tr>
	        <td class="number"><?=( !empty($value->id) ? $value->id : '');?></td>
	        <td class="date"><?=( ! empty($value->create_time) ? date('d.m.Y H:m' , strtotime($value->create_time)) : '');?></td>
	        <td class="status"><?=( ! empty($value->partnerStatus) ? $value->partnerStatus : '');?></td>
            <?php if (Yii::app()->user->checkAccess('Administrator')) :?>
				<td>
					<?=( ! empty($value->partner->name) ? $value->partner->name : '');?>
					<!--<a href="<?=Yii::app()->createUrl('mbTable/deleteFromCabinet', array('id' => $value->id));?>">X</a>-->
				</td>
			<?php endif;?>
        </tr>
    <?php endforeach;?>
<?php endif;?>
