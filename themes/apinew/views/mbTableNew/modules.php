<section class="wrapper_p">
	<header class="header_p">
		<?php $this->renderPartial('_header', array(
			'partner' => $partner,
			'partners' => $partners,
			'phone' => $phone,
		));?>
	</header>
	<section class="nav_modules">
		<div class="nav_modules_wrap">
			<ul class="menu_modules clearfix">
					<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 1));?>">Список
врачей/клиник/услуг(Аякс)</a></li>
					<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 2));?>">Список
врачей/клиник/услуг(PHP)</a></li>
					<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 3));?>">Список
врачей/клиник/услуг(JS)</a></li>
					<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 4));?>">Форма
поиска</a></li>
					<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 5));?>">Телефон
единого центра</a></li>
					<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 6));?>">Форма
подбора</a></li>
					<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 7));?>">Диагностические
центры</a></li>
					<li><a href="<?=Yii::app()->createUrl('mbTable/module', array('id' => 8));?>">Кнопка записи
на прием</a></li>
		
			</ul>
		</div>
	</section>
	<?php $this->renderPartial("modules/_modules", array('partner' => $partner));?>
	<?php $this->renderPartial('_footer');?>
</section>
