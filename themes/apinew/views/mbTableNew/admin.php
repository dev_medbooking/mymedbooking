<section class="wrapper_p">
    <header>
        <?php $this->renderPartial('_header', array(
            'partner' => $partner,
            'partners' => $partners,
            'phone' => $phone,
        )); ?>
    </header>
	<section class="menu">
		<div class="container">
			<ul class="menu_left">
				<li><a class="<?=(empty($status) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => $limit, 'status' => ''));?>">Все</a></li>
				<li><a class="<?=(( ! empty($status) AND $status == 1) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => $limit, 'status' => 1));?>">Требующие ответа</a></li>
				<li><a class="<?=(( ! empty($status) AND $status == 2) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => $limit, 'status' => 2));?>">Записанные</a></li>
				<li><a class="<?=(( ! empty($status) AND $status == 3) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => $limit, 'status' => 3));?>">Отклоненные</a></li>
			</ul>
			<ul class="menu_right">
				<li><a class="menu_calendar"></a></li>
				<li>
					<form action="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => $limit, 'status' => ( ! empty($status) ? $status : 0)));?>" class="js-search-form">
						<input class="menu_search" id="MbRecord[name]" placeholder="Поиск" type="text" name="MbRecord[name]" value="<?=( ! empty($name) ? $name : '');?>"><label for="MbRecord[name]"></label>
					</form>
				</li>
			</ul>
		</div>
	</section>
	<section class="building">
		<div class="container">
		    <ul class="sort_times">
			    <li><a class="<?=(($date_start == $month3 AND $date_end == $today) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $month3, 'date_end' => $today, 'limit' => $limit, 'status' => ( ! empty($status) ? $status : 0)));?>">квартал</a></li>
			    <li><a class="<?=(($date_start == $month AND $date_end == $today) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $month, 'date_end' => $today, 'limit' => $limit, 'status' =>( ! empty($status) ? $status : 0)));?>">месяц</a></li>
			    <li><a class="<?=(($date_start == $week AND $date_end == $today) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $week, 'date_end' =>$today, 'limit' => $limit, 'status' =>( ! empty($status) ? $status : 0)));?>">неделя</a></li>
			    <li><a class="<?=(($date_start == $yesterday AND $date_end == $yesterday) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $yesterday, 'date_end' => $yesterday,'limit' => $limit, 'status' =>( ! empty($status) ? $status : 0)));?>">вчера</a></li>
			    <li><a class="<?=(($date_start == $today AND $date_end == $today) ? 'active' : '');?>" href="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $today, 'date_end' => $today, 'limit' => $limit, 'status' =>( ! empty($status) ? $status : 0)));?>">сегодня</a></li>
		    </ul>
			<form action=""<?=Yii::app()->createUrl('mbTable/admin');?>">
				<label for="date">С</label><input id="js-date-start" type="text" placeholder="____/__/__" name="date_start" value="<?=$date_start;?>">
				<label for="date_2">По</label><input id="js-date-end" type="text" placeholder="____/__/__" name="date_end"  value="<?=$date_end;?>">
				<input type="hidden" name="limit" value="<?=$limit;?>" />
				<input type="hidden" name="status" value="<?=( ! empty($status) ? $status : 0);?>" />
				<input type="submit" value="Применить">
			</form>
		</div>
	</section>
	<section class="groop-table">
		<div class="header__showing clearfix">

            <div class="showing">
                <span>Показывать по</span>
                <select class="js-select-box">
	                <option value="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => 10, 'status' =>( ! empty($status) ? $status : 0)));?>" <?php if($limit==10):?>selected<?php endif;?>>10 записей</option>
	                <option value="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => 20, 'status' =>( ! empty($status) ? $status : 0)));?>" <?php if($limit==20):?>selected<?php endif;?>>20 записей</option>
	                <option value="<?=Yii::app()->createUrl('mbTable/admin', array('date_start' => $date_start, 'date_end' => $date_end, 'limit' => 50, 'status' =>( ! empty($status) ? $status : 0)));?>" <?php if($limit==50):?>selected<?php endif;?>>50 записей</option>
                </select>
            </div>
	    </div>
		<table class="cpa_table">
            <thead><?php $this->renderPartial('//mbTable/filter', array('sort' => $sort));?></thead>
            <tbody class="js-update-data"><?php $this->renderPartial('//mbTable/data', array('model' => $data));?></tbody>
        </table>
		<div class="pagination" id="pages">
			<?php if ( ! empty($pages) AND ! empty($count)): ?>
				<?php $this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count)); ?>
			<?php endif; ?>
		</div>
    </section>
    <?php $this->renderPartial('_footer') ?>
</section>
