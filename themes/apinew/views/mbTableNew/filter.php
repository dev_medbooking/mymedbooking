<tr>
    <th><?=$sort->link('id', null, array('class'=>'number'));?></th>
    <th><?=$sort->link('create_time', null, array('class'=>'date'));?></th>
    <th><?=$sort->link('status', null, array('class'=>'status'));?></th>
    <?php if (Yii::app()->user->checkAccess('Administrator')) :?>
	    <th><span>Партнер</span></th>
    <?php endif;?>
</tr>
