<?php if ( ! empty($id)) :?>
	<?php $this->renderPartial('module', array('id' => $id, 'partner' => $partner, 'partners' => $partners, 'phone' => $phone));?>
<?php else :?>
	<?php $this->renderPartial('modules', array('partner' => $partner, 'partners' => $partners, 'phone' => $phone));?>
<?php endif;?>