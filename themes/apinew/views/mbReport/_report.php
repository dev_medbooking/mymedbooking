<table class="records" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
    <thead>
        <tr>
            <th style="border:1px solid #dfdfdf;">Сайт</th>
            <th style="border:1px solid #dfdfdf;">Заявок</th>
            <th style="border:1px solid #dfdfdf;">Записей</th>
            <th style="border:1px solid #dfdfdf;">Отказы</th>
            <th style="border:1px solid #dfdfdf;">Обратная</th>
            <th style="border:1px solid #dfdfdf;">Дошедшие</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #dfdfdf;">Домен, или метка, по которой создана заявка</td>
            <td style="border:1px solid #dfdfdf;">Общее число заявок, не включает "корзину" и "Oktell"</td>
            <td style="border:1px solid #dfdfdf;">Число записей, дошедших и тех, которые не явились</td>
            <td style="border:1px solid #dfdfdf;">Отказы, отмены, предварительные отказы</td>
            <td style="border:1px solid #dfdfdf;">Воспользовались формой обратной связи</td>
            <td style="border:1px solid #dfdfdf;">Дошедшие</td>
        </tr>
        <?php if(empty($dataBody)||empty($dataBody['domains'])):?>
        <tr>
            <td colspan="5" style="border:1px solid #dfdfdf;">Нет записей</td>
        </tr>
        <?php else:?>
        <?php foreach($dataBody['domains'] as $domain):?>
        <tr>
            <td style="border:1px solid #dfdfdf;"><?php echo $domain['title']; ?></td>
            <td style="border:1px solid #dfdfdf;"><?php echo $domain['records']['new'][0]; ?></td>
            <td style="border:1px solid #dfdfdf;">
                <?php echo $domain['records']['confirmed'][0]; ?>
                <?php if(!empty($domain['records']['new'][0])): ?>
                 (<?php echo round($domain['records']['confirmed'][0]/$domain['records']['new'][0]*100); ?>%)
                <?php endif; ?>
            </td>
            <td style="border:1px solid #dfdfdf;">
                <?php echo $domain['records']['cancel'][0]; ?>
                <?php if(!empty($domain['records']['new'][0])): ?>
                 (<?php echo round($domain['records']['cancel'][0]/$domain['records']['new'][0]*100); ?>%)
                <?php endif; ?>
            </td>
            <td style="border:1px solid #dfdfdf;"><?php echo $domain['records']['call'][0]; ?></td>
            <td style="border:1px solid #dfdfdf;">
                <?php echo $domain['records']['in'][0]; ?>
                <?php if(!empty($domain['records']['new'][0])): ?>
                 (<?php echo round($domain['records']['in'][0]/$domain['records']['new'][0]*100); ?>%)
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach;?>
        <tr>
            <td style="border:1px solid #dfdfdf; font-weight: bold;">Итого:</td>
            <td style="border:1px solid #dfdfdf; font-weight: bold;"><?php echo $dataBody['totals']['new'][0]; ?></td>
            <td style="border:1px solid #dfdfdf; font-weight: bold;">
                <?php echo $dataBody['totals']['confirmed'][0]; ?> 
                <?php if(!empty($domain['records']['new'][0])): ?>
                (<?php echo round($dataBody['totals']['confirmed'][0]/$dataBody['totals']['new'][0]*100); ?>%)
                <?php endif; ?>
            </td>
            <td style="border:1px solid #dfdfdf; font-weight: bold;">
                <?php echo $dataBody['totals']['cancel'][0]; ?>
                <?php if(!empty($dataBody['totals']['new'][0])): ?>
                 (<?php echo round($dataBody['totals']['cancel'][0]/$dataBody['totals']['new'][0]*100); ?>%)
                <?php endif; ?>
            </td>
            <td style="border:1px solid #dfdfdf; font-weight: bold;"><?php echo $dataBody['totals']['call'][0]; ?></td>
            <td style="border:1px solid #dfdfdf; font-weight: bold;"><?php echo $dataBody['totals']['in'][0]; ?>
                <?php if(!empty($domain['records']['new'][0])): ?>
                (<?php echo round($dataBody['totals']['in'][0]/$dataBody['totals']['new'][0]*100); ?>%)
                <?php endif; ?>
            </td>
        </tr>
        <?php endif;?>
    </tbody>
</table>