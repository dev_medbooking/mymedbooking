<div class="calendars-inner">
    <div class="top-calendars clearfix">
        <div class="left">
            <span class="btn active" data-number="0">Дата создания</span>
        </div>
        <div class="right">
            <span class="btn today active">Сегодня</span>
            <span class="btn yesterday">Вчера</span>
            <span class="btn week">За неделю</span>
        </div>
    </div>
    <div class="calendars"></div>
    <span class="calendar-submit" data-href="<?php echo Yii::app()->createUrl('/mbReport/'.Yii::app()->controller->action->id,array('host'=>(!empty($_GET['host'])?$_GET['host']:''),'status'=>(!empty($_GET['status'])?$_GET['status']:''))); ?>">Применить</span>
</div>