<?php


$description[] = <<<EOD
<div id="medbooking-doctor"></div>
EOD;
$description[] = <<<EOD
<script>
  var
    id_client = {$partner->id},
    num = "{$partner->phone}",
    url = "http://medbooking.com/symptom/boly-v-verkhnej-chasti-spiny?ajax=ref",
    scriptJs = document.createElement("script");
  scriptJs.src = 'http://medbooking.com/scripts/integrator/moduleDocCategoryForm/script.js';
  document.head.appendChild(scriptJs);
</script>
EOD;

$instruction[] = <<<EOD
Разместите этот HTML-код там, где вы планируете разместить наш виджет.
EOD;
$instruction[] = <<<EOD
Разместите наш JavaScript код перед закрывающим тегом </body>.
EOD;
?>

<?php foreach($description as $key => $val) :?>
  <div class="module-container clearfix">
    <div id="md-<?=$key;?>" class="module-block module-description">
      <textarea rows="15" cols="30"><?=CHtml::encode($description[$key]);?></textarea>
    </div>
    <div class="module-block module-instruction">
      <p><?=nl2br(CHtml::encode($instruction[$key]));?></p>
    </div>
  </div>
<?php endforeach;?>
