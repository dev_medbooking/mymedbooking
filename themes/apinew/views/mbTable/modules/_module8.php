<?php


$description = <<<EOD
<script>
  var
    id_client = {$partner->id},
    num = "{$partner->phone}",
    btnClass = "test",
    scriptJs = document.createElement("script");
  scriptJs.src = 'http://medbooking.com/scripts/integrator/moduleBtn/script.js';
  document.head.appendChild(scriptJs);
</script>
EOD;

$instruction = <<<EOD
Разместите наш JavaScript код перед закрывающим тегом </body>.
Заполните значение переменной btnClass классом, который используется для кнопок вызова формы.
Примеры:
<button class="test">Записаться на прием</button>
<a class="test" href="#">Записаться на прием</a>
Где test - название вашего класса.
EOD;

?>

<div class="module-container clearfix">
  <div class="module-block module-description">
    <textarea rows="15" cols="30"><?=CHtml::encode($description);?></textarea>
  </div>
  <div class="module-block module-instruction">
    <p><?=nl2br(CHtml::encode($instruction));?></p>
  </div>
</div>
