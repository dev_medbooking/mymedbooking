<?php

$description = <<<EOD
<script>
  var
    id_client = {$partner->id},
    num = "{$partner->phone}",
    url = "http://medbooking.com/search_doctor/otolaringolog?ajax=ref",
    scriptJs = document.createElement("script");
    scriptJs.src = '//medbooking.com/scripts/integrator/moduleDocCategoryL/script.js';
    document.head.appendChild(scriptJs);
</script>
EOD;
$instruction = <<<EOD
Чтобы добавить на ваш сайт наш партёрский модуль, разместите этот JavaScript код перед закрывающим тегом </body>.
EOD;
?>

<div class="module-container">
  <div class="module-block module-description">
    <textarea rows="15" cols="30"><?=CHtml::encode($description);?></textarea>
  </div>
  <div class="module-block module-instruction">
    <p><?=CHtml::encode($instruction);?></p>
  </div>
</div>
