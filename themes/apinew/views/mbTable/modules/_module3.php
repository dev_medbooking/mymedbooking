<?php

$description[] = <<<EOD
<div id="medbooking-doctor"></div>
EOD;
$description[] = <<<EOD
<script type="text/javascript" >
  var
    id_client = {$partner->id},
    num = "{$partner->phone}",
    url = "http://medbooking.com/search_doctor?ajax=ref",
    scriptJs = document.createElement("script");
  scriptJs.src = 'http://medbooking.com/scripts/integrator/part3/part3.js';
  document.head.appendChild(scriptJs);
</script>
EOD;

$instruction[] = <<<EOD
Разместите данный тег в том месте, где должен происходить вывод информации Medbooking.com
EOD;
$instruction[] = <<<EOD
Код должен быть расположен перед закрывающимся тегом - </body>
EOD;
?>

<?php foreach($description as $key => $val) :?>
  <div class="module-container clearfix">
    <div class="module-block module-description">
      <textarea rows="15" cols="30"><?=CHtml::encode($description[$key]);?></textarea>
    </div>
    <div class="module-block module-instruction">
      <p><?=nl2br(CHtml::encode($instruction[$key]));?></p>
    </div>
  </div>
<?php endforeach;?>
