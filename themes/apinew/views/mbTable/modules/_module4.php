<?php

$clientScript = Yii::app()->clientScript;
$clientScript->registerScript('change_module_widget', <<<EOD
$(function(){
  var destinationBlock = $('#md-0').find('textarea');
  $('[name="aside_style"]').on('click', function(){
    destinationBlock.text('<div id="moduleSidebar" class="'+$(this).attr('id')+'"></div>');
  });
  var radio = $('[name="aside_style"]');
  if (radio.size()) {
    radio[0].click();
  }
});
EOD
);


$description[] = <<<EOD
EOD;
$description[] = <<<EOD
<div id="medbooking-doctor"></div>
EOD;
$description[] = <<<EOD
<script type="text/javascript" >
  var id_client = {$partner->id},
    num = "{$partner->phone}",
    dragForm = true,
    searchFlag = "clinic",
    searchCategory = null,
    scriptJs = document.createElement("script");
  scriptJs.src = 'http://medbooking.com/scripts/integrator/sidebarModule/script.js';
  document.head.appendChild(scriptJs);
</script>
EOD;

$instruction[] = <<<EOD
Разместите этот HTML-код там где вы планируете разместить наш поисковой виджет.
EOD;
$instruction[] = <<<EOD
Разместите этот HTML-код там где вы планируете отобразить результаты поиска.
EOD;
$instruction[] = <<<EOD
Разместите наш JavaScript код перед закрывающим тегом </body>.
EOD;
?>

<p>Выберите подходящий вам вариант баннера:</p>

<div class="aside_style">
  <input type="radio" checked name="aside_style" id="bb">&nbsp;<label for="bb">Синий цвет width: 300px</label>
  <input type="radio" name="aside_style" id="bs">&nbsp;<label for="bs">Синий цвет width: 250px</label>
  <input type="radio" name="aside_style" id="wb">&nbsp;<label for="wb">Белый цвет width: 300px</label>
  <input type="radio" name="aside_style" id="ws">&nbsp;<label for="ws">Белый цвет width: 250px</label>
</div>

<script type="text/javascript">
  var id_client = <?=$partner->id;?>,
    num = "<?=$partner->phone;?>",
    dragForm = true,
    searchFlag = "clinic",
    searchCategory = null,
    scriptJs = document.createElement("script");
  scriptJs.src = 'http://medbooking.com/scripts/integrator/sidebarModule/script.js';
  document.head.appendChild(scriptJs);
</script>

<div id="moduleSidebar"></div>

<?php foreach($description as $key => $val) :?>
  <div class="module-container clearfix">
    <div id="md-<?=$key;?>" class="module-block module-description">
      <textarea rows="15" cols="30"><?=CHtml::encode($description[$key]);?></textarea>
    </div>
    <div class="module-block module-instruction">
      <p><?=nl2br(CHtml::encode($instruction[$key]));?></p>
    </div>
  </div>
<?php endforeach;?>
