<?php


$description[] = <<<EOD
<div id="moduleLineWrapper"></div>
EOD;
$description[] = <<<EOD
<script>
    var
      mb_line_num = "{$partner->phone}",
      txt = null,
      scriptJs = document.createElement("script");
    scriptJs.src = 'http://medbooking.com/scripts/integrator/moduleLine/script.js';
    document.head.appendChild(scriptJs);
</script>
EOD;

$instruction[] = <<<EOD
Разместите этот HTML-код там, где вы планируете отобразить строку с вашим индивидуальным номером телефона.
EOD;
$instruction[] = <<<EOD
Разместите наш JavaScript код перед закрывающим тегом </body>.
EOD;
?>

<?php foreach($description as $key => $val) :?>
  <div class="module-container clearfix">
    <div id="md-<?=$key;?>" class="module-block module-description">
      <textarea rows="15" cols="30"><?=CHtml::encode($description[$key]);?></textarea>
    </div>
    <div class="module-block module-instruction">
      <p><?=nl2br(CHtml::encode($instruction[$key]));?></p>
    </div>
  </div>
<?php endforeach;?>
