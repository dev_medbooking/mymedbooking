<div class="footerNavigation">
    
    <?php
    $modules = array();
    for ($i=1; $i <=8 ; ++$i) {
        array_push($modules, array(
            'label' => "Модуль {$i}",
            'url' => array('/mbTable/module', 'id' => $i),
        ));
    }
    $this->widget('zii.widgets.CMenu', array(
        'items' => $modules,
        'htmlOptions' => array(
            'class' => 'inlineList clearfix',
        ),
    ));
    ?>
	
</div>
<div class="footerAgreement">
	<a href="http://partner1.medbooking.com/agreement.php" class="pull-left" target="_blank">Пользовательское соглашение</a>
</div>