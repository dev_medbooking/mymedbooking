<?php if(!empty($model)):?>
    <?php foreach ($model as $key=>$value):?>
        <div class="row_p clearfix">
            <div class="cell_p cell_01"><?php echo !empty($value->id)?$value->id:'';?></div>
            <div class="cell_p cell_02"><?php echo !empty($value->create_time)?date('d.m.Y H:m' ,  strtotime($value->create_time)):'';?></div>
            <div class="cell_p cell_10"><?php echo !empty($value->partnerStatus)?$value->partnerStatus:'';?></div>
            <?php if (Yii::app()->user->checkAccess('Administrator')) :?>
				<div class="cell_p cell_11">
					<?=( ! empty($value->partner->name) ? $value->partner->name : '');?><!--<a href="<?=Yii::app()->createUrl('mbTable/deleteFromCabinet', array('id' => $value->id));?>">X</a>-->
				</div>
			<?php endif;?>
        </div>
    <?php endforeach;?>
<?php endif;?>