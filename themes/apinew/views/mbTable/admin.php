<section class="wrapper_p">
    <header class="header_p">
        <?php $this->renderPartial('_header', array(
            'partner' => $partner,
            'partners' => $partners,
            'phone' => $phone,
        )); ?>
        <div class="header__sortBox clearfix js-href-hidden" data-href="">
            <div class="left">
                <div class="header__sortBox__row">
                    <span class="header__sortBox__row__title">поиск по статусу</span>
                    <ul class="header__sortBox__row_btnItems clearfix">
	                    <li><a href="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$date_start,'date_end'=>$date_end,'limit'=>$limit,'status'=>''));?>">Все</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$date_start,'date_end'=>$date_end,'limit'=>$limit,'status'=>1));?>">Требующие ответа</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$date_start,'date_end'=>$date_end,'limit'=>$limit,'status'=>2));?>">Записанные</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$date_start,'date_end'=>$date_end,'limit'=>$limit,'status'=>3));?>">Отклоненные</a></li>
                    </ul>
                </div>
                <div class="header__sortBox__row clearfix">
                    <ul class="header__sortBox__row__times clearfix">
                        <li><a href="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$prev_month_first,'date_end'=>$prev_month_last,'limit'=>$limit,'status'=>!empty($status)?$status:0));?>">← отчетный период</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$month3,'date_end'=>$today,'limit'=>$limit,'status'=>!empty($status)?$status:0));?>">квартал</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$month,'date_end'=>$today,'limit'=>$limit,'status'=>!empty($status)?$status:0));?>">месяц</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$week,'date_end'=>$today,'limit'=>$limit,'status'=>!empty($status)?$status:0));?>">неделя</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$yesterday,'date_end'=>$yesterday,'limit'=>$limit,'status'=>!empty($status)?$status:0));?>">вчера</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$today,'date_end'=>$today,'limit'=>$limit,'status'=>!empty($status)?$status:0));?>">сегодня</a></li>
                    </ul>
                </div>
            </div>
            <div class="left data_billing">
                <form action="<?php echo Yii::app()->createUrl('mbTable/admin');?>">
                    <div class="header__sortBox__row header__sortBox__rowCalendar">
                        <span class="header__sortBox__row__titleBild">Дата биллинга:</span>
                        <input type="text" name="date_start" id="js-calend-date_start" value="<?=$date_start;?>"/>
                        <i>—</i>
                        <input type="text" name="date_end" id="js-calend-date_end" value="<?=$date_end;?>"/>
                        <input type="hidden" name="limit" value="<?php echo $limit;?>" />
                        <input type="hidden" name="status" value="<?php echo !empty($status)?$status:0;?>" />
                        <input type="submit" value=" " />
                    </div>
                </form>                
            </div>
            <div class="right">
                <form action="<?php echo Yii::app()->createUrl('mbTable/updatePartnerName');?>" class="js-partner-form">
                    <div class="header__sortBox__row">
                        <span class="header__sortBox__row__titleTreatment">Способ обращения</span>
                        <input type="text" name="PartnerName">
                        <input type="submit" value="изменить" />
                    </div>
                </form>
            </div>
        </div>
        <div class="header__showing clearfix">
            <div class="left left_0">

            </div>
            <div class="left">
                <span class="header__showing__titleShowing">Показывать по</span>
                <select class="js-select-box">
                    <option value="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$date_start,'date_end'=>$date_end,'limit'=>10,'status'=>!empty($status)?$status:0));?>" <?php if($limit==10):?>selected<?php endif;?>>10</option>
                    <option value="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$date_start,'date_end'=>$date_end,'limit'=>20,'status'=>!empty($status)?$status:0));?>" <?php if($limit==20):?>selected<?php endif;?>>20</option>
                    <option value="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$date_start,'date_end'=>$date_end,'limit'=>50,'status'=>!empty($status)?$status:0));?>" <?php if($limit==50):?>selected<?php endif;?>>50</option>
                </select>
                <i> записей</i>
            </div>
            <div class="right">
                <form action="<?php echo Yii::app()->createUrl('mbTable/admin',array('date_start'=>$date_start,'date_end'=>$date_end,'limit'=>$limit,'status'=>!empty($status)?$status:0));?>" class="js-search-form">
                <span class="header__showing__titleSearch">Поиск</span>
                 
                    <input type="text" name="MbRecord[name]" value="<?php echo !empty($name)?$name:'';?>">
                </form>
            </div>
        </div>
    </header>
    <section class="content_p">
        <div class="table_p">
            <?php $this->renderPartial('//mbTable/filter',array('sort'=>$sort)); ?>
            <div class="js-update-data">
                <?php $this->renderPartial('//mbTable/data',array('model'=>$data)); ?>
            </div>
        </div>
    </section>
    <div class="pagination row" id="pages">
        <?php if (!empty($pages) && !empty($count)): ?>
            <?php $this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count)); ?>
        <?php endif; ?>
    </div>
    <?php $this->renderPartial('_footer') ?>
</section>
