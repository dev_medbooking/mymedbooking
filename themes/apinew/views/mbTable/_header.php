<div class="header__description">
    <p><strong><?=( ! empty($partner) ? $partner->recordInfo['all_record'] : '0');?></strong><span class="vhr">|</span>к оплате <strong><?=( ! empty($partner) ? $partner->recordInfo['success_record'] : '0');?></strong><span class="vhr">|</span>на сумму <strong class="cost"><?=( ! empty($partner) ? $partner->recordInfo['price'] : '0');?></strong><sup> руб</sup></p>
    <p>Среднее время обработки заявок <strong><?php echo!empty($partner->RecordTimeAverage)?$partner->RecordTimeAverage:'0';?></strong></p>
    <?php if ($partner) { ?>
        <span class="userInfo"><?=$partner->name?> — <a class="logout" href="/logout">Выход</a>
	            <span class="userEmail"><span class="userPhone">Телефон: <?=$partner->phone;?></span>&nbsp;&nbsp;&nbsp;<?=$partner->usr->email?> – ID: <?=$partner->ident_id?></span>
            </span>
    <?php } ?>
    <?php if ( ! empty($partners) AND ! Yii::app()->session->get("partnerID")) :?>
        <div class="gotoUser">
            <form action="<?=Yii::app()->createUrl('mbTable/viewAsPartner');?>" method="post">
                <select name="ident_id">
                    <?php foreach ($partners as $v) :?>
                        <option value="<?=$v->ident_id;?>"><?=$v->name;?></option>
                    <?php endforeach;?>
                </select>
                <input type="submit" value="Посмотреть как партнер">
            </form>
        </div>
    <?php elseif (Yii::app()->session->get("partnerID")) :?>
        <div class="gotoUser back">
            <form action="<?=Yii::app()->createUrl('mbTable/logoutAsPartner');?>" method="post">
                <input type="submit" value="Вернуться к исходному юзеру">
            </form>
        </div>
    <?php endif;?>
</div>