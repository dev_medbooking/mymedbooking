<section class="wrapper_p">
    <header class="header_p">
        <?php $this->renderPartial('_header', array(
            'partner' => $partner,
            'partners' => $partners,
            'phone' => $phone,
        ));?>
    </header>
    <section class="content_p">
        <?php $this->renderPartial("modules/_module{$id}", array('partner' => $partner));?>
    </section>
    <?php $this->renderPartial('_footer');?>
</section>
