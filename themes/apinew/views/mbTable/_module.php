<?php if ( ! empty($id)) :?>
	<?php $this->render('module', array('id' => $id, 'partner' => $partner, 'partners' => $partners, 'phone' => $phone));?>
<?php else :?>
	<?php $this->render('modules', array('partner' => $partner, 'partners' => $partners, 'phone' => $phone));?>
<?php endif;?>