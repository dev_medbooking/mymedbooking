<div id="create">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'create-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'form-horizontal'),
    )); ?>
        <?php if($model->errors): ?>
        <div class="well">
            <?php echo $form->errorSummary($model); ?>
        </div>
        <?php endif; ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'clinic',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'clinic',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'title',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'alt',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'alt',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'price',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'price',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'subway',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'subway',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'status',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList($model,'status',array(0=>'Создано',1=>'Специально')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="form-actions">
            <?php echo CHtml::submitButton("Сохранить",array("class"=>"btn btn-info"));?>
            <?php echo CHtml::resetButton("Сбросить",array("class"=>"btn"));?>
        </div>
    <?php $this->endWidget(); ?>
</div>