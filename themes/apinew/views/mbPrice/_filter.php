<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id',null,array('class'=>'csorting')); ?></td>
        <td class="title<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('title',null,array('class'=>'csorting')); ?></td>
        <td class="alt<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('alt',null,array('class'=>'csorting')); ?></td>
        <td class="subway<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('subway',null,array('class'=>'csorting')); ?></td>
        <td class="create_time<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('create_time',null,array('class'=>'csorting')); ?></td>
        <td class="update_time<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('update_time',null,array('class'=>'csorting')); ?></td>
        <td class="uid<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('uid',null,array('class'=>'csorting')); ?></td>
        <td class="author_id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('author_id',null,array('class'=>'csorting')); ?></td>
        <td class="edit"></td>
        <td class="view"></td>
        <td class="delete"></td>
        <td class="icon-share"></td>
    </tr>
    <tr class="filters">
        <td><?php echo CHtml::activeTextField($model,'id',array('class'=>'search-single','id'=>'filter_id')); ?></td>
        <td><?php echo CHtml::activeTextField($model,'title',array('class'=>'search','id'=>'filter_title')); ?></td>
        <td><?php echo CHtml::activeTextField($model,'alt',array('class'=>'search','id'=>'filter_alt')); ?></td>
        <td><?php echo CHtml::activeTextField($model,'subway',array('class'=>'search','id'=>'filter_subway')); ?></td>
        <td><?php echo CHtml::activeTextField($model,'create_time',array('class'=>'search','id'=>'filter_create_time')); ?></td>
        <td><?php echo CHtml::activeTextField($model,'update_time',array('class'=>'search','id'=>'filter_update_time')); ?></td>
        <td><?php echo CHtml::activeTextField($model,'uid',array('class'=>'search','id'=>'filter_uid')); ?></td>
        <td><?php echo CHtml::activeTextField($model,'author_id',array('class'=>'search','id'=>'filter_author_id')); ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</thead>