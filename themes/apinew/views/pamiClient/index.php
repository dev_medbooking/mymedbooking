<?if (isset($data) || isset($model)): ?>
    <script src="/themes/apinew/js/jquery.datetimepicker.full.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <link href="/themes/apinew/css/jquery.datetimepicker.css" rel="stylesheet" />
<?php if(Yii::app()->user->checkAccess('Administrator')||Yii::app()->user->checkAccess('Taccounter')||Yii::app()->user->checkAccess('Toperator')||Yii::app()->user->checkAccess('Team')||Yii::app()->user->checkAccess('Operator')||Yii::app()->user->checkAccess('NDasha')): ?>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <table  cellspacing="0">
        <tr>
            <?php if(isset($model->action)):?>
                <td style="vertical-align:top" width="200">
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'admin-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => true,
                        'htmlOptions' => array('class' => 'application-form'),
                        'action' => array('/pamiClient/'),
                    )); ?>
                    <?= $form->hiddenField($model, 'action') ?>
                    <?if($model->action==-1):?>
                        Укажите причину:<br>
                        <?php echo $form->dropDownList($model, 'reason', array("15 минут" => "15 минут","обед" => "обед", "технический перерыв" => "технический перерыв","перерыв на доп.обработку" => "перерыв на доп.обработку", "занят" => "занят", "мониторинг" => "мониторинг", "окончание рабочего дня" => "окончание рабочего дня", "технические проблемы" => "технические проблемы")); ?><br>
                    <?endif?>
                    <button class="save_btn"><?php echo $model->button_value?></button>
                    <?php $this->endWidget(); ?>
                </td>
            <?php endif;?>

            <?php if (!empty($data)) :?>
                <td style="vertical-align:top"  width="500">
                    <table>
                        <tr align="top">
                            <td style="vertical-align:top" colspan="5">Текущие разговоры:</td>
                        </tr>
                        <?php foreach ($data as $key=>$value):?>
                            <tr>
                                    <?php if($value['caller_id_num']) :?>
                                        <td  style="vertical-align:top" width="200"><a href="<?php echo Yii::app()->createUrl('/pamiClient/index'); ?>?channel=<?=$value['channel']?>"><?=$value['caller_id_num']?> - <?=(isset($value['connectedlinenum'])?$value['connectedlinenum']:"");?></a></td>
                                        <td width="60"><?=(isset($value['operator'])?$value['operator']:"")?></td>
                                        <td width="60"><?=(isset($value['operator1'])?$value['operator1']:"")?></td>
                                        <td><?=(isset($value['notes'])?$value['notes']:"")?></td>
                                        <td><?=(isset($value['notes1'])?$value['notes1']:"")?></td>
                                        <?//php else :?>
                                        <!---> <!--<?//=($value['dnid']!=''?$value['dnid']:$value['connectedlinenum']);?>-->
                                    <?php endif;?>
                            </tr>
                        <?php endforeach;?>
                        <tr>
                            <td>
                                <?php
                                echo $form = CHtml::beginForm(CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId())),'post');
                                echo CHtml::submitButton("Обновить");
                                echo CHtml::endForm();
                                ?>
                            </td>
                        </tr>
                    </table>
                </td>
            <?php endif;?>

                <td  style="vertical-align:top"  width="600">
                    <table d="1">
                        <tr>
                            <td  style="vertical-align:top">
                                Записи разговоров за:
                                <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'admin-form',
                                    'enableAjaxValidation' => false,
                                    'enableClientValidation' => true,
                                    'htmlOptions' => array('class' => 'application-form'),
                                )); ?>
                                <?php if ($model->errors): ?>
                                    <div class="well"><?php echo $form->errorSummary($model); ?></div>
                                <?php endif; ?>
                                С: <?php echo $form->textField($model, 'dateTimeFrom', array("id" => "dateTimeFrom")); ?><br>
                                По: <?php echo $form->textField($model, 'dateTimeTo', array("id" => "dateTimeTo")); ?><br>
                                Телефон: <?php echo $form->textField($model, 'searchPhone', array()); ?><br>
                                Оператор: <?php echo $form->textField($model, 'operator', array()); ?><br>
                                <? echo CHtml::submitButton("Получить",array('name'=>'getFTPList'));?>
                                <?php $this->endWidget(); ?>
                                <audio controls  id="player" type="audio/mpeg" autoplay style='display:none'></audio>
<!--                                <iframe id="player" width="100%" height="120px" frameborder="0" scrolling="auto" ></iframe>-->
                                <?php if (!empty($ftp_list_db)) :?>
                                    <br><b>Всего записей <?php echo count($ftp_list_db)?></b>
                                    <table>
                                    <?php foreach ($ftp_list_db as $key=>$value):?>
                                        <tr>
                                        <td width="30"><a href=callto:<?=$value['src']?>><?=$value['src']?></a></td><td width="40" align="center"> -> </td><td width="100"><a href=callto:<?=$value['dst']?>><?=$value['dst']?></a></td>
                                        <td width="180"><?=$value['dateprint']?> <?=$value['time']?><td>
                                        <td width="50" style="align-content: center"><?=$value['pamiExt']?></td>
                                        <td width="40"><a href="<?php echo Yii::app()->createUrl('/pamiClient/play',array("f"=>$value['date'].$value['userfield'].".mp3")); ?>">D:<?=$value['duration']?>|B:<?=$value['billsec']?></a>&nbsp;</td>
                                        <?php if (!empty($value['pamiDomain'])) :?>
                                            <td width="40"><a href="<?php echo Yii::app()->createUrl('/mbRecord/create',array("pamiDomain"=>$value['pamiDomain'],"pamiTelephone" => $value['pamiTelephone'])); ?>">+</a></td>
                                        <?php else :?>
                                            <td width="40">&nbsp;</td>
                                        <?php endif;?>
                                            <td class="player" width='20px' title="Прослушать разговор"  style='cursor: pointer' data-file="<?php echo $value['date'].$value['userfield'].".mp3"?>"> <i class="fa fa-play-circle-o"></i>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                    </table>
                                <?//php else :?>
                                    <!--Данные отсутствуют-->
                                <?php endif;?>
                            </td>
                        </tr>
                    </table>
                </td>
<!--------------------------------------     БЛОК ДОБАВЛЕНИЯ / УДАЛЕНИЯ ИЗ ОЧЕРЕДИ      ------------------------------------------------------------------>
<?php
if(false
|| Yii::app()->user->checkAccess('Administrator')
|| true
&& in_array(Yii::app()->user->getUserData()['email'], Yii::app()->params['adminOperatorQueue'])
&& Yii::app()->user->checkAccess('Toperator')
) {
?>
            <td  style="vertical-align:top"  width="300">
            <table>
                <tr>
                    <td  style="vertical-align:top">
                        <?php
                        if (Yii::app()->user->hasFlash('ext_result')) {
                            $result = Yii::app()->user->getFlash('ext_result');
                            $class = (empty($result['success']) ? 'error' : 'success');
                            $message = $result['message'];
                            ?><div class="ext_result <?=$class?>"><?=CHtml::encode($message)?></div><?php
                        }
                        ?>
            <?php
                        echo $form = CHtml::beginForm(CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId())),'post');
                        echo "Очередь: <SELECT name='queue_name'>";
                        foreach ($model->queue_name_array as $key=>$queue_name) {
                            echo "<option value='".$queue_name."'>".$queue_name;
                        }
                        echo "</SELECT><br>";
                        echo "<br>Добавить номер в очередь: <input type=text name='add_ext'><br>";
                        echo "PENALTY:<br> <input type=text name='penalty' value=0><br>";
                        echo "<br><br>Удалить номер из очереди: <input type=text name='remove_ext'><br>";
                        echo CHtml::submitButton();
                        echo CHtml::endForm();
            ?>
                    </td>
                </tr>
            </table>
            </td>
<?php } ?>
<!--------------------------------------     БЛОК ДОБАВЛЕНИЯ / УДАЛЕНИЯ ИЗ ОЧЕРЕДИ       ------------------------------------------------------------------>

            <?php if(Yii::app()->user->checkAccess('Administrator')||Yii::app()->user->checkAccess('Toperator')) :?>
                <td  style="vertical-align:top"  width="500">
                    <table border="1">
                        <tr>
                            <td>Номер</td><td>Оператор</td><td>Состояние</td><td>Комментарий</td><td>Дата</td><td>Время отсутствия (ч.м)</td>
                        </tr>
                        <?php foreach ($operator_status as $key=>$value):?>
                            <tr>
                                <td><?=$value["extention"]?></td><td><?=$value["comments"]?></td><td><?=$value["action"]?></td><td><?=$value["reason"]?></td><td><?=$value["create_time"]?></td><td><?=$value["time_left"]?></td>
                            </tr>
                        <?php endforeach;?>
                    </table>
                </td>
            <?php endif;?>

            <?php if(Yii::app()->user->checkAccess('Administrator')||Yii::app()->user->checkAccess('Toperator')) :?>
                <td  style="vertical-align:top;"  width="200">
                    <a href="<?php echo Yii::app()->createUrl('/operatorDuty/admin/'); ?>">Дежурство операторов</a>
                </td>
            <?php endif;?>


        </tr>
    </table>
<?php endif;?>
<?endif?>
<script type="text/javascript">
    $(function() {
        $(document).on('click','td.player',function(){

            $('tr').css('background-color', '');
            $(this).parent('tr').css('background-color', 'yellow');
            $.post( "/pamiClient/playCall?f="+$(this).data('file'), function( data ) {
                $('audio#player').show();
                $('audio#player').attr('src',data);
            });
            //document.location='/pamiClient/playCall?f='+$(this).data('file');
            //$('iframe#player').attr('src','/pamiClient/playCall?f='+$(this).data('file'));
        })

        $('#dateTimeFrom').datetimepicker({
            lang: "ru",
            dayOfWeekStart: 1,
            step: 5,
            onSelect: function(dateText, inst) {
                var miliseconds = 2628000000;
                var currentTime = new Date();
                var selectedDate = new Date(dateText);
            }
        });
        $('#dateTimeTo').datetimepicker({
            lang: "ru",
            dayOfWeekStart: 1,
            step: 5,
            onSelect: function(dateText, inst) {
                var miliseconds = 2628000000;
                var currentTime = new Date();
                var selectedDate = new Date(dateText);
            }
        });
    })
</script>
