<div class="content-top-menu-section clearfix">
    <ul class="items-project clearfix">
        <?php foreach ($domains as $domain): $class=explode(".",$domain->title); ?>
        <li class="<?php echo $class[0]; if ($currentDomain->id == $domain->id) echo ' active'; ?> clearfix"><?php echo CHtml::link(Yii::t('app', $domain->title), array('/mbContent/admin', 'did' => $domain->id)); ?></li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="row-fluid">
    <div class="span12">
    <?php
    $entityColumnStr = array(
        'clinics' => array(
            array(
                'name' => 'id',
                'header' => 'ID'
            ),
            array(
                'name' => 'title',
                'type' => 'raw',
                'header' => 'Наименование',
                'value' => 'CHtml::link($data["title"], "http://' . $currentDomain->title . '/clinic/$data[translit]", array("target" => "_blank"))',
            ),
            array(
                'type' => 'raw',
                'header' => 'Редактировать',
                'value' => 'CHtml::link("Редактировать", "http://' . $currentDomain->title . '/adminClinic/update?id=$data[id]", array("target" => "_blank"))',
            ),
            array(
                'name' => 'status',
                'header' => 'Статус',
                'type' => 'raw',
                'value' => 'getStatus($data, "clinics", "' . $currentDomain->title . '")',
                'filter' => getEntityStatusesList("clinics", $currentDomain->title)
            ),
            array(
                'name' => 'author',
                'header' => 'Автор',
            ),
            array(
                'name' => 'rating',
                'header' => 'Рейтинг'
            ),
            array(
                'name' => 'is_desc',
                'header' => 'Кр. описание',
                'type' => 'raw',
                'value' => '($data["is_desc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_det_desc',
                'header' => 'Полное описание',
                'type' => 'raw',
                'value' => '($data["is_det_desc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_img',
                'header' => 'Лого',
                'type' => 'raw',
                'value' => '($data["is_img"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_address',
                'header' => 'Адрес',
                'type' => 'raw',
                'value' => '($data["is_address"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mtitle',
                'header' => 'MTitle',
                'type' => 'raw',
                'value' => '($data["is_mtitle"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mdesc',
                'header' => 'MDesc',
                'type' => 'raw',
                'value' => '($data["is_mdesc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mkeys',
                'header' => 'MKeys',
                'type' => 'raw',
                'value' => '($data["is_mkeys"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'reviews',
                'header' => 'Отзывов'
            ),
        ),
        'doctors' => array(
            array(
                'name' => 'id',
                'header' => 'ID'
            ),
            array(
                'name' => 'fio',
                'header' => 'ФИО',
                'type' => 'raw',
                'value' => 'CHtml::link($data["fio"], "http://' . $currentDomain->title . '/doctor/$data[translit]", array("target" => "_blank"))',
            ),
            array(
                'type' => 'raw',
                'header' => 'Редактировать',
                'value' => 'CHtml::link("Редактировать", "http://' . $currentDomain->title . '/adminDoctor/update?id=$data[id]", array("target" => "_blank"))',
            ),
            array(
                'name' => 'status',
                'header' => 'Статус',
                'type' => 'raw',
                'value' => 'getStatus($data, "doctors", "' . $currentDomain->title . '")',
                'filter' => getEntityStatusesList("doctors", $currentDomain->title)
            ),
            array(
                'name' => 'author',
                'header' => 'Автор',
            ),
            array(
                'name' => 'rating',
                'header' => 'Рейтинг'
            ),
            array(
                'name' => 'is_desc',
                'header' => 'Кр. описание',
                'type' => 'raw',
                'value' => '($data["is_desc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_img',
                'header' => 'Фото',
                'type' => 'raw',
                'value' => '($data["is_img"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_exp',
                'header' => 'Стаж',
                'type' => 'raw',
                'value' => '($data["is_exp"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mtitle',
                'header' => 'MTitle',
                'type' => 'raw',
                'value' => '($data["is_mtitle"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mdesc',
                'header' => 'MDesc',
                'type' => 'raw',
                'value' => '($data["is_mdesc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mkeys',
                'header' => 'MKeys',
                'type' => 'raw',
                'value' => '($data["is_mkeys"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_clinic',
                'header' => 'Клиника',
                'type' => 'raw',
                'value' => '($data["is_clinic"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_subway',
                'header' => 'Метро',
                'type' => 'raw',
                'value' => '($data["is_subway"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'reviews',
                'header' => 'Отзывов'
            ),
        ),
        'illnesses' => array(
            array(
                'name' => 'id',
                'header' => 'ID'
            ),
            array(
                'name' => 'title',
                'type' => 'raw',
                'header' => 'Наименование',
                'value' => 'CHtml::link($data["title"], getViewLink($data, "illnesses", "' . $currentDomain->title . '"), array("target" => "_blank"))',
            ),
            array(
                'type' => 'raw',
                'header' => 'Редактировать',
                'value' => 'CHtml::link("Редактировать", getEditLink($data, "illnesses", "' . $currentDomain->title . '"), array("target" => "_blank"))',
            ),
            array(
                'name' => 'status',
                'header' => 'Статус',
                'type' => 'raw',
                'value' => 'getStatus($data, "illnesses", "' . $currentDomain->title . '")',
                'filter' => getEntityStatusesList("illnesses", $currentDomain->title)
            ),
            array(
                'name' => 'author',
                'header' => 'Автор',
            ),
            array(
                'name' => 'cat_title',
                'header' => 'Категория'
            ),
            array(
                'name' => 'is_desc',
                'header' => 'Кр. описание',
                'type' => 'raw',
                'value' => '($data["is_desc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_det_desc',
                'header' => 'Полное описание',
                'type' => 'raw',
                'value' => '($data["is_det_desc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mtitle',
                'header' => 'MTitle',
                'type' => 'raw',
                'value' => '($data["is_mtitle"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mdesc',
                'header' => 'MDesc',
                'type' => 'raw',
                'value' => '($data["is_mdesc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mkeys',
                'header' => 'MKeys',
                'type' => 'raw',
                'value' => '($data["is_mkeys"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
        ),
        'faq' => array(
            array(
                'name' => 'id',
                'header' => 'ID'
            ),
            array(
                'name' => 'title',
                'type' => 'raw',
                'header' => 'Наименование',
                'value' => 'CHtml::link($data["title"], getViewLink($data, "faq", "' . $currentDomain->title . '"), array("target" => "_blank"))',
            ),
            array(
                'type' => 'raw',
                'header' => 'Редактировать',
                'value' => 'CHtml::link("Редактировать", getEditLink($data, "faq", "' . $currentDomain->title . '"), array("target" => "_blank"))',
            ),
            array(
                'name' => 'status',
                'header' => 'Статус',
                'type' => 'raw',
                'value' => 'getStatus($data, "faq", "' . $currentDomain->title . '")',
                'filter' => getEntityStatusesList("faq", $currentDomain->title)
            ),
            array(
                'name' => 'author',
                'header' => 'Автор',
            ),
            array(
                'name' => 'cat_title',
                'header' => 'Категория'
            ),
            array(
                'name' => 'is_desc',
                'header' => 'Кр. описание',
                'type' => 'raw',
                'value' => '($data["is_desc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_det_desc',
                'header' => 'Полное описание',
                'type' => 'raw',
                'value' => '($data["is_det_desc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mtitle',
                'header' => 'MTitle',
                'type' => 'raw',
                'value' => '($data["is_mtitle"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mdesc',
                'header' => 'MDesc',
                'type' => 'raw',
                'value' => '($data["is_mdesc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mkeys',
                'header' => 'MKeys',
                'type' => 'raw',
                'value' => '($data["is_mkeys"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
        ),
        'search' => array(
            array(
                'name' => 'id',
                'header' => 'ID',
            ),
            array(
                'name' => 'meta_title',
                'header' => 'META title',
            ),
            array(
                'name' => 'url',
                'header' => 'URL',
                'type' => 'raw',
                'value' => 'CHtml::link($data["url"], "http://' . $currentDomain->title . '$data[url]", array("target" => "_blank"))',
            ),
            array(
                'type' => 'raw',
                'header' => 'Редактировать',
                'value' => 'CHtml::link("Редактировать", getEditLink($data, "search", "' . $currentDomain->title . '"), array("target" => "_blank"))',
            ),
            array(
                'name' => 'is_mtitle',
                'header' => 'MTitle',
                'type' => 'raw',
                'value' => '($data["is_mtitle"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mdesc',
                'header' => 'MDesc',
                'type' => 'raw',
                'value' => '($data["is_mdesc"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_mkeys',
                'header' => 'MKeys',
                'type' => 'raw',
                'value' => '($data["is_mkeys"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_top',
                'header' => 'В.текст',
                'type' => 'raw',
                'value' => '($data["is_top"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
            array(
                'name' => 'is_bottom',
                'header' => 'Н.текст',
                'type' => 'raw',
                'value' => '($data["is_bottom"] == 1) ? "+": "-"',
                'filter'=>array(0=>'-',1=>'+'),
            ),
        ),
    );
    $rowHtmlOptionsExpression = array(
        'clinics' => 'isClinicSelected($data)',
        'doctors' => 'isDoctorSelected($data)',
        'illnesses' => 'isIllnessSelected($data)',
        'faq' => 'isFaqItemSelected($data)',
        'search' => 'isSearchItemSelected($data)',
    );
    $entityTitles = array(
        'clinics' => 'Клиники',
        'doctors' => 'Доктора',
        'illnesses' => 'Заболевания',
        'faq' => 'Вопросы и ответы',
        'search' => 'Выдача',
    );
    function isClinicSelected($data)
    {
        $requiredFields = array(
            'is_desc',
            'is_img',
            'is_address',
            'is_mtitle',
            'is_mdesc',
            'is_mkeys',
            'is_det_desc',
            'is_subway',
        );
        foreach ($requiredFields as $field) {
            if (isset($data[$field]) && $data[$field] == 0) {
                return array(
                    'class' => 'error'
                );
            }
        }
        return array(
            'class' => ''
        );
    }
    function isDoctorSelected($data)
    {
        $requiredFields = array(
            'is_desc',
            'is_img',
            'is_exp',
            'is_mtitle',
            'is_mdesc',
            'is_mkeys',
            'is_clinic',
            'is_subway',
        );
        foreach ($requiredFields as $field) {
            if (isset($data[$field]) && $data[$field] == 0) {
                return array(
                    'class' => 'error'
                );
            }
        }
        return array(
            'class' => ''
        );
    }
    function isIllnessSelected($data)
    {
        $requiredFields = array(
            'is_desc',
            'is_det_desc',
            'is_mtitle',
            'is_mdesc',
            'is_mkeys',
        );
        foreach ($requiredFields as $field) {
            if (isset($data[$field]) && $data[$field] == 0) {
                return array(
                    'class' => 'error'
                );
            }
        }
        return array(
            'class' => ''
        );
    }
    function isFaqItemSelected($data)
    {
        $requiredFields = array(
            'is_desc',
            'is_det_desc',
            'is_mtitle',
            'is_mdesc',
            'is_mkeys',
        );
        foreach ($requiredFields as $field) {
            if (isset($data[$field]) && $data[$field] == 0) {
                return array(
                    'class' => 'error'
                );
            }
        }
        return array(
            'class' => ''
        );
    }
    function isSearchItemSelected($data)
    {
        $requiredFields = array(
            'is_mtitle',
            'is_mdesc',
            'is_mkeys',
            'is_top',
            'is_bottom',
        );
        foreach ($requiredFields as $field) {
            if (isset($data[$field]) && $data[$field] == 0) {
                return array(
                    'class' => 'error'
                );
            }
        }
        return array(
            'class' => ''
        );
    }
    function getStatusesList()
    {
        return array(
            'timetovisit.ru' => array(
                'clinics' => array(0 => 'Модерировано включено', 1 => 'Не модерировано откючено', 2 => 'Модерировано отключено', 3 => 'Не модерировано включено'),
                'doctors' => array(0 => 'Не показывать', 1 => 'Показывать'),
                'illnesses' => array(0 => 'Не показывать', 1 => 'Показывать'),
                'faq' => array(0 => 'Не показывать', 1 => 'Показывать'),
            ),
            'medbooking.com' => array(
                'clinics' => array(0 => 'Не показывать', 1 => 'Показывать'),
                'doctors' => array(0 => 'Не показывать', 1 => 'Показывать'),
                'illnesses' => array(0 => 'Показывать', 1 => 'Не показывать'),
                'faq' => array(0 => 'Показывать', 1 => 'Не показывать'),
            ),
        );
    }
    function getEntityStatusesList($entity, $domainName)
    {
        static $statuses;
        if (isset($statuses) == false) {
            $statuses = getStatusesList();
        }
        if (isset($statuses[$domainName]) == false) {
            return array();
        }
        if (isset($statuses[$domainName][$entity]) == false) {
            return array();
        }
        return $statuses[$domainName][$entity];
    }
    function getStatus($data, $entity, $domainName)
    {
        static $statuses;
        if (isset($statuses) == false) {
            $statuses = getStatusesList();
        }
        if (isset($statuses[$domainName]) == false) {
            return 'Неизвестно';
        }
        if (isset($statuses[$domainName][$entity]) == false) {
            return 'Неизвестно';
        }
        if (isset($statuses[$domainName][$entity][$data['status']]) == false) {
            return 'Неизвестно';
        }
        return $statuses[$domainName][$entity][$data['status']];
    }
    function getEditLink($data, $entity, $domainName)
    {
        static $domainSettings;
        if (isset($domainSettings) == false) {
            $domainSettings = array(
                'timetovisit.ru' => array(
                    'illnesses' => '/adminNodeIllness/',
                    'faq' => '/adminNodeFaq/',
                    'search' => '/adminAlias/',
                ),
                'medbooking.com' => array(
                    'illnesses' => '/adminBlogIllness/',
                    'faq' => '/adminBlogFaq/',
                    'search' => '/adminCategory/',
                ),
            );
        }
        if (isset($domainSettings[$domainName]) == false) {
            return '#';
        }
        if (isset($domainSettings[$domainName][$entity]) == false) {
            return '#';
        }
        return 'http://' . $domainName . $domainSettings[$domainName][$entity] . 'update?id=' . $data['id'];
    }
    function getViewLink($data, $entity, $domainName)
    {
        static $domainSettings;
        if (isset($domainSettings) == false) {
            $domainSettings = array(
                'timetovisit.ru' => array(
                    'illnesses' => '/illness/page/',
                    'faq' => '/about/faq/Blogs/',
                ),
                'medbooking.com' => array(
                    'illnesses' => '/illness/',
                    'faq' => '/faq/',
                ),
            );
        }
        if (isset($domainSettings[$domainName]) == false) {
            return '#';
        }
        if (isset($domainSettings[$domainName][$entity]) == false) {
            return '#';
        }
        return 'http://' . $domainName . $domainSettings[$domainName][$entity] . $data['translit'];
    }
    foreach ($models as $entity => $model) {
        if (empty($model)) {
            continue;
        }
        echo "<h3>$entityTitles[$entity]</h3><hr>";
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => $entity . '-grid',
            'filter' => $model,
            'dataProvider' => $model->search($currentDomain),
            'itemsCssClass' => 'table table-striped table-bordered table-hover',
            'columns' => $entityColumnStr[$entity],
            'rowHtmlOptionsExpression' => $rowHtmlOptionsExpression[$entity],
            'selectableRows' => 1
        ));
    }
    ?>
    </div>
</div>