<div class="wrap_form">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
    )); ?>
        <?php echo $form->errorSummary($model);?>
        <?php echo $form->textField($model,'username',array('placeholder'=>'Телефон или логин','class'=>'login_input'));?>
        <?php echo $form->passwordField($model,'password',array('placeholder'=>'Пароль','class'=>'password_input'));?>
        <div class="row">
            <?php echo CHtml::submitButton("Войти");?>
        </div>
    <?php $this->endWidget();?>
</div>