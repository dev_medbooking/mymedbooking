<section id="content">
    <div class="center">
        <div class="summary" id="summary">
            <?=$this->renderPartial('//layouts/_admin_count', array('count' => $count));?>
        </div>
        <div class="table table_law all_law" data-href="<?=Yii::app()->createUrl('mbLawReports/law',array('t'=>1));?>">
            <div class="filter-table"><?=$this->renderPartial('//mbLawReports/_filter', array('sort' => $sort, 'model' => $model));?></div>
            <div class="body-table"><?=$this->renderPartial('_law', array('data' => $data));?></div>
        </div>
    </div>
    <div class="pagination row" id="pages">
        <?php if ( ! empty($pages) AND ! empty($count)): ?>
            <?=$this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count)); ?>
        <?php endif;?>
    </div>
</section>
