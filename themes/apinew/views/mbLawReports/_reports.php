<?php if( ! empty($data)): ?>
	<?php foreach($data as $key => $value): ?>
		<?php if( ! empty($value->primaryKey)): ?>

			<?php if ( ! empty($value->law_reports)) :?>
				<?php foreach($value->law_reports as $law_reports) :?>
					<?php $this->renderPartial('_data', array('model' => $value, 'law_reports' => $law_reports, 'key' => $key, 'params' => $params)); ?>
				<?php endforeach;?>
			<?php else :?>
				<?php $this->renderPartial('_data', array('model' => $value, 'law_reports' => NULL, 'key' => $key, 'params' => $params)); ?>
			<?php endif; ?>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>