<?php if( ! empty($data)): ?>
	<?php foreach($data as $key => $value): ?>
		<?php $this->renderPartial('_data', array('model' => $value, 'params' => $params)); ?>
	<?php endforeach; ?>
<?php endif; ?>