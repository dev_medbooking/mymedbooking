<div class="application-content _application-content">
    <?php if(!empty($model->id)): ?><p><strong><?php echo $model->title; ?></strong></p><?php endif;?>
</div>
<div id="create-block" class="_create_block _create_clinic">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'application-form'),
    )); ?>
        <?php if($model->errors): ?><div class="well"><?php echo $form->errorSummary($model); ?></div><?php endif; ?>
		<div class="row clearfix">
			<div class="text-name">
				<span class="title-calendar">Рейтинг на медбукинг</span>
				<?php echo $form->textField($model->rating,'rate',array('placeholder'=>'Рейтинг на медбукинг','class'=>'request')); ?>
			</div>
		</div>
        <div class="row clearfix">
            <button class="_save_btn">Сохранить</button>
        </div>
    <?php $this->endWidget(); ?>
</div>
