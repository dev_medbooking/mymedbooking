<?php
$this->widget('CLinkPager', array(
	'maxButtonCount' => 10,
	'cssFile' => FALSE,
	'htmlOptions' => array(
		'class' => 'paginator iblock'
	),
	'pages' => $pages,
	'header' => '',
	'firstPageLabel' => '&laquo;',
	'lastPageLabel' => '&raquo;',
	'nextPageLabel' => '&#8250;',
	'prevPageLabel' => '&#8249;'
));