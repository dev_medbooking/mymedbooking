<div class="row line clearfix">
    <div class="cell number"><?= $model->id; ?></div>
<!--
    <div class="cell print_clinic">
        <a data-href="<?//= Yii::app()->createUrl('mbLawReports/exelAdminLawSinglePeriod', array('id' => $model->id, 'period' => 1, 'month' => $this->month_number, 'year' => $this->year_number)); ?>"
           class="js-excel-report" class="row_deleted">1</a>
    </div>
    <div class="cell print_clinic">
        <a data-href="<?//= Yii::app()->createUrl('mbLawReports/exelAdminLawSinglePeriod', array('id' => $model->id, 'period' => 2, 'month' => $this->month_number, 'year' => $this->year_number)); ?>"
           class="js-excel-report" class="row_deleted">2</a>
    </div>
-->
    <div class="cell print_clinic">
        <?php if (!empty($law_reports)) : ?><a
            data-href="<?= Yii::app()->createUrl('mbLawReports/exelAdminLawSingle', array('id' => $law_reports->id)); ?>"
            class="row_deleted js-excel-report">Ю</a><?php endif; ?>
    </div>
    <!--<div class="cell print_plus">
		<a href="<?= Yii::app()->createUrl('mbLawReports/createClinic', array('integrator_id' => $model->id)); ?>">+</a>
	</div>-->
    <div class="cell download_file_text"></div>
    <div class="cell email"><?= (!empty($model->iu->nameperson) ? $model->iu->nameperson : ''); ?></div>
    <div class="cell legalName">
        <?php if (!empty($law_reports)) : ?>
        <a data-id="<?= $law_reports->id; ?>" class="import_exel_single" class="row_deleted">
            <?php endif; ?>
            <?= (!empty($model->title) ? $model->title : ''); ?>
            <?php if ($model->expensive_services_agreement) : ?>
                <img src="/themes/apinew/img/dollar.png">
            <?php endif; ?>
            <?php if (!empty($law_reports->comment)) : ?>
                <span class="tipsName"><?= $law_reports->comment; ?></span>
            <?php endif; ?>
            <?php if (!empty($law_reports)) : ?>
        </a>
    <?php endif; ?>
        <a href="<?= Yii::app()->createUrl('mbLawReports/update', array('id' => $model->id)); ?>"
           class="update iconupdate"></a>
        <a href="<?= Yii::app()->createUrl('mbLawReports/delete', array('id' => $model->id)); ?>"
           class="deleted row_deleted iconclear"></a>
    </div>
    <?php if (!empty($law_reports)) : ?>
        <div class="cell affiliates">
            <a href="<?= Yii::app()->createUrl('mbLawReports/viewCompany'); ?>"
               data-report-id="<?= $law_reports->id; ?>"
               data-id="<?= $model->id; ?>"><?= (!empty($model->clinics) ? Yii::t('app', "{n} клиника|{n} клиники|{n} клиник", count($model->clinics)) : 'клиник нет'); ?></a>
        </div>
        <div class="cell recording">
            <?= (!empty($law_reports->patient_record) ? $law_reports->patient_record : '<span class="substrLine">0</span>'); ?>
        </div>
        <div class="cell surviving">
            <?= (!empty($law_reports->patient_count) ? $law_reports->patient_count : '<span class="substrLine">0</span>'); ?>
        </div>
        <div class="cell surviving">
            <?= (!empty($law_reports->patient_success) ? $law_reports->patient_success : '<span class="substrLine">0</span>'); ?>
            <sup><?= $law_reports->successPercentPatient; ?>%</sup>
        </div>
        <div class="cell generalSum">
            <span
                class="red"><?= (!empty($law_reports->summ) ? $law_reports->summ . ' руб.' : '<span class="substrLine">0 руб.</span>'); ?> </span>
            <sup><?= $law_reports->percentSumm; ?></sup>
        </div>
        <div class="cell surviving">
            <?= (!empty($law_reports->patient_diag) ? $law_reports->patient_diag : '<span class="substrLine">0</span>'); ?>
            <sup><?= $law_reports->successPercentdDiagPatient; ?>%</sup>
        </div>
        <div class="cell surviving survivingSum">
            <?= (!empty($law_reports->summ_diag) ? $law_reports->summ_diag . ' руб.' : '<span class="substrLine">0 руб.</span>'); ?>
            <sup><?printf("%6.2f",(($law_reports->patient_diag>0 && $law_reports->summ_diag>0)?$law_reports->summ_diag/$law_reports->patient_diag:"0"))?></sup>
        </div>
        <div class="cell surviving">
            <?= (!empty($law_reports->patient_analyz) ? $law_reports->patient_analyz : '<span class="substrLine">0</span>'); ?>
        </div>
        <div class="cell surviving survivingSum">
            <?= (!empty($law_reports->summ_analyz) ? $law_reports->summ_analyz . ' руб.' : '<span class="substrLine">0 руб.</span>'); ?>
            <sup><?printf("%6.2f",(($law_reports->patient_analyz>0 && $law_reports->summ_analyz)?$law_reports->summ_analyz/$law_reports->patient_analyz:"0"))?></sup>
        </div>
        <div class="cell repeat">
            <?= (!empty($law_reports->patient_repeat) ? $law_reports->patient_repeat : '<span class="substrLine">0</span>'); ?>
        </div>
        <div class="cell insurance">
            <?= (!empty($law_reports->patient_insurance) ? $law_reports->patient_insurance : '<span class="substrLine">0</span>'); ?>
        </div>
        <div class="cell status">
            <?= (!empty($law_reports->status) ? IntegratorLawReports::$statusText[$law_reports->status] : ''); ?>
        </div>
    <?php else : ?>
        <div class="cell affiliates">
            <a href="<?= Yii::app()->createUrl('mbLawReports/viewCompany'); ?>" data-report-id="0"
               data-id="<?= $model->id; ?>" data-date="<?= $params['queryDate']; ?>"
               data-dateEnd="<?= $params['queryDateEnd']; ?>"><?= (!empty($model->clinics) ? Yii::t('app', "{n} клиника|{n} клиники|{n} клиник", count($model->clinics)) : 'клиник нет'); ?></a>
        </div>
        <div class="cell none_order">
            <a href="<?= Yii::app()->createUrl('mbLawReports/createLawReport', array('id' => $model->id)); ?>">нет
                счетов за выбранный период</a>
        </div>
    <?php endif; ?>
</div>