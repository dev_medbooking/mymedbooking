<div class="row line clearfix">
	<div class="cell name"><?=$model->title;?>(<?=($params['type'] == 1 ? $model->paymentsRating : ($params['type'] == 2 ? $model->agreeRating : 0));?>)</div>
	<?php for ($i = 6; $i >= 1; $i--) :?>
		<?php
			switch($params['type']) {
				case 1:
					$value = $model->getSupposedPayment($i);?>
					<div class="cell name count_patient" style="<?php if ($value['status'] == IntegratorLawReports::STATUS_PAY) :?>text-decoration: line-through;<?php endif;?>"><?=$value['sum'];?></div>
					<?php break;
				case 2:
					$value = $model->getSupposedAgree($i);?>
					<div class="cell name count_patient" style="<?php if ($value['status'] != 0) :?>text-decoration: line-through;<?php endif;?>"><?=$value['sum'];?></div>
					<?php break;
				default:
					break;
			}
		?>
	<?php endfor;?>
</div>