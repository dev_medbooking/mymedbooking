<div class="row line clearfix">
    <div class="cell number"><?=$model->id;?></div>
    <div class="cell legalName">
	    <?=( ! empty($model->title) ? $model->title : '');?>
        <a href="<?=Yii::app()->createUrl('mbLawReports/update', array('id' => $model->id));?>" class="update iconupdate"></a>
		<a href="<?=Yii::app()->createUrl('mbLawReports/delete', array('id' => $model->id));?>" class="deleted row_deleted iconclear"></a>
    </div>
    <div class="cell address"><?=( ! empty($model->address) ? $model->address : '');?></div>
    <div class="cell numb_agree"><?=( ! empty($model->numb_agree) ? $model->numb_agree : '');?></div>
    <div class="cell name"><?=( ! empty($model->name) ? $model->name : '');?></div>
    <div class="cell agreement"><?=( ! empty($model->agreement) ? $model->agreement : '');?></div>
</div>