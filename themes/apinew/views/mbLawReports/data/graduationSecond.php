<div class="row line clearfix">
	<div class="cell name"><?=$model->title;?></div>
	<div class="cell name count_patient"><?=( ! empty($law_report) ? $law_report->getGraduationPatient($params['type']) : 0);?></div>
	<div class="cell name next_count">
		<?=( ! empty($law_report) ? ($law_report->activeGraduation) : 0);?>
		<sup><?=( ! empty($law_report) ? "+".($law_report->getGraduationPatient($params['type']) - $law_report->activeGraduationPatient) : 0);?></sup>
	</div>
	<div class="cell name next_count">
		<?php $graduation = ( ! empty($law_report) ? $law_report->getNextGraduation($params['type']) : array());?>
		<?=( ! empty($graduation) ? $graduation['price'] : 0);?>
		<sup><?=(( ! empty($graduation['count']) AND ! empty($law_report)) ? ($law_report->getGraduationPatient($params['type']) - $graduation['count']) : '');?></sup>
	</div>
</div>