<?php if(Yii::app()->controller->action->id == 'reports' OR Yii::app()->controller->action->id == 'payments') :?>
	<?php $this->renderPartial('//mbLawReports/data/reports', array('key'=>$key, 'model'=>$model, 'law_reports' => $law_reports, 'params' => $params)); ?>
<?php elseif(Yii::app()->controller->action->id == 'law') :?>
	<?php $this->renderPartial('//mbLawReports/data/law', array('key'=>$key, 'model'=>$model)); ?>
<?php elseif(Yii::app()->controller->action->id == 'clinics') :?>
	<?php $this->renderPartial('//mbLawReports/data/clinics', array('key'=>$key, 'model'=>$model)); ?>
<?php elseif(Yii::app()->controller->action->id == 'medbookingClinics') :?>
	<?php $this->renderPartial('//mbLawReports/data/clinicsList', array('key'=>$key, 'model'=>$model)); ?>
<?php elseif(Yii::app()->controller->action->id == 'diagnosticMedbookingClinics') :?>
	<?php $this->renderPartial('//mbLawReports/data/clinicsDiagnosticList', array('key'=>$key, 'model'=>$model)); ?>
<?php elseif(Yii::app()->controller->action->id == 'periodPayments') :?>
	<?php $this->renderPartial('//mbLawReports/data/payments', array('day' => $day, 'data'=>$data)); ?>
<?php elseif(Yii::app()->controller->action->id == 'partnerReports') :?>
	<?php $this->renderPartial('//mbLawReports/data/partner', array('day' => $day, 'data'=>$data)); ?>
<?php elseif(Yii::app()->controller->action->id == 'graduation') :?>
	<?php $this->renderPartial('//mbLawReports/data/graduation', array('prices' => $prices, 'model' => $model, 'law_report' => $law_report, 'params' => $params)); ?>
<?php elseif(Yii::app()->controller->action->id == 'graduationSecond') :?>
	<?php $this->renderPartial('//mbLawReports/data/graduationSecond', array('model' => $model, 'law_report' => $law_report, 'params' => $params)); ?>
<?php elseif(Yii::app()->controller->action->id == 'reportsRating') :?>
	<?php $this->renderPartial('//mbLawReports/data/reportsRating', array('model' => $model, 'params' => $params)); ?>
<?php elseif(Yii::app()->controller->action->id == 'reportsRatingSecond') :?>
	<?php $this->renderPartial('//mbLawReports/data/reportsRatingSecond', array('model' => $model, 'params' => $params)); ?>
<?php endif; ?>