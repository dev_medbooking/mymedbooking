<section id="content" class="content_prices">
	<div class="moneyInfo center">
		<?php if( ! empty($data['detailPartner'])) :?>
			<table style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
				<tr>
					<td>Партнер</td>
					<td>Всего заявок</td>
					<td>Записей</td>
					<td>Сумма</td>
				</tr>
				<?php foreach($data['detailPartner'] as $k => $v) :?>
					<tr>
						<td><?=( ! empty($v['name']) ? $v['name'] : '');?><?=( ! empty($v['site']) ? "(<a href='".$v['site']."' target='_blank'>".$v['site']."</a>)" : '');?></td>
						<td><?=( ! empty($v['all_records']) ? $v['all_records'] : '0');?></td>
						<td><?=( ! empty($v['success_record']) ? $v['success_record'] : '0');?></td>
						<td><?=( ! empty($v['price']) ? $v['price'] : '0');?></td>
					</tr>
				<?php endforeach;?>
			</table>
		<?php endif;?>
	</div>
	<div class="center">
		<div class="table admin_0 table_payments" data-href="<?=Yii::app()->createUrl('mbLawReports/'.Yii::app()->controller->action->id, array('t'=>1));?>">
			<div class="filter-table"><?=$this->renderPartial('//mbLawReports/_filter', array('data' => $data));?></div>
			<div class="body-table"><?=$this->renderPartial('_partner', array('days' => $days, 'data' => $data));?></div>
		</div>
	</div>
</section>
