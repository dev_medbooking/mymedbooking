<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script src="https://struts2-jquery.googlecode.com/svn-history/r1647/trunk/struts2-jquery-plugin/src/main/resources/template/js/plugins/jquery-ui-timepicker-addon.js"></script>
<script src="https://dew-scrumy.googlecode.com/svn-history/r240/trunk/FulbitoPortal/WebContent/js/jquery-ui-sliderAccess.js"></script>
<div class="application-content _application-content">
   Создание заявки на Юридическое лицо.
</div>
<div id="create-block" <?php if(!empty($_GET['id'])):?>data-id="<?php echo $_GET['id'];?>"<?php endif;?> class="_create-block">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'admin-form',
        'enableAjaxValidation' => FALSE,
        'enableClientValidation' => TRUE,
        'htmlOptions' => array(
	        'class' => 'application-form'
        ),
    )); ?>
        <?php if($model->errors): ?><div class="well"><?php echo $form->errorSummary($model); ?></div><?php endif;?>
		<div class="row clearfix">
			<div class="text-name note-form row-lb">
				<span>Кол-во записей</span>
				<?=$form->textField($model, 'patient_record', array('placeholder' => '', 'class'=>'request')); ?>
			</div>
		</div>
        <div class="row clearfix">
            <div class="text-name note-form row-lb">
                <span>Количество направленных пациентов</span>
                <?=$form->textField($model, 'patient_count', array('placeholder' => '', 'class'=>'request'));?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form row-lb">
                <span>Количество согласованных пациентов</span>
                <?=$form->textField($model, 'patient_success', array('placeholder' => '', 'class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form row-lb">
                <span>Сумма счета</span>
                <?=$form->textField($model, 'summ', array('placeholder' => '', 'class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name row-lb note-form">
                <span>Дата на которую создается счет</span>
                <?=$form->textField($model, 'report_date', array('placeholder' => '', 'data-href' => Yii::app()->createUrl('mbLawReports/checkReportsInMonth'), 'class'=>'request request_date')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <button class="_save_btn js-save-law-report">Сохранить</button>
        </div>
    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    function callCalendar2() {
            $('#IntegratorLawReports_report_date').datetimepicker({
                lang: "ru",
                onSelect: function(dateText, inst) {
                    var miliseconds = 2628000000;
                    var currentTime = new Date();
                    var selectedDate = new Date(dateText);
                }
            });
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Текущее время',
            closeText: 'Закрыть',
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);
    }
    callCalendar2();
</script>
