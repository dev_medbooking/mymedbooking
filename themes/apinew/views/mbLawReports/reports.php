<section id="content" class="content_prices">
	<div class="moneyInfo center">
		<?php if (Yii::app()->user->checkAccess('Taccounter') AND ! empty($params['stats'])) :?>
			<?php foreach ($params['stats'] as $name => $stats) :?>
				<div><?=$name;?>: <?=$stats['percent']['success']?>% согласованных. Средний чек: <?=$stats['percent']['average_bill'];?> руб.</div>
			<?php endforeach;?>
		<?php endif;?>
		<?php if ( ! empty($params['percent']['average_analyz_bill'])) :?>
			Средний чек: <?=$params['percent']['average_analyz_bill'];?> руб.
		<?php endif;?>
	</div>
	<div class="center">
		<div class="table admin_0 table_law" data-href="<?=Yii::app()->createUrl('mbLawReports/'.Yii::app()->controller->action->id, array('t'=>1, 'status' => $params['status']));?>">
			<div class="filter-table"><?=$this->renderPartial('//mbLawReports/_filter', array('sort' => $sort, 'import' => $import, 'model' => $model, 'params' => $params, 'count' => $count));?></div>
			<div class="body-table"><?=$this->renderPartial('_reports', array('data' => $data, 'params' => $params));?></div>

		</div>
	</div>
	<div class="pagination row" id="pages">
			<?=$this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count));?>
	</div>
</section>
<div class="height-fix-div" ></div>

