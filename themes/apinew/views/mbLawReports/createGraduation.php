<div class="application-content _application-content business-top-form">
	<p><strong>Создание новой градации</strong></p>
</div>
<div id="create-block" class="_create_block business-form-wrapper">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'admin-form',
        'enableAjaxValidation' => FALSE,
        'enableClientValidation' => TRUE,
        'htmlOptions' => array(
	        'class' => 'application-form'
        ),
    )); ?>
        <?php if($model->errors) :?>
	        <div class="well"><?=$form->errorSummary($model);?></div>
        <?php endif; ?>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?=$form->textField($model, 'count', array('placeholder' => 'Количество пациентов', 'class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?=$form->textField($model, 'price', array('placeholder' => 'Цена', 'class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?=$form->dropDownList($model, 'type', IntegratorLawGraduation::$graduationText); ?>
            </div>
        </div>
        <div class="row clearfix">
            <button class="_save_btn">Сохранить</button>
        </div>
    <?php $this->endWidget(); ?>
</div>
