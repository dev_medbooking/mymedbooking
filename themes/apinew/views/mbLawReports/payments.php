<section id="content" class="content_prices">
	<div class="center">
		<div class="table admin_0 table_payments" data-href="<?=Yii::app()->createUrl('mbLawReports/'.Yii::app()->controller->action->id, array('t'=>1));?>">
			<div class="filter-table"><?=$this->renderPartial('//mbLawReports/_filter', array('data' =>$data));?></div>
			<div class="body-table"><?=$this->renderPartial('_payments', array('days' => $days, 'data' => $data));?></div>
		</div>
	</div>
</section>
