<?php if( ! empty($days)): ?>
	<?php foreach($days as $key => $value): ?>
		<?php $this->renderPartial('_data', array('day' => $value, 'data' => $data)); ?>
	<?php endforeach; ?>
<?php endif; ?>