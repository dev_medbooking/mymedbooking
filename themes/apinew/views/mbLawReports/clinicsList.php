<section id="content">
	<div class="center">
		<div class="table table_law table_clinic" data-href="<?=Yii::app()->createUrl('mbLawReports/medbookingClinics', array('t'=>1));?>">
			<div class="filter-table"><?=$this->renderPartial('//mbLawReports/_filter', array('sort' => $sort, 'model' => $model, 'count'=>$count)); ?></div>
			<div class="body-table"><?=$this->renderPartial('_clinicsList', array('data'=>$data)); ?></div>
		</div>
	</div>
	<div class="pagination row" id="pages">
		<?php if ( ! empty($pages) AND ! empty($count)): ?>
			<?=$this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count)); ?>
		<?php endif; ?>
	</div>
</section>
