<div class="row caption clearfix">
	<div class="cell number"><?=$sort->link('title', NULL, array('class' => 'csorting'));?></div>
	<div class="cell number"><?=$sort->link('patient', NULL, array('class' => 'csorting'));?></div>
	<div class="cell number"><?=$sort->link('graduation', NULL, array('class' => 'csorting'));?></div>
</div>
<form name="hidden-form" class="hidden-form">
	<div class="row sort clearfix search_filter">
		<input type="hidden" name="create_time" value="<?=$params['queryDate'];?>">
		<input type="hidden" name="create_time_end" value="<?=$params['queryDateEnd'];?>">
		<div class="cell legalName"><?=CHtml::activeTextField($model, 'title', array('value' => $params['title']));?></div>
		<div class="cell status"><?=CHtml::dropDownList('type', $params['type'], IntegratorLawGraduation::$graduationText, array('empty' => 'Выберите тип', 'class' => 'js-graduation-select'));?></div>
	</div>
</form>