<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'admin-form',
		'action' => Yii::app()->createUrl('mbLawReports/uploadFile'),
		'enableAjaxValidation' => false,
		'htmlOptions' => array(
			'class' => 'form-horizontal',
			'enctype' => 'multipart/form-data'
		),
	));
?>
<?=$form->fileField($import, 'file', array('id' => "id-input-file-2")); ?>
<?=$form->error($import, 'file'); ?>
<?=CHtml::submitButton("Загрузить", array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>
<div class="row caption clearfix">
    <div class="cell number"><?=$sort->link('id', NULL, array('class' => 'csorting'));?></div>
	<div class="cell email" style="margin-left: 76px;"><?=$sort->link('uid', NULL, array('class' => 'csorting')); ?></div>
	<div class="cell legalName"><?=$sort->link('title', NULL, array('class' => 'csorting'));?> - <span class="summary"><?php $this->renderPartial('//layouts/_admin_count', array('count' => $count)); ?></span></div>
	<div class="cell affiliates"></div>
	<div class="cell recording"><?=$sort->link('patient_record', NULL, array('class'=>'csorting'));?></div>
	<div class="cell surviving"><?=$sort->link('patient_count', NULL, array('class'=>'csorting'));?></div>
	<div class="cell successing"><?=$sort->link('patient_success', NULL, array('class'=>'csorting'));?></div>
	<div class="cell generalSum"><?=$sort->link('summ', NULL, array('class'=>'csorting'));?></div>
	<div class="cell diagnostika"><?=$sort->link('patient_diag', NULL, array('class'=>'csorting')); ?></div>
	<div class="cell diagnostika diagnostikaSum"><?=$sort->link('summ_diag', NULL, array('class'=>'csorting'));?></div>
	<div class="cell analyz"><?=$sort->link('patient_analyz', NULL, array('class'=>'csorting')); ?></div>
	<div class="cell analyz analyzSum"><?=$sort->link('summ_analyz', NULL, array('class'=>'csorting'));?></div>
	<div class="cell repeat"><?=$sort->link('patient_repeat', NULL, array('class'=>'csorting')); ?></div>
	<div class="cell insurance"><?=$sort->link('patient_insurance', NULL, array('class'=>'csorting')); ?></div>
	<div class="cell status caption__statusExpense"></div>
</div>
<form name="hidden-form" class="hidden-form">
<div class="row sort clearfix search_filter">
	<input type="hidden" class="js-active-sort" name="sort" value="<?=Yii::app()->request->getQuery('sort');?>">
	<input type="hidden" name="create_time" value="<?=$params['queryDate'];?>">
	<input type="hidden" name="create_time_end" value="<?=$params['queryDateEnd'];?>">
	<div class="cell number"></div>
    <div class="cell email" style="margin-left: 114px"><?=CHtml::activeTextField($model, 'uid', array('value' => (!empty($_REQUEST['IntegratorLaws']['uid']) ? $_REQUEST['IntegratorLaws']['uid'] : '')));?></div>
    <div class="cell legalName"><?=CHtml::activeTextField($model, 'title', array());?></div>
	<div class="cell affiliates"><a class="sortBtn" href="#"></a></div>
	<div class="cell recording"><?=($params['patient_record']);?></div>
	<div class="cell surviving"><?=($params['patient_count']);?></div>
	<div class="cell successing"><?=($params['patient_success']);?><sup><?=($params['percent']['success']);?>%</sup></div>
	<div class="cell generalSum">
		<span class="red"><?=$params['summRed'];?> руб.<sup><?=( ! empty($params['percent']['average_bill']) ? $params['percent']['average_bill'] : '');?></sup></span>
		<span class="green"><?=$params['summGreen'];?> руб.</span>
	</div>
	<div class="cell diagnostika"><?=($params['patient_diag']);?><sup><?=( ! empty($params['percent']['diag']) ? $params['percent']['diag'].'%' : '');?></sup></div>
	<div class="cell diagnostika diagnostikaSum"><?=($params['summ_diag']);?> руб.<sup><?=( ! empty($params['percent']['summ_diag']) ? $params['percent']['summ_diag'].'%' : '');?></sup></div>
	<div class="cell analyz"><?=($params['patient_analyz']);?><sup><?=( ! empty($params['percent']['analyz']) ? $params['percent']['analyz'].'%' : '');?></sup></div>
	<div class="cell analyz analyzSum"><?=($params['summ_analyz']);?> руб.<sup><?=( ! empty($params['percent']['summ_analyz']) ? $params['percent']['summ_analyz'].'%' : '');?></sup></div>
	<div class="cell repeat"><?=($params['patient_repeat']);?></div>
	<div class="cell insurance"><?=($params['patient_insurance']);?></div>
	<div class="cell status"><?php if(empty($_GET['active_status'])):?><?=CHtml::dropDownList('active_status', 0, IntegratorLawReports::$statusText, array('empty' => 'Все', 'class' => 'js-active-status'));?><?php endif;?></div>
</div>
</form>