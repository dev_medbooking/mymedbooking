<div class="row caption clearfix">
	<div class="cell number"><?=$sort->link('title', NULL, array('class' => 'csorting'));?></div>
	<div class="cell number"><?=$sort->link('patient_success', NULL, array('class' => 'csorting'));?></div>
	<?php foreach($prices as $v) :?>
		<div class="cell number"><?=$v->price;?></div>
	<?php endforeach;?>
</div>
<form name="hidden-form" class="hidden-form">
	<div class="row sort clearfix search_filter">
		<input type="hidden" name="create_time" value="<?=$params['queryDate'];?>">
		<input type="hidden" name="create_time_end" value="<?=$params['queryDateEnd'];?>">
		<div class="cell legalName"><?=CHtml::activeTextField($model, 'title', array('value' => $params['title']));?></div>
		<div class="cell status"><?=CHtml::dropDownList('type', $params['type'], IntegratorLawGraduation::$graduationText, array('empty' => 'Выберите тип', 'class' => 'js-graduation-select'));?></div>
		<div class="cell status"><?=CHtml::dropDownList('min', ( ! empty($params['min']) ? $params['min'] : 0), IntegratorLawGraduation::getPricesArray($params['prices']), array('empty' => 'От', 'class' => 'js-graduation-select'));?></div>
		<div class="cell status"><?=CHtml::dropDownList('max', ( ! empty($params['max']) ? $params['max'] : 0), IntegratorLawGraduation::getPricesArray($params['prices']), array('empty' => 'До', 'class' => 'js-graduation-select'));?></div>
	</div>
</form>