<div class="row caption clearfix">
	<div class="cell number"><?=$sort->link('title', NULL, array('class' => 'csorting'));?></div>
	<div class="cell number">1-5</div>
	<div class="cell number">6-10</div>
	<div class="cell number">11-15</div>
	<div class="cell number">16-20</div>
	<div class="cell number">21-25</div>
	<div class="cell number">26-31</div>
</div>
<form name="hidden-form" class="hidden-form">
	<div class="row sort clearfix search_filter">
		<div class="cell legalName"><?=CHtml::activeTextField($model, 'title', array('value' => $params['title']));?></div>
		<?php if ($params['type'] == 1) :?>
			<div class="cell number ratingSum6">
				<?=$params['ratingSum'][6];?>
				<sup><?=$params['ratingPaymentSum'][6];?></sup>
			</div>
			<div class="cell number ratingSum5">
				<?=$params['ratingSum'][5];?>
				<sup><?=$params['ratingPaymentSum'][5];?></sup>
			</div>
			<div class="cell number ratingSum4">
				<?=$params['ratingSum'][4];?>
				<sup><?=$params['ratingPaymentSum'][4];?></sup>
			</div>
			<div class="cell number ratingSum3">
				<?=$params['ratingSum'][3];?>
				<sup><?=$params['ratingPaymentSum'][3];?></sup>
			</div>
			<div class="cell number ratingSum2">
				<?=$params['ratingSum'][2];?>
				<sup><?=$params['ratingPaymentSum'][2];?></sup>
			</div>
			<div class="cell number ratingSum1">
				<?=$params['ratingSum'][1];?>
				<sup><?=$params['ratingPaymentSum'][1];?></sup>
			</div>
			<div class="cell number ratingSumSum">
				
			</div>
		<?php endif;?>
		<div class="cell status"><?=CHtml::dropDownList('type', $params['type'], array(1 => 'Рейтинг по оплатам', 2 => 'Рейтинг согласования'), array('empty' => 'Выберите тип', 'class' => 'js-graduation-select'));?></div>
	</div>
</form>