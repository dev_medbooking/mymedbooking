<?php if( ! empty($prices) AND ! empty($data)): ?>
	<?php foreach($data as $key => $value): ?>
		<?php if( ! empty($value->primaryKey)): ?>
			<?php if ( ! empty($value->law_reports)) :?>
				<?php foreach($value->law_reports as $law_report) :?>
					<?php $this->renderPartial('_data', array('model' => $value, 'law_report' => $law_report, 'prices' => $prices, 'params' => $params)); ?>
				<?php endforeach;?>
			<?php else :?>
				<?php $this->renderPartial('_data', array('model' => $value, 'law_report' => NULL, 'prices' => $prices, 'params' => $params)); ?>
			<?php endif; ?>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>