<div class="hiddenAffiliates__right__top clearfix">
	<div class="hiddenAffiliates__right__top__invoice">
		<?=CHtml::dropDownList('company_status', $model->law_report->status, IntegratorLawReports::getAllowedStatus(), array('id' => 'company_status'));?>
	</div>
	<div class="hiddenAffiliates__right__top__calendarWrap">
		<input type="text" class="calendar_status" />
		<span class="js-save-status save_status" data-id="<?=$model->id;?>" data-report-id="<?=$model->law_report->id;?>" data-href="<?=Yii::app()->createUrl('mbLawReports/updateStatus');?>">Сохранить</span>
	</div>
	<div class="hiddenAffiliates__right__top__calendarWrap__selectDate"><span>Выбрать дату</span></div>
	<div class="cell hiddenAffiliates__right__top__calendarWrap__close">
		<a class="hiddenAffiliates__right__top__btnClose" href="<?=Yii::app()->createUrl('mbLawReports/closeCompany');?>" data-id="<?=$model->id;?>" data-report-id="<?=$model->law_report->id;?>"></a>
	</div>
</div>
<div class="hiddenAffiliates__right__bottom">
	<ul>
		<?php foreach ($model->law_report->law_report_status as $key => $value) :?>
			<li class="clearfix">
				<span class="type_status"><?=$value->getReportStatusText($value->status);?></span>
				<span class="data_status"><?=date('d.m.y', strtotime($value->status_date));?></span>
				<i class="clear del_status" data-id="<?=$value['id'];?>" data-href="<?=Yii::app()->createUrl('mbLawReports/deleteStatus');?>"></i>
			</li>
		<?php endforeach;?>
	</ul>
</div>
<?php if ($model->law_report->status != IntegratorLawReports::STATUS_AGREE AND $model->law_report->status != IntegratorLawReports::STATUS_REPORT) :?>
	<div class="hiddenAffiliates__right__top clearfix">
		<div class="hiddenAffiliates__right__top__sum">
            <div class="label"><strong>Введите сумму:</strong></div>
			<input type="text" name="status_sum" id="status_sum"/>
		</div>
		<div class="hiddenAffiliates__right__top__calendarWrap">
			<input type="text" class="calendar_status" />
			<span class="js-make-payment save_status" data-id="<?=$model->id;?>" data-report-id="<?=$model->law_report->id;?>" data-href="<?=Yii::app()->createUrl('mbLawReports/makePayment');?>">Сохранить</span>
		</div>
		<div class="hiddenAffiliates__right__top__calendarWrap__selectDate"><span>Выбрать дату</span></div>
		<?php if ( ! empty($model->law_report->law_report_payments)) :?>
			<div class="hiddenAffiliates__right__bottom hiddenAffiliates__right__bottom_last">
				<ul>
					<?php $balance = $model->getLawBalance();?>
					<li class="clearfix">
                        <span class="type_status">Счета (сумма):</span>
                        <span class="data_status" style="overflow:visible;"><?=$balance['reportsSumm'];?></span>
					</li>
					<li class="clearfix">
                        <span class="type_status">Опл. (сумма):</span>
                        <span class="data_status" style="overflow:visible;"><?=$balance['paymentsReportsSumm'];?></span>
					</li>
					<li class="clearfix">
                        <span class="type_status">Баланс:</span>
                        <span class="data_status" style="overflow:visible;"><?=$balance['balance'];?></span>
					</li>
					<?php foreach ($model->law_report->law_report_payments as $key => $value) :?>
						<li class="clearfix">
							<span class="type_status"><?=(isset(IntegratorLawReportsPayments::$paymentText[$value->payment_type]) ? IntegratorLawReportsPayments::$paymentText[$value->payment_type] : '');?></span>
                            <span class="data_status" style="overflow:visible;"><?=$value->summ;?></span>
                        </li>
                        <li class="clearfix">
							<span class="type_status"><?=$value->payment_date;?></span>
							<i class="clear del_payment" data-id="<?=$value['id'];?>" data-href="<?=Yii::app()->createUrl('mbLawReports/deletePayment');?>"></i>
						</li>
					<?php endforeach;?>
				</ul>
			</div>
		<?php endif;?>
	</div>
<?php endif;?>