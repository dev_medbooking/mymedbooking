<div class="cell number"><?=$model->id; ?></div>
<div class="cell legalName legalName_open" style="width:436px;"><?=( ! empty($model->title) ? $model->title : ''); ?></div>
<div class="main_number clearfix">
	<?php if ( ! empty($model->law_report)) :?>
		<div class="cell affiliates">
			<a href="<?=Yii::app()->createUrl('mbLawReports/viewCompany');?>" data-report-id="<?=$model->law_report->id;?>" data-id="<?=$model->id;?>"><?=( ! empty($model->clinics) ? Yii::t('app',"{n} клиника|{n} клиники|{n} клиник", count($model->clinics)) : 'клиник нет');?></a>
		</div>
		<div class="cell recording">
			<?=( ! empty($model->law_report->patient_record) ? $model->law_report->patient_record : '<span class="substrLine">0</span>');?>
		</div>
		<div class="cell surviving">
			<?=( ! empty($model->law_report->patient_count) ? $model->law_report->patient_count : '<span class="substrLine">0</span>');?>
		</div>
		<div class="cell successing">
			<?=( ! empty($model->law_report->patient_success) ? $model->law_report->patient_success : '<span class="substrLine">0</span>');?>
			<sup><?=$model->law_report->successPercentPatient;?>%</sup>
		</div>
		<div class="cell generalSum">
			<span class="red"><?=( ! empty($model->law_report->summ) ? $model->law_report->summ . ' руб.' : '<span class="substrLine">0 руб.</span>');?> </span>
			<sup><?=$model->law_report->percentSumm;?></sup>
		</div>
		<div class="cell diagnostic">
			<?=( ! empty($model->law_report->patient_diag) ? $model->law_report->patient_diag : '<span class="substrLine">0</span>');?>
			<sup><?=$model->law_report->successPercentdDiagPatient;?>%</sup>
		</div>
		<div class="cell diagnosticSum">
			<?=( ! empty($model->law_report->summ_diag) ? $model->law_report->summ_diag . ' руб.' : '<span class="substrLine">0 руб.</span>');?>
		</div>
		<div class="cell analyz">
			<?=( ! empty($model->law_report->patient_analyz) ? $model->law_report->patient_analyz : '<span class="substrLine">0</span>');?>
		</div>
		<div class="cell analyzSum">
			<?=( ! empty($model->law_report->summ_analyz) ? $model->law_report->summ_analyz . ' руб.' : '<span class="substrLine">0 руб.</span>');?>
		</div>
		<div class="cell repeat">
			<?=( ! empty($model->law_report->patient_repeat) ? $model->law_report->patient_repeat : '<span class="substrLine">0</span>');?>
		</div>
		<div class="cell insurance">
			<?=( ! empty($model->law_report->patient_insurance) ? $model->law_report->patient_insurance : '<span class="substrLine">0</span>');?>
		</div>
	<?php else :?>
		<div class="cell affiliates">
			<a href="<?=Yii::app()->createUrl('mbLawReports/viewCompany');?>" data-report-id="0" data-id="<?=$model->id;?>">
				<?=( ! empty($model->clinics) ? Yii::t('app',"{n} клиника|{n} клиники|{n} клиник", count($model->clinics)) : 'клиник нет');?>
			</a>
		</div>
	<?php endif;?>
</div>
   
<div class="hiddenRow hiddenAffiliates clearfix">
    <div class="hiddenAffiliates__left" style="padding:20px 0 0 20px;width:1280px;">
        <ul class="hiddenAffiliates__left__decription">
            <li class="hiddenAffiliates__left__decription__contract">
                <span>Договор <strong>№ <?=( ! empty($model->numb_agree) ? $model->numb_agree : 'Номер не указан');?></strong></span>
                <p>Адрес: <?=( ! empty($model->address) ? $model->address : 'Адрес не указан');?></p>
                <p>соглашение: <?=( ! empty($model->agreement) ? $model->agreement : 'нет указано');?></p>
            </li>
            <li class="hiddenAffiliates__left__decription__contactPerson">
                <span class="contactUser">Контактное лицо</span>
                <p>
	                <span class="nameUser"><?=( ! empty($model->name) ? $model->name : 'Конт.лицо не указано');?></span>
	                <span class="phoneUser"><?=( ! empty($model->telephone) ? $model->telephone : 'Телефон не указан');?></span>
                </p>
            </li>
	        <?php if ( ! empty($model->law_report)) :?>
	        <li class="hiddenAffiliates__left__decription__contactComm">
		        <span>Комментарий к счету</span>
		        <textarea name="comment" id="comment"><?=$model->law_report->comment;?></textarea>
		        <a class="js-update-comment" href="<?=Yii::app()->createUrl('mbLawReports/updateComment', array('id' => $model->law_report->id));?>">Сохранить</a>
	        </li>
	        <?php endif;?>
        </ul>
        <?php if( ! empty($model->clinics)) :?>
            <div class="hiddenAffiliates__left__options">
                <?php foreach ($model->clinics as $key => $value) :?>
                <div class="hiddenAffiliates__left__options__row clearfix ajax_link" data-href="<?=Yii::app()->createUrl('mbLawReports/ajaxUpdateRow')?>">
                        <div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cellName">
                            <p>
	                            <?php if( ! empty($value->clinic_report)) :?><a href="<?=Yii::app()->createUrl('mbLawReports/exelAdminRecordsSingle', array('id' => $value->clinic_report->id));?>" class="import_exel_single"><?php endif;?>
		                            <?=( ! empty($value->title) ? $value->title : '');?> (<?=$value->doctors;?>)
		                        <?php if( ! empty($value->clinic_report)) :?></a><?php endif;?>
	                            <a href="<?=Yii::app()->createUrl('mbLawReports/updateClinic', array('id' => $value->id));?>" class="iconupdate"></a>
	                            <span class="download_file_text2"></span>
                            </p>
                            
                            <span class="addressClinic"><?=( ! empty($value->address) ? $value->address : 'Адрес не указан');?></span>
                        </div>
                            <?php if( ! empty($value->clinic_report)) :?>
                                <div class="column_block_price">
	                                <?=$this->renderPartial('//mbLawReports/ajax/value', array('model' => $value->clinic_report)); ?>
                                </div>
                            <?php elseif ( ! empty($model->law_report)) :?>
                                <div class="hiddenAffiliates__left__options__cell none_order">
	                                <a href="<?=Yii::app()->createUrl('mbLawReports/createClinicReport', array('id' => $model->law_report->id, 'clinic_id' => $value->id));?>">нет заявки на этот месяц</a>
                                </div>
	                        <?php else :?>
	                            <div class="hiddenAffiliates__left__options__cell none_order">
		                            нет заявки на этот месяц
	                            </div>
                            <?php endif;?>
                    </div>
                <?php endforeach;?>
            </div>
        <?php else :?>
            <div class="hiddenAffiliates__left__options">
                <?php if ( empty($model->law_report)) :?>
                <div class="hiddenAffiliates__left__options__cell none_order">
                <a href="<?=Yii::app()->createUrl('mbLawReports/createLawReport', array('id' => $model->id));?>">нет счетов за выбранный период</a>
                </div>
                <?php endif;?>
            </div>
        <?php endif;?>
    </div>
	<?php if ( ! empty($model->law_report)) :?>
		<div class="hiddenAffiliates__right">
			<?=$this->renderPartial('ajax/date', array('model' => $model));?>
		</div>
	<?php else :?>
		<div class="hiddenAffiliates__right">
			<div class="hiddenAffiliates__right__top clearfix">
				<div class="cell hiddenAffiliates__right__top__calendarWrap__close">
					<a class="hiddenAffiliates__right__top__btnClose" href="<?=Yii::app()->createUrl('mbLawReports/closeCompany');?>" data-id="<?=$model->id;?>" data-report-id="0"></a>
				</div>
			</div>
		</div>
	<?php endif;?>
</div>

