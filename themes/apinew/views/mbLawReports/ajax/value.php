<div class="column_rows_<?=( ! empty($model['id']) ? $model['id'] : '');?>">
	<div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cell__num main_total" >
	    <a href="#" class="for_calendar_status no-correct" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" data-column="patient_record" ><?=( ! empty($model['patient_record']) ? $model['patient_record'] : '0');?></a>
	</div>
	<div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cell__num">
	    <a href="#" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" data-column="patient_count"><?=( ! empty($model['patient_count']) ? $model['patient_count'] : '0');?></a>
	    <i class="difference"><?=($model['patient_record'] - $model['patient_count']);?></i>
	</div>
	<div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cell__num">
	    <a href="#" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" data-column="patient_success"><?=( ! empty($model['patient_success']) ? $model['patient_success'] : '0');?></a>
	    <i class="difference"><?=($model['patient_record'] - $model['patient_success']);?></i>
	</div>
	<div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cell__num hiddenAffiliates__left__options__cell__num__summ">
	    <a href="#" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" data-column="summ"><?=( ! empty($model['summ']) ? $model['summ'] : '0');?></a>
	</div>
	<div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cell__num">
	    <a href="#" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" data-column="patient_diag"><?=( ! empty($model['patient_diag']) ? $model['patient_diag'] : '0');?></a>
	</div>
	<div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cell__num hiddenAffiliates__left__options__cell__num__sum">
	    <a href="#" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" data-column="summ_diag"><?=( ! empty($model['summ_diag']) ? $model['summ_diag'] : '0');?></a>
	</div>
	<div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cell__num">
		<a href="#" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" data-column="patient_analyz"><?=( ! empty($model['patient_analyz']) ? $model['patient_analyz'] : '0');?></a>
	</div>
	<div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cell__num hiddenAffiliates__left__options__cell__num__sum">
		<a href="#" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" data-column="summ_analyz"><?=( ! empty($model['summ_analyz']) ? $model['summ_analyz'] : '0');?></a>
	</div>
	<div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cell__num">
		<a href="#" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" data-column="patient_repeat"><?=( ! empty($model['patient_repeat']) ? $model['patient_repeat'] : '0');?></a>
	</div>
	<div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cell__num">
		<a href="#" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" data-column="patient_insurance"><?=( ! empty($model['patient_insurance']) ? $model['patient_insurance'] : '0');?></a>
	</div>
	<!--<a href="<?=Yii::app()->createUrl('mbLawReports/deleteClinicReport', array('id' => $model['id']));?>" data-id="<?=( ! empty($model['id']) ? $model['id'] : '');?>" class="js-delete-price delete_price_icon"></a>-->
</div>