<section id="content" class="content_prices">
	<div class="center">
		<div class="table admin_0 graduation" data-href="<?=Yii::app()->createUrl('mbLawReports/'.Yii::app()->controller->action->id, array('t'=>1, 'type' => $params['type']));?>">
			<div class="filter-table js-graduation-filter"><?=$this->renderPartial('//mbLawReports/_filter', array('model' => $model, 'params' => $params, 'sort' => $sort, 'count' => $count));?></div>
			<div class="body-table"><?=$this->renderPartial('_reportsRatingSecond', array('data' => $data, 'params' => $params));?></div>
		</div>
		<div class="pagination row" id="pages">
			<?php if ( ! empty($pages) AND ! empty($count)): ?>
				<?=$this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count)); ?>
			<?php endif;?>
		</div>
	</div>
</section>
