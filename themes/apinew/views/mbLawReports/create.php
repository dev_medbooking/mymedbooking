<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
<script type="text/javascript">$(function(){$("#IntegratorClinicAll_telephone").mask("+7(999)999-9999",{'placeholder':'_'});});$(function(){$(".chosen-default>span").text("Выбрать")});</script>
<div class="application-content _application-content business-top-form">
    <?php if(!empty($model->id)): ?><p><strong><?php echo $model->title; ?></strong></p><?php else:?><p><strong>Создание нового Юр.лица</strong></p><?php endif;?>
</div>
<div id="create-block" class="_create_block business-form-wrapper">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'application-form'),
    )); ?>
        <?php if($model->errors): ?><div class="well"><?php echo $form->errorSummary($model); ?></div><?php endif; ?>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'title',array('placeholder'=>'Юр. название','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'title_company',array('placeholder'=>'Факт.название','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'address',array('placeholder'=>'Адрес','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'name',array('placeholder'=>'Имя, фамилия','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'uid',array('placeholder'=>'ID пользователя','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'numb_agree',array('placeholder'=>'Номер договора','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-uid text-pencil clearfix">
                <label for="agreement"></label>
                <?php echo $form->textField($model,'agreement',array('placeholder'=>'Договор',"id"=>"agreement")); ?>
            </div>
        </div>
		<div class="row clearfix">
			<div class="text-name note-form">
				<label for="duplicate">Убирать ли дубликаты при формировании отчетов</label>
				<?php echo $form->checkBox($model,'duplicate',array('id' => 'duplicate')); ?>
			</div>
		</div>
		<div class="row clearfix">
			<div class="text-name note-form">
				<label for="expensive_services_agreement">Соглашение на дорогие услуги</label>
				<?php echo $form->checkBox($model, 'expensive_services_agreement', array('id' => 'expensive_services_agreement')); ?>
			</div>
		</div>
        <div class="row clearfix">
            <?php echo $form->textArea($model,'comment',array("placeholder"=>"Комментарии")); ?>
        </div>
		<?php if (Yii::app()->controller->action->id == 'update') :?>
			<div class="row clearfix">
					<div class="text-name note-form">
						<label for="duplicate">Использовать общую градацию для всех пациентов</label>
						<?php echo $form->checkBox($model,'graduation_all',array('id' => 'graduation_all')); ?>
					</div>
				</div>
			<div class="row clearfix">
			<?php if ( ! empty($model->law_graduation)) :?>
				<br>Общая медицина:<br>
				<?php foreach($model->law_graduation as $k => $v) :?>
					<?php if ($v->type == 1) :?>
						Цена: <?=$v->price;?> Пациенты: <?=$v->count;?>
						<a href="<?=Yii::app()->createUrl('mbLawReports/updateGraduation', array('id' => $v->id));?>">Р</a>
						<a href="<?=Yii::app()->createUrl('mbLawReports/deleteGraduation', array('id' => $v->id));?>">У</a><br>
					<?php endif;?>
				<?php endforeach;?>
				<br>Диагностика:<br>
				<?php foreach($model->law_graduation as $k => $v) :?>
					<?php if ($v->type == 2) :?>
						Цена: <?=$v->price;?> Пациенты: <?=$v->count;?>
						<a href="<?=Yii::app()->createUrl('mbLawReports/updateGraduation', array('id' => $v->id));?>">Р</a>
						<a href="<?=Yii::app()->createUrl('mbLawReports/deleteGraduation', array('id' => $v->id));?>">У</a><br>
					<?php endif;?>
				<?php endforeach;?>
				<br>Анализы:<br>
				<?php foreach($model->law_graduation as $k => $v) :?>
					<?php if ($v->type == 3) :?>
						Цена: <?=$v->price;?> Пациенты: <?=$v->count;?>
						<a href="<?=Yii::app()->createUrl('mbLawReports/updateGraduation', array('id' => $v->id));?>">Р</a>
						<a href="<?=Yii::app()->createUrl('mbLawReports/deleteGraduation', array('id' => $v->id));?>">У</a><br>
					<?php endif;?>
				<?php endforeach;?>
			<?php endif;?>
			<br><br><a href="<?=Yii::app()->createUrl('mbLawReports/createGraduation', array('id' => $model->id));?>">Добавить градацию</a>
		</div>
		<?php endif;?>
        <div class="row clearfix">
            <button class="_save_btn">Сохранить</button>
        </div>
    <?php $this->endWidget(); ?>
</div>
