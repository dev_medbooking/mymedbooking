<?php if(Yii::app()->controller->action->id == 'reports' OR Yii::app()->controller->action->id == 'payments') :?>
    <?php $this->renderPartial('//mbLawReports/filter/reports', array('sort'=>$sort, 'import' => $import, 'model'=>$model, 'params' => $params, 'count' => $count)); ?>
<?php elseif (Yii::app()->controller->action->id == 'law') :?>
    <?php $this->renderPartial('//mbLawReports/filter/law', array('sort'=>$sort, 'model'=>$model)); ?>
<?php elseif (Yii::app()->controller->action->id == 'clinics') :?>
    <?php $this->renderPartial('//mbLawReports/filter/clinics', array('sort'=>$sort, 'model'=>$model, 'count'=>$count)); ?>
<?php elseif (Yii::app()->controller->action->id == 'medbookingClinics') :?>
    <?php $this->renderPartial('//mbLawReports/filter/clinicsList', array('sort'=>$sort, 'model'=>$model, 'count'=>$count)); ?>
<?php elseif (Yii::app()->controller->action->id == 'diagnosticMedbookingClinics') :?>
    <?php $this->renderPartial('//mbLawReports/filter/clinicsDiagnosticList', array('sort'=>$sort, 'model'=>$model, 'count'=>$count)); ?>
<?php elseif (Yii::app()->controller->action->id == 'periodPayments') :?>
    <?php $this->renderPartial('//mbLawReports/filter/payments', array('data' => $data)); ?>
<?php elseif (Yii::app()->controller->action->id == 'partnerReports') :?>
    <?php $this->renderPartial('//mbLawReports/filter/partner', array('data' => $data)); ?>
<?php elseif (Yii::app()->controller->action->id == 'graduation') :?>
    <?php $this->renderPartial('//mbLawReports/filter/graduation', array('sort' => $sort, 'count' => $count, 'prices' => $prices, 'model' => $model, 'params' => $params)); ?>
<?php elseif (Yii::app()->controller->action->id == 'graduationSecond') :?>
    <?php $this->renderPartial('//mbLawReports/filter/graduationSecond', array('sort' => $sort, 'count' => $count, 'model' => $model, 'params' => $params)); ?>
<?php elseif (Yii::app()->controller->action->id == 'reportsRating') :?>
    <?php $this->renderPartial('//mbLawReports/filter/reportsRating', array('sort' => $sort, 'count' => $count, 'model' => $model, 'params' => $params)); ?>
<?php elseif (Yii::app()->controller->action->id == 'reportsRatingSecond') :?>
    <?php $this->renderPartial('//mbLawReports/filter/reportsRatingSecond', array('sort' => $sort, 'count' => $count, 'model' => $model, 'params' => $params)); ?>
<?php endif; ?>