<?php if(!empty($dataCount)):?><p><?php echo Yii::t('app',"Найден|Найдено|Найдено",array($dataCount)); ?> <?=$dataCount?> из <?=$count?>.<?php endif;?> 
<?php echo Yii::t('app',"Всего&nbsp;{n}&nbsp;результат|Всего&nbsp;{n}&nbsp;результата|Всего&nbsp;{n}&nbsp;результатов",array($count)); ?>
<?php if(!empty($dataPrice)):?>
    Количевство дошедших: <?php echo !empty($dataPrice['koll_come'])?$dataPrice['koll_come']:'0';?>
    Количевство согласованых: <?php echo !empty($dataPrice['koll_success'])?$dataPrice['koll_success']:'0';?>
    Общая сумма: <?php echo !empty($dataPrice['all_price'])?$dataPrice['all_price']:'0';?>
<?php endif; ?>
<?php if(!empty($ordersCount)):?>
	<br><br>За выбранный период:<br>
	<?php foreach ($ordersCount as $o) :?>
		<?php
			switch($o['status']) {
				case 2:
					echo "Всего согласовано ".$o['count']." счетов на сумму ".Yii::t('app',"{n} рубль|{n} рубля|{n} рублей",array($o['price']))."<br>";
					break;
				case 3:
					echo "Всего неоплачено ".$o['count']." счетов на сумму ".Yii::t('app',"{n} рубль|{n} рубля|{n} рублей",array($o['price']))."<br>";
					break;
				case 4:
					echo "Всего оплачено ".$o['count']." счетов на сумму ".Yii::t('app',"{n} рубль|{n} рубля|{n} рублей",array($o['price']))."<br>";
					break;
			}
		?>
	<?php endforeach;?>
<?php endif; ?>
</p>
