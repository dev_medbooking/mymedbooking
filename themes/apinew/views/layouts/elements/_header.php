<header id="header">
    <section class="header-inner clearfix">
        <a href="/" class="logo"><span>CRM.</span>medbooking.com</a>
        <div class="header-user-box clearfix">
            <button class="callsWait">Ожидающие: <span class="waits" title="Число ожидающих вызовов">0</span>, <span class="waitTimeMax" title="Самое длительное ожидание">0:00</span></button>
            <?php if(Yii::app()->user->id): ?>
            <?php if(Yii::app()->user->checkAccess('Administrator') OR Yii::app()->user->checkAccess('Taccounter') OR Yii::app()->user->checkAccess('NDasha') OR Yii::app()->user->checkAccess('Toperator')): ?>
            <a href="<?php echo Yii::app()->createUrl('/mbUser'); ?>" class="<?php echo Yii::app()->controller->id=='mbUser'?'active':'noactive'; ?>" style="color:#fff;text-decoration:none;">
                Пользователи
            </a>
            <?php endif; ?>
            <span class="user-name"><?php echo !empty(Yii::app()->user->userData['email'])?Yii::app()->user->userData['email']:Yii::app()->user->id; ?></span>
            <span class="desc">Добро пожаловать!</span>
            <a href="<?php echo Yii::app()->createUrl('/site/logout'); ?>"><span class="header-exit"></span></a>
            <?php endif; ?>
        </div>
    </section>
</header>