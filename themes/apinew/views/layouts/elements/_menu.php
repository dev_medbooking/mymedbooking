<nav class="nav">
    <div class="inner-section">
        <ul class="nav-menu clearfix">
            <?php if(Yii::app()->user->checkAccess('NDasha')||Yii::app()->user->checkAccess('NCallcenter')): ?>
            <li class="nav-applications <?php echo empty($_GET['rerecord'])&&empty($_GET['status'])&&empty($_GET['deleted'])&&(empty($_GET['flag_id'])||$_GET['flag_id']!=1)&&Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id=='admin' ?'active':'noactive'; ?>"><a href="<?php echo Yii::app()->createUrl('/mbRecord/admin'); ?>" class="<?php echo empty($_GET['rerecord'])&&empty($_GET['status'])&&Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id=='admin'?'active':'noactive'; ?>"><span><?php echo $this->statusCount(0); ?></span>Заявки</a></li>
            <li class="nav-recording <?php echo !empty($_GET['status'])&&($_GET['status']==5)&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>"><a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>5)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==5&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>"><span><?php echo $this->statusCount(5); ?></span>Записи</a></li>
            <li class="nav-failures <?php echo !empty($_GET['status'])&&($_GET['status']==1)&&Yii::app()->controller->id=='mbRecord' || Yii::app()->controller->action->id=='recording'?'active':'noactive'; ?>"><a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>1)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==1&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>"><span><?php echo $this->statusCount(1); ?></span>Отказы</a></li>
            <li class="nav-were-received <?php echo !empty($_GET['status'])&&($_GET['status']==3)&&Yii::app()->controller->id=='mbRecord' || Yii::app()->controller->action->id=='forTime' ?'active':'noactive'; ?>"><a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>3)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==3&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>"><span><?php echo $this->statusCount(3); ?></span>На дату</a></li>
            <?php endif; ?>
            <?php if(Yii::app()->user->checkAccess('NCallcenter')): ?>
            <li class="nav-recording <?php echo !empty($_GET['status'])&&($_GET['status']==7)&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>7)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==7&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                    <span><?php echo $this->statusCount(7); ?></span>
                    Ожидание
                </a>
            </li>
            <?php endif;?>

            <?php if(Yii::app()->user->checkAccess('NCallcenter')): ?>
                <li class="nav-recording <?php echo !empty($_GET['status'])&&($_GET['status']==2)&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                    <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>2)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==2&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                        <span><?php echo $this->statusCount(2); ?></span>
                        Не были
                    </a>
                </li>
                <li class="nav-recording <?php echo !empty($_GET['status'])&&($_GET['status']==11)&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                    <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>11)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==11&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                        <span><?php echo $this->statusCount(11); ?></span>
                        Перезаписаные
                    </a>
                </li>
            <?php endif;?>
<!-- верхнее меню-->
			<?php if(Yii::app()->user->checkAccess('NDasha')||Yii::app()->user->checkAccess('NCallcenter')): ?>
                <?php ?>
            <li class="nav-recording <?php echo !empty($_GET['status'])&&($_GET['status']==9)&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>9)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==9&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                    <span><?php echo $this->statusCount(9); ?></span>
                    Пустой
                </a>
            </li>
			<?php endif;?>

            <?php if(Yii::app()->user->checkAccess('NDasha')||Yii::app()->user->checkAccess('NCallcenter')): ?>
                <?php ?>
                <li class="nav-recording <?php echo !empty($_GET['status'])&&($_GET['status']==60)&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                    <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>60)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==60&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                        <span><?php echo $this->statusCount(60); ?></span>
                        Пропущенные
                    </a>
                </li>
            <?php endif;?>



	        <?php if(Yii::app()->user->checkAccess('NCallcenter')): ?>
            <li class="nav-were-received <?php echo (!empty($_GET['status'])&&($_GET['status']==51)||(!empty($_GET['flag_id'])&&$_GET['flag_id']==1))&&Yii::app()->controller->id=='mbRecord' ?'active':'noactive'; ?>">
                <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>51)); ?>" class="<?php echo (!empty($_GET['status'])&&($_GET['status']==51)||(!empty($_GET['flag_id'])&&$_GET['flag_id']==1))&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                    <span><?php echo $this->statusCount(51); ?></span>
                    Недозвон
                </a>
            </li>
            <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbCall'?'active':'noactive'; ?>">
                <a href="<?php echo Yii::app()->createUrl('/mbCall'); ?>" class="<?php echo Yii::app()->controller->id=='mbCall'?'active':'noactive'; ?>">
                    <span><?php echo $this->statusCount('call'); ?></span>
                    Звонок
                </a>
            </li>               
            <?php endif; ?>
            <?php if(Yii::app()->user->checkAccess('NDasha') OR in_array(Yii::app()->user->id, array(7829, 4645, 7842))): ?>
            <li class="nav-were-received <?php echo !empty($_GET['status'])&&($_GET['status']==30)&&Yii::app()->controller->id=='mbRecord' || Yii::app()->controller->action->id=='were'?'active':'noactive'; ?>">
                <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>30)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==30&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                    <span><?php echo $this->statusCount(30); ?></span>
                    Были
                </a>
            </li>
			<?php endif; ?>
            <?php
            $magickAccess = false;
            $wu  = new WebUser();
            $ud =  $wu->getUserData();
            if(in_array($ud['email'],
                [
                    'sav@medbooking.com',
                    'esh@medbooking.com',
                    'un@medbooking.com',
                    'ed@medbooking.com',
                    'st@medbooking.com',
                    'rt@mrdbooking.com'
                ])) {
                $magickAccess = true;
            }
            ?>
			<?php if(Yii::app()->user->checkAccess('NDasha') or $magickAccess): ?>
            <li class="nav-were-received <?php echo !empty($_GET['rerecord'])&&($_GET['rerecord']==1)&&Yii::app()->controller->id=='mbRecord' || Yii::app()->controller->action->id=='rerecord2' ?'active':'noactive'; ?> nocount">
                <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('rerecord'=>1)); ?>" class="<?php echo !empty($_GET['rerecord'])&&$_GET['rerecord']==1&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                    Повторно
                </a>
            </li>
            <li class="nav-were-received <?php echo !empty($_GET['status'])&&($_GET['status']==31)&&Yii::app()->controller->id=='mbRecord' || Yii::app()->controller->action->id=='lead' ?'active':'noactive'; ?>">
                <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>31)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==31&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                    <span><?php echo $this->statusCount(31); ?></span>
                    Leads
                </a>
            </li>
            <?php endif; ?>
            <?php if(Yii::app()->user->checkAccess('Operator')): ?>
            <li class="nav-statistics nav-incoming-calls <?php echo !empty($_GET['status'])&&($_GET['status']==8)&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('status'=>8)); ?>" class="<?php echo !empty($_GET['status'])&&$_GET['status']==8&&Yii::app()->controller->id=='mbRecord'?'active':'noactive'; ?>">
                    <span><?php echo $this->statusCount(81); ?></span>
                    Входящие
                </a>
            </li>
            <?php endif; ?>
            <?php if(Yii::app()->user->checkAccess('Toperator')): ?>
            <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbRecord'&&!empty($_GET['deleted'])?'active':'noactive'; ?>">
                <a href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('deleted'=>'1')); ?>" class="<?php echo Yii::app()->controller->id=='mbRecord'&&!empty($_GET['deleted'])?'active':'noactive'; ?>">
                    <span><?php echo $this->statusDeleted(); ?></span>
                    Корзина
                </a>
            </li>
            <?php endif; ?>            
            <?php if(Yii::app()->user->checkAccess('Taccounter')||Yii::app()->user->checkAccess('Toperator')||Yii::app()->user->checkAccess('Team')||Yii::app()->user->checkAccess('Operator')): ?>
            <li class="nav-statistics <?php echo Yii::app()->controller->action->id=='statistic' || Yii::app()->controller->action->id=='statistic2'?'active':'noactive'; ?>">
                <a href="<?php echo Yii::app()->createUrl('/mbRecord/statistic'); ?>" class="<?php echo Yii::app()->controller->action->id=='statistic'?'active':'noactive'; ?>">
                    <span><?php echo $this->statusCount('statistic'); ?></span>
                    Статистика
                </a>
            </li>
            <li class="nav-statistics <?php echo Yii::app()->controller->action->id=='report'?'active':'noactive'; ?> nocount">
                <a href="<?php echo Yii::app()->createUrl('/mbRecord/report'); ?>" class="<?php echo Yii::app()->controller->action->id=='report'?'active':'noactive'; ?>">
                    Отчет
                </a>
            </li>
            <?php endif; ?>
            <?php if(Yii::app()->user->checkAccess('Toperator') || Yii::app()->user->checkAccess('Accounter')): ?>
            <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbPrice'?'active':'noactive'; ?> nocount">
                <a href="<?php echo Yii::app()->createUrl('/mbClinicPrice'); ?>" class="<?php echo Yii::app()->controller->id=='mbPrice'?'active':'noactive'; ?>">
                    Прайсы
                </a>
            </li>
            <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbContent'?'active':'noactive'; ?> nocount">
                <a href="<?php echo Yii::app()->createUrl('/mbContent'); ?>" class="<?php echo Yii::app()->controller->id=='mbContent'?'active':'noactive'; ?>">
                    Контент
                </a>
            </li>
            <?php endif; ?>
	        <?php if(Yii::app()->user->checkAccess('Seo') || in_array(Yii::app()->user->id, array('7843'))): ?>
	        <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbCallsReports' ?'active':'noactive'; ?> nocount">
		        <a href="<?php echo Yii::app()->createUrl('/mbCallsReport'); ?>" class="<?php echo Yii::app()->controller->id=='mbCallsReports'?'active':'noactive'; ?>">
			        Отчет по Calltouch
		        </a>
	        </li>
            <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbCallsReports' ?'active':'noactive'; ?> nocount">
                <a href="<?php echo Yii::app()->createUrl('/mbCallsReport/GroupBySourceFirst'); ?>" class="<?php echo (Yii::app()->controller->id=='mbCallsReports' && Yii::app()->controller->action->id=='GroupBySourceFirst')?'active':'noactive'; ?>">
                    Отчет по Calltouch ( новая версия )
                </a>
            </li>
	        <?php endif;?>
	        <?php if(Yii::app()->user->checkAccess('NDasha') || in_array(Yii::app()->user->id, array(8133, 8126, 7829, 4645, 7842, 7833, 7843, 7847, 8067, 1337, 1469))): ?>
                <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id=='proposedClinics' ?'active':'noactive'; ?> nocount">
                    <a href="<?php echo Yii::app()->createUrl('/mbRecord/proposedClinics'); ?>" class="<?php echo Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id!='expensiveServices'?'active':'noactive'; ?>">
                        Предложенные клиники
                    </a>
                </li>
                <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id=='expensiveServices' ?'active':'noactive'; ?> nocount">
                    <a href="<?php echo Yii::app()->createUrl('/mbRecord/expensiveServices'); ?>" class="<?php echo Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id!='expensiveServices'?'active':'noactive'; ?>">
                        Дорогие услуги
                    </a>
                </li>
                <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id=='ninetyDays' ?'active':'noactive'; ?> nocount">
                    <a href="<?php echo Yii::app()->createUrl('/mbRecord/ninetyDays'); ?>" class="<?php echo Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id!='ninetyDays'?'active':'noactive'; ?>">
                         90 дней
                    </a>
                </li>
				<li class="nav-statistics <?php echo Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id=='noneServices' ?'active':'noactive'; ?> nocount">
					<a href="<?php echo Yii::app()->createUrl('/mbRecord/noneServices'); ?>" class="<?php echo Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id!='noneServices'?'active':'noactive'; ?>">
						Услуги вне списка
					</a>
				</li>
                <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id=='partnersList' ?'active':'noactive'; ?> nocount">
                    <a href="<?php echo Yii::app()->createUrl('/mbRecord/partnersList'); ?>" class="<?php echo Yii::app()->controller->id=='mbRecord' && Yii::app()->controller->action->id!='partnersList'?'active':'noactive'; ?>">
                        Партнерка
                    </a>
                </li>

            <?php endif;?>
	        <?php if(Yii::app()->user->checkAccess('NDasha')): ?>
            <!--<li class="nav-statistics <?php echo Yii::app()->controller->id=='mbLaw' && Yii::app()->controller->action->id=='clinic' ?'active':'noactive'; ?> nocount">
                <a href="<?php echo Yii::app()->createUrl('/mbLaw/clinic'); ?>" class="<?php echo Yii::app()->controller->id=='mbLaw' && Yii::app()->controller->action->id=='clinic' ?'active':'noactive'; ?>">
                    Юридические
                </a>
            </li>
            <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbLawSecond' && Yii::app()->controller->action->id=='clinic' ?'active':'noactive'; ?> nocount">
                <a href="<?php echo Yii::app()->createUrl('/mbLawSecond/admin'); ?>" class="<?php echo Yii::app()->controller->id=='mbLawSecond' && Yii::app()->controller->action->id=='admin' ?'active':'noactive'; ?>">
                    АККАУНТ
                </a>
            </li>-->
            <li class="nav-statistics <?php echo Yii::app()->controller->id=='mbLawSecond' && Yii::app()->controller->action->id=='clinic' ?'active':'noactive'; ?> nocount">
	            <a href="<?php echo Yii::app()->createUrl('/mbLawReports'); ?>" class="<?php echo Yii::app()->controller->id=='mbLawReports' && Yii::app()->controller->action->id=='reports' ?'active':'noactive'; ?>">
		            АККАУНТ2
	            </a>
            </li>
            <?php endif; ?>
            <?php if(Yii::app()->user->checkAccess('Toperator')||Yii::app()->user->checkAccess('Team')): ?>
                <li class="nav-statistics <?php echo Yii::app()->controller->id=='operatorLogin' && Yii::app()->controller->action->id=='admin' ?'active':'noactive'; ?> nocount">
                    <a href="<?php echo Yii::app()->createUrl('/operatorLogin/admin'); ?>" class="<?php echo Yii::app()->controller->id=='operatorLogin' && Yii::app()->controller->action->id=='admin' ?'active':'noactive'; ?>">
                        Отчет по операторам
                    </a>
                </li>
            <?php endif; ?>
            <?php if(Yii::app()->user->checkAccess('Administrator')||Yii::app()->user->checkAccess('Taccounter')||Yii::app()->user->checkAccess('Toperator')||Yii::app()->user->checkAccess('Team')||Yii::app()->user->checkAccess('Operator')||Yii::app()->user->checkAccess('NDasha')): ?>
                <li class="nav-statistics <?php echo Yii::app()->controller->id=='pamiClient' && Yii::app()->controller->action->id=='index' ?'active':'noactive'; ?> nocount">
                    <a href="<?php echo Yii::app()->createUrl('/pamiClient/'); ?>" class="<?php echo Yii::app()->controller->id=='pamiClient' && Yii::app()->controller->action->id=='index' ?'active':'noactive'; ?>">
                        Управление звонками
                    </a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>