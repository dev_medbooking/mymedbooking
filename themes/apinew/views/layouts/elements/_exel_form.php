<?php if (Yii::app()->controller->action->id == 'reports' && Yii::app()->controller->id== 'mbLawReports'): ?>
    <form class="exel_form clearfix">
        <div class="check_wrapper">
            <input type="checkbox" name="t.id" />
	        <label>ID</label>
	        <input type="checkbox" name="u.nameperson" />
	        <label>Аккаунт менеджер</label>
            <input type="checkbox" name="t.title_company" />
            <label>Юридическое название</label>
            <input type="checkbox" name="ilr.patient_record" />
            <label>Записи</label>
            <input type="checkbox" name="ilr.patient_count" />
            <label>Пациенты</label>
            <input type="checkbox" name="ilr.patient_success" />
            <label>Соглас.</label>
            <input type="checkbox" name="ilr.summ" />
            <label>Цена</label>
            <input type="checkbox" name="ilr.status" />
            <label>Статус</label>
            <input type="checkbox" name="ilr.patient_diag" />
            <label>Диагн.</label>
            <input type="checkbox" name="ilr.summ_diag" />
            <label>Сумма д.</label>
            <input type="checkbox" name="ilr.patient_analyz" />
            <label>Анализы</label>
            <input type="checkbox" name="ilr.summ_analyz" />
            <label>Сумма а.</label>





        </div>
        <div class="btn_wrapper_down clearfix">
            <button class="import_exel" data-href="<?php echo Yii::app()->createUrl('mbLawReports/exelAdminLaw'); ?>">Юр.лица</button><br>
	        <a href="<?php echo Yii::app()->createUrl('mbLawReports/exelLostOrders', array('queryDate' => $queryDate, 'queryDateEnd' => $queryDateEnd)); ?>" target="_blank">Заявки</a>
            <a href="<?php echo Yii::app()->createUrl('mbLawReports/exelExpensiveService', array('queryDate' => $queryDate, 'queryDateEnd' => $queryDateEnd)); ?>" target="_blank">Дор. услуги</a><br>
            <a href="<?php echo Yii::app()->createUrl('mbLawReports/exelPriceReport', array('from' => $queryDate, 'to' => $queryDateEnd)); ?>" target="_blank">Отчет с ценами</a>
        </div>
    </form>
<?php endif; ?>