<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]--><!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]--><!--[if IE 8]><html class="no-js lt-ie9"><![endif]--><!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<title><?=(preg_match('#^https?://crm#', Yii::app()->request->hostInfo) ? 'CRM.' : 'API.');?>MYMEDBOOKING</title>
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/css.css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/chosen.css" />
	<script type="text/javascript" src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
	<script src="https://struts2-jquery.googlecode.com/svn-history/r1647/trunk/struts2-jquery-plugin/src/main/resources/template/js/plugins/jquery-ui-timepicker-addon.js"></script>
	<script src="https://dew-scrumy.googlecode.com/svn-history/r240/trunk/FulbitoPortal/WebContent/js/jquery-ui-sliderAccess.js"></script>
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/ui/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/ui/jquery-ui.structure.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/ui/jquery-ui.theme.min.css">
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.maskedinput.min.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/all_new.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/chosen.jquery.min.js"></script>
	<script>
		$(function() {
			$("select").chosen({disable_search_threshold: 10});
			$('.table_law .sort .chosen-container-single .chosen-single span').text('Укажите статус');
		})
	</script>
</head>
<body>
<section id="wrapper">
	<header id="header">
		<div class="center">
			<a class="header__logo" href="/mbLawReports">API. Аккаунт</a>
			<a class="headerBtnMenu" href="#"></a>
		</div>
	</header>
	<section class="menuWrap">
		<div class="menuWrap__hiddenMenu">
			<ul class="center clearfix">
				<li><a href="<?=Yii::app()->createUrl('mbRecord');?>">API. Колл-центр</a></li>
				<li class="active"><a href="<?=Yii::app()->createUrl('mbLawReports/reports');?>">API. Аккаунт</a></li>
			</ul>
		</div>
		<div class="menuWrap__reportMenu center clearfix">
			<ul class="menuWrap__reportMenu__list clearfix">
				<li class="menuWrap__reportMenu__list__btn"><a href="<?=Yii::app()->createUrl('mbLawReports/create');?>">+</a></li>
				<li class="menuWrap__reportMenu__list__btn menuWrap__reportMenu__list__btn_K"><a href="<?=Yii::app()->createUrl('mbLawReports/createClinic');?>">K</a></li>
				<li class="menuWrap__reportMenu__list__btn menuWrap__reportMenu__list__btn_K"><a href="<?=Yii::app()->createUrl('register/create');?>">Д</a></li>
				<li class="menuWrap__reportMenu__list__btn menuWrap__reportMenu__list__btn_K"><a href="<?=Yii::app()->createUrl('register/createCommon');?>">Дп</a></li>
				<li class="menuWrap__reportMenu__list__btn menuWrap__reportMenu__list__btn_K <?=(empty($_GET['status']) AND Yii::app()->controller->action->id == 'reports' AND Yii::app()->controller->id == 'mbLawReports' ? 'active' : '');?> menu_icon_1">
					<a href="<?=Yii::app()->createUrl('mbLawReports');?>"></a>
				</li>
				<?php foreach (IntegratorLawReports::$statusText as $key => $value) :?>
					<li class="<?=((( ! empty($_GET['status']) AND $_GET['status'] == $key) AND Yii::app()->controller->action->id == 'reports' AND Yii::app()->controller->id == 'mbLawReports') ? 'active' : '');?>">
						<a href="<?=Yii::app()->createUrl('mbLawReports');?>/?status=<?=$key;?>"><?=$value;?></a>
					</li>
				<?php endforeach;?>
				<?php foreach (IntegratorLawReportsPayments::$paymentText as $key => $value) :?>
					<li class="<?=((( ! empty($_GET['status']) AND $_GET['status'] == $key) AND Yii::app()->controller->action->id == 'payments' AND Yii::app()->controller->id == 'mbLawReports') ? 'active' : '');?>">
						<a href="<?=Yii::app()->createUrl('mbLawReports/payments');?>/?status=<?=$key;?>"><?=$value;?></a>
					</li>
				<?php endforeach;?>
                <li class="menuWrap__reportMenu__sub js-sub-menu">
                <a href="#">...</a>
                <ul class="menuWrap__reportMenu__sub__menu">
                     <li class="<?=((Yii::app()->controller->action->id == 'periodPayments' AND Yii::app()->controller->id == 'mbLawReports') ? 'active' : '');?>">
                        <a href="<?=Yii::app()->createUrl('mbLawReports/periodPayments');?>">Отчет по оплатам</a>
                    </li>
                    <li class="<?=((Yii::app()->controller->action->id == 'partnerReports' AND Yii::app()->controller->id == 'mbLawReports') ? 'active' : '');?>">
                        <a href="<?=Yii::app()->createUrl('mbLawReports/partnerReports');?>">Отчет по партнерке</a>
                    </li>
                    <li class="<?=((Yii::app()->controller->action->id == 'graduationSecond' AND Yii::app()->controller->id == 'mbLawReports') ? 'active' : '');?>">
                        <a href="<?=Yii::app()->createUrl('mbLawReports/graduationSecond');?>">Градации</a>
                    </li>
	                <li class="<?=((Yii::app()->controller->action->id == 'reportsRatingSecond' AND Yii::app()->controller->id == 'mbLawReports') ? 'active' : '');?>">
		                <a href="<?=Yii::app()->createUrl('mbLawReports/reportsRatingSecond');?>">Рейтинги</a>
	                </li>
                    <li class=" <?=Yii::app()->controller->action->id == 'law' ? 'active' : '';?>">
                        <a href="<?=Yii::app()->createUrl('mbLawReports/law');?>">Юр. лица</a>
                    </li>
                    <li class="<?=Yii::app()->controller->id == 'partner' ? 'active' : '';?>"><a href="<?=Yii::app()->createUrl('partner/admin');?>">Партнеры</a></li>
                    <li class="<?=Yii::app()->controller->id == 'domainPhone' ? 'active' : '';?>"><a href="<?=Yii::app()->createUrl('domainPhone/admin');?>">Телефоны</a></li>
	                <li class="<?=(Yii::app()->controller->id == 'mbLawReports' AND Yii::app()->controller->action->id == 'medbookingClinics') ? 'active' : '';?>"><a href="<?=Yii::app()->createUrl('mbLawReports/medbookingClinics');?>">Клиники медбукинга</a></li>
					<li class="<?=(Yii::app()->controller->id == 'mbLawReports' AND Yii::app()->controller->action->id == 'diagnosticMedbookingClinics') ? 'active' : '';?>"><a href="<?=Yii::app()->createUrl('mbLawReports/diagnosticMedbookingClinics');?>">Клиники диагностики</a></li>
			    </ul>
                </li>
			</ul>
			<?php if((Yii::app()->controller->action->id == 'reports' OR Yii::app()->controller->action->id == 'payments' OR Yii::app()->controller->action->id == 'periodPayments' OR Yii::app()->controller->action->id == 'partnerReports' OR Yii::app()->controller->action->id == 'graduation' OR Yii::app()->controller->action->id == 'graduationSecond') AND Yii::app()->controller->id == 'mbLawReports') :?>
				<div class="menuWrap__reportMenu__selectBox clearfix">
                    <div class="data_select_one">
                        <input type="text" id="js-query-date-start" value="<?=$this->queryDate;?>" />
                        <input type="text" id="js-query-date-end" value="<?=$this->queryDateEnd;?>" />
                        <input type="button" value="" id="js-show-date">
                    </div>
					<?=CHtml::dropDownList('law_month', $this->month_number, array('01' => 'Янв', '02' => 'Фев', '03' => 'Март', '04' => 'Апр', '05' => 'Май', '06' => 'Июнь', '07' => 'Июль', '08' => 'Авг', '09' => 'Сен', '10' => 'Окт', '11' => 'Ноя','12' => 'Дек'), array('class' => 'chosen-select', 'id' => 'law_month'));?>
					<?=CHtml::dropDownList('law_year', $this->year_number, array('2015' => '2015', '2016' => '2016', '2017' => '2017', '2018' => '2018'), array('class' => 'chosen-select', 'id' => 'law_year'));?>
				</div>
			<?php endif;?>
			<?php $this->renderPartial('//layouts/elements/_exel_form', array('queryDate' => ( ! empty($this->queryDate) ? $this->queryDate : ''), 'queryDateEnd' => ( ! empty($this->queryDateEnd) ? $this->queryDateEnd : '')));?>
		</div>
	</section>
	<?php if (empty($_GET['status']) AND Yii::app()->controller->action->id == 'reports' AND Yii::app()->controller->id) :?>
        <h1 class="title_page center">Общая статистика</h1>
	<?php elseif (( ! empty($_GET['status']) AND isset(IntegratorLawReports::$statusHeaders[$_GET['status']])) AND Yii::app()->controller->action->id == 'reports' AND Yii::app()->controller->id == 'mbLawReports') :?>
        <h1 class="title_page center"><?=IntegratorLawReports::$statusHeaders[$_GET['status']];?></h1>
	<?php endif;?>
	<?php if (Yii::app()->controller->id == 'mbLawReports') :?>
	<div class="moneyInfo center">
		<?php $todayPayments = $this->getTodayPayments(); $resultSum = 0;?>
		<?php foreach($todayPayments as $key => $value) :?>
			<?php $resultSum += $value->summ;?>
            <?=IntegratorLawReportsPayments::$paymentText[$value->payment_type];?> за сегодня: <strong><?=$value->summ;?></strong>&nbsp;&nbsp;&nbsp;<span class="vhr">|</span>&nbsp;&nbsp;&nbsp;
		<?php endforeach;?>
        Сумма оплат за сегодня: <strong><?=$resultSum;?></strong>
	</div>
	<?php endif;?>
	<?php echo $content; ?>
</section>
</body>
</html>