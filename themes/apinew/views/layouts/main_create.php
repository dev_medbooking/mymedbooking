<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<?php Yii::app()->clientScript->registerPackage('jquery.ui'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="UTF-8" />
<title><?=(preg_match('#^https?://crm#', Yii::app()->request->hostInfo) ? 'CRM.' : 'API.');?>MYMEDBOOKING</title>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/prefixfree.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/chosen.jquery.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.maskedinput.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/chosen.css">
<script src="https://struts2-jquery.googlecode.com/svn-history/r1647/trunk/struts2-jquery-plugin/src/main/resources/template/js/plugins/jquery-ui-timepicker-addon.js"></script>
<script src="https://dew-scrumy.googlecode.com/svn-history/r240/trunk/FulbitoPortal/WebContent/js/jquery-ui-sliderAccess.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/ui/jquery-ui.structure.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/ui/jquery-ui.theme.min.css">
<link rel="stylesheet/less" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.less">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/less-1.3.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
//        $('select').chosen({disable_search_threshold: 10});
        $('#timeContainer').datetimepicker({
            lang: "ru",
            dayOfWeekStart: 1,
            step: 5,
            onSelect: function(dateText, inst) {
                var miliseconds = 2628000000;
                var currentTime = new Date();
                var selectedDate = new Date(dateText);
            }
        });
        $('#timeContainer2').datetimepicker({
            lang: "ru",
            dayOfWeekStart: 1,
            step: 5,
            onSelect: function(dateText, inst) {
                var miliseconds = 2628000000;
                var currentTime = new Date();
                var selectedDate = new Date(dateText);
            }
        });
        $('#callTime').datetimepicker({
            lang:'ru',
            dayOfWeekStart: 1,
            step: 5,
            onSelect: function(dateText, inst) {
                var miliseconds = 2628000000;
                var currentTime = new Date();
                var selectedDate = new Date(dateText);
            }
        });
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Текущее время',
            closeText: 'Закрыть',
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);
    })
</script>
</head>
<body>
<section id="wrapper">
    <div class="overlay"></div>
    <?php $this->renderPartial('//layouts/elements/_header'); ?>
    <?php $this->renderPartial('//layouts/elements/_menu'); ?>
    <section id="middle" class="clearfix">
        <div class="inner-section">
            <section class="content">
                <?php echo $content;?>
            </section>
        </div>
    </section>
</section>
<?php $this->renderPartial('//layouts/elements/_footer'); ?>
</body>
</html>