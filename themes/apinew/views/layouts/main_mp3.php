<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title><?=(preg_match('#^https?://crm#', Yii::app()->request->hostInfo) ? 'CRM.' : 'API.');?>MYMEDBOOKING</title>
</head>
<body>
    <?php echo $content;?>
</body>
</html>