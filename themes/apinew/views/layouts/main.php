<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]--><!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]--><!--[if IE 8]><html class="no-js lt-ie9"><![endif]--><!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
  <meta charset="UTF-8" />
  <title><?=(preg_match('#^https?://crm#', Yii::app()->request->hostInfo) ? 'CRM.' : 'API.');?>MYMEDBOOKING</title>
  <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
  <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pickmeup.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pickmeup.css">
    <script>
        $(function(){
            $(".exel_form input").iCheck({
                checkboxClass: 'icheckbox_polaris',
                radioClass: 'iradio_polaris',
                increaseArea: '-10%'
        });
        })
    </script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.pickmeup.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/prefixfree.min.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/calendar-send.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/ZeroClipboard.js"></script>
  <script>
    $(function() {
        $('.single').pickmeup({
            flat: true
        });
        $('.multiple').pickmeup({
            flat: true,
            mode: 'multiple'
        });
        $('.range').pickmeup({
            flat: true,
            mode: 'range'
        });
        var plus_5_days = new Date;
        plus_5_days.addDays(5);
        $('.calendars').pickmeup({
            flat: true,
            date: [
                new Date,
                plus_5_days
            ],
            mode: 'range',
            calendars: 5
        });
    });
  </script>
  <?php if(Yii::app()->controller->action->id=='report'
      || Yii::app()->controller->action->id=='failure'
      || Yii::app()->controller->action->id=='diagramms'
      || Yii::app()->controller->action->id=='statistic2'
      || Yii::app()->controller->action->id=='lead'
      || Yii::app()->controller->action->id=='forTime'
      || Yii::app()->controller->action->id=='were'
      || Yii::app()->controller->action->id=='rerecord2'
      || Yii::app()->controller->action->id=='report'
      || Yii::app()->controller->action->id=='recording'
      || Yii::app()->controller->action->id=='account'): ?>
  <script src="<?php echo Yii::app()->theme->baseUrl.'/js/chart.js'; ?>"></script>
  <script>!function(){var e=Chart.helpers;Chart.defaults.global.responsive=!0,Chart.defaults.global.animation=!1;var t=document.getElementById("js-toggle-menu");e.addEvent(t,"click",function(){document.body.className=-1!==document.body.className.indexOf("open-menu")?"closed-menu":"open-menu"});var n=document.getElementsByTagName("article");e.each(n,function(t){if(t.id){var n=t.getElementsByTagName("canvas"),a=t.getElementsByTagName("h3"),l=t.querySelectorAll(".javascript"),i=Array.prototype.slice.call(l,0,2);if(articleId=t.id,list=document.createElement("ul"),navigationItem=document.getElementById("link-"+articleId),list.className="subsection-navigation",e.each(a,function(e){var t=document.createElement("li");e.id=articleId+"-"+e.textContent.replace(/\s+/g,"-").toLowerCase(),t.innerHTML='<a href="#'+e.id+'">'+e.textContent+"</a>",list.appendChild(t)}),a.length>0&&navigationItem.appendChild(list),n.length>0){var o="",c=[];e.each(n,function(e){c.push(e.getContext("2d"))}),c=c.length>1?c:c[0];for(var d=i.length-1;d>=0;d--)o+=i[d].textContent;new Function("ctx","options",o)(c)}}})}();</script>
  <?php endif; ?>
</head>
<body>
  <section id="wrapper">
    <?php $this->renderPartial('//layouts/elements/_header'); ?>
    <?php $this->renderPartial('//layouts/elements/_menu'); ?>
    <section id="middle" class="clearfix">
        <div class="inner-section">
            <?php $this->renderPartial('//mbRecord/elements/_section'); ?>
        </div>
        <div class="wrapper-calendars">
            <?php $this->renderPartial('//mbRecord/elements/_calendar'); ?>
        </div>
        <div class="inner-section">
            <section class="content">
                <?php echo $content;?>
            </section>
        </div>
    </section>
  </section>
  <?php $this->renderPartial('//layouts/elements/_footer'); ?>

</body>
</html>