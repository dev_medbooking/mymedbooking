<?php if (!empty($data)): ?>
    <tr>
        <?php foreach ($data as $key => $value): ?>
            <?php if (!empty($value['pick']) && !empty($value['content'])): ?>
                <th><?php echo !empty($value['content']) ? $value['content'] : ''; ?></th>
                <?php continue; endif; ?>
            <?php if (!empty($value['type'])): ?>
                <?php if (!empty($value['roles'])): ?>
                    <th><?php echo !empty($value['content']) ? $value['content'] : ''; ?></th>
                <?php endif; ?>
            <?php else: ?>
                <?php if (isset($value['visibility'])): ?>
                        <th><?php if (!empty($value['sort'])): ?><?php echo $sort->link($key, null, array('class' => $value['sort'])); ?><?php endif; ?></th>
                <?php else: ?>
                    <th><?php if (!empty($value['sort'])): ?><?php echo $sort->link($key, null, array('class' => $value['sort'])); ?><?php endif; ?></th>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </tr>
    <tr class="filters">
        <?php foreach ($data as $key => $value): ?>
            <?php if (!empty($value['type'])): ?>
                <?php if (!empty($value['roles'])): ?>
                    <td></td>
                <?php endif; ?>
            <?php else: ?>
                <?php if (!empty($value['visibility'])): ?>
                        <?php if (!empty($value['filter']) && $value['filter'] == 'select'): ?>
                            <th class="size_m">
                                <div class="table_search_wrap"> <?php if (!empty($value['filter'])): ?><?php echo CHtml::activeDropDownList($model, $key, (!empty($value['data']) ? $value['data'] : array()), array('class' => $value['filter'], 'id' => 'filter_' . $key)); ?><?php endif; ?></div>
                            </th>
                        <?php else: ?>
                            <th class="size_m">
                                <div class="table_search_wrap"> <?php if (!empty($value['filter'])): ?><?php echo CHtml::activeTextField($model, $key, array('class' => $value['filter'], 'id' => 'filter_' . $key)); ?><?php endif; ?></div></div>
                            </th>
                        <?php endif; ?>
                <?php else: ?>
                    <?php if (!empty($value['filter']) && $value['filter'] == 'select'): ?>
                        <th class="size_m">
                            <div class="table_search_wrap"><?php if (!empty($value['filter'])): ?><?php echo CHtml::activeDropDownList($model, $key, (!empty($value['data']) ? $value['data'] : array()), array('class' => $value['filter'], 'id' => 'filter_' . $key)); ?><?php endif; ?></div>
                        </th>
                    <?php else: ?>
                        <th class="size_m">
                            <div class="table_search_wrap"><?php if (!empty($value['filter'])): ?><?php echo CHtml::activeTextField($model, $key, array('class' => $value['filter'], 'id' => 'filter_' . $key)); ?><?php endif; ?></div>
                        </th>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </tr>
<?php endif; ?>