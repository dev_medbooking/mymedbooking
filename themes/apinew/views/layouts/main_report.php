<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]--><!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]--><!--[if IE 8]><html class="no-js lt-ie9"><![endif]--><!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
<meta charset="UTF-8" />
<title><?=(preg_match('#^https?://crm#', Yii::app()->request->hostInfo) ? 'CRM.' : 'API.');?>MYMEDBOOKING</title>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.pickmeup.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pickmeup.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/calendar-send-report.js"></script>
<script>
    $(function() {
        $('.single').pickmeup({
            flat: true
        });
        $('.multiple').pickmeup({
            flat: true,
            mode: 'multiple'
        });
        $('.range').pickmeup({
            flat: true,
            mode: 'range'
        });
        var plus_5_days = new Date;
        plus_5_days.addDays(5);
        $('.calendars').pickmeup({
            flat: true,
            date: [
                new Date,
                plus_5_days
            ],
            mode: 'range',
            calendars: 5
        })
    });
</script>
</head>
<body>
<section id="wrapper">
    <?php $this->renderPartial('//layouts/elements/_header'); ?>
    <?php $this->renderPartial('//layouts/elements/_menu'); ?>
    <section id="middle" class="clearfix">
        <div class="inner-section">
            <?php $this->renderPartial('//mbReport/_section'); ?>
        </div>
        <div class="wrapper-calendars">
            <?php $this->renderPartial('//mbReport/_calendar'); ?>
        </div>
        <div class="inner-section">
            <section class="content">
                <?php echo $content;?>
            </section>
        </div>
    </section>
</section>
<?php $this->renderPartial('//layouts/elements/_footer'); ?>
</body>
</html>