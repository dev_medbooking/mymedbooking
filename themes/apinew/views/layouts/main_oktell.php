<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]--><!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]--><!--[if IE 8]><html class="no-js lt-ie9"><![endif]--><!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
<meta charset="UTF-8" />
<title><?=(preg_match('#^https?://crm#', Yii::app()->request->hostInfo) ? 'CRM.' : 'API.');?>MYMEDBOOKING</title>
<!--<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />-->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/prefixfree.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js"></script>
<!--<link rel="stylesheet/less" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.less" />-->
<link rel="stylesheet/less" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/author.less" />
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/less-1.3.3.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-1.11.2.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
</head>
<body>
<script>
	$(document).ready(function(){
		$( "#resizable, #resizable8, #resizable9, #resizable10, #resizable11" ).resizable({
			resize: function( event, ui ) {
				$("body").width($("table").width() + 100);
			}
//						maxWidth: 70
		});
		$( "#resizable2, #resizable3, #resizable6, #resizable7" ).resizable({
			resize: function( event, ui ) {
				$("body").width($("table").width() + 100);
			}
//						maxWidth: 250
		});
		$( "#resizable4, #resizable5").resizable({
			resize: function( event, ui ) {
				$("body").width($("table").width() + 100);
			}
//						maxWidth: 350
		});
		$(".open_bar").click( function (){
			$(".content").css({
				marginLeft: "245px",
				transition: "all 0.2s"
			});
			$(".bar_active").css("margin-left","0");
			$(".bar_close").css("visibility","hidden");
			$(".setting_img").text("Изменить");
			$(".radio_fixed").show();

		});

		$(".js-btn-top, .btn_add").click( function (){
			$("#popup_edit").css("margin-right","0");

		});
		$("#popup_edit i").click( function (){
			$("#popup_edit").css("margin-right","-730px");

		});

		$("tr.message .js-message-open, .btn_message").click( function (){
			$("#popup_message").css("margin-right","0");

		});
		$("#popup_message i").click( function (){
			$("#popup_message").css("margin-right","-730px");

		});

		$("body").on("click", ".btn_setting", function(){
			if($(this).hasClass("active")){
				$(".setting_sub_menu").slideUp(100);
				$(this).removeClass("active");
			}else{
				$(".setting_sub_menu").slideDown(100);
				$(this).addClass("active");
			}
		});

		$("body").on("click", ".js_btn_ellipsis", function(){
			if($(this).hasClass("active")){
				$(".edit_sub_menu").slideUp(100);
				$(this).removeClass("active");
			}else{
				$(".edit_sub_menu").slideDown(100);
				$(this).addClass("active");
			}
		});

		$(".reply_message_link").click( function (){
			$(this).hide();
			$(".reply_message_hide").show();
			$(".reply_message").slideDown(100);

		});
		$(".reply_message_hide").click( function (){
			$(this).hide();
			$(".reply_message").slideUp(100);
			$(".reply_message_link").show();
		});

		$(".radio_fixed").click( function (){
			$(this).toggleClass("active");
		});

		function close(){
			if(!$(".radio_fixed").hasClass("active")){
				$(".bar_active").css("margin-left","-350px");

				$(".setting_img").text("");
				$(".radio_fixed").hide();
				$(".content").css({
					marginLeft: "100px",
					transition: "all 0.1s"
				});
				$(".bar_close").css("visibility","visible");
			}
		}
		var timeoutId;
		$("aside").hover(
			function () {
				clearTimeout(timeoutId);

			},
			function () {
				timeoutId = setTimeout(close, 4000);
			}
		);
		$("body").on("click",".touch-bar", function(){
			if(!$(this).hasClass("active")){
				$(".touch-bar").removeClass("active");
				$(this).addClass("active");
				$(".bar_active_item").stop().slideUp(300);
				$(this).next().stop().slideDown(300);
			}else{
				$(this).removeClass("active");
				$(".bar_active_item").stop().slideUp(300);
			}

			return false;
		});
	});

</script>
<style>
	#resizable {  height: 150px;  }
	.ui-resizable-handle{  width: 10px;
		width: 10px;
		height: 22px;
		background-color: transparent;
		position: absolute;
		top: 18px;
		right: -4px;
		cursor: move;
	}
	.ui-resizable{
		height: 90px !important;
	}
	#resizable, #resizable11, #resizable10, #resizable9, #resizable8{
		min-width: 60px !important;
	}
	#resizable2, #resizable3, #resizable6, #resizable7{
		min-width: 180px !important;
	}
	#resizable4, #resizable5{
		min-width: 300px !important;
	}
	th{
		position: relative;
	}
</style>
<?=$content;?>
</body>
</html>