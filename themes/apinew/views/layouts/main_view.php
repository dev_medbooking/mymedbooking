<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]--><!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]--><!--[if IE 8]><html class="no-js lt-ie9"><![endif]--><!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
<meta charset="UTF-8" />
<title><?=(preg_match('#^https?://crm#', Yii::app()->request->hostInfo) ? 'CRM.' : 'API.');?>MYMEDBOOKING</title>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/prefixfree.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js"></script>
<link rel="stylesheet/less" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.less" />
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/less-1.3.3.min.js" type="text/javascript"></script>
</head>
<body>
<section id="wrapper">
    <?php $this->renderPartial('//layouts/elements/_header'); ?>
    <?php $this->renderPartial('//layouts/elements/_menu'); ?>
    <section id="middle" class="clearfix">
        <div class="inner-section">
            <section class="content">
                <?php echo $content; ?>
            </section>
        </div>
    </section>
</section>
<?php $this->renderPartial('//layouts/elements/_footer'); ?>
</body>
</html>