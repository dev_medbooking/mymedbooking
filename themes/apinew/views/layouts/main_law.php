<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]--><!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]--><!--[if IE 8]><html class="no-js lt-ie9"><![endif]--><!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <title><?=(preg_match('#^https?://crm#', Yii::app()->request->hostInfo) ? 'CRM.' : 'API.');?>MYMEDBOOKING</title>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/css.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/chosen.css" />
        <script type="text/javascript" src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
        <script src="https://struts2-jquery.googlecode.com/svn-history/r1647/trunk/struts2-jquery-plugin/src/main/resources/template/js/plugins/jquery-ui-timepicker-addon.js"></script>
        <script src="https://dew-scrumy.googlecode.com/svn-history/r240/trunk/FulbitoPortal/WebContent/js/jquery-ui-sliderAccess.js"></script>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/ui/jquery-ui.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/ui/jquery-ui.structure.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/ui/jquery-ui.theme.min.css">
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.maskedinput.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/all.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/chosen.jquery.min.js"></script>
        <script>
            $(function() {
                $("select").chosen({disable_search_threshold: 10});
                $('.sort .chosen-container-single .chosen-single span').text('Укажите статус');
            })
        </script>
    </head>
    <body>
        <section id="wrapper">
            <header id="header">
                <div class="center">
                    <a class="header__logo" href="/mbLawSecond">API. Аккаунт</a>
                    <a class="headerBtnMenu" href="#"></a>
                </div>
            </header>
            <section class="menuWrap">
                <div class="menuWrap__hiddenMenu">
                    <ul class="center clearfix">
                        <li><a href="<?php echo Yii::app()->createUrl('mbRecord')?>">API. Колл-центр</a></li>
                        <li class="active"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/admin')?>">API. Аккаунт</a></li>
                    </ul>
                </div>
                <div class="menuWrap__reportMenu center clearfix">
                    <ul class="menuWrap__reportMenu__list clearfix">
                        <li class="menuWrap__reportMenu__list__btn"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/create');?>">+</a></li>
                        <li class="menuWrap__reportMenu__list__btn menuWrap__reportMenu__list__btn_K"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/createClinic');?>">K</a></li>
                        <li class="menuWrap__reportMenu__list__btn menuWrap__reportMenu__list__btn_K"><a href="<?php echo Yii::app()->createUrl('register/create');?>">Д</a></li>
                        <li class="menuWrap__reportMenu__list__btn menuWrap__reportMenu__list__btn_K"><a href="<?php echo Yii::app()->createUrl('register/createCommon');?>">Дп</a></li>
                        <li class="<?php echo empty($_GET['status'])&&Yii::app()->controller->action->id=='admin' && Yii::app()->controller->id== 'mbLawSecond'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('mbLawSecond');?>">Отчеты</a></li>
                        <li class="<?php echo !empty($_GET['status'])&&$_GET['status']==2&&Yii::app()->controller->action->id=='admin'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/admin',array('status'=>'2'));?>">Согласованные</a></li>
                        <li class="<?php echo !empty($_GET['status'])&&$_GET['status']==4&&Yii::app()->controller->action->id=='admin'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/admin',array('status'=>'4'));?>">Оплаченные</a></li>
                        <li class="<?php echo !empty($_GET['status'])&&$_GET['status']==3&&Yii::app()->controller->action->id=='admin'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/admin',array('status'=>'3'));?>">Счета за месяц</a></li>
	                    <li class="<?php echo !empty($_GET['status'])&&$_GET['status']==6&&Yii::app()->controller->action->id=='admin'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/admin',array('status'=>'6'));?>">Дебиторка</a></li>
	                    <li class="<?php echo !empty($_GET['status'])&&$_GET['status']==7&&Yii::app()->controller->action->id=='admin'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/admin',array('status'=>'7'));?>">Отказы</a></li>
                        <li class="<?php echo Yii::app()->controller->action->id=='law'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/law');?>">Юр.лица</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('mbRecord/admin',array('status'=>30));?>">Были</a></li>
                        <li class="<?php echo Yii::app()->controller->action->id=='clinics'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/clinics');?>">Клиники</a></li>
                        <li class="<?php echo Yii::app()->controller->action->id=='prices'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/prices');?>">Счета</a></li>
                        <li class="<?php echo Yii::app()->controller->action->id=='admin' && Yii::app()->controller->id== 'register'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('register/admin');?>">Договор</a></li>
                        <li class="<?php echo Yii::app()->controller->action->id=='adminCommon' && Yii::app()->controller->id== 'register'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('register/common');?>">Дополн.</a></li>
                        <li class="<?php echo Yii::app()->controller->action->id=='book' && Yii::app()->controller->id== 'mbLawSecond'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/book');?>">Book_ID.</a></li>
	                    <li class="<?php echo Yii::app()->controller->id == 'partner'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('partner/admin');?>">Партнеры</a></li>
	                    <li class="<?php echo Yii::app()->controller->id == 'domainPhone'?'active':'';?>"><a href="<?php echo Yii::app()->createUrl('domainPhone/admin');?>">Телефоны</a></li>
                    </ul>
                    <?php if((Yii::app()->controller->action->id=='admin'||Yii::app()->controller->action->id=='book'||Yii::app()->controller->action->id=='prices') && Yii::app()->controller->id== 'mbLawSecond'):?>
                        <div class="menuWrap__reportMenu__selectBox clearfix">
                            <?php echo CHtml::dropDownList('law_month', $this->month_number, array('01' =>'Янв','02' =>'Фев','03' =>'Март','04' => 'Апр','05' =>'Май','06' =>'Июнь','07' =>'Июль','08' => 'Авг','09' =>'Сен','10' =>'Окт','11' =>'Ноя','12' => 'Дек'),array('class'=>'chosen-select','id'=>'law_month')); ?>
                            <?php echo CHtml::dropDownList('law_year', $this->year_number, array('2015'=>'2015','2016'=>'2016','2017'=>'2017','2018'=>'2018'),array('class'=>'chosen-select','id'=>'law_year')); ?>
                            <input type="text" id="js-query-date-start" value="" />
                            <input type="text" id="js-query-date-end" value="" />
                            <input type="button" value="показать" id="js-show-date">
                        </div>
                    <?php endif;?>
                <?php $this->renderPartial('//layouts/elements/_exel_form');?>
                </div>

            </section>
            <?php echo $content; ?>
        </section>
    </body>
</html>