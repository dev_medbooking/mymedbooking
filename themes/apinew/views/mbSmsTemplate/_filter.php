<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id', null, array('class' => 'csorting')); ?></td>
        <td class="time_start<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('time_start', null, array('class' => 'csorting')); ?></td>
        <td class="time_end<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('time_end', null, array('class' => 'csorting')); ?></td>
        <td class="title<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('title', null, array('class' => 'csorting')); ?></td>
        <td class="message<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('message', null, array('class' => 'csorting')); ?></td>
        <td class="edit"></td>
        <td class="view"></td>
        <td class="delete"></td>
        <td class="icon-share"></td>
    </tr>
    <tr class="filters">
        <td><?php echo CHtml::activeTextField($model, 'id', array('class' => 'search-single', 'id' => 'filter_id')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'time_start', array('class' => 'search', 'id' => 'filter_time_start')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'time_end', array('class' => 'search', 'id' => 'filter_time_end')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'title', array('class' => 'search', 'id' => 'filter_title')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'message', array('class' => 'search', 'id' => 'filter_message')); ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</thead>
