<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/table2.js',CClientScript::POS_END);?>
<?php $this->renderPartial('navbar'); ?>
<div class="row-fluid mb-template">
    <?php echo CHtml::beginForm(array('/'.Yii::app()->controller->id.'/admin'),'get',array('id'=>'admin-form')); ?>
    <?php $this->renderPartial('_admin',array('model'=>$model,'sort'=>$sort,'pages'=>$pages,'count'=>$count,'data'=>$data,'dataCount'=>$dataCount)); ?>
    <?php echo CHtml::endForm(); ?>
</div>