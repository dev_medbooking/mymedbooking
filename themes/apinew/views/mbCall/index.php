<input type="hidden" name="report" value="call">
<form action="<?php echo Yii::app()->createUrl('/mbCall/' . Yii::app()->controller->action->id, array('host' => (!empty($_GET['host']) ? $_GET['host'] : ''))); ?>" class="call-form">
    <input type="hidden" value="" name="data-from"/>
    <input type="hidden" value="" name="data-to"/>
</form>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/table2.js',CClientScript::POS_END);?>
<?php $this->renderPartial('navbar'); ?>
<div class="row-fluid row-call">
    <?php echo CHtml::beginForm(array('/'.Yii::app()->controller->id.'/admin'),'get',array('id'=>'admin-form')); ?>
    <?php $this->renderPartial('_admin',array('model'=>$model,'sort'=>$sort,'pages'=>$pages,'count'=>$count,'data'=>$data,'dataCount'=>$dataCount)); ?>
    <?php echo CHtml::endForm(); ?>
</div>