<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id', null, array('class' => 'csorting')); ?></td>
        <td class="telephone<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('telephone', null, array('class' => 'csorting')); ?></td>
        <td class="create_time<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('create_time', null, array('class' => 'csorting')); ?></td>
        <td class="name<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('name', null, array('class' => 'csorting')); ?></td>
        <td class="message<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('message', null, array('class' => 'csorting')); ?></td>
        <td class="domain<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('domain', null, array('class' => 'csorting')); ?></td>
        <td class="edit"></td>
        <td class="view"></td>
        <td class="delete"></td>
        <td class="icon-share"></td>
        <td class="icon-create"></td>
    </tr>
    <tr class="filters">
        <td><?php echo CHtml::activeTextField($model, 'id', array('class' => 'search-single', 'id' => 'filter_id')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'telephone', array('class' => 'search', 'id' => 'filter_telephone')); ?></td>
        <td data-name_from="date_from" data-name_to="date_to"><?php echo CHtml::activeTextField($model, 'create_time', array('class' => 'search', 'id' => 'filter_create_time')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'name', array('class' => 'search', 'id' => 'filter_name')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'message', array('class' => 'search', 'id' => 'filter_message')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'domain', array('class' => 'search', 'id' => 'filter_domain')); ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</thead>