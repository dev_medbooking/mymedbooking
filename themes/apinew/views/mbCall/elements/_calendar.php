﻿<div class="calendars-inner">
    <div class="top-calendars clearfix">
        <div class="right">
            <span class="btn today active">Сегодня</span>
            <span class="btn yesterday">Вчера</span>
            <span class="btn week">За неделю</span>
        </div>
    </div>
    <div class="calendars"></div>
    <span class="calendar-submit" data-href="<?php echo Yii::app()->createUrl('/mbCall/admin',array('host'=>(!empty($_GET['host'])?$_GET['host']:''))); ?>">Применить</span>
</div>