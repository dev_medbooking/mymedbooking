<div id="create">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'create-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'form-horizontal'),
    )); ?>
        <?php if($model->errors): ?>
        <div class="well">
            <?php echo $form->errorSummary($model); ?>
        </div>
        <?php endif; ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'telephone',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'telephone',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'domain',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'domain',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'telephone',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php $this->widget('CMaskedTextField', array('model'=>$model,'attribute'=>'telephone','mask'=>'(999)999-9999','placeholder' => '_','htmlOptions'=>array('class'=>'span3'))); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'message',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textArea($model,'message',array('class'=>'span6')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="form-actions">
            <?php echo CHtml::submitButton("Сохранить",array("class"=>"btn btn-info"));?>
            <?php echo CHtml::resetButton("Сбросить",array("class"=>"btn"));?>
        </div>
    <?php $this->endWidget(); ?>
</div>