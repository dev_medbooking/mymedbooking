<div class="content clearfix">
	<div class="table_wrap">
		<div class="table_head clearfix">
			<div class="table_records_block left">
				<div class="table_title">Статистика</div>
				<div class="table_records"><span class="table_records_num"><?=$count;?></span> записей в таблице</div>
			</div>
			<div class="table_setting_block right">
				<input type="text" placeholder="Свободный поиск...">
				<span class="btn_n"></span>
			</div>
		</div>
		<table>
		<?=$this->renderPartial('//mbOktell/_filter', array('data' => $data, 'sort' => $sort, 'model' => $model, 'params' => $params, 'count' => $count));?>
		<?=$this->renderPartial('//mbOktell/_data', array('data' => $data, 'sort' => $sort, 'model' => $model, 'params' => $params, 'count' => $count));?>
		</table>
	</div>
	<div class="pagination" id="pages">
		<?php if ( ! empty($pages) AND ! empty($count)): ?>
			<?=$this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count));?>
		<?php endif; ?>
	</div>
</div>