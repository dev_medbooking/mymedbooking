<div class="createPage">

	<div class="application-content">
		<?php if ($model->isNewRecord) : ?>
			<h2 class="title">Новая заявка</h2>
		<?php else : ?>
			<h2 class="title">Редактирование заявки
				#<?php echo $model->id; ?> <?= ($model->ident_id) ? "П" : ""; ?></h2>
		<?php endif; ?>
		<p class="remark">Поля обязательные для заполнения отмечены <span>*</span></p>
		<p><a href="/mbOktell">Вернуться обратно</a></p>
	</div>

	<div id="create-block">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'admin-form',
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'htmlOptions' => array('class' => 'application-form'),
		)); ?>
		<?php if ($model->errors): ?>
			<div class="well"><?php echo $form->errorSummary($model); ?></div><?php endif; ?>
		<div class="row clearfix">
			<label for="MbRecord_name">ФИО <span>*</span></label>

			<div class="text-name note-form"><?php echo $form->textField($model, 'name', array()); ?></div>
		</div>

		<div class="row clearfix small_select">
			<label class="label_hidden">Пол</label>

			<div class="select-new"><?php echo $form->dropDownList($model, 'gender', array('0' => 'n/a', '1' => 'мужчина', '2' => 'женщина')); ?></div>
		</div>

		<div class="row clearfix small_select">
			<label class="label_hidden"></label>
			<?php echo $form->dropDownList($model, 'new_pacient', array(0 => 'Новый', 1 => 'Повторный')); ?>
		</div>

		<div class="row clearfix">
			<label for="phone">Телефон <span>*</span></label>

			<div class="text-phone note-form clearfix"><?php $this->widget('CMaskedTextField', array('model' => $model, 'attribute' => 'telephone', 'mask' => '+7(999)999-9999', 'placeholder' => '_', 'htmlOptions' => array('class' => 'span2', 'id' => 'phone', 'placeholder' => 'Телефон', 'data-href' => Yii::app()->createUrl('mbExel/phoneCheked')))); ?></div>
		</div>

		<div class="row clearfix">
			<label for="email">Е-mail</label>

			<div class="text-email clearfix">
				<?php echo $form->textField($model, 'email', array()); ?>
			</div>
		</div>

		<div class="row clearfix">
			<label for="timeContainer">На указанное время <span>*</span></label>

			<div class="datepicker-box">
				<div class="time1 note-form"><?php echo $form->textField($model, 'formattedRecTime', array('id' => 'timeContainer')); ?></div>
			</div>
		</div>

		<!-- blueBox -->
		<div class="createPage__box blueBox">

			<div class="row clearfix">
				<label for="phone_record">Телефон заявки</label>

				<div class="text-phone">
					<?php $this->widget('CMaskedTextField', array('model' => $model, 'attribute' => 'phone_record', 'mask' => '+7(999)999-9999', 'placeholder' => '_', 'htmlOptions' => array('class' => 'span2', 'id' => 'phone_record', 'placeholder' => 'Телефон', 'data-href' => Yii::app()->createUrl('mbExel/phoneCheked')))); ?>
				</div>
			</div>
			<?= $form->hiddenField($model, 'ident_id', array("class" => "ident_id")) ?>
			<div class="row clearfix">
				<label for="">Источник заявки</label>

				<div class="source-text">
					<?php echo $form->textField($model, 'source', array("class" => "source")); ?>
				</div>
			</div>

			<div class="row clearfix">
				<label for="">Выберите проект <span>*</span></label>

				<div class="select-new select-new-project note-form">
					<?php if ($model->isNewRecord): ?>
						<select id="domain_id" name="MbRecord[domain]" class="long_select">
							<option value="" selected="selected"></option>
							<?php $phones = MbDomainPhone::model()->findAll(array('order' => 'sort DESC, phone'));
							foreach ($phones as $key => $value):?>
								<option value="<?php echo $value->domain;?>"
								        data-phone="<?php echo $value->phone;?>"
								        <?php if (str_replace(array("+", "(", ")", " ", "-", "."), array("", "", "", "", "", ""), $value->phone) == $extPhone): ?>selected<?php endif;?>>
									<?php echo $value->phone;?>
									(<?php echo $value->domain;?> <?php echo !empty($value->domain_name) ? $value->domain_name : '';?>)
								</option>
							<?php endforeach; ?>
						</select>

					<?php else: ?>
						<?php
						$select = 0;
						if (empty($model->domain_phone)) {
							if (!empty($model->domain)) {
								$select = MbDomainPhone::model()->findByAttributes(array('status' => 1, 'domain' => $model->domain));
							}
						}
						?>
						<select id="domain_id" name="MbRecord[domain]" class="long_select">
							<option value=""></option>
							<?php $phones = MbDomainPhone::model()->findAll(array('order' => 'domain_name, domain'));
							foreach ($phones as $key => $value):?>
								<option value="<?php echo $value->domain;?>" data-phone="<?php echo $value->phone;?>"
								        <?php if ($value->phone == $model->domain_phone): ?>selected<?php endif;?>
								        <?php if (!empty($select->phone) && $value->phone == $select->phone): ?>selected<?php endif;?>><?php echo $value->phone;?>
									(<?php echo $value->domain;?> <?php echo !empty($value->domain_name) ? $value->domain_name : '';?>)
								</option>
							<?php endforeach; ?>
						</select>
					<?php endif; ?>
					<?php echo $form->hiddenField($model, 'domain_phone', array()); ?>
				</div>
			</div>

		</div>
		<!-- end blueBlock -->

		<div class="row clearfix">
			<label for="">Клиника</label>
			<?php echo $form->textField($model, 'clinic_name', array('id' => 'clinic_name_auto', "class" => "clinic-text")); ?>
			<?php echo $form->hiddenField($model, 'clinic_id', array('id' => 'clinic_name_auto_id')); ?>
			<div class="error clinic_error" style="display: none">Это поле обязательное для статуса "Записать"</div>
		</div>
		<div class="row clearfix">
			<label for="">Доктор, специалист</label>
			<?php echo $form->textField($model, 'doctor_name', array('id' => 'doctor_name_auto', "class" => "doctor-text")); ?>
			<?php echo $form->hiddenField($model, 'doctor_id', array('id' => 'doctor_name_auto_id')); ?>
			<div class="error doctor_error" style="display:none;">Это поле обязательное для статуса "Записать"</div>
		</div>
		<div class="row clearfix">
			<label for="">Специалист <i>(если не указан доктор)</i></label>
			<?php echo $form->textField($model, 'category_name', array('id' => 'category_name_auto', "class" => "doctor-text")); ?>
		</div>
		<div class="row clearfix">
			<label for="">Услуга</label>
			<?php echo $form->textField($model, 'service', array('id' => 'service_auto', "class" => "doctor-text")); ?>
			<?php echo $form->hiddenField($model, 'category_id', array('id' => 'service_auto_id')); ?>
			<div class="error service_error" style="display:none;">Это поле обязательное для статуса "Записать"</div>
		</div>
		<div class="row clearfix">
			<?php echo $form->textArea($model, 'note', array('placeholder' => 'Пользовательский запрос')); ?>
			<div class="error note_error" style="display: none">Это поле обязательное для статуса "Записать"</div>
		</div>
		<div class="row clearfix">
			<?php echo $form->textArea($model, 'description', array('placeholder' => 'Описание')); ?>
		</div>
		<div class="row clearfix">
			<?php echo $form->textArea($model, 'comment', array('placeholder' => 'Примечание к заявке')); ?>
		</div>

		<!-- yellowBox -->
		<div class="createPage__box yellowBox">
			<div class="row clearfix">
				<label for="">Статус СМС</label>

<!--				<div class="source-text">--><?php //echo $form->dropDownList($model, 'sms_checked', array(0 => 'Отправлять', 1 => 'Не отправлять')); ?><!--</div>-->
				<div class="source-text"><?php echo $form->dropDownList($model, 'sms_checked', array(0 => 'Отправлять')); ?></div>
			</div>
			<div class="row row-max clearfix">
				<label for="">Статус заявки</label>

				<div class="select-new select-status note-form"><?php echo $form->dropDownList($model, 'status', array(0 => 'Создано', 1 => 'Отказ', 5 => 'Записать', 7 => 'Лист ожидания', 9 => 'Пустой звонок', 51 => 'Недозвон')); ?></div>
			</div>

			<div class="row clearfix">
				<label for="">Выберите причину отказа</label>

				<div class="select-new select-failure-cause">
					<?php echo $form->dropDownList($model, 'reason', array('' => '') + CHtml::listData(MbReason::model()->findAll("status!=1"), 'title', 'title'), array('class' => 'span4', 'disabled', 'prompt' => '&nbsp')); ?>
				</div>
			</div>

			<div class="row row-max clearfix">
				<label for="">Тип заявки <span>*</span></label>

				<div class="select-new select-new-record-type note-form">
					<?php echo $form->dropDownList($model, 'record_type', array('' => '', '1' => 'Общая', '2' => 'Диагностика', '3' => 'Анализы')); ?>
				</div>
			</div>

			<div class="row clearfix">
				<label for="">Пациент от нас</label>

				<div class="source-text"><?php echo $form->dropDownList($model, 'not_our_patient', array(0 => 'Да', 1 => 'Не от нас')); ?></div>
			</div>

			<?php echo $form->hiddenField($model, 'tool_checked', array('id' => 'tool_checked')); ?>
		</div>
		<!-- end yellowBox -->

		<div class="row clearfix">
			<button class="save_btn">Сохранить</button>
			<?php if (!empty($model->id)): ?><a href="<?php echo Yii::app()->createUrl('mbRecord/view', array('id' => $model->id)); ?>" style="float:right;">Просмотреть</a><?php endif; ?>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>