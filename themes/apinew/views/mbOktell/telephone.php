<div class="order-container clearfix">
		<h3 class="title-order"><?php echo CHtml::link('Добавить заявку', array('/mbOktell/createOrder')); ?></h3>
	<?php if (!empty($dataRecord)): ?>
		<div class="top-order-box">
			<h3 class="title-order">Заявки — <?=$telephone;?></h3>
		</div>
		<div class="order-boxes clearfix">
			<table class="" cellspacing="0" cellpadding="10"
			       style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
				<thead>
				<tr>
					<th style="border:1px solid #dfdfdf;">ID</th>
					<th style="border:1px solid #dfdfdf;">Время создания</th>
					<th style="border:1px solid #dfdfdf;">Статус</th>
					<th style="border:1px solid #dfdfdf;">ФИО</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($dataRecord as $value): ?>
					<tr>
						<th style="border:1px solid #dfdfdf;"><?php echo CHtml::link($value->id, array('/mbOktell/updateOrder', 'id' => $value->id)); ?></th>
						<th style="border:1px solid #dfdfdf;"><?php echo $value->create_time ?></th>
						<th style="border:1px solid #dfdfdf;"><?php echo $value->getStatusName() ?></th>
						<th style="border:1px solid #dfdfdf;"><?php echo $value->name ?></th>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<?php if (!empty($value->apisms_ar_s)): ?>
			<?php foreach ($dataRecord as $value): ?>
				<table cellspacing="0" cellpadding="10"
				       style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
					<thead>
					<tr>
						<th style="border:1px solid #dfdfdf;">Время создания</th>
						<th style="border:1px solid #dfdfdf;">Сообщение</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($value->apisms_ar_s as $v): ?>
						<tr>
							<th style="border:1px solid #dfdfdf;"><?php echo $value->create_time; ?></th>
							<th style="border:1px solid #dfdfdf;"><?php echo $v->message; ?></th>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			<?php endforeach; ?>
		<?php endif; ?>
	<?php endif; ?>
	<?php if (!empty($dataCall)): ?>
		<div class="top-order-box">
			<h3 class="title-order">Обр.Связь — <?=$telephone;?></h3>
		</div>
		<div class="order-boxes clearfix">
			<table class="" cellspacing="0" cellpadding="10"
			       style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
				<thead>
				<tr>
					<th style="border:1px solid #dfdfdf;">ID</th>
					<th style="border:1px solid #dfdfdf;">Время создания</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($dataCall as $value): ?>
					<tr>
						<th style="border:1px solid #dfdfdf;"><?php echo CHtml::link($value->id, array('/mbCall/view', 'id' => $value->id)); ?></th>
						<th style="border:1px solid #dfdfdf;"><?php echo $value->create_time; ?></th>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	<?php endif; ?>
</div>