<tbody>
	<?php if (!empty($data)) :?>
		<?php foreach ($data as $model) :?>
			<tr class="message">
				<td class="number size_s"><?=$model->id;?></td>
				<td class="number size_s"><?=(!empty($model->title) ? $model->title : '');?></td>
				<td class="number size_s"><?=(!empty($model->address) ? $model->address : '');?></td>
				<td class="number size_s"><?=(!empty($model->telephone) ? $model->telephone : '');?></td>
				<td class="number size_s">
					<?php foreach ($model->regimeClinic as $k => $v) :?>
						<?=$k;?>: <?=$v;?>
					<?php endforeach;?>
				</td>
				<td class="number size_s"><?=$model->specialistAll;?></td>
				<td class="number size_s"><?=$model->detAll;?></td>
				<td class="number size_s"><?=$model->subwayAll;?></td>
			</tr>
		<?php endforeach;?>
	<?php endif;?>
</tbody>