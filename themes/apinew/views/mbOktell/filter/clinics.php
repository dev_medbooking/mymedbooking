<thead>
	<tr>
		<th class="number size_s">
			<div id="resizable">
				<span><?=$sort->link('id', null, array('class'=>'csorting'));?><i class="separator_table"></i></span>
				<div class="table_search_wrap">
					<?=CHtml::activeTextField($model,'id',array()) ;?>
				</div>
			</div>
		</th>
		<th class="number size_s">
			<div id="resizable">
				<span><?=$sort->link('title', null, array('class'=>'csorting'));?><i class="separator_table"></i></span>
				<div class="table_search_wrap">
					<?=CHtml::activeTextField($model,'title',array()) ;?>
				</div>
			</div>
		</th>
		<th class="number size_s">
			<div id="resizable">
				<span><?=$sort->link('address', null, array('class'=>'csorting'));?><i class="separator_table"></i></span>
				<div class="table_search_wrap">
					<?=CHtml::activeTextField($model,'address',array()) ;?>
				</div>
			</div>
		</th>
		<th class="number size_s">
			<div id="resizable">
				<span><?=$sort->link('telephone', null, array('class'=>'csorting'));?><i class="separator_table"></i></span>
				<div class="table_search_wrap">
					<?=CHtml::activeTextField($model,'telephone',array()) ;?>
				</div>
			</div>
		</th>
		<th class="number size_s">
			<div id="resizable">
				<span>Режим<i class="separator_table"></i></span>
				<div class="table_search_wrap">

				</div>
			</div>
		</th>
		<th class="number size_s">
			<div id="resizable">
				<span>Специалисты<i class="separator_table"></i></span>
				<div class="table_search_wrap">
					<?=CHtml::activeTextField($model,'specialistAll',array()) ;?>
				</div>
			</div>
		</th>
		<th class="number size_s">
			<div id="resizable">
				<span>Детские<i class="separator_table"></i></span>
				<div class="table_search_wrap">
					<?=CHtml::activeTextField($model,'detAll',array()) ;?>
				</div>
			</div>
		</th>
		<th class="number size_s">
			<div id="resizable">
				<span>Метро<i class="separator_table"></i></span>
				<div class="table_search_wrap">
					<?=CHtml::activeTextField($model,'subwayAll',array()) ;?>
				</div>
			</div>
		</th><th class="number size_s">
			<div id="resizable">
				<span>Метро<i class="separator_table"></i></span>
				<div class="table_search_wrap">
					<?=CHtml::activeTextField($model,'subwayAll',array('1' => 'Москва', '4' => 'Питер')) ;?>
					<?=CHtml::activeDropDownList($model, 'clinic_city_line_id', array()) ;?>
				</div>
			</div>
		</th>

		<th class="menu_ellipsis">
		</th>
	</tr>
</thead>