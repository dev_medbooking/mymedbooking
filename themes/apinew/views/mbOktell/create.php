<script type="text/javascript">
	$(function () {
		$("#phone").mask("+7(999)999-9999", {'placeholder': '_'});
	});
	$(function () {
		$(".chosen-default>span").text("Выбрать");
		$(document).on('change', "#phone, #phone_record, #MbRecord_source", function () {
			$("#MbRecord_ident_id").val(0);
		});
	});
</script>
<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
<?php $this->renderPartial('//mbOktell/_operator/create', array('model' => $model, 'extPhone' => $extPhone)); ?>
<div id="ajax-block"></div>
<script type="text/javascript">
	$(function () {
		$("#doctor_name_auto").autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "http://" + $("#domain_id option:selected").val() + "/check/doctorJSON",
					dataType: "jsonp",
					data: {
						style: "full",
						maxRows: 12,
						term: request.term
					},
					success: function (data) {
						$("#doctor_name_auto_id").val('');
						response($.map(data, function (item) {
							return {
								label: item.title,
								value: item.title,
								id: item.id
							}
						}));
					}
				});
			},
			minLength: 2,
			select: function (event, ui) {
				if (ui.item) {
					$("#doctor_name_auto_id").val(ui.item.id);
				}
			}
		});
		$("#clinic_name_auto").autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "http://" + $("#domain_id option:selected").val() + "/check/clinicJSON",
					dataType: "jsonp",
					data: {
						style: "full",
						maxRows: 12,
						term: request.term,
						user:<?php echo (Yii::app()->user->checkAccess('NDasha'))?1:0;?>,
					},
					success: function (data) {
						response($.map(data, function (item) {
							return {
								label: item.title,
								value: item.title,
								id: item.id,
								none_status: item.none_status,
								fio: item.fio
							}
						}));
					}
				});
			},
			minLength: 2,
			select: function (event, ui) {
				if (ui.item) {
					$("#clinic_name_auto_id").val(ui.item.id);
					if (-1 < ui.item.value.toLowerCase().indexOf('медси')) {
						if ($("#comment_for_none_date").length == 0)
							$("#clinic_name_auto").parents(".row").after("<div class='row clearfix'><label for='comment_for_none_date'>Коммент</label><input id='comment_for_none_date' name='MbRecord[comment_for_none_date]' type='text' value=''></div>");
					} else {
						$("#comment_for_none_date").parents(".row").remove();
					}
				}
			}
		});
		$("#service_auto").autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "http://a.medbooking.com/check/servicesJSON",
					dataType: "jsonp",
					data: {
						style: "full",
						maxRows: 12,
						term: request.term,
						user:<?php echo (Yii::app()->user->checkAccess('NDasha'))?1:0;?>,
					},
					success: function (data) {
						response($.map(data, function (item) {
							return {
								label: item.title,
								value: item.title,
								id: item.id,
								type: item.type
							}
						}));
					}
				});
			},
			minLength: 2,
			select: function (event, ui) {
				if (ui.item) {
					$("#service_auto_id").val(ui.item.id);
					$("#MbRecord_record_type").val(ui.item.type);
				}
			}
		});
	});
</script>