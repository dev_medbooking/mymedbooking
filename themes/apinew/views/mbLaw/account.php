<nav class="nav submenu">
    <ul class="nav-menu clearfix">
        <li class="nav-applications">
            <a href="<?php echo Yii::app()->createUrl('mbLaw/price')?>">Счета</a>
        </li>
        <li class="nav-applications">
            <a class="active" href="<?php echo Yii::app()->createUrl('mbLaw/account')?>">Графики</a>
        </li>
    </ul>
</nav>
<?php if(!empty($data)):?>
<div class="list_charts">
    <div class="title">Таблица дошедших клиентов</div>
    <canvas id="myChart" height="100" width="400" style="width: 800px; height: 300px;"></canvas>
    <div class="title">Таблица согласованных  клиентов</div>
    <canvas id="myChart2" height="100" width="400" style="width: 800px; height: 300px;"></canvas>
    <div class="title">Таблица cтоимости</div>
    <canvas id="myChart3" height="100" width="400" style="width: 800px; height: 300px;"></canvas>
    <div class="title">Таблица кол-ва клиник</div>
    <canvas id="myChart4" height="100" width="400" style="width: 800px; height: 300px;"></canvas>
    <script>
        var data = [
            <?php foreach ($data as $key=>$value):?>
            {
                value: <?php echo !empty($value['koll_come']) ? $value['koll_come'] : '0'; ?>,
                color: "<?php echo !empty($value['color']) ? $value['color'] : '#fff'; ?>",
                highlight: "<?php echo !empty($value['color']) ? $value['color'] : '#fff'; ?>",
                label: "<?php echo !empty($value['email']) ? $value['email'] : '#fff'; ?>"
            },
            <?php endforeach;?>
        ]
        var data2 = [
            <?php foreach ($data as $key=>$value):?>
            {
                value: <?php echo !empty($value['koll_success']) ? $value['koll_success'] : '0'; ?>,
                color: "<?php echo !empty($value['color']) ? $value['color'] : '#fff'; ?>",
                highlight: "<?php echo !empty($value['color']) ? $value['color'] : '#fff'; ?>",
                label: "<?php echo !empty($value['email']) ? $value['email'] : '#fff'; ?>"
            },
            <?php endforeach;?>
        ]
        var data3 = [
            <?php foreach ($data as $key=>$value):?>
            {
                value: <?php echo !empty($value['all_price']) ? $value['all_price'] : '0'; ?>,
                color: "<?php echo !empty($value['color']) ? $value['color'] : '#fff'; ?>",
                highlight: "<?php echo !empty($value['color']) ? $value['color'] : '#fff'; ?>",
                label: "<?php echo !empty($value['email']) ? $value['email'] : '#fff'; ?>"
            },
            <?php endforeach;?>
        ]
        var data4 = [
            <?php foreach ($data as $key=>$value):?>
            {
                value: <?php echo !empty($value['koll_clinic']) ? $value['koll_clinic'] : '0'; ?>,
                color: "<?php echo !empty($value['color']) ? $value['color'] : '#fff'; ?>",
                highlight: "<?php echo !empty($value['color']) ? $value['color'] : '#fff'; ?>",
                label: "<?php echo !empty($value['email']) ? $value['email'] : '#fff'; ?>"
            },
            <?php endforeach;?>
        ]
        var options = {
            segmentShowStroke: true,
            segmentStrokeColor: "#fff",
            segmentStrokeWidth: 2,
            percentageInnerCutout: 50,
            animationSteps: 100,
            animationEasing: "easeOutBounce",
            animateRotate: true,
            animateScale: false,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        }
        var ctx = $("#myChart").get(0).getContext("2d");
        var myDoughnutChart = new Chart(ctx).Doughnut(data, options);
        var ctx = $("#myChart2").get(0).getContext("2d");
        var myDoughnutChart = new Chart(ctx).Doughnut(data2, options);
        var ctx = $("#myChart3").get(0).getContext("2d");
        var myDoughnutChart = new Chart(ctx).Doughnut(data3, options);
        var ctx = $("#myChart4").get(0).getContext("2d");
        var myDoughnutChart = new Chart(ctx).Doughnut(data4, options);
    </script>
</div>
<div></div>
<?php endif;?>