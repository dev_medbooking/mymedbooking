<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/table2.js',CClientScript::POS_END);?>
<?php $this->renderPartial('//mbLaw/exel/record',array()); ?>
<div class="hidden-block" style="display: none"></div>
<form action="<?php echo Yii::app()->createUrl('/mbLaw/'.Yii::app()->controller->action->id,array('status'=>(!empty($status)?$status:''),'host'=>(!empty($host)?$host:''))); ?>" method="POST" class="hidden-form">
    <input type="hidden" name="report" value="law" />
    <div class="summary" id="summary">
        <?php $this->renderPartial('//layouts/_admin_count',array('dataCount'=>$dataCount,'count'=>$count)); ?>
    </div>
    <table class="table-api-main">
        <?php $this->renderPartial('//mbLaw/_filter',array('sort'=>$sort,'model'=>$model,'status'=>'1')); ?>
        <tbody id="admin-tbody">
            <?php $this->renderPartial('_admin',array('data'=>$data)); ?>
        </tbody>
    </table>
    <div class="pagination row" id="pages">
        <?php if(!empty($pages)&&!empty($count)): ?>
            <?php $this->renderPartial('_pages',array('sort'=>$sort,'pages'=>$pages,'count'=>$count)); ?>
        <?php endif; ?>
    </div>
</form>