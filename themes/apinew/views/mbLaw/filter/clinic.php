<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"># <?php echo CHtml::activeTextField($model,'id',array('class'=>'search-single','id'=>'filter_id')); ?></td>
        <?php if (Yii::app()->controller->action->id != 'statistic'): ?>
        <td class="edit"></td>
        <td class="delete"></td>
        <?php endif; ?>
        <td class="title<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('title',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'title',array('class'=>'search-single','id'=>'filter_title')); ?></div></td>
        <td class="title_company<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('title_company',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'title_company',array('class'=>'search-single','id'=>'filter_title_company')); ?></div></td>
        <td class="koll_come<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('koll_come',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'koll_come',array('class'=>'search-single','id'=>'filter_koll_come')); ?></div></td>
        <td class="koll_success<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('koll_success',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'koll_success',array('class'=>'search-single','id'=>'filter_koll_success')); ?></div></td>
        <td class="price<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('price',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'price',array('class'=>'search-single','id'=>'filter_price')); ?></div></td>
        <td class="all_price<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('all_price',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'all_price',array('class'=>'search-single','id'=>'filter_all_price')); ?></div></td>
        <td class="contact<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('contact',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'contact',array('class'=>'search-single','id'=>'filter_contact')); ?></div></td>
        <td class="comment<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('comment',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'comment',array('class'=>'search-single','id'=>'filter_comment')); ?></div></td>
        <td class="email_callcent<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('email_callcent',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'email_callcent',array('class'=>'search-single','id'=>'filter_email_callcent')); ?></div></td>
        <td class="number_agree<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('numb_agree',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'numb_agree',array('class'=>'search-single','id'=>'filter_numb_agree')); ?></div></td>
        <td class="data_agree<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('data_agree',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'data_agree',array('class'=>'search-single','id'=>'filter_data_agree')); ?></div></td>
        <td class="subway<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('subway',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'subway',array('class'=>'search-single','id'=>'filter_subway')); ?></div></td>
        <td class="address<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('address',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'address',array('class'=>'search-single','id'=>'filter_address')); ?></div></td>
        <td class="agreement<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('agreement',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'agreement',array('class'=>'search-single','id'=>'filter_agreement')); ?></div></td>
        <td class="price_value<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('price_value',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'price_value',array('class'=>'search-single','id'=>'filter_price_value')); ?></div></td>
        <td class="po<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('po',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'po',array('class'=>'search-single','id'=>'filter_po')); ?></div></td>
        <td class="date<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('date',null,array('class'=>'csorting')); ?><div></div></td>
    </tr>
</thead>