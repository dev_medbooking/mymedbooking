<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"># <?php echo CHtml::activeTextField($model, 'id', array('class' => 'search-single', 'id' => 'filter_id')); ?></td>
        <td class="edition<?php echo !empty($status)?'':'_2'; ?>" data-name_from="lead_time_from" data-name_to="lead_time_to">
            <i><?php echo $sort->link('lead_time', null, array('class' => 'csorting')); ?></i>
            <?php if(isset($_GET['lead_time_from'])):?>
                <i class="dn5"><a class="lstr" href="#"><?=$_GET['lead_time_from'];?></a></i>
            <?php endif;?>
            <?php if(isset($_GET['lead_time_to'])):?>
                <i class="dn6"><a class="lstr" href="#"><?=$_GET['lead_time_to'];?></a></i>
            <?php endif;?>
            <input class="search" class="edition1" type="hidden" name="lead_time_from" value="">
            <input class="search" class="edition2" type="hidden" name="lead_time_to" value="">
        </td>
        <td class="name<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('name', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'name', array('class' => 'search-single', 'id' => 'filter_name')); ?></div></td>
        <td class="telephone<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('telephone', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'telephone', array('class' => 'search-single', 'id' => 'filter_telephone')); ?></div></td>
        <td class="clinic_name<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('clinic_name', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'clinic_name', array('class' => 'search-single', 'id' => 'filter_clinic_name')); ?></div></td>
        <td class="doctor_name<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('doctor_name', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'doctor_name', array('class' => 'search-single', 'id' => 'filter_doctor_name')); ?></div></td>
        <td class="title_company<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('title_company', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'title_company', array('class' => 'search-single', 'id' => 'filter_title_company')); ?></div></td>
        <?php if (Yii::app()->controller->action->id != 'statistic'): ?>
            <td class="edit"></td>
            <td class="view"></td>
        <?php endif; ?>
    </tr>
</thead>