<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"># <?php echo CHtml::activeTextField($model, 'id', array('class' => 'search-single', 'id' => 'filter_id')); ?></td>
        <td class="status<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('status', null, array('class' => 'csorting')); ?> <div></div></td>
        <td class="koll_come<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('koll_come', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'koll_come', array('class' => 'search-single', 'id' => 'filter_koll_come')); ?></div></td>
        <td class="koll_success<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('koll_success', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'koll_success', array('class' => 'search-single', 'id' => 'filter_koll_success')); ?></div></td>
        <td class="price<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('price', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'price', array('class' => 'search-single', 'id' => 'filter_price')); ?></div></td>
        <td class="all_price<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('all_price', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'all_price', array('class' => 'search-single', 'id' => 'filter_all_price')); ?></div></td>
        <td class="date_agree<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('date_agree', null, array('class' => 'csorting')); ?> <div></div>
            <input class="search" class="edition1" type="hidden" name="time_from" value="">
            <input class="search" class="edition2" type="hidden" name="time_to" value="">
        </td>
        <td class="comment<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('comment', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'comment', array('class' => 'search-single', 'id' => 'filter_comment')); ?></div></td>
        <?php if(empty($_GET['id'])):?>
            <td class="title<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('title', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'title', array('class' => 'search-single', 'id' => 'filter_title')); ?></div></td>
            <td class="title_company<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('title_company', null, array('class' => 'csorting')); ?> <div><?php echo CHtml::activeTextField($model, 'title_company', array('class' => 'search-single', 'id' => 'filter_title_company')); ?></div></td>
        <?php else:?>
            <input type="hidden" name="IntegratorClinicPrice[integrator_id]" value="<?php echo $_GET['id']?>">
        <?php endif;?>
        <?php if (Yii::app()->controller->action->id != 'statistic'): ?>
            <td class="edit"></td>
            <td class="view"></td>
            <td class="delete"></td>
        <?php endif; ?>
    </tr>
</thead>