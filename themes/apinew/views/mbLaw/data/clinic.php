<tr class="">
    <td style="background-color: <?php if($model->is_status==1):?>#f24841<?php elseif($model->is_status==2):?>#00FF00<?php endif;?>"><?php echo CHtml::link($model->id,array('mbLaw/price','id'=>$model->id)); ?></td>
    <?php if(Yii::app()->controller->action->id!='statistic'): ?>
    <td class="ico-edit"><?php echo CHtml::link('',array('update','id'=>$model->id)); ?></td>
    <td class="ico-delete"><?php echo CHtml::link('',array('delete','id'=>$model->id)); ?></td>
    <?php endif; ?>
    <td><?php echo !empty($model->title)?$model->title:''; ?></td>
    <td><?php echo !empty($model->title_company)?$model->title_company:''; ?></td>
    <td><?php echo !empty($model->koll_come)?$model->koll_come:''; ?></td>
    <td><?php echo !empty($model->koll_success)?$model->koll_success:''; ?></td>
    <td><?php echo !empty($model->price)?$model->price:''; ?></td>
    <td><?php echo !empty($model->all_price)?$model->all_price:''; ?></td>
    <td><?php echo !empty($model->contact)?$model->contact:''; ?></td>
    <td><?php echo !empty($model->comment)?$model->comment:''; ?></td>
    <td><?php echo CHtml::link(!empty($model->u->email)?$model->u->email:'',array('mbLaw/account','id'=>$model->uid)); ?></td>
    <td><?php echo !empty($model->numb_agree)?$model->numb_agree:''; ?></td>
    <td><?php echo !empty($model->data_agree)?$model->data_agree:''; ?></td>
    <td><?php echo !empty($model->subway)?$model->subway:''; ?></td>
    <td><?php echo !empty($model->address)?$model->address:''; ?></td>
    <td><?php echo !empty($model->agreement)?$model->agreement:''; ?></td>
    <td><?php echo !empty($model->price_value)?$model->price_value:''; ?></td>
    <td><?php echo !empty($model->po)?$model->po:''; ?></td>
    <td><?php echo !empty($model->date)?$model->date:''; ?></td>
</tr>