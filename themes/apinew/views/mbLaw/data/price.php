<tr>
    <td<?php echo !empty($model->statusColor)?' style="background-color:'.$model->statusColor.';"':''; ?>><?php echo $model->id; ?></td>
    <td><?php echo !empty($model->statusText)?$model->statusText:''; ?></td>
    <td><?php echo !empty($model->koll_come)?$model->koll_come:''; ?></td>
    <td><?php echo !empty($model->koll_success)?$model->koll_success:''; ?></td>
    <td><?php echo !empty($model->price)?$model->price:''; ?></td>
    <td><?php echo !empty($model->all_price)?$model->all_price:''; ?></td>
    <td><?php echo !empty($model->date_agree)?$model->date_agree:''; ?></td>
    <td><?php echo !empty($model->comment)?$model->comment:''; ?></td>
    <?php if(empty($_GET['id'])):?>
        <td><?php echo !empty($model->ic_price->title)?$model->ic_price->title:''; ?></td>
        <td><?php echo !empty($model->ic_price->title_company)?$model->ic_price->title_company:''; ?></td>
    <?php endif;?>
    <?php if(Yii::app()->controller->action->id!='statistic'): ?>
    <td class="ico-edit"><?php echo CHtml::link('',array('updatePrice','id'=>$model->id)); ?></td>
    <td class="ico-view"><?php echo CHtml::link('',array('viewPrice','id'=>$model->id)); ?></td>
    <td class="ico-delete"><?php echo CHtml::link('',array('deletePrice','id'=>$model->id)); ?></td>
    <?php endif; ?>
</tr>