<tr class="<?=!empty($model->flag_id)?'api-item-tr-'.$model->flag_id.' ':'api-item-tr ';?><?php echo $model->color; ?><?php $key%2?'-light':''; ?>">
    <td class="<?php echo empty($model->status)?'new':'old'; ?>"><?php echo $model->id; ?><span<?=!empty($model->fl->color)?'class="flag" style="background:'.$model->fl->color.';"':'';?>></span></td>
    <td><?php echo !empty($model->formattedLeadTime)?$model->formattedLeadTime:''; ?></td>
    <td><?php echo !empty($model->name)?$model->name:''; ?></td>
    <td><?php echo !empty($model->telephone)?CHtml::link($model->telephone,array('/mbRecord/telephone','id'=>$model->telephone)):''; ?></td>
    <td><?php echo !empty($model->clinic_name)?$model->clinic_name:''; ?></td>
    <td><?php echo !empty($model->doctor_name)?$model->doctor_name:''; ?></td>
    <td><?php echo !empty($model->integrator->title_company)?$model->integrator->title_company:''; ?></td>
    <?php if(Yii::app()->controller->action->id!='statistic'): ?>
    <td class="ico-edit"><?php echo CHtml::link('',array('mbRecord/update','id'=>$model->id)); ?></td>
    <td class="ico-view"><?php echo CHtml::link('',array('mbRecord/view','id'=>$model->id)); ?></td>
    <?php endif; ?>
</tr>