<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/table2.js',CClientScript::POS_END);?>
<nav class="nav submenu">
    <ul class="nav-menu clearfix">
        <li class="nav-applications">
            <a href="<?php echo Yii::app()->createUrl('mbLaw/create')?>" style="color:red;">Добавить</a>
        </li>
        <li class="nav-applications">
            <a href="<?php echo Yii::app()->createUrl('mbLaw/price')?>">Счета</a>
        </li>
        <li class="nav-applications">
            <a href="<?php echo Yii::app()->createUrl('mbLaw/account')?>">Графики</a>
        </li>
    </ul>
</nav>
<div class="_header_bottom_menu _width_header_bottom_menu">
    <?php $this->renderPartial('//mbLaw/exel/clinic',array()); ?>
    <div class="hidden-block" style="display: none"></div>
    <form action="<?php echo Yii::app()->createUrl('/mbLaw/'.Yii::app()->controller->action->id,array('status'=>(!empty($status)?$status:''),'host'=>(!empty($host)?$host:''))); ?>" method="POST" class="hidden-form">
        <div class="summary" id="summary">
            <?php $this->renderPartial('//layouts/_admin_count',array('dataCount'=>$dataCount,'count'=>$count,'dataPrice'=>$dataPrice)); ?>
        </div>
        <div class="table-api-main_wrapper">
            <table class="table-api-main">
                <?php $this->renderPartial('//mbLaw/_filter',array('sort'=>$sort,'model'=>$model,'status'=>'1')); ?>
                <tbody id="admin-tbody">
                    <?php $this->renderPartial('//mbLaw/_admin',array('data'=>$data)); ?>
                </tbody>
            </table>
        </div>
        <div class="pagination row" id="pages">
            <?php if(!empty($pages)&&!empty($count)): ?>
                <?php $this->renderPartial('//mbLaw/_pages',array('sort'=>$sort,'pages'=>$pages,'count'=>$count)); ?>
            <?php endif; ?>
        </div>
    </form>
</div>