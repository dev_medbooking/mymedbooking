<div class="order-container clearfix">
    <div class="top-order-box">
        <?php if(!empty($model->title)):?>Название клиники: <?php echo $model->title;?><?php endif;?>
    </div>
    <div class="order-boxes clearfix">
        <div class="order-left">
            <div class="name-box">
                <?php if(!empty($model->alias)):?>Алиас: <?php echo $model->alias;?><?php endif;?>
            </div>
            <div class="name-box">
                <?php if(!empty($model->title_company)):?>Юр.Лицо: <?php echo $model->title_company;?><?php endif;?>
            </div>
            <div class="name-box">
                <?php if(!empty($model->external_id)):?>Внешний ID: <?php echo $model->external_id;?><?php endif;?>
            </div>
            <div class="phone-box">
                <p class="heed-mini"><?php echo !empty($model->telephone1)?$model->telephone1:''; ?></p>
                <p class="heed-mini"><?php echo !empty($model->telephone2)?$model->telephone2:''; ?></p>
                <p class="heed-mini"><?php echo !empty($model->telephone3)?$model->telephone3:''; ?></p>
                <p class="heed-mini"><?php echo !empty($model->telephone4)?$model->telephone4:''; ?></p>
                <span><?php echo !empty($model->email)?$model->email:''; ?></span>
            </div>
            <div class="order-description">
                <div class="order-description-top">
                    <span class="record-client"><?php echo !empty($model->comment)?$model->comment:''; ?></span>
                </div><div class="order-description-top">
                    <span class="record-client"> Время создания: <?php echo !empty($model->create_time)?$model->create_time:''; ?></span>
                    <span class="record-client"> Время редактирования: <?php echo !empty($model->update_time)?$model->update_time:''; ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="line"><span><a href="<?php echo Yii::app()->createUrl('mbLaw/update',array('id'=>$model->id)); ?>">Редактировать клинику</a></span></div>
    <a href="<?php echo Yii::app()->createUrl('mbLaw/clinic');?>" style="float:right;">К таблице</a>
                <a href="<?php echo Yii::app()->createUrl('mbLaw/price',array('id'=>$model->id));?>" style="float:right;">К таблице заказов</a>
</div>