<nav class="nav submenu">
    <ul class="nav-menu clearfix">
        <li class="nav-applications">
            <a href="<?php echo Yii::app()->createUrl('mbLaw/createAccount')?>" style="color:red;">Добавить</a>
        </li>
        <li class="nav-applications">
            <a href="<?php echo Yii::app()->createUrl('mbLaw/accounter')?>">Задачи</a>
        </li>
    </ul>
</nav>
<div class="order-container clearfix">
    <div class="top-order-box">
        <?php echo !empty($model->title)?$model->title:'';?>
    </div>
    <div class="order-boxes clearfix">
        <div class="order-left">
            <div class="name-box">
                <?php echo !empty($model->url)?$model->url:'';?>
            </div>
            <div class="name-box">
                <?php echo !empty($model->companyname)?$model->companyname:'';?>
            </div>
            <div class="name-box">
                <?php echo !empty($model->clinic_name)?$model->clinic_name:'';?>
            </div>
            <div class="name-box">
                <?php echo !empty($model->create_time)?$model->create_time:'';?>
            </div>
            <div class="name-box">
                <?php echo !empty($model->close_time)?$model->close_time:'';?>
            </div>
            <div class="name-box">
                <?php echo !empty($model->status)?$model->status:'';?>
            </div>
            <div class="name-box">
                <?php echo !empty($model->doctor_count)?$model->doctor_count:'';?>
            </div>
            <div class="order-description">
                <div class="order-description-top">
                    <span class="record-client"><?php echo !empty($model->description)?$model->description:''; ?></span>
                </div>
            </div>
            <div class="order-description">
                <div class="order-description-top">
                    <span class="record-client"><?php echo !empty($model->comment_account)?$model->comment_account:''; ?></span>
                </div>
            </div>
            <div class="order-description">
                <div class="order-description-top">
                    <span class="record-client"><?php echo !empty($model->comment_content)?$model->comment_content:''; ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="line">
        <span>
            <a href="<?php echo Yii::app()->createUrl('mbLaw/updateAccount',array('id'=>$model->id)); ?>">Редактировать</a>
        </span>
    </div>
</div>