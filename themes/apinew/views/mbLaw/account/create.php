<nav class="nav submenu">
    <ul class="nav-menu clearfix">
        <li class="nav-applications">
            <a href="<?php echo Yii::app()->createUrl('mbLaw/createAccount')?>" style="color:red;">Добавить</a>
        </li>
        <li class="nav-applications">
            <a href="<?php echo Yii::app()->createUrl('mbLaw/accounter')?>">Задачи</a>
        </li>
    </ul>
</nav>
<div class="application-content _application-content">
    <?php if(!empty($model->id)): ?><p><strong><?php echo $model->title; ?></strong></p><?php else:?><p><strong>Создание нового Юр.лица</strong></p><?php endif;?>
</div>
<div id="create-block" class="_create_block">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'application-form'),
    )); ?>
        <?php if($model->errors): ?><div class="well"><?php echo $form->errorSummary($model); ?></div><?php endif; ?>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'title',array('placeholder'=>'Название','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'clinic_name',array('placeholder'=>'Фактическое','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'companyname',array('placeholder'=>'Юридическое название')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'url',array('placeholder'=>'URL')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'doctor_count',array('placeholder'=>'Количество врачей')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'status',array('placeholder'=>'Статус')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'clinic_id',array('placeholder'=>'ID системы','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <span class="title-calendar">Комментарии контента</span>
            <?php echo $form->textArea($model,'comment_content',array()); ?>
        </div>
        <div class="row clearfix">
            <span class="title-calendar">Комментарии аккаунтера</span>
            <?php echo $form->textArea($model,'comment_account',array()); ?>
        </div>
        <div class="row clearfix">
            <span class="title-calendar">Описание</span>
            <?php echo $form->textArea($model,'description',array()); ?>
        </div>
        <div class="row clearfix">
            <button class="_save_btn">Сохранить</button>
            <div class="row row_link">
                <?php if(!empty($model->id)):?><a href="<?php echo Yii::app()->createUrl('mbLaw/viewAccount',array('id'=>$model->id));?>" style="float:right;">Просмотреть</a><?php endif;?>
            </div>
        </div>
    <?php $this->endWidget(); ?>
</div>
