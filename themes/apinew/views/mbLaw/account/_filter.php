<thead>
    <tr>
        <td class="id_2"># <?php echo CHtml::activeTextField($model,'id',array('class'=>'search-single','id'=>'filter_id')); ?></td>
        <td class="clinic_id_2">ID системы</td>
        <td class="title_2">
            <?php echo $sort->link('title',null,array('class'=>'csorting')); ?>
            <div><?php echo CHtml::activeTextField($model,'title',array('class'=>'search-single','id'=>'filter_title')); ?></div>
        </td>
        <td class="url_2">
            <?php echo $sort->link('url',null,array('class'=>'csorting')); ?>
            <div><?php echo CHtml::activeTextField($model,'url',array('class'=>'search-single','id'=>'filter_url')); ?></div>
        </td>
        <td class="companyname_2">
            <?php echo $sort->link('companyname',null,array('class'=>'csorting')); ?>
            <div><?php echo CHtml::activeTextField($model,'companyname',array('class'=>'search-single','id'=>'filter_companyname')); ?></div>
        </td>
        <td class="clinic_name_2">
            <?php echo $sort->link('clinic_name',null,array('class'=>'csorting')); ?>
            <div><?php echo CHtml::activeTextField($model,'clinic_name',array('class'=>'search-single','id'=>'filter_clinic_name')); ?></div>
        </td>
        <td class="create_time_2">
            <?php echo $sort->link('create_time',null,array('class'=>'csorting')); ?>
        </td>
        <td class="close_time_2">
            <?php echo $sort->link('close_time',null,array('class'=>'csorting')); ?>
        </td>
        <td class="status_2">
            <?php echo $sort->link('status',null,array('class'=>'csorting')); ?>
            <div><?php echo CHtml::activeTextField($model,'status',array('class'=>'search-single','id'=>'filter_status')); ?></div>
        </td>
        <td class="edit"></td>
        <td class="view"></td>
    </tr>
</thead>