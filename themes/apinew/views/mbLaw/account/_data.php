<tr class="api-item-tr">
    <td><?php echo $model->id; ?></td>
    <td><?php echo !empty($model->clinic_id)?$model->clinic_id:''; ?></td>
    <td><?php echo !empty($model->title)?$model->title:''; ?></td>
    <td><?php echo !empty($model->url)?$model->url:''; ?></td>
    <td><?php echo !empty($model->companyname)?$model->companyname:''; ?></td>
    <td><?php echo !empty($model->clinic_name)?$model->clinic_name:''; ?></td>
    <td><?php echo !empty($model->create_time)?$model->create_time:''; ?></td>
    <td><?php echo !empty($model->close_time)?$model->close_time:''; ?></td>
    <td><?php echo !empty($model->status)?$model->status:''; ?></td>
    <td class="ico-edit"><?php echo CHtml::link('',array('mbLaw/updateAccount','id'=>$model->id)); ?></td>
    <td class="ico-view"><?php echo CHtml::link('',array('mbLaw/viewAccount','id'=>$model->id)); ?></td>
</tr>