<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/table2.js',CClientScript::POS_END);?>
<nav class="nav submenu">
    <ul class="nav-menu clearfix">
        <li class="nav-applications">
            <a href="<?php echo Yii::app()->createUrl('mbLaw/createAccount')?>" style="color:red;">Добавить</a>
        </li>
        <li class="nav-applications">
            <a href="<?php echo Yii::app()->createUrl('mbLaw/accounter')?>">Задачи</a>
        </li>
    </ul>
</nav>
<form action="<?php echo Yii::app()->createUrl('/mbLaw/'.Yii::app()->controller->action->id); ?>" method="POST" class="hidden-form">
    <input type="hidden" name="report" value="law" />
    <div class="summary" id="summary">
        <?php $this->renderPartial('//mbLaw/account/_count',array('count'=>$count)); ?>
    </div>
    <table class="table-api-main">
        <?php $this->renderPartial('//mbLaw/account/_filter',array('sort'=>$sort,'model'=>$model)); ?>
        <tbody id="admin-tbody">
            <?php $this->renderPartial('//mbLaw/account/_admin',array('data'=>$data)); ?>
        </tbody>
    </table>
    <div class="pagination row" id="pages">
        <?php if(!empty($pages)&&!empty($count)): ?><?php $this->renderPartial('//mbLaw/account/_pages',array('sort'=>$sort,'pages'=>$pages,'count'=>$count)); ?><?php endif; ?>
    </div>
</form>