<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
<script type="text/javascript">$(function(){$("#phone").mask("+7(999)999-9999",{'placeholder':'_'});});$(function(){$(".chosen-default>span").text("Выбрать")});</script>
<div class="application-content _application-content">
    <?php if(!empty($model->id)): ?><p><strong><?php echo $model->title; ?></strong></p><?php else:?><p><strong>Создание нового Юр.лица</strong></p><?php endif;?>
</div>
<div id="create-block" class="_create_block">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'application-form'),
    )); ?>
        <?php if($model->errors): ?><div class="well"><?php echo $form->errorSummary($model); ?></div><?php endif; ?>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'title',array('placeholder'=>'название','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'numb_agree',array('placeholder'=>'Номер договора')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'add_agree',array('placeholder'=>'Доп соглашение')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'po',array('placeholder'=>'ПО')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'subway',array('placeholder'=>'Метро')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'address',array('placeholder'=>'Адрес','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'contact',array('placeholder'=>'Контакты')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name">
                <?php echo $form->textField($model,'alias',array('placeholder'=>'Алиас')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="select-new select-new-last">
                <?php echo $form->textField($model,'external_id',array('placeholder'=>'Внешний ID')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-phone note-form clearfix">
                <label for="phone"></label>
                <?php $this->widget('CMaskedTextField',array('model'=>$model,'attribute'=>'telephone1','mask'=>'+7(999)999-9999','placeholder'=>'_','htmlOptions'=>array('class'=>'span2 request','id'=>'phone','placeholder'=>'Телефон','data-href'=>Yii::app()->createUrl('mbExel/phoneCheked')))); ?>
            </div>
            <div class="text-phone clearfix">
                <label for="phone2"></label>
                <?php $this->widget('CMaskedTextField',array('model'=>$model,'attribute'=>'telephone2','mask'=>'+7(999)999-9999','placeholder'=>'_','htmlOptions'=>array('class'=>'span2','id'=>'phone2','placeholder'=>'Телефон','data-href'=>Yii::app()->createUrl('mbExel/phoneCheked')))); ?>
            </div>
            <div class="text-phone  clearfix">
                <label for="phone3"></label>
                <?php $this->widget('CMaskedTextField',array('model'=>$model,'attribute'=>'telephone3','mask'=>'+7(999)999-9999','placeholder'=>'_','htmlOptions'=>array('class'=>'span2','id'=>'phone3','placeholder'=>'Телефон','data-href'=>Yii::app()->createUrl('mbExel/phoneCheked')))); ?>
            </div>
            <div class="text-phone clearfix">
                <label for="phone4"></label>
                <?php $this->widget('CMaskedTextField',array('model'=>$model,'attribute'=>'telephone4','mask'=>'+7(999)999-9999','placeholder'=>'_','htmlOptions'=>array('class'=>'span2','id'=>'phone4','placeholder'=>'Телефон','data-href'=>Yii::app()->createUrl('mbExel/phoneCheked')))); ?>
            </div>
            <div class="text-email clearfix">
                <label for="email"></label>
                <?php echo $form->textField($model,'email',array('placeholder'=>'E-mail',"id"=>"email")); ?>
            </div>
            <div class="text-uid text-pencil clearfix">
                <label for="uid"></label>
                <?php echo $form->textField($model,'uid',array('placeholder'=>'№ оператора',"id"=>"uid")); ?>
            </div>
            <div class="text-uid text-pencil clearfix">
                <label for="agreement"></label>
                <?php echo $form->textField($model,'agreement',array('placeholder'=>'Договор',"id"=>"agreement")); ?>
            </div>
            <div class="text-uid text-pencil clearfix">
                <label for="k1"></label>
                <?php echo $form->textField($model,'k1',array('placeholder'=>'K1',"id"=>"k1")); ?>
            </div>
            <div class="text-uid text-pencil clearfix">
                <label for="k2"></label>
                <?php echo $form->textField($model,'k2',array('placeholder'=>'K2',"id"=>"k2")); ?>
            </div>
            <div class="text-uid text-pencil clearfix">
                <label for="predoplata"></label>
                <?php echo $form->textField($model,'predoplata',array('placeholder'=>'Предоплата',"id"=>"predoplata")); ?>
            </div>
            <div class="text-uid text-pencil clearfix">
                <label for="status_site"></label>
                <?php echo $form->textField($model,'status_site',array('status_site'=>'Статус',"id"=>"status_site")); ?>
            </div>
            <div class="text-uid text-pencil clearfix">
                <label for="cont_person"></label>
                <?php echo $form->textField($model,'cont_person',array('placeholder'=>'Контактный',"id"=>"cont_person")); ?>
            </div>
            <div class="text-uid text-pencil clearfix">
                <label for="price_category"></label>
                <?php echo $form->textField($model,'price_category',array('placeholder'=>'Ценовой сегмент',"id"=>"price_category")); ?>
            </div>
            <div class="text-uid text-dollar clearfix">
                <label for="price_value"></label>
                <?php echo $form->textField($model,'price_value',array('placeholder'=>'Стоматология',"id"=>"price_value")); ?>
            </div>
            <div class="text-uid text-dollar clearfix">
                <label for="com_med"></label>
                <?php echo $form->textField($model,'com_med',array('placeholder'=>'Общая',"id"=>"com_med")); ?>
            </div>
            <div class="text-uid text-dollar clearfix">
                <label for="com_diagn"></label>
                <?php echo $form->textField($model,'com_diagn',array('placeholder'=>'Диагностика',"id"=>"com_diagn")); ?>
            </div>
            <div class="text-uid text-dollar clearfix">
                <label for="com_cosmo"></label>
                <?php echo $form->textField($model,'com_cosmo',array('placeholder'=>'Косметология',"id"=>"com_cosmo")); ?>
            </div>
            <div class="text-uid text-dollar clearfix">
                <label for="percent"></label>
                <?php echo $form->textField($model,'percent',array('placeholder'=>'Стоматология %',"id"=>"percent")); ?>
            </div>
            <div class="text-uid text-dollar clearfix">
                <label for="percent_med"></label>
                <?php echo $form->textField($model,'percent_med',array('placeholder'=>'Общая %',"id"=>"percent_med")); ?>
            </div>
            <div class="text-uid text-dollar clearfix">
                <label for="percent_diagn"></label>
                <?php echo $form->textField($model,'percent_diagn',array('placeholder'=>'Диагностика %',"id"=>"percent_diagn")); ?>
            </div>
            <div class="text-uid text-dollar clearfix">
                <label for="percent_cosmo"></label>
                <?php echo $form->textField($model,'percent_cosmo',array('placeholder'=>'Косметология %',"id"=>"percent_cosmo")); ?>
            </div>
        </div>
        <div class="row clearfix">
           <div class="text-phone row-lb note-form clearfix">
            <span class="title-calendar">Юр.лицо</span>
            <?php echo $form->textField($model,'title_company',array('id'=>'clinic_name_auto',"class"=>"clinic-text request")); ?>
            </div>
        </div>
        <div class="row clearfix">
           <div class="text-phone row-lb clearfix">
            <span class="title-calendar">Юр.лицо</span>
            <?php echo $form->dropDownList($model,'all_id',array(''=>'Укажите сеть')+CHtml::listData(IntegratorClinicAll::model()->findAll(),'id','title')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name">
                <span class="title-calendar">MB ID</span>
                <?php echo $form->textField($model,'medbooking_id',array('placeholder'=>'MB ID','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name">
                <span class="title-calendar">Diagnostica ID</span>
                <?php echo $form->textField($model,'diagnostica_id',array('placeholder'=>'Diagnostica ID','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name">
                <span class="title-calendar">TP ID</span>
                <?php echo $form->textField($model,'testpuls_id',array('placeholder'=>'TP ID','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name">
                <span class="title-calendar">T2V ID</span>
                <?php echo $form->textField($model,'timetovisit_id',array('placeholder'=>'T2V ID','class'=>'request')); ?>
            </div>
        </div>     
        <div class="row clearfix">
            <div class="text-name">
                <span class="title-calendar">Fromed ID</span>
                <?php echo $form->textField($model,'fromed_id',array('placeholder'=>'Fromed ID','class'=>'request')); ?>
            </div>
        </div>         
        <div class="row clearfix">
            <span class="title-calendar">Комментарии</span>
            <?php echo $form->textArea($model,'comment',array()); ?>
        </div>
        <div class="row clearfix">
            <button class="_save_btn">Сохранить</button>
            <div class="row row_link">
                <?php if(!empty($model->id)):?><a href="<?php echo Yii::app()->createUrl('mbLaw/view',array('id'=>$model->id));?>" style="float:right;">Просмотреть</a><?php endif;?>
                <a href="<?php echo Yii::app()->createUrl('mbLaw/clinic');?>" style="float:right;">К таблице</a>
                <?php if(!empty($model->id)):?><a href="<?php echo Yii::app()->createUrl('mbLaw/price',array('id'=>$model->id));?>" style="float:right;">К таблице заказов</a><?php endif;?>
            </div>
        </div>
    <?php $this->endWidget(); ?>
</div>
