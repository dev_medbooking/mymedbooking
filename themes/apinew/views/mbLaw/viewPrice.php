<div class="order-container clearfix">
   
    <div class="order-boxes clearfix">
        <div class="order-left full_block">
           <?php if(!empty($model->id)): ?>
               <div class="title">Cчет № <?php echo $model->id; ?></div>
           <?php endif;?>
            <div class="table_row">
                <?php if(!empty($model->koll_come)):?>
                <div class="table_line">
                    Количевство  пришедших: <?php echo $model->koll_come;?>
                </div>
                <?php endif;?>
                <?php if(!empty($model->koll_success)):?>
                <div class="table_line">
                    Количевство  согласованых: <?php echo $model->koll_success;?>
                </div>
                <?php endif;?>
                <?php if(!empty($model->price)):?>
                <div class="table_line">
                    Стоимость: <?php echo $model->price;?>
                </div>
                <?php endif;?>
                <?php if(!empty($model->all_price)):?>
                <div class="table_line">
                    Общаяя сумма: <?php echo $model->all_price;?>
                </div>
                <?php endif;?>
                <?php if(!empty($model->status)):?>
                <div class="table_line">
                    Стоимость: <?php echo $model->statusText;?>
                </div>
                <?php endif;?>
            </div>
            <div class="order-description">
                <div class="order-description-top">
                    <span class="record-client"> Коментарии : <?php echo !empty($model->comment)?$model->comment:'нет комментария'; ?></span>
                </div>
                <div class="order-description-top">
                    <span class="record-client"> Время создания: <?php echo !empty($model->date)?$model->date:''; ?></span>
                </div>
            </div>
            <div class="table_link">
                <a href="<?php echo Yii::app()->createUrl('mbLaw/price',array('id'=>$model->integrator_id));?>" style="float:right;">К таблице</a>
            </div>
        </div>
    </div>
    <?php if(!empty($model->id)):?>
        <div class="line"><span><a href="<?php echo Yii::app()->createUrl('mbLaw/updatePrice',array('id'=>$model->id)); ?>">Редактировать заказ</a></span></div>
    <?php endif;?>
</div>