<div class="row line clearfix">
    <div class="cell number"><?php echo $model->id;?></div>
    <div class="cell legalName"><?php echo!empty($model->domain)?$model->domain:'';?></div>
    <div class="cell actualName"><?php echo!empty($model->phone)?$model->phone:'';?></div>
    <div class="cell actualName"><?php echo!empty($model->description)?$model->description:'';?></div>
    <div class="cell update"><a href="<?php echo Yii::app()->createUrl('domainPhone/update',array('id'=>$model->id));?>" class="iconupdate"></a></div>
    <div class="cell deleted"><a href="<?php echo Yii::app()->createUrl('domainPhone/delete',array('id'=>$model->id));?>" class="row_deleted iconclear"></a></div>
</div>