<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
<script type="text/javascript">$(function(){$("#MbDomainPhone_phone").mask("+7(999)999-9999",{'placeholder':'_'});});$(function(){$(".chosen-default>span").text("Выбрать")});</script>
<div class="application-content _application-content business-top-form">
    <?php if(!empty($model->id)): ?><p><strong><?php echo $model->domain?></strong></p><?php else:?><p><strong>Создание нового телефона для домена</strong></p><?php endif;?>
</div>
<div id="create-block" class="_create_block business-form-wrapper">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'application-form','enctype'=>'multipart/form-data'),
    )); ?>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'domain',array('class'=>'control-label')); ?>
            <div class="text-name note-form">
                <?php echo $form->textField($model,'domain',array('placeholder'=>'','class'=>'request')); ?>
                <?php echo $form->error($model, 'domain'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'phone',array('class'=>'control-label')); ?>
            <div class="text-name note-form">
                <?php echo $form->textField($model,'phone',array('placeholder'=>'')); ?>
                <?php echo $form->error($model, 'phone'); ?>
            </div>
        </div>
    <div class="row clearfix">
            <?php echo $form->labelEx($model,'description',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->textArea($model,'description',array('placeholder'=>'')); ?>
                <?php echo $form->error($model, 'description'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <button class="">Сохранить</button>
        </div>
    <?php $this->endWidget(); ?>
</div>