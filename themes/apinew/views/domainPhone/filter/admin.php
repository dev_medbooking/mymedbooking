<div class="row caption clearfix">
    <div class="cell number"><?php echo $sort->link('id',null,array('class'=>'csorting')); ?></div>
    <div class="cell legalName"><?php echo $sort->link('domian',null,array('class'=>'csorting')); ?> -
        <div class="summary" id="summary">
            <?php $this->renderPartial('//layouts/_admin_count', array('count' => $count)); ?>
        </div>
    </div>
    <div class="cell actualName"><?php echo $sort->link('phone',null,array('class'=>'csorting')); ?></div>
    <div class="cell actualName"><?php echo $sort->link('description',null,array('class'=>'csorting')); ?></div>
    <div class="cell update"></div>
    <div class="cell deleted"></div>
</div>
<form name="hidden-form" class="hidden-form">
    <div class="row sort clearfix search_filter">
        <div class="cell number"></div>
        <div class="cell legalName"><?php echo CHtml::activeTextField($model,'domain',array()); ?></div>
        <div class="cell actualName"><?php echo CHtml::activeTextField($model,'phone',array()); ?></div>
        <div class="cell actualName"><?php echo CHtml::activeTextField($model,'description',array()); ?></div>
        <div class="cell update"></div>
        <div class="cell deleted"></div>
    </div>
</form>