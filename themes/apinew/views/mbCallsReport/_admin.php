<div class="grid-view" id="admin-admin-request">
	<div class="summary" id="summary">
		<?php //$this->renderPartial('//layouts/_admin_count',array('count'=>$count)); ?>
	</div>
    <?php
    echo $form = CHtml::beginForm(CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId())),'get');
    $this->widget('zii.widgets.jui.CJuiDatePicker',array(
        'name'=>'dateStart',
        'value'=>!empty($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : date('Y-m-d'),
        // additional javascript options for the date picker plugin
        'options'=>array(
            'showAnim'=>'fold',
            'dateFormat' => 'yy-mm-dd'
        ),
        'htmlOptions'=>array(
            'style'=>'height:20px;',

        ),
    ));
    $this->widget('zii.widgets.jui.CJuiDatePicker',array(
        'name'=>'dateEnd',
        'value'=>!empty($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : date('Y-m-d'),
        // additional javascript options for the date picker plugin
        'options'=>array(
            'showAnim'=>'fold',
            'dateFormat' => 'yy-mm-dd'
        ),
        'htmlOptions'=>array(
            'style'=>'height:20px;',
            'value'=>date('Y-m-d'),
        ),
    ));
    echo CHtml::dropDownList('request', !empty($_REQUEST['request']) ? $_REQUEST['request'] : 0, array(''=>'Звонки','1'=>'Заявки'));
    echo CHtml::submitButton();
    echo CHtml::link('Скачать', ['download',
        'dateFrom' => !empty($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : date('Y-m-d'),
        'dateTo'   => !empty($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : date('Y-m-d'),
        'request'   => !empty($_REQUEST['request']) ? $_REQUEST['request'] : "",
    ]);
    /*echo "&nbsp;&nbsp;&nbsp;&nbsp;".CHtml::link('Скачать Заявки', ['download',
        'dateFrom' => !empty($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : date('Y-m-d'),
        'dateTo'   => !empty($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : date('Y-m-d'),
        'request'   => 1,
    ]);*/
    echo CHtml::endForm();
    ?>
	<div class="hidden-block" style="display: none"></div>
	<table class="table table-striped table-bordered table-hover table-api-main">
		<?php //$this->renderPartial('_filter', array('sort' => $sort, 'model' => $model, 'status' => '1')); ?>
		<tbody id="admin-tbody">
		<?php $this->renderPartial('admin', array('data' => $data, 'apicalltouch' => $apicalltouch)); ?>
		</tbody>
	</table>
</div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/callsReports.js"></script>