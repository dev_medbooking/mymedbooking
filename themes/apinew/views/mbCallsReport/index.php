<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'admin-form',
	'action' => Yii::app()->createUrl('mbCallsReport/uploadFile'),
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'
	),
));
?>
<?=$form->fileField($import, 'file', array('id' => "id-input-file-2")); ?>
<?=$form->error($import, 'file'); ?>
<?=CHtml::submitButton("Загрузить", array()); ?>
<?php $this->endWidget(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'admin-form',
	'action' => Yii::app()->createUrl('mbCallsReport/table'),
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'
	),
));
?>
<?=$form->fileField($import, 'file', array('id' => "id-input-file-2")); ?>
<?=$form->error($import, 'file'); ?>
<?=CHtml::submitButton("Загрузить", array()); ?>
<?php $this->endWidget(); ?>


<div class="row-fluid row-call">
	<?php $this->renderPartial('_admin', array('model' => $model, 'sort' => $sort, 'count' => $count, 'data' => $data , 'apicalltouch' => $apicalltouch)); ?>
</div>