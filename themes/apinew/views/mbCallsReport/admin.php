<?php if (empty($apicalltouch)):?>
	<?php foreach ($data as $key => $model) :?>
			<tr>
				<td>
					<table style="width: 100%;">
						<?if($key == 0):?>
							<tr>
								<th>Группа</th>
								<th>Тип источника</th>
								<th>Звонки</th>
								<th>Записи</th>
								<th>Дошедшие</th>
							</tr>
						<?endif?>
							<tr>
								<td style="width: 20%;">
									<a class="js-show-source-report" data-start="<?= isset($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : ''?>" data-end="<?= isset($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : ''?>" data-request="" data-source="<?=( ! empty($model->utmSource) ? $model->utmSource : (!empty($model->source) ? $model->source :''));?>" data-medium="<?=( ! empty($model->medium) ? $model->medium : '');?>" data-ref="<?= CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId())) ?>"><?=( !empty($model->utmSource) ? $model->utmSource : (!empty($model->source) ? $model->source: 'не указан'));?></a>
								</td>
								<td style="width: 20%;">
									<?= $model->medium; ?>
								</td>
								<td style="width: 20%;">
									<?= $count = $model->utmSourceCount;?>
								</td>
								<td style="width: 20%;">
									<?= $model->recordsCount . ' ' . '(' . round($model->recordsCount*100/$count, 2) . '% )' ?>
								</td>
								<td style="width: 20%;">
									<?=  $model->recordsDCount . ' ' . '(' . round($model->recordsDCount*100/$count, 2) . '% )' ?>
								</td>
							</tr>
							<tr class="js-source-report-table-<?=( ! empty($model->utmSource) ? $model->utmSource : (!empty($model->source) ? $model->source : ''));?>" data-request="" data-medium="<?=( ! empty($model->medium) ? $model->medium : '');?>">

							</tr>
					</table>
				</td>
			</tr>
	<?php endforeach;?>
<? else :?>
	<?if(count($apicalltouch)> 0):?>
	<tr>
	<td>
	<table style="width: 100%;">
	<tr>
	<th>Группа</th>
	<th>Тип источника</th>
	<th>Заявки</th>
	<th>Записи</th>
	<th>Дошедшие</th>
	</tr>
	<?php foreach ($apicalltouch as $group => $val_array) :?>
		<?php foreach ($val_array as $key => $val) :?>
			<tr>
				<td style="width: 20%;">
					<a class="js-show-source-report" data-start="<?= isset($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : ''?>" data-end="<?= isset($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : ''?>" data-request="1" data-source="<?=( ! empty($group) ? $group : (!empty($group) ? $group :''));?>" data-medium="<?=( ! empty($key) ? $key : '');?>" data-ref="<?= CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId())) ?>"><?=( !empty($group) ? $group : (!empty($group) ? $group: 'не указан'));?></a>
				</td>
				<td style="width: 20%;">
					<?=$key;?>
				</td>
				<td style="width: 20%;">
					<?=$val['count']?>
				</td>
				<td style="width: 20%;">
					<?= $val['written'] . ' ' . '(' . round($val['written']*100/$val['count'], 2) . '% )' ?>
				</td>
				<td style="width: 20%;">
					<?=  $val['coming'] . ' ' . '(' . round($val['coming']*100/$val['count'], 2) . '% )' ?>
				</td>
			</tr>
			<tr class="js-source-report-table-<?=( ! empty($group) ? $group : (!empty($group) ? $group : ''));?>" data-medium="<?=( ! empty($key) ? $key : '');?>" data-request="1">

			</tr>
		<?php endforeach;?>
	<?php endforeach;?>
	</table>
	</td>
	</tr>
	<?endif?>
<?endif?>