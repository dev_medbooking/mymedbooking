<?php if (empty($apicalltouch)):?>
    <td colspan="5">
        <table style="width: 100%;text-align: left">
            <?php foreach($data as $k => $v) :?>
                <tr>
                    <td style="width: 20%;">
                        <? if (empty($v->utmSource) && !empty($v->source)) :?>
                            <div  data-start="<?= isset($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : ''?>" data-end="<?= isset($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : ''?>" data-request="" data-source="<?= !empty($v->utmSource) ? $v->utmSource : ''?>" data-campaign="<?= !empty($v->utmCampaign) ? $v->utmCampaign : '';?>" data-medium="<?= !empty($v->medium) ? $v->medium : '';?>"><?= !empty($v->utmCampaign) ? $v->utmCampaign : (!empty($v->utmSource) ? $v->utmSource : $v->source) ;?></div>
                        <? else :?>
                            <a class="js-show-campaign-report"  data-start="<?= isset($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : ''?>" data-end="<?= isset($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : ''?>" data-request="" data-source="<?= !empty($v->utmSource) ? $v->utmSource : ''?>" data-campaign="<?= !empty($v->utmCampaign) ? $v->utmCampaign : '';?>" data-ref="<?= CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId())) ?>"><?= !empty($v->utmCampaign) ? $v->utmCampaign : (!empty($v->utmSource) ? $v->utmSource : $v->source) ;?></a>
                        <? endif ?>
                    </td>
                    <td> <?= $v->medium?> </td>
                    <td style="width: 20%;"><?= $count = !empty( $v->utmCampaignCount) ? $v->utmCampaignCount : $v->utmSourceCount;?></td>
                    <td style="width: 20%;"><?= $v->recordsCount . ' ' . '(' . round($v->recordsCount*100/$count, 2) . '% )' ?></td>
                    <td style="width: 20%;"><?=  $v->recordsDCount . ' ' . '(' . round($v->recordsDCount*100/$count, 2) . '% )' ?></td>
                </tr>
                <tr data-medium="<?= !empty($v->medium) ? $v->medium : ''?>" data-table-name="js-campaign-report-table-<?=( !empty($v->utmCampaign) ? $v->utmCampaign : $v->utmSource);?>">

                </tr>
            <?php endforeach;?>
        </table>
    </td>
<? else :?>
    <td colspan="5">
        <table style="width: 100%;text-align: left">
            <?php foreach($apicalltouch as $source => $val_array) :?>
                <?php foreach ($val_array as $medium => $val_array1) :?>
                    <? if (!empty($val_array1['utmCampaign'])):?>
                        <?php foreach ($val_array1['utmCampaign'] as $campaign => $v) :?>
                            <?php if (!empty($v['count'])):?>
                                <tr>
                                    <td style="width: 20%;">
                                        <?// if (empty($utmSource) && !empty($source)) :?>
                                            <!--<div  data-start="<?= isset($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : ''?>" data-end="<?= isset($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : ''?>" data-request="1" data-source="<?= !empty($utmSource) ? $utmSource : ''?>" data-campaign="<?= !empty($v->utmCampaign) ? $v->utmCampaign : '';?>" data-medium="<?= !empty($v->medium) ? $v->medium : '';?>"><?= !empty($campaign) ? $campaign : (!empty($utmSource) ? $utmSource : $source) ;?></div>-->
                                        <?// else :?>
                                            <a class="js-show-campaign-report"  data-start="<?= isset($_REQUEST['dateStart']) ? $_REQUEST['dateStart'] : ''?>" data-end="<?= isset($_REQUEST['dateEnd']) ? $_REQUEST['dateEnd'] : ''?>" data-request="1" data-source="<?= $source?>" data-campaign="<?= $campaign;?>" data-ref="<?= CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId())) ?>"><?= !empty($utmCampaign) ? $utmCampaign : (!empty($utmSource) ? $utmSource : $campaign) ;?></a>
                                        <?// endif ?>
                                    </td>
                                    <td> <?= $medium?></td>
                                    <td style="width: 20%;"><?= $count=!empty( $v['count']) ? $v['count'] : 0;?></td>
                                    <td style="width: 20%;"><?= !empty( $v['written'])?$v['written'] . ' ' . '(' . round($v['written']*100/$count, 2) . '% )':"0 0% " ?></td>
                                    <td style="width: 20%;"><?= !empty( $v['coming'])?$v['coming'] . ' ' . '(' . round($v['coming']*100/$count, 2) . '% )':"0 0% " ?></td>
                                </tr>
                                <tr data-mediumt="<?= !empty($medium) ? $medium : ''?>" data-table-name="js-campaign-report-table-<?=$campaign;?>" data-request="1">

                                </tr>
                             <?php endif ?>
                        <?php endforeach;?>
                    <?php endif ?>
                <?php endforeach;?>
            <?php endforeach;?>
        </table>
    </td>
<?endif?>