<section id="content">
    <div class="center">
        <div class="table" data-href="<?php echo Yii::app()->createUrl('partner/admin', array('t'=>1)); ?>">
            <div class="filter-table"><?php $this->renderPartial('//partner/_filter', array('sort' => $sort, 'model' => $model,'count'=>$count)); ?></div>
            <div class="body-table"><?php  $this->renderPartial('_admin', array('data' => $data)); ?></div>
        </div>
    </div>
    <div class="pagination row" id="pages">
        <?php if (!empty($pages) && !empty($count)): ?>
            <?php $this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count)); ?>
        <?php endif; ?>
    </div>
</section>
