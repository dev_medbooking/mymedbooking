<div class="row caption clearfix">
    <div class="cell number"><?php echo $sort->link('id',null,array('class'=>'csorting')); ?></div>
    <div class="cell legalName"><?php echo $sort->link('partner_id',null,array('class'=>'csorting')); ?> -
        <div class="summary" id="summary">
            <?php $this->renderPartial('//layouts/_admin_count', array('count' => $count)); ?>
        </div>
    </div>
    <div class="cell actualName"><?php echo $sort->link('price',null,array('class'=>'csorting')); ?></div>
    <div class="cell actualName"><?php echo $sort->link('status',null,array('class'=>'csorting')); ?></div>
    <div class="cell actualName"><?php echo $sort->link('note',null,array('class'=>'csorting')); ?></div>
    <div class="cell update"></div>
    <div class="cell deleted"></div>
</div>
<form name="hidden-form" class="hidden-form">
    <div class="row sort clearfix search_filter">
        <div class="cell number"></div>
        <div class="cell actualName"><?php echo CHtml::activeTextField($model,'partner_id',array()); ?></div>
        <div class="cell actualName"><?php echo CHtml::activeTextField($model,'price',array()); ?></div>
        <div class="cell actualName"><?php echo CHtml::activeTextField($model,'status',array()); ?></div>
        <div class="cell actualName"><?php echo CHtml::activeTextField($model,'note',array()); ?></div>
        <div class="cell update"></div>
    <div class="cell deleted"></div>
    </div>
</form>