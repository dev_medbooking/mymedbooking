<div class="row caption clearfix">
    <div class="cell number"><?php echo $sort->link('id',null,array('class'=>'csorting')); ?></div>
    <div class="cell legalName"><?php echo $sort->link('title',null,array('class'=>'csorting')); ?> -
        <div class="summary" id="summary">
            <?php $this->renderPartial('//layouts/_admin_count', array('count' => $count)); ?>
        </div>
    </div>
    <div class="cell actualName"><?php echo $sort->link('name',null,array('class'=>'csorting')); ?></div>
    <div class="cell actualName"><?php echo $sort->link('phone',null,array('class'=>'csorting')); ?></div>
	<div class="cell actualName"><?php echo $sort->link('site',null,array('class'=>'csorting')); ?></div>
    <div class="cell recording"><?php echo $sort->link('ident_id',null,array('class'=>'csorting')); ?></div>
    <div class="cell surviving"><?php echo $sort->link('domain',null,array('class'=>'csorting')); ?></div>
    <div class="cell successing"><?php echo $sort->link('create_time',null,array('class'=>'csorting')); ?></div>
    <div class="cell update"></div>
    <div class="cell deleted"></div>
</div>
<form name="hidden-form" class="hidden-form">
    <div class="row sort clearfix search_filter">
        <div class="cell number"></div>
        <div class="cell legalName"><?php echo CHtml::activeTextField($model,'title',array()); ?></div>
        <div class="cell actualName"><?php echo CHtml::activeTextField($model,'name',array()); ?></div>
        <div class="cell actualName"><?php echo CHtml::activeTextField($model,'phone',array()); ?></div>
	    <div class="cell actualName"><?php echo CHtml::activeTextField($model,'site',array()); ?></div>
        <div class="cell recording"><?php echo CHtml::activeTextField($model,'ident_id',array()); ?></div>
        <div class="cell surviving"><?php echo CHtml::activeTextField($model,'domain',array()); ?></div>
        <div class="cell successing"><?php echo CHtml::activeTextField($model,'create_time',array()); ?></div>
        <div class="cell update"></div>
        <div class="cell deleted"></div>
    </div>
</form>