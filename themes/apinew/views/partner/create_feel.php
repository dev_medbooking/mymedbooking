<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
<script type="text/javascript">$(function(){$("#RegisterCommon_telephone").mask("+7(999)999-9999",{'placeholder':'_'});});$(function(){$(".chosen-default>span").text("Выбрать")});</script>
<div class="application-content _application-content business-top-form">
    <?php if(!empty($model->id)): ?><p><strong><?php echo $model->pfp->name; ?></strong></p><?php else:?><p><strong>Partner Feel</strong></p><?php endif;?>
</div>
<div id="create-block" class="_create_block business-form-wrapper">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'application-form','enctype'=>'multipart/form-data'),
    )); ?>
        
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'partner_id',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->dropDownList($model, 'partner_id',array(""=>"")+CHtml::listData(Partner::model()->findAll(),'id', 'name')); ?>
                <?php echo $form->error($model, 'partner_id'); ?>
            </div>
         </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'price',array('class'=>'control-label')); ?>
            <div class="text-name note-form">
                <?php echo $form->textField($model,'price',array('placeholder'=>'','class'=>'request')); ?>
                <?php echo $form->error($model, 'price'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'status',array('class'=>'control-label')); ?>
            <div class="text-name note-form">
                <?php echo $form->textField($model,'status',array('placeholder'=>'','class'=>'request')); ?>
                <?php echo $form->error($model, 'status'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'note',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->textArea($model,'note',array('placeholder'=>'')); ?>
                <?php echo $form->error($model, 'note'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <button class="">Сохранить</button>
        </div>
    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    function callCalendar2() {
            $('#RegisterCommon_data_agree').datetimepicker({
                lang: "ru",
                onSelect: function(dateText, inst) {
                    var miliseconds = 2628000000;
                    var currentTime = new Date();
                    var selectedDate = new Date(dateText);
                }
            });
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Текущее время',
            closeText: 'Закрыть',
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);
    }
    callCalendar2();
</script>