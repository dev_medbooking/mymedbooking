<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
<script type="text/javascript">$(function(){$("#Partner_phone").mask("+7(999)999-9999",{'placeholder':'_'});});$(function(){$(".chosen-default>span").text("Выбрать")});</script>
<div class="application-content _application-content business-top-form">
    <?php if(!empty($model->id)): ?><p><strong><?php echo $model->name; ?></strong></p><?php else:?><p><strong>Создание нового партнера</strong></p><?php endif;?>
</div>
<div id="create-block" class="_create_block business-form-wrapper">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'application-form','enctype'=>'multipart/form-data'),
    )); ?>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'title',array('class'=>'control-label')); ?>
            <div class="text-name note-form">
                <?php echo $form->textField($model,'title',array('placeholder'=>'','class'=>'request')); ?>
                <?php echo $form->error($model, 'title'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?>
            <div class="text-name note-form">
                <?php echo $form->textField($model,'name',array('placeholder'=>'')); ?>
                <?php echo $form->error($model, 'name'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'phone',array('class'=>'control-label')); ?>
            <div class="text-name note-form">
	            <?php echo $form->dropDownList($model, 'phone', array(""=>"")+CHtml::listData(MbDomainPhone::model()->findAll(array('condition' => 'description = \'не применен\''.( empty($model->ident_id) ? '' : ' OR partner_id = '.$model->ident_id ))), 'phone', 'phone')); ?>
                <?php echo $form->error($model, 'phone'); ?>
            </div>
        </div>
		<div class="row clearfix">
			<?php echo $form->labelEx($model,'price_all',array('class'=>'control-label')); ?>
			<div class="text-name note-form">
				<?php echo $form->textField($model,'price_all',array('placeholder'=>'','class'=>'request')); ?>
				<?php echo $form->error($model, 'price_all'); ?>
			</div>
			</div>
		<div class="row clearfix">
			<?php echo $form->labelEx($model,'price_diag',array('class'=>'control-label')); ?>
			<div class="text-name note-form">
				<?php echo $form->textField($model,'price_diag',array('placeholder'=>'','class'=>'request')); ?>
				<?php echo $form->error($model, 'price_diag'); ?>
			</div>
		</div>
		<div class="row clearfix">
			<?php echo $form->labelEx($model,'price_analyz',array('class'=>'control-label')); ?>
			<div class="text-name note-form">
				<?php echo $form->textField($model,'price_analyz',array('placeholder'=>'','class'=>'request')); ?>
				<?php echo $form->error($model, 'price_analyz'); ?>
			</div>
		</div>
		<div class="row clearfix">
			<?php echo $form->labelEx($model,'price_stomat',array('class'=>'control-label')); ?>
			<div class="text-name note-form">
				<?php echo $form->textField($model,'price_stomat',array('placeholder'=>'','class'=>'request')); ?>
				<?php echo $form->error($model, 'price_stomat'); ?>
			</div>
		</div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'ident_id',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->textField($model,'ident_id',array('placeholder'=>'','class'=>'request')); ?>
                <?php echo $form->error($model, 'ident_id'); ?>
            </div>
        </div>
		<div class="row clearfix">
			<?php echo $form->labelEx($model,'contact_phone',array('class'=>'control-label')); ?>
			<div class="text-name">
				<?php echo $form->textField($model,'contact_phone',array('placeholder'=>'','class'=>'request')); ?>
				<?php echo $form->error($model, 'contact_phone'); ?>
			</div>
		</div>
		<div class="row clearfix">
			<?php echo $form->labelEx($model,'payment_type',array('class'=>'control-label')); ?>
			<div class="text-name">
				<?php echo $form->dropDownList($model, 'payment_type', array(Partner::$paymentTypeText), array('placeholder'=>'','class'=>'request')); ?>
				<?php echo $form->error($model, 'payment_type'); ?>
			</div>
		</div>
		<div class="row clearfix">
			<?php echo $form->labelEx($model,'payment_number',array('class'=>'control-label')); ?>
			<div class="text-name">
				<?php echo $form->textField($model,'payment_number',array('placeholder'=>'','class'=>'request')); ?>
				<?php echo $form->error($model, 'payment_number'); ?>
			</div>
		</div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'domain',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->dropDownList($model, 'domain',array(""=>"")+CHtml::listData(MbDomain::model()->findAll(),'title', 'title')); ?>
                <?php echo $form->error($model, 'domain'); ?>
            </div>
         </div>
		<div class="row clearfix">
			<?php echo $form->labelEx($model,'site',array('class'=>'control-label')); ?>
			<div class="text-name">
				<?php echo $form->textField($model,'site',array('placeholder'=>'','class'=>'request')); ?>
				<?php echo $form->error($model, 'site'); ?>
			</div>
		</div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'body',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->textArea($model,'body',array('placeholder'=>'')); ?>
                <?php echo $form->error($model, 'body'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <button class="">Сохранить</button>
        </div>
    <?php $this->endWidget(); ?>
</div>