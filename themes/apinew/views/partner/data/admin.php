<div class="row line clearfix">
    <div class="cell number"><?php echo $model->id;?></div>
    <div class="cell legalName"><?php echo!empty($model->title)?$model->title:'';?></div>
    <div class="cell actualName"><?php echo!empty($model->name)?$model->name:'';?></div>
    <div class="cell actualName"><?php echo!empty($model->phone)?$model->phone:'';?></div>
	<div class="cell actualName"><?php echo!empty($model->site)?$model->site:'';?></div>
    <div class="cell recording"><?php echo!empty($model->ident_id)?$model->ident_id:'';?></div>
    <div class="cell surviving"><?php echo!empty($model->domain)?$model->domain:'';?></div>
    <div class="cell successing"><?php echo!empty($model->create_time)?$model->create_time:'';?></div>
    <div class="cell update"><a href="<?php echo Yii::app()->createUrl('partner/update',array('id'=>$model->id));?>" class="iconupdate"></a></div>
    <div class="cell deleted"><a href="<?php echo Yii::app()->createUrl('partner/delete',array('id'=>$model->id));?>" class="row_deleted iconclear"></a></div>
</div>