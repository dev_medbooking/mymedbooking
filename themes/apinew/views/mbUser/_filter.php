<thead>
            <tr>
                <td class="id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('uid',null,array('class'=>'csorting')); ?></td>
                <td class="telephone<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('telephone',null,array('class'=>'csorting')); ?></td>
                <td class="email<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('email',null,array('class'=>'csorting')); ?></td>
                <td class="nameperson<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('nameperson',null,array('class'=>'csorting')); ?></td>
                <td>Роль</td>
                <td class="edit"></td>
                <td class="view"></td>
                <td class="delete"></td>
                <td class="icon-share"></td>
            </tr>
            <tr class="filters">
                <td><?php echo CHtml::activeTextField($model,'uid',array('class'=>'search-single','id'=>'filter_id')); ?></td>
                <td><?php echo CHtml::activeTextField($model,'telephone',array('class'=>'search','id'=>'filter_telephone')); ?></td>
                <td><?php echo CHtml::activeTextField($model,'email',array('class'=>'search','id'=>'filter_email')); ?></td>
                <td><?php echo CHtml::activeTextField($model,'nameperson',array('class'=>'search','id'=>'filter_nameperson')); ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </thead>
