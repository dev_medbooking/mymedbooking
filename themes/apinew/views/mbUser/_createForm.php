<div id="create">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'create-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'form-horizontal'),
    )); ?>
        <?php if($model->errors): ?>
        <div class="well">
            <?php echo $form->errorSummary($model); ?>
        </div>
        <?php endif; ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'password',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'role',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList($model,'role',$this->availableRoles); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'telephone',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php $this->widget('CMaskedTextField', array('model'=>$model,'attribute'=>'telephone','mask'=>'(999)999-9999','placeholder' => '_','htmlOptions'=>array('class'=>'span3'))); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
                <?php echo $form->labelEx($model,'email',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->emailField($model,'email',array('size'=>60,'maxlength'=>255,'class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'nameperson',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'nameperson',array('size'=>60,'maxlength'=>255,'class'=>'span6')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'fio',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'fio',array('size'=>60,'maxlength'=>255,'class'=>'span6')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'gender',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList($model,'gender',array(0=>'М',1=>'Ж'),array('class'=>'span3')); ?>
                <div class="error"></div>
            </div>
        </div>
        <div class="form-actions">
            <?php echo CHtml::submitButton("Сохранить",array("class"=>"btn btn-info"));?>
            <?php echo CHtml::resetButton("Сбросить",array("class"=>"btn"));?>
        </div>
    <?php $this->endWidget(); ?>
</div>