<?php if(empty($type)): ?>
<tr id="tr-item-<?php echo $model->uid; ?>" class="edit" data-id="<?php echo $model->uid; ?>">
<?php endif; ?>
    <td>
        <?php echo $model->uid; ?>
    </td>
    <td>
        <div class="visihid" data-hid="<?php echo $model->uid; ?>">
            <?php echo !empty($model->telephone)?$model->telephone:''; ?>
        </div>
        <div data-hid="<?php echo $model->uid; ?>" class="hid clearfix" style="display:none;">
            <?php echo CHtml::activeTextField($model,'telephone',array('id'=>$this->contrModel.'_telephone_'.$model->uid,'class'=>'span12','data-model'=>$model->uid)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td>
        <div class="visihid" data-hid="<?php echo $model->uid; ?>">
            <?php echo !empty($model->email)?$model->email:''; ?>
        </div>
        <div data-hid="<?php echo $model->uid; ?>" class="hid clearfix" style="display:none;">
            <?php echo CHtml::activeTextField($model,'email',array('id'=>$this->contrModel.'_email_'.$model->uid,'class'=>'span12','data-model'=>$model->uid)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td>
        <div class="visihid" data-hid="<?php echo $model->uid; ?>">
            <?php echo !empty($model->nameperson)?$model->nameperson:''; ?>
        </div>
        <div data-hid="<?php echo $model->uid; ?>" class="hid clearfix" style="display:none;">
            <?php echo CHtml::activeTextField($model,'nameperson',array('id'=>$this->contrModel.'_nameperson_'.$model->uid,'class'=>'span12','data-model'=>$model->uid)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td>
        <div class="visihid" data-hid="<?php echo $model->uid; ?>">
            <?php echo !empty($model->role)?$model->role:''; ?>
        </div>
        <div data-hid="<?php echo $model->uid; ?>" class="hid clearfix" style="display:none;">
            <?php echo CHtml::activeDropDownList($model,'role',$this->availableRoles,array('id'=>$this->contrModel.'_role_'.$model->uid,'class'=>'span12','data-model'=>$model->uid)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td class="ico-edit">
        <?php echo CHtml::link('',Yii::app()->createUrl('/'.Yii::app()->controller->id.'/createForm',array('id'=>$model->uid)),array("data-href"=>Yii::app()->createUrl('/'.Yii::app()->controller->id.'/form',array('id'=>$model->uid)),"class"=>'form','data-id'=>$model->uid)); ?>
    </td>
    <td class="ico-view">
        <?php echo CHtml::link('',array('/'.Yii::app()->controller->id.'/view','id'=>$model->uid),array('data-id'=>$model->uid)); ?>
    </td>
    <td class="ico-delete">
        <?php echo CHtml::link('',array('/'.Yii::app()->controller->id.'/delete','id'=>$model->uid),array('data-id'=>$model->uid,'class'=>'delete')); ?>
    </td>
    <td>
        <div data-hid="<?php echo $model->uid; ?>" class="visihid">
            <?php echo CHtml::link('<i class="icon-share"></i>',array('#','id'=>$model->uid),array('data-id'=>$model->uid,'class'=>'fast-change')); ?>
        </div>
        <div data-hid="<?php echo $model->uid; ?>" class="hid clearfix" style="display:none;">
            <?php echo CHtml::link('<i class="icon-check-empty"></i>',"#",array("class"=>'unedit','data-id'=>$model->uid)); ?>
            <?php echo CHtml::link('<i class="icon-pencil"></i>',array('/'.Yii::app()->controller->id.'/update','id'=>$model->uid),array("class"=>'update','data-id'=>$model->uid)); ?>
        </div>
    </td>
<?php if(empty($type)): ?>
</tr>
<?php endif; ?>
