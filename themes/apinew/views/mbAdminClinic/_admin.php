<div class="grid-view" id="admin-admin" data-href="<?php echo Yii::app()->createUrl("/".Yii::app()->controller->id."/admin",array('page'=>(!empty($_REQUEST['page'])?$_REQUEST['page']:'1'))); ?>">
    <div class="summary"><?php echo Yii::t('app',"Всего&nbsp;{n}&nbsp;результат|Всего&nbsp;{n}&nbsp;результата|Всего&nbsp;{n}&nbsp;результатов",array($count)); ?></div>
    <table>
        <thead>
            <?php $this->renderPartial('//layouts/_admin_thead',array('data'=>array(
                'id'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search-single',
                    'visibility'=>true,
                ),
                'title'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'translit'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'address'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'telephone'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'url'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'regimeClinic'=>array(
                    'sort'=>false,
                    'filter'=>false,
                    'visibility'=>true,
                ),
                'specialistAll'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'detAll'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'rate10'=>array(
                    'sort'=>false,
                    'filter'=>false,
                    'visibility'=>true,
                ),
                'countDoctors'=>array(
                    'sort'=>false,
                    'filter'=>false,
                    'visibility'=>true,
                ),
                'subway_name'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'status'=>array(
                    'sort'=>'sorting',
                    'filter'=>'select',
                    'visibility'=>true,
                    'data'=>array(""=>"",3=>"Нет",1=>"Да",2=>"Удалено"),
                ),
                'pencil'=>array(
                    'type'=>true,
                    'content'=>'<i class="icon-pencil">',
//                    'roles'=>BController::roles(2,$this->contrModel),
                ),
                'trash'=>array(
                    'type'=>true,
                    'content'=>'<i class="icon-trash">',
//                    'roles'=>BController::roles(3,$this->contrModel),
                ),
                'view'=>array(
                    'type'=>true,
                    'content'=>'<i class="icon-eye-open">',
                    'roles'=>true,
                ),
                'open'=>array(
                    'type'=>true,
                    'content'=>'<i class="icon-share">',
//                    'roles'=>BController::roles(2,$this->contrModel),
                ),
            ),'sort'=>$sort,'model'=>$model)); ?>
        </thead>
        <tbody id="admin-tbody">
            <?php $this->renderPartial('//layouts/_admin_tbody',array('data'=>$data)); ?>
        </tbody>
    </table>
    <?php if(!empty($pages)&&!empty($count)): ?>
    <div class="pagination row">
        <?php $this->renderPartial('//layouts/_admin_pages',array('sort'=>$sort,'pages'=>$pages,'count'=>$count)); ?>
    </div>
    <?php endif; ?>
</div>