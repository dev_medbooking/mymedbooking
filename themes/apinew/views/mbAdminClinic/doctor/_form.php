<div id="create">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'action'=>empty($model->id)?array('/'.Yii::app()->controller->id.'/createDoctor','id'=>$id):array('/'.Yii::app()->controller->id.'/updateDoctorForm','id'=>$model->id),
        'id'=>'create-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'form-horizontal'),
    )); ?>
        <?php if($model->errors): ?>
        <div class="well">
            <?php echo $form->errorSummary($model); ?>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('speciality',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'speciality',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'speciality', array('id'=>'category_name','size'=>60,'maxlength'=>255,'class'=>'autocomplete span6','data-aid'=>'category_id','data-href'=>Yii::app()->createUrl('/site/categoryJSON'))); ?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('position',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'position',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'position',array('size'=>60,'maxlength'=>255,'class'=>'span6')); ?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('doctor_id',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'doctor_name',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'doctor_name',array('id'=>'doctor_name','size'=>60,'maxlength'=>255,'class'=>'autocomplete span6','data-aid'=>'AdminClinicDoctor_doctor_id','data-href'=>Yii::app()->createUrl('/site/doctorJSON'))); ?>
                <?php echo $form->hiddenField($model,'doctor_id',array('id'=>'AdminClinicDoctor_doctor_id','class'=>'span12')); ?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <div class="form-actions">
            <div class="span3">
                <?php if(!empty($model->id)): ?>
                <?php echo CHtml::submitButton("Сохранить",array('data-id'=>$model->id,'id'=>'update-btn','class'=>'btn btn-large btn-primary')); ?>
                <?php else: ?>
                <?php echo CHtml::submitButton("Создать",array('id'=>'create-btn','class'=>'btn btn-large btn-primary')); ?>
                <?php endif; ?>
            </div>
        </div>
    <?php $this->endWidget(); ?>
</div>