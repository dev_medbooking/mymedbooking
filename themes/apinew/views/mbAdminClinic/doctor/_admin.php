<div class="grid-view" id="admin-admin" data-href="<?php echo Yii::app()->createUrl("/".Yii::app()->controller->id."/adminDoctor",array('id'=>$_GET['id'],'page'=>(!empty($_REQUEST['page'])?$_REQUEST['page']:'1'))); ?>">
    <div class="summary"><?php echo Yii::t('app',"Всего&nbsp;{n}&nbsp;результат|Всего&nbsp;{n}&nbsp;результата|Всего&nbsp;{n}&nbsp;результатов",array($count)); ?></div>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <?php $this->renderPartial('//layouts/_admin_thead',array('data'=>array(
                'id'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search-single',
                    'visibility'=>true,
                ),
                'doctor_id'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'speciality'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'position'=>array(
                    'sort'=>'sorting',
                    'filter'=>'search',
                    'visibility'=>true,
                ),
                'pencil'=>array(
                    'type'=>true,
                    'content'=>'<i class="icon-pencil">',
                    'roles'=>BController::roles(2,$this->contrModel),
                ),
                'trash'=>array(
                    'type'=>true,
                    'content'=>'<i class="icon-trash">',
                    'roles'=>BController::roles(3,$this->contrModel),
                ),
            ),'sort'=>$sort,'model'=>$model)); ?>
        </thead>
        <tbody id="admin-tbody">
            <?php $this->renderPartial('doctor/_admin_tbody',array('data'=>$data)); ?>
        </tbody>
    </table>
    <?php if(!empty($pages)&&!empty($count)): ?>
    <div class="pagination row">
        <?php $this->renderPartial('//layouts/_admin_pages',array('sort'=>$sort,'pages'=>$pages,'count'=>$count)); ?>
    </div>
    <?php endif; ?>
</div>