<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/table2.js',CClientScript::POS_END);?>
<?php $this->renderPartial('navbar'); ?>
<div class="row-fluid">
    <?php echo CHtml::beginForm(array('/'.Yii::app()->controller->id.'/adminPrice','id'=>$clinic->id),'get',array('id'=>'admin-form')); ?>
    <?php $this->renderPartial('price/_admin',array('model'=>$model,'sort'=>$sort,'pages'=>$pages,'count'=>$count,'data'=>$data)); ?>
    <?php echo CHtml::endForm(); ?>
</div>
<div class="row-fluid" id="form-ajax">
    <?php $this->renderPartial('price/_form',array('model'=>$model,'id'=>$clinic->id)); ?>
</div>