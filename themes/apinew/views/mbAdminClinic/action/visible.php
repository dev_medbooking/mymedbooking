<?php $this->renderPartial('navbar');?>
<div class="row-fluid">
    <div id="create">
        <?php echo CHtml::beginForm(); ?>
            <?php $this->renderPartial('//layouts/_role',array('data'=>$data));?>
            <div class="form-actions">
                <?php echo CHtml::submitButton("Сохранить",array("class"=>"btn btn-info"));?>
            </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>