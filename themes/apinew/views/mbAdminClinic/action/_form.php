<div id="create">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'action'=>empty($model->id)?array('/'.Yii::app()->controller->id.'/createAction','id'=>$id):array('/'.Yii::app()->controller->id.'/updateActionForm','id'=>$model->id),
        'id'=>'create-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'form-horizontal'),
    )); ?>
        <?php if($model->errors): ?>
        <div class="well">
            <?php echo $form->errorSummary($model); ?>
        </div>
        <?php endif; ?>
        
        <?php if(BController::visibility('action_id',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'action_name',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'action_name',array('id'=>'action_name','size'=>60,'maxlength'=>255,'class'=>'autocomplete span6','data-aid'=>'action_action_id','data-href'=>Yii::app()->createUrl('/site/actionJSON'))); ?>
                <?php echo $form->hiddenField($model,'action_id',array('id'=>'action_action_id','class'=>'span12')); ?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <div class="form-actions">
            <div class="span3">
                <?php if(!empty($model->id)): ?>
                <?php echo CHtml::submitButton("Сохранить",array('data-id'=>$model->id,'id'=>'update-btn','class'=>'btn btn-large btn-primary')); ?>
                <?php else: ?>
                <?php echo CHtml::submitButton("Создать",array('id'=>'create-btn','class'=>'btn btn-large btn-primary')); ?>
                <?php endif; ?>
            </div>
        </div>
    <?php $this->endWidget(); ?>
</div>