<?php if(empty($type)): ?>
<tr id="tr-item-<?php echo $model->id; ?>" class="edit" data-id="<?php echo $model->id; ?>">
<?php endif; ?>
    <?php if(BController::visibility('id',$this->contrModel)): ?> 
    <td>
        <?php echo $model->id; ?>
    </td>
    <?php endif; ?>
    <?php if(BController::visibility('action_id',$this->contrModel)): ?>
    <td>
        <div data-hid="<?php echo $model->id; ?>" class="visihid">
            <?php echo !empty($model->clinic_action_action->title)?$model->clinic_action_action->title:''; ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::activeTextField($model,'action_name',array('id'=>$this->contrModel.'_action_name_'.$model->id,'class'=>'span12 autocomplete','data-model'=>$model->id,'data-aid'=>$this->contrModel.'_action_id_'.$model->id,'data-href'=>Yii::app()->createUrl('/site/actionJSON'))); ?>
            <?php echo CHtml::activeHiddenField($model,'action_id',array('id'=>$this->contrModel.'_action_id_'.$model->id,'class'=>'span12','data-model'=>$model->id)); ?>
            <div class="error"></div>
        </div>
    </td>
    <?php endif; ?>
    
    <?php if(BController::roles(2,$this->contrModel)): ?>
    <td>
        <div data-hid="<?php echo $model->id; ?>" class="visihid">
            <?php echo CHtml::link('Ф',"#create-form",array("data-href"=>Yii::app()->createUrl('/'.Yii::app()->controller->id.'/formAction',array('id'=>$model->id)),"class"=>'form','data-id'=>$model->id)); ?>
            <?php echo CHtml::link('<i class="icon-share"></i>',array('#','id'=>$model->id),array('data-id'=>$model->id,'class'=>'fast-change')); ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::link('<i class="icon-check-empty"></i>',"#",array("class"=>'unedit','data-id'=>$model->id)); ?>
            <?php echo CHtml::link('<i class="icon-pencil"></i>',array('/'.Yii::app()->controller->id.'/updateAction','id'=>$model->id),array("class"=>'update','data-id'=>$model->id)); ?>
        </div>
    </td>
    <?php endif; ?>
    <?php if(BController::roles(3,$this->contrModel)): ?>
    <td>
        <?php echo CHtml::link('<i class="icon-trash"></i>',array('/'.Yii::app()->controller->id.'/deleteAction','id'=>$model->id),array('data-id'=>$model->id,'class'=>'delete')); ?>
    </td>
    <?php endif; ?>
<?php if(empty($type)): ?>
</tr>
<?php endif; ?>