<div class="">
    <ul class="nav nav-tabs">
        <li<?php echo Yii::app()->controller->action->id=='index'?' class="active"':''; ?>><?php echo CHtml::link('Таблица',array('/'.Yii::app()->controller->id.'/index'),array('class'=>Yii::app()->controller->action->id=='index'?'active':'')); ?></li>
        <?php if(Yii::app()->user->checkAccess('Moderator')): ?>
        <?php if(BController::roles(1,$this->contrModel)): ?>
        <li<?php echo Yii::app()->controller->action->id=='create'?' class="active"':''; ?>><?php echo CHtml::link('Создать',array('/'.Yii::app()->controller->id.'/create'),array('class'=>Yii::app()->controller->action->id=='create'?'active':'')); ?></li>
        <?php endif;?>
        <?php endif;?>
        <?php if(!empty($_GET['id'])): ?>
        <li<?php echo Yii::app()->controller->action->id=='view'?' class="active"':''; ?>><?php echo CHtml::link('Смотреть',array('/'.Yii::app()->controller->id.'/view','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='view'?'active':'')); ?></li>
        <?php endif;?>
        <?php if(Yii::app()->user->checkAccess('Moderator')): ?>
        <?php if(!empty($_GET['id'])): ?>
        <?php if(BController::roles(2,$this->contrModel)): ?>
        <li<?php echo Yii::app()->controller->action->id=='updateClinic'?' class="active"':''; ?>><?php echo CHtml::link('Редактировать',array('/'.Yii::app()->controller->id.'/updateClinic','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='updateClinic'?'active':'')); ?></li>
        <?php endif;?>
        <?php endif;?>
        <?php endif;?>
        <?php if(!empty($_GET['id'])): ?>
        <li<?php echo Yii::app()->controller->action->id=='indexDoctor'?' class="active"':''; ?>><?php echo CHtml::link('Врачи',array('/'.Yii::app()->controller->id.'/indexDoctor','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='indexDoctor'?'active':'')); ?></li>
        <li<?php echo Yii::app()->controller->action->id=='indexCategory'?' class="active"':''; ?>><?php echo CHtml::link('Услуги',array('/'.Yii::app()->controller->id.'/indexCategory','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='indexCategory'?'active':'')); ?></li>
        <li<?php echo Yii::app()->controller->action->id=='indexPrice'?' class="active"':''; ?>><?php echo CHtml::link('Прайсы',array('/'.Yii::app()->controller->id.'/indexPrice','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='indexPrice'?'active':'')); ?></li>
        <li<?php echo Yii::app()->controller->action->id=='indexAction'?' class="active"':''; ?>><?php echo CHtml::link('Акции',array('/'.Yii::app()->controller->id.'/indexAction','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='indexAction'?'active':'')); ?></li>
        <?php endif; ?>
        <li<?php echo Yii::app()->controller->action->id=='visible'?' class="active"':''; ?>><?php echo CHtml::link('Видимость',array('/'.Yii::app()->controller->id.'/visible','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='visible'?'active':'')); ?></li>
        <li<?php echo Yii::app()->controller->action->id=='visibleCategory'?' class="active"':''; ?>><?php echo CHtml::link('Видимость (cat.)',array('/'.Yii::app()->controller->id.'/visibleCategory','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='visibleCategory'?'active':'')); ?></li>
        <li<?php echo Yii::app()->controller->action->id=='visibleDoctor'?' class="active"':''; ?>><?php echo CHtml::link('Видимость (doc.)',array('/'.Yii::app()->controller->id.'/visibleDoctor','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='visibleDoctor'?'active':'')); ?></li>
        <li<?php echo Yii::app()->controller->action->id=='visiblePrice'?' class="active"':''; ?>><?php echo CHtml::link('Видимость (pri.)',array('/'.Yii::app()->controller->id.'/visiblePrice','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='visiblePrice'?'active':'')); ?></li>
        <?php if(Yii::app()->user->checkAccess('Administrator')): ?>
        <li<?php echo Yii::app()->controller->action->id=='role'?' class="active"':''; ?>><?php echo CHtml::link('Роль',array('/'.Yii::app()->controller->id.'/role','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='role'?'active':'')); ?></li>
        <li<?php echo Yii::app()->controller->action->id=='roleCategory'?' class="active"':''; ?>><?php echo CHtml::link('Роль (cat.)',array('/'.Yii::app()->controller->id.'/roleCategory','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='roleCategory'?'active':'')); ?></li>
        <li<?php echo Yii::app()->controller->action->id=='roleDoctor'?' class="active"':''; ?>><?php echo CHtml::link('Роль (doc.)',array('/'.Yii::app()->controller->id.'/roleDoctor','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='roleDoctor'?'active':'')); ?></li>
        <li<?php echo Yii::app()->controller->action->id=='rolePrice'?' class="active"':''; ?>><?php echo CHtml::link('Роль (pri.)',array('/'.Yii::app()->controller->id.'/rolePrice','id'=>!empty($_GET['id'])?$_GET['id']:''),array('class'=>Yii::app()->controller->action->id=='rolePrice'?'active':'')); ?></li>
        <?php endif; ?>          
    </ul>
</div>
<hr>