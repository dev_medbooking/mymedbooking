<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/table2.js', CClientScript::POS_END); ?>
<div class="table_wrap" style="<? if(!empty($_REQUEST['partial'])) echo 'margin:0'?>">
<!--    --><?php // if(empty($_REQUEST['partial'])) $this->renderPartial('navbar'); ?>
    <div class="">
        <div class="block-seacrh-free">
            <?php echo CHtml::textField('search-free', '', array('id' => 'search-free', 'class' => 'span6', 'placeholder' => 'Свободный поиск')); ?>
        </div>
        <?php echo CHtml::beginForm(array('/' . Yii::app()->controller->id . '/admin'), 'get', array('id' => 'admin-form')); ?>
        <?php $this->renderPartial('_admin', array('model' => $model, 'sort' => $sort, 'pages' => $pages, 'count' => $count, 'data' => $data)); ?>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>