<?php if(empty($type)): ?>
<tr id="tr-item-<?php echo $model->id; ?>" class="edit" data-id="<?php echo $model->id; ?>">
<?php endif; ?>
    <td>
        <?php echo $model->id; ?>
    </td>
    <td <?php echo !empty($model['none_status'])?'style="color:red;"':''; ?>>
        <div data-hid="<?php echo $model->id; ?>" class="visihid">
            <?php echo !empty($model->title)?$model->title:''; ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::activeTextField($model,'title',array('id'=>$this->contrModel.'_title_'.$model->id,'class'=>'span12','data-model'=>$model->id)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td>
        <div data-hid="<?php echo $model->id; ?>" class="visihid">
            <?php echo !empty($model->translit)?$model->translit:''; ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::activeTextField($model,'translit',array('id'=>$this->contrModel.'_translit_'.$model->id,'class'=>'span12','data-model'=>$model->id)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td>
        <div data-hid="<?php echo $model->id; ?>" class="visihid">
            <?php if(!empty($model->url)): ?><?php echo CHtml::link(!empty($model->address)?$model->address:'n/a',$model->url); ?><?php else: ?><?php echo !empty($model->address)?$model->address:''; ?><?php endif; ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::activeTextField($model,'address',array('id'=>$this->contrModel.'_address_'.$model->id,'class'=>'span12','data-model'=>$model->id)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td>
        <div data-hid="<?php echo $model->id; ?>" class="visihid">
            <?php echo !empty($model->telephone)?$model->telephone:''; ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::activeTextField($model,'telephone',array('id'=>$this->contrModel.'_telephone_'.$model->id,'class'=>'span12 telephone','data-model'=>$model->id)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td>
        <?php echo !empty($model->url)?$model->url:''; ?>
    </td>
    <td>
        <?php if(!empty($model->regimeClinic)): ?>
        <?php foreach($model->regimeClinic as $k=>$v): ?>
        <?php echo $k.": "; echo $v."\n"; ?>
        <?php endforeach; ?>
        <?php endif; ?>
    </td>
    <td>
        <?php echo $model->getSpecialistAll(); ?>
    </td>
    <td>
        <?php echo $model->getDetAll(); ?>
    </td>
    <td>
        <?php echo $model->rate10; ?>
    </td>
    <td>
        <?php echo $model->countDoctors; ?>
    </td>
    <td>
        <?php echo !empty($model->subway_name)?$model->subway_name:''; ?>
    </td>
    <td>
        <div data-hid="<?php echo $model->id; ?>" class="visihid">
            <?php echo !empty($model->status)?$model->status:'0'; ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::activeDropDownList($model, 'status', array(0=>'Не модерирован', 1=>'Модерирован',2=>'Удалено'), array('id' => $this->contrModel.'_status_' . $model->id, 'class' => 'span12', 'data-model' => $model->id)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td>
        <?php echo CHtml::link('<i class="icon-pencil"></i>',Yii::app()->createUrl('/'.Yii::app()->controller->id.'/updateClinic',array('id'=>$model->id)),array("data-href"=>Yii::app()->createUrl('/adminAlias/form',array('id'=>$model->id)),"class"=>'form','data-id'=>$model->id)); ?>
    </td>
    <td>
        <?php echo CHtml::link('<i class="icon-trash"></i>',array('/'.Yii::app()->controller->id.'/delete','id'=>$model->id),array('data-id'=>$model->id,'class'=>'delete')); ?>
    </td>
    <td>
        <?php echo CHtml::link('<i class="icon-eye-open"></i>',array('/'.Yii::app()->controller->id.'/view','id'=>$model->id),array('data-id'=>$model->id,'class'=>'view')); ?>
    </td>
    <td>
        <div data-hid="<?php echo $model->id; ?>" class="visihid">
            <?php echo CHtml::link('<i class="icon-share"></i>',array('#','id'=>$model->id),array('data-id'=>$model->id,'class'=>'fast-change')); ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::link('<i class="icon-check-empty"></i>',"#",array("class"=>'unedit','data-id'=>$model->id)); ?>
            <?php echo CHtml::link('<i class="icon-pencil"></i>',array('/'.Yii::app()->controller->id.'/update','id'=>$model->id),array("class"=>'update','data-id'=>$model->id)); ?>
        </div>
    </td>
<?php if(empty($type)): ?>
</tr>
<?php endif; ?>