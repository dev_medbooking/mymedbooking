<div id="create">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'action'=>empty($model->id)?array('/'.Yii::app()->controller->id.'/create'):array('/'.Yii::app()->controller->id.'/updateClinic','id'=>$model->id),
        'id'=>'create-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'form-horizontal','enctype' => 'multipart/form-data'),
    )); ?>
        <?php if($model->errors): ?>
        <div class="well">
            <?php echo $form->errorSummary($model); ?>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('title',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'title',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('sames',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'sames',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'sames',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('price_title',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'price_title',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'price_title',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>    
        <?php if(BController::visibility('translit',$this->contrModel)): ?>
        <div class="control-group">
                <?php echo $form->labelEx($model,'translit',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'translit',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('address',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'address',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('email',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'email',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('inn',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'inn',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'inn',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('district',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'district',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'district',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('url',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'url',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('alias',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'alias',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('external_id',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'external_id',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'external_id',array('size'=>60,'maxlength'=>255,'class'=>'span6'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('status',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'status',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->dropDownList($model,'status',array(0=>"не модерировано",1=>"модерировано",2=>"Удалено",3=>"Сеть"),array('class'=>'span3'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('none_status',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'none_status',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->dropDownList($model,'none_status',array(0=>"записывать",1=>"не записывать"),array('class'=>'span3'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('start_rate',$this->contrModel)): ?>
        <?php if(!$model->isNewRecord&&$model->status==3): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'start_rate',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'start_rate',array('size'=>60,'maxlength'=>255,'class'=>'span6'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php else: ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'start_rate',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->dropDownList($model,'start_rate',array(0=>"Не установлен",1=>"1",2=>"2",3=>"3",4=>"4",5=>"5",6=>"6",7=>"7",8=>"8",9=>"9",10=>"10"),array('class'=>'span3'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php endif; ?>
        <?php if(BController::visibility('telephone',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'telephone',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'telephone',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('modificator',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'modificator',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'modificator',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif;?>
        <?php if(BController::visibility('image',$this->contrModel)): ?>
        <div class="control-group">
            <label for="" class="control-label">
                <?php echo $form->labelEx($model,'image');?>
            </label>
            <div class="controls">
                <?php if(!empty($model->image)):?> 
                <?php if(file_exists(Yii::getPathOfAlias('webroot').'/'.$model->getImagePath('small'))):?>
                <div id="file_image">
                    <?php echo CHtml::image(Yii::app()->baseUrl.'/'.$model->getImagePath('small'));?>
                    <?php echo CHtml::ajaxLink('Удалить',array('fileDelete','id'=>$model->primaryKey,'field'=>'image'),array('success'=>'js:function(){$("#file_image").remove();}'),array('data-dismiss'=>"fileupload",'class'=>'btn btn-delete btn-small'));?>
                </div>
                <?php endif;?>
                <?php endif;?>
                <?php echo $form->fileField($model,'file_image',array('id'=>"id-input-file-2"));?>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('lat',$this->contrModel)): ?>
        <?php
        $lat=!empty($model->lat)?$model->lat:'ymaps.geolocation.latitude';
        $lng=!empty($model->lng)?$model->lng:'ymaps.geolocation.longitude';
        $_id=!empty($model->lat)&&!empty($model->lng)?1:0;
        Yii::app()->clientScript->registerScript("yandexMapAdd","
            ymaps.ready(init);
            function init () {
                var coords=[{$lat},{$lng}];
                var id={$_id};
                var myMap = new ymaps.Map(\"map\", {
                        center: [coords[0],coords[1]],
                        zoom: 10
                    }),
                    myPlacemark = new ymaps.Placemark([coords[0],coords[1]]),
                    myCollection = new ymaps.GeoObjectCollection({});
                    myMap.controls.add('zoomControl').add('typeSelector').add('smallZoomControl', { right: 5, top: 75 }).add('mapTools').add('searchControl');
                    myMap.events.add(\"click\",
                        function(e) {
                            coords = e.get('coordPosition');
                            myPlacemark = new ymaps.Placemark([coords[0],coords[1]], {
                                hintContent: 'Подвинь меня!'
                            }, {
                                draggable: true
                            });
                            $('#lat').val(coords[0]);
                            $('#lng').val(coords[1]);
                            myCollection.removeAll();
                            myCollection.add(myPlacemark);
                            myMap.geoObjects.add(myCollection);
                        }
                    );
                    if(id) myCollection.add(myPlacemark);
                    myMap.geoObjects.add(myCollection);
            }
        ",CClientScript::POS_READY); ?>
        <?php endif; ?>
        <?php if(BController::visibility('title',$this->contrModel)): ?>
        <div class="control-group">
            <label for="" class="control-label">Метка на карте</label>
            <div class="controls">
                <div id="map" style="width:100%; height:425px"></div>
                <?php echo $form->hiddenField($model,'lat',array("id"=>'lat'));?>
                <?php echo $form->hiddenField($model,'lng',array("id"=>'lng'));?>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('description',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'description',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textArea($model,'description',array('rows'=>12,'cols'=>50,'class'=>'span12'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('body',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'body',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textArea($model,'body',array('rows'=>12,'cols'=>50,'class'=>'span12'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('companyname',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'companyname',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'companyname',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('subway_name',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'subway_name',array('class'=>'control-label')); ?>
            <div class="controls">
                <span class="new-block">
                    <?php echo $form->textField($model,'subway_array[new][title]',array('id'=>'subway_name','size'=>60,'maxlength'=>255,'class'=>'autocomplete-subway span6','data-href'=>Yii::app()->createUrl('/site/subwayJSON'))); ?>
                    <?php echo $form->textField($model,'subway_array[new][lin1]',array('size'=>60,'maxlength'=>255,'id'=>'min_man','class'=>'span2','placeholder'=>'Мин. пешком'));?>
                    <?php echo $form->textField($model,'subway_array[new][lin2]',array('size'=>60,'maxlength'=>255,'id'=>'min_car','class'=>'span2','placeholder'=>'Мин. авто'));?>
                </span>    
                    <input type="button" value="Добавить" id="add-metro" data-class="AdminClinic">
                <div class="error"></div>
            </div>
             <div class="more-metro">
                <?php if(!empty($model->subway_array)):?>
                        <?php foreach ($model->subway_array as $key=>$val):?>
                            <div class="controls  subway-block-<?=$key?>">
                                <?php echo $form->textField($model,'subway_array['.$key.'][title]',array('id'=>'subway_name'.$key,'size'=>60,'maxlength'=>255,'class'=>'autocomplete-subway span6','placeholder'=>'Метро','data-href'=>Yii::app()->createUrl('/site/subwayJSON'))); ?>
                                <?php echo $form->textField($model,'subway_array['.$key.'][lin1]',array('size'=>60,'maxlength'=>255,'id'=>'min_man','class'=>'span2','placeholder'=>'Мин. пешком'));?>
                                <?php echo $form->textField($model,'subway_array['.$key.'][lin2]',array('size'=>60,'maxlength'=>255,'id'=>'min_car','class'=>'span2','placeholder'=>'Мин. авто'));?>
                                <input type="button" value="удалить" data-id="<?=$key?>" class="del-metro">
                            </div>
                        <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('meta_title',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'meta_title',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'meta_title',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('meta_keywords',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'meta_keywords',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'meta_keywords',array('size'=>60,'maxlength'=>255,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('meta_description',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'meta_description',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textArea($model,'meta_description',array('rows'=>6,'cols'=>50,'class'=>'span8'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <div class="control-group">
            <div class="controls">
                <?php if(BController::visibility('regime_byd',$this->contrModel)): ?>
                <div class="span3">
                    <?php echo $form->textField($model,'regime_byd',array('size'=>60,'maxlength'=>255,'class'=>'span12 input-mask-eyescript1','placeholder'=>'Режим будни'));?>
                    <div class="error"></div>
                </div>
                <?php endif; ?>
                <?php if(BController::visibility('regime_mon',$this->contrModel)): ?>
                <div class="span3">
                    <?php echo $form->textField($model,'regime_mon',array('size'=>60,'maxlength'=>255,'class'=>'span12 input-mask-eyescript1','placeholder'=>'Режим пн.'));?>
                    <div class="error"></div>
                </div>
                <?php endif; ?>
                <?php if(BController::visibility('regime_tue',$this->contrModel)): ?>
                <div class="span3">
                    <?php echo $form->textField($model,'regime_tue',array('size'=>60,'maxlength'=>255,'class'=>'span12 input-mask-eyescript1','placeholder'=>'Режим вт.'));?>
                    <div class="error"></div>
                </div>
                <?php endif; ?>
                <?php if(BController::visibility('regime_wed',$this->contrModel)): ?>
                <div class="span3">
                    <?php echo $form->textField($model,'regime_wed',array('size'=>60,'maxlength'=>255,'class'=>'span12 input-mask-eyescript1','placeholder'=>'Режим ср.'));?>
                    <div class="error"></div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php if(BController::visibility('regime_thu',$this->contrModel)): ?>
                <div class="span3">
                    <?php echo $form->textField($model,'regime_thu',array('size'=>60,'maxlength'=>255,'class'=>'span12 input-mask-eyescript1','placeholder'=>'Режим чт.'));?>
                    <div class="error"></div>
                </div>
                <?php endif; ?>
                <?php if(BController::visibility('regime_fri',$this->contrModel)): ?>
                <div class="span3">
                    <?php echo $form->textField($model,'regime_fri',array('size'=>60,'maxlength'=>255,'class'=>'span12 input-mask-eyescript1','placeholder'=>'Режим пт.'));?>
                    <div class="error"></div>
                </div>
                <?php endif; ?>
                <?php if(BController::visibility('regime_sat',$this->contrModel)): ?>
                <div class="span3">
                    <?php echo $form->textField($model,'regime_sat',array('size'=>60,'maxlength'=>255,'class'=>'span12 input-mask-eyescript1','placeholder'=>'Режим сб.'));?>
                    <div class="error"></div>
                </div>
                <?php endif; ?>
                <?php if(BController::visibility('regime_sun',$this->contrModel)): ?>
                <div class="span3">
                    <?php echo $form->textField($model,'regime_sun',array('size'=>60,'maxlength'=>255,'class'=>'span12 input-mask-eyescript1','placeholder'=>'Режим вс.'));?>
                    <div class="error"></div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <?php if(BController::visibility('network_id',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'network_id',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->dropDownList($model,'network_id',array(""=>"Нет") + CHtml::listData(AdminClinic::model()->findAll(array('condition'=>'network_id IS NULL OR network_id=""','order'=>'title')),'id','title'),array('class'=>'span3'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('district_id',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'district_id',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->dropDownList($model,'district_id',array(""=>"Нет") + CHtml::listData(AdminDistrict::model()->findAll(),'id','title'),array('class'=>'span3'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('clinic_city_line_id',$this->contrModel)): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model,'clinic_city_line_id',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->dropDownList($model,'clinic_city_line_id',CHtml::listData(AdminCityLine::model()->findAll(),'id','title'),array('class'=>'span3'));?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('clinic_city_id',$this->contrModel)): ?>   
        <div class="control-group">
            <?php echo $form->labelEx($model,'clinic_city_id',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'clinic_city_name',array('id'=>'clinic_city_name','size'=>60,'maxlength'=>255,'class'=>'autocomplete span6','data-aid'=>'clinic_clinic_city_id','data-href'=>Yii::app()->createUrl('/site/cityJSON'))); ?>
                <?php echo $form->hiddenField($model,'clinic_city_id',array('id'=>'clinic_clinic_city_id','class'=>'span12')); ?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('gallery_id',$this->contrModel)): ?>
        <?php if($model->galleryBehavior->getGallery()===null) echo '<p>Нет значения галлереи</p>'; else $this->widget('GalleryManager',array('gallery'=>$model->galleryBehavior->getGallery()));?>
        <?php echo $form->hiddenField($model,'gallery_id',array("id"=>'gallery_id'));?>
        <?php endif; ?>
        <div class="form-actions">
            <?php echo CHtml::submitButton("Сохранить",array("class"=>"btn btn-info"));?>
            <?php echo CHtml::resetButton("Сбросить",array("class"=>"btn"));?>
        </div>
    <?php $this->endWidget(); ?>
</div>