<div id="create">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'action'=>empty($model->id)?array('/'.Yii::app()->controller->id.'/createCategory','id'=>$id):array('/'.Yii::app()->controller->id.'/updateCategoryForm','id'=>$model->id),
        'id'=>'create-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'form-horizontal'),
    )); ?>
        <?php if($model->errors): ?>
        <div class="well">
            <?php echo $form->errorSummary($model); ?>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('price',$this->contrModel)): ?>    
        <div class="control-group">
            <?php echo $form->labelEx($model,'price',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'price',array('size'=>60,'maxlength'=>255,'class'=>'span6')); ?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('name',$this->contrModel)): ?>    
        <div class="control-group">
            <?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class'=>'span6')); ?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('parent_id',$this->contrModel)): ?>    
        <div class="control-group">
            <?php echo $form->labelEx($model,'parent_name',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'parent_name',array('id'=>'parent_name','size'=>60,'maxlength'=>255,'class'=>'autocomplete span6','data-aid'=>'parent_id','data-href'=>Yii::app()->createUrl('/site/parentClinicJSON'))); ?>
                <?php echo $form->hiddenField($model,'parent_id',array('id'=>'parent_id','class'=>'span12')); ?>
            </div>
        </div>
        <?php endif; ?>
        <?php if(BController::visibility('category_id',$this->contrModel)): ?>    
        <div class="control-group">
            <?php echo $form->labelEx($model,'category_name',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model,'category_name',array('id'=>'category_name','size'=>60,'maxlength'=>255,'class'=>'autocomplete span6','data-aid'=>'category_id','data-href'=>Yii::app()->createUrl('/site/categoryJSON'))); ?>
                <?php echo $form->hiddenField($model,'category_id',array('id'=>'category_id','class'=>'span12 AdminCategoryClinic_category_id')); ?>
                <div class="error"></div>
            </div>
        </div>
        <?php endif; ?>
        <div class="form-actions">
            <div class="span3">
                <?php if(!empty($model->id)): ?>
                <?php echo CHtml::submitButton("Сохранить",array('data-id'=>$model->id,'id'=>'update-btn','class'=>'btn btn-large btn-primary')); ?>
                <?php else: ?>
                <?php echo CHtml::submitButton("Создать",array('id'=>'create-btn','class'=>'btn btn-large btn-primary')); ?>
                <?php endif; ?>
            </div>
        </div>
    <?php $this->endWidget(); ?>
</div>