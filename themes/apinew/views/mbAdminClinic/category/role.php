<?php $this->renderPartial('navbar');?>
<div class="row-fluid">
    <div id="create">
        <?php echo CHtml::beginForm(); ?>
            <div id='ajax-uid'>
                <?php $this->renderPartial('//layouts/_role',array('data'=>$data));?>
            </div>
            <div class="control-group">
                <?php echo CHtml::label('Пользователь','uid',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo CHtml::dropDownList('uid', $uid, array(""=>"") + CHtml::listData(AdminUser::model()->findAll(), 'uid', 'username'),array('data-url'=>Yii::app()->createUrl('/'.Yii::app()->controller->id.'/roleAjax'),'class' => 'span6','id'=>'uid')); ?>
                    <div class="error"></div>
                </div>
            </div>        
            <div class="form-actions">
                <?php echo CHtml::submitButton("Сохранить",array("class"=>"btn btn-info"));?>
            </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("body").on('change','#uid',function(){
            var url = $(this).attr("data-url");
            var user = $(this).val();
            $.ajax({
                url: url,
                data: {uid : user},
                success: function(r) {
                    if (r) {
                        $("#ajax-uid").html(r);
                    } else {
                        $("#ajax-uid").html('');
                    }
                }
            });
        });
    });
</script>