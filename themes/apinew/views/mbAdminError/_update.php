<?php if(empty($type)): ?>
<tr id="tr-item-<?php echo $model->id; ?>" class="edit" data-id="<?php echo $model->id; ?>">
<?php endif; ?>
    <td>
        <?php echo $model->id; ?>
    </td>
    <td>
        <?php echo !empty($model->startparam1)?$model->startparam1:''; ?>
    </td>
    <td>
        <?php echo !empty($model->startparam2)?$model->startparam2:''; ?>
    </td>
    <td>
        <?php echo !empty($model->mp3)?$model->mp3:''; ?>
    </td>
    <td class="ico-view">
        <?php echo CHtml::link('',array('/'.Yii::app()->controller->id.'/view','id'=>$model->id),array('data-id'=>$model->id)); ?>
    </td>
<?php if(empty($type)): ?>
</tr>
<?php endif; ?>