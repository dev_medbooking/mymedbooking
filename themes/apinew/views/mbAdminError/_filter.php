<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id', null, array('class' => 'csorting')); ?></td>
        <td class="context<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('context', null, array('class' => 'csorting')); ?></td>
        <td class="create_time<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('create_time', null, array('class' => 'csorting')); ?></td>
        <td class="view"></td>
    </tr>
    <tr class="filters">
        <td><?php echo CHtml::activeTextField($model, 'id', array('class' => 'search-single', 'id' => 'filter_id')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'context', array('class' => 'search', 'id' => 'filter_context')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'create_time', array('class' => 'search', 'id' => 'filter_create_time')); ?></td>
        <td></td>
    </tr>
</thead>