<?php $this->renderPartial('navbar'); ?>
<div class="row-fluid">
    <div class="span12">
        <?php $this->widget('zii.widgets.CDetailView', array(
            'data' => $model,
            'htmlOptions' => array('class' => 'table table-striped table-bordered table-hover')
        )); ?>
    </div>
</div>