<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id', null, array('class' => 'csorting')); ?></td>
        <td class="startparam1<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('startparam1', null, array('class' => 'csorting')); ?></td>
        <td class="startparam2<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('startparam2', null, array('class' => 'csorting')); ?></td>
        <td class="mp3<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('mp3', null, array('class' => 'csorting')); ?></td>
        <td class="create_time<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('create_time', null, array('class' => 'csorting')); ?></td>
        <td class="view"></td>
    </tr>
    <tr class="filters">
        <td><?php echo CHtml::activeTextField($model, 'id', array('class' => 'search-single', 'id' => 'filter_id')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'startparam1', array('class' => 'search', 'id' => 'filter_startparam1')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'startparam2', array('class' => 'search', 'id' => 'filter_startparam2')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'mp3', array('class' => 'search', 'id' => 'filter_mp3')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'create_time', array('class' => 'search', 'id' => 'filter_create_time')); ?></td>
        <td></td>
    </tr>
</thead>