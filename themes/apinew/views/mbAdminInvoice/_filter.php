<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id', null, array('class' => 'csorting')); ?></td>
        <td class="id_call<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id_call', null, array('class' => 'csorting')); ?></td>
        <td class="id_com<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id_com', null, array('class' => 'csorting')); ?></td>
        <td class="date<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('date', null, array('class' => 'csorting')); ?></td>
        <td class="time<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('time', null, array('class' => 'csorting')); ?></td>
        <td class="during<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('during', null, array('class' => 'csorting')); ?></td>
        <td class="calledid<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('calledid', null, array('class' => 'csorting')); ?></td>
        <td class="callerid1<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('callerid1', null, array('class' => 'csorting')); ?></td>
        <td class="callerid2<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('callerid2', null, array('class' => 'csorting')); ?></td>
        <td class="direction<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('direction', null, array('class' => 'csorting')); ?></td>
        <td class="create_time<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('create_time', null, array('class' => 'csorting')); ?></td>
        <td class="view"></td>
    </tr>
    <tr class="filters">
        <td><?php echo CHtml::activeTextField($model, 'id', array('class' => 'search-single', 'id' => 'filter_id')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'id_call', array('class' => 'search-single', 'id' => 'filter_id_call')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'id_com', array('class' => 'search-single', 'id' => 'filter_id_com')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'date', array('class' => 'search-single', 'id' => 'filter_date')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'time', array('class' => 'search-single', 'id' => 'filter_time')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'during', array('class' => 'search-single', 'id' => 'filter_during')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'calledid', array('class' => 'search-single', 'id' => 'filter_calledid')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'callerid1', array('class' => 'search-single', 'id' => 'filter_callerid1')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'callerid2', array('class' => 'search-single', 'id' => 'filter_callerid2')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'direction', array('class' => 'search-single', 'id' => 'filter_direction')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'create_time', array('class' => 'search-single', 'id' => 'filter_create_time')); ?></td>
        <td></td>
    </tr>
</thead>