<?php if(empty($type)): ?>
<tr id="tr-item-<?php echo $model->id; ?>" class="edit" data-id="<?php echo $model->id; ?>">
<?php endif; ?>
    <td>
        <?php echo $model->id; ?>
    </td>
    <td>
        <?php echo !empty($model->id_call)?$model->id_call:''; ?>
    </td>
    <td>
        <?php echo !empty($model->id_com)?$model->id_com:''; ?>
    </td>
    <td>
        <?php echo !empty($model->date)?$model->date:''; ?>
    </td>
    <td>
        <?php echo !empty($model->time)?$model->time:''; ?>
    </td>
    <td>
        <?php echo !empty($model->during)?$model->during:''; ?>
    </td>
    <td>
        <?php echo !empty($model->calledid)?$model->calledid:''; ?>
    </td>
    <td>
        <?php echo !empty($model->callerid1)?$model->callerid1:''; ?>
    </td>
    <td>
        <?php echo !empty($model->callerid2)?$model->callerid2:''; ?>
    </td>
    <td>
        <?php echo !empty($model->direction)?$model->direction:''; ?>
    </td>
    <td>
        <?php echo !empty($model->create_time)?$model->create_time:''; ?>
    </td>
    <td class="ico-view">
        <?php echo CHtml::link('',array('/'.Yii::app()->controller->id.'/view','id'=>$model->id),array('data-id'=>$model->id)); ?>
    </td>
<?php if(empty($type)): ?>
</tr>
<?php endif; ?>