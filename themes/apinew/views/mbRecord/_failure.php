<a href="<?php echo Yii::app()->createUrl('mbRecord/admin',array('status'=>1));?>">Таблица</a>
    <b>Общее количевство:<?php echo $count;?></b>
    <b><?php echo (!empty($user->nameperson)?'Автор: '.$user->nameperson:(!empty($user->email)?'Автор:'.$user->email:''))?></b>
    <table class="records" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
        <thead>
            <tr>
                <th style="border:1px solid #dfdfdf;">Цвет</th>
                <th style="border:1px solid #dfdfdf;">Причина</th>
                <th style="border:1px solid #dfdfdf;">Количество</th>
                <th style="border:1px solid #dfdfdf;">Процент</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($model as $key=>$value):?>
            <tr>
                <th style="border:1px solid #dfdfdf;"><div style="background-color:<?php echo MbController::Color1($value->reason);?>; height:20px;"></div></th>
                <th style="border:1px solid #dfdfdf;"><?php echo !empty($value->reason) ? $value->reason : 'Причина не известна'; ?></th>
                <th style="border:1px solid #dfdfdf;"><?php echo !empty($value->qty) ? $value->qty : ''; ?></th>
                <th style="border:1px solid #dfdfdf;"><?php echo MbController::Percent($count,$value->qty);?> %</th>
            </tr> 
            <?php endforeach;?>
        </tbody>
    </table>
<input type="hidden" name="report" value="failure">
<form action="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array('host'=>(!empty($_GET['host'])?$_GET['host']:''),'status'=>(!empty($_GET['status'])?$_GET['status']:''))); ?>" class="failure-form">
    <input type="hidden" value="" name="time1"/>
    <input type="hidden" value="" name="time2"/>
</form>
<?php if (!empty($model)): ?>
    <canvas id="myChart" height="300" width="800" style="width: 800px; height: 300px;"></canvas>
    <script>
        var data = [
            <?php foreach ($model as $key=>$value):?>
            {
                value: <?php echo !empty($value->qty)?$value->qty:'';?>,
                color: "<?php echo MbController::Color($value->reason);?>",
                highlight: "<?php echo MbController::Color1($value->reason);?>",
                label: "<?php echo !empty($value->reason)?$value->reason:'Причина не известна';?>"
            },
            <?php endforeach;?>
        ];
        var options = {
            segmentShowStroke: true,
            segmentStrokeColor: "#fff",
            segmentStrokeWidth: 2,
            percentageInnerCutout: 50,
            animationSteps: 100,
            animationEasing: "easeOutBounce",
            animateRotate: true,
            animateScale: false,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        var ctx = $("#myChart").get(0).getContext("2d");
        var myDoughnutChart = new Chart(ctx).Doughnut(data, options);
    </script>
<?php endif; ?>