<form class="exel_form">
    <span style="border:1px dotted black;padding:5px;margin-right:5px;" >
        <label>ID</label>
        <input type="checkbox"  checked disabled/>
        <input name="ap.id" type="hidden" value="on">
    </span>

    <span style="border:1px dotted black;padding:5px;margin-right:5px">
       <label>Дата создания</label>
        <input type="checkbox" name="ap.create_time" />
    </span>

    <span style="border:1px dotted black;padding:5px;margin-right:5px">
        <label>На время</label>
        <input type="checkbox" name="ap.reception_time" />
    </span>

    <span style="border:1px dotted black;padding:5px;margin-right:5px">
        <label>Имя</label>
        <input type="checkbox" name="ap.name" />
    </span>

    <span style="border:1px dotted black;padding:5px;margin-right:5px">
        <label>Телефон</label>
        <input type="checkbox" name="ap.telephone" />
    </span>

    <span style="border:1px dotted black;padding:5px;margin-right:5px">
        <label>Клиника</label>
        <input type="checkbox" name="ap.clinic_name" />
    </span>

    <span style="border:1px dotted black;padding:5px;margin-right:5px">
        <label>Доктор</label>
        <input type="checkbox" name="ap.doctor_name" />
    </span>

    <span style="border:1px dotted black;padding:5px;margin-right:5px">
        <label>Услуга</label>
        <input type="checkbox" name="services" />
    </span>

    <span style="border:1px dotted black;padding:5px;margin-right:5px">
        <label>Примечание</label>
        <input type="checkbox" name="ap.note" />
    </span>

    <span style="border:1px dotted black;padding:5px;margin-right:5px">
        <label>Статус</label>
        <input type="checkbox" name="ap.status" />
    </span>

    <?php if(Yii::app()->controller->action->id=='expensiveServices'): ?>
	    <input type="hidden" name="ap.expensive_service" />
    <?php endif; ?>
    <?php if(Yii::app()->controller->action->id=='partnersList'): ?>
	    <input type="hidden" name="ident_id" value="0"/>
    <?php endif; ?>
    <button class="import_exel" data-href="<?php echo Yii::app()->createUrl('mbExel/record');?>">Скачать</button>
</form>
<br>
