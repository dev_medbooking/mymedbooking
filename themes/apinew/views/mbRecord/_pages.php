<?php
$this->widget('CLinkPager',
    array(
        'maxButtonCount' => 10,
        'cssFile' => false,
        'htmlOptions' => array('class' => 'paginator iblock'),
        'pages' => $pages,
        'header' => '',
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => '&#8250;',
        'prevPageLabel' => '&#8249;'));
?>
<a href="<?php echo Yii::app()->createUrl(
    Yii::app()->controller->getId() . '/' . Yii::app()->controller->getAction()->getId(),
    array_merge($_REQUEST, ['noPages' => 1])) ?>">Показать все</a>
<?php if (!empty($_REQUEST['noPages'])): ?>
    <?php unset($_REQUEST['noPages']) ?>
    <a href="<?php echo Yii::app()->createUrl(
        Yii::app()->controller->getId() . '/' . Yii::app()->controller->getAction()->getId(),
        $_REQUEST
    ) ?>">Вернуться в режим пагинации</a>
<?php endif ?>
