<div class="order-container clearfix">
   <div class="ajax-order-container clearfix">
        <?php if(!empty($dataRecord)): ?>
        <div class="top-order-box">
            <h3>Заявки — <?php echo $_GET['id']; ?></h3>
        </div>
        <div class="order-boxes clearfix">
            <table class="history_order" cellspacing="0">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Время создания</td>
                        <td>Статус</td>
                        <td>ФИО</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataRecord as $value): ?>
                    <tr>
                        <td><?php echo CHtml::link($value->id,array('/mbRecord/view','id'=>$value->id)); ?></td>
                        <td><?php echo $value->create_time?></td>
                        <td><?php echo $value->getStatusName()?></td>
                        <td><?php echo $value->name?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <?php if(!empty($value->apisms_ar_s)): ?>
        <?php foreach ($dataRecord as $value): ?>
        <table class="history_order history_order_msg" cellspacing="0">
            <thead>
                <tr>
                    <td width="20%">Время создания</td>
                    <td>Сообщение</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($value->apisms_ar_s as $v): ?>
                <tr>
                    <td><?php echo $value->create_time?></td>
                    <td><?php echo $v->message?></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <?php endforeach;?>
        <?php endif; ?>
        <?php endif; ?>
        <?php if(!empty($dataCall)): ?>
        <div class="top-order-box">
            <h3 class="title-order">ОС — <?php echo $_GET['id']; ?></h3>
        </div>
        <div class="order-boxes clearfix">
            <table class="" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
                <thead>
                    <tr>
                        <td style="border:1px solid #dfdfdf;">ID</td>
                        <td style="border:1px solid #dfdfdf;">Время создания</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataCall as $value): ?>
                    <tr>
                        <td style="border:1px solid #dfdfdf;"><?php echo CHtml::link($value->id,array('/mbCall/view','id'=>$value->id)); ?></td>
                        <td style="border:1px solid #dfdfdf;"><?php echo $value->create_time?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <?php endif; ?>
        <?php if(!empty($dataInvoice)): ?>
        <div class="top-order-box">
            <h3 class="title-order">Звонки — <?php echo $_GET['id']; ?></h3>
        </div>
        <div class="order-boxes clearfix">
            <table class="" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
                <thead>
                    <tr>
                        <td style="border:1px solid #dfdfdf;">ID</td>
                        <td style="border:1px solid #dfdfdf;">Время создания</td>
                        <td style="border:1px solid #dfdfdf;">Время звонка</td>
                        <td style="border:1px solid #dfdfdf;">Продолжительность</td>
                        <td style="border:1px solid #dfdfdf;">ID CALL</td>
                        <td style="border:1px solid #dfdfdf;">Оператор</td>
                        <td style="border:1px solid #dfdfdf;">Прослушать</td>
                        <td style="border:1px solid #dfdfdf;">Направление</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataInvoice as $value): ?>
                    <tr>
                        <td style="border:1px solid #dfdfdf;"><?php echo CHtml::link($value->id,array('/adminInvoice/view','id'=>$value->id)); ?></td>
                        <td style="border:1px solid #dfdfdf;"><?php echo $value->create_time;?></td>
                        <td style="border:1px solid #dfdfdf;"><?php echo !empty($value->dt)?$value->dt:'n/a';?></td>
                        <td style="border:1px solid #dfdfdf;"><?php echo $value->during;?></td>
                        <td style="border:1px solid #dfdfdf;"><?php echo !empty($value->id_call)?$value->id_call:'n/a';?></td>
                        <td style="border:1px solid #dfdfdf;"><?php echo !empty($value->callerid2)?$value->callerid2:'n/a';?></td>
                        <td style="border:1px solid #dfdfdf;"><?php echo $value->getMp3()?CHtml::link('Прослушать',$value->getMp3(),array('target'=>'popup','onclick'=>"window.open('".$value->getMp3()."','Прослушивание #".$value->id."','width=350,height=50'); return false;")):'&mdash;';?></td>
                        <td style="border:1px solid #dfdfdf;"><?php echo $value->direction=='cdIncoming'?'Входящий':'Исходящий';?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>