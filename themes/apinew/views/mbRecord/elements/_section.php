<?php if(Yii::app()->controller->id=='mbLaw'&&(Yii::app()->controller->action->id=='admin'||Yii::app()->controller->action->id=='price')): ?>
<div class="content-top-menu-section clearfix">
    <div class="calendar-box">
        <span class="calendar"></span>
    </div>
</div>
<?php endif;?>
<?php if(Yii::app()->controller->id=='mbRecord'): ?>
<div class="content-top-menu-section clearfix">
    <?php if(Yii::app()->controller->action->id=='report'):?>
        <?php if(Yii::app()->user->checkAccess('Toperator')):?>
            <div class="calendar-box">
                <span class="calendar"></span>
            </div>
        <?php endif;?>
    <?php else:?>
            <div class="calendar-box">
                <span class="calendar"></span>
            </div>
    <?php endif;?>
    <a class="restart" href="<?php echo Yii::app()->createUrl('mbRecord/'.Yii::app()->controller->action->id,array('status'=>(!empty($_GET['status'])?$_GET['status']:'')));?>">Сбросить</a>
    <?php if(Yii::app()->controller->action->id!='report'): ?>
    <ul class="items-project clearfix">
        <li class="medbooking clearfix <?php echo isset($_GET['host']) && $_GET['host']==1?'active':'';?>">
            <a href="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array('host'=>1,'status'=>(!empty($_GET['status'])?$_GET['status']:''),'author'=>(!empty($_GET['author'])?$_GET['author']:''))); ?>">medbooking</a>
            <?php if(Yii::app()->controller->action->id=='statistic'): ?>
            <span><?php echo $this->statusCountHost('statistic',1); ?></span>
            <?php else: ?>
                <?php if(!empty($_GET['status'])): ?>
                <span><?php echo $this->statusCountHost($_GET['status'],1); ?></span>
                <?php else: ?>
                <span><?php echo $this->statusCountHost(0,1); ?></span>
                <?php endif; ?>
            <?php endif; ?>
        </li>
        <li class="timetovisit clearfix <?php echo isset($_GET['host']) && $_GET['host']==2?'active':'';?>">
            <a href="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array('host'=>2,'status'=>(!empty($_GET['status'])?$_GET['status']:''),'author'=>(!empty($_GET['author'])?$_GET['author']:''))); ?>">timetovisit</a>
            <?php if(Yii::app()->controller->action->id=='statistic'): ?>
            <span><?php echo $this->statusCountHost('statistic',2); ?></span>
            <?php else: ?>
                <?php if(!empty($_GET['status'])): ?>
                <span><?php echo $this->statusCountHost($_GET['status'],2); ?></span>
                <?php else: ?>
                <span><?php echo $this->statusCountHost(0,2); ?></span>
                <?php endif; ?>
            <?php endif; ?>
        </li>
        <li class="testpuls clearfix <?php echo isset($_GET['host']) && $_GET['host']==3?'active':'';?>">
            <a href="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array('host'=>3,'status'=>(!empty($_GET['status'])?$_GET['status']:''),'author'=>(!empty($_GET['author'])?$_GET['author']:''))); ?>">testpuls</a>
            <?php if(Yii::app()->controller->action->id=='statistic'): ?>
            <span><?php echo $this->statusCountHost('statistic',3); ?></span>
            <?php else: ?>
                <?php if(!empty($_GET['status'])): ?>
                <span><?php echo $this->statusCountHost($_GET['status'],3); ?></span>
                <?php else: ?>
                <span><?php echo $this->statusCountHost(0,3); ?></span>
                <?php endif; ?>
            <?php endif; ?>
        </li>
        <li class="fromed clearfix <?php echo isset($_GET['host']) && $_GET['host']==4?'active':'';?>">
            <a href="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array('host'=>4,'status'=>(!empty($_GET['status'])?$_GET['status']:''),'author'=>(!empty($_GET['author'])?$_GET['author']:''))); ?>">fromed</a>
            <?php if(Yii::app()->controller->action->id=='statistic' || Yii::app()->controller->action->id=='statistic2'): ?>
            <span><?php echo $this->statusCountHost('statistic',4); ?></span>
            <?php else: ?>
                <?php if(!empty($_GET['status'])): ?>
                <span><?php echo $this->statusCountHost($_GET['status'],4); ?></span>
                <?php else: ?>
                <span><?php echo $this->statusCountHost(0,4); ?></span>
                <?php endif; ?>
            <?php endif; ?>
        </li>
        <li class="child clearfix <?php echo isset($_GET['host']) && $_GET['host']==9?'active':'';?>">
            <a href="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array('host'=>9,'status'=>(!empty($_GET['status'])?$_GET['status']:''),'author'=>(!empty($_GET['author'])?$_GET['author']:''))); ?>">child</a>
            <?php if(Yii::app()->controller->action->id=='statistic' || Yii::app()->controller->action->id=='statistic2'): ?>
            <span><?php echo $this->statusCountHost('statistic',9); ?></span>
            <?php else: ?>
                <?php if(!empty($_GET['status'])): ?>
                <span><?php echo $this->statusCountHost($_GET['status'],9); ?></span>
                <?php else: ?>
                <span><?php echo $this->statusCountHost(0,9); ?></span>
                <?php endif; ?>
            <?php endif; ?>
        </li>
        <li class="diagnostic clearfix <?php echo isset($_GET['host']) && $_GET['host']==8?'active':'';?>">
            <a href="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array('host'=>8,'status'=>(!empty($_GET['status'])?$_GET['status']:''),'author'=>(!empty($_GET['author'])?$_GET['author']:''))); ?>">diagnostic</a>
            <?php if(Yii::app()->controller->action->id=='statistic' || Yii::app()->controller->action->id=='statistic2'): ?>
            <span><?php echo $this->statusCountHost('statistic',8); ?></span>
            <?php else: ?>
                <?php if(!empty($_GET['status'])): ?>
                <span><?php echo $this->statusCountHost($_GET['status'],8); ?></span>
                <?php else: ?>
                <span><?php echo $this->statusCountHost(0,8); ?></span>
                <?php endif; ?>
            <?php endif; ?>
        </li>
    </ul>
    <strong class="restart2">Сбросить</strong>
    <?php endif; ?>
    <?php if(Yii::app()->controller->action->id!='statistic2'&&Yii::app()->controller->action->id!='statistic'&&Yii::app()->controller->action->id!='report'&&Yii::app()->controller->action->id!='diagramms'): ?>
    <div class="add-application">
        <?php echo CHtml::link('Добавить заявку',array('create')); ?>
    </div>
    <?php if(Yii::app()->user->checkAccess('Toperator') || Yii::app()->user->checkAccess('Operator') || Yii::app()->user->checkAccess('NCallcenter') || Yii::app()->user->checkAccess('NDasha') ): ?>
    <div class="add-application">
    <?php echo CHtml::link('Автодозвон',array('autodial')); ?>
    </div>
    <?php endif; ?>
    <ul class="content-menu clearfix">
        <li class="house" data-href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('host'=>(!empty($_GET['host'])?$_GET['host']:''),'status'=>(!empty($_GET['status'])?$_GET['status']:''))); ?>"></li>
        <li class="call" data-href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('host'=>(!empty($_GET['host'])?$_GET['host']:''),'status'=>(!empty($_GET['status'])?$_GET['status']:''))); ?>"></li>
        <li class="application-site" data-href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('host'=>(!empty($_GET['host'])?$_GET['host']:''),'status'=>(!empty($_GET['status'])?$_GET['status']:''))); ?>"></li>
        <li class="feedback" data-href="<?php echo Yii::app()->createUrl('/mbRecord/admin',array('host'=>(!empty($_GET['host'])?$_GET['host']:''),'status'=>(!empty($_GET['status'])?$_GET['status']:''))); ?>"></li>
    </ul>
    <?php endif; ?>
</div>
<?php endif; ?>