<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script src="<?=Yii::app()->theme->baseUrl;?>/js/ZeroClipboard.js"></script>
<script src="/themes/apinew/js/copy_button.js"></script>
<script src="/themes/apinew/js/jquery.datetimepicker.full.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<link href="/themes/apinew/css/jquery.datetimepicker.css" rel="stylesheet" />
<?php if(Yii::app()->user->checkAccess('NUser')||Yii::app()->user->checkAccess('NCallcenter')||Yii::app()->user->checkAccess('NDasha')): ?>
    <script type="text/javascript">
        $(function(){
            $("#phone").mask("+7(999)999-9999",{'placeholder':'_'});});$(function(){$(".chosen-default>span").text("Выбрать");
            $(document).on('change', "#phone, #phone_record, #MbRecord_source", function(){
                $("#MbRecord_ident_id").val(0);
            });
        });
    </script>
    <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
    <?php if(Yii::app()->user->checkAccess('Administrator')): ?>
        <?php $this->renderPartial('//mbRecord/_accounter/create',array('model'=>$model)); ?>
    <?php elseif(Yii::app()->user->checkAccess('Toperator')): ?>
        <?php $this->renderPartial('//mbRecord/_team_operator/create',array('model'=>$model)); ?>
    <?php elseif(Yii::app()->user->checkAccess('NCallcenter')): ?>
        <?php $this->renderPartial('//mbRecord/_operator/create',array('model'=>$model)); ?>
    <?php elseif(Yii::app()->user->checkAccess('NDasha')): ?>
        <?php $this->renderPartial('//mbRecord/_accounter/create',array('model'=>$model)); ?>
    <?php elseif(Yii::app()->user->checkAccess('NUser')): ?>
        <?php $this->renderPartial('//mbRecord/_superadmin/create',array('model'=>$model)); ?>
    <?php endif; ?>
    <div id="ajax-block"></div>
    <script type="text/javascript">
        $('select').select2();
        function ajaxTelephone(tel){
            $.ajax({
                type: 'get',
                url: '/mbRecord/telephoneAjax/id/' + tel,
                success: function(r) {
                    if (r) {
                        $("#ajax-block").html(r);
                    } else {
                        $("#ajax-block").html('');
                    }
                }
            });
        }
        $(document).ready(function(){
            var tel = $("#phone").val();
            if(tel){
                ajaxTelephone(tel);
            }
            $("body").on("change","#phone, #phone_record",function(){
                var tel = $(this).val();
                if(tel){
                    ajaxTelephone(tel);
                }
                return;
            });
        });
        $(function() {
            $("#doctor_name_auto").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "http://" + $("#domain_id option:selected").val() + "/check/doctorJSON",
                        dataType: "jsonp",
                        data: {
                            style: "full",
                            maxRows: 12,
                            term: request.term
                        },
                        success: function(data) {
                            $("#doctor_name_auto_id").val('');
                            response($.map(data, function(item) {
                                return {
                                    label: item.title,
                                    value: item.title,
                                    id: item.id
                                }
                            }));
                        }
                    });
                },
                minLength: 2,
                select: function(event,ui) {
                    if(ui.item){
                        $("#doctor_name_auto_id").val(ui.item.id);
                    }
                }
            });
            $("#clinic_name_auto").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "http://" + $("#domain_id option:selected").val() + "/check/clinicJSON",
                        dataType: "jsonp",
                        data: {
                            style: "full",
                            maxRows: 12,
                            term: request.term,
                            user:<?php echo (Yii::app()->user->checkAccess('NDasha'))?1:0;?>,
                        },
                        success: function(data) {
                            response($.map(data, function(item) {
                                return {
                                    label: item.title,
                                    value: item.title,
                                    id: item.id,
                                    none_status: item.none_status,
                                    fio: item.fio
                                }
                            }));
                        }
                    });
                },
                minLength: 2,
                select: function(event,ui) {
                    if(ui.item){
                        $("#clinic_name_auto_id").val(ui.item.id);
                        if (-1 < ui.item.value.toLowerCase().indexOf('медси')) {
                            if ($("#comment_for_none_date").length == 0)
                                $("#clinic_name_auto").parents(".row").after("<div class='row clearfix'><label for='comment_for_none_date'>Коммент</label><input id='comment_for_none_date' name='MbRecord[comment_for_none_date]' type='text' value=''></div>");
                        } else {
                            $("#comment_for_none_date").parents(".row").remove();
                        }
                    }
                }
            });
            $("#service_auto").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "http://a.medbooking.com/check/servicesJSON",
                        dataType: "jsonp",
                        data: {
                            style: "full",
                            maxRows: 12,
                            term: request.term,
                            user:<?php echo (Yii::app()->user->checkAccess('NDasha'))?1:0;?>,
                        },
                        success: function(data) {
                            response($.map(data, function(item) {
                                return {
                                    label: item.title,
                                    value: item.title,
                                    id: item.id,
                                    type: item.type
                                }
                            }));
                        }
                    });
                },
                minLength: 2,
                select: function(event,ui) {
                    if(ui.item){
                        $("#service_auto_id").val(ui.item.id);
                        $("#MbRecord_record_type").val(ui.item.type);
                    }
                }
            });
        });
    </script>
<?php endif; ?>