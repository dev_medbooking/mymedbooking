<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/table2.js', CClientScript::POS_END); ?>
<?php $this->renderPartial('//mbRecord/exel/index', array()); ?>
<div class="hidden-block" style="display: none"></div>
<!--<form action="/mbRecord/getStatusesExcel" class="form ">-->
<!--    <h2>Получить статусы</h2><br>-->
<!--    <label for="date">За 48 часов, начиная с </label><br>-->
<!--    <input required="required" type="date" name="date" class=""><br>-->
<!--    <label for="excel_phones">Телефоны</label><br>-->
<!--    <textarea required="required" style="100px" name="phones" id="excel_phones" placeholder="Телефоны заявок через пробел или каждый телефон с новой строки "></textarea><br>-->
<!--    <input type="submit" value="ok"><br><br>-->
<!--</form>-->
<form
    action="<?php echo Yii::app()->createUrl('/mbRecord/' . Yii::app()->controller->action->id, array('status' => (!empty($status) ? $status : ''), 'host' => (!empty($host) ? $host : ''))); ?>"
    method="POST" class="hidden-form">
    <div class="control_block_row">
        <?php if (Yii::app()->user->checkAccess("Team") || Yii::app()->user->checkAccess("Toperator") || Yii::app()->user->checkAccess("Taccounter") || Yii::app()->user->checkAccess("Operator")): ?>
            <?php $this->renderPartial('elements/_links', array('data' => $data)); ?>
        <?php endif; ?>
        <?php if (empty($_GET['status'])): ?>
            <ul class="waiting_list">
                <li><?php echo CHtml::link('Лист ожиданий (' . $this->statusCount(7) . ')', array('/mbRecord/admin', 'status' => 7)); ?></li>
            </ul>
        <?php elseif ($_GET['status'] == 7): ?>
            <ul class="waiting_list">
                <li class="active"><?php echo CHtml::link('Лист ожиданий (' . $this->statusCount(7) . ')', array('/mbRecord/admin', 'status' => 7)); ?></li>
            </ul>
        <?php endif; ?>
        <?php if (Yii::app()->user->checkAccess('NDash')): ?>
            <div class="row row_cart clearfix">
                <span class="deleted-item">Повторно</span>

                <div
                    class="select_styled"><?php echo CHtml::dropDownList('rerecord', (!empty($_GET['rerecord']) ? $_GET['rerecord'] : ''), array('' => '------') + array(1 => "Повторно", 2 => "Уникальный")); ?>
                    <span></span></div>
            </div>
        <?php endif; ?>
        <?php if (Yii::app()->user->checkAccess('Toperator')): ?>
            <div class="row row_cart clearfix">
                <span class="deleted-item">Корзина...</span>
                <?php echo CHtml::checkBox('deleted', (!empty($_GET['deleted']) ? true : false), array('value' => 1)); ?>
            </div>
            <div class="row row_flag clearfix">
                <span class="flag-title">Выбор флага...</span>

                <div
                    class="select_styled"><?php echo CHtml::dropDownList('flag_id', (!empty($_GET['flag_id']) ? $_GET['flag_id'] : ''), array('' => 'Без флага') + CHtml::listData(MbFlag::model()->findAll(), 'id', 'title')); ?>
                    <span></span></div>
            </div>
        <?php endif; ?>

        <span class="control-submit" data-href="">Применить</span>
        <?php if (Yii::app()->user->checkAccess('Toperator') OR in_array(Yii::app()->user->id, array(7829, 4645, 7842))): ?>
            <a class="js-excel-export-call" data-href="<?= Yii::app()->createUrl('mbRecord/excelExportCall'); ?>"
               target="_blank">Заявки из коллтача</a>
            <a class="js-excel-export-call" data-href="<?= Yii::app()->createUrl('mbRecord/excelMissedCalls'); ?>"
               target="_blank">Звонки без заявок</a>
            <a class="js-excel-export-call" data-href="<?= Yii::app()->createUrl('mbRecord/excelClinicDisagreed'); ?>" target="_blank">Не согласились</a>
        <?php endif; ?>
    </div>
    <div class="summary" id="summary">
        <?php $this->renderPartial('//layouts/_admin_count', array('dataCount' => $dataCount, 'count' => $count)); ?>
    </div>

    <div class='table-api-main_wrapper'>
        <table class="table-api-main">
            <?php $this->renderPartial('_filter', array('sort' => $sort, 'model' => $model, 'status' => '1')); ?>
            <tbody id="admin-tbody">
            <?php $this->renderPartial('_admin', array('data' => $data)); ?>
            </tbody>
        </table>
    </div>
    <div class="pagination row" id="pages">
        <?php if (!empty($pages) && !empty($count)): ?>
            <?php $this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count)); ?>
        <?php endif; ?>

    </div>
</form>
