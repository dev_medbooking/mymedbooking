111
<div class="application-content">
	<?php if($model->isNewRecord) :?>
		<h2 class="title">Новая заявка</h2>
	<?php else :?>
		<h2 class="title">Редактирование заявки #<?php echo $model->id; ?> <?=($model->ident_id) ? "П" : "";?></h2>
	<?php endif; ?>
    <p class="remark">Поля отмеченные <i></i> — обязательны для заполнения. <a href="<?php echo Yii::app()->request->urlReferrer; ?>">Вернуться обратно</a></p>
</div>
<div id="create-block">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'application-form'),
    )); ?>
        <?php if($model->errors): ?><div class="well"><?php echo $form->errorSummary($model); ?></div><?php endif; ?>
        <div class="row clearfix">
            <label for="MbRecord_tracking_url">&nbsp;</label>
            <div class="text-name note-form"><?php echo (isset($model->tracking_url)?"<a href='".$model->tracking_url."' target=_blank>Последняя просмотренная страница</a>":"");?></div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'name',array('placeholder'=>'ФИО')); ?>
            </div>
            <div class="text-name note-form">
                <?php echo $form->textField($model,'first_name',array('placeholder'=>'Имя')); ?>
            </div>
            <div class="select-new">
                <label class="man"></label>
                <label class="woman"></label>
                <?php echo $form->dropDownList($model,'gender',array('0'=>'n/a','1'=>'мужчина','2'=>'женщина')); ?>
            </div>
            <div class="select-new select-new-last">
                <?php echo $form->dropDownList($model,'new_pacient',array(0=>'Новый',1=>'Повторный')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-phone note-form clearfix">
                <label for="phone"></label>
                <?php $this->widget('CMaskedTextField',array('model'=>$model,'attribute'=>'telephone','mask'=>'+7(999)999-9999','placeholder'=>'_','htmlOptions'=>array('class'=>'span2','id'=>'phone','placeholder'=>'Телефон','data-href'=>Yii::app()->createUrl('mbExel/phoneCheked')))); ?>
            </div>
            <div class="text-email clearfix">
                <label for="email"></label>
                <?php echo $form->textField($model,'email',array('placeholder'=>'E-mail',"id"=>"email")); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="datepicker-box">
                <div class="time1 note-form">
                    <span class="title-calendar">На указанное время:</span>
                    <label for="date"></label>
                    <?php echo $form->textField($model,'formattedRecTime',array('id'=>'timeContainer')); ?>
                </div>
                <?php if($model->status==3): ?><div class="time2">
                    <span class="title-calendar">На указанное время:</span>
                    <label for="date"></label>
                    <?php echo $form->textField($model,'formattedLeadTime',array('id'=>'timeContainer2')); ?>
                </div><?php endif; ?>
            </div>
            <?php if($model->status==3): ?><button class="rewrite_btn">Указать лида</button><?php endif; ?>
        </div>
        <div class="row clearfix">
            <label for="timeContainer">Время звонка </label>

            <div class="datepicker-box">
                <div
                    class="time1 note-form"><?php echo $form->textField($model, 'call_time', ['id'=>'callTime']); ?></div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-phone">
               <span class="title-calendar">Телефон заявки:</span>
               <label for="phone_record"></label>
               <?php $this->widget('CMaskedTextField',array('model'=>$model,'attribute'=>'phone_record','mask'=>'+7(999)999-9999','placeholder'=>'_','htmlOptions'=>array('class'=>'span2','id'=>'phone_record','placeholder'=>'Телефон'))); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="source-text">
               <span class="title-calendar">Источник заявки</span>
                <?php echo $form->textField($model,'source',array("class"=>"source")); ?>
            </div>
            <div class="select-new select-new-project note-form">
                <span class="title-calendar">Выберите проект:</span>
                <?php if($model->isNewRecord):?>
                    <select id="domain_id" name="MbRecord[domain]" class="long_select">
                        <option value="" selected="selected"></option>
                        <?php $phones = MbDomainPhone::model()->findAll();
                            foreach ($phones as $key=>$value):?>
                        <option value="<?php echo $value->domain;?>" data-phone="<?php echo $value->phone;?>"><?php echo $value->phone;?>  (<?php echo $value->domain;?> <?php echo !empty($value->domain_name)?$value->domain_name:'';?>)</option>
                        <?php endforeach;?>
                    </select>
                        
                <?php else:?>
                <?php 
                    $select = 0;
                    if(empty($model->domain_phone)){
                        if(!empty($model->domain)){
                            $select = MbDomainPhone::model()->findByAttributes(array('status'=>1,'domain'=>$model->domain));
                        }
                    }
                ?>
                <select id="domain_id" name="MbRecord[domain]" class="long_select">
                    <option value=""></option>
                    <?php $phones = MbDomainPhone::model()->findAll();
                        foreach ($phones as $key=>$value):?>
                    <option value="<?php echo $value->domain;?>" data-phone="<?php echo $value->phone;?>" <?php if($value->phone==$model->domain_phone):?>selected<?php endif;?> <?php if(!empty($select->phone)&&$value->phone==$select->phone):?>selected<?php endif;?>><?php echo $value->phone;?>  (<?php echo $value->domain;?> <?php echo !empty($value->domain_name)?$value->domain_name:'';?>)</option>
                    <?php endforeach;?>
                </select>
                <?php endif;?>
                <?php echo $form->hiddenField($model,'domain_phone',array()); ?>
            </div>
        </div>
    <div class="row clearfix">
        <label for="">Клиника</label>
        <div class="select-new js-copy-btn-enable">
            <?php echo $form->dropDownList($model, 'clinic_id', array('0' => 'Выберите клинику') + CHtml::listData(MedbookingClinic::model()->findAll(array("condition" => "nation != 1 AND clinic_city_line_id IN (1,2) AND status > 0", "order" => "title ASC")), 'id', function($clinic) { return ($clinic->status != 1) ? $clinic->title.'(мод '.$clinic->status.')' : $clinic->title;}), array()); ?>
        </div>
        <div style="float: left; margin: 9px 0 0 10px;">
            <?=$form->checkBox($model, 'clinic_propose')?>
            <span>Предложенная клиника</span><br>
            <?=$form->checkBox($model, 'clinic_disagreed')?>
            <span>Не согласились</span>
        </div>
        <div class="error clinic_error" style="display: none">Это поле обязательное для статуса "Записать"</div>
    </div>
		<div class="row clearfix">
			<label for="">Доктор, специалист</label>
			<div class="select-new  js-copy-btn-enable">
				<?php echo $form->dropDownList($model, 'doctor_id', array('0' => 'Выберите доктора') + CHtml::listData(MedbookingDoctor::model()->findAll(array("condition" => "nation != 1 AND doctor_city_id IN (1,2)", "order" => "lname ASC")), 'id', function($doctor) { return $doctor->lname." ".$doctor->fname." ".$doctor->sname.($doctor->status != 1 ? '(мод '.$doctor->status.')' : '');}), array()); ?>
			</div>
		</div>
		<div class="row clearfix">
			<label for="">Специалист <i>(если не указан доктор)</i></label>
			<select class="category_select" name="MbRecord[category_name]">
				<option value="">Выберите специализацию</option>
				<?php $this->widget('application.components.PriceSearch', array('name' => $model->category_name)); ?>
			</select>
		</div>
		<div class="row clearfix">
			<?php if (!empty($model->services)) : ?>
				<?php foreach ($model->services as $s) : ?>
					<div id="services_select_div">
						<div class="row clearfix services_select_div_inner">
							<label for="">Услуга</label>
							<select class="services_select" name="MbRecordServices[service_id][<?= $s->id; ?>]">
								<option value="0">Выберите услугу</option>
								<?php $this->widget('application.components.CategoryIndex', array('id' => $s->service_id)); ?>
							</select>
						</div>
					</div>
				<?php endforeach; ?>
			<?php else : ?>
				<div id="services_select_div">
					<div class="row clearfix services_select_div_inner">
						<label for="">Услуга</label>
						<select class="services_select" name="MbRecordServices[service_id][]">
							<option value="0">Выберите услугу</option>
							<?php $this->widget('application.components.CategoryIndex'); ?>
						</select>
					</div>
				</div>
			<?php endif; ?>
			<div style="padding: 5px;font-size: 25px;">
				<div id="plus_service" style="color:green;margin:0px" class="glyphicons glyphicons-plus">+</div>
				<div id="minus_service" style="color:red;margin-left:5px;margin-top:-2px"
				     class="glyphicons glyphicons-minus">-
				</div>
			</div>
		</div>
		<div class="row clearfix" style="display: <?=($model->none_service ? '' : 'none')?>;">
			<div>
				<label for="">Услуги нету в списке</label>
				<?php echo $form->checkBox($model, 'none_service', array()); ?>
			</div>
		</div>
		<div class="row clearfix">
			<div>
				<label for="name_none_service">Услуга вне списка</label>
				<?php echo $form->textField($model, 'name_none_service', array()); ?>
			</div>
		</div>
		<div class="row clearfix">
			<div>
				<label for="name_none_service">Вызов на дом</label>
				<?php echo $form->checkBox($model, 'home', array()); ?>
			</div>
		</div>
        <div class="row clearfix">
            <div>
                <label for="name_none_service">Детский</label>
                <?php echo $form->checkBox($model, 'child', array()); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div>
                <label for="name_none_service">С контрастом</label>
                <?php echo $form->checkBox($model, 'contrast', array()); ?>
            </div>
        </div>
        <?php /* ?><div class="row clearfix">
            <div class="source-text">
                <span class="title-calendar">Услуга</span>
                <?php echo $form->textField($model,'service',array("class"=>"source")); ?>
            </div>        
        </div><?php */ ?>  
        <div class="row clearfix">
            <span class="title-calendar">Пользовательский запрос...</span>
            <?php echo $form->textArea($model,'note',array()); ?>
            <div class="error note_error" style="display: none">Это поле обязательное для статуса "Записать"</div>
        </div>
        <div class="row clearfix">
            <span class="title-calendar">Описание...</span>
            <?php echo $form->textArea($model,'description',array()); ?>
        </div>
        <div class="row clearfix">
            <span class="title-calendar">Примечание к заявке...</span>
            <?php echo $form->textArea($model,'comment',array()); ?>
        </div>
        <div class="row row-max clearfix">
            <div class="select-new select-status note-form select-status-repeat">
                <span class="title-calendar">Повторно:</span>
                <?php echo $form->dropDownList($model,'rerecord',array('0'=>'Нет','1'=>'Повторно')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="source-text">
               <span class="title-calendar">Статус СМС...</span>
<!--                --><?php //echo $form->dropDownList($model,'sms_checked',array(0=>'Отправлять',1=>'Не отправлять')); ?>
                <?php echo $form->dropDownList($model,'sms_checked',array(0=>'Отправлять')); ?>
            </div>
        </div>        
        <div class="row row-max clearfix">
            <strong class="status">Статус заявки —</strong>
            <div class="select-new select-status note-form">
                <?php echo $form->dropDownList($model,'status',array(0=>'Создано',1=>'Отказ',3=>'Был на приеме',5=>'Записать',7=>'Лист ожидания',9=>'Пустой звонок',60=> 'Пропущенный',51=>'Недозвон', 53 =>'90 дней')); ?>
            </div>
            <div class="select-new select-failure-cause">
                <span class="title-calendar">Выберите причину отказа:</span>
                <?php echo $form->dropDownList($model,'reason',array(''=>'')+CHtml::listData(MbReason::model()->findAll("status!=1"),'title','title'),array('class'=>'span4','disabled','prompt' =>'&nbsp')); ?>
            </div>
            <span class="add-comment">Добавить комментарий</span>
            <div class="hid">
                <span class="hid-select-title">Оценка доктора</span><?php echo CHtml::activeDropDownList($model,'doctor_value',array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10),array('class'=>'span1')); ?>
                <span class="hid-select-title">Оценка внимания</span><?php echo CHtml::activeDropDownList($model,'attention_value',array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10),array('class'=>'span1')); ?>
                <span class="hid-select-title">Оценка стоимости</span><?php echo CHtml::activeDropDownList($model,'price_value',array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10),array('class'=>'span1')); ?>
                <span class="title-calendar title-textarea-hide">Отзыв для сайтов ...</span>
                <?php echo $form->textArea($model,'review',array()); ?>
            </div>
        </div>
        <?php echo $form->hiddenField($model,'tool_checked',array('id'=>'tool_checked')); ?>
        <?php echo (isset($model->tracking_session_id) && $model->isNewRecord?$form->hiddenField($model,'tracking_session_id',array('id'=>'tracking_session_id')):""); ?>
        <?php echo (isset($model->tracking_url) && $model->isNewRecord?$form->hiddenField($model,'tracking_url',array('id'=>'tracking_url')):""); ?>
        <?php echo (isset($model->tracking_phone) && $model->isNewRecord?$form->hiddenField($model,'tracking_phone',array('id'=>'tracking_phone')):""); ?>
        <div class="row row_flag clearfix">
            <span class="flag-calendar">Установка флага...</span>
            <?php echo $form->dropDownList($model,'flag_id',array(''=>'Без флага')+CHtml::listData(MbFlag::model()->findAll(),'id','title')); ?>
        </div>
        <div class="row row_flag clearfix">
            <span class="flag-calendar">Корзина...</span>
            <?php echo $form->dropDownList($model,'deleted',array('1'=>'Удалено',''=>'Не удалено')); ?>
        </div>
		<div class="row clearfix">
			<label for="">Пациент от нас</label>
			<div class="source-text"><?php echo $form->dropDownList($model, 'not_our_patient', array(0 => 'Да', 1 => 'Не от нас')); ?></div>
		</div>
        <div class="row clearfix">
            <button class="save_btn">Сохранить</button>
            <?php if(!empty($model->id)):?><a href="<?php echo Yii::app()->createUrl('mbRecord/view',array('id'=>$model->id));?>" style="float:right;">Просмотреть</a><?php endif;?>
        </div>
    <?php $this->endWidget(); ?>
</div>