<?php if(Yii::app()->user->checkAccess('Toperator')): ?>
<?php $this->renderPartial('//mbRecord/_team_operator/report_full',array('get'=>$get,'author_id'=>$author_id,'data_from'=>$data_from,'data_to'=>$data_to,'dataBody'=>$dataBody,'dataBodySite'=>$dataBodySite,'host'=>$host,'status'=>$status,'leftDateParam'=>$leftDateParam,'rightDateParam'=>$rightDateParam, 'partner' => $partner, 'domains' => $domains, 'queryDomains' => $queryDomains)); ?>
<?php elseif(Yii::app()->user->checkAccess('Operator')): ?>
<?php $this->renderPartial('//mbRecord/_team_operator/report_full_operator',array('get'=>$get,'author_id'=>$author_id,'data_from'=>$data_from,'data_to'=>$data_to,'dataBody'=>$dataBody,'dataBodySite'=>$dataBodySite,'host'=>$host,'status'=>$status,'leftDateParam'=>$leftDateParam,'rightDateParam'=>$rightDateParam)); ?>
<?php elseif(Yii::app()->user->checkAccess('Taccounter')): ?>
<?php $this->renderPartial('//mbRecord/_team_accounter/report_full',array('get'=>$get,'author_id'=>$author_id,'data_from'=>$data_from,'data_to'=>$data_to,'dataBody'=>$dataBody,'dataBodySite'=>$dataBodySite,'host'=>$host,'status'=>$status,'leftDateParam'=>$leftDateParam,'rightDateParam'=>$rightDateParam)); ?>
<?php elseif(Yii::app()->user->checkAccess('Administrator')): ?>
<?php $this->renderPartial('//mbRecord/_superadmin/report_full',array('get'=>$get,'author_id'=>$author_id,'data_from'=>$data_from,'data_to'=>$data_to,'dataBody'=>$dataBody,'dataBodySite'=>$dataBodySite,'host'=>$host,'status'=>$status,'leftDateParam'=>$leftDateParam,'rightDateParam'=>$rightDateParam)); ?>
<?php endif; ?>