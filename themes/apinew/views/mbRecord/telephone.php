<div class="order-container clearfix">
    <?php if(!empty($dataRecord)): ?>
    <div class="top-order-box">
        <h3 class="title-order">Заявки — <?php echo $_GET['id']; ?></h3>
    </div>
    <div class="order-boxes clearfix">
        <table class="" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
            <thead>
                <tr>
                    <th style="border:1px solid #dfdfdf;">ID</th>
                    <th style="border:1px solid #dfdfdf;">Время создания</th>
                    <th style="border:1px solid #dfdfdf;">Статус</th>
                    <th style="border:1px solid #dfdfdf;">ФИО</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataRecord as $value): ?>
                <tr>
                    <th style="border:1px solid #dfdfdf;"><?php echo CHtml::link($value->id,array('/mbRecord/view','id'=>$value->id)); ?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo $value->create_time?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo $value->getStatusName()?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo $value->name?></th>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <?php if(!empty($value->apisms_ar_s)): ?>
        <?php foreach ($dataRecord as $value): ?>
        <table cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
            <thead>
                <tr>
                    <th style="border:1px solid #dfdfdf;">Время создания</th>
                    <th style="border:1px solid #dfdfdf;">Сообщение</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($value->apisms_ar_s as $v): ?>
                <tr>
                    <th style="border:1px solid #dfdfdf;"><?php echo $value->create_time; ?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo $v->message; ?></th>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <?php endforeach;?>
    <?php endif; ?>
    <?php endif; ?>
    <?php if(!empty($dataCall)): ?>
    <div class="top-order-box">
        <h3 class="title-order">Обр.Связь — <?php echo $_GET['id']; ?></h3>
    </div>
    <div class="order-boxes clearfix">
        <table class="" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
            <thead>
                <tr>
                    <th style="border:1px solid #dfdfdf;">ID</th>
                    <th style="border:1px solid #dfdfdf;">Время создания</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataCall as $value): ?>
                <tr>
                    <th style="border:1px solid #dfdfdf;"><?php echo CHtml::link($value->id,array('/mbCall/view','id'=>$value->id)); ?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo $value->create_time; ?></th>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <?php endif; ?>
    <?php if(!empty($dataInvoice)): ?>
    <div class="top-order-box">
        <h3 class="title-order">Звонки — <?php echo $_GET['id']; ?></h3>
    </div>
    <div class="order-boxes clearfix">
        <table cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
            <thead>
                <tr>
                    <th style="border:1px solid #dfdfdf;">ID</th>
                    <th style="border:1px solid #dfdfdf;">Время создания</th>
                    <th style="border:1px solid #dfdfdf;">Время звонка</th>
                    <th style="border:1px solid #dfdfdf;">Продолжительность</th>
                    <th style="border:1px solid #dfdfdf;">ID CALL</th>
                    <th style="border:1px solid #dfdfdf;">Оператор</th>
                    <th style="border:1px solid #dfdfdf;">Прослушать</th>
                    <th style="border:1px solid #dfdfdf;">Направление</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataInvoice as $value): ?>
                <tr>
                    <th style="border:1px solid #dfdfdf;"><?php echo CHtml::link($value->id,array('/adminInvoice/view','id'=>$value->id)); ?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo $value->create_time;?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo !empty($value->dt)?$value->dt:'n/a';?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo $value->during;?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo !empty($value->id_call)?$value->id_call:'n/a';?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo !empty($value->callerid2)?$value->callerid2:'n/a';?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo $value->getMp3()?CHtml::link('Прослушать',$value->getMp3(),array('target'=>'popup','onclick'=>"window.open('".$value->getMp3()."','Прослушивание #".$value->id."','width=350,height=50'); return false;")):'&mdash;';?></th>
                    <th style="border:1px solid #dfdfdf;"><?php echo $value->direction=='cdIncoming'?'Входящий':'Исходящий';?></th>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <?php endif; ?>
</div>