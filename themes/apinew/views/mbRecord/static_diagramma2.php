<form action="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array()); ?>" class="failure-form">
    <input type="hidden" value="<?php echo $data_from; ?>" name="data_from"/>
    <input type="hidden" value="<?php echo $data_to; ?>" name="data_to"/>
    <input type="hidden" value="<?php echo $author_id; ?>" name="author_id"/>
</form>
<div class="diagramms_body">
    <div class="control_block_row leads">
        <?php if(Yii::app()->user->checkAccess("Toperator")):?>
            <div class="select_styled">
                <?php echo CHtml::dropDownList('report_list',$author_id,CHtml::listData(MbUser::model()->findAllByAttributes(array('apicall'=>1)),'uid','nameperson'),array('prompt'=>'Все операторы')); ?>
                <span></span>
            </div>
        <?php endif;?>
        <?php $this->renderPartial('elements/_links2'); ?>
        <b>Общее количество:<?php echo $count; ?></b>
    </div>
    <table class="records" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
        <thead>
            <tr>
                <th style="border:1px solid #dfdfdf;">Цвет</th>
                <th style="border:1px solid #dfdfdf;">Домен</th>
                <th style="border:1px solid #dfdfdf;">Количество</th>
                <th style="border:1px solid #dfdfdf;">Процент</th>
            </tr>
        </thead>
        <tbody>
            <?php echo $select_1; ?>
        </tbody>
    </table>
    <input type="hidden" name="report" value="statistic">
    <canvas id="myChart" height="300" width="800" style="width: 800px; height: 300px;"></canvas>
    <script>
        var data = [<?php echo $select_2; ?>];
        var options = {
            segmentShowStroke: true,
            segmentStrokeColor: "#fff",
            segmentStrokeWidth: 2,
            percentageInnerCutout: 50,
            animationSteps: 100,
            animationEasing: "easeOutBounce",
            animateRotate: true,
            animateScale: false,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        var ctx = $("#myChart").get(0).getContext("2d");
        var myDoughnutChart = new Chart(ctx).Doughnut(data, options);
    </script>
</div>