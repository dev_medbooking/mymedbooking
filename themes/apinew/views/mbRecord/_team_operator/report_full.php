<form action="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array()); ?>" class="failure-form">
    <input type="hidden" name="report" value="report" />
    <input type="hidden" value="<?php echo $data_from;?>" name="data-from"/>
    <input type="hidden" value="<?php echo $data_to;?>" name="data-to"/>
    <input type="hidden" value="<?php echo $author_id;?>" name="author_id"/>
	<div class="row-fluid">
		<ul class="nav nav-tabs nav_report">
			<?php foreach($domains as $domain) :?>
				<li><input type="checkbox" name="queryDomains[]" value="<?=$domain->title;?>" <?=(in_array($domain->title, $queryDomains) ? "checked" : "");?>><?=$domain->title;?></li>
			<?php endforeach;?>
			<li><input type="submit" data-href="<?=Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array());?>" value="Показать"></li>
		</ul>
	</div>
</form>
<div class="row-fluid">
    <ul class="nav nav-tabs nav_report">
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==1?' class="active"':''; ?>>
            <?php $get['long']=1; echo CHtml::link('1 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==2?' class="active"':''; ?>>
            <?php $get['long']=2; echo CHtml::link('2 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==3?' class="active"':''; ?>>
            <?php $get['long']=3; echo CHtml::link('3 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==7?' class="active"':''; ?>>
            <?php $get['long']=7; echo CHtml::link('7 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==10?' class="active"':''; ?>>
            <?php $get['long']=10; echo CHtml::link('10 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==30?' class="active"':''; ?>>
            <?php $get['long']=30; echo CHtml::link('30 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==31?' class="active"':''; ?>>
            <?php $get['long']=31; echo CHtml::link('31 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==90?' class="active"':''; ?>>
            <?php $get['long']=90; echo CHtml::link('90 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==91?' class="active"':''; ?>>
            <?php $get['long']=91; echo CHtml::link('91 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==92?' class="active"':''; ?>>
            <?php $get['long']=92; echo CHtml::link('92 дн.',$get); ?>
        </li>
            <li><div class="select_styled"><?php echo CHtml::dropDownList('report_list', $author_id, CHtml::listData(MbUser::model()->findAllByAttributes(array('apicall'=>1)), 'uid', 'nameperson'), array('prompt' => 'Все операторы')); ?><span></span></div></li>
        <li>
            <?php echo CHtml::link('Oktell',array('/mbRecord/reportOktell')); ?>
        </li>
        <li>
            <?php echo CHtml::link('Отчет',array('/mbReport/report')); ?>
        </li>     
    </ul>
</div>
<div class="row-fluid">
    <div class="span12" id="admin-admin">
        <?php if(!empty($_GET['data-from'])): ?>
        <h3>
            <?php if(!empty($_GET['data-from'])): ?>
            <?php echo $_GET['data-from']; ?>
            <?php endif; ?>
            <?php if(!empty($_GET['data-to'])): ?>
            &mdash; <?php echo $_GET['data-to']; ?>
            <?php endif; ?>
        </h3>
        <?php else: ?>
        <h3>Данные за сегодня</h3>
        <?php endif; ?>
        <?php $this->renderPartial('_report',array('dataBody'=>$dataBody)); ?>
        <?php if(!empty($_GET['data-from'])): ?>
        <h3>
            <?php if(!empty($_GET['data-from'])): ?>
            <?php echo $_GET['data-from']; ?>
            <?php endif; ?>
            <?php if(!empty($_GET['data-to'])): ?>
            &mdash; <?php echo $_GET['data-to']; ?>
            <?php endif; ?>
             - ONLINE источник
        </h3>
        <?php else: ?>
        <h3>Данные за сегодня ONLINE источник</h3>
        <?php endif; ?>
        <?php $this->renderPartial('_report',array('dataBody'=>$dataBodySite)); ?>
    </div>
</div>
<?php if(!empty($data)):?>
<canvas id="myChart" height="300" width="800" style="width: 800px; height: 300px;"></canvas>
<script>
    var data = <?php echo $data; ?>;
    var options = {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        barShowStroke : true,
        barStrokeWidth : 2,
        barValueSpacing : 5,
        barDatasetSpacing : 1,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
    };
    var ctx = $("#myChart").get(0).getContext("2d");
    var myBarChart = new Chart(ctx).Bar(data,options);
</script>
<?php endif;?>
<div class="row-fluid">
    <div class="span12">
        <p>Сравнение периодов заявок, при выборе вычитаний на [LONG] количества дней</p>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <?php if(!empty($_GET['data-from'])): ?>
        <h3>
            <?php if(!empty($_GET['data-from'])): ?>
            <?php echo $_GET['data-from']; ?>
            <?php endif; ?>
            <?php if(!empty($_GET['data-to'])): ?>
            &mdash; <?php echo $_GET['data-to']; ?>
            <?php endif; ?>
        </h3>
        <?php else: ?>
        <h3>Данные за сегодня  в сравнении с аналогичным днем прошлой недели</h3>
        <?php endif; ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span12" id="admin-admin">
        <table class="records" cellspacing="0" cellpadding="10" style="border:1px solid #dfdfdf;color:#666;font:13px Arial;line-height:1.4em;width:100%;">
            <thead>
                <tr>
                    <th style="border:1px solid #dfdfdf;">Сайт</th>
                    <th style="border:1px solid #dfdfdf;">Заявок</th>
                    <th style="border:1px solid #dfdfdf;">Записей</th>
                    <th style="border:1px solid #dfdfdf;">Отказы</th>
                    <th style="border:1px solid #dfdfdf;">Обратная</th>
                    <th style="border:1px solid #dfdfdf;">Дошедшие</th>
                </tr>
            </thead>
            <tbody>
                <?php if(empty($dataBody)||empty($dataBody['domains'])):?>
                <tr>
                    <td colspan="5" style="border:1px solid #dfdfdf;">Нет записей</td>
                </tr>
                <?php else:?>
                <?php foreach($dataBody['domains'] as $domain):?>
                <tr>
                    <td style="border:1px solid #dfdfdf;"><?php echo $domain['title']; ?></td>
                    <td style="border:1px solid #dfdfdf;">
                        <span style="color:green;"><?php echo $domain['records']['new'][0]; ?></span>&nbsp;/&nbsp;
                        <span style="color:red;"><?php echo $domain['records']['new'][1]; ?></span>&nbsp;/&nbsp;
                        &nbsp;
                        <?php if($domain['records']['new'][1]>$domain['records']['new'][0]): ?>
                        <span style="color:red;">(<?php echo ($domain['records']['new'][0]-$domain['records']['new'][1]); ?>)</span>
                        <?php elseif($domain['records']['new'][1]<$domain['records']['new'][0]): ?>
                        <span style="color:green;">(<?php echo ($domain['records']['new'][0]-$domain['records']['new'][1]); ?>)</span>
                        <?php else: ?>
                        (0)
                        <?php endif; ?>
                    </td>
                    <td style="border:1px solid #dfdfdf;">
                        <span style="color:green;"><?php echo $domain['records']['confirmed'][0]; ?></span>&nbsp;/&nbsp;
                        <span style="color:red;"><?php echo $domain['records']['confirmed'][1]; ?></span>&nbsp;/&nbsp;
                        &nbsp;
                        <?php if($domain['records']['confirmed'][1]>$domain['records']['confirmed'][0]): ?>
                        <span style="color:red;">(<?php echo ($domain['records']['confirmed'][0]-$domain['records']['confirmed'][1]); ?>)</span>
                        <?php elseif($domain['records']['confirmed'][1]<$domain['records']['confirmed'][0]): ?>
                        <span style="color:green;">(<?php echo ($domain['records']['confirmed'][0]-$domain['records']['confirmed'][1]); ?>)</span>
                        <?php else: ?>
                        (0)
                        <?php endif; ?>
                    </td>
                    <td style="border:1px solid #dfdfdf;">
                        <span style="color:green;"><?php echo $domain['records']['cancel'][0]; ?></span>&nbsp;/&nbsp;
                        <span style="color:red;"><?php echo $domain['records']['cancel'][1]; ?></span>&nbsp;/&nbsp;
                        &nbsp;
                        <?php if($domain['records']['cancel'][1]>$domain['records']['cancel'][0]): ?>
                        <span style="color:green;">(<?php echo ($domain['records']['cancel'][1]-$domain['records']['cancel'][0]); ?>)</span>
                        <?php elseif($domain['records']['cancel'][1]<$domain['records']['cancel'][0]): ?>
                        <span style="color:red;">(<?php echo ($domain['records']['cancel'][1]-$domain['records']['cancel'][0]); ?>)</span>
                        <?php else: ?>
                        (0)
                        <?php endif; ?>
                    </td>
                    <td style="border:1px solid #dfdfdf;">
                        <span style="color:green;"><?php echo $domain['records']['call'][0]; ?></span>&nbsp;/&nbsp;
                        <span style="color:red;"><?php echo $domain['records']['call'][1]; ?></span>&nbsp;/&nbsp;
                        &nbsp;
                        <?php if($domain['records']['call'][1]>$domain['records']['call'][0]): ?>
                        <span style="color:red;">(<?php echo ($domain['records']['call'][0]-$domain['records']['call'][1]); ?>)</span>
                        <?php elseif($domain['records']['call'][1]<$domain['records']['call'][0]): ?>
                        <span style="color:green;">(<?php echo ($domain['records']['call'][0]-$domain['records']['call'][1]); ?>)</span>
                        <?php else: ?>
                        (0)
                        <?php endif; ?>
                    </td>
                    <td style="border:1px solid #dfdfdf;">
                        <span style="color:green;"><?php echo $domain['records']['in'][0]; ?></span>&nbsp;/&nbsp;
                        <span style="color:red;"><?php echo $domain['records']['in'][1]; ?></span>&nbsp;/&nbsp;
                        &nbsp;
                        <?php if($domain['records']['in'][1]>$domain['records']['in'][0]): ?>
                        <span style="color:red;">(<?php echo ($domain['records']['in'][0]-$domain['records']['in'][1]); ?>)</span>
                        <?php elseif($domain['records']['in'][1]<$domain['records']['in'][0]): ?>
                        <span style="color:green;">(<?php echo ($domain['records']['in'][0]-$domain['records']['in'][1]); ?>)</span>
                        <?php else: ?>
                        (0)
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach;?>
                <tr>
                    <td style="border:1px solid #dfdfdf; font-weight: bold;">Итого:</td>
                    <td style="border:1px solid #dfdfdf; font-weight: bold;">
                        <span style="color:green;"><?php echo $dataBody['totals']['new'][0]; ?></span>&nbsp;/&nbsp;
                        <span style="color:red;"><?php echo $dataBody['totals']['new'][1]; ?></span>&nbsp;/&nbsp;
                        &nbsp;
                        <?php if($dataBody['totals']['new'][1]>$dataBody['totals']['new'][0]): ?>
                        <span style="color:red;">(<?php echo ($dataBody['totals']['new'][0]-$dataBody['totals']['new'][1]); ?>)</span>
                        <?php elseif($dataBody['totals']['new'][1]<$dataBody['totals']['new'][0]): ?>
                        <span style="color:green;">(<?php echo ($dataBody['totals']['new'][0]-$dataBody['totals']['new'][1]); ?>)</span>
                        <?php else: ?>
                        (0)
                        <?php endif; ?>
                    </td>
                    <td style="border:1px solid #dfdfdf; font-weight: bold;">
                        <span style="color:green;"><?php echo $dataBody['totals']['confirmed'][0]; ?></span>&nbsp;/&nbsp;
                        <span style="color:red;"><?php echo $dataBody['totals']['confirmed'][1]; ?></span>&nbsp;/&nbsp;
                        &nbsp;
                        <?php if($dataBody['totals']['confirmed'][1]>$dataBody['totals']['confirmed'][0]): ?>
                        <span style="color:red;">(<?php echo ($dataBody['totals']['confirmed'][0]-$dataBody['totals']['confirmed'][1]); ?>)</span>
                        <?php elseif($dataBody['totals']['confirmed'][1]<$dataBody['totals']['confirmed'][0]): ?>
                        <span style="color:green;">(<?php echo ($dataBody['totals']['confirmed'][0]-$dataBody['totals']['confirmed'][1]); ?>)</span>
                        <?php else: ?>
                        (0)
                        <?php endif; ?>
                    </td>
                    <td style="border:1px solid #dfdfdf; font-weight: bold;">
                        <span style="color:green;"><?php echo $dataBody['totals']['cancel'][0]; ?></span>&nbsp;/&nbsp;
                        <span style="color:red;"><?php echo $dataBody['totals']['cancel'][1]; ?></span>&nbsp;/&nbsp;
                        &nbsp;
                        <?php if($dataBody['totals']['cancel'][1]>$dataBody['totals']['cancel'][0]): ?>
                        <span style="color:green;">(<?php echo ($dataBody['totals']['cancel'][1]-$dataBody['totals']['cancel'][0]); ?>)</span>
                        <?php elseif($dataBody['totals']['cancel'][1]<$dataBody['totals']['cancel'][0]): ?>
                        <span style="color:red;">(<?php echo ($dataBody['totals']['cancel'][1]-$dataBody['totals']['cancel'][0]); ?>)</span>
                        <?php else: ?>
                        (0)
                        <?php endif; ?>
                    </td>
                    <td style="border:1px solid #dfdfdf; font-weight: bold;">
                        <span style="color:green;"><?php echo $dataBody['totals']['call'][0]; ?></span>&nbsp;/&nbsp;
                        <span style="color:red;"><?php echo $dataBody['totals']['call'][1]; ?></span>&nbsp;/&nbsp;
                        &nbsp;
                        <?php if($dataBody['totals']['call'][1]>$dataBody['totals']['call'][0]): ?>
                        <span style="color:red;">(<?php echo ($dataBody['totals']['call'][0]-$dataBody['totals']['call'][1]); ?>)</span>
                        <?php elseif($dataBody['totals']['call'][1]<$dataBody['totals']['call'][0]): ?>
                        <span style="color:green;">(<?php echo ($dataBody['totals']['call'][0]-$dataBody['totals']['call'][1]); ?>)</span>
                        <?php else: ?>
                        (0)
                        <?php endif; ?>
                    </td>
                    <td style="border:1px solid #dfdfdf; font-weight: bold;">
                        <span style="color:green;"><?php echo $dataBody['totals']['in'][0]; ?></span>&nbsp;/&nbsp;
                        <span style="color:red;"><?php echo $dataBody['totals']['in'][1]; ?></span>&nbsp;/&nbsp;
                        &nbsp;
                        <?php if($dataBody['totals']['in'][1]>$dataBody['totals']['in'][0]): ?>
                        <span style="color:red;">(<?php echo ($dataBody['totals']['in'][0]-$dataBody['totals']['in'][1]); ?>)</span>
                        <?php elseif($dataBody['totals']['in'][1]<$dataBody['totals']['in'][0]): ?>
                        <span style="color:green;">(<?php echo ($dataBody['totals']['in'][0]-$dataBody['totals']['in'][1]); ?>)</span>
                        <?php else: ?>
                        (0)
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endif;?>
            </tbody>
        </table>
    </div>
</div>
<?php if(!empty($_GET['data-from'])&&!empty($_GET['data-to'])&&$_GET['data-to']!=$_GET['data-from']): ?>
<?php if(!empty($data1)):?>
<div class="row-fluid">
    <div class="span12">
        <h3>Заявки</h3>
    </div>
</div>
    <canvas id="myChart1" height="300" width="800" style="width: 800px; height: 300px;"></canvas>
    <script>
        var data = <?php echo $data1; ?>;
        var options = {
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1,
            bezierCurve : true,
            bezierCurveTension : 0.4,
            pointDot : true,
            pointDotRadius : 4,
            pointDotStrokeWidth : 1,
            pointHitDetectionRadius : 20,
            datasetStroke : true,
            datasetStrokeWidth : 2,
            datasetFill : true,
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
        };
        var ctx = $("#myChart1").get(0).getContext("2d");
        var myLineChart1 = new Chart(ctx).Line(data, options);
    </script>
<?php endif;?>
<?php if(!empty($data2)):?>
<div class="row-fluid">
    <div class="span12">
        <h3>Записи</h3>
    </div>
</div>
<canvas id="myChart2" height="300" width="800" style="width: 800px; height: 300px;"></canvas>
<script>
    var data = <?php echo $data2; ?>;
    var options = {
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 2,
        datasetFill : true,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
    };
    var ctx = $("#myChart2").get(0).getContext("2d");
    var myLineChart2 = new Chart(ctx).Line(data, options);
</script>
<?php endif;?>
<?php if(!empty($data3)):?>
<div class="row-fluid">
    <div class="span12">
        <h3>Отказы</h3>
    </div>
</div>
<canvas id="myChart3" height="300" width="800" style="width: 800px; height: 300px;"></canvas>
<script>
    var data = <?php echo $data3; ?>;
    var options = {
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 2,
        datasetFill : true,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
    };
    var ctx = $("#myChart3").get(0).getContext("2d");
    var myLineChart3 = new Chart(ctx).Line(data, options);
</script>
<?php endif;?>
<?php if(!empty($data4)):?>
<div class="row-fluid">
    <div class="span12">
        <h3>Обратная</h3>
    </div>
</div>
<canvas id="myChart4" height="300" width="800" style="width: 800px; height: 300px;"></canvas>
<script>
    var data = <?php echo $data4; ?>;
    var options = {
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 2,
        datasetFill : true,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
    };
    var ctx = $("#myChart4").get(0).getContext("2d");
    var myLineChart4 = new Chart(ctx).Line(data, options);
</script>
<?php endif;?>
<?php if(!empty($data5)):?>
<div class="row-fluid">
    <div class="span12">
        <h3>Дошедшая</h3>
    </div>
</div>
<canvas id="myChart5" height="300" width="800" style="width: 800px; height: 300px;"></canvas>
<script>
    var data = <?php echo $data5; ?>;
    var options = {
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 2,
        datasetFill : true,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
    };
    var ctx = $("#myChart5").get(0).getContext("2d");
    var myLineChart5 = new Chart(ctx).Line(data, options);
</script>
<?php endif;?>
<?php endif; ?>
<?=$partner;?>