<script>
$(document).ready(function(){
    $('body').on('click', '.atelephone', function() {
        $.ajax({
            url: $(this).attr('href'),
            type: 'GET',
            success: function(r) {
                if (r) {
                    $("#ajax-telephone").html(r);
                } else {
                    $("#ajax-telephone").html('');
                }
            }
        });
        return false;
    });
});
</script>
<form action="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array()); ?>" class="failure-form">
    <input type="hidden" name="report" value="report" />
    <input type="hidden" value="<?php echo $data_from;?>" name="data-from"/>
    <input type="hidden" value="<?php echo $data_to;?>" name="data-to"/>
</form>
<div class="row-fluid">
    <ul class="nav nav-tabs nav_report">
        <li>
            <?php echo CHtml::link('Отчет',array('/mbRecord/report')); ?>
        </li>
    </ul>
</div>
<div class="row-fluid">
    <div class="span12">
        <p></p>
        <h3>Данные по количество, в сравнении с Oktell</h3>
        <?php if(!empty($stat)): ?>
        <p></p>
        <table cellspacing="0" cellpadding="10" style="text-align:center;border:1px solid #dfdfdf;color:#666;font:12px Arial;line-height:1.4em;width:100%;">
            <thead>
                <tr>
                    <?php foreach($stat[0] as $key=>$value): ?>
                    <th style="border:1px solid #dfdfdf;"><?php echo $key; ?></th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($stat as $k=>$st): ?>
                <tr>
                    <?php foreach($st as $key=>$value): ?>
                    <td style="border:1px solid #dfdfdf;"><?php echo $key!='Тип'&&$k!=0?CHtml::link(count($value),array('/mbRecord/reportSelect','k'=>$k,'type'=>$key,'leftDateParam'=>$leftDateParam,'rightDateParam'=>$rightDateParam),array('class'=>'atelephone')):$value; ?></td>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php endif; ?>
        <p></p>
        <p>ВНИМАНИЕ! Данные могут не совпадать с отчетами на почте, если только не вчерашний отчет выбран!!!</p>
    </div>
    <div class="span12" id="ajax-telephone">
    </div>
</div>