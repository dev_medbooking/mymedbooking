<form action="<?php echo Yii::app()->createUrl('/mbRecord/'.Yii::app()->controller->action->id,array()); ?>" class="failure-form">
    <input type="hidden" name="report" value="report" />
    <input type="hidden" value="<?php echo $data_from;?>" name="data-from"/>
    <input type="hidden" value="<?php echo $data_to;?>" name="data-to"/>
    <input type="hidden" value="<?php echo $author_id;?>" name="author_id"/>
</form>
<div class="row-fluid">
    <ul class="nav nav-tabs nav_report">
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==1?' class="active"':''; ?>>
            <?php $get['long']=1; echo CHtml::link('1 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==2?' class="active"':''; ?>>
            <?php $get['long']=2; echo CHtml::link('2 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==3?' class="active"':''; ?>>
            <?php $get['long']=3; echo CHtml::link('3 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==7?' class="active"':''; ?>>
            <?php $get['long']=7; echo CHtml::link('7 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==10?' class="active"':''; ?>>
            <?php $get['long']=10; echo CHtml::link('10 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==30?' class="active"':''; ?>>
            <?php $get['long']=30; echo CHtml::link('30 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==31?' class="active"':''; ?>>
            <?php $get['long']=31; echo CHtml::link('31 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==90?' class="active"':''; ?>>
            <?php $get['long']=90; echo CHtml::link('90 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==91?' class="active"':''; ?>>
            <?php $get['long']=91; echo CHtml::link('91 дн.',$get); ?>
        </li>
        <li <?php echo !empty($_GET['long'])&&$_GET['long']==92?' class="active"':''; ?>>
            <?php $get['long']=92; echo CHtml::link('92 дн.',$get); ?>
        </li>
        <li><div class="select_styled"><?php echo CHtml::dropDownList('report_list', $author_id, CHtml::listData(MbUser::model()->findAllByAttributes(array('apicall'=>1)), 'uid', 'nameperson'), array('prompt' => 'Все операторы')); ?><span></span></div></li>
        <li>
            <?php echo CHtml::link('Oktell',array('/mbRecord/reportOktell')); ?>
        </li>
        <li>
            <?php echo CHtml::link('Отчет',array('/mbReport/report')); ?>
        </li>
    </ul>
</div>
<div class="row-fluid">
    <div class="span12" id="admin-admin">
        <?php if(!empty($_GET['data-from'])): ?>
        <h3>
            <?php if(!empty($_GET['data-from'])): ?>
            <?php echo $_GET['data-from']; ?>
            <?php endif; ?>
            <?php if(!empty($_GET['data-to'])): ?>
            &mdash; <?php echo $_GET['data-to']; ?>
            <?php endif; ?>
        </h3>
        <?php else: ?>
        <h3>Данные за сегодня</h3>
        <?php endif; ?>
        <?php $this->renderPartial('_report',array('dataBody'=>$dataBody)); ?>
    </div>
</div>