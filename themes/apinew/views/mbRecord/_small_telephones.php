<?php if(!empty($data)): ?>
<table cellspacing="0" cellpadding="10" style="text-align:center;border:1px solid #dfdfdf;color:#666;font:12px Arial;line-height:1.4em;width:100%;" class="t-telephones">
    <thead>
        <tr>
            <?php $count=count($data)>=9?9:count($data); ?>
            <?php for($index=0; $index<$count; $index++): ?>
            <th style="border:1px solid #dfdfdf;">Телефон</th>
            <?php endfor; ?>
        </tr>
    </thead>
    <tbody>
        <?php $i=0; foreach($data as $value): ?>
        <?php if($i==0): ;?>
        <tr>
        <?php endif; ?>
        <td style="border:1px solid #dfdfdf;"><?php echo CHtml::link($value['tl'],array('/mbRecord/telephone','id'=>$value['tl'])); ?></td>
        <?php if($i==9): ;?>
        </tr>
        <?php endif; ?>
        <?php $i++; $i=($i==9)?0:$i; endforeach; ?>
    </tbody>
</table>
<?php endif; ?>