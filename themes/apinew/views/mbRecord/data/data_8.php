<tr class="<?php echo !empty($model->flag_id)?'api-item-tr-'.$model->flag_id.' ':'api-item-tr ';?><?php echo $model->color; ?><?php $key%2?'-light':''; ?>">
    <td class="<?php echo empty($model->status)?'new':'old'; ?>"><?php echo $model->id; ?><span<?=!empty($model->fl->color)?' style="color:'.$model->fl->color.';"':'';?>></span></td>
    <td><?php echo !empty($model->formattedCreateTime)?$model->formattedCreateTime:''; ?></td>
    <td><?php echo !empty($model->countTel)?$model->countTel:''; ?> / <?php echo !empty($model->countTelAll)?$model->countTelAll:''; ?></td>
    <td class="phone_td"><?php echo !empty($model->telephone)?CHtml::link($model->telephone,array('/mbRecord/telephone','id'=>$model->telephone)):''; ?></td>
    <td><?php echo !empty($model->during)?$model->during:''; ?></td>
    <td><?php echo !empty($model->o->dt)?$model->o->dt:''; ?></td>
    <td><?php echo !empty($model->o->callerid2)?$model->o->callerid2:''; ?></td>
    <td><?php echo !empty($model->telephoneNext)?$model->telephoneNext:''; ?></td>
    <td><?php echo !empty($model->telephoneOut)?$model->telephoneOut:''; ?></td>
    <td><?php echo !empty($model->id_call)?$model->id_call:''; ?></td>
    <td><?php echo !empty($model->oktell_id)?CHtml::link($model->oktell_id,array('/mbAdminInvoice/view','id'=>$model->oktell_id)):''; ?></td>
    <td class="ico-edit"><?php echo CHtml::link('',array('update','id'=>$model->id)); ?></td>
    <td class="ico-view"><?php echo CHtml::link('',array('view','id'=>$model->id)); ?></td>
    <td class="ico-delete"><?php echo CHtml::link('',array('delete','id'=>$model->id)); ?></td>
</tr>