<tr class="<?php echo !empty($model->flag_id)?'api-item-tr-'.$model->flag_id.' ':'api-item-tr ';?><?php echo $model->color; ?><?php $key%2?'-light':''; ?>">
    <td class="<?php echo empty($model->status)?'new':'old'; ?> <?php echo !empty($model->house)?'house':'';?>"><?php echo $model->id; ?><span<?=!empty($model->fl->color)?' class="flag" style="background:'.$model->fl->color.';"':'';?>></span></td>
    <td><?php echo !empty($model->formattedCreateTime)?$model->formattedCreateTime:''; ?></td>
    <td><?php echo !empty($model->formattedRecTime)?$model->formattedRecTime:''; ?></td>
    <td><?php echo !empty($model->name)?$model->name:''; ?></td>
	<td class="phone_td_j js-copy-phone"><?php echo !empty($model->telephone)?CHtml::link($model->telephone,array('/mbRecord/telephone','id'=>$model->telephone)).(($model->ident_id != 0 AND is_null($model->first_time_status)) ? ' <span class="js-update-first-time-status updateStatus_time" data-href="'.Yii::app()->createUrl('mbRecord/updatePartnerFirstTimeStatus/'.$model->id).'"></span>' : ($model->ident_id != 0 ? ' <span class="updatedStatus_time"></span>' : '')) : ''; ?></td>
    <td><?php echo !empty($model->clinic_name)?(!empty($model->info->clinic_url)?CHtml::link($model->clinic_name,$model->info->clinic_url):$model->clinic_name):''; ?></td>
    <td><?php echo !empty($model->doctor_name)?(!empty($model->info->doctor_url)?CHtml::link($model->doctor_name,$model->info->doctor_url):$model->doctor_name):''; ?></td>
	<td>
		<?php if (!empty($model->services)) :?>
			<?php $array = array();?>
			<?php foreach ($model->services as $k => $v) :?>
                <?php
                if (!empty( $v->service->title)) {
                    $array[] = $v->service->title
                        . (($model->contrast && 2 == $v->service->type) ? ' [контраст]' : '')
                    ;
                }
                ?>
			<?php endforeach;?>
			<?=implode('/', $array);?>
		<?php endif;?>
	</td>
    <?php if(Yii::app()->user->checkAccess('Toperator')): ?><td><a href="<?php echo Yii::app()->createUrl('mbRecord/failure',array('status'=>1,'host'=>(!empty($_GET['host'])?$_GET['host']:''),'author'=>!empty($model->f->uid)?$model->f->uid:''));?>"><?php echo (!empty($model->f->nameperson)?$model->f->nameperson:(!empty($model->f->email)?$model->f->email:'')); ?></a></td>
    <?php else: ?><td><?php echo (!empty($model->f->nameperson)?$model->f->nameperson:(!empty($model->f->email)?$model->f->email:'')); ?></td><?php endif; ?>
    <?php if(Yii::app()->user->checkAccess('Toperator')): ?><td><a href="<?php echo Yii::app()->createUrl('mbRecord/failure',array('status'=>1,'host'=>(!empty($_GET['host'])?$_GET['host']:''),'author'=>!empty($model->author->uid)?$model->author->uid:''));?>"><?php echo (!empty($model->author->nameperson)?$model->author->nameperson:(!empty($model->author->email)?$model->author->email:'')); ?></a></td>
    <?php else: ?><td><?php echo (!empty($model->author->nameperson)?$model->author->nameperson:(!empty($model->author->email)?$model->author->email:'')); ?></td><?php endif; ?>
    <td><?php echo !empty($model->note)?$model->note:''; ?></td>
	<td><?=$model->recordTypeText;?></td>
    <td class="ico-edit"><?php echo CHtml::link('',array('update','id'=>$model->id)); ?></td>
    <td class="ico-view"><?php echo CHtml::link('',array('view','id'=>$model->id)); ?></td>
    <td class="ico-delete"><?php echo CHtml::link('',array('delete','id'=>$model->id)); ?></td>
</tr>