<div class="order-container clearfix mModel_top">
   <div class="ajax-order-container clearfix">
        <?php if(!empty($dataRecord)): ?>
        <div class="top-order-box">
            <h3>Последние заявки — <?php echo $_GET['id']; ?></h3>
        </div>
        <div class="order-boxes clearfix">
            <table class="history_order" cellspacing="0">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Время создания</td>
                        <td>ФИО</td>
                        <td>Врач</td>
                        <td>Клиника</td>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0; ?>
                    <?php foreach ($dataRecord as $value): ?>
                        <?php if ($value->status==0):?>
                            <tr>
                                <td><?php echo CHtml::link($value->id,array('/mbRecord/update','id'=>$value->id)); ?></td>
                                <td><?php echo $value->create_time?></td>
                                <td><?php echo $value->name?></td>
                                <td><?php echo $value->doctor_name?></td>
                                <td><?php echo $value->clinic_name?></td>
                            </tr>
                            <?php $i++; ?>
                        <?php endif; ?>
                        <?php if ($i==4): ?>
                            <?php break;?>
                        <?php endif; ?>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>