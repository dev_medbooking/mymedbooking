<div class="order-container clearfix">
    <div class="top-order-box">
        <h2 class="title-order">Заявка <span class="order-number">#<?php echo $model->id; ?></span></h2>
        <span class="status-order"><?php echo $model->statusName; ?> / <?php echo !empty($model->status)?$model->status:""; ?></span><span class="edit_record"><a href="<?php echo Yii::app()->createUrl('mbRecord/update',array('id'=>$model->id)); ?>">Редактировать заявку</a></span>
    </div>
    <div class="order-boxes clearfix">
        <div class="order-left">
            <div class="name-box">
                <p class="heed"><?php echo !empty($model->name)?$model->name:''; ?></p>
                <span><?php echo $model->sex; ?><?php echo !empty($model->rerecord)?', новый клиент':''; ?></span>
            </div>
            <div class="phone-box">
                <p class="heed-mini"><?php echo !empty($model->telephone)?$model->telephone:''; ?></p>
                <span><?php echo !empty($model->email)?$model->email:''; ?></span>
            </div>
            <div class="order-description">
                <div class="order-description-top">
                    <p>Клиент записывался на прием —</p>
                    <span class="record-client"><?php echo !empty($model->reception_time)?$model->reception_time:''; ?></span>
                </div>
                <div class="order-description-bottom">
                    <p>Клинника:</p>
                    <span class="address-clinic"><?php echo !empty($model->clinic_name)?$model->clinic_name:''; ?> <?php echo !empty($model->info->address)?$model->info->address:''; ?> <?php echo !empty($model->info->subway)?$model->info->subway:''; ?></span>
                    <p>Доктор, специалист:</p>
                    <span class="doctor-name"><?php echo !empty($model->doctor_name)?$model->doctor_name:''; ?></span>
                    <p>Специализация:</p>
                    <?php echo !empty($model->category_name)?'<span class="category-name">'.$model->category_name.'</span>':''; ?>
                    <p class="description"><?php echo !empty($model->info->speciality)?$model->info->speciality:''; ?></p>
                    <p class="description"><?php echo !empty($model->info->position)?$model->info->position:''; ?></p>
                    <p class="description"><?php echo !empty($model->info->doctor_description)?$model->info->doctor_description:''; ?></p>
                    <p class="description"><?php echo !empty($model->service)?$model->service:''; ?></p>
                </div>
            </div>
        </div>
        <div class="order-right">
            <div class="status-box">
                <?php if(!empty($model->domain)): ?>
                <div class="link-inner"><a target="_blank" href="http://<?php echo $model->domain; ?>"><?php echo $model->domain; ?></a></div>
                <?php endif; ?>
	            <?php if(!empty($model->ident_id)): ?>
		        <span><?=(!empty($model->partner->name)) ? $model->partner->name : '';?></span>
	            <?php endif; ?>
                <?php if(!empty($model->fromcall)): ?>
                <span>Обр.связь</span>
                <?php endif; ?>
	            <?php if($model->home): ?>
		            <span>Вызов на дом</span>
	            <?php endif; ?>
            </div>
            <div class="date-box">
	            <span>Статус заявки:</span>
	            <p><?php echo !empty($model->status)?$model->statusName:''; ?></p>
                <span>Заявка создана:</span>
                <p><?php echo !empty($model->create_time)?$model->create_time:''; ?></p>
                <span>Последня редакция:</span>
                <p><?php echo !empty($model->update_time)?$model->update_time:''; ?></p>
            </div>
            <div class="description-box">
                <?php if(!empty($model->note)):?>
                    <span>Пользовательский запрос:</span>
                    <p><?php echo !empty($model->note)?$model->note:''; ?></p>
                <?php endif;?>
                    <?php if(!empty($model->description)):?>
                    <span>Описание:</span>
                    <p><?php echo !empty($model->description)?$model->description:''; ?></p>
                <?php endif;?>
                <?php if(!empty($model->comment)):?>
                    <span>Примечание:</span>
                    <p><?php echo !empty($model->comment)?$model->comment:''; ?></p>
                <?php endif;?>
                <?php if(!empty($model->reason)):?>
                    <span>Причина отказа:</span>
                    <strong><?php echo !empty($model->reason)?$model->reason:''; ?></strong>
                <?php endif;?>
            </div>
        </div>
    </div>
    <?php if(!empty($model->apisms_ar_s)):?>
    <table class="history_order" cellspacing="0">
        <thead>
            <tr>
                <th>Сообщение</th>
                <th width="20%">Время создания</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model->apisms_ar_s as $key=>$value):?>
                <tr>
                    <td><?php echo $value->message; ?></td>
                    <td><?php echo $value->create_time; ?></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <?php endif; ?>
	<?php if( ! empty($model->history)):?>
		<table class="history_order" cellspacing="0">
			<thead>
			<tr>
				<th>История изменений</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($model->history as $key => $value):?>
				<?php $old = unserialize($value->old);?>
				<?php $new = unserialize($value->new);?>
				<tr>
					<td>
						<span class="js-show-history"><?=$value->date;?> - <?=$value->user_history->nameperson;?></span>
						<table class="js-history" style="display: none;">
								<?php foreach($old as $k => $v) :?>
									<?php if ($v != $new[$k]) :?>
										<tr>
											<td><?=$k;?></td>
											<td><?=$v;?></td>
											<td><?=$new[$k];?></td>
										</tr>
									<?php endif;?>
								<?php endforeach;?>
						</table>
					</td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	<?php endif; ?>
</div>