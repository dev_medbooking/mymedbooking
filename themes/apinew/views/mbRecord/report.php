<?php if(Yii::app()->user->checkAccess('Toperator')): ?>
<?php $this->renderPartial('//mbRecord/_team_operator/report'); ?>
<?php elseif(Yii::app()->user->checkAccess('Taccounter')): ?>
<?php $this->renderPartial('//mbRecord/_team_accounter/report'); ?>
<?php elseif(Yii::app()->user->checkAccess('Administrator')): ?>
<?php $this->renderPartial('//mbRecord/_superadmin/report'); ?>
<?php endif; ?>