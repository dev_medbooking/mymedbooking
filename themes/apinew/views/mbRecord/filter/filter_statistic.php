<thead>
    <tr>
        <td class="id"># <?php echo CHtml::activeTextField($model,'id',array('class'=>'search-single','id'=>'filter_id')); ?></td>
        <td class="date-create<?php echo !empty($status)?'':'_2'; ?>" data-name_from="create_time_from" data-name_to="create_time_to">
            <i><?php echo $sort->link('create_time',null,array('class'=>'csorting')); ?></i>
            <i class="dn1"><a class='lstr' href='#'><?php echo isset($_GET['create_time_from'])?$_GET['create_time_from']:''; ?></a></i>
            <i class="dn2"><a class='lstr' href='#'><?php echo isset($_GET['create_time_to'])?$_GET['create_time_to']:''; ?></a></i>
            <input class="search hidden-date-create1" type="hidden" name="create_time_from" value="<?php echo isset($_GET['create_time_to'])?$_GET['create_time_to']:''; ?>">
            <input class="search hidden-date-create2" type="hidden" name="create_time_to" value="<?php echo isset($_GET['create_time_from'])?$_GET['create_time_from']:''; ?>">
        </td>
        <td class="time<?php echo !empty($status)?'':'_2'; ?>" data-name_from="reception_time_from" data-name_to="reception_time_to">
            <i><?php echo $sort->link('reception_time',null,array('class'=>'csorting')); ?></i>
            <i class="dn3"><a class="lstr" href="#"><?php echo isset($_GET['reception_time_from'])?$_GET['reception_time_from']:'';?></a></i>
            <i class="dn4"><a class="lstr" href="#"><?php echo isset($_GET['reception_time_to'])?$_GET['reception_time_to']:'';?></a></i>
            <input class="search time1" type="hidden" name="reception_time_from" value="<?php echo isset($_GET['reception_time_from'])?$_GET['reception_time_from']:'';?>">
            <input class="search time2" type="hidden" name="reception_time_to" value="<?php echo isset($_GET['reception_time_to'])?$_GET['reception_time_to']:'';?>">
            <input class="search" type="hidden" name="fromsite" value="<?php echo isset($_GET['fromsite'])?$_GET['fromsite']:'';?>">
            <input class="search" type="hidden" name="fromcall" value="<?php echo isset($_GET['fromcall'])?$_GET['fromcall']:'';?>">
            <input class="search" type="hidden" name="feedback" value="<?php echo isset($_GET['feedback'])?$_GET['feedback']:'';?>">
            <input class="search" type="hidden" name="house" value="<?php echo isset($_GET['house'])?$_GET['house']:'';?>">
        </td>
        <td class="name<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('name',null,array('class'=>'csorting')); ?> <div><?php echo CHtml::activeTextField($model,'name',array('class'=>'search-single','id'=>'filter_name')); ?></div></td>
        <td class="phone<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('telephone',null,array('class'=>'csorting')); ?> <div><?php echo CHtml::activeTextField($model,'telephone',array('class'=>'search-single','id'=>'filter_telephone')); ?></div></td>
        <td class="clinic<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('clinic_name',null,array('class'=>'csorting')); ?> <div><?php echo CHtml::activeTextField($model,'clinic_name',array('class'=>'search-single','id'=>'filter_clinic_name')); ?></div></td>
        <td class="doctor<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('doctor_name',null,array('class'=>'csorting')); ?> <div><?php echo CHtml::activeTextField($model,'doctor_name',array('class'=>'search-single','id'=>'filter_doctor_name')); ?></div></td>
        <td class="author<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('first_uid',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'first_uid',array('class'=>'search-single','id'=>'filter_first_uid')); ?></div></td>
        <td class="creater<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('author_id',null,array('class'=>'csorting')); ?> <div><?php echo CHtml::activeTextField($model,'author_id',array('class'=>'search-single','id'=>'filter_author_id')); ?></div></td>
        <td class="status-application"><div>-X-</div></td>
    </tr>
</thead>