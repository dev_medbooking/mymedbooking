<thead>
    <tr>
        <td class="id"># <?php echo CHtml::activeTextField($model,'id',array('class'=>'search-single','id'=>'filter_id')); ?></td>
        <td class="date-create_2" data-name_from="create_time_from" data-name_to="create_time_to">
            <i><?php echo $sort->link('create_time',null,array('class'=>'csorting')); ?></i>
            <i class="dn1"><a class='lstr' href='#'><?php echo isset($_GET['create_time_from']) ? $_GET['create_time_from'] : ''; ?></a></i>
            <i class="dn2"><a class='lstr' href='#'><?php echo isset($_GET['create_time_to']) ? $_GET['create_time_to'] : ''; ?></a></i>
            <input class="search hidden-date-create1" type="hidden" name="create_time_from" value="<?= isset($_GET['create_time_to']) ? $_GET['create_time_to'] : ''; ?>">
            <input class="search hidden-date-create2" type="hidden" name="create_time_to" value="<?= isset($_GET['create_time_from']) ? $_GET['create_time_from'] : ''; ?>">
            <input class="search" type="hidden" name="fromsite" value="<?php echo isset($_GET['fromsite'])?$_GET['fromsite']:'';?>">
            <input class="search" type="hidden" name="fromcall" value="<?php echo isset($_GET['fromcall'])?$_GET['fromcall']:'';?>">
            <input class="search" type="hidden" name="feedback" value="<?php echo isset($_GET['feedback'])?$_GET['feedback']:'';?>">
            <input class="search" type="hidden" name="house" value="<?php echo isset($_GET['house'])?$_GET['house']:'';?>">
            <input class="search" type="hidden" name="deleted" value="<?php echo isset($_GET['deleted'])&&Yii::app()->user->checkAccess('Administrator')?$_GET['deleted']:'';?>">
        </td>
        <td class="name_2"><?php echo $sort->link('name',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'name',array('class'=>'search-single','id'=>'filter_name')); ?></div></td>
        <td class="phone_2"><?php echo $sort->link('telephone',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'telephone',array('class'=>'search-single','id'=>'filter_telephone')); ?></div></td>
        <td class="clinic_2"><?php echo $sort->link('clinic_name',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'clinic_name',array('class'=>'search-single','id'=>'filter_clinic_name')); ?></div></td>
        <td class="doctor_2"><?php echo $sort->link('doctor_name',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'doctor_name',array('class'=>'search-single','id'=>'filter_doctor_name')); ?></div></td>
	    <td class="service_2"><?php echo $sort->link('service',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'service',array('class'=>'search-single','id'=>'filter_service')); ?></div></td>
        <td class="creater_2"><?php echo $sort->link('first_uid',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'first_uid',array('class'=>'search-single','id'=>'filter_first_uid')); ?></div></td>
        <td class="creater_2"><?php echo $sort->link('author_id',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'author_id',array('class'=>'search-single','id'=>'filter_author_id')); ?></div></td>
        <td class="note">Пользовательский запрос<div><?php echo CHtml::activeTextField($model,'note',array('class'=>'search-single','id'=>'filter_note')); ?></div></td>
        <td class="note">Причина <div><?php echo CHtml::activeTextField($model,'reason',array('class'=>'search-single','id'=>'reason_note')); ?></div></td>
	    <td></td>
        <?php if(Yii::app()->user->checkAccess('NCallcenter')): ?>
        <td class="edit"></td>
        <td class="view"></td>
        <td class="delete"></td>
        <?php endif; ?>
    </tr>
</thead>