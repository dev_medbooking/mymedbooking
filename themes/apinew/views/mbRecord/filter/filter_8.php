<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"># <?php echo CHtml::activeTextField($model,'id',array('class'=>'search-single','id'=>'filter_id')); ?></td>
        <td class="date-create<?php echo !empty($status)?'':'_2'; ?>" data-name_from="create_time_from" data-name_to="create_time_to">
            <i><?php echo $sort->link('create_time',null,array('class'=>'csorting')); ?></i>
            <i class="dn1"><a class='lstr' href='#'><?php echo isset($_GET['create_time_from'])?$_GET['create_time_from']:''; ?></a></i>
            <i class="dn2"><a class='lstr' href='#'><?php echo isset($_GET['create_time_to'])?$_GET['create_time_to']:''; ?></a></i>
            <input class="search hidden-date-create1" type="hidden" name="create_time_from" value="<?php echo isset($_GET['create_time_to'])?$_GET['create_time_to']:''; ?>">
            <input class="search hidden-date-create2" type="hidden" name="create_time_to" value="<?php echo isset($_GET['create_time_from'])?$_GET['create_time_from']:''; ?>">
            <input class="search" type="hidden" name="fromsite" value="<?php echo isset($_GET['fromsite'])?$_GET['fromsite']:'';?>">
            <input class="search" type="hidden" name="fromcall" value="<?php echo isset($_GET['fromcall'])?$_GET['fromcall']:'';?>">
            <input class="search" type="hidden" name="feedback" value="<?php echo isset($_GET['feedback'])?$_GET['feedback']:'';?>">
            <input class="search" type="hidden" name="house" value="<?php echo isset($_GET['house'])?$_GET['house']:'';?>">
        </td>
        <td class="countTel<?php echo !empty($status)?'':'_2'; ?>">Кол-во (дат. / общее)</td>
        <td class="phone<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('telephone',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'telephone',array('class'=>'search-single','id'=>'filter_telephone')); ?></div></td>
        <td class="during<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('during',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'during',array('class'=>'search-single','id'=>'filter_during')); ?></div></td>
        <td class="dt<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('dt',null,array('class'=>'csorting')); ?></td>
        <td class="callerid2<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('callerid2',null,array('class'=>'csorting')); ?></td>
        <td class="qtyTel<?php echo !empty($status)?'':'_2'; ?>">Обработка</td>
        <td class="telephoneOut<?php echo !empty($status)?'':'_2'; ?>">Обратный</td>
        <td class="id_call<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id_call',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'id_call',array('class'=>'search-single','id'=>'filter_id_call')); ?></div></td>
        <td class="oktell_id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('oktell_id',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'oktell_id',array('class'=>'search-single','id'=>'filter_oktell_id')); ?></div></td>
        <td class="edit"></td>
        <td class="view"></td>
        <td class="delete"></td>
    </tr>
</thead>