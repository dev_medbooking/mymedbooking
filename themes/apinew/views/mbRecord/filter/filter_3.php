<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"># <?php echo CHtml::activeTextField($model,'id',array('class'=>'search-single','id'=>'filter_id')); ?></td>
        <td class="date-create<?php echo !empty($status)?'':'_2'; ?>" data-name_from="create_time_from" data-name_to="create_time_to">
            <i><?php echo $sort->link('create_time',null,array('class'=>'csorting')); ?></i>
            <i class="dn1"><a class='lstr' href='#'><?php echo isset($_GET['create_time_from']) ? $_GET['create_time_from']:''; ?></a></i>
            <i class="dn2"><a class='lstr' href='#'><?php echo isset($_GET['create_time_to']) ? $_GET['create_time_to']:''; ?></a></i>
            <input class="search hidden-date-create1" type="hidden" name="create_time_from" value="<?= isset($_GET['create_time_to']) ? $_GET['create_time_to']:''; ?>">
            <input class="search hidden-date-create2" type="hidden" name="create_time_to" value="<?= isset($_GET['create_time_from']) ? $_GET['create_time_from']:''; ?>">
        </td>
        <td class="time_2" data-name_from="reception_time_from" data-name_to="reception_time_to">
            <i><?php echo $sort->link('reception_time',null,array('class'=>'csorting')); ?></i>
            <i class="dn3"><a class="lstr" href="#"><?php echo isset($_GET['reception_time_from'])?$_GET['reception_time_from']:'';?></a></i>
            <i class="dn4"><a class="lstr" href="#"><?php echo isset($_GET['reception_time_to'])?$_GET['reception_time_to']:'';?></a></i>
            <input class="search time1" type="hidden" name="reception_time_from" value="<?=isset($_GET['reception_time_from'])?$_GET['reception_time_from']:'';?>">
            <input class="search time2" type="hidden" name="reception_time_to" value="<?=isset($_GET['reception_time_to'])?$_GET['reception_time_to']:'';?>">
            <input class="search" type="hidden" name="fromsite" value="<?=isset($_GET['fromsite'])?$_GET['fromsite']:'';?>">
            <input class="search" type="hidden" name="fromcall" value="<?=isset($_GET['fromcall'])?$_GET['fromcall']:'';?>">
            <input class="search" type="hidden" name="feedback" value="<?=isset($_GET['feedback'])?$_GET['feedback']:'';?>">
            <input class="search" type="hidden" name="house" value="<?=isset($_GET['house'])?$_GET['house']:'';?>">
        </td>
        <td class="edition<?php echo !empty($status)?'':'_2'; ?>" data-name_from="lead_time_from" data-name_to="lead_time_to">
            <i><?php echo $sort->link('lead_time',null,array('class'=>'csorting')); ?></i>
            <?php if(isset($_GET['lead_time_from'])):?>
                <i class="dn5"><a class="lstr" href="#"><?=$_GET['lead_time_from'];?></a></i>
            <?php endif;?>
            <?php if(isset($_GET['lead_time_to'])):?>
                <i class="dn6"><a class="lstr" href="#"><?=$_GET['lead_time_to'];?></a></i>
            <?php endif;?>
            <input class="search edition1" type="hidden" name="lead_time_from" value="">
            <input class="search edition2" type="hidden" name="lead_time_to" value="">
        </td>
        <td class="name<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('name',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'name',array('class'=>'search-single','id'=>'filter_name')); ?></div></td>
        <td class="phone<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('telephone',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'telephone',array('class'=>'search-single','id'=>'filter_telephone')); ?></div></td>
        <td class="clinic<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('clinic_name',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'clinic_name',array('class'=>'search-single','id'=>'filter_clinic_name')); ?></div></td>
        <td class="doctor<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('doctor_name',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'doctor_name',array('class'=>'search-single','id'=>'filter_doctor_name')); ?></div></td>
	    <td class="service<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('service',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'service',array('class'=>'search-single','id'=>'filter_service')); ?></div></td>
        <td class="creater<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('first_uid',null,array('class'=>'csorting')); ?> <div><?php echo CHtml::activeTextField($model,'first_uid',array('class'=>'search-single','id'=>'filter_first_uid')); ?></div></td>
        <td class="creater<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('author_id',null,array('class'=>'csorting')); ?><div><?php echo CHtml::activeTextField($model,'author_id',array('class'=>'search-single','id'=>'filter_author_id')); ?></div></td>
	    <td class="note">Примечание <div><?php echo CHtml::activeTextField($model,'note', array('class'=>'search-single')); ?></div></td>
        <td>Проект</td>
        <td>&nbsp;</td>
        <td class="edit"></td>
        <td class="view"></td>
        <td class="delete"></td>
    </tr>
</thead>