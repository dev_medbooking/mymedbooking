<?php if(empty($type)): ?>
<tr class="edit" id="tr-item-<?=$model->id;?>" data-id="<?=$model->id;?>">
<?php endif;?>
    <td>
        <span class="dn2"><?=$model->id; ?></span>
    </td>
    <td>
        <div class="visihid" data-hid="<?=$model->id;?>">
            <?=( ! empty($model->name) ? $model->name : '');?>
        </div>
        <div data-hid="<?=$model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::activeTextField($model, 'name', array('class'=>'span12', 'data-model'=>$model->id)); ?>
            <div class="error"></div>
        </div>
    </td>
    <td>
        <div class="visihid" data-hid="<?=$model->id;?>">
            <?=( ! empty($model->user->nameperson) ? $model->user->nameperson : '');?>
        </div>
        <div data-hid="<?=$model->id; ?>" class="hid" style="display:none;">
	        <?=( ! empty($model->user->nameperson) ? $model->user->nameperson : '');?>
            <div class="error"></div>
        </div>
    </td>
	<td>
		<div class="visihid" data-hid="<?=$model->id;?>">
			<?=$model->priceType;?>
		</div>
		<div data-hid="<?=$model->id; ?>" class="hid" style="display:none;">
			<?=CHtml::activeDropDownList($model, 'type', $model->availableTypes, array('data-model'=>$model->id));?>
			<div class="error"></div>
		</div>
	</td>
    <td>
        <div data-hid="<?php echo $model->id; ?>" class="visihid">
            <?php echo CHtml::link('<i class="icon-share"></i>',array('#','id'=>$model->id),array('data-id'=>$model->id,'class'=>'fast-change')); ?>
        </div>
        <div data-hid="<?php echo $model->id; ?>" class="hid" style="display:none;">
            <?php echo CHtml::link('<i class="icon-check-empty"></i>',"#",array("class"=>'unedit','data-id'=>$model->id)); ?>
            <?php echo CHtml::link('<i class="icon-pencil"></i>',array('/'.Yii::app()->controller->id.'/update','id'=>$model->id),array("class"=>'update','data-id'=>$model->id)); ?>
        </div>
    </td>
	<td class="ico-delete">
		<?php echo CHtml::link('',array('/'.Yii::app()->controller->id.'/delete','id'=>$model->id),array('data-id'=>$model->id)); ?>
	</td>
<?php if(empty($type)): ?>
</tr>
<?php endif;?>