<div class="row-fluid row-call">
	<?php $this->renderPartial('_admin', array('model' => $model, 'sort' => $sort, 'pages' => $pages, 'count' => $count, 'data' => $data)); ?>
</div>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/table2.js',CClientScript::POS_END);?>