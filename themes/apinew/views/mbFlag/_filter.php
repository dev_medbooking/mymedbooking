<thead>
    <tr>
        <td class="id<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('id', null, array('class' => 'csorting')); ?></td>
        <td class="title<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('title', null, array('class' => 'csorting')); ?></td>
        <td class="color<?php echo !empty($status)?'':'_2'; ?>"><?php echo $sort->link('color', null, array('class' => 'csorting')); ?></td>
        <td class="edit"></td>
        <td class="view"></td>
        <td class="delete"></td>
        <td class="icon-share"></td>
    </tr>
    <tr class="filters">
        <td><?php echo CHtml::activeTextField($model, 'id', array('class' => 'search-single', 'id' => 'filter_id')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'title', array('class' => 'search', 'id' => 'filter_title')); ?></td>
        <td><?php echo CHtml::activeTextField($model, 'color', array('class' => 'search', 'id' => 'filter_color')); ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</thead>
