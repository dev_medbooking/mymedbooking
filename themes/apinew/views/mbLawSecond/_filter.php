<?php if(Yii::app()->controller->action->id=='admin'): ?>
<?php $this->renderPartial('//mbLawSecond/filter/admin',array('sort'=>$sort,'model'=>$model, 'queryDate'=>$queryDate, 'queryDateEnd'=>$queryDateEnd, 'count'=>$count,'import'=>$import)); ?>
<?php elseif(Yii::app()->controller->action->id=='law'): ?>
    <?php $this->renderPartial('//mbLawSecond/filter/law',array('sort'=>$sort,'model'=>$model)); ?>
<?php elseif(Yii::app()->controller->action->id=='clinics'): ?>
    <?php $this->renderPartial('//mbLawSecond/filter/clinics',array('sort'=>$sort,'model'=>$model,'count'=>$count)); ?>
<?php elseif(Yii::app()->controller->action->id=='prices'): ?>
    <?php $this->renderPartial('//mbLawSecond/filter/prices',array('sort'=>$sort,'model'=>$model)); ?>
<?php elseif(Yii::app()->controller->action->id=='book'): ?>
<?php $this->renderPartial('//mbLawSecond/filter/book',array('sort'=>$sort,'model'=>$model, 'queryDate'=>$queryDate,'count'=>$count)); ?>
<?php endif; ?>