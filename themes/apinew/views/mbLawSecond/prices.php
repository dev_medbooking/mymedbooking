<section id="content" class="content_prices">
    <div class="center">
        <div class="summary summary__lavSecond" id="summary">
            <?php $this->renderPartial('//layouts/_admin_count',array('count'=>$count, 'ordersCount'=>$ordersCount)); ?>
        </div>
        <div class="table table_law table_lawSecond price" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/prices',array('t'=>1));?>">
            <div class="filter-table"><?php $this->renderPartial('//mbLawSecond/_filter', array('sort' => $sort, 'model' => $model)); ?></div>
            <div class="body-table"><?php $this->renderPartial('_prices',array('data'=>$data)); ?></div>
        </div>
    </div>
    <div class="pagination row" id="pages">
        <?php if (!empty($pages) && !empty($count)): ?>
            <?php $this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count)); ?>
        <?php endif; ?>
    </div>
</section>
