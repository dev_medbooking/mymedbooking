<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'admin-form',
    'action' => Yii::app()->createUrl('mbLawSecond/uploadFile'),
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
));
?>
<?php echo $form->fileField($import, 'file', array('id' => "id-input-file-2")); ?>
<?php echo $form->error($import, 'file'); ?>
<?php echo CHtml::submitButton("Загрузить", array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>
<div class="row caption clearfix">
    <div class="cell number"><?php echo $sort->link('id',null,array('class'=>'csorting')); ?></div>
    <div class="cell email"><?php echo $sort->link('uid',null,array('class'=>'csorting')); ?></div>
    <div class="cell legalName"><?php echo $sort->link('title',null,array('class'=>'csorting')); ?> - <span class="summary"><?php $this->renderPartial('//layouts/_admin_count', array('count' => $count)); ?></span></div>
<!--    <div class="cell actualName"><?php //echo $sort->link('title',null,array('class'=>'csorting')); ?></div>-->
     <div class="cell affiliates"></div> 
    <div class="cell recording"><?php echo $sort->link('koll_record',null,array('class'=>'csorting')); ?></div>
    <div class="cell surviving"><?php echo $sort->link('koll_come',null,array('class'=>'csorting')); ?></div>
    <div class="cell successing"><?php echo $sort->link('koll_success',null,array('class'=>'csorting')); ?></div>
    <div class="cell generalSum"><?php echo $model->countKollAll['all_price'];?> <sup>руб</sup></div>
    <div class="cell repeat"><?php echo $sort->link('koll_diagnostika',null,array('class'=>'csorting')); ?></div>
    <div class="cell diagnostika"><?php echo $model->countKollAll['d_price'];?> <sup>руб</sup></div>
    <div class="cell date"><?php echo $sort->link('koll_repeat',null,array('class'=>'csorting')); ?></div>
    <div class="cell status caption__statusExpense"><span><span class="count_status_all"><?php echo!empty($model->countStatusAll)?$model->countStatusAll:'0';?></span>  счета,</span>  <span><span class="count_status_4"><?php echo!empty($model->countStatus4)?$model->countStatus4:'0';?></span> опл.</span></div>
</div>
<form name="hidden-form" class="hidden-form">
    <div class="row sort clearfix search_filter">
        <input type="hidden" name="create_time" value="<?php echo $queryDate;?>">
	    <input type="hidden" name="create_time_end" value="<?php echo $queryDateEnd;?>">
        <div class="cell email"><?php echo CHtml::activeTextField($model,'uid',array()); ?></div>
        <div class="cell legalName"><?php echo CHtml::activeTextField($model,'title',array()); ?></div>
<!--        <div class="cell actualName"><?php // echo CHtml::activeTextField($model,'title',array()); ?></div>-->
        <div class="cell affiliates"><a class="sortBtn" href="#"></a></div>
        <div class="cell recording"><span class="js-update-koll_record"><?php echo $model->countKollAll['koll_record'];?><span><a class="sortBtn" href="#"></a></div>
        <div class="cell surviving"><span class="js-update-koll_come"><?php echo $model->countKollAll['koll_come'];?><span><a class="sortBtn" href="#"></a></div>
        <div class="cell successing"><?php echo $model->countKollAll['koll_success'];?></div>
        <div class="cell generalSum"><span class="green"><?php echo !empty($model->price4)?$model->price4:'';?> </span><span class="red"><?php echo !empty($model->priceNo4)?$model->priceNo4:'';?></span></div>
        <div class="cell repeat"><?php echo $model->countKollAll['diagnostik'];?></div>
        <div class="cell diagnostika"><?php echo $model->countKollAll['d_price'];?></div>
        <div class="cell date"><?php echo $model->countKollAll['repeat_koll'];?></div>
        <div class="cell status"><?php if(empty($_GET['status'])):?><?php echo CHtml::activeDropDownList($model,'status',array("0"=>"Без статуса",1=>'Отчет',2=>'Согласование',3=>'Счет',4=>'Оплата',5=>'Частично',6=>'Дебиторка',7=>'Отказ'),array('')); ?><?php endif;?></div>
    </div>
</form>