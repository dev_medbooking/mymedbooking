<div class="row caption clearfix">
    <div class="cell number"><?php echo $sort->link('id',null,array('class'=>'csorting')); ?></div>
    <div class="cell legalName"><?php echo $sort->link('clinic_name',null,array('class'=>'csorting')); ?> -
        <div class="summary" id="summary">
            <?php $this->renderPartial('//layouts/_admin_count', array('count' => $count)); ?>
        </div>
    </div>
    <div class="cell udpate"><i class="iconupdate"></i></div>
</div>
<form name="hidden-form" class="hidden-form">
    <div class="row sort clearfix search_filter">
        <input type="hidden" name="create_time" value="<?php echo $queryDate;?>">
        <div class="cell number"></div>
        <div class="cell legalName"><?php echo CHtml::activeTextField($model,'clinic_name',array()); ?></div>
    </div>
</form>