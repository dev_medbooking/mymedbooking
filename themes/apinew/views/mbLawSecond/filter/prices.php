<div class="row caption clearfix">
    <div class="cell number"><?php echo $sort->link('id',null,array('class'=>'csorting')); ?></div>
    <div class="cell legalName"><?php echo $sort->link('title',null,array('class'=>'csorting')); ?></div>
    <!-- <div class="cell actualName"><?php //echo $sort->link('title_company',null,array('class'=>'csorting')); ?></div> -->
    <div class="cell status"><?php echo $sort->link('status',null,array('class'=>'csorting')); ?></div>
    <div class="cell koll_record"><?php echo $sort->link('koll_record',null,array('class'=>'csorting')); ?></div>
    <div class="cell koll_come"><?php echo $sort->link('koll_come',null,array('class'=>'csorting')); ?></div>
    <div class="cell koll_success"><?php echo $sort->link('koll_success',null,array('class'=>'csorting')); ?></div>
    <div class="cell all_price"><?php echo $sort->link('all_price',null,array('class'=>'csorting')); ?></div>
    <div class="cell create_time"><?php echo $sort->link('create_time',null,array('class'=>'csorting')); ?></div>
	<div class="cell date_status_1"><?php echo $sort->link('date_status_1',null,array('class'=>'csorting')); ?></div>
	<div class="cell date_status_2"><?php echo $sort->link('date_status_2',null,array('class'=>'csorting')); ?></div>
	<div class="cell date_status_3"><?php echo $sort->link('date_status_3',null,array('class'=>'csorting')); ?></div>
	<div class="cell date_status_4"><?php echo $sort->link('date_status_4',null,array('class'=>'csorting')); ?></div>
	<div class="cell date_status_5"><?php echo $sort->link('date_status_5',null,array('class'=>'csorting')); ?></div>
</div>
<form name="hidden-form" class="hidden-form">
<div class="row sort clearfix search_filter">
    <div class="cell number"></div>
    <div class="cell legalName"><?php echo CHtml::activeTextField($model,'title',array()); ?></div>
    <!--  <div class="cell actualName"><?php //echo CHtml::activeTextField($model,'title_company',array()); ?></div> -->
    <div class="cell status"><?php echo CHtml::activeDropDownList($model,'status',array("0"=>"Без статуса",1=>'Отчет',2=>'Согласование',3=>'Счет',4=>'Оплата'),array('')); ?></a></div>
    <div class="cell koll_record"><?php echo CHtml::activeTextField($model,'koll_record',array()); ?></div>
    <div class="cell koll_come"><?php echo CHtml::activeTextField($model,'koll_come',array()); ?></div>
    <div class="cell koll_success"><?php echo CHtml::activeTextField($model,'koll_success',array()); ?></div>
    <div class="cell all_price"><?php echo CHtml::activeTextField($model,'all_price',array()); ?></div>
    <div class="cell create_time"><?php echo CHtml::activeTextField($model,'create_time',array()); ?></div>
	<div class="cell date_status_1"><?php echo CHtml::activeTextField($model,'date_status_1',array()); ?></div>
	<div class="cell date_status_2"><?php echo CHtml::activeTextField($model,'date_status_2',array()); ?></div>
	<div class="cell date_status_3"><?php echo CHtml::activeTextField($model,'date_status_3',array()); ?></div>
	<div class="cell date_status_4"><?php echo CHtml::activeTextField($model,'date_status_4',array()); ?></div>
	<div class="cell date_status_5"><?php echo CHtml::activeTextField($model,'date_status_5',array()); ?></div>
</div>
</form>