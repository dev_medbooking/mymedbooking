<div class="row caption clearfix">
    <div class="cell number"><?php echo $sort->link('id',null,array('class'=>'csorting')); ?></div>
    <div class="cell legalName"><?php echo $sort->link('title',null,array('class'=>'csorting')); ?>
        <?php $this->renderPartial('//layouts/_admin_count',array('count'=>$count)); ?>
    </div>
    <!-- <div class="cell actualName"><?php //echo $sort->link('title_company',null,array('class'=>'csorting')); ?></div> -->
    <div class="cell address"><?php echo $sort->link('address',null,array('class'=>'csorting')); ?></div>
    <div class="cell email"><?php echo $sort->link('email',null,array('class'=>'csorting')); ?></div>
    <div class="cell phone"><?php echo $sort->link('telephone1',null,array('class'=>'csorting')); ?></div>
    <div class="cell metro"><?php echo $sort->link('subway',null,array('class'=>'csorting')); ?></div>
</div>
<form name="hidden-form" class="hidden-form">
    <div class="row sort clearfix search_filter">
        <div class="cell number"></div>
        <div class="cell legalName"><?php echo CHtml::activeTextField($model,'title',array()); ?></div>
        <!-- <div class="cell actualName"><?php //echo CHtml::activeTextField($model,'title_company',array()); ?></div> -->
        <div class="cell address"><?php echo CHtml::activeTextField($model,'address',array()); ?></a></div>
        <div class="cell email"><?php echo CHtml::activeTextField($model,'email',array()); ?></div>
        <div class="cell phone"><?php echo CHtml::activeTextField($model,'telephone1',array()); ?></div>
        <div class="cell metro"><?php echo CHtml::activeTextField($model,'subway',array()); ?></div>
    </div>
</form>