<?php if(!empty($data)): ?>
    <?php foreach($data as $key=>$value): ?>
        <?php if(!empty($value->primaryKey)): ?>
            <?php $value->queryDate=$queryDate;?>
            <?php $this->renderPartial('_data',array('model'=>$value,'key'=>$key,'queryDate'=>$queryDate)); ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>