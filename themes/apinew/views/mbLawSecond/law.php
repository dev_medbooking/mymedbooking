<section id="content">
    <div class="center">
        <div class="summary" id="summary">
            <?php $this->renderPartial('//layouts/_admin_count',array('count'=>$count)); ?>
        </div>
        <div class="table table_law all_law" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/law',array('t'=>1));?>">
            <div class="filter-table"><?php $this->renderPartial('//mbLawSecond/_filter', array('sort' => $sort, 'model' => $model)); ?></div>
            <div class="body-table"><?php  $this->renderPartial('_law',array('data'=>$data,'queryDate'=>$queryDate)); ?></div>
        </div>
    </div>
    <div class="pagination row" id="pages">
        <?php if (!empty($pages) && !empty($count)): ?>
            <?php $this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count)); ?>
        <?php endif; ?>
    </div>
</section>
