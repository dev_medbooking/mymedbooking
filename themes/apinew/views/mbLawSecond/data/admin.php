<div class="row line clearfix <?php echo!empty($model->statusInteger())?'status_color_'.$model->statusInteger():'';?> <?php echo $model->agreement=='расторгнут'?'no-agreement':'';?>">
    <div class="cell number"><?php echo $model->id;?></div>
    <div class="cell print_clinic"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/exelAdminSingle',array('id'=>$model->id));?>" class="import_exel_single" class="row_deleted">K</a></div>
    <div class="cell print_clinic"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/exelAdminLawSingle',array('id'=>$model->id));?>" class="import_exel_single" class="row_deleted">Ю</a></div>
    <div class="cell print_plus"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/createClinic',array('integrator_id'=>$model->id));?>">+</a></div>
    <div class="cell download_file_text"></div>
    <div class="cell email"><?php echo !empty($model->iu->nameperson)?$model->iu->nameperson:'';?></div>
    <div class="cell legalName"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/exelAdminRecords',array('id'=>$model->id));?>" class="import_exel_single" class="row_deleted"><?php echo!empty($model->title)?$model->title:'';?></a>
     <a href="<?php echo Yii::app()->createUrl('mbLawSecond/update',array('id'=>$model->id));?>" class="update iconupdate"></a><a href="<?php echo Yii::app()->createUrl('mbLawSecond/delete',array('id'=>$model->id));?>" class="deleted row_deleted iconclear"></a>
    </div>
<!--    <div class="cell actualName"><?php //echo!empty($model->title)?$model->title:'';?></div>-->
    <div class="cell affiliates"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/viewCompany');?>" data-id="<?php echo $model->id;?>" data-status="<?=$status;?>" data-period="<?=$period;?>" data-date="<?php echo $queryDate?>" data-dateEnd="<?php echo $queryDateEnd?>"><?php echo !empty($model->countClinic)?Yii::t('app',"{n} клиника|{n} клиники|{n} клиники",array($model->countClinic)) :'клиник нет';?></a></div>
    <div class="cell recording"><?php echo!empty($model->countAll['koll_record'])?$model->countAll['koll_record']:'<span class="substrLine">0</span>';?></div>
    <div class="cell surviving"><?php echo!empty($model->countAll['koll_come'])?$model->countAll['koll_come']:'<span class="substrLine">0</span>';?><?php if(!empty($model->countAll['koll_record'])&&!empty($model->countAll['koll_come'])):?><i class="difference"> <?php echo $model->countAll['koll_record']-$model->countAll['koll_come']; ?></i><?php endif;?></div>
    <div class="cell surviving"><?php echo!empty($model->countAll['koll_success'])?$model->countAll['koll_success']:'<span class="substrLine">0</span>';?><?php if(!empty($model->countAll['koll_record'])&&!empty($model->countAll['koll_come'])):?><i class="difference"><?php echo $model->countAll['koll_record']-$model->countAll['koll_success']; ?></i><?php endif;?></div>
    <div class="cell generalSum"><span class="red"><?php echo!empty($model->countAll['all_price'])?$model->countAll['all_price'].'':'<span class="substrLine">0</span>';?> </span></div>
    <div class="cell surviving"><?php echo!empty($model->countAll['diagnostik'])?$model->countAll['diagnostik']:'<span class="substrLine">0</span>';?></div>
    <div class="cell surviving"><?php echo!empty($model->countAll['d_price'])?$model->countAll['d_price']:'<span class="substrLine">0</span>';?></div>
    <div class="cell surviving data"><?php echo!empty($model->countAll['repeat_koll'])?$model->countAll['repeat_koll']:'<span class="substrLine">0</span>';?></div>
    <div class="cell status"><?php echo!empty($model->statusText)?$model->statusText:'<span class="substrLine">0</span>';?> <?php echo!empty($model->statusDate)?' - '.strftime ('%d.%m.%y', strtotime($model->statusDate)):'';?></div>
</div>