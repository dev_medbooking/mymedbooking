<div class="cell number"><?php echo $model->id; ?></div>
<div class="cell legalName legalName_open"><?php echo!empty($model->title) ? $model->title : ''; ?></div>
<!--<div class="cell actualName"><?php //echo!empty($model->title) ? $model->title : ''; ?></div>-->
<div class="cell affiliates"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/viewCompany'); ?>" data-id="<?php echo $model->id; ?>" data-status="<?=$status;?>" data-date="<?php echo $queryDate ?>" data-dateEnd="<?php echo $queryDateEnd ?>"><?php echo!empty($model->countClinic) ? Yii::t('app', "{n} клиника|{n} клиники|{n} клиники", array($model->countClinic)) : 'клиник нет'; ?></a></div>
<div class="main_number clearfix">
    <div class="cell recording"><?php echo!empty($model->countAll['koll_record'])?$model->countAll['koll_record']:'0';?></a></div>
    <div class="cell surviving"><span><?php echo!empty($model->countAll['koll_come']) ? $model->countAll['koll_come'] : '<span class="substrLine">0</span>'; ?><?php if (!empty($model->countAll['koll_record']) && !empty($model->countAll['koll_come'])): ?></span><i class="difference"> <?php echo $model->countAll['koll_record'] - $model->countAll['koll_come']; ?></i><?php endif; ?></div>
    <div class="cell successing"><span><?php echo!empty($model->countAll['koll_success']) ? $model->countAll['koll_success'] : '<span class="substrLine">0</span>'; ?><?php if (!empty($model->countAll['koll_record']) && !empty($model->countAll['koll_come'])): ?></span><i class="difference"><?php echo $model->countAll['koll_record'] - $model->countAll['koll_success']; ?></i><?php endif; ?></div>
    <div class="cell generalSum"><span class="red"><?php echo!empty($model->countAll['all_price']) ? $model->countAll['all_price'] . '' : '<span class="substrLine">0</span>'; ?> </span></div>
<!--    <div class="cell status"><?php //echo!empty($model->statusText) ? $model->statusText : '<span class="substrLine">…</span>'; ?> <?php //echo!empty($model->statusDate) ? ' - ' . strftime('%d/%m/%y', strtotime($model->statusDate)) : ''; ?></div>-->
</div>
   
   <div class="hiddenRow hiddenAffiliates clearfix">
    <div class="hiddenAffiliates__left clearfix">
        <ul class="hiddenAffiliates__left__decription">
            <li class="hiddenAffiliates__left__decription__contract">
                <span>Договор <strong>№ <?php echo !empty($model->numb_agree)?$model->numb_agree:'Номер не указан';?></strong></span>
                <p>Адрес: <?php echo !empty($model->address)?$model->address:'Адресс не указан';?></p>
                <p>соглашение: <?php echo !empty($model->agreement)?$model->agreement:'нет указано';?></p>
            </li>
            <li class="hiddenAffiliates__left__decription__address">
                
                
            </li>
            <li class="hiddenAffiliates__left__decription__contactPerson">
                <span class="contactUser">Контактное лицо</span>
                <p><span class="nameUser"><?php echo !empty($model->name)?$model->name:'Конт.лицо не указано';?></span> <span class="phoneUser"><?php echo !empty($model->telephone)?$model->telephone:'Телефон не указан';?></span></p>
            </li>
        </ul>
        <?php if(!empty($clinics)):?>
            <div class="hiddenAffiliates__left__options">
                <?php foreach ($clinics as $key=>$value):$value->queryDate=$queryDate;$value->queryDateEnd=$queryDateEnd;$value->status=$status;?>
                <div class="hiddenAffiliates__left__options__row clearfix ajax_link" data-date="<?php echo $queryDate;?>" data-dateEnd="<?php echo $queryDateEnd;?>" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/ajaxUpdateRow')?>">
                        <div class="hiddenAffiliates__left__options__cell hiddenAffiliates__left__options__cellName">
                            <p><a href="<?php echo Yii::app()->createUrl('mbLawSecond/exelAdminRecordsSingle',array('id'=>$value->id));?>" class="import_exel_single"><?php echo!empty($value->title)?$value->title:'';?></a><a href="<?php echo Yii::app()->createUrl('mbLawSecond/updateClinic',array('id'=>$value->id));?>" class="iconupdate"></a><span class="download_file_text2"></span></p>
                            
                            <span class="addressClinic"><?php echo!empty($value->address)?$value->address:'Адресс не указан';?></span>
                        </div>
                            <?php if(!empty($value->countKollRecord)):?>
                                <div class="column_block_price">
                                    <?php if (!empty($value->countAll)): ?>
                                        <a href="<?php echo Yii::app()->createUrl('mbLawSecond/clonePrice', array('id' => $value->countAll[0]['id'])) ?>" class="js-clone-price clone_price_icon">+</a>
                                        <?php foreach ($value->countAll as $key => $value): ?>
                                            <?php $this->renderPartial('//mbLawSecond/ajax/value', array('model' => $value)); ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            <?php else:?>
                                <div class="hiddenAffiliates__left__options__cell none_order"><a href="<?php echo Yii::app()->createUrl('mbLawSecond/createPrice',array('id'=>$value->id));?>">нет заявки на этот месяц</a></div>
                            <?php endif;?>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif;?>
    </div>
    <div class="hiddenAffiliates__right">
        <div class="hiddenAffiliates__right__top clearfix">
            <div class="hiddenAffiliates__right__top__invoice" <?php if ($period) : ?>style="display: none;"<?php endif;?>>
                <select name="company_status" id="company_status" <?php if ($period) : ?>disabled="disabled"<?php endif;?>>
                    <option value="1" <?php echo $model->statusInteger()==1?'selected':'';?>>Отч.</option>
                    <option value="2" <?php echo $model->statusInteger()==2?'selected':'';?>>Согл.</option>
                    <option value="3" <?php echo $model->statusInteger()==3?'selected':'';?>>Счет</option>
                    <option value="4" <?php echo $model->statusInteger()==4?'selected':'';?>>Опл.</option>
                    <option value="5" <?php echo $model->statusInteger()==5?'selected':'';?>>Част.</option>
	                <option value="6" <?php echo $model->statusInteger()==6?'selected':'';?>>Дебит</option>
	                <option value="7" <?php echo $model->statusInteger()==7?'selected':'';?>>Отказ</option>
                </select>
            </div>
            <div class="hiddenAffiliates__right__top__calendarWrap" <?php if ($period) : ?>style="display: none;"<?php endif;?>>
                <input type="text" class="calendar_status" <?php if ($period) : ?>disabled="disabled"<?php endif;?>/>
                <span class="save_status" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/updateStatus');?>">Сохранить</span>
            </div>
            <div class="hiddenAffiliates__right__top__calendarWrap__selectDate" <?php if ($period) : ?>style="display: none;"<?php endif;?>><span>Выбрать дату</span></div>
            <div class="cell hiddenAffiliates__right__top__calendarWrap__close">
                <a class="hiddenAffiliates__right__top__btnClose" href="<?php echo Yii::app()->createUrl('mbLawSecond/closeCompany');?>" data-id="<?php echo $model->id;?>" data-status="<?php echo $status;?>" data-period="<?php echo $period;?>" data-date="<?php echo $queryDate;?>" data-dateEnd="<?php echo $queryDateEnd;?>"></a>
            </div>
        </div>
        <div class="hiddenAffiliates__right__bottom" <?php if ($period) : ?>style="display: none;"<?php endif;?>>
            <ul>
                <?php if(!empty($model->statusDateQuery(1))):?><li class="clearfix"><span>Отч. – </span><span><?php echo strftime ('%d.%m.%y', strtotime($model->statusDateQuery(1)));?></span></li><?php endif;?>
                <?php if(!empty($model->statusDateQuery(2))):?><li class="clearfix"><span>Согл. – </span><span><?php echo strftime ('%d.%m.%y', strtotime($model->statusDateQuery(2)));?></span><?php if($model->statusInteger()==2):?><i class="clear del_status" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/deleteStatus');?>" data-status="2"></i><?php endif;?></li><?php endif;?>
                <?php if(!empty($model->statusDateQuery(3))):?><li class="clearfix"><span>Счет – </span><span><?php echo strftime ('%d.%m.%y', strtotime($model->statusDateQuery(3)));?></span><?php if($model->statusInteger()==3):?><i class="clear del_status" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/deleteStatus');?>" data-status="3"></i><?php endif;?></li><?php endif;?>
                <?php if(!empty($model->statusDateQuery(4))):?><li class="clearfix"><span>Опл. – </span><span><?php echo strftime ('%d.%m.%y', strtotime($model->statusDateQuery(4)));?></span><?php if($model->statusInteger()==4):?><i class="clear del_status" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/deleteStatus');?>" data-status="4"></i><?php endif;?></li><?php endif;?>
                <?php if(!empty($model->statusDateQuery(5))):?><li class="clearfix"><span>Част. – </span><span><?php echo strftime ('%d.%m.%y', strtotime($model->statusDateQuery(5)));?></span><?php if($model->statusInteger()==5):?><i class="clear del_status" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/deleteStatus');?>" data-status="5"></i><?php endif;?></li><?php endif;?>
	            <?php if(!empty($model->statusDateQuery(6))):?><li class="clearfix"><span>Дебит – </span><span><?php echo strftime ('%d.%m.%y', strtotime($model->statusDateQuery(6)));?></span><?php if($model->statusInteger()==6):?><i class="clear del_status" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/deleteStatus');?>" data-status="6"></i><?php endif;?></li><?php endif;?>
	            <?php if(!empty($model->statusDateQuery(7))):?><li class="clearfix"><span>Отказ – </span><span><?php echo strftime ('%d.%m.%y', strtotime($model->statusDateQuery(7)));?></span><?php if($model->statusInteger()==7):?><i class="clear del_status" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/deleteStatus');?>" data-status="7"></i><?php endif;?></li><?php endif;?>
            </ul>
        </div>
        <div class="hiddenAffiliates__right__top__price price" <?php if ($period) : ?>style="display: none;"<?php endif;?>>
            <?php echo!empty($model->countAll['all_price'])?$model->countAll['all_price']:'0';?> <sup>руб</sup>
        </div>
    </div>
</div>

