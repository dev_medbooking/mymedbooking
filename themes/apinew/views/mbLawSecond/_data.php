<?php if(Yii::app()->controller->action->id=='admin'): ?>
<?php $this->renderPartial('//mbLawSecond/data/admin',array('key'=>$key,'model'=>$model,'queryDate'=>$queryDate,'queryDateEnd'=>$queryDateEnd,'period'=>$period, 'status' => $status)); ?>
<?php elseif(Yii::app()->controller->action->id=='law'): ?>
<?php $this->renderPartial('//mbLawSecond/data/law',array('key'=>$key,'model'=>$model,'queryDate'=>$queryDate)); ?>
<?php elseif(Yii::app()->controller->action->id=='clinics'): ?>
<?php $this->renderPartial('//mbLawSecond/data/clinics',array('key'=>$key,'model'=>$model)); ?>
<?php elseif(Yii::app()->controller->action->id=='prices'): ?>
<?php $this->renderPartial('//mbLawSecond/data/prices',array('key'=>$key,'model'=>$model)); ?>
<?php elseif(Yii::app()->controller->action->id=='book'): ?>
<?php $this->renderPartial('//mbLawSecond/data/book',array('key'=>$key,'model'=>$model)); ?>
<?php endif; ?>