<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
<script type="text/javascript">$(function(){$("#IntegratorClinicAll_telephone").mask("+7(999)999-9999",{'placeholder':'_'});});$(function(){$(".chosen-default>span").text("Выбрать")});</script>
<div class="application-content _application-content business-top-form">
    <?php if(!empty($model->id)): ?><p><strong><?php echo $model->title; ?></strong></p><?php else:?><p><strong>Создание нового Юр.лица</strong></p><?php endif;?>
</div>
<div id="create-block" class="_create_block business-form-wrapper">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'application-form'),
    )); ?>
        <?php if($model->errors): ?><div class="well"><?php echo $form->errorSummary($model); ?></div><?php endif; ?>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'title',array('placeholder'=>'Юр. название','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'title_company',array('placeholder'=>'Факт.название','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'address',array('placeholder'=>'Адрес','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'name',array('placeholder'=>'Имя, фамилия','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'uid',array('placeholder'=>'ID пользователя','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-name note-form">
                <?php echo $form->textField($model,'numb_agree',array('placeholder'=>'Номер договора','class'=>'request')); ?>
            </div>
        </div>
        <div class="row clearfix">
            <div class="text-uid text-pencil clearfix">
                <label for="agreement"></label>
                <?php echo $form->textField($model,'agreement',array('placeholder'=>'Договор',"id"=>"agreement")); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->textArea($model,'comment',array("placeholder"=>"Комментарии")); ?>
        </div>
        <div class="row clearfix">
            <button class="_save_btn">Сохранить</button>
        </div>
    <?php $this->endWidget(); ?>
</div>
