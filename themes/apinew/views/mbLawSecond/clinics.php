<section id="content">
    <div class="center">
        <div class="table table_law table_clinic" data-href="<?php echo Yii::app()->createUrl('mbLawSecond/clinics',array('t'=>1));?>">
            <div class="filter-table"><?php $this->renderPartial('//mbLawSecond/_filter', array('sort' => $sort, 'model' => $model,'count'=>$count)); ?></div>
            <div class="body-table"><?php  $this->renderPartial('_clinics',array('data'=>$data)); ?></div>
        </div>
    </div>
    <div class="pagination row" id="pages">
        <?php if (!empty($pages) && !empty($count)): ?>
            <?php $this->renderPartial('_pages', array('sort' => $sort, 'pages' => $pages, 'count' => $count)); ?>
        <?php endif; ?>
    </div>
</section>
