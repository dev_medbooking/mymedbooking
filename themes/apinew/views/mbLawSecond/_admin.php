<?php if(!empty($data)): ?>
    <?php foreach($data as $key=>$value): ?>
        <?php if(!empty($value->primaryKey)): ?>
            <?php $value->queryDate=$queryDate;?>
			<?php $value->queryDateEnd=$queryDateEnd;?>
            <?php $this->renderPartial('_data',array('model'=>$value,'key'=>$key,'queryDate'=>$queryDate,'queryDateEnd'=>$queryDateEnd, 'period' => $period, 'status' => $status)); ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>