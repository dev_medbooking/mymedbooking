<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
<script type="text/javascript">$(function(){$("#Register_telephone").mask("+7(999)999-9999",{'placeholder':'_'});});$(function(){$(".chosen-default>span").text("Выбрать")});</script>
<div class="application-content _application-content business-top-form">
    <?php if(!empty($model->id)): ?><p><strong><?php echo $model->title; ?></strong></p><?php else:?><p><strong>Создание нового реестра договора</strong></p><?php endif;?>
</div>
<div id="create-block" class="_create_block business-form-wrapper">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'admin-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'application-form','enctype'=>'multipart/form-data'),
    )); ?>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'title',array('class'=>'control-label')); ?>
            <div class="text-name note-form">
                <?php echo $form->textField($model,'title',array('placeholder'=>'','class'=>'request')); ?>
                <?php echo $form->error($model, 'title'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'contact',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->textArea($model,'contact',array('placeholder'=>'')); ?>
                <?php echo $form->error($model, 'contact'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'telephone',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->textField($model,'telephone',array('placeholder'=>'','class'=>'request')); ?>
                <?php echo $form->error($model, 'telephone'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'data_agree',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->textField($model,'data_agree',array('placeholder'=>'','class'=>'request')); ?>
                <?php echo $form->error($model, 'data_agree'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'integrator_id',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->dropDownList($model, 'integrator_id',CHtml::listData(IntegratorClinic::model()->findAll(),'id', 'title')); ?>
                <?php echo $form->error($model, 'integrator_id'); ?>
            </div>
         </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'integrator_all_id',array('class'=>'control-label')); ?>
            <div class="text-name">
                <?php echo $form->dropDownList($model, 'integrator_all_id',CHtml::listData(IntegratorClinicAll::model()->findAll(),'id', 'title_company')); ?>
                <?php echo $form->error($model, 'integrator_all_id'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'file1',array('class'=>'control-label')); ?>
            <div class="text-name uploadFile" >
                <?php echo $form->fileField($model,'file1',array('id'=>"file1")); ?>
                <?php echo $form->error($model, 'file1'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'file2',array('class'=>'control-label')); ?>
            <div class="text-name uploadFile">
                <?php echo $form->fileField($model,'file2',array('id'=>"file1")); ?>
                <?php echo $form->error($model, 'file2'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <?php echo $form->labelEx($model,'file3',array('class'=>'control-label')); ?>
            <div class="text-name uploadFile">
                <?php echo $form->fileField($model,'file3',array('id'=>"file1")); ?>
                <?php echo $form->error($model, 'file3'); ?>
            </div>
        </div>
        <div class="row clearfix">
            <button class="">Сохранить</button>
        </div>
    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    function callCalendar2() {
            $('#Register_data_agree').datetimepicker({
                lang: "ru",
                onSelect: function(dateText, inst) {
                    var miliseconds = 2628000000;
                    var currentTime = new Date();
                    var selectedDate = new Date(dateText);
                }
            });
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Текущее время',
            closeText: 'Закрыть',
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);
    }
    callCalendar2();
</script>