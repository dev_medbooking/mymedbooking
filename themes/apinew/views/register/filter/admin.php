<div class="row caption clearfix">
    <div class="cell number"><?php echo $sort->link('id',null,array('class'=>'csorting')); ?></div>
    <div class="cell legalName"><?php echo $sort->link('title',null,array('class'=>'csorting')); ?> -
        <div class="summary" id="summary">
            <?php $this->renderPartial('//layouts/_admin_count', array('count' => $count)); ?>
        </div>
    </div>
    <div class="cell actualName"><?php echo $sort->link('telephone',null,array('class'=>'csorting')); ?></div>
    <div class="cell actualName"><?php echo $sort->link('contact',null,array('class'=>'csorting')); ?></div>
    <div class="cell recording"><?php echo $sort->link('integrator_id',null,array('class'=>'csorting')); ?></div>
    <div class="cell surviving"><?php echo $sort->link('integrator_all_id',null,array('class'=>'csorting')); ?></div>
    <div class="cell successing">Файл 1</div>
    <div class="cell date">Файл 2</div>
    <div class="cell status">Файл 3</div>
    <div class="cell update"></div>
    <div class="cell deleted"></div>
</div>
<form name="hidden-form" class="hidden-form">
    <div class="row sort clearfix search_filter">
        <div class="cell number"></div>
        <div class="cell legalName"><?php echo CHtml::activeTextField($model,'title',array()); ?></div>
        <div class="cell actualName"><?php echo CHtml::activeTextField($model,'telephone',array()); ?></div>
        <div class="cell actualName"><?php echo CHtml::activeTextField($model,'contact',array()); ?></div>
        <div class="cell recording"><?php echo CHtml::activeTextField($model,'integrator_id',array()); ?></div>
        <div class="cell surviving"><?php echo CHtml::activeTextField($model,'integrator_all_id',array()); ?></div>
        <div class="cell successing"></div>
        <div class="cell date"></div>
        <div class="cell status"></div>
        <div class="cell update"></div>
    <div class="cell deleted"></div>
    </div>
</form>