jQuery(function($){
	var validateEmail = function (email) {
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	}
    $("#date, #date_2").mask("99/99/9999");
    $("#phone").mask("+7(999) 999-99-99");
    $("#cell_land").mask("+7(999) 999-99-99");
    $("#tin").mask("99-9999999");
    $("#ssn").mask("999-99-9999");


	$("body").on("click", ".js-btn-land", function(){
		if (validateEmail($(".js-email").val())){
			$('#popup_reg').show();
			$('body').append('<div class="overlay"></div>');
			$('.tooltip').hide();
		}else{
			console.log("ERROR");
			$('.tooltip').show();
		}
		return false;
	});	
	$("body").on("click", ".js-btn-bottom", function(){
		if(validateEmail($(".js-email-bottom").val())){
			$('#popup_reg').show();
			$('body').append('<div class="overlay"></div>');
			$('.tooltip_bottom').hide();
			
		}else{
			console.log("ERROR");
			$('.tooltip_bottom').show();
		}
		return false;
	});
    $('body').on('click','.popup_head i, .overlay', function(){
        $('#popup_reg').hide();
     });    
	$('.btn_call').click(function(){
        $('#popup_call').show();
        return false;
    });
    $('body').on('click','.popup_head_call i, .overlay', function(){
        $('#popup_call').hide();
     });
     $('.btn_land, .btn_call').click(function(){
         $('body').append('<div class="overlay"></div>');
     });
     $('body').on('click','.popup_head i, .popup_head_call i, .overlay', function(){
        $('.overlay').remove();
    });
	});

	$(document).ready(function(){
    $('section[data-type="background"]').each(function(){
        var $bgobj = $(this); // создаем объект
        $(window).scroll(function() {
            var yPos = -($(window).scrollTop() / $bgobj.data('speed')); // вычисляем коэффициент 
            // Присваиваем значение background-position
            var coords = 'center '+ yPos + 'px';
            // Создаем эффект Parallax Scrolling
            $bgobj.css({ backgroundPosition: coords });
        });
    });
});
$(document).ready(function() {
        new WOW().init();
    });
$(document).ready(function($){
   $('a[href*=#]').on("click", function(e){
      var anchor = $(this);
      $('html, body').animate({
         scrollTop: $(anchor.attr('href')).offset().top
      }, 1000);
      e.preventDefault();
   });

});
//function wefwef() {
//var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
//$("#f_mail").change(function(){
//var r=$("#f_mail").val();
//if (!reg.test(r)) {
//$("#f_mail").css("backgroundColor", "red");
//}
//else{
//$("#f_mail").css("backgroundColor", "white");   
//}
//});
//}
//   function isEmail() {
//    var str = document.getElementById("email").value;
//    var status = document.getElementById("status");
//    var re = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;
//    if (re.test(str)) status.innerHTML = "Адрес правильный";
//      else status.innerHTML = "Адрес неверный";
//    if(isEmpty(str)) status.innerHTML = "Поле пустое";
//   }
//   function isEmpty(str){
//    return (str == null) || (str.length == 0);
//   }
