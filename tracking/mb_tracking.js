/**
 * Created by I.Noskov on 29.04.2016.
 */
var MB_STAT_SERVER = "http://crm.medbooking.com/track";
var COOKIE_NAME = 'domain_tracking_sid';

var mb_getCookie = function(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

var mb_setCookie = function (name, value, options) {
    options = options || {};
    var expires = options.expires;
    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }
    value = encodeURIComponent(value);
    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
    document.cookie = updatedCookie;
}

var mb_tracking=function(project_id, location){
    var uid=mb_getCookie(COOKIE_NAME);
    $.ajax({
        url: MB_STAT_SERVER,
        type: 'post',
        dataType: 'text',
        data:{
            referer:(location?document.location:document.referrer),
            project:project_id,
            sid:(uid===undefined?'':uid)
        },
        crossDomain: true,
        success: function(data) {
            mb_setCookie(COOKIE_NAME, data, {expires:3600*24*365*5});
        }
    });


}