define({ "api": [
  {
    "type": "get",
    "url": "/api/categories",
    "title": "Categories",
    "version": "0.1.0",
    "group": "Category",
    "description": "<p>Список категорий</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": "<p>ID категории</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Название</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "translit",
            "description": "<p>Название (транслит)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "level",
            "description": "<p>Уровень вложенности категории</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>Страница</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "description": "<p>Размер страницы</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sort",
            "description": "<p>Сортировка</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/categories"
      }
    ],
    "filename": "protected/controllers/api/ApiCategoryController.php",
    "groupTitle": "Category",
    "name": "GetApiCategories"
  },
  {
    "type": "get",
    "url": "/api/clinics",
    "title": "Clinics",
    "version": "0.1.0",
    "group": "Clinics",
    "description": "<p>Список клиник</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID Партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": "<p>ID Клиники</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "title",
            "description": "<p>Название</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "specialist",
            "description": "<p>Специальность врача</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "translit",
            "description": "<p>Название (транслит)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>Адрес</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "telephone",
            "description": "<p>Телефон</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "url",
            "description": "<p>Ссылка</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "subway_name",
            "description": "<p>Станция метро</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>Страница</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "description": "<p>Размер странцы</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sort",
            "description": "<p>Сортировка</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/clinics"
      }
    ],
    "filename": "protected/controllers/api/ApiClinicController.php",
    "groupTitle": "Clinics",
    "name": "GetApiClinics"
  },
  {
    "type": "get",
    "url": "/api/dclinics",
    "title": "Diagnostic clinics",
    "version": "0.1.0",
    "group": "Clinics",
    "description": "<p>Список диагностических клиник</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID Партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": "<p>ID Клиники</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "title",
            "description": "<p>Название</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "translit",
            "description": "<p>Название (транслит)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>Адрес</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "telephone",
            "description": "<p>Телефон</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "subway_name",
            "description": "<p>Станция метро</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "service",
            "description": "<p>ID услуги</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>Страница</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "description": "<p>Размер странцы</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sort",
            "description": "<p>Сортировка</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/dclinics"
      }
    ],
    "filename": "protected/controllers/api/ApiDiagnosticClinicController.php",
    "groupTitle": "Clinics",
    "name": "GetApiDclinics"
  },
  {
    "type": "get",
    "url": "/api/doctors",
    "title": "Doctors",
    "version": "0.1.0",
    "group": "Doctor",
    "description": "<p>Список врачей</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": "<p>ID Врача</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "fio",
            "description": "<p>Фио</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "translit",
            "description": "<p>Фио (транслит)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "clinic_name",
            "description": "<p>Название клиники</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "child",
            "description": "<p>Детский врач</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "speciality",
            "description": "<p>Специальность</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "position",
            "description": "<p>Должность</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "startyear",
            "description": "<p>Год начала работ</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "degree",
            "description": "<p>Степень</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "clinics",
            "description": "<p>Клиники</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": true,
            "field": "clinic_id",
            "description": "<p>ID клиники</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "doctor_city_id",
            "description": "<p>ID города</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>Страница</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "description": "<p>Колл-во на странице</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sort",
            "description": "<p>Сортировка</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/doctors"
      }
    ],
    "filename": "protected/controllers/api/ApiDoctorController.php",
    "groupTitle": "Doctor",
    "name": "GetApiDoctors"
  },
  {
    "type": "get",
    "url": "/api/order/create",
    "title": "Order",
    "version": "0.1.0",
    "group": "Order",
    "description": "<p>Создание завки (для тестирования и отладки скриптов интеграции использовать тестовый режим - метод /api/order/testCreate )</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "service_id",
            "description": "<p>ID услуги</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>Телефон</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Фио пациента</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "clinic_id",
            "description": "<p>ID клиники</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "clinic_name",
            "description": "<p>Название клиники</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "doctor_id",
            "description": "<p>ID врача</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "doctor_name",
            "description": "<p>Фио доктора</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/order/testCreate"
      }
    ],
    "filename": "protected/controllers/api/ApiOrderController.php",
    "groupTitle": "Order",
    "name": "GetApiOrderCreate"
  },
  {
    "type": "get",
    "url": "/api/partner/status",
    "title": "Partner's order status",
    "version": "0.1.0",
    "group": "Partner",
    "description": "<p>Статус партнерской заявки</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID заявки</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/partner/status"
      }
    ],
    "filename": "protected/controllers/api/ApiPartnerController.php",
    "groupTitle": "Partner",
    "name": "GetApiPartnerStatus"
  },
  {
    "type": "get",
    "url": "/api/clinic-services",
    "title": "Clinics by Service",
    "version": "0.1.0",
    "group": "Services",
    "description": "<p>Услуги и список клиник с ценами эти услуги</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID Партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "services",
            "description": "<p>ID услуги</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/clinic-services"
      }
    ],
    "filename": "protected/controllers/api/ApiClinicByServicesController.php",
    "groupTitle": "Services",
    "name": "GetApiClinicServices"
  },
  {
    "type": "get",
    "url": "/api/service",
    "title": "Service Info",
    "version": "0.1.0",
    "group": "Services",
    "description": "<p>Информация об услуге</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID Партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID услуги</p>"
          }
        ]
      }
    },
    "response": {
      "fields": {
        "Parameter": [
          {
            "group": "response",
            "type": "Number",
            "optional": true,
            "field": "type",
            "description": "" +
              "<ul>" +
                "<li>" +
                    "1 - Общая" +
                "</li>" +
                "<li>" +
                    "2 - Диагностика" +
                "</li>" +
                "<li>" +
                    "3 - Анализ" +
                "</li>" +
                "<li>" +
                    "4 - Стоматология" +
                "</li>" +
              "</ul>"
          },
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/service"
      }
    ],
    "filename": "protected/controllers/api/ApiServiceController.php",
    "groupTitle": "Services",
    "name": "GetApiService"
  },
  {
    "type": "get",
    "url": "/api/services",
    "title": "Services",
    "version": "0.1.0",
    "group": "Services",
    "description": "<p>Список услуг</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID Партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": "<p>ID категории услуг</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "name",
            "description": "<p>Название категории услуг</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>Страница</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "description": "<p>размер странцы</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/services"
      }
    ],
    "filename": "protected/controllers/api/ApiServiceController.php",
    "groupTitle": "Services",
    "name": "GetApiServices"
  },
  {
    "type": "get",
    "url": "/api/statistics",
    "title": "Statistics",
    "version": "0.1.0",
    "group": "Statistics",
    "description": "<p>Статистика</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Поиск (по полям: имя, телефон, услуга, id)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "status",
            "description": "<p>Статус</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "date_start",
            "description": "<p>Дата &quot;от&quot; формат &quot;Y-m-d H:i:s&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "date_end",
            "description": "<p>Дата &quot;до&quot;  формат &quot;Y-m-d H:i:s&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>Страница</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "description": "<p>размер странцы</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/statistics"
      }
    ],
    "filename": "protected/controllers/api/ApiStatisticsController.php",
    "groupTitle": "Statistics",
    "name": "GetApiStatistics"
  },
  {
    "type": "get",
    "url": "/api/subway",
    "title": "Subway",
    "version": "0.1.0",
    "group": "Subway",
    "description": "<p>Список станций метро</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ident_id",
            "description": "<p>ID Партнера</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": "<p>ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "title",
            "description": "<p>Нвзвание</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "translit",
            "description": "<p>Название (транслит)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "city",
            "description": "<p>ID Города</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "subway_line_id",
            "description": "<p>id ветки метро</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>Страница</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "description": "<p>размер странцы</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sort",
            "description": "<p>Сортировка</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/subway"
      }
    ],
    "filename": "protected/controllers/api/ApiSubwayController.php",
    "groupTitle": "Subway",
    "name": "GetApiSubway"
  }
] });
