define({
  "name": "API medbooking.com",
  "version": "0.1.0",
  "description": "API для работы с сайтом medbooking.com. Вопросы по работе API/технические вопросы/предложения по улучшению отправлять на  mg@medbooking.com, mario@medbooking.com",
  "title": "API medbooking.com",
  "url": "a.medbooking.com",
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-05-25T10:16:45.594Z",
    "url": "http://apidocjs.com",
    "version": "0.16.1"
  }
});
